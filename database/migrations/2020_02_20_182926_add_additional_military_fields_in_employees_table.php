<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalMilitaryFieldsInEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('group_of_accounting')
                ->nullable()
                ->comment('Группа учета');

            $table->string('reserve_category')
                ->nullable()
                ->comment('Группа запаса');

            $table->boolean('is_on_special_accounting')
                ->default(false)
                ->comment('Состоит на спецучете');

            $table->boolean('has_military_preparation')
                ->default(false)
                ->comment('Имеет военную подготовку');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
        });
    }
}
