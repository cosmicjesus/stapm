<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProgramSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_subjects', function (Blueprint $table) {
            $table->dropColumn('year');
            $table->unsignedBigInteger('program_academic_year_id')->after('subject_id')->comment('ID учебного года профпрограммы')->nullable();
        });
        Schema::table('program_subjects', function (Blueprint $table) {
            $table->foreign('program_academic_year_id')
                ->references('id')
                ->on('profession_programs_academic_years')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
