<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupLoadSubjectPeriods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_load_subject_periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('load_subject_id');
            $table->unsignedBigInteger('term_id');
            $table->unsignedInteger('plan');
            $table->unsignedInteger('semester')->nullable();
            $table->unsignedBigInteger('employee_id');
            $table->timestamps();
        });

        Schema::table('group_load_subject_periods', function (Blueprint $table) {
            $table->foreign('load_subject_id')
                ->on('group_load_subjects')
                ->references('id')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('term_id')->on('training_calendars_items')->references('id')->onDelete('RESTRICT')->onUpdate('CASCADE');
            $table->foreign('employee_id')->on('employees')->references('id')->onDelete('RESTRICT')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_load_subject_periods');
    }
}
