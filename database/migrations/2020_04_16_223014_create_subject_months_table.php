<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_months', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('month_id');
            $table->unsignedBigInteger('subject_id');
            $table->timestamps();
        });

        Schema::table('subject_months', function (Blueprint $table) {
            $table->foreign('month_id')
                ->on('load_months')
                ->references('id')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

            $table->foreign('subject_id')
                ->on('group_load_subjects')
                ->references('id')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_months');
    }
}
