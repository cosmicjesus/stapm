<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledInStudentDecreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_decrees', function (Blueprint $table) {
            $table->string('reason')->nullable()->default('onConditionsOfFreeAdmission');
            $table->string('group_from')->nullable();
            $table->string('group_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_decrees', function (Blueprint $table) {
            //
        });
    }
}
