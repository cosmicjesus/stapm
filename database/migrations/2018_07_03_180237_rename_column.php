<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            $table->renameColumn('count_out_with_documents', 'count_with_out_documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            $table->renameColumn('count_with_out_documents', 'count_out_with_documents');
        });
    }
}
