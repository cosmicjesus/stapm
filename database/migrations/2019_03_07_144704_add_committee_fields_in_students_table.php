<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommitteeFieldsInStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->unsignedBigInteger('selection_committee_id')->nullable();
            $table->unsignedBigInteger('recruitment_program_id')->nullable();

            $table->foreign('selection_committee_id')
                ->references('id')
                ->on('selection_committees')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

            $table->foreign('recruitment_program_id')
                ->references('id')
                ->on('priem_programs')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
