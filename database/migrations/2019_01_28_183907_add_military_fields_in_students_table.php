<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMilitaryFieldsInStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('military_document_type')->comment('Тип документа воинского учета')->nullable();
            $table->string('military_document_number')->comment('Номер документа воинского учета')->nullable();
            $table->string('fitness_for_military_service')->comment('Категория годности к военной службе')->nullable();
            $table->string('military_recruitment_office')->comment('Название военкомата')->nullable();
            $table->string('military_specialty')->comment('Специальность')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
