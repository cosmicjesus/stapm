<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lastname');
            $table->string('firstname');
            $table->string('middlename');
            $table->date('birthday');
            $table->integer('gender');
            $table->unsignedBigInteger('education_id');
            $table->unsignedBigInteger('profession_program_id');
            $table->unsignedBigInteger('group_id')->nullable();
            $table->date('passport_issuance_date')->nullable();
            $table->double('avg')->nullable();
            $table->date('graduation_date');
            $table->date('start_date');
            $table->boolean('has_original');
            $table->boolean('has_certificate');
            $table->boolean('need_hostel');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
