<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacingClassesImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replacing_classes_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('replacing_class_id');
            $table->string('path');
            $table->integer('sort')->default(500);
            $table->timestamps();
        });

        Schema::table('replacing_classes_images', function (Blueprint $table) {
            $table->foreign('replacing_class_id')
                ->references('id')
                ->on('replacing_classes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replacing_classes_images');
    }
}
