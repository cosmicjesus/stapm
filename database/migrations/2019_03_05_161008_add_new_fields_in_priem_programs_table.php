<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsInPriemProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            $table->unsignedBigInteger('selection_committee_id')->nullable()->after('profession_program_id');
            $table->foreign('selection_committee_id')
                ->references('id')
                ->on('selection_committees')
                ->onDelete('RESTRICT')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            //
        });
    }
}
