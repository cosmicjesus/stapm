<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('profession_program_id');
            $table->string('name');
            $table->integer('count_period');
            $table->date('start_training');
            $table->date('end_training');
            $table->string('file')->nullable();
            $table->string('full_path')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('in_revision');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_plans');
    }
}
