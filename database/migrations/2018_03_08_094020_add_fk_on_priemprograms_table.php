<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkOnPriemprogramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('priem_programs',function (Blueprint $table){
            $table->foreign('profession_program_id')
                ->references('id')
                ->on('profession_programs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('priem_programs',function (Blueprint $table){
            $table->dropForeign('priem_programs_profession_program_id_foreign');
        });
    }
}
