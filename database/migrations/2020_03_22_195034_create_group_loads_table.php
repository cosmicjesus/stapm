<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_loads', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('academic_year');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('group_period_id');
            $table->timestamps();
        });

        Schema::table('group_loads', function (Blueprint $table) {
            $table->foreign('group_id')
                ->on('training_groups')
                ->references('id')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('group_period_id')
                ->on('training_group_academic_years')
                ->references('id')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_loads');
    }
}
