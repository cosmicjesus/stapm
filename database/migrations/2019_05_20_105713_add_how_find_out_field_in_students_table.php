<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHowFindOutFieldInStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('how_find_out')->comment('Откуда узнали')->nullable();
            $table->unsignedBigInteger('invited_student_id')->comment('ID Пригласившего студента')->nullable();

            $table->foreign('invited_student_id')
                ->on('students')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_invited_student_id_foreign');
            $table->dropColumn(['how_find_out', 'invited_student_id']);
        });
    }
}
