<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFixCountFieldInProgramPriem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            $table->integer('count_with_documents')->default(0);
            $table->integer('count_out_with_documents')->default(0);
            $table->integer('number_of_enrolled')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('priem_programs', function (Blueprint $table) {
            $table->dropColumn(['count_with_documents', 'count_out_with_documents', 'number_of_enrolled']);
        });
    }
}
