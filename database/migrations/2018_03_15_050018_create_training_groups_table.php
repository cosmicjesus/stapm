<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('profession_program_id');
            $table->unsignedBigInteger('education_plan_id');
            $table->unsignedBigInteger('employee_id')->nullable()->default(null);
            $table->string('unique_code');
            $table->string('pattern');
            $table->integer('max_people')->default(25);
            $table->integer('course')->default(1);
            $table->integer('period')->default(1);
            $table->string('status')->default();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_groups');
    }
}
