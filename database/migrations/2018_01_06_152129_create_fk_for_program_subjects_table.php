<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkForProgramSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_subjects', function (Blueprint $table) {
           $table->foreign('profession_program_id')
               ->references('id')
               ->on('profession_programs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('subject_id')
                ->references('id')
                ->on('subjects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_subjects', function (Blueprint $table) {
            $table->dropForeign('program_subjects_profession_program_id_foreign');

            $table->dropForeign('program_subjects_subject_id_foreign');
        });
    }
}
