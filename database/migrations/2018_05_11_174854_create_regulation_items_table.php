<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regulation_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('regulation_id');
            $table->text('name_of_violation');
            $table->text('result')->nullable();
            $table->string('status');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::table('regulation_items', function (Blueprint $table) {
            $table->foreign('regulation_id')
                ->references('id')
                ->on('regulations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regulation_items', function (Blueprint $table) {
            $table->dropForeign('regulation_items_regulation_id_foreign');
        });

        Schema::dropIfExists('regulation_items');
    }
}
