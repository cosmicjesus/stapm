<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingGroupAcademicYearTerm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_group_academic_year_terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('academic_year_id')->nullable()->comment('ID академического года');
            $table->unsignedBigInteger('calendar_item_id')->nullable()->comment('ID элемента учебного календаря');
            $table->integer('number')->nullable()->default(1)->comment('Номер периода');
            $table->timestamps();
        });

        Schema::table('training_group_academic_year_terms', function (Blueprint $table) {
            $table->foreign('academic_year_id')
                ->on('training_group_academic_years')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('calendar_item_id')
                ->on('training_calendars_items')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_group_academic_year_terms');
    }
}
