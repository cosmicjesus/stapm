<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadSubjectItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_subject_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('month_id');
            $table->date('date');
            $table->unsignedInteger('number');
            $table->unsignedInteger('count');
            $table->timestamps();
        });

        Schema::table('load_subject_items', function (Blueprint $table) {
            $table->foreign('month_id')
                ->on('subject_months')
                ->references('id')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_subject_items');
    }
}
