<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkForTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_educations',function (Blueprint $table){
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('education_id')
                ->references('id')
                ->on('education')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('employee_positions',function (Blueprint $table){
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('position_id')
                ->references('id')
                ->on('positions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_educations',function (Blueprint $table){
            $table->dropForeign('employee_educations_employee_id_foreign');

            $table->dropForeign('employee_educations_education_id_foreign');
        });

        Schema::table('employee_positions',function (Blueprint $table){
            $table->dropForeign('employee_positions_employee_id_foreign');

            $table->dropForeign('employee_positions_position_id_foreign');
        });
    }
}
