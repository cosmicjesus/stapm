<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkForProfessionProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profession_programs', function (Blueprint $table) {
            $table->foreign('profession_id')
                ->references('id')
                ->on('professions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('education_form_id')
                ->references('id')
                ->on('education_forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profession_programs', function (Blueprint $table) {
            $table->dropForeign('profession_programs_profession_id_foreign');

            $table->dropForeign('profession_programs_education_form_id_foreign');
        });
    }
}
