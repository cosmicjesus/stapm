<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFormatFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn(['privileged_category', 'health_category', 'disability']);
        });
        Schema::table('students', function (Blueprint $table) {

            $table->integer('privileged_category')->nullable();
            $table->integer('health_category')->nullable();
            $table->integer('disability')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
//            $table->integer('privileged_category')->nullable();
//            $table->integer('health_category')->nullable();
//            $table->integer('disability')->nullable();
        });
    }
}
