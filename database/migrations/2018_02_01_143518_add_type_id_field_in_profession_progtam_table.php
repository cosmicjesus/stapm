<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeIdFieldInProfessionProgtamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profession_programs',function (Blueprint $table){
            $table->unsignedBigInteger('type_id')->nullable()->after('education_form_id');

            $table->foreign('type_id')
                ->references('id')
                ->on('profession_program_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profession_programs',function (Blueprint $table){
            $table->dropForeign('profession_programs_type_id_foreign');
            $table->dropColumn('type_id');
        });
    }
}
