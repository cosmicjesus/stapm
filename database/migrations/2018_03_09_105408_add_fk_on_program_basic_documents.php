<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkOnProgramBasicDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_basic_documents', function (Blueprint $table) {
            $table->foreign('profession_program_id')
                ->references('id')
                ->on('profession_programs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_basic_documents', function (Blueprint $table) {
            $table->dropForeign('program_basic_documents_profession_program_id_foreign');
        });
    }
}
