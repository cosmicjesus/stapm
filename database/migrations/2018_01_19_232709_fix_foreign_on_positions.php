<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixForeignOnPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('positions',function (Blueprint $table){
            $table->dropForeign('positions_position_type_id_foreign');
            $table->foreign('position_type_id')
                ->references('id')
                ->on('position_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('positions',function (Blueprint $table){
            $table->dropForeign('positions_position_type_id_foreign');
        });
    }
}
