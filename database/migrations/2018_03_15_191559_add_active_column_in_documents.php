<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveColumnInDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_documents', function (Blueprint $table) {
            $table->boolean('active')->default(true)->after('parent_id');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->boolean('active')->default(true)->after('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_documents', function (Blueprint $table) {
            $table->dropColumn('active');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
