<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFkOnTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_load_subject_periods', function (Blueprint $table) {
            $table->dropForeign('group_load_subject_periods_term_id_foreign');
            $table->foreign('term_id')
                ->on('training_group_academic_year_terms')
                ->references('id')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_load_subject_periods', function (Blueprint $table) {
            //
        });
    }
}
