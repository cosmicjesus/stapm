<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingCalendarsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_calendars_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('training_calendar_id')->nullable()->index();
            $table->integer('number');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('session_start_date')->nullable();
            $table->date('session_end_date')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_calendars_items');
    }
}
