<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveFiledInTrainingGroupAcademicYearTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_group_academic_year_terms', function (Blueprint $table) {
            $table->boolean('active')->after('number')->default(false)->comment('Флаг активности');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_group_academic_year_terms', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
