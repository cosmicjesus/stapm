<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionProgramsAcademicYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profession_programs_academic_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('profession_program_id')->comment('ID программы');
            $table->unsignedInteger('year_number')->comment('Номер года');
            $table->boolean('active')->default(true)->comment('Флаг активности');
            $table->timestamps();
        });

        Schema::table('profession_programs_academic_years', function (Blueprint $table) {
            $table->foreign('profession_program_id')
                ->references('id')
                ->on('profession_programs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profession_programs_academic_years');
    }
}
