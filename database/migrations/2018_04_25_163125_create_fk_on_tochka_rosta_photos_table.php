<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFkOnTochkaRostaPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tochka_rosta_photos', function (Blueprint $table) {
            $table->foreign('news_id')
                ->on('tochka_rosta_items')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tochka_rosta_photos', function (Blueprint $table) {
            $table->dropForeign('tochka_rosta_photos_news_id_foreign');
        });
    }
}
