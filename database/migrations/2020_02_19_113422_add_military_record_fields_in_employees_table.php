<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMilitaryRecordFieldsInEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('military_document_type')
                ->default(null)
                ->nullable()
                ->comment('Тип документа ВУ');

            $table->string('military_document_number')
                ->default(null)
                ->nullable()
                ->comment('Номер документа документа ВУ');

            $table->string('fitness_for_military_service')
                ->default(null)
                ->nullable()
                ->comment('Категория годности к ВС');

            $table->string('military_specialty')
                ->default(null)
                ->nullable()
                ->comment('Специальность');

            $table->string('military_rank')
                ->default(null)
                ->nullable()
                ->comment('Воинское звание');

            $table->string('military_composition')
                ->default(null)
                ->nullable()
                ->comment('Группа учета');

            $table->string('military_status')
                ->default(null)
                ->nullable()
                ->comment('Статус ВУ');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn([
                'military_document_type',
                'military_document_number',
                'fitness_for_military_service',
                'military_specialty',
                'military_composition',
                'military_status',
                'military_rank'
            ]);
        });
    }
}
