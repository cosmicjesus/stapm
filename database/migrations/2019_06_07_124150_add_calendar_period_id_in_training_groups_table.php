<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalendarPeriodIdInTrainingGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('training_calendar_item_id')->nullable()->comment('ID элемента календаря');
            $table->foreign('training_calendar_item_id')
                ->on('training_calendars_items')
                ->references('id')
                ->onDelete('SET NULL')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_groups', function (Blueprint $table) {
            //
        });
    }
}
