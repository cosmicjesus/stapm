<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewEnrolleeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->json('passport_data')->nullable()->comment('Паспорт');
            $table->string('snils')->nullable()->comment('СНИЛС');
            $table->string('graduation_organization_place')->after('graduation_organization_name')->nullable()->comment('Населеный пункт уч.заведения');
            $table->string('language')->nullable()->comment('Изучаемый язык');
            $table->string('privileged_category')->nullable()->comment('Льготная категория');
            $table->string('nationality')->nullable()->comment('Гражданство');
            $table->string('health_category')->nullable()->comment('Категория здоровья');
            $table->string('disability')->nullable()->comment('Инвалидность');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
