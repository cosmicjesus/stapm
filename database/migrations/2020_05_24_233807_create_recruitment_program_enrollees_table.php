<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecruitmentProgramEnrolleesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruitment_program_enrollees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recruitment_program_id');
            $table->unsignedBigInteger('enrollee_id');
            $table->boolean('is_priority')->default(false);
            $table->unsignedInteger('sum_of_grades_of_specialized_disciplines')->default(0);
            $table->timestamps();
        });

        Schema::table('recruitment_program_enrollees', function (Blueprint $table) {
            $table->foreign('enrollee_id')
                ->on('students')
                ->references('id')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruitment_program_enrollees');
    }
}
