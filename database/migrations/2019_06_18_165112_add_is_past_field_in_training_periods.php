<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPastFieldInTrainingPeriods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_group_academic_years', function (Blueprint $table) {
            $table->boolean('is_past')->after('active')->default(false)->comment('Флаг того, пройден ли период');
        });

        Schema::table('training_group_academic_year_terms', function (Blueprint $table) {
            $table->boolean('is_past')->after('active')->default(false)->comment('Флаг того, пройден ли период');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_periods', function (Blueprint $table) {
            $table->dropColumn('is_past');
        });

        Schema::table('training_group_academic_year_terms', function (Blueprint $table) {
            $table->dropColumn('is_past');
        });
    }
}
