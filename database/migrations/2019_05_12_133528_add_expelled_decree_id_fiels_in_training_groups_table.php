<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpelledDecreeIdFielsInTrainingGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('expelled_decree_id')->nullable();
            $table->foreign('expelled_decree_id')
                ->references('id')
                ->on('decrees')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_groups', function (Blueprint $table) {
            $table->dropColumn('expelled_decree_id');
        });
    }
}
