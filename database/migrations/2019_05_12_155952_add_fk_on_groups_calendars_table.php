<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkOnGroupsCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups_calendars', function (Blueprint $table) {
            $table->foreign('training_group_id')
                ->on('training_groups')
                ->references('id')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

            $table->foreign('training_calendar_id')
                ->on('training_calendars')
                ->references('id')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

        });

        Schema::table('training_calendars_items', function (Blueprint $table) {
            $table->foreign('training_calendar_id')
                ->on('training_calendars')
                ->references('id')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
