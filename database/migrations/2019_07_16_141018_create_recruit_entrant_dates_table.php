<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitEntrantDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruit_entrant_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recruitment_program_id')->comment('ID программы приема');
            $table->date('date')->nullable()->comment('Дата приема');
            $table->integer('count')->nullable()->comment('Количество');
            $table->timestamps();
        });
        Schema::table('recruit_entrant_dates', function (Blueprint $table) {
            $table->foreign('recruitment_program_id')
                ->references('id')
                ->on('priem_programs')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruit_entrant_dates');
    }
}
