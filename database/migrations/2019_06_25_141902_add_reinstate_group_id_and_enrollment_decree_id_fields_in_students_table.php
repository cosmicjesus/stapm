<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReinstateGroupIdAndEnrollmentDecreeIdFieldsInStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->unsignedBigInteger('reinstate_group_id')->nullable()->comment('ID группу в которую будет восстановлен студент');
            $table->unsignedBigInteger('enrollment_decree_id')->nullable()->comment('ID приказа о зачислении');

            $table->foreign('reinstate_group_id')
                ->on('training_groups')
                ->references('id')
                ->onDelete('SET NULL')
                ->onUpdate('cascade');

            $table->foreign('enrollment_decree_id')
                ->on('student_decrees')
                ->references('id')
                ->onDelete('SET NULL')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
