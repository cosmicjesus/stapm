<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPartnersFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_partner_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('social_partner_id');
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });

        Schema::table('social_partner_files', function (Blueprint $table) {
            $table->foreign('social_partner_id')
                ->references('id')
                ->on('social_partners')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_partner_files');
    }
}
