<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainigGroupAcademicYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_group_academic_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('training_group_id')->nullable()->comment('Айдишник группы');
            $table->unsignedBigInteger('training_calendar_id')->nullable()->comment('Айдишник учебного календаря');
            $table->unsignedBigInteger('education_plan_period_id')->nullable()->comment('Айдишник периода учебного плана');
            $table->integer('number')->default(1)->comment('Номер');
            $table->integer('year')->default(2019)->comment('Академический год');
            $table->boolean('active')->default(true)->comment('Флаг активности');
            $table->timestamps();
        });

        Schema::table('training_group_academic_years', function (Blueprint $table) {

            $table->foreign('training_group_id')
                ->on('training_groups')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('training_calendar_id')
                ->on('training_calendars')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_group_academic_years');
    }
}
