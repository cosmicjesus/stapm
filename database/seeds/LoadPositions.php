<?php

use Illuminate\Database\Seeder;

class LoadPositions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PositionTypesSeed::class);
        $this->call(PositionsSeed::class);
        $this->call(EducationSeed::class);
        $this->call(EmployeesSeed::class);
    }
}
