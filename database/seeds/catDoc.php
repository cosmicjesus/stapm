<?php

use Illuminate\Database\Seeder;
use App\Models\Document;
class catDoc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $docs = Document::query()->orderBy('id')->get();


        $ref = [];
        foreach ($docs as $doc) {
            $ref[$doc->name]['id'][] = $doc->id;
            $ref[$doc->name]['ids'][] = (string)$doc->category_id;
        }

        foreach ($ref as $item) {
            $docs = Document::query()->findMany($item['id']);
            foreach ($docs as $doc) {
                $doc->category_ids = $item['ids'];
                $doc->save();
            }
        }
    }
}
