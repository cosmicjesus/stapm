<?php

use Illuminate\Database\Seeder;

class FixParentPlaceOfWork extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parents = \App\Models\UserParent::query()->orderBy('lastname')->whereNotNull('place_of_work')->get();

        foreach ($parents as $parent) {
            $placeOfWork = str_replace(["\r\n", "\r", "\n"], ' ', strip_tags($parent->place_of_work));
            $parent->update([
                'place_of_work' => $placeOfWork
            ]);
        }
    }
}
