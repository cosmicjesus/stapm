<?php

use Illuminate\Database\Seeder;

class ImportOtherDocuments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $calendar=DB::connection('asu_stapm')->table('calendar')->select('*')->get();
        $akts=DB::connection('asu_stapm')->table('akts')->select('*')->get();
        $ppkrs=DB::connection('asu_stapm')->table('ppkrs')->select('*')->get();

        foreach ($calendar as $item) {
            $item=(array)$item;
            $item['type'] = 'calendar';
            $item['title'] = 'Календарный график';
            dispatch(new \App\Jobs\ImportOtherDocument($item))->onConnection('database')->onQueue('otherDocuments');
        }
        foreach ($akts as $item) {
            $item=(array)$item;
            $item['type'] = 'akt';
            $item['title'] = 'Акт согласования';
            dispatch(new \App\Jobs\ImportOtherDocument($item))->onConnection('database')->onQueue('otherDocuments');
        }
        foreach ($ppkrs as $item) {
            $item=(array)$item;
            $item['type'] = 'ppssz';
            $item['title'] = 'ППССЗ/ППКРС';
            dispatch(new \App\Jobs\ImportOtherDocument($item))->onConnection('database')->onQueue('otherDocuments');
        }
    }
}
