<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TestSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        for ($i = 0; $i < 50; $i++) {
//            dispatch((new \App\Jobs\TestJob())->delay(Carbon::now()->addMinutes(1)));
//        }

        $documents=\App\Models\Document::query()->get();

        foreach ($documents as $document) {
            $document->update(['name'=>trim($document->name)]);
        }
    }
}
