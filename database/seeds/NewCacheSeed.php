<?php

use App\Models\Student;
use Illuminate\Database\Seeder;

class NewCacheSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $students = Student::query()
//            ->whereNotNull('graduation_organization_name')
//            ->whereNotNull('graduation_organization_place')
//            ->get();
//
//        $namesOrg = $students->map(function ($student) {
//            return trim($student->graduation_organization_name);
//        })->unique()->sort()->values()->toArray();
//
//        $placesOrg = $students->map(function ($student) {
//            return trim($student->graduation_organization_place);
//        })->unique()->sort()->values()->toArray();
//
//        Cache::forever('educationalOrganizations', $namesOrg);
//        Cache::forever('locationsOfEducationalInstitutions', $placesOrg);
//
//        \Cache::forget('subdivision_codes');
//        $st = Student::query()->select('passport_data')
//            ->whereNotNull('passport_data')
//            ->get()
//            ->map(function ($item) {
//                return $item->passport_data;
//            })
//            ->filter(function ($data, $key) {
//                return !is_null($data['subdivisionCode']);
//            })
//            ->map(function ($item) {
//                return ['code' => $item['subdivisionCode'], 'issued' => $item['issued']];
//            })
//            ->unique(function ($item) {
//                return $item['code'] . $item['issued'];
//            })
//            ->sortBy('code')->values()->toArray();
//        \Cache::forever('subdivision_codes', $st);
//
//        $st1 = Student::query()
//            ->whereNull('passport_data')
//            ->get();
//
//        foreach ($st1 as $item) {
//            $item->passport_data = [
//                'issued' => null,
//                'number' => null,
//                'series' => null,
//                'birthPlace' => null,
//                'documentType' => 21,
//                'issuanceDate' => null,
//                'subdivisionCode' => null,
//            ];
//
//            $item->save();
//        }

        $st2 = Student::query()
            ->whereNull('addresses')
            ->get();

        foreach ($st2 as $item) {
            $item->addresses = [
                'residential' =>
                    [
                        'area' => null,
                        'city' => null,
                        'index' => null,
                        'region' => null,
                        'street' => null,
                        'housing' => null,
                        'settlement' => null,
                        'house_number' => null,
                        'apartment_number' => null,
                    ],
                'registration' =>
                    [
                        'area' => null,
                        'city' => null,
                        'index' => null,
                        'region' => null,
                        'street' => null,
                        'housing' => null,
                        'settlement' => null,
                        'house_number' => null,
                        'apartment_number' => null,
                    ],
                'place_of_stay' =>
                    [
                        'area' => null,
                        'city' => null,
                        'index' => null,
                        'region' => null,
                        'street' => null,
                        $st3 = Student::query()
//            ->whereNull('ratings_map')
//            ->get();
//
//        foreach ($st3 as $item) {
//            $item->ratings_map = [
//                'threes' => 0,
//                'fours' => 0,
//                'fives' => 0
//            ];
//
//            $item->save();
//        }                        'housing' => null,
                        'settlement' => null,
                        'house_number' => null,
                        'apartment_number' => null,
                    ],
                'isAddressSimilar' => true,
                'isAddressPlaceOfStay' => true,
            ];

            $item->save();
        }

//
    }
}
