<?php

use App\Imports\NominalNumberImport;
use App\Models\Student;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Excel;

class SetNominalNumberSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path('app/numbers.csv');
        if (!file_exists($path)) {
            echo 'Нет файла для импорта' . PHP_EOL;
            return false;
        }

        $collection = (new NominalNumberImport())->toArray($path, null, Excel::CSV);
        $first = array_shift($collection);
        $total = 0;
        foreach ($first as $item) {
            $name = $this->formatName($item[0]);

            $explodeName = explode(' ', $name);
            $number = round($item[1], 0);
            echo "Пишем студента {$explodeName[0]} {$explodeName[1]} {$explodeName[2]} с номером {$number}" . PHP_EOL;
            $student = Student::query()->onlyTeach()->where([
                'lastname' => trim($explodeName[0]),
                'firstname' => trim($explodeName[1]),
                'middlename' => trim($explodeName[2])
            ])->first();

            if ($student) {
                $student->update([
                    'nominal_number' => $number
                ]);
                $total++;
            }
            echo "Записали " . PHP_EOL;
        }
        echo "Всего загружено {$total}". PHP_EOL;
        \App\Models\Activity::query()->where(['causer_id' => null, 'causer_type' => null])->update(['causer_id' => 3, 'causer_type' => 'App\User']);
    }

    protected function formatName($name)
    {
        $formatName = str_replace('.', '', $name);
        $formatName = str_replace('1', '', $formatName);

        return trim($formatName);
    }
}
