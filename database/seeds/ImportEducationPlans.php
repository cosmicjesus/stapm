<?php

use App\Models\EducationPlan;
use Illuminate\Database\Seeder;

class ImportEducationPlans extends Seeder
{
    use \Illuminate\Foundation\Bus\DispatchesJobs;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $educationPlans=DB::connection('asu_stapm')
                            ->table('educationPlans')
                            ->select('*')
                            ->get()
                            ->toArray();
        EducationPlan::query()->truncate();
        foreach ($educationPlans as $plan){
            $model=new EducationPlan();
            $model->profession_program_id=$plan->program;
            $model->name=$plan->title;
            $model->count_period=$plan->countPeriod;
            $model->start_training=$plan->startTraining;
            $model->end_training=$plan->endTraining;
            $model->file=$plan->file;
            $model->active=1;
            $model->in_revision=0;
            $model->save();
            if(!is_null($plan->file)){
                dispatch(new \App\Jobs\ImportEducationPlan($model))->onConnection('database')->onQueue('education_plans');
            }

        }
    }
}
