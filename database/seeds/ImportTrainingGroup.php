<?php

use Illuminate\Database\Seeder;

class ImportTrainingGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        include 'dumps/groupsDump.php';

        foreach ($groups as $group) {
            $model = new \App\Models\TrainingGroup();

            $employee = \App\Models\Employee::query()->where('lastname', $group['curator']['lastName'])
                ->where('firstname', $group['curator']['firstName'])
                ->where('middlename', $group['curator']['middleName'])
                ->first();
            $model->id = $group['id'];
            $model->profession_program_id = $group['program'];
            $model->education_plan_id = $group['plan'];
            $model->employee_id = $employee->id;
            $model->unique_code = $group['code'];
            $model->pattern = $group['pattern'];
            $model->max_people = 25;
            $model->course = $group['educationPeriod']['yearNumber'];
            if ($group['educationPeriod']['termType'] == 'Course') {
                $model->period = $group['educationPeriod']['yearNumber'];
                $model->term_type = $group['educationPeriod']['termType'];
            } else {
                $model->period = $group['educationPeriod']['yearNumber'] * 2;
                $model->term_type = $group['educationPeriod']['termType'];
            }
            $model->status = 'teach';
            $model->save();
        }
    }
}
