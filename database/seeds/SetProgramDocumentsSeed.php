<?php

use Illuminate\Database\Seeder;

class SetProgramDocumentsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = \App\Models\ProgramSubject::all();

//        for ($i = 0; $i < 9999999999999999999; $i++) {
//            echo $i . PHP_EOL;
//            \App\Models\Profession::create([
//                'code' => 1,
//                'name' => '3323'
//            ]);
//        }

        foreach ($subjects as $subject) {
            $files = [
                [
                    'name' => 'Рабочая программа [исходник]',
                    'file' => null,
                    'active' => false
                ],
                [
                    'name' => 'КОС',
                    'file' => null,
                    'active' => false
                ],
                [
                    'name' => 'Методические указания для ЛПЗ',
                    'file' => null,
                    'active' => false
                ],
                [
                    'name' => 'Методические указания для практических работ',
                    'file' => null,
                    'active' => false
                ]
            ];

            foreach ($files as $file) {
                echo $subject->professionProgram->full_name . " " . $subject->subject->name . PHP_EOL;
                (new \App\AppLogic\Services\ProgramDocumentService)->createDocumentRecord(array_merge(
                    $file,
                    ['subject_id' => $subject->id, 'profession_program_id' => $subject->profession_program_id]));
                echo "|------------------------------------------------------------------------------------------|".PHP_EOL;
            }
        }
        \App\Models\Activity::query()->where(['causer_id' => null, 'causer_type' => null])->update(['causer_id' => 3, 'causer_type' => 'App\User']);
    }
}
