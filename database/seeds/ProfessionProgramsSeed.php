<?php

use App\Models\ProfessionProgram;
use App\Models\ProgramBasicDocument;
use Illuminate\Database\Seeder;

class ProfessionProgramsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professionPrograms=DB::connection('asu_stapm')->table('professionPrograms')->select('*')->get();

        DB::statement("SET foreign_key_checks=0");
        ProfessionProgram::query()->truncate();
        ProgramBasicDocument::query()->truncate();
        DB::statement("SET foreign_key_checks=1");
        foreach ($professionPrograms as $program){
            $model=new ProfessionProgram();
            $model->id=$program->id;
            $model->profession_id=$program->profession;
            $model->education_form_id=$program->form;
            $model->type_id=$program->programType;
            $model->trainin_period=$program->periodTime;
            $model->on_site=$program->onSite;
            $model->sort=100;
            $model->licence=$program->licence;
            $model->save();

            $model->documents()->save(new ProgramBasicDocument([
                'profession_program_id'=>$program->id
            ]));
        }
    }
}
