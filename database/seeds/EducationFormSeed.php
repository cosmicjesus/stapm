<?php

use App\Models\EducationForm;
use Illuminate\Database\Seeder;

class EducationFormSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EducationForm::create([
           'name'=>'Очная'
        ]);
        EducationForm::create([
            'name'=>'Очно-заочная'
        ]);
        EducationForm::create([
            'name'=>'Заочная'
        ]);
    }
}
