<?php

use Illuminate\Database\Seeder;

class RecruitCentresSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\RecruitmentCenter::truncate();
        $centers = [
            'Самарской области по Октябрьскому, Куйбышевскому, Ленинскому и Самарскому районам',
            'Самарской области по Кировскому и Красноглинскому районам',
            'Самарской области по Советскому и Железнодорожному районам',
            'Самарской области по Промышленному району',
            'Самарской области по Автозаводскому, Комсомольскому и Центральному районам г. Тольятти',
            'Самарской области по Ставропольскому району',
            'Самарской области по г. Жигулевску',
            'Самарской области по г. Новокуйбышевску',
            'Самарской области по городам Сызрань, Октябрьск, Сызранскому и Шигонскому районам',
            'Самарской области по г. Чапаевску',
            'Самарской области по Нефтегорскому и Алексеевскому районам',
            'Самарской области по Безенчукскому, Приволжскому и Хворостянскому районам',
            'Самарской области по Богатовскому и Борскому районам',
            'Самарской области по Большеглушицкому и Большечерниговскому районам',
            'Самарской области по Волжскому району',
            'Самарской области по Елховскому и Кошкинскому районам',
            'Самарской области по г. Похвистнево, Похвистневскому, Клявлинскому и Камышлинскому районам',
            'Самарской области по г. Кинелю',
            'Самарской области по Красноярскому району',
            'Самарской области по Красноармейскому и Пестравскому районам',
            'Самарской области по Отрадненскому и Кинель-Черкасскому районам',
            'Самарской области по Сергиевскому и Исаклинскому районам',
            'Самарской области по Челно-Вершинскому и Шенталинскому районам',
        ];

        foreach ($centers as $center) {
            \App\Models\RecruitmentCenter::query()->create([
                'name' => $center
            ]);
        }

        \App\Models\Activity::query()->where(['causer_id' => null, 'causer_type' => null])->update(['causer_id' => 3, 'causer_type' => 'App\User']);
    }
}
