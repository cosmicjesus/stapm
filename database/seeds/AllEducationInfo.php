<?php

use Illuminate\Database\Seeder;

class AllEducationInfo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SubjectTypes::class);
        $this->call(Subjects::class);
        $this->call(ProfessionsSeed::class);
        $this->call(ProfessionProgramsSeed::class);
        $this->call(ImportBasicDocuments::class);
        $this->call(ImportEducationPlans::class);
        $this->call(ImportProgramSubject::class);
        $this->call(ImportOtherDocuments::class);
    }
}
