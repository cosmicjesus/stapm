<?php

use Illuminate\Database\Seeder;

use App\Models\Employee;
use App\Helpers\AsuRsoImportMapping;

class EmployeesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        include 'dumps/empl.php';

        DB::statement("SET foreign_key_checks=0");
        \App\Models\EmployeePosition::query()->truncate();
        \App\Models\EmployeePosition::query()->truncate();
        Employee::query()->truncate();
        DB::statement("SET foreign_key_checks=1");

        foreach ($empl as $employee){
            $start_date=explode('T',$employee['decrees'][0]['date']);
            $model=new Employee();
            $model->lastname=$employee['lastName'];
            $model->firstname=$employee['firstName'];
            $model->middlename=$employee['middleName'];
            $model->gender=AsuRsoImportMapping::gender($employee['gender']);
            $birthday=explode('T',$employee['birthday']);
            $model->birthday=$birthday[0];
            $model->category=isset($employee['qualificationCategory'])?AsuRsoImportMapping::category($employee['qualificationCategory']):0;
            $model->status='work';
            $model->start_work_date=$start_date[0];
            $model->save();

            $model->educations()->attach(AsuRsoImportMapping::education($employee['educationLevel']));

            $positions=[];
            foreach ($employee['decrees'] as $decree){
                if($decree['type']!='Dismiss'){
                    $positions[$decree['position']['id']]['type']=0;
                    $positions[$decree['position']['id']]['sort']=1;
                    $positions[$decree['position']['id']]['experience']=0;
                    $positions[$decree['position']['id']]['start_date']=(explode('T',$decree['date']))[0];
                }
//                else{
//                    $positions[$decree['position']['id']]['date_layoff']=(explode('T',$decree['date']))[0];
//                }
            }
            $model->positions()->attach($positions);
        }
    }
}
