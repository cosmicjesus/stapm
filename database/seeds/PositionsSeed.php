<?php

use App\Models\Position;
use Illuminate\Database\Seeder;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class PositionsSeed extends Seeder
{
    protected $log;
    protected $logPath = '/logs/import-of-asu/positions.log';

    public function __construct()
    {
        $this->log = new Logger('FtpSyncLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
    }

    public function run()
    {
        include "dumps/positions.php";
        DB::statement("SET foreign_key_checks=0");
        $this->log->addInfo( \Carbon\Carbon::now()->format('H:i:s d.m.Y') .". Очищаем таблицу positions");
        Position::query()->truncate();
        $this->log->addInfo(\Carbon\Carbon::now()->format('H:i:s d.m.Y') .". Таблица очищена. Начинаем запись");
        DB::statement("SET foreign_key_checks=1");

        foreach ($positions as $position){
            $this->log->addInfo(\Carbon\Carbon::now()->format('H:i:s d.m.Y') .". Пишем должность ".$position['name']);
            $model=new Position();
            $model->id=$position['id'];
            $model->name=$position['name'];
            $model->position_type_id=\App\Helpers\AsuRsoImportMapping::positionType($position['type']);
            $model->save();
            $this->log->addInfo(\Carbon\Carbon::now()->format('H:i:s d.m.Y') .". Записана должность ".$position['name']);
        }
        $this->log->addInfo(\Carbon\Carbon::now()->format('H:i:s d.m.Y') .". Импорт должностей завершен");
    }
}
