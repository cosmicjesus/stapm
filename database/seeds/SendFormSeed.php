<?php

use Illuminate\Database\Seeder;

class SendFormSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entrants=\App\Models\Entrant::query()->whereNotNull('email')->get();

        $now=\Carbon\Carbon::now();
        foreach ($entrants as $entrant) {
            $now->addSeconds(30);
            if($entrant->email){
                dispatch(new \App\Jobs\SendUploadingFormJob($entrant))->delay($now);
            }
        }
    }
}
