<?php

use Illuminate\Database\Seeder;

class ImportBasicDocuments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $programDocuments=DB::connection('asu_stapm')->table('programDocuments')->select('*')->get();

        foreach ($programDocuments as $document){
            $arr=[
                'program'=>$document->program,
                'standart'=>$document->standart,
                'annotation'=>$document->annotation
            ];
            dispatch(new \App\Jobs\ImportBasicDocument($arr))->onConnection('database')->onQueue('basic_documents');
        }
    }
}
