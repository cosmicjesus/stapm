<?php

use Illuminate\Database\Seeder;

class ProfessionProgramTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ProfessionProgramType::create([
            'name'=>'Программа подготовки специалистов среднего звена',
            'abbreviation'=>'ППССЗ'
        ]);

        \App\ProfessionProgramType::create([
            'name'=>'Программа подготовки квалифицированных рабочих и служащих',
            'abbreviation'=>'ППКРС'
        ]);
    }
}
