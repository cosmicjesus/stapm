<?php

use App\Models\Student as StudentAlias;
use Illuminate\Database\Seeder;

class SetEnrollmentDecree extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = StudentAlias::query()->onlyTeach()->with('decrees')->get();

        foreach ($students as $student) {
            $enrollmentDecree = $student->decrees->last();
            $student->update(['enrollment_decree_id' => $enrollmentDecree->pivot->id]);
        }
    }
}
