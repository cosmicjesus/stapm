<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class Permissions7May extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::query()
            ->firstOrCreate([
                'name' => 'enrollees.view',
                'ru_name' => 'Просмотр абитуриентов',
                'group_name' => 'Работа с абитуриентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'enrollees.create',
                'ru_name' => 'Добавление абитуриентов',
                'group_name' => 'Работа с абитуриентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'enrollees.actions',
                'ru_name' => 'Действия с абитуриентами',
                'group_name' => 'Работа с абитуриентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'students.view',
                'ru_name' => 'Просмотр студентов',
                'group_name' => 'Работа со студентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'students.create',
                'ru_name' => 'Добавление студентов',
                'group_name' => 'Работа со студентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'students.actions',
                'ru_name' => 'Действия со студентами',
                'group_name' => 'Работа со студентами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'tocka.view',
                'ru_name' => 'Просмотр событий',
                'group_name' => 'Точка роста'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'tocka.actions',
                'ru_name' => 'Действия с событиями',
                'group_name' => 'Точка роста'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'tocka.actions',
                'ru_name' => 'Действия с событиями',
                'group_name' => 'Точка роста'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'program.actions',
                'ru_name' => 'Действия с ОПОП',
                'group_name' => 'Работа с образовательными программами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'program.create',
                'ru_name' => 'Создание ОПОП',
                'group_name' => 'Работа с образовательными программами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'eduplans.actions',
                'ru_name' => 'Действия с учебными планами',
                'group_name' => 'Работа с учебными планами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'eduplans.create',
                'ru_name' => 'Создание учебных планов',
                'group_name' => 'Работа с учебными планами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'documents.view',
                'ru_name' => 'Просмотр документов и категорий',
                'group_name' => 'Работа с документами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'documents.actions',
                'ru_name' => 'Действия с документами и категориями',
                'group_name' => 'Работа с документами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'news.view',
                'ru_name' => 'Просмотр новостей',
                'group_name' => 'Работа с новостями'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'news.actions',
                'ru_name' => 'Действия с новостями',
                'group_name' => 'Работа с новостями'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'site.administrations',
                'ru_name' => 'Работа со страницей администрации',
                'group_name' => 'Взаимодействие с сайтом'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'employees.create',
                'ru_name' => 'Добавление сотрудников',
                'group_name' => 'Работа с сотрудниками'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'employees.actions',
                'ru_name' => 'Действия с сотрудниками',
                'group_name' => 'Работа с сотрудниками'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'subject.view',
                'ru_name' => 'Просмотр дисциплин',
                'group_name' => 'Работа с дисциплинами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'subject.actions',
                'ru_name' => 'Действия с дисциплинами',
                'group_name' => 'Работа с дисциплинами'
            ]);

        Permission::query()
            ->firstOrCreate([
                'name' => 'references.view',
                'ru_name' => 'Действия со справками',
                'group_name' => 'Разное'
            ]);
        Permission::query()
            ->firstOrCreate([
                'name' => 'education.load',
                'ru_name' => 'Работа с нагрузкой',
                'group_name' => 'Образование'
            ]);
    }
}
