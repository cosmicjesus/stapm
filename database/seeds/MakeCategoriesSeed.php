<?php

use Illuminate\Database\Seeder;

class MakeCategoriesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [

            '11' => 11,
            '12' => 12,
            '12a' => '12А',
            13 => 13,
            14 => 14,
            16 => 16,
            17 => 17,
            'tm1' => 'ТМ-1',
            'tm1a' => 'ТМ-1А',
            'te1' => 'ТЭ-1',
            'ssa1' => 'ССА-1',
            'is1' => 'ИС-1',
            'bas1' => 'БАС-1',
            'asu1' => 'АСУ-1',

            21 => 21,
            22 => 22,
            24 => 24,
            26 => 26,
            'tm2' => 'ТМ-2',
            'tm2a' => 'ТМ-2А',
            'te2' => 'ТЭ-2',
            'ssa2' => 'ССА-2',
            'bas2' => 'БАС-2',
            'asu2' => 'АСУ-2',

            'tm3' => 'ТМ-3',
            'tm3a' => 'ТМ-3А',
            'te3' => 'ТЭ-3',
            'asu3' => 'АСУ-3',
            'ks3' => 'КС-3',
            'sp3' => 'СП-3'

        ];

        foreach ($groups as $key => $group) {
            \App\Models\CategoriesDocument::query()->create([
                'name' => "Расписание группы {$group}",
                'sort' => 500,
                'parent_id' => null,
                'active' => false,
                'slug' => "time_table_{$key}"
            ]);
        }
    }
}
