<?php

use Illuminate\Database\Seeder;

class CreateNewPermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //\Spatie\Permission\Models\Permission::query()->where('name', '!=', 'super.admin')->delete();

        $permissionsGropus = [
            'god' =>
                [
                    'name' => 'Уровень бога',
                    'permissions' =>
                        [
                            'super.admin' => 'Бог',
                        ],
                ],
            'students' =>
                [
                    'name' => 'Работа со студентами',
                    'permissions' =>
                        [
                            'student.view' => 'Просмотр списка и дел студентов',
                            'student.edit' => 'Редактирование дела студента',
                            'student.create' => 'Добавление студентов',
                            'student.actions' => 'Действия со студентами',
                            'student.military' => 'Редактирование данных воинского учета',
                            'student.make-reference' => 'Генерация справок для студентов',
                            'student.decrees' => 'Загрузка приказов'
                        ],
                ],
            'employees' =>
                [
                    'name' => 'Работа с сотрудниками',
                    'permissions' =>
                        [
                            'employees.view' => 'Просмотр списка и дел сотруников',
                            'employees.edit' => 'Редактирование дела сотрудника',
                        ],
                ],
            'enrollees' =>
                [
                    'name' => 'Работа с абитуриентами',
                    'permissions' =>
                        [
                            'enrollees.view' => 'Просмотр списка и дел абитуриентов',
                            'enrollees.create' => 'Добавление абитуриентов',
                            'enrollees.actions' => 'Действия с абитуриентами',
                            'enrollees.edit' => 'Редактирование дел абитуриентов',
                        ],
                ],
            "education" => [
                "name" => "Образование",
                "permissions" => [
                    'opop.view' => 'Просмотр ОПОП',
                    'opop.actions' => 'Действия с ОПОП',
                    'eduplans.view' => 'Просмотр учебных планов',
                    'eduplans.actions' => 'Действия с учебными планами',
                    'profession.view' => 'Просмотр профессий',
                    'profession.actions' => 'Действия с профессиями',
                    'subjects.view' => 'Просмотр дисциплин',
                    'subjects.actions' => 'Действия с дисциплинами',
                    'training_calendars.view' => 'Просмотр учебных календарей',
                    'training_calendars.actions' => 'Действия с учебными календарями'
                ]
            ],
            'site' => [
                'name' => 'Работа с сайтом',
                'permissions' => [
                    "news.actions" => "Работа с новостями",
                    "documents.actions" => "Работа с документами",
                ]
            ]
        ];

        foreach ($permissionsGropus as $key => $gropu) {
            foreach ($gropu['permissions'] as $permKey => $permission) {
                \Spatie\Permission\Models\Permission::query()->updateOrCreate(
                    ['name' => $permKey, 'guard_name' => 'web'],
                    ['ru_name' => $permission, 'group_name' => $key]
                );
            }
        }

    }
}
