<?php

use App\Models\CategoriesDocument;
use Illuminate\Database\Seeder;

class MaterialCategoriesDocuments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoriesDocument::create([
            'id'=>11,
            'name'=>'Документы на странице материального обеспечения',
            'sort'=>500,
            'parent_id'=>null,
            'active'=>false,
            'slug'=>'doc_material_page'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на странице структура управления',
            'sort'=>500,
            'parent_id'=>null,
            'active'=>false,
            'slug'=>'structure_files'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на 1 корпус',
            'sort'=>1,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_first_housing'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на 2 корпус',
            'sort'=>2,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_second_housing'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на мастерские 1 корпуса',
            'sort'=>2,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_first_housing_labs'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на мастерские 2 корпуса',
            'sort'=>2,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_second_housing_labs'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на общежитие',
            'sort'=>3,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_hostel'
        ]);

        CategoriesDocument::create([
            'name'=>'Документы на Универсальный общественно-бытовой корпус',
            'sort'=>4,
            'parent_id'=>11,
            'active'=>false,
            'slug'=>'doc_universal_public_housing'
        ]);
    }
}
