<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class SetUuidInStudentFiles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $files = \App\Models\StudentFile::query()->get();
        foreach ($files as $file) {
            $file->uuid = (string)Uuid::generate(4);
            $file->save();
        }
//        \App\Models\StudentFile::query()->update(['uuid'=>(string) Uuid::generate(4)]);
    }
}
