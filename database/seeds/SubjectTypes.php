<?php

use Illuminate\Database\Seeder;
use App\Models\SubjectType;

class SubjectTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        SubjectType::create([
            'name'=>'Дисциплина',
            'reduction'=>'Дисц'
        ]);
        SubjectType::create([
            'name'=>'Междисциплинарный курс',
            'reduction'=>'МДК'
        ]);
        SubjectType::create([
            'name'=>'Учебная практика',
            'reduction'=>'УП'
        ]);
        SubjectType::create([
            'name'=>'Производственная практика',
            'reduction'=>'ПП'
        ]);
        SubjectType::create([
            'name'=>'Профессиональный модуль',
            'reduction'=>'ПМ'
        ]);
    }
}
