<?php

use Illuminate\Database\Seeder;

class WriteDecreeTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $decrees = \App\Models\Decree::query()->with('students')->get();

        foreach ($decrees as $decree) {
            $type = $decree->students->groupBy('pivot.type')->keys()->first();
            $decree->update(['type' => $type]);
        }
    }
}
