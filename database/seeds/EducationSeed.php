<?php

use App\Models\Education;
use Illuminate\Database\Seeder;

class EducationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Education::create(['name'=>'Без основного общего образования','type'=>'low']);
        Education::create(['name'=>'Основное общее образование','type'=>'low']);
        Education::create(['name'=>'Среднее общее образование','type'=>'low']);
        Education::create(['name'=>'Неполное среднее профессиональное образование','type'=>'medium']);
        Education::create(['name'=>'СПО по программам подготовки квалифицированных рабочих (служащих)','type'=>'medium']);
        Education::create(['name'=>'СПО по программам подготовки специалистов среднего звена','type'=>'medium']);
        Education::create(['name'=>'Высшее образование - бакалавриат','type'=>'high']);
        Education::create(['name'=>'Высшее образование - незаконченное высшее образование','type'=>'high']);
        Education::create(['name'=>'Высшее образование - подготовка кадров высшей квалификации','type'=>'high']);
        Education::create(['name'=>'Высшее образование - специалитет, магистратура','type'=>'high']);
    }
}
