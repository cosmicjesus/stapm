<?php

use Illuminate\Database\Seeder;

class SetDefaultSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        $settilgsList = [
            ['showEnrolleesRatins' => false],
            ['licenceNum' => '63Л01 № 0001987'],
            ['licenceDate' => '14.12.2015'],
            ['accredNum' => '63A01 № 0000526'],
            ['accredDate' => '18.01.2016'],
            ['countPlaceOnMan' => 12],
            ['countPlaceOnWoman' => 22],
            ['orderHelpsFromSite' => true],
            ['showStaticCountEnrollees' => true]
        ];

        foreach ($settilgsList as $value) {
            setting($value)->save();
        }
    }
}
