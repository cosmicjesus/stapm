<?php

use Illuminate\Database\Seeder;

class SetRecruitEntrantDate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entrants = \App\Models\Entrant::query()->orderBy('start_date')->get();

        foreach ($entrants as $entrant) {
            $model = \App\Models\RecruitEntrantDate::query()
                ->where([
                    'date' => $entrant->start_date,
                    'recruitment_program_id' => $entrant->recruitment_program_id
                ])->first();

            if ($model) {
                $model->increment('count');
            } else {
                \App\Models\RecruitEntrantDate::query()->create([
                    'date' => $entrant->start_date,
                    'recruitment_program_id' => $entrant->recruitment_program_id,
                    'count' => 1
                ]);
            }
        }
    }
}
