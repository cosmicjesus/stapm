<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('APP_SEED')) {
            if (is_dir(storage_path() . "/app/public/employees")) {
                $this->removeDirectory(storage_path() . "/app/public/employees");
            }
            if (is_dir(storage_path() . "/app/public/news")) {
                $this->removeDirectory(storage_path() . "/app/public/news");
            }
            if (is_dir(storage_path() . "/app/public/programs")) {
                $this->removeDirectory(storage_path() . "/app/public/programs");
            }

            $this->call(Admin::class);
            $this->call(EducationFormSeed::class);
            $this->call(ProfessionProgramTypeSeed::class);
            $this->call(LoadPositions::class);
            $this->call(AllEducationInfo::class);
            $this->call(CategoriesDocumentsSeed::class);
            $this->call(ImportNews::class);
            $this->call(ImportTrainingGroup::class);
            $this->call(ImportStudents::class);
        }
    }

    private function removeDirectory($dir)
    {
        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? $this->removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }
}
