<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WriteDecreeTermSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $decrees = \App\Models\Decree::query()->get();

        foreach ($decrees as $decree) {
            $decree->number = trim(str_replace('№', '', $decree->number));
            $decree->term = $this->getDateKey($decree->date);
            $decree->save();
        }


    }

    protected function getDateKey($decreeDate)
    {
        $startYear = 2012;
        $maxYear = Carbon::now()->format('Y');

        $table = [];
        $date = $decreeDate;
        for ($i = $startYear; $i <= $maxYear; $i++) {
            $start = Carbon::create($i, 8, 1);
            $end = Carbon::create($i + 1, 7, 31);

            $table[$i] = [$start, $end];
        }
        $keyYear = null;

        foreach ($table as $key => $item) {
            if ($date >= $item[0] && $date <= $item[1]) {
                $keyYear = $key;
                break;
            }
        }

        return $keyYear;
    }
}
