<?php

use Illuminate\Database\Seeder;

class RewriteParents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parents = \App\Models\StudentParent::all();

        $relationShipTypes = [
            'mother',
            'father',
            'relative',
            'guardian',
            'adoptiveParent',
            'stepparent',
            'trustee'
        ];

        foreach ($parents as $parent) {
            $clparent = \App\Models\UserParent::query()->create([
                'lastname' => $parent->lastname,
                'firstname' => $parent->firstname,
                'middlename' => $parent->middlename,
                'birthday' => \Carbon\Carbon::now()->format('Y-m-d'),
                'gender' => $parent->gender,
                'relation_ship_type' => $relationShipTypes[$parent->relationshipType],
                'phone' => $parent->phone,
            ]);

            $clparent->childrens()->attach($parent->student_id);
        }
    }
}
