<?php

use Illuminate\Database\Seeder;

class SetPrivilegedCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Models\Student::query()->onlyTeach()->whereNotNull('privileged_category')->get();

        $arr = [
            1 => 'other',
            2 => 'invalid',
            3 => 'needy_family',
            4 => 'incomplete_family',
            5 => 'full_state_support',
            6 => 'without_parental_care',
        ];

        foreach ($students as $student) {
            $student->update(['preferential_categories' => [$arr[$student->privileged_category]]]);
        }

        \App\Models\Activity::query()->where(['causer_id' => null, 'causer_type' => null])->update(['causer_id' => 3, 'causer_type' => 'App\User']);
    }
}
