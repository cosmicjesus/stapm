<?php

use Illuminate\Database\Seeder;

class AddSlugForPriemPrograms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs = \App\Models\PriemProgram::query()->get();

        foreach ($programs as $program) {
            $program->update(['slug'=>null,'updated_at' => null]);
        }
    }
}
