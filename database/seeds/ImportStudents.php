<?php

use App\Jobs\ImportStudent;
use App\Models\Education;
use App\Models\TrainingGroup;
use Illuminate\Database\Seeder;

class ImportStudents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $csvData = file_get_contents(storage_path() . "/app/students.csv");
            $lines = explode(PHP_EOL, $csvData);
            $groups = TrainingGroup::query()->where('status', 'teach')
                ->get()
                ->keyBy('unique_code')
                ->toArray();

            $educations = Education::query()
                ->get()
                ->keyBy('name')
                ->toArray();
            DB::statement("SET foreign_key_checks=0");
            \App\Models\StudentDecree::query()->truncate();
            \App\Models\Student::query()->truncate();
            \App\Models\Decree::query()->truncate();
            DB::statement("SET foreign_key_checks=1");


            foreach ($lines as $key => $line) {
                if ($key > 0) {
                    $array = explode(';', $line);
                    dispatch(new ImportStudent($array, $educations, $groups))->onConnection('database')->onQueue('students');
                }
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }

    }
}
