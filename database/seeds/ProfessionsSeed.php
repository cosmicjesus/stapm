<?php

use App\Models\Profession;
use Illuminate\Database\Seeder;

class ProfessionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $professions=DB::connection('asu_stapm')->table('professions')->select('*')->get();
        DB::statement("SET foreign_key_checks=0");
        Profession::query()->truncate();
        DB::statement("SET foreign_key_checks=1");
        foreach ($professions as $profession) {

            Profession::create([
                'id'   => $profession->id,
                'code' => $profession->code,
                'name' => $profession->name
            ]);
        }

    }
}
