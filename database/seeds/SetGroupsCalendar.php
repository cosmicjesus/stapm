<?php

use App\Models\TrainingCalendar;
use App\Models\TrainingGroupCalendar;
use Illuminate\Database\Seeder;

class SetGroupsCalendar extends Seeder
{

    public function makePeriod($startYear, $termType)
    {
        $arr = [
            'Semester' => [
                [
                    'number' => 1,
                    'start_date' => \Carbon\Carbon::create($startYear, 9, 1)->toDateString(),
                    'end_date' => \Carbon\Carbon::create($startYear, 12, 28)->toDateString(),
                    'session_start_date' => null,
                    'session_end_date' => null
                ],
                [
                    'number' => 2,
                    'start_date' => \Carbon\Carbon::create($startYear + 1, 1, 14)->toDateString(),
                    'end_date' => \Carbon\Carbon::create($startYear + 1, 6, 30)->toDateString(),
                    'session_start_date' => null,
                    'session_end_date' => null
                ]
            ],
            'Course' => [
                [
                    'number' => 1,
                    'start_date' => \Carbon\Carbon::create($startYear, 9, 1)->toDateString(),
                    'end_date' => \Carbon\Carbon::create($startYear + 1, 6, 30)->toDateString(),
                    'session_start_date' => null,
                    'session_end_date' => null
                ]
            ]
        ];

        return $arr[$termType];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        try {
            $groups = \App\Models\TrainingGroup::query()->with('program')->onlyTeach()->get();

            $courseYear = [
                5 => 2014,
                4 => 2015,
                3 => 2016,
                2 => 2017,
                1 => 2018
            ];

            \App\Models\TrainingGroupAcademicYearTerm::query()->delete();
            \App\Models\TrainingGroupAcademicYear::query()->delete();
            \App\Models\TrainingCalendarsItem::query()->delete();
            TrainingCalendar::query()->delete();
            foreach ($groups as $group) {
                $programType = $group->program->type_id;


                $startYear = $courseYear[$group->course];

                $courseIterator = $programType == 1 ? 5 : 4;
                if ($group->course == 5) {
                    $courseIterator = 6;
                }
                for ($i = 1; $i < $courseIterator; $i++) {
                    $calendarModel = TrainingCalendar::create([
                        'name' => str_replace('{Курс}', $i, $group->pattern),
                        'year' => $startYear,
                        'term_type' => strtolower($group->term_type)
                    ]);
                    $calendarId = $calendarModel->id;
                    $calendarItemsIds = [];
                    foreach ($this->makePeriod($startYear, $group->term_type) as $item) {
                        $calendarItemModel = $calendarModel->periods()->create($item);
                        $calendarItemsIds[] = $calendarItemModel->id;
                    }

                    $groupAcademicYear = \App\Models\TrainingGroupAcademicYear::create([
                        'training_group_id' => $group->id,
                        'training_calendar_id' => $group->course < $i ? null : $calendarId,
                        'education_plan_period_id' => null,
                        'number' => $i,
                        'year' => $startYear,
                        'active' => $group->course == $i ? true : false,
                        'is_past' => $group->course == $i ? false : true,
                    ]);

                    foreach ($calendarItemsIds as $key => $calendarItemsId) {
                        $active = false;
                        $isPast = true;
                        if (($group->course == $i) && $group->term_type == 'Semester' && $key == 1) {
                            $active = true;
                            $isPast = false;
                        }

                        if (($group->course == $i) && $group->term_type == 'Course' && $key == 0) {
                            $active = true;
                            $isPast = false;
                        }

                        $groupAcademicYear->terms()->create([
                            'calendar_item_id' => $calendarItemsId,
                            'number' => $key + 1,
                            'active' => $active,
                            'is_past' => $isPast,
                        ]);
                    }
                    $startYear++;
                }

            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
