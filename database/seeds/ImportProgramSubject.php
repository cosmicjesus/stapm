<?php

use App\Models\ProgramSubject;
use Illuminate\Database\Seeder;

class ImportProgramSubject extends Seeder
{
    use \Illuminate\Foundation\Bus\DispatchesJobs;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement("SET foreign_key_checks=0");
        ProgramSubject::query()->truncate();
        \App\Models\ProgramDocument::query()->truncate();
        DB::statement("SET foreign_key_checks=1");

        $program_subjects=DB::connection('asu_stapm')->table('program-subjects')->select('*')->get();

        foreach ($program_subjects as $program_subject) {
            if (!is_null($program_subject->file)) {
                $model = new ProgramSubject();
                $model->profession_program_id = $program_subject->program;
                $model->subject_id = $program_subject->subject;
                $model->in_revision = false;
                $model->file = $program_subject->file;
                $model->save();
                dispatch(new \App\Jobs\ImportWorkingProgram($model))->onConnection('database')->onQueue('workingPrograms');
            }
        }

        $programProfessionModule=DB::connection('asu_stapm')->table('programProfessionModule')->select('*')->get();

        foreach ($programProfessionModule as $program_subject) {
            $subject = \App\Models\Subject::query()->where('name', $program_subject->title)->first();
            $model = new ProgramSubject();
            $model->profession_program_id = $program_subject->program;
            $model->subject_id = $subject->id;
            $model->in_revision = false;
            $model->file = $program_subject->file;
            $model->save();
            if (!is_null($program_subject->file)) {
                dispatch(new \App\Jobs\ImportWorkingProgram($model))->onConnection('database')->onQueue('workingPrograms');
            }
        }
    }
}
