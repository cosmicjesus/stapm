<?php

use App\Models\CategoriesDocument;
use Illuminate\Database\Seeder;

class CategoriesDocumentsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoriesDocument::query()->truncate();

        CategoriesDocument::create([
            'id'=>1,
            'name'=>'Документы федерального уровня',
            'parent_id'=>null,
            'sort'=>1
        ]);
        CategoriesDocument::create([
            'id'=>2,
            'name'=>'Документы регионального уровня',
            'parent_id'=>null,
            'sort'=>2
        ]);
        CategoriesDocument::create([
            'id'=>3,
            'name'=>'Документы уровня техникума',
            'parent_id'=>null,
            'sort'=>3
        ]);
        CategoriesDocument::create([
            'name'=>'Основные',
            'parent_id'=>3,
            'sort'=>1
        ]);
        CategoriesDocument::create([
            'name'=>'Локальные нормативные акты',
            'parent_id'=>3,
            'sort'=>2
        ]);
        CategoriesDocument::create([
            'name'=>'Финансово-хозяйственная деятельность',
            'parent_id'=>3,
            'sort'=>3
        ]);
        CategoriesDocument::create([
            'name'=>'Дуальное обуение',
            'parent_id'=>3,
            'sort'=>4
        ]);

        CategoriesDocument::create([
            'name'=>'Приемная комиссия',
            'parent_id'=>3,
            'sort'=>5
        ]);
    }
}
