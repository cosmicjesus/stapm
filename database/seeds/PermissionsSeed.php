<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement("SET foreign_key_checks=0");
        Permission::query()->truncate();
        DB::statement("SET foreign_key_checks=1");


        Permission::create([
            'name'=>'program.work',
            'ru_name'=>'Работа с ОПОП'
        ]);

        Permission::create([
            'name'=>'eduplans.work',
            'ru_name'=>'Работа с учебными планами'
        ]);

        Permission::create([
            'name'=>'employees.work',
            'ru_name'=>'Работа с сотрудниками'
        ]);

        Permission::create([
            'name'=>'students.work',
            'ru_name'=>'Работа со студентами'
        ]);

        Permission::create([
            'name'=>'priem.work',
            'ru_name'=>'Работа с приемной комиссией'
        ]);

        Permission::create([
            'name'=>'report.work',
            'ru_name'=>'Доступ к отчетам'
        ]);

        Permission::create([
            'name'=>'groups.work',
            'ru_name'=>'Работа с учебными группами'
        ]);

        Permission::create([
            'name'=>'enrollees.work',
            'ru_name'=>'Работа с абитуриентами'
        ]);

        Permission::create([
            'name'=>'super.admin',
            'ru_name'=>'Владыка'
        ]);

        Permission::create([
            'name'=>'user.work',
            'ru_name'=>'Просмотр пользователей'
        ]);

        Permission::create([
            'name'=>'user.create',
            'ru_name'=>'Добавление пользователей'
        ]);

        Permission::create([
            'name'=>'user.edit',
            'ru_name'=>'Редактирование пользователей'
        ]);

        Permission::create([
            'name'=>'user.delete',
            'ru_name'=>'Удаление пользователей'
        ]);

    }
}
