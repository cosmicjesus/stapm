<?php

use App\Models\Subject;
use Illuminate\Database\Seeder;

class Subjects extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects=DB::connection('asu_stapm')->table('subjects')->select('*')->get();
        $programProfessionModule=DB::connection('asu_stapm')->table('programProfessionModule')->select('*')->get();
        DB::statement("SET foreign_key_checks=0");
        Subject::query()->truncate();
        DB::statement("SET foreign_key_checks=1");

        foreach ($subjects as $subject) {
            Subject::create([
                'id'      =>$subject->id,
                'type_id' => $subject->type,
                'name' => $subject->name
            ]);
        }

        foreach ($programProfessionModule as $subject) {
            Subject::create([
                'type_id' => 5,
                'name' => $subject->title
            ]);
        }
    }
}