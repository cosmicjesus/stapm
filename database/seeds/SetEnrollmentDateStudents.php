<?php

use Illuminate\Database\Seeder;

class SetEnrollmentDateStudents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stundents = \App\Models\Student::query()
            ->with('decrees')
            ->whereIn('status', ['student', 'inArmy', 'academic'])
            ->whereNull('enrollment_date')
            ->get();

        foreach ($stundents as $stundent) {
            $decreeDate = $stundent->decrees()->get()->last()['date']->format('Y-m-d');
            $stundent->enrollment_date = $decreeDate;
            $stundent->save();
        }
    }
}
