<?php

use Illuminate\Database\Seeder;

class RebuildPassportCodesSeed extends Seeder
{

    protected $service;

    public function __construct(\App\AppLogic\Services\CacheService $cacheService)
    {
        $this->service = $cacheService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $studetns = \App\Models\Student::query()->whereNotNull('passport_data->documentType')->get();

        $newData = [];

        foreach ($studetns as $studetn) {
            $code = $studetn->passport_data['subdivisionCode'];
            if (!is_null($code)) {
                $issued = $studetn->passport_data['issued'];
                $newData[$code][] = $issued;
            }
        }
        $n = collect($newData)->map(function ($data) {
            return collect($data)->unique()->values();
        })->toArray();


        Cache::forever('subdivision_codes', $n);
    }
}
