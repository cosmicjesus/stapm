<?php

use App\Models\News;
use Illuminate\Database\Seeder;
use App\Helpers\StorageFile;

class ImportNews extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news=DB::connection('news')->table('news')->select('*')->get();
        News::query()->truncate();
        foreach ($news as $item) {
            $model = new News();
            $model->title = $item->title;
            $model->publication_date = \Carbon\Carbon::createFromFormat('d-m-Y', $item->data)->format('Y-m-d');
            $model->active = true;
            $model->preview=$item->text_news;
            $model->full_text=$item->text_news;
            $model->image=null;
            $model->save();
            dispatch(new \App\Jobs\ImportNewsImage($model,$item->images))->onConnection('database')->onQueue('news_img');
        }
    }
}
