<?php

use Illuminate\Database\Seeder;

class AttachCategoryIds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documents = \App\Models\Document::with('categories')->get();

        \App\Models\CategoryDocument::query()->truncate();

        foreach ($documents as $document) {
            $document->categories()->sync($document->category_ids);
        }
    }
}
