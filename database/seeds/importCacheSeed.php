<?php

use Illuminate\Database\Seeder;

class importCacheSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Артём',
            'Артем',
            'Антон',
            'Александр',
            'Вадим',
            'Валерий',
            'Владимир',
            'Максим',
            'Даниил',
            'Дмитрий',
            'Евгений',
            'Иван',
            'Илхам',
            'Кирилл',
            'Никита',
            'Михаил',
            'Егор',
            'Матвей',
            'Андрей',
            'Илья',
            'Алексей',
            'Роман',
            'Сергей',
            'Владислав',
            'Ярослав',
            'Тимофей',
            'Арсений',
            'Денис',
            'Владимир',
            'Павел',
            'Глеб',
            'Константин',
            'Богдан',
            'Евгений',
            'Николай',
            'Степан',
            'Захар',
            'София',
            'Анастасия',
            'Дарья',
            'Мария',
            'Анна',
            'Виктория',
            'Полина',
            'Елизавета',
            'Екатерина',
            'Ксения',
            'Валерия',
            'Варвара',
            'Александра',
            'Вероника',
            'Арина',
            'Алиса',
            'Алина',
            'Маргарита',
            'Диана*',
            'Ульяна',
            'Алёна',
            'Ангелина',
            'Анжелика',
            'Кристина',
            'Юлия',
            'Кира',
            'Ева',
            'Карина',
            'Ольга'
        ];

        $middlenames = [
            'Александрович',
            'Адамович',
            'Анатольевич',
            'Аркадьевич',
            'Алексеевич',
            'Андреевич',
            'Артемович',
            'Альбертович',
            'Антонович',
            'Богданович',
            'Богуславович',
            'Борисович',
            'Вадимович',
            'Васильевич',
            'Владимирович',
            'Валентинович',
            'Вениаминович',
            'Вячеславович',
            'Валерьевич',
            'Викторович',
            'Геннадиевич',
            'Георгиевич',
            'Геннадьевич',
            'Григорьевич',
            'Давидович',
            'Денисович',
            'Данилович',
            'Дмитриевич',
            'Евгеньевич',
            'Егорович',
            'Ефимович',
            'Иванович',
            'Ильич',
            'Игоревич',
            'Иосифович',
            'Кириллович',
            'Константинович',
            'Леонидович',
            'Львович',
            'Макарович',
            'Максович',
            'Миронович',
            'Максимович',
            'Матвеевич',
            'Михайлович',
            'Натанович',
            'Наумович',
            'Николаевич',
            'Олегович',
            'Оскарович',
            'Павлович',
            'Петрович',
            'Платонович',
            'Робертович',
            'Ростиславович',
            'Рудольфович',
            'Романович',
            'Рубенович',
            'Русланович',
            'Святославович',
            'Сергеевич',
            'Степанович',
            'Семенович',
            'Станиславович',
            'Тарасович',
            'Тимофеевич',
            'Тимурович',
            'Федорович',
            'Феликсович',
            'Филиппович',
            'Харитонович',
            'Эдуардович',
            'Эмануилович',
            'Эльдарович',
            'Юрьевич',
            'Юхимович',
            'Яковлевич',
            'Ярославович',
            'Александровна',
            'Андреевна',
            'Архиповна',
            'Алексеевна',
            'Антоновна',
            'Аскольдовна',
            'Альбертовна',
            'Аркадьевна',
            'Афанасьевна',
            'Анатольевна',
            'Артемовна',
            'Богдановна',
            'Болеславовна',
            'Борисовна',
            'Вадимовна',
            'Васильевна',
            'Владимировна',
            'Валентиновна',
            'Вениаминовна',
            'Владиславовна',
            'Валериевна',
            'Викторовна',
            'Вячеславовна',
            'Геннадиевна',
            'Георгиевна',
            'Геннадьевна',
            'Григорьевна',
            'Даниловна',
            'Дмитриевна',
            'Евгеньевна',
            'Егоровны',
            'Егоровна',
            'Ефимовна',
            'Ждановна',
            'Захаровна',
            'Ивановна',
            'Игоревна',
            'Ильинична',
            'Кирилловна',
            'Кузминична',
            'Константиновна',
            'Кузьминична',
            'Леонидовна',
            'Леоновна',
            'Львовна',
            'Макаровна',
            'Матвеевна',
            'Михайловна',
            'Максимовна',
            'Мироновна',
            'Натановна',
            'Никифоровна',
            'Ниловна',
            'Наумовна',
            'Николаевна',
            'Олеговна',
            'Оскаровна',
            'Павловна',
            'Петровна',
            'Робертовна',
            'Рубеновна',
            'Руслановна',
            'Романовна',
            'Рудольфовна',
            'Святославовна',
            'Сергеевна',
            'Степановна',
            'Семеновна',
            'Станиславовна',
            'Тарасовна',
            'Тимофеевна',
            'Тимуровна',
            'Федоровна',
            'Феликсовна',
            'Филипповна',
            'Харитоновна',
            'Эдуардовна',
            'Эльдаровна',
            'Юльевна',
            'Юрьевна',
            'Яковлевна'
        ];

        $graduation_organizations_name = \App\Models\Student::query()->whereNotNull('graduation_organization_name')->distinct()->get()->map(function ($student) {
            return $student->graduation_organization_name;
        })->unique()->toArray();

        $graduation_organizations_place = \App\Models\Student::query()->whereNotNull('graduation_organization_place')->distinct()->get()->map(function ($student) {
            return $student->graduation_organization_place;
        })->unique()->toArray();

        $birthPlaces = \App\Models\Student::query()->whereNotNull('passport_data')->get()->map(function ($student) {
            $data = json_decode($student->passport_data, true);
            return $data['birthPlace'];
        })->unique()->toArray();

        $streets = \App\Models\Student::query()
            ->whereNotNull('addresses')
            ->get()
            ->map(function ($student) {
                $data = json_decode($student->addresses, true);
                if (!is_null($data['registration']['street'])) {
                    return $data['registration']['street'];
                }
                //return '';
            })->unique()
            ->toArray();

        $cities = \App\Models\Student::query()
            ->whereNotNull('addresses')
            ->get()
            ->map(function ($student) {
                $data = json_decode($student->addresses, true);
                if (!is_null($data['registration']['city'])) {
                    return $data['registration']['city'];
                }
                //return '';
            })
            ->unique()
            ->toArray();

        $settlement = \App\Models\Student::query()
            ->whereNotNull('addresses')
            ->get()
            ->map(function ($student) {
                $data = json_decode($student->addresses, true);
                if (!is_null($data['registration']['settlement']) && $data['registration']['settlement'] != 'null') {
                    return $data['registration']['settlement'];
                }
                return '';
            })
            ->unique()
            ->toArray();


        $cities = array_diff($cities, array('', NULL, false));
        $streets = array_diff($streets, array('', NULL, false));
        $settlement = array_diff($settlement, array('', NULL, false));
        $graduation_organizations_place = array_diff($graduation_organizations_place, array('', NULL, false));
        $graduation_organizations_name = array_diff($graduation_organizations_name, array('', NULL, false));

        $regions = [
            'Самарская',
            'Ульяновская',
            'Оренбургская'
        ];
        $area = ['Волжский', 'Кинельский'];


        sort($names);
        sort($middlenames);
        sort($graduation_organizations_name);
        sort($graduation_organizations_place);
        sort($birthPlaces);
        sort($regions);
        sort($streets);
        sort($cities);
        sort($settlement);
        sort($area);

        $arr = compact(
            'names',
            'middlenames',
            'graduation_organizations_name',
            'graduation_organizations_place',
            'birthPlaces',
            'regions',
            'streets',
            'cities',
            'settlement',
            'area'
        );

        if (env('RELOAD_CACHE')) {
            foreach ($arr as $key => $item) {
                Cache::forget($key);
            }
        }

        foreach ($arr as $key => $item) {
            if (Cache::has($key)) {
                echo 'Загрузка кэша с кодом ' . $key . ' не требуется' . PHP_EOL;
            } else {
                Cache::forever($key, $item);
            }

        }
    }
}
