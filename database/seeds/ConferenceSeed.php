<?php

use Illuminate\Database\Seeder;

class ConferenceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('APP_ENV') == 'local' && env('APP_DEBUG') == true) {
            $faker = Faker\Factory::create('ru_RU');

            $limit = 10;
            \App\Models\Conference::truncate();
            for ($i = 0; $i < $limit; $i++) {
                \App\Models\Conference::create([
                    'title' => $faker->text(70),
                    'date_of_event' => $faker->date(),
                    'active' => true,
                    'full_text' => "<p> <h4 style='text-align: center'>{$faker->text(10)}</h4>" . $faker->text(200) . "</p>"
                ]);
            }
        } else {
            echo 'Not call in production';
        }
    }
}
