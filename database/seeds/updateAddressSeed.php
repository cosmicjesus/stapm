<?php

use Illuminate\Database\Seeder;

class updateAddressSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enrollees = \App\Models\Student::query()
            ->where('status', 'enrollee')
            ->where('need_hostel', true)
            ->get();
        $address = [
            'region' => 'Самарская',
            'area' => null,
            'settlement' => null,
            'city' => 'Самара',
            'index' => '443052',
            'street' => 'Старый переулок',
            'house_number' => '6',
            'housing' => null,
            'apartment_number' => null,
        ];
        foreach ($enrollees as $enrollee) {
            $addressses = (array)json_decode($enrollee->addresses);

            $addressses['place_of_stay'] = $address;
            $addressses['residential'] = $address;
            $addressses['isAddressSimilar'] = false;
            $addressses['isAddressPlaceOfStay'] = false;
            $enrollee->addresses = json_encode($addressses);
            $enrollee->save();
        }
    }
}
