<?php

use Illuminate\Database\Seeder;

class RebuildPassportData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Models\Student::query()
            ->whereNotNull('passport_data')
            ->get();
        foreach ($students as $student) {
            $data = $student->passport_data;
            if(!is_null($data['documentType'])){
                $data['documentType'] = (int)$data['documentType'];
            }

            if(!is_null($data['issuanceDate'])){
                $data['issuanceDate'] = \Carbon\Carbon::createFromFormat('d.m.Y', $data['issuanceDate'])->format('Y-m-d');
                dd($student);
            }
            $student->passport_data = $data;
            $student->save();
        }

    }
}
