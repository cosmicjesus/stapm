<?php

use Illuminate\Database\Seeder;

class UpdateAddressesFields extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Models\Student::whereNotNull('addresses')->get();

        $keys = ['residential', 'registration', 'place_of_stay'];

        foreach ($students as $student) {
            $addresses = $student->addresses;

            foreach ($keys as $key) {
                $addresses[$key]['city_area'] = null;
                $addresses[$key]['registration_date'] = null;
            }
            $student->addresses = $addresses;
            $student->save();
        }

        \App\Models\Activity::query()->where(['causer_id' => null, 'causer_type' => null])->update(['causer_id' => 3, 'causer_type' => 'App\User']);
    }
}
