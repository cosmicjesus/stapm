<?php

use App\Models\PositionType;
use Illuminate\Database\Seeder;

class PositionTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");
        PositionType::query()->truncate();
        DB::statement("SET foreign_key_checks=1");

        PositionType::create(['name'=>'Руководящие работники']);
        PositionType::create(['name'=>'Педагогические работники']);
        PositionType::create(['name'=>'Учебно-вспомогательный персонал']);
        PositionType::create(['name'=>'Обслуживающий персонал']);
    }
}
