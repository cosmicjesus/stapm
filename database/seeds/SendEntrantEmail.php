<?php

use Illuminate\Database\Seeder;

class SendEntrantEmail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entrants=\App\Models\Entrant::query()->where('status','unconfirmed_enrollee')->get();

        $now=\Carbon\Carbon::now();
        foreach ($entrants as $entrant) {
            $now->addSeconds(60);
            if($entrant->email){
                dispatch(new \App\Jobs\SendEntrantEmail($entrant))->delay($now);
            }
        }
    }
}
