<?php

$positions = [
    0 =>
        array(
            'id' => 8,
            'name' => 'Администратор АСУ РСО',
            'permissionGroup' =>
                array(
                    'name' => 'Администратор АСУ РСО',
                    'id' => 1,
                ),
            'type' => 'SupportStuff',
        ),
    1 =>
        array(
            'id' => 2078,
            'name' => 'Библиотекарь',
            'type' => 'ServiceStuff',
        ),
    2 =>
        array(
            'id' => 2081,
            'name' => 'Бухгалтер',
            'type' => 'ServiceStuff',
        ),
    3 =>
        array(
            'id' => 43,
            'name' => 'Вахтёр',
            'type' => 'SupportStuff',
        ),
    4 =>
        array(
            'id' => 18,
            'name' => 'Ведущий бухгалтер',
            'type' => 'SupportStuff',
        ),
    5 =>
        array(
            'id' => 37,
            'name' => 'Водитель',
            'type' => 'SupportStuff',
        ),
    6 =>
        array(
            'id' => 21,
            'name' => 'Воспитатель',
            'type' => 'SupportStuff',
        ),
    7 =>
        array(
            'id' => 38,
            'name' => 'Гардеробщик',
            'type' => 'SupportStuff',
        ),
    8 =>
        array(
            'id' => 19,
            'name' => 'Главный бухгалтер',
            'type' => 'SupportStuff',
        ),
    9 =>
        array(
            'id' => 1060,
            'name' => 'Дворник',
            'type' => 'SupportStuff',
        ),
    10 =>
        array(
            'id' => 52,
            'name' => 'Дежурный по общежитию',
            'type' => 'SupportStuff',
        ),
    11 =>
        array(
            'id' => 49,
            'name' => 'Директор',
            'permissionGroup' =>
                array(
                    'name' => 'Директор',
                    'id' => 9,
                ),
            'type' => 'Executives',
        ),
    12 =>
        array(
            'id' => 12,
            'name' => 'Диспетчер ОУ',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебной частью',
                    'id' => 2,
                ),
            'type' => 'SupportStuff',
        ),
    13 =>
        array(
            'id' => 58,
            'name' => 'Заведующая библиотекой',
            'type' => 'SupportStuff',
        ),
    14 =>
        array(
            'id' => 1061,
            'name' => 'Заведующая хозяйства',
            'type' => 'SupportStuff',
        ),
    15 =>
        array(
            'id' => 2059,
            'name' => 'Заведующий лабораторией',
            'type' => 'SupportStuff',
        ),
    16 =>
        array(
            'id' => 2064,
            'name' => 'Заведующий отделением',
            'permissionGroup' =>
                array(
                    'name' => 'Управление обучением',
                    'id' => 3,
                ),
            'type' => 'Executives',
        ),
    17 =>
        array(
            'id' => 2063,
            'name' => 'Заведующий учебной частью',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебной частью',
                    'id' => 2,
                ),
            'type' => 'SupportStuff',
        ),
    18 =>
        array(
            'id' => 2062,
            'name' => 'Заведующий учебно-производственным сектором',
            'permissionGroup' =>
                array(
                    'name' => 'Управление образовательной организацией',
                    'id' => 2011,
                ),
            'type' => 'SupportStuff',
        ),
    19 =>
        array(
            'id' => 2082,
            'name' => 'заведующий хозяйством',
            'type' => 'Executives',
        ),
    20 =>
        array(
            'id' => 26,
            'name' => 'Заместитель директора по АХЧ',
            'type' => 'Executives',
        ),
    21 =>
        array(
            'id' => 57,
            'name' => 'Заместитель директора по безопасности',
            'type' => 'Executives',
        ),
    22 =>
        array(
            'id' => 2074,
            'name' => 'Заместитель директора по методической работе',
            'permissionGroup' =>
                array(
                    'name' => 'Управление образовательной организацией',
                    'id' => 2011,
                ),
            'type' => 'Executives',
        ),
    23 =>
        array(
            'id' => 2067,
            'name' => 'Заместитель директора по социально-педагогической работе',
            'permissionGroup' =>
                array(
                    'name' => 'Управление студентами',
                    'id' => 2012,
                ),
            'type' => 'TeachingStuff',
        ),
    24 =>
        array(
            'id' => 2060,
            'name' => 'Заместитель директора по УМР',
            'permissionGroup' =>
                array(
                    'name' => 'Управление образовательной организацией',
                    'id' => 2011,
                ),
            'type' => 'Executives',
        ),
    25 =>
        array(
            'id' => 2065,
            'name' => 'Заместитель директора по УР',
            'permissionGroup' =>
                array(
                    'name' => 'Управление обучением',
                    'id' => 3,
                ),
            'type' => 'Executives',
        ),
    26 =>
        array(
            'id' => 13,
            'name' => 'Заместитель директора по учебной работе',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебной частью',
                    'id' => 2,
                ),
            'type' => 'Executives',
        ),
    27 =>
        array(
            'id' => 14,
            'name' => 'Заместитель директора по учебно-производственной работе',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебно-производственной работой',
                    'id' => 4,
                ),
            'type' => 'Executives',
        ),
    28 =>
        array(
            'id' => 16,
            'name' => 'Заместитель директора учебно-воспитательной работе',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебно-воспитательной работой',
                    'id' => 6,
                ),
            'type' => 'Executives',
        ),
    29 =>
        array(
            'id' => 2071,
            'name' => 'Инженер-программист',
            'permissionGroup' =>
                array(
                    'name' => 'Техник-программист',
                    'id' => 1011,
                ),
            'type' => 'ServiceStuff',
        ),
    30 =>
        array(
            'id' => 36,
            'name' => 'Инженер-электроник',
            'type' => 'SupportStuff',
        ),
    31 =>
        array(
            'id' => 15,
            'name' => 'Инспектор по кардрам',
            'permissionGroup' =>
                array(
                    'name' => 'Управление кадрами сотрудников',
                    'id' => 5,
                ),
            'type' => 'SupportStuff',
        ),
    32 =>
        array(
            'id' => 29,
            'name' => 'Инструктор по физкультуре',
            'type' => 'SupportStuff',
        ),
    33 =>
        array(
            'id' => 55,
            'name' => 'Кастелянша',
            'type' => 'SupportStuff',
        ),
    34 =>
        array(
            'id' => 53,
            'name' => 'Комендант',
            'type' => 'SupportStuff',
        ),
    35 =>
        array(
            'id' => 48,
            'name' => 'Лаборант',
            'type' => 'SupportStuff',
        ),
    36 =>
        array(
            'id' => 17,
            'name' => 'Мастер п/о',
            'permissionGroup' =>
                array(
                    'name' => 'Мастер п/о',
                    'id' => 10,
                ),
            'type' => 'TeachingStuff',
        ),
    37 =>
        array(
            'id' => 51,
            'name' => 'Мастер производства',
            'type' => 'TeachingStuff',
        ),
    38 =>
        array(
            'id' => 35,
            'name' => 'Медицинская сестра',
            'type' => 'SupportStuff',
        ),
    39 =>
        array(
            'id' => 2066,
            'name' => 'Методист',
            'permissionGroup' =>
                array(
                    'name' => 'Управление обучением',
                    'id' => 3,
                ),
            'type' => 'TeachingStuff',
        ),
    40 =>
        array(
            'id' => 33,
            'name' => 'Механик',
            'type' => 'SupportStuff',
        ),
    41 =>
        array(
            'id' => 2061,
            'name' => 'Начальник информационно-вычислительного центра',
            'permissionGroup' =>
                array(
                    'name' => 'Управление образовательной организацией',
                    'id' => 2011,
                ),
            'type' => 'SupportStuff',
        ),
    42 =>
        array(
            'id' => 2083,
            'name' => 'Начальник отдела кадров',
            'type' => 'Executives',
        ),
    43 =>
        array(
            'id' => 25,
            'name' => 'Начальник штаба ГО',
            'type' => 'SupportStuff',
        ),
    44 =>
        array(
            'id' => 2075,
            'name' => 'Оператор',
            'permissionGroup' =>
                array(
                    'name' => 'Оператор',
                    'id' => 2015,
                ),
            'type' => 'SupportStuff',
        ),
    45 =>
        array(
            'id' => 54,
            'name' => 'Паспортист',
            'type' => 'SupportStuff',
        ),
    46 =>
        array(
            'id' => 2072,
            'name' => 'Педагог-организатор',
            'permissionGroup' =>
                array(
                    'name' => 'Управление студентами',
                    'id' => 2012,
                ),
            'type' => 'TeachingStuff',
        ),
    47 =>
        array(
            'id' => 30,
            'name' => 'Педагог-психолог',
            'permissionGroup' =>
                array(
                    'name' => 'Управление обучением',
                    'id' => 3,
                ),
            'type' => 'TeachingStuff',
        ),
    48 =>
        array(
            'id' => 39,
            'name' => 'Плотник',
            'type' => 'SupportStuff',
        ),
    49 =>
        array(
            'id' => 11,
            'name' => 'Преподаватель',
            'permissionGroup' =>
                array(
                    'name' => 'Управление обучением',
                    'id' => 3,
                ),
            'type' => 'TeachingStuff',
        ),
    50 =>
        array(
            'id' => 2069,
            'name' => 'Преподаватель-организатор ОБЖ',
            'permissionGroup' =>
                array(
                    'name' => 'Управление студентами',
                    'id' => 2012,
                ),
            'type' => 'TeachingStuff',
        ),
    51 =>
        array(
            'id' => 2085,
            'name' => 'Профконсультант',
            'type' => 'SupportStuff',
        ),
    52 =>
        array(
            'id' => 2073,
            'name' => 'Руководитель учено-производственным сектором',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебно-производственной работой',
                    'id' => 4,
                ),
            'type' => 'TeachingStuff',
        ),
    53 =>
        array(
            'id' => 2068,
            'name' => 'Руководитель физвоспитания',
            'permissionGroup' =>
                array(
                    'name' => 'Управление студентами',
                    'id' => 2012,
                ),
            'type' => 'SupportStuff',
        ),
    54 =>
        array(
            'id' => 23,
            'name' => 'Руководитель физического воспитания',
            'type' => 'SupportStuff',
        ),
    55 =>
        array(
            'id' => 10,
            'name' => 'Секретарь',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебно-производственной работой',
                    'id' => 4,
                ),
            'type' => 'SupportStuff',
        ),
    56 =>
        array(
            'id' => 34,
            'name' => 'Секретарь руководителя',
            'type' => 'SupportStuff',
        ),
    57 =>
        array(
            'id' => 9,
            'name' => 'Секретарь учебной части',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебной частью',
                    'id' => 2,
                ),
            'type' => 'SupportStuff',
        ),
    58 =>
        array(
            'id' => 42,
            'name' => 'Слесарь-ремонтник',
            'type' => 'SupportStuff',
        ),
    59 =>
        array(
            'id' => 40,
            'name' => 'Слесарь-сантехник',
            'type' => 'SupportStuff',
        ),
    60 =>
        array(
            'id' => 41,
            'name' => 'Слесарь-электрик по ремонту электрического оборудования',
            'type' => 'SupportStuff',
        ),
    61 =>
        array(
            'id' => 28,
            'name' => 'Социальный педагог',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебно-воспитательной работой',
                    'id' => 6,
                ),
            'type' => 'TeachingStuff',
        ),
    62 =>
        array(
            'id' => 2086,
            'name' => 'Специалист по закупкам',
            'type' => 'Executives',
        ),
    63 =>
        array(
            'id' => 2084,
            'name' => 'Специалист по защите информации',
            'type' => 'ServiceStuff',
        ),
    64 =>
        array(
            'id' => 2077,
            'name' => 'Специалист по кадрам',
            'permissionGroup' =>
                array(
                    'name' => 'Управление кадрами сотрудников',
                    'id' => 5,
                ),
            'type' => 'Executives',
        ),
    65 =>
        array(
            'id' => 2080,
            'name' => 'Специалист по охране труда',
            'type' => 'ServiceStuff',
        ),
    66 =>
        array(
            'id' => 24,
            'name' => 'Старший мастер',
            'type' => 'TeachingStuff',
        ),
    67 =>
        array(
            'id' => 50,
            'name' => 'Старший мастер производства',
            'type' => 'TeachingStuff',
        ),
    68 =>
        array(
            'id' => 27,
            'name' => 'Старший методист',
            'permissionGroup' =>
                array(
                    'name' => 'Управление учебной частью',
                    'id' => 2,
                ),
            'type' => 'TeachingStuff',
        ),
    69 =>
        array(
            'id' => 44,
            'name' => 'Сторож',
            'type' => 'SupportStuff',
        ),
    70 =>
        array(
            'id' => 2087,
            'name' => 'Сурдопереводчик',
            'type' => 'ServiceStuff',
        ),
    71 =>
        array(
            'id' => 2070,
            'name' => 'Техник информационно-вычислительного центра',
            'permissionGroup' =>
                array(
                    'name' => 'Управление кадрами сотрудников',
                    'id' => 5,
                ),
            'type' => 'SupportStuff',
        ),
    72 =>
        array(
            'id' => 1059,
            'name' => 'Техник-программист',
            'permissionGroup' =>
                array(
                    'name' => 'Администратор АСУ РСО',
                    'id' => 1,
                ),
            'type' => 'SupportStuff',
        ),
    73 =>
        array(
            'id' => 31,
            'name' => 'Техник-технолог',
            'type' => 'SupportStuff',
        ),
    74 =>
        array(
            'id' => 46,
            'name' => 'Уборщик производственных помещений',
            'type' => 'SupportStuff',
        ),
    75 =>
        array(
            'id' => 45,
            'name' => 'Уборщик служебных помещений',
            'type' => 'SupportStuff',
        ),
    76 =>
        array(
            'id' => 47,
            'name' => 'Уборщик территории',
            'type' => 'SupportStuff',
        ),
    77 =>
        array(
            'id' => 2076,
            'name' => 'Экономист',
            'permissionGroup' =>
                array(
                    'name' => 'Экономист',
                    'id' => 2016,
                ),
            'type' => 'ServiceStuff',
        ),
    78 =>
        array(
            'id' => 56,
            'name' => 'Электромонтёр связи',
            'type' => 'SupportStuff',
        ),
    79 =>
        array(
            'id' => 32,
            'name' => 'Юрисконсульт',
            'type' => 'SupportStuff',
        )

];