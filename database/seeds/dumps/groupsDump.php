<?php
$groups = array(
    0 =>
        array(
            'hasStudents' => true,
            'id' => 11084,
            'pattern' => '{Курс}1',
            'name' => '11',
            'code' => 'Стан-2017',
            'program' => 5,
            'plan' => 38,
            'educationProgram' =>
                array(
                    'name' => 'Станочник (металлообработка)(СТ-2017)',
                    'id' => 11105,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10143,
                ),
            'curator' =>
                array(
                    'firstName' => 'Валентина',
                    'lastName' => 'Певцова',
                    'middleName' => 'Александровна',
                    'id' => 27,
                ),
            'shiftId' => 1,
        ),
    1 =>
        array(
            'hasStudents' => true,
            'id' => 11081,
            'pattern' => '{Курс}2',
            'name' => '12',
            'code' => 'Свар-2017',
            'program' => 27,
            'plan' => 35,
            'educationProgram' =>
                array(
                    'name' => 'Свар-2017',
                    'id' => 12110,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10144,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александр',
                    'lastName' => 'Тимофеев',
                    'middleName' => 'Владимирович',
                    'id' => 30,
                ),
            'shiftId' => 1,
        ),
    2 =>
        array(
            'hasStudents' => true,
            'id' => 11082,
            'pattern' => '{Курс}3',
            'name' => '13',
            'code' => 'Контр-2017',
            'program' => 19,
            'plan' => 34,
            'educationProgram' =>
                array(
                    'name' => 'Контролер станочных и слесарных работ(КОНТР-2017)',
                    'id' => 11099,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10145,
                ),
            'curator' =>
                array(
                    'firstName' => 'Надежда',
                    'lastName' => 'Котелкина',
                    'middleName' => 'Евгеньевна',
                    'id' => 6060,
                ),
            'shiftId' => 1,
        ),
    3 =>
        array(
            'hasStudents' => true,
            'id' => 11075,
            'pattern' => '{Курс}4',
            'name' => '14',
            'code' => 'ТУ-2017',
            'program' => 24,
            'plan' => 40,
            'educationProgram' =>
                array(
                    'name' => 'Токарь-универсал(ТУ-2017)',
                    'id' => 11101,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10146,
                ),
            'curator' =>
                array(
                    'firstName' => 'Антон',
                    'lastName' => 'Куприянов',
                    'middleName' => 'Геннадьевич',
                    'id' => 6159,
                ),
            'shiftId' => 1,
        ),
    4 =>
        array(
            'hasStudents' => true,
            'id' => 11076,
            'pattern' => '{Курс}5',
            'name' => '15',
            'code' => 'Слес-2017',
            'program' => 30,
            'plan' => 36,
            'educationProgram' =>
                array(
                    'name' => 'Слесарь по ремонту строительных машин(СЛ-2017)',
                    'id' => 11104,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10147,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александр',
                    'lastName' => 'Тимофеев',
                    'middleName' => 'Владимирович',
                    'id' => 30,
                ),
            'shiftId' => 1,
        ),
    5 =>
        array(
            'hasStudents' => true,
            'id' => 9071,
            'pattern' => '{Курс}1',
            'name' => '21',
            'code' => 'СТ-2016',
            'program' => 5,
            'plan' => 12,
            'educationProgram' =>
                array(
                    'name' => 'Станочник (металлообработка)(СТ-2016)',
                    'id' => 10064,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10148,
                ),
            'curator' =>
                array(
                    'firstName' => 'Андрей',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Петрович',
                    'id' => 22,
                ),
            'shiftId' => 1,
        ),
    6 =>
        array(
            'hasStudents' => true,
            'id' => 9063,
            'pattern' => '{Курс}2',
            'name' => '22',
            'code' => 'СВ-2016',
            'program' => 27,
            'plan' => 15,
            'educationProgram' =>
                array(
                    'name' => 'Сварщик (СВ-2016)',
                    'id' => 10060,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10149,
                ),
            'curator' =>
                array(
                    'firstName' => 'Марина',
                    'lastName' => 'Тельцова',
                    'middleName' => 'Ивановна',
                    'id' => 28,
                ),
            'shiftId' => 1,
        ),
    7 =>
        array(
            'hasStudents' => true,
            'id' => 9065,
            'pattern' => '{Курс}3',
            'name' => '23',
            'code' => 'КОНТР-2016',
            'program' => 19,
            'plan' => 13,
            'educationProgram' =>
                array(
                    'name' => 'Контролер станочных и слесарных работ(КОНТР-2016)',
                    'id' => 10057,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10150,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Колмакова',
                    'middleName' => 'Андреевна',
                    'id' => 23,
                ),
            'shiftId' => 1,
        ),
    8 =>
        array(
            'hasStudents' => true,
            'id' => 9069,
            'pattern' => '{Курс}4',
            'name' => '24',
            'code' => 'ТУ-2016',
            'program' => 24,
            'plan' => 10,
            'educationProgram' =>
                array(
                    'name' => 'Токарь-универсал(ТУ-2016)',
                    'id' => 10059,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10151,
                ),
            'curator' =>
                array(
                    'firstName' => 'Валентина',
                    'lastName' => 'Певцова',
                    'middleName' => 'Александровна',
                    'id' => 27,
                ),
            'shiftId' => 1,
        ),
    9 =>
        array(
            'hasStudents' => true,
            'id' => 7032,
            'pattern' => '{Курс}1',
            'name' => '31',
            'code' => 'СТАН-2015',
            'program' => 5,
            'plan' => 11,
            'educationProgram' =>
                array(
                    'name' => 'Станочник 2015',
                    'id' => 3023,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10153,
                ),
            'curator' =>
                array(
                    'firstName' => 'Андрей',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Петрович',
                    'id' => 22,
                ),
            'shiftId' => 1,
        ),
    10 =>
        array(
            'hasStudents' => true,
            'id' => 9031,
            'pattern' => '{Курс}2',
            'name' => '32',
            'code' => 'СВАР-2015',
            'program' => 27,
            'plan' => 14,
            'educationProgram' =>
                array(
                    'name' => 'Сварщик (Электросварочные и газосварочные работы) 2015',
                    'id' => 10027,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10154,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александр',
                    'lastName' => 'Тимофеев',
                    'middleName' => 'Владимирович',
                    'id' => 30,
                ),
            'shiftId' => 1,
        ),
    11 =>
        array(
            'hasStudents' => true,
            'id' => 5033,
            'pattern' => '{Курс}4',
            'name' => '34',
            'code' => 'ТОК-2015',
            'program' => 24,
            'plan' => 9,
            'educationProgram' =>
                array(
                    'name' => 'Токарь-универсал(Токарь - 2015)',
                    'id' => 3026,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10155,
                ),
            'curator' =>
                array(
                    'firstName' => 'Валентина',
                    'lastName' => 'Певцова',
                    'middleName' => 'Александровна',
                    'id' => 27,
                ),
            'shiftId' => 1,
        ),
    12 =>
        array(
            'hasStudents' => true,
            'id' => 6030,
            'pattern' => '{Курс}5',
            'name' => '35',
            'code' => 'СЛЕС.РЕМ.МАШ-2015',
            'program' => 30,
            'plan' => 25,
            'educationProgram' =>
                array(
                    'name' => 'Слесарь по ремонту строительных машин(Слесарь-2015)',
                    'id' => 4019,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10156,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александр',
                    'lastName' => 'Тимофеев',
                    'middleName' => 'Владимирович',
                    'id' => 30,
                ),
            'shiftId' => 1,
        ),
    13 =>
        array(
            'hasStudents' => true,
            'id' => 11078,
            'pattern' => 'АСУ-{Курс}',
            'name' => 'АСУ-1',
            'code' => 'АСУ-2017',
            'program' => 29,
            'plan' => 31,
            'educationProgram' =>
                array(
                    'name' => 'Автоматические системы управления(АСУ-2017)',
                    'id' => 11098,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10163,
                ),
            'curator' =>
                array(
                    'firstName' => 'Светлана',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Басировна',
                    'id' => 11,
                ),
            'shiftId' => 1,
        ),
    14 =>
        array(
            'hasStudents' => true,
            'id' => 9066,
            'pattern' => 'АСУ-{Курс}',
            'name' => 'АСУ-2',
            'code' => 'АСУ-2016',
            'program' => 29,
            'plan' => 29,
            'educationProgram' =>
                array(
                    'name' => 'Автоматические системы управления(АСУ-2016)',
                    'id' => 10061,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10170,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
            'shiftId' => 1,
        ),
    15 =>
        array(
            'hasStudents' => true,
            'id' => 9054,
            'pattern' => 'АСУ-{Курс}',
            'name' => 'АСУ-3',
            'code' => 'АСУ-2015',
            'program' => 29,
            'plan' => 28,
            'educationProgram' =>
                array(
                    'name' => 'Автоматические системы управления(АСУ-2015)',
                    'id' => 8020,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10177,
                ),
            'curator' =>
                array(
                    'firstName' => 'Светлана',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Басировна',
                    'id' => 11,
                ),
        ),
    16 =>
        array(
            'hasStudents' => true,
            'id' => 9038,
            'pattern' => 'АСУ-{Курс}',
            'name' => 'АСУ-4',
            'code' => 'АСУ-2014',
            'program' => 29,
            'plan' => 27,
            'educationProgram' =>
                array(
                    'name' => 'Автоматические системы управления(АСУ - 2014)',
                    'id' => 8025,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10184,
                ),
            'curator' =>
                array(
                    'firstName' => 'Валентина',
                    'lastName' => 'Андропова',
                    'middleName' => 'Васильевна',
                    'id' => 6072,
                ),
        ),
    17 =>
        array(
            'hasStudents' => true,
            'id' => 11074,
            'pattern' => 'ИС-{Курс}',
            'name' => 'ИС-1',
            'code' => 'ИС-2017',
            'program' => 1,
            'plan' => 30,
            'educationProgram' =>
                array(
                    'name' => 'Информационные системы (по отраслям) (ИС-2017)',
                    'id' => 11097,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10157,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александра',
                    'lastName' => 'Китаева',
                    'middleName' => 'Николаевна',
                    'id' => 12,
                ),
            'shiftId' => 1,
        ),
    18 =>
        array(
            'hasStudents' => true,
            'id' => 9070,
            'pattern' => 'ИС-{Курс}',
            'name' => 'ИС-2',
            'code' => 'ИС-2016',
            'program' => 1,
            'plan' => 4,
            'educationProgram' =>
                array(
                    'name' => 'Информационные системы (по отраслям) (ИС-2016)',
                    'id' => 10063,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10164,
                ),
            'curator' =>
                array(
                    'firstName' => 'Анна',
                    'lastName' => 'Зуева',
                    'middleName' => 'Александровна',
                    'id' => 1,
                ),
            'shiftId' => 1,
        ),
    19 =>
        array(
            'hasStudents' => true,
            'id' => 4032,
            'pattern' => 'ИС-{Курс}',
            'name' => 'ИС-3',
            'code' => 'ИС-2015',
            'program' => 1,
            'plan' => 3,
            'educationProgram' =>
                array(
                    'name' => 'Информационные системы (по отраслям) (Группа ИС-2015)',
                    'id' => 1025,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10171,
                ),
            'curator' =>
                array(
                    'firstName' => 'Анна',
                    'lastName' => 'Зуева',
                    'middleName' => 'Александровна',
                    'id' => 1,
                ),
        ),
    20 =>
        array(
            'hasStudents' => true,
            'id' => 8034,
            'pattern' => 'ИС-{Курс}',
            'name' => 'ИС-4',
            'code' => 'ИС-2014',
            'program' => 1,
            'plan' => 2,
            'educationProgram' =>
                array(
                    'name' => 'Информационные системы (по отраслям ) (Группа ИС-2014)',
                    'id' => 9026,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10178,
                ),
            'curator' =>
                array(
                    'firstName' => 'Виктор',
                    'lastName' => 'Намнясов',
                    'middleName' => 'Геннадьевич',
                    'id' => 25,
                ),
        ),
    21 =>
        array(
            'hasStudents' => true,
            'id' => 11080,
            'pattern' => 'КС-{Курс}',
            'name' => 'КС-1',
            'code' => 'КС - 2017',
            'program' => 33,
            'plan' => 33,
            'educationProgram' =>
                array(
                    'name' => 'Компьютерные сети(КС-2017)',
                    'id' => 11096,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10158,
                ),
            'curator' =>
                array(
                    'firstName' => 'Валентина',
                    'lastName' => 'Певцова',
                    'middleName' => 'Александровна',
                    'id' => 27,
                ),
            'shiftId' => 1,
        ),
    22 =>
        array(
            'hasStudents' => true,
            'id' => 9061,
            'pattern' => 'КС-{Курс}',
            'name' => 'КС-2',
            'code' => 'КС-2016',
            'program' => 33,
            'plan' => 32,
            'educationProgram' =>
                array(
                    'name' => 'Компьютерные сети(КС-2016)',
                    'id' => 10058,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10165,
                ),
            'curator' =>
                array(
                    'firstName' => 'Евгения',
                    'lastName' => 'Глистенкова',
                    'middleName' => 'Александровна',
                    'id' => 6082,
                ),
        ),
    23 =>
        array(
            'hasStudents' => true,
            'id' => 9036,
            'pattern' => 'ПК-{Курс}',
            'name' => 'ПК-4',
            'code' => 'ПК-2014',
            'program' => 21,
            'plan' => 1,
            'educationProgram' =>
                array(
                    'name' => 'Программирование в компьютерных системах(ПК-2014)',
                    'id' => 10031,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10179,
                ),
            'curator' =>
                array(
                    'firstName' => 'Надежда',
                    'lastName' => 'Котелкина',
                    'middleName' => 'Евгеньевна',
                    'id' => 6060,
                ),
        ),
    24 =>
        array(
            'hasStudents' => true,
            'id' => 9033,
            'pattern' => 'РА-{Курс}',
            'name' => 'РА-3',
            'code' => 'РА-2015',
            'program' => 28,
            'plan' => 5,
            'educationProgram' =>
                array(
                    'name' => 'Радиоаппаратостроение(РА-2015)',
                    'id' => 10028,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10172,
                ),
            'curator' =>
                array(
                    'firstName' => 'Диляра',
                    'lastName' => 'Ещенко',
                    'middleName' => 'Рашидовна',
                    'id' => 6069,
                ),
        ),
    25 =>
        array(
            'hasStudents' => true,
            'id' => 11077,
            'pattern' => 'СП-{Курс}',
            'name' => 'СП-1',
            'code' => 'СП2017',
            'program' => 23,
            'plan' => 37,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство (Группа СП-2017)',
                    'id' => 11106,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10162,
                ),
            'curator' =>
                array(
                    'firstName' => 'Марина',
                    'lastName' => 'Тельцова',
                    'middleName' => 'Ивановна',
                    'id' => 28,
                ),
            'shiftId' => 1,
        ),
    27 =>
        array(
            'hasStudents' => true,
            'id' => 10069,
            'pattern' => 'СП-{Курс}',
            'name' => 'СП-2',
            'code' => 'СП-2016',
            'program' => 23,
            'plan' => 24,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство (Группа СП-2016)',
                    'id' => 10065,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10169,
                ),
            'curator' =>
                array(
                    'firstName' => 'Марина',
                    'lastName' => 'Тельцова',
                    'middleName' => 'Ивановна',
                    'id' => 28,
                ),
        ),
    28 =>
        array(
            'hasStudents' => true,
            'id' => 5031,
            'pattern' => 'СП-{Курс}',
            'name' => 'СП-3',
            'code' => 'СП-2015',
            'program' => 23,
            'plan' => 23,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство (Группа СП-2015)',
                    'id' => 1028,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10176,
                ),
            'curator' =>
                array(
                    'firstName' => 'Марина',
                    'lastName' => 'Тельцова',
                    'middleName' => 'Ивановна',
                    'id' => 28,
                ),
        ),
    29 =>
        array(
            'hasStudents' => true,
            'id' => 6036,
            'pattern' => 'СП-{Курс}',
            'name' => 'СП-4',
            'code' => 'СП-2014',
            'program' => 23,
            'plan' => 22,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство (Группа СП-2014)',
                    'id' => 7018,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10183,
                ),
            'curator' =>
                array(
                    'firstName' => 'Александр',
                    'lastName' => 'Тимофеев',
                    'middleName' => 'Владимирович',
                    'id' => 30,
                ),
        ),
    31 =>
        array(
            'hasStudents' => true,
            'id' => 11085,
            'pattern' => 'ТМ-{Курс}',
            'name' => 'ТМ-1',
            'code' => 'ТМ-2017',
            'program' => 25,
            'plan' => 39,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2017 И ТМ-2017А)',
                    'id' => 11107,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10160,
                ),
            'curator' =>
                array(
                    'firstName' => 'Андрей',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Петрович',
                    'id' => 22,
                ),
            'shiftId' => 1,
        ),
    32 =>
        array(
            'hasStudents' => true,
            'id' => 11086,
            'pattern' => 'ТМ-{Курс}А',
            'name' => 'ТМ-1А',
            'code' => 'ТМ-2017А',
            'program' => 25,
            'plan' => 39,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2017А)',
                    'id' => 11102,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10161,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Шамова',
                    'middleName' => 'Николаевна',
                    'id' => 38,
                ),
            'shiftId' => 1,
        ),
    34 =>
        array(
            'hasStudents' => true,
            'id' => 9067,
            'pattern' => 'ТМ-{Курс}',
            'name' => 'ТМ-2',
            'code' => 'ТМ-2016',
            'program' => 25,
            'plan' => 21,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2016 И ТМ-2016А)',
                    'id' => 10062,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10167,
                ),
            'curator' =>
                array(
                    'firstName' => 'Андрей',
                    'lastName' => 'Краснюк',
                    'middleName' => 'Петрович',
                    'id' => 22,
                ),
        ),
    35 =>
        array(
            'hasStudents' => true,
            'id' => 9068,
            'pattern' => 'ТМ-{Курс}А',
            'name' => 'ТМ-2А',
            'code' => 'ТМ-2016А',
            'program' => 25,
            'plan' => 19,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2016А)',
                    'id' => 11093,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10168,
                ),
            'curator' =>
                array(
                    'firstName' => 'Наталия',
                    'lastName' => 'Останина',
                    'middleName' => 'Ивановна',
                    'id' => 17,
                ),
        ),
    38 =>
        array(
            'hasStudents' => true,
            'id' => 4037,
            'pattern' => 'ТМ-{Курс}',
            'name' => 'ТМ-3',
            'code' => 'ТМ-2015',
            'program' => 25,
            'plan' => 20,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2015)',
                    'id' => 1027,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10174,
                ),
            'curator' =>
                array(
                    'firstName' => 'Наталия',
                    'lastName' => 'Останина',
                    'middleName' => 'Ивановна',
                    'id' => 17,
                ),
        ),
    39 =>
        array(
            'hasStudents' => true,
            'id' => 10071,
            'pattern' => 'ТМ-{Курс}А',
            'name' => 'ТМ-3А',
            'code' => 'ТМ-2015А',
            'program' => 25,
            'plan' => 19,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2015)',
                    'id' => 1027,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10175,
                ),
            'curator' =>
                array(
                    'firstName' => 'Галина',
                    'lastName' => 'Тимофеева',
                    'middleName' => 'Владимировна',
                    'id' => 6056,
                ),
        ),
    42 =>
        array(
            'hasStudents' => true,
            'id' => 4036,
            'pattern' => 'ТМ-{Курс}',
            'name' => 'ТМ-4',
            'code' => 'ТМ-2014',
            'program' => 25,
            'plan' => 17,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2014)',
                    'id' => 1022,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10181,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Шамова',
                    'middleName' => 'Николаевна',
                    'id' => 38,
                ),
        ),
    43 =>
        array(
            'hasStudents' => true,
            'id' => 9051,
            'pattern' => 'ТМ-{Курс}А',
            'name' => 'ТМ-4А',
            'code' => 'ТМ-2014А',
            'program' => 25,
            'plan' => 16,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения(ТМ-2014А)',
                    'id' => 10044,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10182,
                ),
            'curator' =>
                array(
                    'firstName' => 'Галина',
                    'lastName' => 'Тимофеева',
                    'middleName' => 'Владимировна',
                    'id' => 6056,
                ),
        ),
    46 =>
        array(
            'hasStudents' => true,
            'id' => 11079,
            'pattern' => 'ТЭ-{Курс}',
            'name' => 'ТЭ-1',
            'code' => 'ТЭ-2017',
            'program' => 31,
            'plan' => 41,
            'educationProgram' =>
                array(
                    'name' => 'Техническая эксплуатация и обслуживание электрического и электромеханического оборудования (по отраслям) (ТЭ-2017)',
                    'id' => 11108,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10159,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Колмакова',
                    'middleName' => 'Андреевна',
                    'id' => 23,
                ),
            'shiftId' => 1,
        ),
    47 =>
        array(
            'hasStudents' => true,
            'id' => 10067,
            'pattern' => 'ТЭ-{Курс}',
            'name' => 'ТЭ-2',
            'code' => 'ТЭ-2016',
            'program' => 31,
            'plan' => 8,
            'educationProgram' =>
                array(
                    'name' => 'Техническая эксплуатация и обслуживание электрического и электромеханического оборудования (по отраслям) (ТЭ-2016)',
                    'id' => 10066,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10166,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Колмакова',
                    'middleName' => 'Андреевна',
                    'id' => 23,
                ),
        ),
    48 =>
        array(
            'hasStudents' => true,
            'id' => 5030,
            'pattern' => 'ТЭ-{Курс}',
            'name' => 'ТЭ-3',
            'code' => 'ТЭ-2015',
            'program' => 31,
            'plan' => 7,
            'educationProgram' =>
                array(
                    'name' => 'Техническая эксплуатация и обслуживание электрического и электромеханического оборудования (по отраслям) (ТЭ-2015)',
                    'id' => 3024,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10173,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    49 =>
        array(
            'hasStudents' => true,
            'id' => 6034,
            'pattern' => 'ТЭ-{Курс}',
            'name' => 'ТЭ-4',
            'code' => 'ТЭ-2014',
            'program' => 31,
            'plan' => 6,
            'educationProgram' =>
                array(
                    'name' => 'Техническая эксплуатация и обслуживание электрического и электромеханического оборудования (по отраслям ) (ТЭ-2014)',
                    'id' => 3018,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10180,
                ),
            'curator' =>
                array(
                    'firstName' => 'Татьяна',
                    'lastName' => 'Колмакова',
                    'middleName' => 'Андреевна',
                    'id' => 23,
                ),
        ),
    50 =>
        array(
            'hasStudents' => true,
            'id' => 11072,
            'pattern' => 'ТМ-{Курс}В',
            'name' => 'ТМ-2В',
            'code' => 'ТМ-2016-В',
            'program'=>32,
            'plan'=>44,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2016-В)',
                    'id' => 11068,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10192,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    51 =>
        array(
            'hasStudents' => true,
            'id' => 9055,
            'pattern' => 'ТМ-{Курс}В',
            'name' => 'ТМ-3В',
            'code' => 'ТМ-2015-В',
            'program'=>32,
            'plan'=>43,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2015-В)',
                    'id' => 10042,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10193,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    52 =>
        array(
            'hasStudents' => true,
            'id' => 9058,
            'pattern' => 'ТМ-{Курс}В',
            'name' => 'ТМ-4В',
            'code' => 'ТМ-2014-В',
            'program'=>32,
            'plan'=>42,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения (ТМ-2014-В)',
                    'id' => 10049,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 2,
                    'termType' => 'Semester',
                    'educationCalendarId' => 10194,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    53 =>
        array(
            'hasStudents' => true,
            'id' => 11091,
            'pattern' => 'СП-{Курс}з',
            'name' => 'СП-1з',
            'code' => 'СП-2017з',
            'program'=>34,
            'plan'=>50,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство(СП-2017-3)',
                    'id' => 12109,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10202,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    54 =>
        array(
            'hasStudents' => true,
            'id' => 9042,
            'pattern' => 'СП-{Курс}з',
            'name' => 'СП-4з',
            'code' => 'СП-2014-З',
            'program'=>34,
            'plan'=>51,
            'educationProgram' =>
                array(
                    'name' => 'Сварочное производство(СП-2014-3)',
                    'id' => 10036,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10186,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    55 =>
        array(
            'hasStudents' => true,
            'id' => 11087,
            'pattern' => 'ТМ-{Курс}з',
            'name' => 'ТМ-1з',
            'code' => 'ТМ-2017-з',
            'program'=>26,
            'plan'=>49,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения(ТМ-2017-З)',
                    'id' => 11100,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 1,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10195,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    56 =>
        array(
            'hasStudents' => true,
            'id' => 11071,
            'pattern' => 'ТМ-{Курс}з',
            'name' => 'ТМ-2З',
            'code' => 'ТМ-2016-З',
            'program'=>26,
            'plan'=>48,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения(ТМ-2016-З)',
                    'id' => 11066,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 2,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10191,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    57 =>
        array(
            'hasStudents' => true,
            'id' => 9039,
            'pattern' => 'ТМ-{Курс}з',
            'name' => 'ТМ-3з',
            'code' => 'ТМ-2015-3',
            'program'=>26,
            'plan'=>47,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения(ТМ-2015-З)',
                    'id' => 10032,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 3,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10189,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    58 =>
        array(
            'hasStudents' => true,
            'id' => 9040,
            'pattern' => 'ТМ-{Курс}з',
            'name' => 'ТМ-4з',
            'code' => 'ТМ-2014-З',
            'program'=>26,
            'plan'=>46,
            'educationProgram' =>
                array(
                    'name' => 'Технология машиностроения(ТМ-2014-З)',
                    'id' => 10035,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10187,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                ),
        ),
    59 =>
        array(
            'hasStudents' => true,
            'id' => 9045,
            'pattern' => 'ТЭ-4з',
            'name' => 'ТЭ-4з',
            'code' => 'ТЭ-2014-З',
            'program'=>35,
            'plan'=>45,
            'educationProgram' =>
                array(
                    'name' => 'Техническая эксплуатация и обслуживание электрического и электромеханического оборудования (по отраслям)[ТЭ-2014-З]',
                    'id' => 11071,
                ),
            'educationPeriod' =>
                array(
                    'yearNumber' => 4,
                    'termNumber' => 1,
                    'termType' => 'Course',
                    'educationCalendarId' => 10187,
                ),
            'curator' =>
                array(
                    'firstName' => 'Юлия',
                    'lastName' => 'Бедченко',
                    'middleName' => 'Анатольевна',
                    'id' => 4,
                )
        )

);