<?php
$empl=array (
    0 => array (
        'decrees' =>
            array (
                0 =>
                    array (
                        'number' => '06-к',
                        'date' => '2013-02-08T00:00:00.0000000',
                        'effectiveDate' => '2013-02-11T00:00:00.0000000',
                        'type' => 'Hire',
                        'position' =>
                            array (
                                'name' => 'Юрисконсульт',
                                'id' => 32,
                            ),
                        'id' => 18151,
                    ),
            ),
        'userProfileId' => 12719,
        'credentials' =>
            array (
                'login' => 'алексеев731',
            ),
        'positions' =>
            array (
                0 =>
                    array (
                        'laborContract' => 'PartTime',
                        'rate' => 0.5,
                        'name' => 'Юрисконсульт',
                        'id' => 32,
                    ),
            ),
        'editable' => true,
        'id' => 6103,
        'educationLevel' => 'HigherMagistracy',
        'hasPedagogicalEducation' => false,
        'workExperience' => 0,
        'pedagogicalExperience' => 0,
        'inn' => '636204245573',
        'retraining' => '',
        'firstName' => 'Юрий',
        'lastName' => 'Алексеев',
        'middleName' => 'Юрьевич',
        'gender' => 'Male',
        'isIndigenousMinority' => false,
        'hasChildrenUnderThreeYears' => false,
        'address' => 'г. Самара, ул. Белорусская, д. 42, кв. 61, индекс 443042',
        'countryId' => 643,
        'birthday' => '1972-01-04T00:00:00.0000000',
        'passport' =>
            array (
                'documentType' => 'RfPassport',
                'series' => '36 16',
                'number' => '305196',
                'issuanceDate' => '2017-01-19T00:00:00.0000000',
                'issued' => 'Отделением УФМС России по Самарской области в Куйбышевском районе',
                'subdivisionCode' => '630-032',
                'birthplace' => 'дер. Воронцовка Канашского р-на Чувашской АССР',
                'registration' => 'г. Самара, ул. Белорусская, д. 42, кв. 61, индекс 443042',
            ),
        'snils' => '13255850755',
        'isEsiaBound' => false,
    ),
    1 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '234',
                            'date' => '1987-08-26T00:00:00.0000000',
                            'effectiveDate' => '1987-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15249,
                        ),
                ),
            'userProfileId' => 11582,
            'credentials' =>
                array (
                    'login' => '496',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6072,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 460306,
            'pedagogicalExperience' => 430517,
            'inn' => '631911718971',
            'firstName' => 'Валентина',
            'lastName' => 'Андропова',
            'middleName' => 'Васильевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443125 г. Самара, ул. Губанова, д. 10, кв. 18',
            'countryId' => 643,
            'birthday' => '1948-03-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 99',
                    'number' => '262749',
                    'issuanceDate' => '2000-11-22T00:00:00.0000000',
                    'issued' => 'отделом внутренних дел №8 Промышленного района г. Самара',
                    'subdivisionCode' => '633-005',
                    'birthplace' => 'с-з им. Фрунзе Больше-Глушицкого р-на Куйбышевской обл.',
                    'registration' => '443125 г. Самара, ул. Губанова, д. 10, кв. 18',
                ),
            'snils' => '01531910004',
            'isEsiaBound' => false,
        ),
    2 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '9-к',
                            'date' => '2010-03-01T00:00:00.0000000',
                            'effectiveDate' => '2010-03-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дворник',
                                    'id' => 1060,
                                ),
                            'id' => 1176,
                        ),
                    1 =>
                        array (
                            'number' => '32-к',
                            'date' => '2016-04-19T00:00:00.0000000',
                            'effectiveDate' => '2016-04-20T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 15492,
                        ),
                ),
            'userProfileId' => 345,
            'credentials' =>
                array (
                    'login' => 'аннина345',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дворник',
                            'id' => 1060,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 56,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631937266575',
            'firstName' => 'Нина',
            'lastName' => 'Аннина',
            'middleName' => 'Георгиевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Георгия Димитрова, д. 117., кв. 37, индекс 443115',
            'countryId' => 643,
            'birthday' => '1945-09-18T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '597014',
                    'issuanceDate' => '2012-05-30T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Кинель Куйбышевской обл.',
                    'registration' => 'г. Самара, ул. Георгия Димитрова, д. 117., кв. 37, индекс 443115',
                ),
            'snils' => '00840490628',
            'isEsiaBound' => false,
        ),
    3 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '187/02-03',
                            'date' => '2015-10-16T00:00:00.0000000',
                            'effectiveDate' => '2015-10-16T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18061,
                        ),
                ),
            'userProfileId' => 12664,
            'credentials' =>
                array (
                    'login' => 'апаликов676',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6095,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'academicDegree' => 'Doctorant',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '635002210583',
            'firstName' => 'Александр',
            'lastName' => 'Апаликов',
            'middleName' => 'Иванович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область. пгт. Усть-Кинельский, ул. Спортивная, д. 15, кв. 36, индекс 446442',
            'countryId' => 643,
            'birthday' => '1956-06-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '780337',
                    'issuanceDate' => '2001-11-09T00:00:00.0000000',
                    'issued' => 'Кинельским ГРОВД Самарской области',
                    'subdivisionCode' => '632-043',
                    'birthplace' => 'с. II-я Филипповка Кинельского р-на Куйбышевской обл.',
                    'registration' => 'Самарская область. пгт. Усть-Кинельский, ул. Спортивная, д. 15, кв. 36, индекс 446442',
                ),
            'snils' => '00968294990',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'Sergeant',
                    'militarySpecialty' => '',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'Кинельский объединенный городской ВК Куйбышевской области',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => '5625216',
                    'reserveCategory' => 'FirstCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => true,
                ),
            'isEsiaBound' => false,
        ),
    4 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '19-к',
                            'date' => '2016-02-15T00:00:00.0000000',
                            'effectiveDate' => '2016-02-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Инженер-программист',
                                    'id' => 2071,
                                ),
                            'id' => 15497,
                        ),
                    1 =>
                        array (
                            'number' => '78-к',
                            'date' => '2016-11-01T00:00:00.0000000',
                            'effectiveDate' => '2016-11-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21287,
                        ),
                ),
            'userProfileId' => 12299,
            'credentials' =>
                array (
                    'login' => 'Апаликов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Инженер-программист',
                            'id' => 2071,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6081,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'academicDegree' => 'Doctorant',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '635002904398',
            'qualificationCourses' => '',
            'firstName' => 'Максим',
            'lastName' => 'Апаликов',
            'middleName' => 'Александрович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, пгт. Усть-Кинельский, ул. Спортивная, д. 15, кв. 36, индекс 446442',
            'countryId' => 643,
            'birthday' => '1978-12-03T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '780335',
                    'issuanceDate' => '2001-11-09T00:00:00.0000000',
                    'issued' => 'Кинельским ГРОВД Самарской области',
                    'subdivisionCode' => '632-043',
                    'birthplace' => 'г. Кинель Самарской обл.',
                    'registration' => 'Самарская область, пгт. Усть-Кинельский, ул. Спортивная, д. 15, кв. 36, индекс 446442',
                ),
            'snils' => '07961420586',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'Lieutenant',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'Кинельский ОГВК Куйбышевской области',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АВ0275914',
                    'reserveCategory' => 'FirstCategory',
                    'militaryComposition' => 'OfficersAndMarshals',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    5 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '131/02-03',
                            'date' => '2007-08-23T00:00:00.0000000',
                            'effectiveDate' => '2007-08-23T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Библиотекарь',
                                    'id' => 2078,
                                ),
                            'id' => 18152,
                        ),
                ),
            'userProfileId' => 12720,
            'credentials' =>
                array (
                    'login' => 'артюшкина732',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Библиотекарь',
                            'id' => 2078,
                        ),
                ),
            'editable' => true,
            'id' => 6104,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631917477715',
            'firstName' => 'Зинаида',
            'lastName' => 'Артюшкина',
            'middleName' => 'Матвеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Карла Маркса, д. 320, кв. 44, индекс 443016',
            'countryId' => 643,
            'birthday' => '1952-02-25T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '639019',
                    'issuanceDate' => '2003-12-01T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, ул. Карла Маркса, д. 320, кв. 44, индекс 443016',
                ),
            'snils' => '02753393963',
            'isEsiaBound' => false,
        ),
    6 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '37-к',
                            'date' => '2014-09-15T00:00:00.0000000',
                            'effectiveDate' => '2014-09-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 40,
                        ),
                ),
            'userProfileId' => 39,
            'credentials' =>
                array (
                    'login' => 'Афанасьева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 39,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '633088613108',
            'firstName' => 'Юлия',
            'lastName' => 'Афанасьева',
            'middleName' => 'Андреевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Промышленности, д. 303, кв. 26, индекс 443083',
            'countryId' => 643,
            'birthday' => '1991-05-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 14',
                    'number' => '896029',
                    'issuanceDate' => '2014-08-07T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Хворостянском районе',
                    'subdivisionCode' => '630-041',
                    'birthplace' => 'г. Самара',
                    'registration' => 'г. Самара, ул. Промышленности, д. 303, кв. 26, индекс 443083',
                ),
            'snils' => '16138234147',
            'isEsiaBound' => false,
        ),
    7 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '24-к',
                            'date' => '2010-07-19T00:00:00.0000000',
                            'effectiveDate' => '2010-07-19T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Водитель',
                                    'id' => 37,
                                ),
                            'id' => 1177,
                        ),
                ),
            'userProfileId' => 346,
            'credentials' =>
                array (
                    'login' => 'баранов346',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Водитель',
                            'id' => 37,
                        ),
                ),
            'editable' => true,
            'id' => 57,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631604531080',
            'firstName' => 'Сергей',
            'lastName' => 'Баранов',
            'middleName' => 'Юрьевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., Октябрьский р-н, г. Самара, Тупой пер., д. 23, кв. 6, индекс 443080',
            'countryId' => 643,
            'birthday' => '1964-04-10T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '050396',
                    'issuanceDate' => '2009-04-21T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Октябрьском районе гор. Самары',
                    'subdivisionCode' => '630-010',
                    'birthplace' => 'с. Борма Кошкинского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., Октябрьский р-н, г. Самара, Тупой пер., д. 23, кв. 6, индекс 443080',
                ),
            'snils' => '01624173931',
            'isEsiaBound' => false,
        ),
    8 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '21-к',
                            'date' => '2016-02-01T00:00:00.0000000',
                            'effectiveDate' => '2016-02-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                                    'id' => 41,
                                ),
                            'id' => 18154,
                        ),
                ),
            'userProfileId' => 12722,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                            'id' => 41,
                        ),
                ),
            'editable' => true,
            'id' => 6106,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631921326821',
            'firstName' => 'Геннадий',
            'lastName' => 'Бедрин',
            'middleName' => 'Игнатьевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. 22 Партсъезда, д. 39, кв. 116, индекс 443058',
            'countryId' => 643,
            'birthday' => '1955-03-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '534770',
                    'issuanceDate' => '2001-07-31T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с. Крепость Кондурча Ч.-Вершинского района Куйбышевской области',
                    'registration' => 'г. Самара, ул. 22 Партсъезда, д. 39, кв. 116, индекс 443058',
                ),
            'snils' => '02097710136',
            'isEsiaBound' => false,
        ),
    9 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '31-к',
                            'date' => '2008-07-14T00:00:00.0000000',
                            'effectiveDate' => '2008-07-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Секретарь учебной части',
                                    'id' => 9,
                                ),
                            'id' => 4,
                        ),
                    1 =>
                        array (
                            'number' => '29-к',
                            'date' => '2012-09-01T00:00:00.0000000',
                            'effectiveDate' => '2012-09-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21289,
                        ),
                ),
            'userProfileId' => 4,
            'credentials' =>
                array (
                    'login' => 'Бедченко',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Секретарь учебной части',
                            'id' => 9,
                        ),
                ),
            'editable' => true,
            'id' => 4,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631232868757',
            'firstName' => 'Юлия',
            'lastName' => 'Бедченко',
            'middleName' => 'Анатольевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Литвинова, д. 322, кв 16, индекс 443109',
            'countryId' => 643,
            'birthday' => '1989-07-16T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '089115',
                    'issuanceDate' => '2009-07-30T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская обл., г. Самара, ул. Литвинова, д. 322, кв 16, индекс 443109',
                ),
            'snils' => '14346476468',
            'isEsiaBound' => false,
        ),
    10 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '47-к',
                            'date' => '2013-09-12T00:00:00.0000000',
                            'effectiveDate' => '2013-09-12T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 36,
                        ),
                ),
            'userProfileId' => 35,
            'credentials' =>
                array (
                    'login' => 'Бекетова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 35,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631903609190',
            'firstName' => 'Галина',
            'lastName' => 'Бекетова',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Московское шоссе, д. 308, кв. 191, индекс 443122',
            'countryId' => 643,
            'birthday' => '1948-03-08T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '277491',
                    'issuanceDate' => '2002-05-10T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел №8 Промышленного района города Самары',
                    'subdivisionCode' => '633-005',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, Московское шоссе, д. 308, кв. 191, индекс 443122',
                ),
            'snils' => '01438796868',
            'isEsiaBound' => false,
        ),
    11 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '7-к',
                            'date' => '2014-05-19T00:00:00.0000000',
                            'effectiveDate' => '2014-05-19T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Ведущий бухгалтер',
                                    'id' => 18,
                                ),
                            'id' => 1198,
                        ),
                    1 =>
                        array (
                            'number' => '87-к',
                            'date' => '2016-12-01T00:00:00.0000000',
                            'effectiveDate' => '2016-12-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Бухгалтер',
                                    'id' => 2081,
                                ),
                            'id' => 21288,
                        ),
                ),
            'userProfileId' => 367,
            'credentials' =>
                array (
                    'login' => 'беспалова367',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Бухгалтер',
                            'id' => 2081,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Ведущий бухгалтер',
                            'id' => 18,
                        ),
                ),
            'editable' => true,
            'id' => 78,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631209568353',
            'firstName' => 'Екатерина',
            'lastName' => 'Беспалова',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Победы, д. 154, кв. 91 индекс 443077',
            'countryId' => 643,
            'birthday' => '1958-06-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '552557',
                    'issuanceDate' => '2003-10-14T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с. Васильевка Мордовского р-на Тамбовской обл.',
                    'registration' => 'г. Самара, ул. Победы, д. 154, кв. 91 индекс 443077',
                ),
            'snils' => '00540237090',
            'isEsiaBound' => false,
        ),
    12 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '31-к',
                            'date' => '2010-08-26T00:00:00.0000000',
                            'effectiveDate' => '2010-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 5,
                        ),
                ),
            'userProfileId' => 5,
            'credentials' =>
                array (
                    'login' => 'Бокарева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 5,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636400916411',
            'firstName' => 'Наталья',
            'lastName' => 'Бокарева',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., пос. Южный, ул. Луговая, д. 1, индекс 446186',
            'countryId' => 643,
            'birthday' => '1985-09-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '800432',
                    'issuanceDate' => '2007-12-05T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большеглушицком районе',
                    'subdivisionCode' => '630-022',
                    'birthplace' => 'с. Преображеновка Ленинского р-на Хабаровского края',
                    'registration' => 'Самарская обл., пос. Южный, ул. Луговая, д. 1, индекс 446186',
                ),
            'snils' => '12683657890',
            'isEsiaBound' => false,
        ),
    13 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '185',
                            'date' => '1985-09-10T00:00:00.0000000',
                            'effectiveDate' => '1985-09-10T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Секретарь руководителя',
                                    'id' => 34,
                                ),
                            'id' => 1224,
                        ),
                ),
            'userProfileId' => 387,
            'credentials' =>
                array (
                    'login' => 'бондарева387',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Секретарь руководителя',
                            'id' => 34,
                        ),
                ),
            'editable' => true,
            'id' => 86,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631907283816',
            'firstName' => 'Галина',
            'lastName' => 'Бондарева',
            'middleName' => 'Емельяновна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый переулок, д. 3, кв. 15, индекс 443052',
            'countryId' => 643,
            'birthday' => '1950-03-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '560643',
                    'issuanceDate' => '2001-10-17T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, Старый переулок, д. 3, кв. 15, индекс 443052',
                ),
            'snils' => '00851803735',
            'isEsiaBound' => false,
        ),
    14 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '79-к',
                            'date' => '2016-11-01T00:00:00.0000000',
                            'effectiveDate' => '2016-11-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18194,
                        ),
                ),
            'userProfileId' => 12794,
            'credentials' =>
                array (
                    'login' => 'бондаренко806',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6141,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '635007995790',
            'firstName' => 'Николай',
            'lastName' => 'Бондаренко',
            'middleName' => 'Викторович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл. гор. Кинель п. Усть-Кинельский, ул. Испытателей, д. 2, корп. 2, кв. 111, индекс 446442',
            'countryId' => 643,
            'birthday' => '1992-03-11T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 11',
                    'number' => '573470',
                    'issuanceDate' => '2012-03-28T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Кинельском районе',
                    'subdivisionCode' => '630-043',
                    'birthplace' => 'гор. Аркалык Тургайской обл. Республика Казахстан',
                    'registration' => 'Самарская обл. гор. Кинель п. Усть-Кинельский, ул. Испытателей, д. 2, корп. 2, кв. 111, индекс 446442',
                ),
            'snils' => '12092214007',
            'militaryRecord' =>
                array (
                    'fitnessForMilitaryService' => 'Unfit',
                    'recruitmentOffice' => 'Отделом ВКСО по г. Кинель и Кинельскому р-ну',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АЕ3035638',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    15 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '47-к',
                            'date' => '2015-09-01T00:00:00.0000000',
                            'effectiveDate' => '2015-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Вахтёр',
                                    'id' => 43,
                                ),
                            'id' => 18155,
                        ),
                ),
            'userProfileId' => 12723,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Вахтёр',
                            'id' => 43,
                        ),
                ),
            'editable' => true,
            'id' => 6107,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631211947630',
            'firstName' => 'Владимир',
            'lastName' => 'Борисов',
            'middleName' => 'Семенович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ташкентская, д. 107, кв. 55, индекс 443106',
            'countryId' => 643,
            'birthday' => '1944-03-21T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '544726',
                    'issuanceDate' => '2002-10-03T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с. Озерное У.-Большерецкого р-на Камчатской обл.',
                    'registration' => 'г. Самара, ул. Ташкентская, д. 107, кв. 55, индекс 443106',
                ),
            'snils' => '10397898497',
            'isEsiaBound' => false,
        ),
    16 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '121/02-03',
                            'date' => '2010-08-17T00:00:00.0000000',
                            'effectiveDate' => '2010-08-17T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Специалист по кадрам',
                                    'id' => 2077,
                                ),
                            'id' => 18156,
                        ),
                ),
            'userProfileId' => 12724,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Специалист по кадрам',
                            'id' => 2077,
                        ),
                ),
            'editable' => true,
            'id' => 6108,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631208828947',
            'firstName' => 'Татьяна',
            'lastName' => 'Бушкина',
            'middleName' => 'Евгеньевна',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443028 г. Самара, Красноглинский р-н, ул. Жалнина, д. 9, кв. 42',
            'countryId' => 643,
            'birthday' => '1977-09-13T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 15',
                    'number' => '110345',
                    'issuanceDate' => '2015-07-23T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по  Самарской области в Кинельском районе',
                    'subdivisionCode' => '630-043',
                    'birthplace' => 'г. Отрадный Куйбышевской области',
                    'registration' => '443028 г. Самара, Красноглинский р-н, ул. Жалнина, д. 9, кв. 42',
                ),
            'snils' => '04716609765',
            'isEsiaBound' => false,
        ),
    17 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '130/02-03',
                            'date' => '2015-08-31T00:00:00.0000000',
                            'effectiveDate' => '2015-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18059,
                        ),
                ),
            'userProfileId' => 12647,
            'credentials' =>
                array (
                    'login' => 'волков659',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6094,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631917238058',
            'firstName' => 'Владимир',
            'lastName' => 'Волков',
            'middleName' => 'Алексеевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Железной дивизии, д. 7, кв. 89, индекс 443052',
            'countryId' => 643,
            'birthday' => '1951-11-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '573680',
                    'issuanceDate' => '2003-11-04T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, ул. Железной дивизии, д. 7, кв. 89, индекс 443052',
                ),
            'snils' => '00840685037',
            'isEsiaBound' => false,
        ),
    18 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '89-к',
                            'date' => '2016-12-14T00:00:00.0000000',
                            'effectiveDate' => '2016-12-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21227,
                        ),
                ),
            'userProfileId' => 14114,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6143,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637203575059',
            'firstName' => 'Антон',
            'lastName' => 'Вязовкин',
            'middleName' => 'Михайлович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., Кинель-Черкасский р-н, с. Кинель-Черкассы, ул. Крестьянская, д. 228, индекс 446350',
            'countryId' => 643,
            'birthday' => '1989-05-28T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '070503',
                    'issuanceDate' => '2009-08-20T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Кинель-Черкасском районе',
                    'subdivisionCode' => '630-015',
                    'birthplace' => 'с. Кинель-Черкассы Кинель-Черкасского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., Кинель-Черкасский р-н, с. Кинель-Черкассы, ул. Крестьянская, д. 228, индекс 446350',
                ),
            'snils' => '14317231728',
            'isEsiaBound' => false,
        ),
    19 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '18-к',
                            'date' => '1994-04-18T00:00:00.0000000',
                            'effectiveDate' => '1994-04-18T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Водитель',
                                    'id' => 37,
                                ),
                            'id' => 1180,
                        ),
                ),
            'userProfileId' => 349,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Водитель',
                            'id' => 37,
                        ),
                ),
            'editable' => true,
            'id' => 60,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631906437559',
            'firstName' => 'Джамал',
            'lastName' => 'Гаджиев',
            'middleName' => 'Ашуралиевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6, ком. 305, 306 ,307, 308, индекс 443052',
            'countryId' => 643,
            'birthday' => '1971-08-09T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '257507',
                    'issuanceDate' => '2016-08-27T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Мисси Агульского р-на Дагестанской АССР',
                    'registration' => 'Самарская обл., г. Самара, Старый пер., д. 6, ком. 305, 306 ,307, 308, индекс 443052',
                ),
            'snils' => '00851800628',
            'isEsiaBound' => false,
        ),
    20 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '152-к',
                            'date' => '2017-10-10T00:00:00.0000000',
                            'effectiveDate' => '2017-10-10T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Плотник',
                                    'id' => 39,
                                ),
                            'id' => 21777,
                        ),
                ),
            'userProfileId' => 130,
            'credentials' =>
                array (
                    'login' => 'Гаджиев',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Плотник',
                            'id' => 39,
                        ),
                ),
            'editable' => true,
            'id' => 6168,
            'educationLevel' => 'Basic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'firstName' => 'Рамазан',
            'lastName' => 'Гаджиев',
            'middleName' => 'Джамалович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'гор. Самара, пер. Старый, дом 6 общ., ком. 306',
            'countryId' => 643,
            'birthday' => '1996-01-30T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 15',
                    'number' => '184343',
                    'issuanceDate' => '2010-04-02T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'гор. Самара, пер. Старый, дом 6 общ., ком. 306',
                ),
            'snils' => '17609978427',
            'isEsiaBound' => false,
        ),
    22 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '58-к',
                            'date' => '2016-08-26T00:00:00.0000000',
                            'effectiveDate' => '2016-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18050,
                        ),
                ),
            'userProfileId' => 12645,
            'credentials' =>
                array (
                    'login' => 'Глистенкова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6082,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631804896634',
            'firstName' => 'Евгения',
            'lastName' => 'Глистенкова',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Георгия Ратнера, д. 1, кв. 153, индекс 443066',
            'countryId' => 643,
            'birthday' => '1972-04-18T00:00:00.0000000',
            'email' => 'glistenkova@bk.ru',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 17',
                    'number' => '351279',
                    'issuanceDate' => '2017-05-04T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор. Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, ул. Георгия Ратнера, д. 1, кв. 153, индекс 443066',
                ),
            'snils' => '00749784986',
            'isEsiaBound' => false,
        ),
    23 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '02-к',
                            'date' => '2012-01-16T00:00:00.0000000',
                            'effectiveDate' => '2012-01-16T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Инженер-электроник',
                                    'id' => 36,
                                ),
                            'id' => 18157,
                        ),
                ),
            'userProfileId' => 12725,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Инженер-электроник',
                            'id' => 36,
                        ),
                ),
            'editable' => true,
            'id' => 6109,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631625331268',
            'firstName' => 'Игорь',
            'lastName' => 'Гольдберг',
            'middleName' => 'Михайлович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, пр. К. Маркса, д. 235, кв. 67, индекс 443090',
            'countryId' => 643,
            'birthday' => '1984-06-28T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '022387',
                    'issuanceDate' => '2004-08-25T00:00:00.0000000',
                    'issued' => 'Октябрьским районным отделом внутренних дел города Самары',
                    'subdivisionCode' => '632-010',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, пр. К. Маркса, д. 235, кв. 67, индекс 443090',
                ),
            'snils' => '12461335731',
            'isEsiaBound' => false,
        ),
    24 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '4-к',
                            'date' => '2017-02-13T00:00:00.0000000',
                            'effectiveDate' => '2017-02-13T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21300,
                        ),
                ),
            'userProfileId' => 15439,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6147,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631229361501',
            'firstName' => 'Елена',
            'lastName' => 'Гордеева',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. пр. Металлургов, д. 81, кв. 5, индекс 443051',
            'countryId' => 643,
            'birthday' => '1985-01-07T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '140627',
                    'issuanceDate' => '2005-03-03T00:00:00.0000000',
                    'issued' => 'Кировским районным управлением внутренних дел города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. пр. Металлургов, д. 81, кв. 5, индекс 443051',
                ),
            'snils' => '13729504470',
            'isEsiaBound' => false,
        ),
    25 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '67-к',
                            'date' => '2016-09-08T00:00:00.0000000',
                            'effectiveDate' => '2016-09-12T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Плотник',
                                    'id' => 39,
                                ),
                            'id' => 18103,
                        ),
                ),
            'userProfileId' => 12692,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Плотник',
                            'id' => 39,
                        ),
                ),
            'editable' => true,
            'id' => 6101,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631907723986',
            'firstName' => 'Александр',
            'lastName' => 'Гошкодеров',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Нагорная, д. 12, кв. 89, индекс 443004',
            'countryId' => 643,
            'birthday' => '1973-06-23T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '491492',
                    'issuanceDate' => '2001-05-31T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, ул. Нагорная, д. 12, кв. 89, индекс 443004',
                ),
            'snils' => '04802998278',
            'isEsiaBound' => false,
        ),
    26 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '21-к',
                            'date' => '2012-08-18T00:00:00.0000000',
                            'effectiveDate' => '2012-08-18T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Старший методист',
                                    'id' => 27,
                                ),
                            'id' => 1196,
                        ),
                    1 =>
                        array (
                            'number' => '2-к',
                            'date' => '2016-01-11T00:00:00.0000000',
                            'effectiveDate' => '2016-01-11T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора по методической работе',
                                    'id' => 2074,
                                ),
                            'id' => 15507,
                        ),
                ),
            'userProfileId' => 365,
            'credentials' =>
                array (
                    'login' => 'Губарь',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора по методической работе',
                            'id' => 2074,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Старший методист',
                            'id' => 27,
                        ),
                ),
            'editable' => true,
            'id' => 76,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631227143730',
            'firstName' => 'Анна',
            'lastName' => 'Губарь',
            'middleName' => 'Сергеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Алма-Атинская, д. 3, кв. 714, индекс 443051',
            'countryId' => 643,
            'birthday' => '1983-01-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '924925',
                    'issuanceDate' => '2008-09-16T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'пос. Горелый Колок Клявлинского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., г. Самара, ул. Алма-Атинская, д. 3, кв. 714, индекс 443051',
                ),
            'snils' => '07714782894',
            'isEsiaBound' => false,
        ),
    27 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '43-к',
                            'date' => '2015-08-21T00:00:00.0000000',
                            'effectiveDate' => '2015-08-21T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Вахтёр',
                                    'id' => 43,
                                ),
                            'id' => 18160,
                        ),
                ),
            'userProfileId' => 12728,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Вахтёр',
                            'id' => 43,
                        ),
                ),
            'editable' => true,
            'id' => 6112,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631916591239',
            'firstName' => 'Валентина',
            'lastName' => 'Гусельникова',
            'middleName' => 'Яковлевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Стационарный пер., д. 11, общ., кв. 311, индекс 443052',
            'countryId' => 643,
            'birthday' => '1948-09-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '861633',
                    'issuanceDate' => '2004-03-03T00:00:00.0000000',
                    'issued' => 'Промышленный РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с. Александровна Бавлинского района Татарской АССР',
                    'registration' => 'г. Самара, Стационарный пер., д. 11, общ., кв. 311, индекс 443052',
                ),
            'snils' => '02490301920',
            'isEsiaBound' => false,
        ),
    28 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '58',
                            'date' => '1967-08-31T00:00:00.0000000',
                            'effectiveDate' => '1967-08-31T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15244,
                        ),
                ),
            'userProfileId' => 11581,
            'credentials' =>
                array (
                    'login' => '70',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6067,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631602869797',
            'firstName' => 'Борис',
            'lastName' => 'Дементьев',
            'middleName' => 'Георгиевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443110 г. Самара, ул. Ново-Садовая д. 42, кв. 411',
            'countryId' => 643,
            'birthday' => '1939-07-09T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '774200',
                    'issuanceDate' => '2002-10-05T00:00:00.0000000',
                    'issued' => 'Октябрьским РОВД г. Самары',
                    'subdivisionCode' => '632-010',
                    'birthplace' => 'с. Авдеевка Донецкой обл.',
                    'registration' => '443110 г. Самара, ул. Ново-Садовая д. 42, кв. 411',
                ),
            'snils' => '01531911309',
            'isEsiaBound' => false,
        ),
    29 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '60-к',
                            'date' => '2016-08-26T00:00:00.0000000',
                            'effectiveDate' => '2016-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Воспитатель',
                                    'id' => 21,
                                ),
                            'id' => 18161,
                        ),
                ),
            'userProfileId' => 11854,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Воспитатель',
                            'id' => 21,
                        ),
                ),
            'editable' => true,
            'id' => 6113,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637604671024',
            'firstName' => 'Дарья',
            'lastName' => 'Демьянова',
            'middleName' => 'Викторовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1990-04-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '224117',
                    'issuanceDate' => '2010-06-08T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Красноярском районе',
                    'subdivisionCode' => '630-045',
                    'birthplace' => 'гор. Душанбе Таджикской ССР',
                    'registration' => 'Самарская область, Красноярский  р-н, с. Большая Каменка, ул. Луговая, д. 9, кв. 2, индекс 446382',
                ),
            'snils' => '16128859585',
            'isEsiaBound' => false,
        ),
    30 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '10',
                            'date' => '2004-08-02T00:00:00.0000000',
                            'effectiveDate' => '2004-08-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик производственных помещений',
                                    'id' => 46,
                                ),
                            'id' => 18162,
                        ),
                ),
            'userProfileId' => 12729,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Уборщик производственных помещений',
                            'id' => 46,
                        ),
                ),
            'editable' => true,
            'id' => 6114,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631506415688',
            'firstName' => 'Геннадий',
            'lastName' => 'Дериглазов',
            'middleName' => 'Сергеевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443010 г. Самара. ул. Куйбышева, д. 147, д. 5',
            'countryId' => 643,
            'birthday' => '1953-03-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 00',
                    'number' => '403411',
                    'issuanceDate' => '2001-04-12T00:00:00.0000000',
                    'issued' => 'Ленинским РОВД города Самары',
                    'subdivisionCode' => '632-011',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => '443010 г. Самара. ул. Куйбышева, д. 147, д. 5',
                ),
            'snils' => '13053026904',
            'isEsiaBound' => false,
        ),
    31 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '65-к',
                            'date' => '1993-09-08T00:00:00.0000000',
                            'effectiveDate' => '1993-09-08T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 6,
                        ),
                ),
            'userProfileId' => 6,
            'credentials' =>
                array (
                    'login' => 'Дормидонтова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631803351980',
            'firstName' => 'Валентина',
            'lastName' => 'Дормидонтова',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Аэродромная. д. 95, кв. 25 индекс 443117',
            'countryId' => 643,
            'birthday' => '1944-11-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '359732',
                    'issuanceDate' => '2002-07-24T00:00:00.0000000',
                    'issued' => 'Советским РУВД гор. Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, ул. Аэродромная. д. 95, кв. 25 индекс 443117',
                ),
            'snils' => '00851804131',
            'isEsiaBound' => false,
        ),
    32 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '30-к',
                            'date' => '2013-07-15T00:00:00.0000000',
                            'effectiveDate' => '2013-07-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заведующая хозяйства',
                                    'id' => 1061,
                                ),
                            'id' => 1197,
                        ),
                ),
            'userProfileId' => 366,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующая хозяйства',
                            'id' => 1061,
                        ),
                ),
            'editable' => true,
            'id' => 77,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631928261203',
            'firstName' => 'Людмила',
            'lastName' => 'Дорошенко',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самары, Стационарный пер., д. 11, кв. 716, индекс 443052',
            'countryId' => 643,
            'birthday' => '1951-06-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '927031',
                    'issuanceDate' => '2008-11-07T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Краснотурьинск Свердловской обл.',
                    'registration' => 'Самарская обл., гор. Самары, Стационарный пер., д. 11, кв. 716, индекс 443052',
                ),
            'snils' => '15337566476',
            'isEsiaBound' => false,
        ),
    33 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '21-к',
                            'date' => '1987-02-26T00:00:00.0000000',
                            'effectiveDate' => '1987-02-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора учебно-воспитательной работе',
                                    'id' => 16,
                                ),
                            'id' => 45,
                        ),
                ),
            'userProfileId' => 44,
            'credentials' =>
                array (
                    'login' => 'Дрожжина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора учебно-воспитательной работе',
                            'id' => 16,
                        ),
                ),
            'editable' => true,
            'id' => 44,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205404280',
            'firstName' => 'Лидия',
            'lastName' => 'Дрожжина',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ст. Загора, д. 285, кв. 67, индекс 443106',
            'countryId' => 643,
            'birthday' => '1958-04-03T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '188746',
                    'issuanceDate' => '2003-06-03T00:00:00.0000000',
                    'issued' => 'Управление внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с. Старокленское Первомайского р-на Тамбовской обл.',
                    'registration' => 'г. Самара, ул. Ст. Загора, д. 285, кв. 67, индекс 443106',
                ),
            'snils' => '00851802632',
            'isEsiaBound' => false,
        ),
    34 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '23-к',
                            'date' => '2012-08-27T00:00:00.0000000',
                            'effectiveDate' => '2012-08-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 8,
                        ),
                ),
            'userProfileId' => 8,
            'credentials' =>
                array (
                    'login' => 'Дудов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 8,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631501999670',
            'firstName' => 'Андрей',
            'lastName' => 'Дудов',
            'middleName' => 'Николаевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Г. Димитрова, д. 112, кв. 40, индекс 443115',
            'countryId' => 643,
            'birthday' => '1973-03-11T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '804343',
                    'issuanceDate' => '2001-10-27T00:00:00.0000000',
                    'issued' => 'Ленинским РОВД города Самары',
                    'subdivisionCode' => '632-011',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Г. Димитрова, д. 112, кв. 40, индекс 443115',
                ),
            'snils' => '08919611510',
            'isEsiaBound' => false,
        ),
    35 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '27-л',
                            'date' => '1985-04-02T00:00:00.0000000',
                            'effectiveDate' => '1985-04-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Вахтёр',
                                    'id' => 43,
                                ),
                            'id' => 1181,
                        ),
                ),
            'userProfileId' => 350,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Вахтёр',
                            'id' => 43,
                        ),
                ),
            'editable' => true,
            'id' => 61,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631912716769',
            'firstName' => 'Валентина',
            'lastName' => 'Ермолаева',
            'middleName' => 'Петровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Стационарный пер., д. 3, кв. 41, 44, индекс 443052',
            'countryId' => 643,
            'birthday' => '1956-11-12T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '729757',
                    'issuanceDate' => '2002-02-07T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с. Ст. Максимкино Кошкинского района Куйбышевской обл.',
                    'registration' => 'г. Самара, Стационарный пер., д. 3, кв. 41, 44, индекс 443052',
                ),
            'snils' => '02039750627',
            'isEsiaBound' => false,
        ),
    36 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '138/02-03',
                            'date' => '2007-08-30T00:00:00.0000000',
                            'effectiveDate' => '2007-08-30T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15264,
                        ),
                ),
            'userProfileId' => 11589,
            'credentials' =>
                array (
                    'login' => '44',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6069,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631608127103',
            'firstName' => 'Диляра',
            'lastName' => 'Ещенко',
            'middleName' => 'Рашидовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ерошевского, д. 18, кв. 12, индекс 443086',
            'countryId' => 643,
            'birthday' => '1983-09-30T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '040310',
                    'issuanceDate' => '2009-03-18T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Ерошевского, д. 18, кв. 12, индекс 443086',
                ),
            'snils' => '11631656335',
            'isEsiaBound' => false,
        ),
    37 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '70-к',
                            'date' => '2016-10-03T00:00:00.0000000',
                            'effectiveDate' => '2016-10-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                                    'id' => 41,
                                ),
                            'id' => 18165,
                        ),
                ),
            'userProfileId' => 12732,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                            'id' => 41,
                        ),
                ),
            'editable' => true,
            'id' => 6117,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631604866008',
            'firstName' => 'Владимир',
            'lastName' => 'Заборников',
            'middleName' => 'Анатольевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самары, ул. Ялтинская, д. 9, кв. 21, индекс 443045',
            'countryId' => 643,
            'birthday' => '1958-07-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '920227',
                    'issuanceDate' => '2004-05-11T00:00:00.0000000',
                    'issued' => 'Октябрьским РОВД города Самары',
                    'subdivisionCode' => '632-010',
                    'birthplace' => 'с. Бор-Игар Клявлинского р-на Куйбышевской обл.',
                    'registration' => 'г. Самары, ул. Ялтинская, д. 9, кв. 21, индекс 443045',
                ),
            'snils' => '10971323850',
            'isEsiaBound' => false,
        ),
    38 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '20-к',
                            'date' => '2015-06-02T00:00:00.0000000',
                            'effectiveDate' => '2015-06-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дежурный по общежитию',
                                    'id' => 52,
                                ),
                            'id' => 18164,
                        ),
                ),
            'userProfileId' => 12731,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дежурный по общежитию',
                            'id' => 52,
                        ),
                ),
            'editable' => true,
            'id' => 6116,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631201521681',
            'firstName' => 'Римма',
            'lastName' => 'Землянская',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Днепровский пр., д. 5, кв. 45, индекс 443109',
            'countryId' => 643,
            'birthday' => '1947-09-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '824726',
                    'issuanceDate' => '2004-03-16T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Ташкент',
                    'registration' => 'г. Самара, Днепровский пр., д. 5, кв. 45, индекс 443109',
                ),
            'snils' => '00852080928',
            'isEsiaBound' => false,
        ),
    39 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => 'Б/Н',
                            'date' => '2008-10-01T00:00:00.0000000',
                            'effectiveDate' => '2008-10-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Администратор АСУ РСО',
                                    'id' => 8,
                                ),
                            'id' => 1,
                        ),
                    1 =>
                        array (
                            'number' => '56-к',
                            'date' => '2008-10-01T00:00:00.0000000',
                            'effectiveDate' => '2008-10-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18128,
                        ),
                    2 =>
                        array (
                            'number' => '55',
                            'date' => '2015-05-06T00:00:00.0000000',
                            'effectiveDate' => '2015-05-06T00:00:00.0000000',
                            'type' => 'Dismiss',
                            'position' =>
                                array (
                                    'name' => 'Администратор АСУ РСО',
                                    'id' => 8,
                                ),
                            'id' => 20211,
                        ),
                ),
            'userProfileId' => 1,
            'credentials' =>
                array (
                    'login' => 'Зуева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 1,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631934228282',
            'firstName' => 'Анна',
            'lastName' => 'Зуева',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Заводское шоссе 67а, кв. 3 , индекс 443052',
            'countryId' => 643,
            'birthday' => '1987-07-26T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '3607',
                    'number' => '745321',
                    'issuanceDate' => '2007-08-18T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Кировском районе гор. Самара',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская обл., г. Самара, Заводское шоссе 67а, кв. 3 , индекс 443052',
                ),
            'snils' => '12560341324',
            'isEsiaBound' => true,
        ),
    40 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '130',
                            'date' => '2000-05-29T00:00:00.0000000',
                            'effectiveDate' => '2000-05-29T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15252,
                        ),
                ),
            'userProfileId' => 11587,
            'credentials' =>
                array (
                    'login' => '95',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6061,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631908902402',
            'firstName' => 'Галина',
            'lastName' => 'Инжеватова',
            'middleName' => 'Владимировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ново-Вокзальная, д. 146, кв. 163, индекс 443111',
            'countryId' => 643,
            'birthday' => '1956-06-15T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '080371',
                    'issuanceDate' => '2002-03-13T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, ул. Ново-Вокзальная, д. 146, кв. 163, индекс 443111',
                ),
            'snils' => '01898391603',
            'isEsiaBound' => false,
        ),
    41 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '50-к',
                            'date' => '2003-06-02T00:00:00.0000000',
                            'effectiveDate' => '2003-06-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дежурный по общежитию',
                                    'id' => 52,
                                ),
                            'id' => 1225,
                        ),
                ),
            'userProfileId' => 388,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дежурный по общежитию',
                            'id' => 52,
                        ),
                ),
            'editable' => true,
            'id' => 87,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636501581803',
            'firstName' => 'Зухра',
            'lastName' => 'Иноятова',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, Заводское шоссе, д. 40, кв. 48, индекс 443052',
            'countryId' => 643,
            'birthday' => '1962-05-06T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '740198',
                    'issuanceDate' => '2007-08-09T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'дер. Имелеевка Большечерниговского р-на Куйбышевской обл. РСФСР',
                    'registration' => 'Самарская область, г. Самара, Заводское шоссе, д. 40, кв. 48, индекс 443052',
                ),
            'snils' => '02078671347',
            'isEsiaBound' => false,
        ),
    42 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '125-к',
                            'date' => '2017-07-03T00:00:00.0000000',
                            'effectiveDate' => '2017-07-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Педагог-психолог',
                                    'id' => 30,
                                ),
                            'id' => 21376,
                        ),
                ),
            'userProfileId' => 15719,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Педагог-психолог',
                            'id' => 30,
                        ),
                ),
            'editable' => true,
            'id' => 6157,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631927956805',
            'firstName' => 'Амина',
            'lastName' => 'Исхакова',
            'middleName' => 'Рашидовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443122 г. Самара, ул. Силина, д. 17, кв. 124',
            'countryId' => 643,
            'birthday' => '1995-05-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 17',
                    'number' => '322610',
                    'issuanceDate' => '2017-03-17T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Самара',
                    'registration' => '443122 г. Самара, ул. Силина, д. 17, кв. 124',
                ),
            'snils' => '17109491263',
            'isEsiaBound' => false,
        ),
    43 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '98/02-03',
                            'date' => '2005-08-31T00:00:00.0000000',
                            'effectiveDate' => '2005-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заведующий отделением',
                                    'id' => 2064,
                                ),
                            'id' => 15261,
                        ),
                ),
            'userProfileId' => 11606,
            'credentials' =>
                array (
                    'login' => 'Ищенко',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующий отделением',
                            'id' => 2064,
                        ),
                ),
            'editable' => true,
            'id' => 6054,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631605023628',
            'firstName' => 'Татьяна',
            'lastName' => 'Ищенко',
            'middleName' => 'Алексеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ветлянская, д. 40, кв. 4, индекс 443052',
            'countryId' => 643,
            'birthday' => '1958-02-15T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '929735',
                    'issuanceDate' => '2003-03-04T00:00:00.0000000',
                    'issued' => 'Самарским РОВД города Самары',
                    'subdivisionCode' => '632-001',
                    'birthplace' => 'с. Благодатовка Б.-Черниговского р-на Куйбышевской обл.',
                    'registration' => 'г. Самара, ул. Ветлянская, д. 40, кв. 4, индекс 443052',
                ),
            'snils' => '01439330322',
            'isEsiaBound' => false,
        ),
    44 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '320',
                            'date' => '2002-09-02T00:00:00.0000000',
                            'effectiveDate' => '2002-09-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15258,
                        ),
                ),
            'userProfileId' => 11602,
            'credentials' =>
                array (
                    'login' => '54',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6058,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631605064303',
            'firstName' => 'Розалия',
            'lastName' => 'Кадацкая',
            'middleName' => 'Бариевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Победы, д. 146/1, кв. 72, индекс 443077',
            'countryId' => 643,
            'birthday' => '1956-04-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '710505',
                    'issuanceDate' => '2001-10-10T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'г. Куляб Таджикской ССР',
                    'registration' => 'г. Самара, ул. Победы, д. 146/1, кв. 72, индекс 443077',
                ),
            'snils' => '01628425134',
            'isEsiaBound' => false,
        ),
    45 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '35',
                            'date' => '1974-09-02T00:00:00.0000000',
                            'effectiveDate' => '1974-09-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Старший мастер',
                                    'id' => 24,
                                ),
                            'id' => 44,
                        ),
                ),
            'userProfileId' => 43,
            'credentials' =>
                array (
                    'login' => 'Калашников',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Старший мастер',
                            'id' => 24,
                        ),
                ),
            'editable' => true,
            'id' => 43,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631913274312',
            'firstName' => 'Владимир',
            'lastName' => 'Калашников',
            'middleName' => 'Николаевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Стара Загора, д. 88, кв. 145, индекс 443081',
            'countryId' => 643,
            'birthday' => '1952-04-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '887850',
                    'issuanceDate' => '2008-05-21T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Тамбов',
                    'registration' => 'Самарская обл., г. Самара, ул. Стара Загора, д. 88, кв. 145, индекс 443081',
                ),
            'snils' => '02039748741',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'SeniorLieutenant',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'ВКР Промышленного р-на',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'ДЕ228438',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    46 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '40-к',
                            'date' => '2009-09-01T00:00:00.0000000',
                            'effectiveDate' => '2009-09-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 18166,
                        ),
                ),
            'userProfileId' => 12733,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6118,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '362703756506',
            'firstName' => 'Вера',
            'lastName' => 'Карпенко',
            'middleName' => 'Тихоновна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1953-01-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '20 04',
                    'number' => '147105',
                    'issuanceDate' => '2003-12-16T00:00:00.0000000',
                    'issued' => 'Россошанским ГРОВД Воронежской обл.',
                    'subdivisionCode' => '362-035',
                    'birthplace' => 'гор. Самараканд Респ. Узбекистан',
                    'registration' => 'Воронежская область, г. Россошь, ул. Фурманова, д. 14, кв. 48',
                ),
            'snils' => '00851800931',
            'isEsiaBound' => false,
        ),
    47 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '75-к',
                            'date' => '1987-07-23T00:00:00.0000000',
                            'effectiveDate' => '1987-07-24T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Техник-технолог',
                                    'id' => 31,
                                ),
                            'id' => 1199,
                        ),
                ),
            'userProfileId' => 368,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Техник-технолог',
                            'id' => 31,
                        ),
                ),
            'editable' => true,
            'id' => 79,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205406369',
            'firstName' => 'Надежда',
            'lastName' => 'Карягина',
            'middleName' => 'Митрофановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, пр. Кирова, д 261, в. 149, индекс 443091',
            'countryId' => 643,
            'birthday' => '1953-09-21T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '994813',
                    'issuanceDate' => '2003-04-04T00:00:00.0000000',
                    'issued' => 'Управление внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Днепропетровск',
                    'registration' => 'г. Самара, пр. Кирова, д 261, в. 149, индекс 443091',
                ),
            'snils' => '00851805638',
            'isEsiaBound' => false,
        ),
    48 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '49-к',
                            'date' => '2016-06-16T00:00:00.0000000',
                            'effectiveDate' => '2016-06-16T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Вахтёр',
                                    'id' => 43,
                                ),
                            'id' => 18167,
                        ),
                ),
            'userProfileId' => 12734,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Вахтёр',
                            'id' => 43,
                        ),
                ),
            'editable' => true,
            'id' => 6119,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'firstName' => 'Юрий',
            'lastName' => 'Катин',
            'middleName' => 'Алексеевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, пр. К. Маркса, д. 510, кв. 87, индекс 443106',
            'countryId' => 643,
            'birthday' => '1961-02-07T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 06',
                    'number' => '614106',
                    'issuanceDate' => '2007-02-27T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, пр. К. Маркса, д. 510, кв. 87, индекс 443106',
                ),
            'snils' => '02966330065',
            'isEsiaBound' => false,
        ),
    49 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '34-к',
                            'date' => '2001-06-18T00:00:00.0000000',
                            'effectiveDate' => '2001-06-19T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Ведущий бухгалтер',
                                    'id' => 18,
                                ),
                            'id' => 1200,
                        ),
                ),
            'userProfileId' => 369,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Ведущий бухгалтер',
                            'id' => 18,
                        ),
                ),
            'editable' => true,
            'id' => 80,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631211206590',
            'firstName' => 'Ирина',
            'lastName' => 'Катина',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, пр. К. Маркса, д. 510, кв. 87, индекс 443106',
            'countryId' => 643,
            'birthday' => '1962-06-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 06',
                    'number' => '676277',
                    'issuanceDate' => '2007-07-11T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'п. Алексеевка Кинельского района',
                    'registration' => 'г. Самара, пр. К. Маркса, д. 510, кв. 87, индекс 443106',
                ),
            'snils' => '02966108670',
            'isEsiaBound' => false,
        ),
    50 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '145-к',
                            'date' => '2017-10-02T00:00:00.0000000',
                            'effectiveDate' => '2017-10-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 21775,
                        ),
                ),
            'userProfileId' => 16095,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6166,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631921213384',
            'firstName' => 'Татьяна',
            'lastName' => 'Киреева',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Стационарный переулок,  д. 11, кв. 210, 211   индекс 443052',
            'countryId' => 643,
            'birthday' => '1974-07-02T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 13',
                    'number' => '817168',
                    'issuanceDate' => '2013-11-23T00:00:00.0000000',
                    'issued' => 'отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, Стационарный переулок,  д. 11, кв. 210, 211   индекс 443052',
                ),
            'snils' => '05991742307',
            'isEsiaBound' => false,
        ),
    51 =>
        array (
            'photo' => '/services/people/photos/12713?636564623845122506',
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '41-к',
                            'date' => '2015-08-10T00:00:00.0000000',
                            'effectiveDate' => '2015-08-10T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Секретарь учебной части',
                                    'id' => 9,
                                ),
                            'id' => 18127,
                        ),
                ),
            'userProfileId' => 12713,
            'credentials' =>
                array (
                    'login' => 'Кирсанова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Секретарь учебной части',
                            'id' => 9,
                        ),
                ),
            'editable' => true,
            'id' => 6102,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631226481913',
            'awards' => 'Благодарственное письмо Губернатора Самарской области-2016 год, Благодарственное письмо Самарского управления-2017 год',
            'firstName' => 'Татьяна',
            'lastName' => 'Кирсанова',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. А-Атинская, д. 144, кв. 38, индекс 443106',
            'countryId' => 643,
            'birthday' => '1982-10-23T00:00:00.0000000',
            'phone' => '89276874332',
            'email' => 'funtik1982@list.ru',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '3604',
                    'number' => '259242',
                    'issuanceDate' => '2003-06-23T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. А-Атинская, д. 144, кв. 38, индекс 443106',
                ),
            'snils' => '07674371902',
            'isEsiaBound' => true,
        ),
    52 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '42-к',
                            'date' => '2007-08-27T00:00:00.0000000',
                            'effectiveDate' => '2007-08-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 13,
                        ),
                ),
            'userProfileId' => 12,
            'credentials' =>
                array (
                    'login' => 'Китаева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 12,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637400687731',
            'firstName' => 'Александра',
            'lastName' => 'Китаева',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1983-07-20T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '874863',
                    'issuanceDate' => '2004-04-14T00:00:00.0000000',
                    'issued' => 'Отделом Внутренних дел Кошкинского района Самарской области',
                    'subdivisionCode' => '632-013',
                    'birthplace' => 'с. Ст. Кармала Кошкинского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., с. Ст. Кармала, ул. Центральная, д. 120, индекс 446812',
                ),
            'snils' => '07614225552',
            'isEsiaBound' => false,
        ),
    53 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '123',
                            'date' => '1986-05-14T00:00:00.0000000',
                            'effectiveDate' => '1986-05-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Директор',
                                    'id' => 49,
                                ),
                            'id' => 43,
                        ),
                ),
            'userProfileId' => 42,
            'credentials' =>
                array (
                    'login' => 'Климов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Директор',
                            'id' => 49,
                        ),
                ),
            'editable' => true,
            'id' => 42,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631808374702',
            'firstName' => 'Валерий',
            'lastName' => 'Климов',
            'middleName' => 'Федорович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. 22 Партсъезда, д. 56, кв. 174, индекс 443066',
            'countryId' => 643,
            'birthday' => '1950-04-07T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '859515',
                    'issuanceDate' => '2003-02-01T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Советского района города Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'гор. Алма-Ата Каз ССР',
                    'registration' => 'г. Самара, ул. 22 Партсъезда, д. 56, кв. 174, индекс 443066',
                ),
            'snils' => '00851803634',
            'isEsiaBound' => false,
        ),
    54 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '153-к',
                            'date' => '2017-10-13T00:00:00.0000000',
                            'effectiveDate' => '2017-10-13T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21778,
                        ),
                ),
            'userProfileId' => 16097,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6169,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631915946520',
            'firstName' => 'Татьяна',
            'lastName' => 'Климова',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'индекс	443114,	г. Самара, ул. Г. Димитрова,  д. 68, кв. 21',
            'countryId' => 643,
            'birthday' => '1960-02-13T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '310493',
                    'issuanceDate' => '2005-08-16T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'индекс	443114,	г. Самара, ул. Г. Димитрова,  д. 68, кв. 21',
                ),
            'snils' => '00840525015',
            'isEsiaBound' => false,
        ),
    55 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '92',
                            'date' => '1970-08-28T00:00:00.0000000',
                            'effectiveDate' => '1970-08-28T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Руководитель физического воспитания',
                                    'id' => 23,
                                ),
                            'id' => 11,
                        ),
                ),
            'userProfileId' => 10,
            'credentials' =>
                array (
                    'login' => 'Козлов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Руководитель физического воспитания',
                            'id' => 23,
                        ),
                ),
            'editable' => true,
            'id' => 10,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631808374879',
            'firstName' => 'Владимир',
            'lastName' => 'Козлов',
            'middleName' => 'Васильевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Аэродромная д. 70, кв. 20, индекс 443074',
            'countryId' => 643,
            'birthday' => '1947-06-30T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '785245',
                    'issuanceDate' => '2001-09-12T00:00:00.0000000',
                    'issued' => 'Советским РУВД гор. Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'город Боровичи Новгородской области',
                    'registration' => 'г. Самара, ул. Аэродромная д. 70, кв. 20, индекс 443074',
                ),
            'snils' => '00851805840',
            'isEsiaBound' => false,
        ),
    56 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '50-к',
                            'date' => '2006-08-28T00:00:00.0000000',
                            'effectiveDate' => '2006-08-28T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 24,
                        ),
                ),
            'userProfileId' => 23,
            'credentials' =>
                array (
                    'login' => 'Колмакова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 23,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631901434177',
            'firstName' => 'Татьяна',
            'lastName' => 'Колмакова',
            'middleName' => 'Андреевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Силина, д. 6, кв. 81, индекс 443115',
            'countryId' => 643,
            'birthday' => '1954-10-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '202334',
                    'issuanceDate' => '2010-02-16T00:00:00.0000000',
                    'issued' => 'Отделом УФМС Росси по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'пос. Резиденция Ульчского р-на Хабаровского края',
                    'registration' => 'Самарская обл., г. Самара, ул. Силина, д. 6, кв. 81, индекс 443115',
                ),
            'snils' => '01890403140',
            'isEsiaBound' => false,
        ),
    57 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '80-к',
                            'date' => '2016-11-01T00:00:00.0000000',
                            'effectiveDate' => '2016-11-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18195,
                        ),
                ),
            'userProfileId' => 12795,
            'credentials' =>
                array (
                    'login' => 'комшин807',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6142,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631919628670',
            'firstName' => 'Виталий',
            'lastName' => 'Комшин',
            'middleName' => 'Валериевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ставропольская, д. 163, кв. 27, индекс 443105',
            'countryId' => 643,
            'birthday' => '1975-12-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '602090',
                    'issuanceDate' => '2001-03-03T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'г. Самара, ул. Ставропольская, д. 163, кв. 27, индекс 443105',
                ),
            'snils' => '11476366765',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'CommonSoldier',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'ВК Кировского р-на г. Самара',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АВ0964298',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    58 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '32-к',
                            'date' => '2007-07-13T00:00:00.0000000',
                            'effectiveDate' => '2007-07-16T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Механик',
                                    'id' => 33,
                                ),
                            'id' => 1201,
                        ),
                ),
            'userProfileId' => 370,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Механик',
                            'id' => 33,
                        ),
                ),
            'editable' => true,
            'id' => 81,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631901498100',
            'firstName' => 'Виктор',
            'lastName' => 'Кондрашев',
            'middleName' => 'Анатольевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. В. Фадеева, д. 64, кв. 103, индекс 443111',
            'countryId' => 643,
            'birthday' => '1955-03-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '494401',
                    'issuanceDate' => '2001-07-04T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Ишинбай БАССР',
                    'registration' => 'г. Самара, ул. В. Фадеева, д. 64, кв. 103, индекс 443111',
                ),
            'snils' => '01535489649',
            'isEsiaBound' => false,
        ),
    59 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '24-к',
                            'date' => '2013-06-14T00:00:00.0000000',
                            'effectiveDate' => '2013-06-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора по АХЧ',
                                    'id' => 26,
                                ),
                            'id' => 46,
                        ),
                ),
            'userProfileId' => 45,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора по АХЧ',
                            'id' => 26,
                        ),
                ),
            'editable' => true,
            'id' => 45,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631704197476',
            'firstName' => 'Мария',
            'lastName' => 'Коньшина',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Максима Горького, д. 44, кв. 3, индекс 443099',
            'countryId' => 643,
            'birthday' => '1985-04-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '145583',
                    'issuanceDate' => '2005-05-27T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Самарского района города Самары',
                    'subdivisionCode' => '632-001',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Максима Горького, д. 44, кв. 3, индекс 443099',
                ),
            'snils' => '13006221180',
            'isEsiaBound' => false,
        ),
    60 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '88-к',
                            'date' => '2016-12-01T00:00:00.0000000',
                            'effectiveDate' => '2016-12-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 18169,
                        ),
                ),
            'userProfileId' => 12735,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6120,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631927119300',
            'firstName' => 'Сергей',
            'lastName' => 'Копаев',
            'middleName' => 'Евгеньевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Стационарный пер., д. 3, кв. 11, 443052',
            'countryId' => 643,
            'birthday' => '1984-03-20T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 14',
                    'number' => '974971',
                    'issuanceDate' => '2014-10-24T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Новокуйбышевск Куйбышевской обл.',
                    'registration' => 'Самарская обл., г. Самара, Стационарный пер., д. 3, кв. 11, 443052',
                ),
            'snils' => '11492961367',
            'isEsiaBound' => false,
        ),
    61 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '21-к',
                            'date' => '2001-04-04T00:00:00.0000000',
                            'effectiveDate' => '2001-04-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 1184,
                        ),
                ),
            'userProfileId' => 353,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 64,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631907237538',
            'firstName' => 'Нина',
            'lastName' => 'Копаева',
            'middleName' => 'Петровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Стационарный пер-к, д. 3, кв. 18, 21, индекс 443052',
            'countryId' => 643,
            'birthday' => '1953-09-25T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '690689',
                    'issuanceDate' => '2002-11-23T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Новокуйбышевск Куйбышевской области',
                    'registration' => 'г. Самара, Стационарный пер-к, д. 3, кв. 18, 21, индекс 443052',
                ),
            'snils' => '02039748539',
            'isEsiaBound' => false,
        ),
    62 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '202/02-03',
                            'date' => '2012-10-15T00:00:00.0000000',
                            'effectiveDate' => '2012-10-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15273,
                        ),
                ),
            'userProfileId' => 11593,
            'credentials' =>
                array (
                    'login' => '781',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6060,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631181571057',
            'firstName' => 'Надежда',
            'lastName' => 'Котелкина',
            'middleName' => 'Евгеньевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. К. Маркса, д. 37, кв. 33, индекс 443082',
            'countryId' => 643,
            'birthday' => '1955-04-28T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '137957',
                    'issuanceDate' => '2002-04-05T00:00:00.0000000',
                    'issued' => 'Железнодорожным РОВД города Самары',
                    'subdivisionCode' => '632-003',
                    'birthplace' => 'город Ташкент',
                    'registration' => 'г. Самара, ул. К. Маркса, д. 37, кв. 33, индекс 443082',
                ),
            'snils' => '01531912412',
            'isEsiaBound' => false,
        ),
    63 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '95-к',
                            'date' => '1987-08-24T00:00:00.0000000',
                            'effectiveDate' => '1987-08-24T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 14,
                        ),
                ),
            'userProfileId' => 13,
            'credentials' =>
                array (
                    'login' => 'Котлярова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 13,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631702415500',
            'firstName' => 'Ирина',
            'lastName' => 'Котлярова',
            'middleName' => 'Юрьевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Молодогвардейская, д. 9, кв. 28, индекс 443099',
            'countryId' => 643,
            'birthday' => '1966-06-08T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 11',
                    'number' => '460326',
                    'issuanceDate' => '2011-07-13T00:00:00.0000000',
                    'issued' => 'Отделение УФМС России по Самарской области в Самарском районе гор. Самары',
                    'subdivisionCode' => '630-001',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'Самарская обл., гор. Самара, ул. Молодогвардейская, д. 9, кв. 28, индекс 443099',
                ),
            'snils' => '00851805537',
            'isEsiaBound' => false,
        ),
    64 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '4-к',
                            'date' => '2016-01-15T00:00:00.0000000',
                            'effectiveDate' => '2016-01-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дежурный по общежитию',
                                    'id' => 52,
                                ),
                            'id' => 1173,
                        ),
                ),
            'userProfileId' => 342,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дежурный по общежитию',
                            'id' => 52,
                        ),
                ),
            'editable' => true,
            'id' => 53,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637720211363',
            'firstName' => 'Людмила',
            'lastName' => 'Краева',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, с. Языково, ул. Кооперативная, д. 45, индекс 446688',
            'countryId' => 643,
            'birthday' => '1964-01-11T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 10',
                    'number' => '372823',
                    'issuanceDate' => '2010-12-25T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Борском районе',
                    'subdivisionCode' => '630-025',
                    'birthplace' => 'гор. Фрунзе Республики Киргизия',
                    'registration' => 'Самарская область, с. Языково, ул. Кооперативная, д. 45, индекс 446688',
                ),
            'snils' => '12691451361',
            'isEsiaBound' => false,
        ),
    65 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '11-к',
                            'date' => '2006-03-01T00:00:00.0000000',
                            'effectiveDate' => '2006-03-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 23,
                        ),
                ),
            'userProfileId' => 22,
            'credentials' =>
                array (
                    'login' => 'КраснюкА',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 22,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '638102889903',
            'firstName' => 'Андрей',
            'lastName' => 'Краснюк',
            'middleName' => 'Петрович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., кв. 314. индекс 443052',
            'countryId' => 643,
            'birthday' => '1958-04-10T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '321834',
                    'issuanceDate' => '2003-07-11T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Сергиевского района Самарской области',
                    'subdivisionCode' => '632-045',
                    'birthplace' => 'г. Сулюкта Ошской обл.',
                    'registration' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., кв. 314. индекс 443052',
                ),
            'snils' => '02351931220',
            'isEsiaBound' => false,
        ),
    66 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '29-к',
                            'date' => '2004-08-27T00:00:00.0000000',
                            'effectiveDate' => '2004-08-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 12,
                        ),
                ),
            'userProfileId' => 11,
            'credentials' =>
                array (
                    'login' => 'Краснюк',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 11,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631901769102',
            'firstName' => 'Светлана',
            'lastName' => 'Краснюк',
            'middleName' => 'Басировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Старый пер., д. 6, кв. 314, индекс 443052',
            'countryId' => 643,
            'birthday' => '1965-10-18T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 11',
                    'number' => '451982',
                    'issuanceDate' => '2011-09-02T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'пос. Колхозабад Колхозабадского р-на Тад. ССР',
                    'registration' => 'г. Самара, ул. Старый пер., д. 6, кв. 314, индекс 443052',
                ),
            'snils' => '01370498938',
            'isEsiaBound' => false,
        ),
    67 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '260',
                            'date' => '1985-11-03T00:00:00.0000000',
                            'effectiveDate' => '1985-11-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора по учебной работе',
                                    'id' => 13,
                                ),
                            'id' => 2,
                        ),
                ),
            'userProfileId' => 2,
            'credentials' =>
                array (
                    'login' => 'Кривчун',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора по учебной работе',
                            'id' => 13,
                        ),
                ),
            'editable' => true,
            'id' => 2,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631907284873',
            'firstName' => 'Наталья',
            'lastName' => 'Кривчун',
            'middleName' => 'Васильевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарской обл., гор. Самара, Днепровский проезд, д. 7, кв. 46, индекс 443109',
            'countryId' => 643,
            'birthday' => '1963-03-10T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '844162',
                    'issuanceDate' => '2008-03-20T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Уральск Казахской ССР',
                    'registration' => 'Самарской обл., гор. Самара, Днепровский проезд, д. 7, кв. 46, индекс 443109',
                ),
            'snils' => '00851801327',
            'isEsiaBound' => false,
        ),
    68 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '10-к',
                            'date' => '2015-03-04T00:00:00.0000000',
                            'effectiveDate' => '2015-03-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Главный бухгалтер',
                                    'id' => 19,
                                ),
                            'id' => 1170,
                        ),
                ),
            'userProfileId' => 339,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Главный бухгалтер',
                            'id' => 19,
                        ),
                ),
            'editable' => true,
            'id' => 50,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631913070728',
            'firstName' => 'Анастасия',
            'lastName' => 'Кузуб',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, Волжский район, пгт. Смышляевка, д. 26, кв. 61, 443548',
            'countryId' => 643,
            'birthday' => '1986-05-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 016',
                    'number' => '256819',
                    'issuanceDate' => '2016-08-10T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская область, Волжский район, пгт. Смышляевка, д. 26, кв. 61, 443548',
                ),
            'snils' => '12952300544',
            'isEsiaBound' => false,
        ),
    69 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '134-к',
                            'date' => '2017-09-04T00:00:00.0000000',
                            'effectiveDate' => '2017-09-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сурдопереводчик',
                                    'id' => 2087,
                                ),
                            'id' => 21690,
                        ),
                ),
            'userProfileId' => 16012,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сурдопереводчик',
                            'id' => 2087,
                        ),
                ),
            'editable' => true,
            'id' => 6159,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '632517017747',
            'firstName' => 'Антон',
            'lastName' => 'Куприянов',
            'middleName' => 'Геннадьевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443052    г. Самара, Старый переулок, д. 6., общ.',
            'countryId' => 643,
            'birthday' => '1986-12-08T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 06',
                    'number' => '602481',
                    'issuanceDate' => '2007-01-26T00:00:00.0000000',
                    'issued' => 'Управление внутренних дел города Сызрани Самарской области',
                    'subdivisionCode' => '632-007',
                    'birthplace' => 'г. Сызрань Куйбышевской области',
                    'registration' => '446009    Самарская обл., г. Сызрань, ул. Астраханская, д. 27., кв. 3',
                ),
            'snils' => '11952672674',
            'militaryRecord' =>
                array (
                    'fitnessForMilitaryService' => 'Unfit',
                    'recruitmentOffice' => 'ОВК г. Сызрань',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АЕ1935109',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    70 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '76-к',
                            'date' => '2016-10-27T00:00:00.0000000',
                            'effectiveDate' => '2016-10-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-сантехник',
                                    'id' => 40,
                                ),
                            'id' => 18187,
                        ),
                ),
            'userProfileId' => 12752,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Слесарь-сантехник',
                            'id' => 40,
                        ),
                ),
            'editable' => true,
            'id' => 6137,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631903230718',
            'firstName' => 'Юрий',
            'lastName' => 'Лапушкин',
            'middleName' => 'Николаевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Краснодонская, д. 70, кв. 50, индекс 443035',
            'countryId' => 643,
            'birthday' => '1951-06-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '539579',
                    'issuanceDate' => '2001-09-11T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с. Кр. Бор Вешкаймского района Ульяновской области',
                    'registration' => 'г. Самара, ул. Краснодонская, д. 70, кв. 50, индекс 443035',
                ),
            'snils' => '00628645847',
            'isEsiaBound' => false,
        ),
    71 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '14-к',
                            'date' => '2017-04-03T00:00:00.0000000',
                            'effectiveDate' => '2017-04-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Социальный педагог',
                                    'id' => 28,
                                ),
                            'id' => 21316,
                        ),
                ),
            'userProfileId' => 15460,
            'credentials' =>
                array (
                    'login' => 'Ларина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Социальный педагог',
                            'id' => 28,
                        ),
                ),
            'editable' => true,
            'id' => 6149,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631906145605',
            'firstName' => 'Светлана',
            'lastName' => 'Ларина',
            'middleName' => 'Сергеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Заводское шоссе, д. 71 Б, кв. 70, индекс 443052',
            'countryId' => 643,
            'birthday' => '1980-08-28T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '219768',
                    'issuanceDate' => '2002-06-21T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, Заводское шоссе, д. 71 Б, кв. 70, индекс 443052',
                ),
            'snils' => '11593019748',
            'isEsiaBound' => false,
        ),
    73 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '164-к',
                            'date' => '2017-12-22T00:00:00.0000000',
                            'effectiveDate' => '2017-12-23T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 22857,
                        ),
                ),
            'userProfileId' => 17491,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6175,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631200333082',
            'firstName' => 'Алексей',
            'lastName' => 'Лепихов',
            'middleName' => 'Михайлович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г.Самара  ул.Гвардейская д.13  кв.19',
            'countryId' => 643,
            'birthday' => '1949-07-21T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '827777',
                    'issuanceDate' => '2003-01-09T00:00:00.0000000',
                    'issued' => 'Управлением Внутренних Дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с.ст.Кленское  Первомайского р-на Тамбовской обл.',
                    'registration' => 'г.Самара  ул.Гвардейская д.13  кв.19',
                ),
            'snils' => '11931814853',
            'isEsiaBound' => false,
        ),
    74 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '17-к',
                            'date' => '2017-04-13T00:00:00.0000000',
                            'effectiveDate' => '2017-04-13T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Специалист по охране труда',
                                    'id' => 2080,
                                ),
                            'id' => 21328,
                        ),
                ),
            'userProfileId' => 15491,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Специалист по охране труда',
                            'id' => 2080,
                        ),
                ),
            'editable' => true,
            'id' => 6153,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631909128171',
            'firstName' => 'Вера',
            'lastName' => 'Лиликина',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. А. Матросова, д. 57/63, кв. 25, индекс 443063',
            'countryId' => 643,
            'birthday' => '1969-11-12T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 15',
                    'number' => '012766',
                    'issuanceDate' => '2014-12-11T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Ахтубинск Владимирского р-на Астраханской обл.',
                    'registration' => 'Самарская обл., г. Самара, Московское ш., д. 147, кв. 141, индекс 443084',
                ),
            'snils' => '01050128165',
            'isEsiaBound' => false,
        ),
    76 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '157-к',
                            'date' => '2017-11-20T00:00:00.0000000',
                            'effectiveDate' => '2017-11-20T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 22838,
                        ),
                ),
            'userProfileId' => 17322,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6173,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631805908402',
            'firstName' => 'Нина',
            'lastName' => 'Лунева',
            'middleName' => 'Борисовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г.Самара  ул.Двадцать второго партсъезда д.17   кв.47',
            'countryId' => 643,
            'birthday' => '1948-04-26T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '191657',
                    'issuanceDate' => '2010-03-18T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор.Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'гор.Куйбышев',
                    'registration' => 'г.Самара  ул.Двадцать второго партсъезда д.17   кв.47',
                ),
            'snils' => '00840289433',
            'isEsiaBound' => false,
        ),
    77 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '324',
                            'date' => '2001-09-12T00:00:00.0000000',
                            'effectiveDate' => '2001-09-12T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заведующий учебно-производственным сектором',
                                    'id' => 2062,
                                ),
                            'id' => 15255,
                        ),
                    1 =>
                        array (
                            'number' => '73-к',
                            'date' => '2015-12-01T00:00:00.0000000',
                            'effectiveDate' => '2015-12-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора по учебно-производственной работе',
                                    'id' => 14,
                                ),
                            'id' => 21311,
                        ),
                ),
            'userProfileId' => 11577,
            'credentials' =>
                array (
                    'login' => 'Ляпнев',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующий учебно-производственным сектором',
                            'id' => 2062,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора по учебно-производственной работе',
                            'id' => 14,
                        ),
                ),
            'editable' => true,
            'id' => 6055,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631906699307',
            'firstName' => 'Александр',
            'lastName' => 'Ляпнев',
            'middleName' => 'Викторович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул . Физкультурная, д. 37., кв. 49, 443008',
            'countryId' => 643,
            'birthday' => '1973-04-02T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '279246',
                    'issuanceDate' => '2002-04-20T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'п. Кособа Комсомольского района Кустанайской области',
                    'registration' => 'Самарская область, п. Вертяевка, ул. Пензенская, д. 12, 446416',
                ),
            'snils' => '07707753391',
            'factAddress' => '',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'LanceSergeant',
                    'militarySpecialty' => 'Механик',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'Кинельский объединенным ВК Самарской области',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'НП9592035',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => true,
                ),
            'isEsiaBound' => false,
        ),
    78 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '283',
                            'date' => '2001-08-28T00:00:00.0000000',
                            'effectiveDate' => '2001-08-28T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заведующий учебной частью',
                                    'id' => 2063,
                                ),
                            'id' => 15253,
                        ),
                    1 =>
                        array (
                            'number' => '56/02-03',
                            'date' => '2012-04-02T00:00:00.0000000',
                            'effectiveDate' => '2012-04-02T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Заведующий учебно-производственным сектором',
                                    'id' => 2062,
                                ),
                            'id' => 15272,
                        ),
                    2 =>
                        array (
                            'number' => '14-к',
                            'date' => '2016-02-01T00:00:00.0000000',
                            'effectiveDate' => '2016-02-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Старший методист',
                                    'id' => 27,
                                ),
                            'id' => 15501,
                        ),
                ),
            'userProfileId' => 11595,
            'credentials' =>
                array (
                    'login' => 'Ляпнева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующий учебной частью',
                            'id' => 2063,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующий учебно-производственным сектором',
                            'id' => 2062,
                        ),
                    2 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Старший методист',
                            'id' => 27,
                        ),
                ),
            'editable' => true,
            'id' => 6057,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631902031766',
            'firstName' => 'Наталья',
            'lastName' => 'Ляпнева',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, Костормской переулок, д. 9., общ., ком. 125  индекс 443052',
            'countryId' => 643,
            'birthday' => '1979-11-15T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '146450',
                    'issuanceDate' => '2009-09-18T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Эмба Мугоджарского р-на Актюбинской обл.',
                    'registration' => 'Самарская обл., гор. Самара, Костормской переулок, д. 9., общ., ком. 125  индекс 443052',
                ),
            'snils' => '11476380456',
            'isEsiaBound' => false,
        ),
    79 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '30-к',
                            'date' => '2006-06-15T00:00:00.0000000',
                            'effectiveDate' => '2006-06-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-сантехник',
                                    'id' => 40,
                                ),
                            'id' => 1186,
                        ),
                ),
            'userProfileId' => 355,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Слесарь-сантехник',
                            'id' => 40,
                        ),
                ),
            'editable' => true,
            'id' => 66,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205286534',
            'firstName' => 'Дмитрий',
            'lastName' => 'Макров',
            'middleName' => 'Михайлович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Дыбенко, д. 114, кв. 197, индекс 443066',
            'countryId' => 643,
            'birthday' => '1944-08-06T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '139393',
                    'issuanceDate' => '2002-04-29T00:00:00.0000000',
                    'issued' => '1 отделом милиции Кировского РУВД города Самары',
                    'subdivisionCode' => '633-001',
                    'birthplace' => 'Гиджуванский р-н Бухарской обл.',
                    'registration' => 'г. Самара, ул. Дыбенко, д. 114, кв. 197, индекс 443066',
                ),
            'snils' => '04161165920',
            'isEsiaBound' => false,
        ),
    80 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '54-к',
                            'date' => '2000-08-21T00:00:00.0000000',
                            'effectiveDate' => '2000-08-21T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заместитель директора по учебно-производственной работе',
                                    'id' => 14,
                                ),
                            'id' => 15,
                        ),
                ),
            'userProfileId' => 14,
            'credentials' =>
                array (
                    'login' => 'Мальцев',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заместитель директора по учебно-производственной работе',
                            'id' => 14,
                        ),
                ),
            'editable' => true,
            'id' => 14,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631212823814',
            'firstName' => 'Николай',
            'lastName' => 'Мальцев',
            'middleName' => 'Григорьевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Товарная, д. 7в, кв. 56, индекс 443109',
            'countryId' => 643,
            'birthday' => '1972-12-04T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '388983',
                    'issuanceDate' => '2002-07-16T00:00:00.0000000',
                    'issued' => '1 отделом милиции Кировского РУВД города Самары',
                    'subdivisionCode' => '633-001',
                    'birthplace' => 'п. Полякова Б.-Черниговского Куйбышевской обл.',
                    'registration' => 'г. Самара, ул. Товарная, д. 7в, кв. 56, индекс 443109',
                ),
            'snils' => '04100836294',
            'isEsiaBound' => false,
        ),
    81 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '55-к',
                            'date' => '2000-08-21T00:00:00.0000000',
                            'effectiveDate' => '2000-08-21T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Диспетчер ОУ',
                                    'id' => 12,
                                ),
                            'id' => 17,
                        ),
                    1 =>
                        array (
                            'number' => '55',
                            'date' => '2005-06-01T00:00:00.0000000',
                            'effectiveDate' => '2005-06-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18190,
                        ),
                    2 =>
                        array (
                            'number' => '65',
                            'date' => '2005-06-01T00:00:00.0000000',
                            'effectiveDate' => '2005-06-01T00:00:00.0000000',
                            'type' => 'Dismiss',
                            'position' =>
                                array (
                                    'name' => 'Диспетчер ОУ',
                                    'id' => 12,
                                ),
                            'id' => 18191,
                        ),
                ),
            'userProfileId' => 16,
            'credentials' =>
                array (
                    'login' => 'Мальцева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 16,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631215288001',
            'firstName' => 'Елена',
            'lastName' => 'Мальцева',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Товарная, д. 7в, кв. 56, индекс 443109',
            'countryId' => 643,
            'birthday' => '1973-06-21T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '388982',
                    'issuanceDate' => '2002-07-16T00:00:00.0000000',
                    'issued' => '1 отделом милиции кировского РУВД города Самары',
                    'subdivisionCode' => '633-001',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, ул. Товарная, д. 7в, кв. 56, индекс 443109',
                ),
            'snils' => '04100835898',
            'isEsiaBound' => false,
        ),
    82 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '20-к',
                            'date' => '2012-07-27T00:00:00.0000000',
                            'effectiveDate' => '2012-07-30T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Ведущий бухгалтер',
                                    'id' => 18,
                                ),
                            'id' => 1202,
                        ),
                ),
            'userProfileId' => 371,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Ведущий бухгалтер',
                            'id' => 18,
                        ),
                ),
            'editable' => true,
            'id' => 82,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637701211246',
            'firstName' => 'Ирина',
            'lastName' => 'Медведева',
            'middleName' => 'Анатольевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1982-06-07T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 13',
                    'number' => '788355',
                    'issuanceDate' => '2013-11-07T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Волжском районе',
                    'subdivisionCode' => '630-028',
                    'birthplace' => 'с. Рождественно Волжского р-на Самарской обл.',
                    'registration' => 'Самарская область, Волжский район, с. Рождественно, ул. Западная, д. 15, кв. 2, индекс 443541',
                ),
            'snils' => '10969103466',
            'isEsiaBound' => false,
        ),
    83 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '18-к',
                            'date' => '2017-04-13T00:00:00.0000000',
                            'effectiveDate' => '2017-04-13T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                                    'id' => 41,
                                ),
                            'id' => 21329,
                        ),
                ),
            'userProfileId' => 15492,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Слесарь-электрик по ремонту электрического оборудования',
                            'id' => 41,
                        ),
                ),
            'editable' => true,
            'id' => 6154,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631815401900',
            'firstName' => 'Алексей',
            'lastName' => 'Мелихов',
            'middleName' => 'Валерьевич',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл , г. Самара, ул. Карбышева, д. 24, кв. 35, индекс 443067',
            'countryId' => 643,
            'birthday' => '1987-03-26T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 06',
                    'number' => '642794',
                    'issuanceDate' => '2007-04-26T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор. Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'пос. Нефтегорск Нефтегорского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл , г. Самара, ул. Карбышева, д. 24, кв. 35, индекс 443067',
                ),
            'snils' => '11031805082',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'CommonSoldier',
                    'militarySpecialty' => 'механик',
                    'fitnessForMilitaryService' => 'LimitedlyFit',
                    'recruitmentOffice' => 'ВК Советского и Железнодорожного районов',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АЕ2576438',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    84 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '197',
                            'date' => '1986-08-04T00:00:00.0000000',
                            'effectiveDate' => '1986-08-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 25,
                        ),
                ),
            'userProfileId' => 24,
            'credentials' =>
                array (
                    'login' => 'Миронова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 24,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205286608',
            'firstName' => 'Валентина',
            'lastName' => 'Миронова',
            'middleName' => 'Викторовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., Красноглинский район, г. Самара, мкр-н Крутые Ключи, д. 30, кв. 22, индекс 443028',
            'countryId' => 643,
            'birthday' => '1950-01-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '889169',
                    'issuanceDate' => '2003-02-11T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская обл., Красноглинский район, г. Самара, мкр-н Крутые Ключи, д. 30, кв. 22, индекс 443028',
                ),
            'snils' => '00851805234',
            'isEsiaBound' => false,
        ),
    85 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '27-к',
                            'date' => '1998-04-01T00:00:00.0000000',
                            'effectiveDate' => '1998-04-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Электромонтёр связи',
                                    'id' => 56,
                                ),
                            'id' => 1187,
                        ),
                ),
            'userProfileId' => 356,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Электромонтёр связи',
                            'id' => 56,
                        ),
                ),
            'editable' => true,
            'id' => 67,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631200121070',
            'firstName' => 'Галина',
            'lastName' => 'Митькина',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Самарская, д. 203, кв. 41, индекс 443001',
            'countryId' => 643,
            'birthday' => '1960-12-10T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '359109',
                    'issuanceDate' => '2005-12-28T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Ленинского района города Самары',
                    'subdivisionCode' => '632-011',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Самарская, д. 203, кв. 41, индекс 443001',
                ),
            'snils' => '03002488494',
            'isEsiaBound' => false,
        ),
    87 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '128-к',
                            'date' => '2017-08-28T00:00:00.0000000',
                            'effectiveDate' => '2017-08-28T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21689,
                        ),
                ),
            'userProfileId' => 16011,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6158,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631219682094',
            'firstName' => 'Василий',
            'lastName' => 'Михайлов',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443109  Самарская обл., гор. Самара, Зубчаниновское шоссе, д. 159, кв. 71',
            'countryId' => 643,
            'birthday' => '1960-10-19T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '747051',
                    'issuanceDate' => '2007-09-28T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'с. Сиченцы Дунаевецкого р-на Хмельницкой обл.',
                    'registration' => '443109  Самарская обл., гор. Самара, Зубчаниновское шоссе, д. 159, кв. 71',
                ),
            'snils' => '10862208842',
            'isEsiaBound' => false,
        ),
    88 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '31-к',
                            'date' => '1988-04-01T00:00:00.0000000',
                            'effectiveDate' => '1988-04-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 16,
                        ),
                ),
            'userProfileId' => 15,
            'credentials' =>
                array (
                    'login' => 'Муракова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 15,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631803403357',
            'firstName' => 'Галина',
            'lastName' => 'Муракова',
            'middleName' => 'Валентиновна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Победы, д. 7А, кв. 103, индекс 443083',
            'countryId' => 643,
            'birthday' => '1951-11-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '733169',
                    'issuanceDate' => '2003-11-28T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Советского района города Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'п. Писцово Комсомольского р-на Ивановской обл.',
                    'registration' => 'г. Самара, ул. Победы, д. 7А, кв. 103, индекс 443083',
                ),
            'snils' => '00851801933',
            'isEsiaBound' => false,
        ),
    89 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '71-к',
                            'date' => '1990-10-01T00:00:00.0000000',
                            'effectiveDate' => '1990-10-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 26,
                        ),
                ),
            'userProfileId' => 25,
            'credentials' =>
                array (
                    'login' => 'Намнясов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 25,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631104876551',
            'firstName' => 'Виктор',
            'lastName' => 'Намнясов',
            'middleName' => 'Геннадьевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Партизанская, д.136, кв.53, индекс 443070',
            'countryId' => 643,
            'birthday' => '1959-09-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '109828',
                    'issuanceDate' => '2005-02-02T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Железнодорожного района города Самары',
                    'subdivisionCode' => '632-003',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Партизанская, д.136, кв.53, индекс 443070',
                ),
            'snils' => '00851802935',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'SeniorLieutenant',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'ВКР Железнодорожного района',
                    'isOnSpecialAccounting' => false,
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    91 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '160-к',
                            'date' => '2017-12-06T00:00:00.0000000',
                            'effectiveDate' => '2017-12-06T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 22859,
                        ),
                ),
            'userProfileId' => 17492,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6176,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205286929',
            'firstName' => 'Анатолий',
            'lastName' => 'Науменко',
            'middleName' => 'Анатольевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г.Самара Днепровский проезд  д.7  кв.46',
            'countryId' => 643,
            'birthday' => '1957-04-02T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '244703',
                    'issuanceDate' => '2002-05-20T00:00:00.0000000',
                    'issued' => 'Кировским РУВД гор.Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'г.Сызрань   Куйбышевской обл.',
                    'registration' => 'г.Самара Днепровский проезд  д.7  кв.46',
                ),
            'snils' => '02039749339',
            'isEsiaBound' => false,
        ),
    92 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '4-к',
                            'date' => '1999-01-04T00:00:00.0000000',
                            'effectiveDate' => '1999-01-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Медицинская сестра',
                                    'id' => 35,
                                ),
                            'id' => 1203,
                        ),
                ),
            'userProfileId' => 372,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Медицинская сестра',
                            'id' => 35,
                        ),
                ),
            'editable' => true,
            'id' => 83,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631811184927',
            'firstName' => 'Людмила',
            'lastName' => 'Несветаева',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Юрия Гагарина, д. 83 А, кв. 29, индекс 443047',
            'countryId' => 643,
            'birthday' => '1963-03-08T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '189105',
                    'issuanceDate' => '2009-11-24T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор. Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'пос. Колтубанка Бузулукского р-на Оренбургской обл.',
                    'registration' => 'Самарская обл., гор. Самара, ул. Юрия Гагарина, д. 83 А, кв. 29, индекс 443047',
                ),
            'snils' => '01146768131',
            'isEsiaBound' => false,
        ),
    93 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '29/02-03',
                            'date' => '2014-02-06T00:00:00.0000000',
                            'effectiveDate' => '2014-02-06T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Бухгалтер',
                                    'id' => 2081,
                                ),
                            'id' => 18172,
                        ),
                ),
            'userProfileId' => 12738,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Бухгалтер',
                            'id' => 2081,
                        ),
                ),
            'editable' => true,
            'id' => 6123,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '025503662034',
            'firstName' => 'Светлана',
            'lastName' => 'Никитина',
            'middleName' => 'Валерьевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, Конный проезд, д. 2/320, кв. 172, индекс 443109',
            'countryId' => 643,
            'birthday' => '1985-11-15T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '80 10',
                    'number' => '079567',
                    'issuanceDate' => '2010-06-25T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Республике Башкортостан в гор. Белебей',
                    'subdivisionCode' => '020-011',
                    'birthplace' => 'гор. Белебей Респ. Башкортостан',
                    'registration' => 'Самарская область, г. Самара, Конный проезд, д. 2/320, кв. 172, индекс 443109',
                ),
            'snils' => '13816261860',
            'isEsiaBound' => false,
        ),
    94 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '249/02-03',
                            'date' => '2013-12-03T00:00:00.0000000',
                            'effectiveDate' => '2013-12-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'заведующий хозяйством',
                                    'id' => 2082,
                                ),
                            'id' => 18171,
                        ),
                ),
            'userProfileId' => 12737,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'заведующий хозяйством',
                            'id' => 2082,
                        ),
                ),
            'editable' => true,
            'id' => 6122,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631200186529',
            'firstName' => 'Татьяна',
            'lastName' => 'Никитина',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Литвинова, д. 326, кв. 20, индекс 443109',
            'countryId' => 643,
            'birthday' => '1963-04-16T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '845558',
                    'issuanceDate' => '2008-04-19T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Ангарск Иркутской обл.',
                    'registration' => 'Самарская обл., гор. Самара, ул. Литвинова, д. 326, кв. 20, индекс 443109',
                ),
            'snils' => '05739145380',
            'isEsiaBound' => false,
        ),
    95 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '27-к',
                            'date' => '2016-03-01T00:00:00.0000000',
                            'effectiveDate' => '2016-03-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 15495,
                        ),
                ),
            'userProfileId' => 12298,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6080,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631900245152',
            'firstName' => 'Лилия',
            'lastName' => 'Никифорова',
            'middleName' => 'Андреевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Железной дивизии. д. 7. кв. 148, индекс 443052',
            'countryId' => 643,
            'birthday' => '1958-04-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '488293',
                    'issuanceDate' => '2006-04-13T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Промышленного района города Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Железной дивизии. д. 7. кв. 148, индекс 443052',
                ),
            'snils' => '00840459533',
            'isEsiaBound' => false,
        ),
    96 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '50-к',
                            'date' => '2015-09-01T00:00:00.0000000',
                            'effectiveDate' => '2015-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15287,
                        ),
                ),
            'userProfileId' => 11610,
            'credentials' =>
                array (
                    'login' => 'николаева621',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6078,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '021200664236',
            'firstName' => 'Роза',
            'lastName' => 'Николаева',
            'middleName' => 'Петровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., пгт. Усть-Кинельский, ул. Спортивная, д. 9 а, корп. 4, кв. 156, индекс 446442',
            'countryId' => 643,
            'birthday' => '1995-05-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '80 15',
                    'number' => '019421',
                    'issuanceDate' => '2014-06-25T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Республике Башкортостан в Бижбулякском районе',
                    'subdivisionCode' => '020-036',
                    'birthplace' => 'д. Елбулак-Матвеевка Бижбулякский район Республика Башкортостан',
                    'registration' => 'Респ. Башкортостан, р-н Бижбулякский, с. Елбулак-Матвеевка, ул. Чкалова, д. 67 индекс 452040',
                ),
            'snils' => '10921977463',
            'isEsiaBound' => false,
        ),
    98 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '20-к',
                            'date' => '2016-02-01T00:00:00.0000000',
                            'effectiveDate' => '2016-02-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 18176,
                        ),
                ),
            'userProfileId' => 12740,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6124,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631915639294',
            'firstName' => 'Михаил',
            'lastName' => 'Орехов',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, проспект Юных Пионеров, д. 73, кв. 6, индекс 443063',
            'countryId' => 643,
            'birthday' => '1963-07-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '890812',
                    'issuanceDate' => '2008-07-25T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Сеславино Сайрамского р-на Чимкентской обл.',
                    'registration' => 'Самарская обл., гор. Самара, проспект Юных Пионеров, д. 73, кв. 6, индекс 443063',
                ),
            'snils' => '04803009828',
            'isEsiaBound' => false,
        ),
    99 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '63-к',
                            'date' => '1999-09-02T00:00:00.0000000',
                            'effectiveDate' => '1999-09-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18,
                        ),
                ),
            'userProfileId' => 17,
            'credentials' =>
                array (
                    'login' => 'Останина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 17,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631909367370',
            'firstName' => 'Наталия',
            'lastName' => 'Останина',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Московское шоссе, д. 107, кв. 95, индекс 443111',
            'countryId' => 643,
            'birthday' => '1975-06-16T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '447399',
                    'issuanceDate' => '2003-09-18T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'город Куйбышев',
                    'registration' => 'г. Самара, Московское шоссе, д. 107, кв. 95, индекс 443111',
                ),
            'snils' => '02350978746',
            'isEsiaBound' => false,
        ),
    100 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '47-к',
                            'date' => '1997-10-06T00:00:00.0000000',
                            'effectiveDate' => '1997-10-06T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 27,
                        ),
                ),
            'userProfileId' => 26,
            'credentials' =>
                array (
                    'login' => 'Оськина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 26,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631201269866',
            'firstName' => 'Татьяна',
            'lastName' => 'Оськина',
            'middleName' => 'Степановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Свободы, д. 181, кв. 26, индекс 443077',
            'countryId' => 643,
            'birthday' => '1953-07-24T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 00',
                    'number' => '364688',
                    'issuanceDate' => '2000-12-20T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'д. Просандеевка Щацкого р-на Рязанской обл.',
                    'registration' => 'г. Самара, ул. Свободы, д. 181, кв. 26, индекс 443077',
                ),
            'snils' => '03002487189',
            'isEsiaBound' => false,
        ),
    101 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '47-к',
                            'date' => '2014-11-01T00:00:00.0000000',
                            'effectiveDate' => '2014-11-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дворник',
                                    'id' => 1060,
                                ),
                            'id' => 18177,
                        ),
                ),
            'userProfileId' => 12741,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дворник',
                            'id' => 1060,
                        ),
                ),
            'editable' => true,
            'id' => 6125,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631903449002',
            'firstName' => 'Лидия',
            'lastName' => 'Павельева',
            'middleName' => 'Павловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Н.-Вокзальная, д. 138, кв. 132, индекс 443111',
            'countryId' => 643,
            'birthday' => '1938-10-26T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '638633',
                    'issuanceDate' => '2003-11-28T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с/з "Оленьково" Мордвесского района Тульской области',
                    'registration' => 'г. Самара, ул. Н.-Вокзальная, д. 138, кв. 132, индекс 443111',
                ),
            'snils' => '00590287741',
            'isEsiaBound' => false,
        ),
    102 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '22-к',
                            'date' => '2010-07-01T00:00:00.0000000',
                            'effectiveDate' => '2010-07-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 1188,
                        ),
                ),
            'userProfileId' => 357,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 68,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '563102952373',
            'firstName' => 'Алексей',
            'lastName' => 'Паляница',
            'middleName' => 'Анатольевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый переулок, д. 6, кв. 312, индекс 443052',
            'countryId' => 643,
            'birthday' => '1979-09-03T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '53 02',
                    'number' => '753781',
                    'issuanceDate' => '2002-05-29T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Красногвардейского района Оренбургской области',
                    'subdivisionCode' => '562-021',
                    'birthplace' => 'с. Староникольское Красногвардейского района Оренбургской области',
                    'registration' => 'Оренбургская область, с. Староникольское, ул. Октябрьская, д. 14 А, кв. 2, индекс 461152',
                ),
            'snils' => '07386506490',
            'isEsiaBound' => false,
        ),
    103 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '59',
                            'date' => '1986-03-10T00:00:00.0000000',
                            'effectiveDate' => '1986-03-10T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 28,
                        ),
                ),
            'userProfileId' => 27,
            'credentials' =>
                array (
                    'login' => 'Певцова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 27,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631907071667',
            'firstName' => 'Валентина',
            'lastName' => 'Певцова',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый переулок, д. 6, общ., ком. 211, индекс 443052',
            'countryId' => 643,
            'birthday' => '1964-01-12T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '027699',
                    'issuanceDate' => '2009-02-03T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Ст. Эштебенькино Челно-Вершинского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., г. Самара, Старый переулок, д. 6, общ., ком. 211, индекс 443052',
                ),
            'snils' => '00851804434',
            'isEsiaBound' => false,
        ),
    104 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '2-к',
                            'date' => '2018-01-09T00:00:00.0000000',
                            'effectiveDate' => '2018-01-09T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 22860,
                        ),
                ),
            'userProfileId' => 17493,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6177,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636706481366',
            'firstName' => 'Владимир',
            'lastName' => 'Пеньков',
            'middleName' => 'Александрович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область  с.Сухая Вязовка  ул.Гаражная   д.2  кв.1',
            'countryId' => 643,
            'birthday' => '1989-12-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '211093',
                    'issuanceDate' => '2010-01-28T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Волжском районе',
                    'subdivisionCode' => '630-028',
                    'birthplace' => 'с.Сухая Вязовка  Волжского р-на Куйбышевской обл.',
                    'registration' => 'Самарская область  с.Сухая Вязовка  ул.Гаражная   д.2  кв.1',
                ),
            'snils' => '16680813188',
            'isEsiaBound' => false,
        ),
    106 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '44-к',
                            'date' => '2014-10-14T00:00:00.0000000',
                            'effectiveDate' => '2014-10-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Гардеробщик',
                                    'id' => 38,
                                ),
                            'id' => 1174,
                        ),
                ),
            'userProfileId' => 343,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Гардеробщик',
                            'id' => 38,
                        ),
                ),
            'editable' => true,
            'id' => 54,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631912671726',
            'firstName' => 'Вера',
            'lastName' => 'Пищева',
            'middleName' => 'Петровна',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Свободы, д. 124, кв. 8, индекс 443009',
            'countryId' => 643,
            'birthday' => '1952-09-25T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '950112',
                    'issuanceDate' => '2003-02-18T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'с. Частая Дубрава Боринского района Воронежской области',
                    'registration' => 'г. Самара, ул. Свободы, д. 124, кв. 8, индекс 443009',
                ),
            'snils' => '03890618783',
            'isEsiaBound' => false,
        ),
    107 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '71-к',
                            'date' => '2003-08-27T00:00:00.0000000',
                            'effectiveDate' => '2003-08-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 19,
                        ),
                ),
            'userProfileId' => 18,
            'credentials' =>
                array (
                    'login' => 'Редькин',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 18,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631207301227',
            'firstName' => 'Александр',
            'lastName' => 'Редькин',
            'middleName' => 'Робертович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Черемшанская, д. 156, кв. 41, индекс 443056',
            'countryId' => 643,
            'birthday' => '1957-03-01T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '434647',
                    'issuanceDate' => '2003-09-24T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Черемшанская, д. 156, кв. 41, индекс 443056',
                ),
            'snils' => '01219015692',
            'isEsiaBound' => false,
        ),
    108 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '01-19-04',
                            'date' => '1992-01-14T00:00:00.0000000',
                            'effectiveDate' => '1992-01-20T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Заведующая библиотекой',
                                    'id' => 58,
                                ),
                            'id' => 33,
                        ),
                ),
            'userProfileId' => 32,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Заведующая библиотекой',
                            'id' => 58,
                        ),
                ),
            'editable' => true,
            'id' => 32,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631207868489',
            'firstName' => 'Ольга',
            'lastName' => 'Рубан',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Литвинова, д. 336, кв. 13, индекс 443109',
            'countryId' => 643,
            'birthday' => '1955-11-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '475718',
                    'issuanceDate' => '2001-06-19T00:00:00.0000000',
                    'issued' => '1 отделом милиции Кировского РУВД города Самары',
                    'subdivisionCode' => '633-001',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Литвинова, д. 336, кв. 13, индекс 443109',
                ),
            'snils' => '02039749238',
            'isEsiaBound' => false,
        ),
    109 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '100/02-03',
                            'date' => '2011-08-01T00:00:00.0000000',
                            'effectiveDate' => '2011-08-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 18178,
                        ),
                ),
            'userProfileId' => 12742,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6126,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631220984990',
            'firstName' => 'Татьяна',
            'lastName' => 'Сабанина',
            'middleName' => 'Семеновна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Пугачевская, д. 19 А, кв. 35, индекс 443077',
            'countryId' => 643,
            'birthday' => '1962-11-27T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '184750',
                    'issuanceDate' => '2009-12-10T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская обл., г. Самара, ул. Пугачевская, д. 19 А, кв. 35, индекс 443077',
                ),
            'snils' => '07860522042',
            'isEsiaBound' => false,
        ),
    110 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '34-к',
                            'date' => '2016-04-25T00:00:00.0000000',
                            'effectiveDate' => '2016-04-25T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Бухгалтер',
                                    'id' => 2081,
                                ),
                            'id' => 18179,
                        ),
                    1 =>
                        array (
                            'number' => '81-к',
                            'date' => '2016-11-23T00:00:00.0000000',
                            'effectiveDate' => '2016-11-23T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Главный бухгалтер',
                                    'id' => 19,
                                ),
                            'id' => 21285,
                        ),
                ),
            'userProfileId' => 12743,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Бухгалтер',
                            'id' => 2081,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Главный бухгалтер',
                            'id' => 19,
                        ),
                ),
            'editable' => true,
            'id' => 6127,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631906731180',
            'firstName' => 'Вера',
            'lastName' => 'Салеба',
            'middleName' => 'Васильевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Воронежская, д. 34, кв. 11, индекс 443009',
            'countryId' => 643,
            'birthday' => '1967-05-19T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '596635',
                    'issuanceDate' => '2012-05-22T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'Самарская обл., гор. Самара, ул. Воронежская, д. 34, кв. 11, индекс 443009',
                ),
            'snils' => '07597134504',
            'isEsiaBound' => false,
        ),
    111 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '52-к',
                            'date' => '2007-09-01T00:00:00.0000000',
                            'effectiveDate' => '2007-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Воспитатель',
                                    'id' => 21,
                                ),
                            'id' => 35,
                        ),
                    1 =>
                        array (
                            'number' => '63-к',
                            'date' => '2016-09-01T00:00:00.0000000',
                            'effectiveDate' => '2016-09-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21284,
                        ),
                ),
            'userProfileId' => 34,
            'credentials' =>
                array (
                    'login' => 'Самохина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Воспитатель',
                            'id' => 21,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 34,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637603185279',
            'firstName' => 'Вера',
            'lastName' => 'Самохина',
            'middleName' => 'Басировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1969-06-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '363091',
                    'issuanceDate' => '2002-07-10T00:00:00.0000000',
                    'issued' => 'Красноярским РОВД Самарской обл.',
                    'subdivisionCode' => '632-045',
                    'birthplace' => 'Колхозабад Колхозабадского р. Курган-Тюбинской обл.',
                    'registration' => 'Самарская обл., с. Большая Каменка, ул. Луговая, д. 9, кв. 2 индекс 446382',
                ),
            'snils' => '02480289748',
            'isEsiaBound' => false,
        ),
    112 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '130/02-03',
                            'date' => '2006-08-30T00:00:00.0000000',
                            'effectiveDate' => '2006-08-30T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15262,
                        ),
                ),
            'userProfileId' => 11590,
            'credentials' =>
                array (
                    'login' => 'сараева601',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6077,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631815554079',
            'firstName' => 'Ирина',
            'lastName' => 'Сараева',
            'middleName' => 'Евгеньевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Средне-Садовая, д. 16, кв. 66, индекс 443016',
            'countryId' => 643,
            'birthday' => '1981-07-24T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '127055',
                    'issuanceDate' => '2009-10-22T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор. Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Средне-Садовая, д. 16, кв. 66, индекс 443016',
                ),
            'snils' => '10139585638',
            'isEsiaBound' => false,
        ),
    113 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '40-к',
                            'date' => '2013-08-19T00:00:00.0000000',
                            'effectiveDate' => '2013-08-19T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Кастелянша',
                                    'id' => 55,
                                ),
                            'id' => 18188,
                        ),
                ),
            'userProfileId' => 12753,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Кастелянша',
                            'id' => 55,
                        ),
                ),
            'editable' => true,
            'id' => 6138,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636501904726',
            'firstName' => 'Алтынай',
            'lastName' => 'Сарбапеева',
            'middleName' => 'Каиржановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6 общ., кв. 802, индекс 443052',
            'countryId' => 643,
            'birthday' => '1991-11-04T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 11',
                    'number' => '522284',
                    'issuanceDate' => '2011-11-16T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'с. Большая Черниговка Большечерниговского р-на Самарской обл.',
                    'registration' => 'Самарская обл., Большечерниговский р-н, с. Большая Черниговка, ул. Советская, д. 5, кв. 2, индекс 446290',
                ),
            'snils' => '12102459394',
            'isEsiaBound' => false,
        ),
    114 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '34-к',
                            'date' => '2012-10-03T00:00:00.0000000',
                            'effectiveDate' => '2012-10-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Воспитатель',
                                    'id' => 21,
                                ),
                            'id' => 1204,
                        ),
                ),
            'userProfileId' => 373,
            'credentials' =>
                array (
                    'login' => 'Сарбапеева',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Воспитатель',
                            'id' => 21,
                        ),
                ),
            'editable' => true,
            'id' => 84,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636501329850',
            'firstName' => 'Карлыга',
            'lastName' => 'Сарбапеева',
            'middleName' => 'Абдулкабировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6 общ., кв. 802, индекс 443052',
            'countryId' => 643,
            'birthday' => '1967-08-28T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 11',
                    'number' => '579247',
                    'issuanceDate' => '2012-09-22T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'с. Большая Черниговка Большечерниговского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., Большечерниговский р-н, с. Большая Черниговка, ул. Советская, д. 5, кв. 2, индекс 446290',
                ),
            'snils' => '02204216982',
            'factAddress' => '',
            'isEsiaBound' => false,
        ),
    115 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '01-20-47',
                            'date' => '1997-09-01T00:00:00.0000000',
                            'effectiveDate' => '1997-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Слесарь-ремонтник',
                                    'id' => 42,
                                ),
                            'id' => 1191,
                        ),
                ),
            'userProfileId' => 360,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Слесарь-ремонтник',
                            'id' => 42,
                        ),
                ),
            'editable' => true,
            'id' => 71,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631913383777',
            'firstName' => 'Сергей',
            'lastName' => 'Свиридов',
            'middleName' => 'Иванович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Железной дивизии, д. 7, кв. 67, индекс 443052',
            'countryId' => 643,
            'birthday' => '1962-07-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '730584',
                    'issuanceDate' => '2007-08-03T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Новобогоявленское Первомайского района Тамбовской обл.',
                    'registration' => 'г. Самара, ул. Железной дивизии, д. 7, кв. 67, индекс 443052',
                ),
            'snils' => '02039751326',
            'isEsiaBound' => false,
        ),
    116 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '61-к',
                            'date' => '2015-10-15T00:00:00.0000000',
                            'effectiveDate' => '2015-10-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Гардеробщик',
                                    'id' => 38,
                                ),
                            'id' => 18180,
                        ),
                ),
            'userProfileId' => 12744,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Гардеробщик',
                            'id' => 38,
                        ),
                ),
            'editable' => true,
            'id' => 6128,
            'educationLevel' => 'WithoutBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631803884717',
            'firstName' => 'Ольга',
            'lastName' => 'Седова',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ю. Гагарина, ул. 83 а, кв. 30, индекс 443047',
            'countryId' => 643,
            'birthday' => '1949-11-06T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 00',
                    'number' => '436275',
                    'issuanceDate' => '2001-03-16T00:00:00.0000000',
                    'issued' => 'Советским РУВД гор. Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'с. М-Кармалка Шугуровского района Татарской АССР',
                    'registration' => 'г. Самара, ул. Ю. Гагарина, ул. 83 а, кв. 30, индекс 443047',
                ),
            'snils' => '00590367537',
            'isEsiaBound' => false,
        ),
    117 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '8-к',
                            'date' => '2017-03-01T00:00:00.0000000',
                            'effectiveDate' => '2017-03-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Специалист по защите информации',
                                    'id' => 2084,
                                ),
                            'id' => 21319,
                        ),
                ),
            'userProfileId' => 15462,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Специалист по защите информации',
                            'id' => 2084,
                        ),
                ),
            'editable' => true,
            'id' => 6151,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631606089935',
            'firstName' => 'Павел',
            'lastName' => 'Семёнов',
            'middleName' => 'Иванович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Ялтинская, д. 7, ком. 82, индекс 443045',
            'countryId' => 643,
            'birthday' => '1973-01-24T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '446098',
                    'issuanceDate' => '2002-07-26T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Кинель-Черкасского района Самарской области',
                    'subdivisionCode' => '632-015',
                    'birthplace' => 'г. Грозный',
                    'registration' => 'Самарская обл., гор. Самара, ул. Ялтинская, д. 7, ком. 82, индекс 443045',
                ),
            'snils' => '06793079811',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'CommonSoldier',
                    'fitnessForMilitaryService' => 'Unfit',
                    'recruitmentOffice' => 'Ленинский  РВК г. Грозного Чечено-Ингушской Республики',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'НД8219507',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    118 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '101-к',
                            'date' => '2017-06-23T00:00:00.0000000',
                            'effectiveDate' => '2017-06-23T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Специалист по закупкам',
                                    'id' => 2086,
                                ),
                            'id' => 21354,
                        ),
                ),
            'userProfileId' => 15716,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Специалист по закупкам',
                            'id' => 2086,
                        ),
                ),
            'editable' => true,
            'id' => 6155,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '370250126418',
            'firstName' => 'Сергей',
            'lastName' => 'Сентюров',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Физкультурная, д. 33, кв.53 индекс 443008',
            'countryId' => 643,
            'birthday' => '1976-02-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '24 01',
                    'number' => '180251',
                    'issuanceDate' => '2001-06-20T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Ленинского района города Иваново',
                    'subdivisionCode' => '372-005',
                    'birthplace' => 'гор. Иваново',
                    'registration' => 'г. Самара, ул. Физкультурная, д. 33, кв.53 индекс 443008',
                ),
            'snils' => '00923538337',
            'militaryRecord' =>
                array (
                    'militaryRank' => 'CommonSoldier',
                    'fitnessForMilitaryService' => 'LimitedlyFit',
                    'recruitmentOffice' => 'ВК г. Похвистнево',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АВ1119120',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    119 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '185',
                            'date' => '1990-09-01T00:00:00.0000000',
                            'effectiveDate' => '1990-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Руководитель физвоспитания',
                                    'id' => 2068,
                                ),
                            'id' => 15251,
                        ),
                    1 =>
                        array (
                            'number' => '11-к',
                            'date' => '2016-02-01T00:00:00.0000000',
                            'effectiveDate' => '2016-02-01T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15504,
                        ),
                ),
            'userProfileId' => 11583,
            'credentials' =>
                array (
                    'login' => '26',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Руководитель физвоспитания',
                            'id' => 2068,
                        ),
                ),
            'editable' => true,
            'id' => 6066,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631205277307',
            'firstName' => 'Валерий',
            'lastName' => 'Сергеев',
            'middleName' => 'Александрович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443034 г. Самара, ул. Енисейская, д. 37, кв. 1',
            'countryId' => 643,
            'birthday' => '1956-07-25T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '837114',
                    'issuanceDate' => '2001-12-13T00:00:00.0000000',
                    'issued' => 'УВД Кировского район города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с. Чувичи Хворостянского района Куйбышевской обл.',
                    'registration' => '443034 г. Самара, ул. Енисейская, д. 37, кв. 1',
                ),
            'snils' => '01531913616',
            'isEsiaBound' => false,
        ),
    120 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '2222',
                            'date' => '2015-06-08T00:00:00.0000000',
                            'effectiveDate' => '2015-06-08T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Администратор АСУ РСО',
                                    'id' => 8,
                                ),
                            'id' => 3238,
                        ),
                ),
            'userProfileId' => 262,
            'credentials' =>
                array (
                    'login' => 'Силантьев',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Администратор АСУ РСО',
                            'id' => 8,
                        ),
                ),
            'editable' => true,
            'id' => 1050,
            'educationLevel' => 'VocationalBasic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 600,
            'pedagogicalExperience' => 0,
            'inn' => '631189615179',
            'firstName' => 'Евгений',
            'lastName' => 'Силантьев',
            'middleName' => 'Алексеевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Мориса Тореза,  д. 21, кв. 6',
            'countryId' => 643,
            'birthday' => '1996-12-06T00:00:00.0000000',
            'phone' => '+7 (987) 156-28-11',
            'email' => 'silantev-zhenya@mail.ru',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '3617',
                    'number' => '350442',
                    'issuanceDate' => '2017-05-25T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Железнодорожном районе гор. Самары',
                    'subdivisionCode' => '630-003',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'г. Самара, ул. Мориса Тореза,  д. 21, кв. 6',
                ),
            'snils' => '17799067245',
            'militaryRecord' =>
                array (
                    'isOnSpecialAccounting' => false,
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => true,
        ),
    121 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '66-к',
                            'date' => '2016-09-08T00:00:00.0000000',
                            'effectiveDate' => '2016-09-08T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Специалист по кадрам',
                                    'id' => 2077,
                                ),
                            'id' => 18102,
                        ),
                ),
            'userProfileId' => 12691,
            'credentials' =>
                array (
                    'login' => 'Симонова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Специалист по кадрам',
                            'id' => 2077,
                        ),
                ),
            'editable' => true,
            'id' => 6100,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '560200396658',
            'firstName' => 'Наталья',
            'lastName' => 'Симонова',
            'middleName' => 'Васильевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул Свободы, д. 173, кв. 36, индекс 443077',
            'countryId' => 643,
            'birthday' => '1974-10-21T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '745564',
                    'issuanceDate' => '2007-08-23T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. куйбышев',
                    'registration' => 'г. Самара, ул Свободы, д. 173, кв. 36, индекс 443077',
                ),
            'snils' => '04462741653',
            'isEsiaBound' => false,
        ),
    123 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '69-к',
                            'date' => '2016-09-26T00:00:00.0000000',
                            'effectiveDate' => '2016-09-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18182,
                        ),
                ),
            'userProfileId' => 12746,
            'credentials' =>
                array (
                    'login' => 'сладкова758',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6131,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631206121292',
            'firstName' => 'Татьяна',
            'lastName' => 'Сладкова',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Свободы, д. 157, кв. 51, индекс 443077',
            'countryId' => 643,
            'birthday' => '1951-03-24T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '424905',
                    'issuanceDate' => '2003-08-17T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'с. Кротовка К-Черкасского р-на Куйбышевской обл.',
                    'registration' => 'г. Самара, ул. Свободы, д. 157, кв. 51, индекс 443077',
                ),
            'snils' => '00840290216',
            'isEsiaBound' => false,
        ),
    124 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '56-к',
                            'date' => '2016-08-22T00:00:00.0000000',
                            'effectiveDate' => '2016-08-22T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 18075,
                        ),
                ),
            'userProfileId' => 12670,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6096,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631909578572',
            'firstName' => 'Нина',
            'lastName' => 'Сметанникова',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самары, Стационарный пер-к, д. 11, кв. 303, 304, индекс 443052',
            'countryId' => 643,
            'birthday' => '1972-06-24T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '472046',
                    'issuanceDate' => '2002-08-22T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'п. Красный Строитель Челно-Вершинского района Самарской обл.',
                    'registration' => 'г. Самары, Стационарный пер-к, д. 11, кв. 303, 304, индекс 443052',
                ),
            'snils' => '02351473620',
            'isEsiaBound' => false,
        ),
    125 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '1-к',
                            'date' => '2018-01-09T00:00:00.0000000',
                            'effectiveDate' => '2018-01-09T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 22862,
                        ),
                ),
            'userProfileId' => 17494,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6178,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631802720040',
            'firstName' => 'Наталия',
            'lastName' => 'Советкина',
            'middleName' => 'Сергеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г.Самара  ул.Черногорская д.9',
            'countryId' => 643,
            'birthday' => '1988-09-18T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '973847',
                    'issuanceDate' => '2008-10-09T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Советском районе гор.Самары',
                    'subdivisionCode' => '630-002',
                    'birthplace' => 'гор.Куйбышев',
                    'registration' => 'г.Самара  ул.Черногорская д.9',
                ),
            'snils' => '14259983398',
            'isEsiaBound' => false,
        ),
    126 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '151-к',
                            'date' => '2017-10-09T00:00:00.0000000',
                            'effectiveDate' => '2017-10-09T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Вахтёр',
                                    'id' => 43,
                                ),
                            'id' => 21776,
                        ),
                ),
            'userProfileId' => 16096,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Вахтёр',
                            'id' => 43,
                        ),
                ),
            'editable' => true,
            'id' => 6167,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631909304780',
            'firstName' => 'Наталья',
            'lastName' => 'Соколова',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'индекс  443051 г. Самара, ул. Олимпийская, д. 16, кв. 141',
            'countryId' => 643,
            'birthday' => '1953-01-18T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '642082',
                    'issuanceDate' => '2004-01-06T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'индекс  443051 г. Самара, ул. Олимпийская, д. 16, кв. 141',
                ),
            'snils' => '05625606155',
            'isEsiaBound' => false,
        ),
    127 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '58-1-к',
                            'date' => '2017-06-01T00:00:00.0000000',
                            'effectiveDate' => '2017-06-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 21375,
                        ),
                ),
            'userProfileId' => 15718,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6156,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631184912690',
            'firstName' => 'Юлия',
            'lastName' => 'Сорокина',
            'middleName' => 'Михайловна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443030 г. Самара, ул. Красноармейская, д. 141, кв. 8',
            'countryId' => 643,
            'birthday' => '1983-04-06T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 10',
                    'number' => '249845',
                    'issuanceDate' => '2010-04-22T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Железнодорожном районе гор. Самары',
                    'subdivisionCode' => '630-003',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => '443030 г. Самара, ул. Красноармейская, д. 141, кв. 8',
                ),
            'snils' => '14716757385',
            'isEsiaBound' => false,
        ),
    128 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '28-к',
                            'date' => '2009-08-01T00:00:00.0000000',
                            'effectiveDate' => '2009-08-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 18158,
                        ),
                ),
            'userProfileId' => 11846,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6130,
            'educationLevel' => 'Basic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '637502404870',
            'firstName' => 'Александр',
            'lastName' => 'Суров',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, пер. Старый, дом 6, ком. 215, индекс 443052',
            'countryId' => 643,
            'birthday' => '1984-08-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '424863',
                    'issuanceDate' => '2006-06-16T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Большечерниговского района Самарской области',
                    'subdivisionCode' => '632-020',
                    'birthplace' => 'гор. Махачкала Дагестанской АССР',
                    'registration' => 'Самарская область, п. Краснооктябрьский, ул. Микрорайон, д. 5, кв. 2, 446297',
                ),
            'snils' => '11593019142',
            'factAddress' => 'Самарская область, г. Самара, пер. Старый, дом 6, ком. 215, индекс 443052',
            'isEsiaBound' => false,
        ),
    129 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '47-к',
                            'date' => '2008-09-01T00:00:00.0000000',
                            'effectiveDate' => '2008-09-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 1190,
                        ),
                ),
            'userProfileId' => 359,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 70,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636500176792',
            'firstName' => 'Халида',
            'lastName' => 'Сурова',
            'middleName' => 'Хакимьяновна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер., д. 6, кв. 216, индекс 443052',
            'countryId' => 643,
            'birthday' => '1963-07-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '911553',
                    'issuanceDate' => '2008-08-19T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'с-з "Красный Октябрь" Большеглушицкого р-на Куйбышевской обл.',
                    'registration' => '446297, Самарская область, п. Краснооктябрьский, ул. Микрорайон, д. 5, кв. 2',
                ),
            'snils' => '01913923140',
            'isEsiaBound' => false,
        ),
    130 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '153',
                            'date' => '1983-09-07T00:00:00.0000000',
                            'effectiveDate' => '1983-09-07T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 30,
                        ),
                ),
            'userProfileId' => 29,
            'credentials' =>
                array (
                    'login' => 'Тельцов',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 29,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'firstName' => 'Геннадий',
            'lastName' => 'Тельцов',
            'middleName' => 'Викторович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый переулок, д. 6, общ., кв. 201, 202, 203, индекс 443052',
            'countryId' => 643,
            'birthday' => '1962-04-23T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '729221',
                    'issuanceDate' => '2007-07-10T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Богоявленка Борского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., г. Самара, Старый переулок, д. 6, общ., кв. 201, 202, 203, индекс 443052',
                ),
            'isEsiaBound' => false,
        ),
    131 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '95-к',
                            'date' => '1987-08-19T00:00:00.0000000',
                            'effectiveDate' => '1987-08-20T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 29,
                        ),
                ),
            'userProfileId' => 28,
            'credentials' =>
                array (
                    'login' => 'Тельцова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 28,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631920091943',
            'firstName' => 'Марина',
            'lastName' => 'Тельцова',
            'middleName' => 'Ивановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., кв. 201, 202, 203, 204, индекс 443052',
            'countryId' => 643,
            'birthday' => '1967-07-22T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '631910',
                    'issuanceDate' => '2012-09-07T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'с. Каменный Брод Челно-Вершинского района Куйбышевской обл.',
                    'registration' => 'Самарская обл., г. Самара, Старый пер., д. 6, общ., кв. 201, 202, 203, 204, индекс 443052',
                ),
            'snils' => '00851801024',
            'isEsiaBound' => false,
        ),
    132 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '43-к',
                            'date' => '1999-07-14T00:00:00.0000000',
                            'effectiveDate' => '1999-07-14T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Мастер п/о',
                                    'id' => 17,
                                ),
                            'id' => 31,
                        ),
                ),
            'userProfileId' => 30,
            'credentials' =>
                array (
                    'login' => 'Тимофеев',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Мастер п/о',
                            'id' => 17,
                        ),
                ),
            'editable' => true,
            'id' => 30,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'firstName' => 'Александр',
            'lastName' => 'Тимофеев',
            'middleName' => 'Владимирович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Солнечная, д. 15, кв. 3, индекс 443029',
            'countryId' => 643,
            'birthday' => '1959-01-30T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '155151',
                    'issuanceDate' => '2005-02-02T00:00:00.0000000',
                    'issued' => 'Отделом милиции №8 УВД Промышленного района города Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'разъезд Денискино Шенталинского р-на Куйбышевской обл.',
                    'registration' => 'г. Самара, ул. Солнечная, д. 15, кв. 3, индекс 443029',
                ),
            'militaryRecord' =>
                array (
                    'militaryRank' => 'CommonSoldier',
                    'fitnessForMilitaryService' => 'Fit',
                    'recruitmentOffice' => 'ВКР Промышленного района',
                    'isOnSpecialAccounting' => false,
                    'reserveCategory' => 'SecondCategory',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    133 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '139/02-03',
                            'date' => '2007-08-30T00:00:00.0000000',
                            'effectiveDate' => '2007-08-30T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15263,
                        ),
                ),
            'userProfileId' => 11588,
            'credentials' =>
                array (
                    'login' => '10',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6056,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631926049064',
            'firstName' => 'Галина',
            'lastName' => 'Тимофеева',
            'middleName' => 'Владимировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Теннисная, д. 12, кв. 18, индекс 443092',
            'countryId' => 643,
            'birthday' => '1984-04-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 06',
                    'number' => '562374',
                    'issuanceDate' => '2006-10-13T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Промышленного района города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Теннисная, д. 12, кв. 18, индекс 443092',
                ),
            'snils' => '12292698473',
            'isEsiaBound' => false,
        ),
    134 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '138-к',
                            'date' => '2017-09-11T00:00:00.0000000',
                            'effectiveDate' => '2017-09-11T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21692,
                        ),
                ),
            'userProfileId' => 16014,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6161,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631210146363',
            'firstName' => 'Людмила',
            'lastName' => 'Тужилкина',
            'middleName' => 'Владимировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443095   г. Самара, ул. Ташкентская, д. 149, кв. 191',
            'countryId' => 643,
            'birthday' => '1969-03-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 14',
                    'number' => '892337',
                    'issuanceDate' => '2014-04-02T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => '443095   г. Самара, ул. Ташкентская, д. 149, кв. 191',
                ),
            'snils' => '00749662366',
            'isEsiaBound' => false,
        ),
    135 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '669',
                            'date' => '2016-09-07T00:00:00.0000000',
                            'effectiveDate' => '2016-09-07T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Администратор АСУ РСО',
                                    'id' => 8,
                                ),
                            'id' => 18057,
                        ),
                ),
            'userProfileId' => 72,
            'credentials' =>
                array (
                    'login' => 'Тюрина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Администратор АСУ РСО',
                            'id' => 8,
                        ),
                ),
            'editable' => true,
            'id' => 6084,
            'educationLevel' => 'Basic',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'firstName' => 'Анна',
            'lastName' => 'Тюрина',
            'middleName' => 'Юрьевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'гор. Самара, ул. Алма-Атинская дом 78, кв. 58, индекс 443078',
            'countryId' => 643,
            'birthday' => '1998-02-09T00:00:00.0000000',
            'phone' => '89279017120',
            'note' => 'п/н 669',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '3617',
                    'number' => '482551',
                    'issuanceDate' => '2018-02-16T00:00:00.0000000',
                    'issued' => 'ГУ МВД России по Самарской области',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'гор. Самара, ул. Алма-Атинская дом 78, кв. 58, индекс 443078',
                ),
            'snils' => '19311233136',
            'isEsiaBound' => false,
        ),
    136 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '10',
                            'date' => '2012-04-02T00:00:00.0000000',
                            'effectiveDate' => '2012-04-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 18183,
                        ),
                ),
            'userProfileId' => 12747,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 6132,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636501601009',
            'firstName' => 'Рамиль',
            'lastName' => 'Фахретдинов',
            'middleName' => 'Якубович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1988-01-20T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 07',
                    'number' => '831692',
                    'issuanceDate' => '2008-02-05T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'пос. Краснооктябрьский Большечерниговского р-на Куйбышевской обл.',
                    'registration' => 'Самарская область, Большечерниговский район, п. Краснооктябрьский, ул. Заречная, д. 23, индекс 446297',
                ),
            'snils' => '16093437064',
            'isEsiaBound' => false,
        ),
    137 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '34-к',
                            'date' => '2011-11-01T00:00:00.0000000',
                            'effectiveDate' => '2011-11-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Сторож',
                                    'id' => 44,
                                ),
                            'id' => 1193,
                        ),
                ),
            'userProfileId' => 362,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Сторож',
                            'id' => 44,
                        ),
                ),
            'editable' => true,
            'id' => 73,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636500888310',
            'firstName' => 'Самига',
            'lastName' => 'Фахретдинова',
            'middleName' => 'Хасановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер., д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1960-10-06T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 05',
                    'number' => '354508',
                    'issuanceDate' => '2005-10-22T00:00:00.0000000',
                    'issued' => 'Отделом внутренних дел Большечерниговского района Самарской области',
                    'subdivisionCode' => '632-020',
                    'birthplace' => 'пос. Краснооктябрьский Большечерниговского р-на Куйбышевской',
                    'registration' => 'Самарская обл., Большечерниговский район, п. Краснооктябрьский, ул. Заречная, д. 23, индекс 446297',
                ),
            'snils' => '04100992712',
            'isEsiaBound' => false,
        ),
    138 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '141-к',
                            'date' => '2017-09-25T00:00:00.0000000',
                            'effectiveDate' => '2017-09-25T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21751,
                        ),
                ),
            'userProfileId' => 16042,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6163,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631210146758',
            'firstName' => 'Анна',
            'lastName' => 'Федякина',
            'middleName' => 'Александровна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443035 г. Самара, пр. Кирова, д. 140, кв. 3',
            'countryId' => 643,
            'birthday' => '1963-11-26T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '018974',
                    'issuanceDate' => '2009-02-12T00:00:00.0000000',
                    'issued' => 'отделом УФМС России по Самарской области в Кировском районе гор. Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор. Александровск-Сахалинский Сахалинская обл.',
                    'registration' => '443035 г. Самара, пр. Кирова, д. 140, кв. 3',
                ),
            'snils' => '00749665069',
            'isEsiaBound' => false,
        ),
    139 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '136-к',
                            'date' => '2017-09-07T00:00:00.0000000',
                            'effectiveDate' => '2017-09-07T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21691,
                        ),
                ),
            'userProfileId' => 16013,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6160,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631921346401',
            'firstName' => 'Юлия',
            'lastName' => 'Фельдбуш',
            'middleName' => 'Владимировна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443034  г. Самара, пр-т Металлургов/ул. Советская, д. 31/39, кв. 4',
            'countryId' => 643,
            'birthday' => '1983-11-09T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 04',
                    'number' => '575350',
                    'issuanceDate' => '2003-11-21T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД города Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'г. Алма-Ата',
                    'registration' => '443034  г. Самара, пр-т Металлургов/ул. Советская, д. 31/39, кв. 4',
                ),
            'snils' => '12678335584',
            'isEsiaBound' => false,
        ),
    140 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '45-к',
                            'date' => '2006-08-01T00:00:00.0000000',
                            'effectiveDate' => '2006-08-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик производственных помещений',
                                    'id' => 46,
                                ),
                            'id' => 18184,
                        ),
                ),
            'userProfileId' => 12748,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Уборщик производственных помещений',
                            'id' => 46,
                        ),
                ),
            'editable' => true,
            'id' => 6133,
            'educationLevel' => 'Secondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636800394086',
            'firstName' => 'Галия',
            'lastName' => 'Хайруллова',
            'middleName' => 'Абдулахатовна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Земеца, д. 30, кв. 16, индекс 443052',
            'countryId' => 643,
            'birthday' => '1979-10-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 99',
                    'number' => '182136',
                    'issuanceDate' => '2000-04-28T00:00:00.0000000',
                    'issued' => 'Советским РУВД гор. Самары',
                    'subdivisionCode' => '632-002',
                    'birthplace' => 'с. Большой Чирклей Николаевского р-на Ульяновской обл.',
                    'registration' => 'Самарская обл., Елховский район, с. Мулловка, ул. Широкая, д. 45, индекс 446878',
                ),
            'snils' => '12760877076',
            'isEsiaBound' => false,
        ),
    141 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '164',
                            'date' => '1976-09-02T00:00:00.0000000',
                            'effectiveDate' => '1976-09-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 15245,
                        ),
                ),
            'userProfileId' => 11596,
            'credentials' =>
                array (
                    'login' => '45',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6063,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631209221383',
            'firstName' => 'Наталья',
            'lastName' => 'Харитонова',
            'middleName' => 'Сергеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Ст. Загора, д. 184, кв. 59, индекс 443095',
            'countryId' => 643,
            'birthday' => '1952-03-29T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 03',
                    'number' => '993650',
                    'issuanceDate' => '2003-03-29T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'г. Самара, ул. Ст. Загора, д. 184, кв. 59, индекс 443095',
                ),
            'snils' => '01531914113',
            'isEsiaBound' => false,
        ),
    142 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '2-к',
                            'date' => '2017-01-23T00:00:00.0000000',
                            'effectiveDate' => '2017-01-23T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 21286,
                        ),
                ),
            'userProfileId' => 14318,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6145,
            'educationLevel' => 'IncompleteSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '560200926376',
            'firstName' => 'Татьяна',
            'lastName' => 'Хмара',
            'middleName' => 'Юрьевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, ул. Олимпийская, д. 18, кв. 211, индекс 443051',
            'countryId' => 643,
            'birthday' => '1980-07-19T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '53 06',
                    'number' => '509610',
                    'issuanceDate' => '2007-08-16T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Оренбургской области в гор. Оренбурге',
                    'subdivisionCode' => '560-010',
                    'birthplace' => 'с. Елатомка Бугурусланского района Оренбургской области',
                    'registration' => 'Самарская область, г. Самара, ул. Олимпийская, д. 18, кв. 211, индекс 443051',
                ),
            'snils' => '11810554223',
            'isEsiaBound' => false,
        ),
    143 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '27-к',
                            'date' => '2014-08-26T00:00:00.0000000',
                            'effectiveDate' => '2014-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 42,
                        ),
                ),
            'userProfileId' => 41,
            'credentials' =>
                array (
                    'login' => 'Холопова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 41,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '760215513257',
            'firstName' => 'Дарья',
            'lastName' => 'Холопова',
            'middleName' => 'Игоревна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, мкр. Крутые Ключи, д. 1, кв. 42, индекс 443028',
            'countryId' => 643,
            'birthday' => '1992-02-16T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '78 14',
                    'number' => '055096',
                    'issuanceDate' => '2014-05-29T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Ярославской области в Дзержинском районе гор. Ярославля',
                    'subdivisionCode' => '760-003',
                    'birthplace' => 'гор. Ярославль',
                    'registration' => 'Ярославская обл., гор. Ярославль, ул. Керамическая, д. 7, кв. 45',
                ),
            'snils' => '12601556119',
            'isEsiaBound' => false,
        ),
    144 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '24-к',
                            'date' => '2010-07-12T00:00:00.0000000',
                            'effectiveDate' => '2010-07-12T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 1194,
                        ),
                ),
            'userProfileId' => 363,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 74,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631663190674',
            'firstName' => 'Светлана',
            'lastName' => 'Черемшанцева',
            'middleName' => 'Сергеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самары, Пятая просека, д. 99 Б, кв. 62, индекс 443029',
            'countryId' => 643,
            'birthday' => '1971-07-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 16',
                    'number' => '255465',
                    'issuanceDate' => '2016-08-11T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Октябрьском районе гор. Самары',
                    'subdivisionCode' => '630-010',
                    'birthplace' => 'г. Куйбышев',
                    'registration' => 'Самарская обл., г. Самары, Пятая просека, д. 99 Б, кв. 62, индекс 443029',
                ),
            'snils' => '06598434412',
            'isEsiaBound' => false,
        ),
    145 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '51-к',
                            'date' => '2001-09-04T00:00:00.0000000',
                            'effectiveDate' => '2001-09-04T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Педагог-психолог',
                                    'id' => 30,
                                ),
                            'id' => 34,
                        ),
                ),
            'userProfileId' => 33,
            'credentials' =>
                array (
                    'login' => 'Чудочкина',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Педагог-психолог',
                            'id' => 30,
                        ),
                ),
            'editable' => true,
            'id' => 33,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636501492984',
            'firstName' => 'Наталья',
            'lastName' => 'Чудочкина',
            'middleName' => 'Васильевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый переулок, д. 6, общ., кв. 313, индекс 443052',
            'countryId' => 643,
            'birthday' => '1979-06-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '595589',
                    'issuanceDate' => '2012-04-17T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'пос. Аверьяновский Большечерниговского р-на Куйбышевской обл.',
                    'registration' => 'г. Самара, Старый переулок, д. 6, общ., кв. 313, индекс 443052',
                ),
            'snils' => '07747696530',
            'isEsiaBound' => false,
        ),
    146 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '30-к',
                            'date' => '2010-08-26T00:00:00.0000000',
                            'effectiveDate' => '2010-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Комендант',
                                    'id' => 53,
                                ),
                            'id' => 1227,
                        ),
                ),
            'userProfileId' => 390,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Комендант',
                            'id' => 53,
                        ),
                ),
            'editable' => true,
            'id' => 89,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '636500194826',
            'firstName' => 'Канзиля',
            'lastName' => 'Чукаева',
            'middleName' => 'Кинжегалеевна',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер., д. 6, ком. 310, индекс 443052',
            'countryId' => 643,
            'birthday' => '1963-11-30T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '966753',
                    'issuanceDate' => '2009-01-15T00:00:00.0000000',
                    'issued' => 'Территориальным пунктом УФМС России по Самарской области в Большечерниговском районе',
                    'subdivisionCode' => '630-020',
                    'birthplace' => 'с/з Глушицкий Большечерниговского р-на Куйбышевской обл.',
                    'registration' => 'Самарская обл., Большечерниговский р-н, пос. Шумовский, ул. Советская, д. 19, кв. 1, индекс 446292',
                ),
            'snils' => '04100645590',
            'isEsiaBound' => false,
        ),
    147 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '25-к',
                            'date' => '2014-08-26T00:00:00.0000000',
                            'effectiveDate' => '2014-08-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 39,
                        ),
                ),
            'userProfileId' => 38,
            'credentials' =>
                array (
                    'login' => 'Шамова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 38,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631310426251',
            'firstName' => 'Татьяна',
            'lastName' => 'Шамова',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, п. Прибрежный, ул. Парусная, д. 19, кв. 83, индекс 443902',
            'countryId' => 643,
            'birthday' => '1977-02-18T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 02',
                    'number' => '739311',
                    'issuanceDate' => '2003-04-01T00:00:00.0000000',
                    'issued' => '6 отделением милиции Красноглинского РОВД гор. Самары',
                    'subdivisionCode' => '633-007',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, п. Прибрежный, ул. Парусная, д. 19, кв. 83, индекс 443902',
                ),
            'snils' => '06657882114',
            'isEsiaBound' => false,
        ),
    148 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '194',
                            'date' => '1986-08-06T00:00:00.0000000',
                            'effectiveDate' => '1986-08-06T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 20,
                        ),
                ),
            'userProfileId' => 19,
            'credentials' =>
                array (
                    'login' => 'Шапошникова',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 19,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'qualificationCategory' => 'First',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631906159020',
            'firstName' => 'Сания',
            'lastName' => 'Шапошникова',
            'middleName' => 'Сафаргалеевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Пугачевская, д. 6, кв. 27 индекс 443077',
            'countryId' => 643,
            'birthday' => '1954-10-26T00:00:00.0000000',
            'phone' => '997-56-38',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '460597',
                    'issuanceDate' => '2001-05-15T00:00:00.0000000',
                    'issued' => 'Промышленным РУВД гор. Самары',
                    'subdivisionCode' => '632-005',
                    'birthplace' => 'п. Южный Большеглушицкого района Куйбышевской области',
                    'registration' => 'г. Самара, ул. Пугачевская, д. 6, кв. 27 индекс 443077',
                ),
            'snils' => '00851806135',
            'isEsiaBound' => false,
        ),
    149 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '10-к',
                            'date' => '2002-03-22T00:00:00.0000000',
                            'effectiveDate' => '2002-03-22T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Дежурный по общежитию',
                                    'id' => 52,
                                ),
                            'id' => 1226,
                        ),
                ),
            'userProfileId' => 389,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Дежурный по общежитию',
                            'id' => 52,
                        ),
                ),
            'editable' => true,
            'id' => 88,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '635703668903',
            'firstName' => 'Рузалия',
            'lastName' => 'Шаяхметова',
            'middleName' => 'Галимжановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, Старый пер. , д. 6, кв. 811, индекс 443052',
            'countryId' => 643,
            'birthday' => '1968-02-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '698264',
                    'issuanceDate' => '2013-02-22T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Похвистневском районе',
                    'subdivisionCode' => '630-021',
                    'birthplace' => 'с. Мочалеевка Похвистневский район Куйбышевская область',
                    'registration' => 'Самарская обл., р-н Похвистневский, с. Мочалеевка, ул. М. Джалиля, д. 82, индекс 446464',
                ),
            'snils' => '11492961064',
            'isEsiaBound' => false,
        ),
    150 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '144-к',
                            'date' => '2017-09-27T00:00:00.0000000',
                            'effectiveDate' => '2017-09-27T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21752,
                        ),
                ),
            'userProfileId' => 16043,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6164,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => true,
            'academicDegree' => 'Doctorant',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631702957180',
            'firstName' => 'Ольга',
            'lastName' => 'Шишкина',
            'middleName' => 'Юрьевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => '443124 г. Самара, ул. Солнечная, д. 12, кв. 42',
            'countryId' => 643,
            'birthday' => '1980-03-14T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '048087',
                    'issuanceDate' => '2009-08-01T00:00:00.0000000',
                    'issued' => 'отделение УФМС России по Самарской области в Самарском районе гор. Самары',
                    'subdivisionCode' => '630-001',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => '443099 г. Самара, ул. А. Толстого, д. 92, кв. 3',
                ),
            'snils' => '04717742873',
            'isEsiaBound' => false,
        ),
    151 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '155-к',
                            'date' => '2017-11-01T00:00:00.0000000',
                            'effectiveDate' => '2017-11-01T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 22839,
                        ),
                ),
            'userProfileId' => 17323,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6174,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631224890614',
            'firstName' => 'Эльнара',
            'lastName' => 'Шошева',
            'middleName' => 'Экмановна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г.Самара  ул.ш.Зубчаниновское  д.120  кв.26',
            'countryId' => 643,
            'birthday' => '1990-03-31T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 13',
                    'number' => '793728',
                    'issuanceDate' => '2013-08-09T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Кировском районе гор.Самары',
                    'subdivisionCode' => '630-006',
                    'birthplace' => 'гор.Куйбышев',
                    'registration' => 'г.Самара  ул.ш.Зубчаниновское  д.120  кв.26',
                ),
            'snils' => '17200255213',
            'isEsiaBound' => false,
        ),
    152 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '21/02-03',
                            'date' => '2010-02-15T00:00:00.0000000',
                            'effectiveDate' => '2010-02-15T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Секретарь учебной части',
                                    'id' => 9,
                                ),
                            'id' => 15266,
                        ),
                    1 =>
                        array (
                            'number' => '131/02-03',
                            'date' => '2010-08-31T00:00:00.0000000',
                            'effectiveDate' => '2010-08-31T00:00:00.0000000',
                            'type' => 'Appoint',
                            'position' =>
                                array (
                                    'name' => 'Методист',
                                    'id' => 2066,
                                ),
                            'id' => 15268,
                        ),
                ),
            'userProfileId' => 11600,
            'credentials' =>
                array (
                    'login' => '31',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Методист',
                            'id' => 2066,
                        ),
                    1 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Секретарь учебной части',
                            'id' => 9,
                        ),
                ),
            'editable' => true,
            'id' => 6062,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => true,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '250602374089',
            'firstName' => 'Ольга',
            'lastName' => 'Шпакова',
            'middleName' => 'Евгеньевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, г. Самара, ул. Двадцать второго Партсъезда, д. 32, кв. 49, индекс 443066',
            'countryId' => 643,
            'birthday' => '1984-09-03T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '05 08',
                    'number' => '539474',
                    'issuanceDate' => '2008-10-15T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Приморскому краю в гор. Дальнереченск',
                    'subdivisionCode' => '250-027',
                    'birthplace' => 'гор. Петропавловск-Камчатский',
                    'registration' => 'Самарская область, г. Самара, ул. Двадцать второго Партсъезда, д. 32, кв. 49, индекс 443066',
                ),
            'snils' => '10635806441',
            'isEsiaBound' => false,
        ),
    153 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '240/02-03',
                            'date' => '2013-11-26T00:00:00.0000000',
                            'effectiveDate' => '2013-11-26T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Уборщик служебных помещений',
                                    'id' => 45,
                                ),
                            'id' => 18185,
                        ),
                ),
            'userProfileId' => 12750,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Уборщик служебных помещений',
                            'id' => 45,
                        ),
                ),
            'editable' => true,
            'id' => 6135,
            'educationLevel' => 'VocationalSecondary',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631914343520',
            'firstName' => 'Нина',
            'lastName' => 'Штырлова',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., г. Самара, ул. Железной дивизии, д. 13, кв. 73, индекс 443052',
            'countryId' => 643,
            'birthday' => '1967-04-17T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 12',
                    'number' => '595914',
                    'issuanceDate' => '2012-04-26T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'дер. Старое Узеево Аксубаевского р-на Татарской АССР',
                    'registration' => 'Самарская обл., г. Самара, ул. Железной дивизии, д. 13, кв. 73, индекс 443052',
                ),
            'snils' => '06768587534',
            'isEsiaBound' => false,
        ),
    154 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '15-к',
                            'date' => '2017-04-03T00:00:00.0000000',
                            'effectiveDate' => '2017-04-03T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Профконсультант',
                                    'id' => 2085,
                                ),
                            'id' => 21327,
                        ),
                ),
            'userProfileId' => 15490,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 0.5,
                            'name' => 'Профконсультант',
                            'id' => 2085,
                        ),
                ),
            'editable' => true,
            'id' => 6152,
            'educationLevel' => 'HigherPostGraduate',
            'hasPedagogicalEducation' => false,
            'academicDegree' => 'Doctorant',
            'academicTitle' => 'AssociateProfessor',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631219255014',
            'firstName' => 'Дмитрий',
            'lastName' => 'Щелоков',
            'middleName' => 'Александрович',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'г. Самара, ул. Пугачевская, д. 10 а, кв. 26, индекс 443077',
            'countryId' => 643,
            'birthday' => '1981-05-15T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 01',
                    'number' => '710504',
                    'issuanceDate' => '2001-10-10T00:00:00.0000000',
                    'issued' => 'Управлением внутренних дел Кировского района города Самары',
                    'subdivisionCode' => '632-006',
                    'birthplace' => 'гор. Куйбышев',
                    'registration' => 'г. Самара, ул. Пугачевская, д. 10 а, кв. 26, индекс 443077',
                ),
            'snils' => '03385096662',
            'isEsiaBound' => false,
        ),
    155 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '57-к',
                            'date' => '2003-07-03T00:00:00.0000000',
                            'effectiveDate' => '2003-07-07T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Начальник штаба ГО',
                                    'id' => 25,
                                ),
                            'id' => 3,
                        ),
                ),
            'userProfileId' => 3,
            'credentials' =>
                array (
                    'login' => 'Якименко',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Начальник штаба ГО',
                            'id' => 25,
                        ),
                ),
            'editable' => true,
            'id' => 3,
            'educationLevel' => 'HigherBachelor',
            'hasPedagogicalEducation' => false,
            'qualificationCategory' => 'Higher',
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631804148692',
            'firstName' => 'Владимир',
            'lastName' => 'Якименко',
            'middleName' => 'Васильевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, Старый переулок, д. 6, общ., индекс 443052',
            'countryId' => 643,
            'birthday' => '1964-06-05T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 09',
                    'number' => '105534',
                    'issuanceDate' => '2009-06-19T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Самарской области в Промышленном районе гор. Самары',
                    'subdivisionCode' => '630-005',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'Самарская обл., гор. Самара, Старый переулок, д. 6, общ., индекс 443052',
                ),
            'snils' => '08929488038',
            'isEsiaBound' => false,
        ),
    156 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '73-к',
                            'date' => '2016-10-17T00:00:00.0000000',
                            'effectiveDate' => '2016-10-17T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 18186,
                        ),
                ),
            'userProfileId' => 12751,
            'credentials' =>
                array (
                    'login' => 'яковлев763',
                ),
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'PartTime',
                            'rate' => 0.5,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6136,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '631703326638',
            'firstName' => 'Александр',
            'lastName' => 'Яковлев',
            'middleName' => 'Сергеевич',
            'gender' => 'Male',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская обл., гор. Самара, ул. Фрунзе, д. 56, кв. 9, индекс 443099',
            'countryId' => 643,
            'birthday' => '1988-04-09T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '36 08',
                    'number' => '855853',
                    'issuanceDate' => '2008-04-25T00:00:00.0000000',
                    'issued' => 'Отделением УФМС России по Самарской области в Самарском районе гор. Самары',
                    'subdivisionCode' => '630-001',
                    'birthplace' => 'гор. Самара',
                    'registration' => 'Самарская обл., гор. Самара, ул. Фрунзе, д. 56, кв. 9, индекс 443099',
                ),
            'snils' => '13623755155',
            'militaryRecord' =>
                array (
                    'militarySpecialty' => 'Техник',
                    'fitnessForMilitaryService' => 'LimitedlyFit',
                    'recruitmentOffice' => 'ВК Самарского района г. Самары',
                    'isOnSpecialAccounting' => false,
                    'militaryCardNumber' => 'АВ0965742',
                    'reserveCategory' => 'SecondCategory',
                    'groupOfAccounting' => 'Army',
                    'militaryComposition' => 'SoldiersAndSailors',
                    'hasMilitaryPreparation' => false,
                ),
            'isEsiaBound' => false,
        ),
    157 =>
        array (
            'decrees' =>
                array (
                    0 =>
                        array (
                            'number' => '9-к',
                            'date' => '2017-03-02T00:00:00.0000000',
                            'effectiveDate' => '2017-03-02T00:00:00.0000000',
                            'type' => 'Hire',
                            'position' =>
                                array (
                                    'name' => 'Преподаватель',
                                    'id' => 11,
                                ),
                            'id' => 21317,
                        ),
                ),
            'userProfileId' => 15461,
            'positions' =>
                array (
                    0 =>
                        array (
                            'laborContract' => 'Employment',
                            'rate' => 1,
                            'name' => 'Преподаватель',
                            'id' => 11,
                        ),
                ),
            'editable' => true,
            'id' => 6150,
            'educationLevel' => 'HigherMagistracy',
            'hasPedagogicalEducation' => false,
            'workExperience' => 0,
            'pedagogicalExperience' => 0,
            'inn' => '164510995295',
            'firstName' => 'Татьяна',
            'lastName' => 'Якутина',
            'middleName' => 'Николаевна',
            'gender' => 'Female',
            'isIndigenousMinority' => false,
            'hasChildrenUnderThreeYears' => false,
            'address' => 'Самарская область, гор. Самары, мкр-н Крутые Ключи, д. 120, кв 24, индекс 443028',
            'countryId' => 643,
            'birthday' => '1977-05-09T00:00:00.0000000',
            'passport' =>
                array (
                    'documentType' => 'RfPassport',
                    'series' => '92 08',
                    'number' => '527960',
                    'issuanceDate' => '2008-07-07T00:00:00.0000000',
                    'issued' => 'Отделом УФМС России по Республике Татарстан в Бугульминском районе',
                    'subdivisionCode' => '160-025',
                    'birthplace' => 'с. Приютовка Александрийского р-на Кировоградской области',
                    'registration' => 'Самарская область, гор. Самары, мкр-н Крутые Ключи, д. 120, кв 24, индекс 443028',
                ),
            'snils' => '02945625471',
            'isEsiaBound' => false,
        ),
);