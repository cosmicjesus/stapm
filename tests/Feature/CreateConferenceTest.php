<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory as Factory;

class CreateConferenceTest extends TestCase
{

    use WithoutMiddleware;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {

        $faker = Factory::create('ru_RU');
        $data = [
            'title' => $faker->text(),
            'active' => 'true',
            'date_of_event' => $faker->date(),
            'full_text' => $faker->randomHtml(),
            'files' => [
                [
                    'name' => $faker->text(20),
                    'path' => UploadedFile::fake()->create('file1.pdf')
                ],
                [
                    'name' => $faker->text(20),
                    'path' => UploadedFile::fake()->create('file2.pdf')
                ]
            ]
        ];

        $response = $this->json('POST', '/api/conferences', $data);

        $response->assertStatus(422);
    }
}
