<?php
Route::group(['prefix' => 'students'], function () {
    Route::get('/{id}/export-to-asurso', 'Api\ExportController@getStudentProfileToAsuRso');
    Route::get('/{id}/make-training-reference', 'Api\StudentController@makeTrainingReference')->name('api.student.make-training-reference');
    Route::post('/{id}/make-training-reference', 'Api\ReferencesController@training');
    Route::get('/{id}/make-military-reference', 'Api\ReferencesController@makeMilitaryReference')->name('api.student.make-military-reference');
    Route::post('/{id}/make-voenkomat-reference', 'Api\ReferencesController@voenkomat');
    Route::post('/{id}/update-passport', 'Api\StudentController@updatePassport');
    Route::post('/{id}/add-decree', 'Api\StudentController@addDecree');
    Route::put('/{id}/personal', 'Api\StudentController@updatePersonal');
    Route::post('/{id}/update-addresses', 'Api\StudentController@updateAddresses');
    Route::post('/{id}/update-education', 'Api\StudentController@updateEducation');
    Route::post('/{id}/add-file', 'Api\StudentController@addFile');
    Route::post('/{id}/upload-photo', 'Api\StudentController@uploadPhoto');
    Route::post('/{id}/delete-photo', 'Api\StudentController@deletePhoto');
    Route::post('/check-unique', 'Api\StudentController@checkUnique');
    Route::post('/retire-students', 'Api\StudentController@retireStudents');
    Route::post('/transfer-students', 'Api\StudentController@transferStudents');
    Route::post('/reinstate-students', 'Api\StudentController@reinstateStudents');
    Route::post('/print-references/military', 'Api\StudentController@massPrintMilitaryReferences');
    Route::get('/{id}/export-to-asurso', 'Api\StudentController@exportProfileToAsuRso')->name('api.students.profile-to-asurso');
});

Route::resource('students', 'Api\StudentController', ['parametrs' => ['student' => 'id']])->names('api.students');