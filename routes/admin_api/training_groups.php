<?php
Route::group(['prefix' => 'training-groups'], function () {
    Route::post('/graduation', 'Api\TrainingGroupController@graduation')->name('admin.training-groups.graduation');
    Route::post('/transfer-next-term', 'Api\TrainingGroupController@transferNextTerm')->name('admin.training-groups.transfer-next-term');
    Route::post('/transfer-next-year', 'Api\TrainingGroupController@transferNextYear')->name('admin.training-groups.transfer-next-year');
    Route::get('/transfer-map', 'Api\TrainingGroupController@getTransferMap')->name('admin.training-groups.transfer-map');
    Route::get('/{id}/export-students-profiles', 'Api\TrainingGroupController@exportStudentsProfiles')->name('admin.training-groups.export-students-profiles');
    Route::get('/{id}/export-students-to-asurso', 'Api\TrainingGroupController@exportStudentsToAsurso')->name('admin.training-groups.export-students-to-asurso');
    Route::get('/{id}/export-students-parents-to-asurso', 'Api\TrainingGroupController@exportStudentsParentsToAsuRso')->name('admin.training-groups.export-students-parents-to-asurso');
    Route::get('/{id}/export-students-photos', 'Api\TrainingGroupController@exportStudentsPhotos')->name('admin.training-groups.export-students-photos');
    Route::get('/{id}/mass-print-references', 'Api\TrainingGroupController@massPrintReferences')->name('api.training-groups.mass-print-references');
    Route::get('/{year}/list','Api\TeacherLoadController@getGroupsByYear')->name('api.training-groups.by-year');
});
Route::resource('training-groups', 'Api\TrainingGroupController', ['params' => ['training_group' => 'id'
]])->except(['edit', 'create'])->names('admin.training-groups');
