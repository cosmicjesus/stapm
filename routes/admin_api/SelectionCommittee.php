<?php
/*Роуты приемок*/
Route::get('selection-committees/{id}/programs', 'Api\SelectionCommitteeController@getPrograms');

Route::get('selection-committe/only-active', 'Api\SelectionCommitteeController@onlyActive');
Route::get('selection-committe/active-programs', 'Api\SelectionCommitteeController@activePrograms');
Route::get('selection-committee/entrant-report', 'Api\EntrantController@makeReport')->name('api.selection-committee.entrant-report');
Route::resource('selection-committees', 'Api\SelectionCommitteeController', ['parametrs' => ['selection-committee' => 'id']])->names('api.selection-committee');
/*Роуты приемок*/

/*Роуты программ набора*/
Route::get('active-recruitment-programs', 'Api\SelectionCommitteeController@activePrograms');
Route::resource('recruitment-programs', 'Api\RecruitmentProgramController', ['parametrs' => ['recruitment-program' => 'id']]);
/*Роуты программ набора*/