<?php

Route::group(['prefix' => 'training-calendars'], function () {
    Route::get('/all', 'Api\TrainingCalendarController@all')->name('api.training-calendars.all');
});

Route::apiResource('training-calendars', 'Api\TrainingCalendarController')->except(['edit', 'create'])->names('admin.training-calendars');