<?php
Route::post('entrants/check-unique', 'Api\EntrantController@checkUnique');
Route::post('entrants/delete', 'Api\EntrantController@destroy')->name('api.entrants.delete-entrants');
Route::post('entrants/refresh-count', 'Api\EntrantController@refreshCount')->name('api.entrants.refresh-count');
Route::post('entrants/enrollment', 'Api\EntrantController@enrollment')->name('api.entrants.enrollment');
Route::group(['prefix' => 'entrants/{entrant_id}'], function () {
    Route::resource('recruitment-programs', 'Api\Entrant\EntrantProgramController',['parametrs' => ['recruitment_program' => 'id']])
        ->names('api.entrants.recruitment-programs')->except(['create','edit','index','show']);
});
Route::resource('entrants', 'Api\EntrantController', ['parametrs' => ['entrant' => 'id']])
    ->except('delete')
    ->names('api.entrants');
