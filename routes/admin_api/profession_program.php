<?php
Route::post('/profession-programs/{program_id}/subjects/{subject_id}/add-file', 'Api\ProgramDocumentController@addDocumentInSubject');
Route::post('/profession-programs/{id}/add-subject', 'Api\ProgramDocumentController@addSubject');
Route::post('/profession-programs/{program_id}/add-additional', 'Api\ProgramDocumentController@addAdditionalDocument');
Route::delete('/profession-programs/program-files/{id}/delete', 'Api\ProgramDocumentController@deleteDocument');
Route::post('/profession-programs/program-files/{id}/update-file', 'Api\ProgramDocumentController@updateDocument');
Route::delete('/profession-programs/{profession_program_id}/subjects/{subject_id}/delete', 'Api\ProgramDocumentController@deleteSubject');
Route::get('/profession-programs/{profession_program_id}/subjects/{subject_id}/files', 'Api\ProgramDocumentController@getSubjectFiles');
Route::delete('/profession-programs/{profession_program_id}/subjects/{subject_id}/delete', 'Api\ProgramDocumentController@deleteSubjectFromProgram');
Route::post('/profession-programs/basic-documents/{id}/update', 'Api\ProgramDocumentController@updateBasicDocument');

Route::post('/program-documents/{id}/create-component', 'Api\ProgramDocumentController@createComponent');
Route::post('/program-documents/{id}/build', 'Api\ProgramDocumentController@buildFromComponents');

Route::put('/program-subject/{id}', 'Api\ProgramDocumentController@updateProgramSubject');
Route::post('/profession-programs/{profession_program_id}/program-documents/{id}/reload', 'Api\ProgramDocumentController@reloadFile')->name('admin.api.program-document.reload');

Route::get('/profession-programs/{profession_program_id}/education-plans', 'Api\EducationPlanController@getPlansByProgramId')->name('api.profession-program.education-plans');
Route::resource('profession-programs', 'Api\ProfessionProgramController', ['parameters' => ['profession-program' => 'id']]);