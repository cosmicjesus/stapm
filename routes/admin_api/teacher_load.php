<?php

Route::group(['prefix' => 'teacher-load'], function () {
    Route::get('/{id}', 'Api\TeacherLoadController@show')->name('api.teacher-load.group');
    Route::post('/{id}/subjects', 'Api\TeacherLoadController@addSubject')->name('api.teacher-load.group.add-subject');
    Route::get('/{id}/subjects/{subject_id}', 'Api\TeacherLoadController@getDetailSubject')->name('api.teacher-load.group.get-subject');
    Route::get('/{id}/subjects/{subject_id}/months/{month_id}/lessons', 'Api\TeacherLoadController@getMonthItems')->name('api.teacher-load.group.get-month-item');
    Route::post('/{id}/subjects/{subject_id}/months/{month_id}/lessons', 'Api\TeacherLoadController@addLesson')->name('api.teacher-load.group.add-lesson');
    Route::get('/{year}/groups', 'Api\TeacherLoadController@index')->name('api.teacher-load.groups');
    Route::post('/groups', 'Api\TeacherLoadController@addGroupInLoad')->name('api.teacher-load.add-group');
});
