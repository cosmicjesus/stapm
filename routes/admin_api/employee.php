<?php

Route::get('employees/all', 'Api\EmployeeController@allForSelect')->name('api.employees.all');
Route::resource('employees', 'Api\EmployeeController', ['parametrs' => ['employee' => 'id']])->names('api.employees');
Route::group(['prefix' => 'employees'], function () {
    Route::post('/{id}/add-position', 'Api\EmployeePositionController@addPosition');
    Route::post('/{id}/add-education', 'Api\EmployeeEducationController@addEducation');
    Route::post('/{id}/add-subject', 'Api\EmployeeSubjectController@addSubject');
    Route::post('/{id}/add-course', 'Api\EmployeeCourseController@addCourse');
    Route::post('/{id}/military-record', 'Api\EmployeeController@militaryRecord')->name('api.employee.military-record');
});

Route::group(['prefix' => 'employees/{id}'], function () {
    Route::resource('/previous-jobs', 'Api\Employee\EmployeePreviousJobsController')->names('api.employee.previous-jobs');
});

Route::put('/employee-position/{id}/update', 'Api\EmployeePositionController@updatePosition');
Route::put('/employee-education/{id}/update', 'Api\EmployeeEducationController@updateEducation');
Route::delete('/employee-education/{id}/delete', 'Api\EmployeeEducationController@deleteEducation');
Route::delete('/employee-subject/{id}/delete', 'Api\EmployeeSubjectController@deleteSubject');
Route::delete('/employee-course/{id}/delete', 'Api\EmployeeCourseController@deleteCourse');
