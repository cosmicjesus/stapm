<?php

Route::group(['prefix'=>'human/{human_id}'],function (){
    Route::resource('files', 'Api\Human\HumanFileController',['parametrs' => ['file' => 'id']])
        ->names('api.human.files')->except(['create','edit','index','show']);
});
