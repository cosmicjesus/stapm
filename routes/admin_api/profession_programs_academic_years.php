<?php
Route::apiResource('profession-programs/{profession_program_id}/academic-years', 'Api\ProfessionProgramsAcademicYear', ['params' => ['academic_year' => 'id'
]])->except(['edit', 'create'])->names('api.programs-academic-years');

Route::group(['prefix' => 'profession-programs/{profession_program_id}/academic-years'], function () {
    Route::get('/{academic_year_id}/subjects', 'Api\ProfessionProgramController@subjectByYear')->name('api.profession-programs.subject-by-year');
    Route::post('/{academic_year_id}/copy-from-another-year', 'Api\ProfessionProgramsAcademicYear@copySubjectFromAnotherYear')->name('api.profession-programs.copy-from-another-year');
});
