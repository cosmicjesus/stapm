<?php

Route::post('/education-plans/{id}/refresh-file', 'Api\EducationPlanController@refreshFile');
Route::post('/education-plans/{id}/create-component', 'Api\EducationPlanController@createComponent');
Route::post('/education-plans/{id}/build', 'Api\EducationPlanController@build');
Route::get('/education-plans/all', 'Api\EducationPlanController@all')->name('api.education-plans.all');
Route::resource('education-plans', 'Api\EducationPlanController')->names('api.education-plans');