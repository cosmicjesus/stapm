<?php

Route::group(['domain' => env('ADMIN_DOMAIN'), 'middleware' => ['web']], function () {

    Auth::routes();
    Route::get('/test', 'TestController@test');
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'Admin\DashboardController@index')->name('admin.index');
        Route::get('/site-settings', 'Admin\SiteSettingsController@edit')->name('admin.site-settings');
        Route::post('/site-settings', 'Admin\SiteSettingsController@update')->name('admin.site-settings.update');
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'Admin\UserController@index')->name('admin.users.index');
            Route::get('/data', 'Admin\UserController@data')->name('admin.users.data');
            Route::get('/create', 'Admin\UserController@create')->name('admin.users.create');
            Route::post('/store', 'Admin\UserController@store')->name('admin.users.store');
            Route::get('/{id}/edit', 'Admin\UserController@edit')->name('admin.users.edit');
            Route::post('/{id}/update', 'Admin\UserController@update')->name('admin.users.update');
            Route::post('/{id}/delete', 'Admin\UserController@delete')->name('admin.users.delete');
        });

        Route::group(['prefix' => 'qr'], function () {
            Route::get('/', 'Admin\QrController@index')->name('admin.qr');
            Route::get('/generate/{type?}', 'Admin\QrController@generate')->name('admin.qr.generate');
        });
        Route::group(['prefix' => 'profession-programs'], function () {
            Route::get('/', 'Admin\ProfessionProgramController@all')->name('admin.programs');
            Route::get('/data', 'Admin\ProfessionProgramController@getAllData')->name('admin.programs.data');
            Route::get('/create', 'Admin\ProfessionProgramController@createPage')->name('admin.programs.createPage');
            Route::post('/create', 'Admin\ProfessionProgramController@create')->name('admin.programs.create');

            Route::get('/{slug}', 'Admin\ProfessionProgramController@detail')->name('admin.programs.detail');

            Route::get('/{slug}/edit', 'Admin\ProfessionProgramController@edit')->name('admin.programs.edit');
            Route::post('/{slug}/update', 'Admin\ProfessionProgramController@update')->name('admin.programs.update');

            Route::post('/{id}/upload/{type}', 'Admin\ProgramDocumentController@uploadBasic')->name('admin.program.document.upload');

            Route::post('/{id}/add-subject', 'Admin\ProgramDocumentController@addSubject')->name('admin.program.add-subject');
            Route::post('/program-subject/{id}/add-document', 'Admin\ProgramDocumentController@addSubjectDocument')->name('admin.program.add-subject-document');
            Route::post('/program-subject/{id}/delete', 'Admin\ProgramDocumentController@deleteSubject')->name('admin.program.delete-subject');
            Route::post('/program-document/{id}/reload/{other?}', 'Admin\ProgramDocumentController@reloadDocument')->name('admin.program.reload-document');
            Route::post('/program-document/{id}/delete', 'Admin\ProgramDocumentController@deleteDocument')->name('admin.program.delete-document');
            Route::post('/progam-document/{id}/other', 'Admin\ProgramDocumentController@addOtherDocument')->name('admin.program.add-other');
            Route::get('/{id}/metodicals', 'Admin\ProgramDocumentController@getMetodicals')->name('admin.program.metodicals');
            Route::post('/{id}/metodicals/create', 'Admin\ProgramDocumentController@createMetodical')->name('admin.program.create-metodical');
        });

        Route::group(['prefix' => 'positions'], function () {
            Route::get('/', 'Admin\PositionController@all')->name('admin.positions');
            Route::get('/data', 'Admin\PositionController@allData')->name('admin.positions.data');

            Route::get('/create', 'Admin\PositionController@createPage')->name('admin.position.create');
            Route::post('/create', 'Admin\PositionController@create')->name('admin.position.create.post');

            Route::get('/{id}/edit', 'Admin\PositionController@editPage')->name('admin.position.edit');
            Route::post('/{id}/edit', 'Admin\PositionController@edit')->name('admin.position.edit.post');
            Route::post('/{id}/delete', 'Admin\PositionController@delete')->name('admin.position.delete');
        });

        Route::group(['prefix' => 'employees'], function () {
            Route::get('/', 'Admin\EmployeeController@all')->name('admin.employees');
            Route::get('/data', 'Admin\EmployeeController@allData')->name('admin.employees.data');
            Route::get('/{slug}/profile', 'Admin\EmployeeController@profile')->name('admin.employee.profile');
            Route::get('/{slug}/edit', 'Admin\EmployeeController@edit')->name('admin.employee.edit');
            Route::post('/{slug}/edit', 'Admin\EmployeeController@update')->name('admin.employee.update');
            Route::post('/education/{id}/update', 'Admin\EmployeeController@educationUpdate')->name('admin.employee.education.update');

            Route::post('/{slug}/photo', 'Admin\EmployeeController@uploadPhoto')->name('admin.employee.photo');
            Route::post('/{slug}/delete-photo', 'Admin\EmployeeController@deletePhoto')->name('admin.employee.photo.delete');
            Route::post('/{id}/add-subject', 'Admin\EmployeeController@addSubject')->name('admin.employee.add-subject');
            Route::post('/employee-subject/{id}/delete', 'Admin\EmployeeController@deleteSubject')->name('admin.employee.delete-subject');

            Route::post('/education/add', 'Admin\EmployeeController@addEducation')->name('admin.employees.add-education');
            Route::post('{employee_id}/education/{id}/delete', 'Admin\EmployeeController@deleteEducation')->name('admin.employee.delete-education');

            Route::get('/{id}/courses', 'Admin\EmployeeController@getCourses')->name('admin.employee.getcourses');
            Route::post('/{id}/positions/add', 'Admin\EmployeeController@addPosition')->name('admin.employee.add.position');
            Route::post('/{employee_id}/positions/{id}/delete', 'Admin\EmployeeController@deletePosition')->name('admin.employee.position.delete');
            Route::post('/position-layoff', 'Admin\EmployeeController@layoff_position')->name('admin.employee.layoff.position');
            Route::post('/courses/{id}', 'Admin\EmployeeController@deleteCourse')->name('admin.course.delete');

            Route::post('/add-course', 'Admin\EmployeeController@addCourse')->name('admin.employee.course.add');

            Route::get('/create', 'Admin\EmployeeController@createPage')->name('admin.employees.create');
            Route::post('/create', 'Admin\EmployeeController@create')->name('admin.employees.create.post');
            Route::post('/{id}/dismiss', 'Admin\EmployeeController@dismiss')->name('admin.employee.dismiss');
        });

        Route::group(['prefix' => 'support'], function () {
            Route::get('/form-data', 'HomeController@formData')->name('admin.form-data');
            Route::get('/test', 'TestController@create');
        });

        Route::group(['prefix' => 'enrollees'], function () {
            Route::get('/', 'Admin\EnrolleeController@index')->name('admin.enrollees');
            Route::post('/delete', 'Admin\EnrolleeController@delete')->name('admin.enrollees.delete');
            Route::get('/data', 'Admin\EnrolleeController@data')->name('admin.enrollees.data');

            Route::get('/create', 'Admin\EnrolleeController@createPage')->name('admin.enrollees.create.page');
            Route::get('/subdivisions-codes', 'Admin\EnrolleeController@getCodes')->name('admin.enrollees.getCodes');
            Route::post('/create', 'Admin\EnrolleeController@create')->name('admin.enrollees.create');

            Route::post('/enrollment', 'Admin\EnrolleeController@enrollment')->name('admin.enrollees.enrollment');

            Route::get('/{id}/profile', 'Admin\EnrolleeController@profile')->name('admin.enrollee.profile');
            Route::get('/{id}/edit', 'Admin\EnrolleeController@edit')->name('admin.enrollee.edit');
            Route::post('/{id}/update', 'Admin\EnrolleeController@update')->name('admin.enrollee.update');
            Route::post('/{id}/update-passport', 'Admin\EnrolleeController@updatePassport')->name('admin.enrollee.update-passport');
            Route::post('/{id}/update-education', 'Admin\EnrolleeController@updateEducation')->name('admin.enrollee.update-education');
            Route::post('/{id}/update-additional', 'Admin\EnrolleeController@updateAdditional')->name('admin.enrollee.update-additional');
            Route::post('/{id}/update-addresses', 'Admin\EnrolleeController@updateAddresses')->name('admin.enrollee.update-addresses');
        });

        Route::group(['prefix' => 'students'], function () {
            Route::get('/', 'Admin\StudentController@index')->name('admin.students');
            Route::get('/data', 'Admin\StudentController@data')->name('admin.students.data');
            Route::get('/{slug}/profile', 'Admin\StudentController@profile')->name('admin.students.profile');
            Route::get('/{slug}/edit', 'Admin\StudentController@edit')->name('admin.students.edit');
            Route::post('/{slug}/edit', 'Admin\StudentController@update')->name('admin.students.update');
            Route::post('/allocation', 'Admin\StudentController@allocation')->name('admin.students.allocation');
            Route::post('/transfer', 'Admin\StudentController@transferOtherGroup')->name('admin.students.transfer');
            Route::get('/create', 'Admin\StudentController@create')->name('admin.students.create');
            Route::post('/store', 'Admin\StudentController@store')->name('admin.students.store');
            Route::get('{id}/get-decrees', 'Admin\StudentController@getDecrees')->name('admin.student.get-decrees');
            Route::post('{id}/parent/add', 'Admin\StudentController@addParent')->name('admin.student.parent.add');
            Route::get('/count-report', 'Admin\StudentController@createCountReport')->name('admin.student.count.report');
            Route::get('/report-on-date', 'Admin\StudentController@reportOnDate')->name('admin.student.report-on-date');
            Route::get('/{id}/spravka-v-voenkomat', 'Admin\HelpController@voenkomat')->name('admin.student.spravka-v-voenkomat');
        });

        Route::group(['prefix' => 'administrations'], function () {
            Route::get('/', 'Admin\AdministrationEmployeeController@index')->name('admin.administrations');
            Route::get('/add', 'Admin\AdministrationEmployeeController@createPage')->name('admin.administrations.create.page');
            Route::post('/add', 'Admin\AdministrationEmployeeController@create')->name('admin.administrations.create');
            Route::get('/{id}/edit', 'Admin\AdministrationEmployeeController@editPage')->name('admin.administrations.edit.page');
            Route::post('/{id}/edit', 'Admin\AdministrationEmployeeController@edit')->name('admin.administrations.edit');
            Route::post('/{id}/delete', 'Admin\AdministrationEmployeeController@delete')->name('admin.administrations.delete');
        });

        Route::group(['prefix' => 'education-plans'], function () {
            Route::get('/', 'Admin\EducationPlanController@all')->name('admin.education-plans');
            Route::get('/data', 'Admin\EducationPlanController@data')->name('admin.education-plans.data');
            Route::get('/create', 'Admin\EducationPlanController@createPage')->name('admin.education-plans.createPage');
            Route::post('/create', 'Admin\EducationPlanController@create')->name('admin.education-plans.create');
            Route::get('/{id}/edit', 'Admin\EducationPlanController@editPage')->name('admin.education-plans.editPage');
            Route::post('/{id}/update', 'Admin\EducationPlanController@edit')->name('admin.education-plans.edit');
            Route::post('/{id}/delete', 'Admin\EducationPlanController@delete')->name('admin.education-plans.delete');
        });

        Route::group(['prefix' => 'social-partners'], function () {
            Route::get('/', 'Admin\SocialPartnerController@index')->name('admin.social-partners');
            Route::get('/create', 'Admin\SocialPartnerController@create')->name('admin.social-partners.create');
            Route::post('/store', 'Admin\SocialPartnerController@store')->name('admin.social-partners.store');
            Route::get('/{id}/edit', 'Admin\SocialPartnerController@edit')->name('admin.social-partners.edit');
            Route::post('/{id}/update', 'Admin\SocialPartnerController@update')->name('admin.social-partners.update');
            Route::post('/{id}/delete', 'Admin\SocialPartnerController@delete')->name('admin.social-partners.delete');
        });

        Route::group(['prefix' => 'meta-tags'], function () {
            Route::get('/', 'Admin\MetaTagController@index')->name('admin.meta-tags');
            Route::get('/create', 'Admin\MetaTagController@create')->name('admin.meta-tags.create');
            Route::post('/store', 'Admin\MetaTagController@store')->name('admin.meta-tags.store');
            Route::get('/{id}/edit', 'Admin\MetaTagController@edit')->name('admin.meta-tags.edit');
            Route::post('/{id}/update', 'Admin\MetaTagController@update')->name('admin.meta-tags.update');
        });


        Route::get('/site-news', 'Admin\NewsController@index')->name('admin.news');
        Route::get('/site-news/data', 'Admin\NewsController@data')->name('admin.news.data');
        Route::get('/site-news/create', 'Admin\NewsController@createPage')->name('admin.news.create');
        Route::post('/site-news/create', 'Admin\NewsController@create')->name('admin.news.create.post');
        Route::get('/site-news/{slug}/edit', 'Admin\NewsController@edit')->name('admin.news.edit');
        Route::post('/site-news/{slug}/edit', 'Admin\NewsController@update')->name('admin.news.update');
        Route::post('/site-news/{id}/delete', 'Admin\NewsController@delete')->name('admin.news.delete');

        Route::post('/news-file/{id}/rename', 'Admin\NewsController@editFileName')->name('admin.news-file.rename');
        Route::post('/news-file/{id}/delete', 'Admin\NewsController@deleteNewsFile')->name('admin.news-file.delete');

        Route::group(['prefix' => 'priem-programs'], function () {
            Route::get('/', 'Admin\PriemProgramController@index')->name('admin.priem');
            Route::get('/create', 'Admin\PriemProgramController@createPage')->name('admin.priem.create.page');
            Route::post('/make-report', 'Admin\PriemProgramController@makeReport')->name('admin.priem.make-report');
            Route::post('/create', 'Admin\PriemProgramController@create')->name('admin.priem.create');
            Route::post('/{id}/update', 'Admin\PriemProgramController@update')->name('admin.priem.update');
            Route::post('/{id}/delete', 'Admin\PriemProgramController@delete')->name('admin.priem.delete');
        });

        Route::group(['prefix' => 'training-groups'], function () {
            Route::get('/', 'Admin\GroupController@index')->name('admin.groups');
            Route::get('/data', 'Admin\GroupController@data')->name('admin.groups.data');
            Route::get('/create', 'Admin\GroupController@createPage')->name('admin.groups.create.page');
            Route::post('/create', 'Admin\GroupController@create')->name('admin.groups.create');
            Route::post('/release', 'Admin\GroupController@releaseGroup')->name('admin.groups.release');
            Route::post('/{id}/transfer', 'Admin\GroupController@transfer')->name('admin.groups.transfer');
            Route::post('/{id}/delete', 'Admin\GroupController@delete')->name('admin.groups.delete');
            Route::post('/{id}/print-references', 'Admin\GroupController@printReferences')->name('admin.groups.print-references');
            Route::get('/avg', 'Admin\GroupController@reportGroupAvg')->name('admin.groups.avg');
        });

        Route::get('/documents-categories', 'Admin\DocumentController@indexCategories')->name('admin.documents.categories');
        Route::get('/documents-categories/create', 'Admin\DocumentController@createCategoryPage')->name('admin.documents.categories.create.page');
        Route::post('/documents-categories/create', 'Admin\DocumentController@createCategory')->name('admin.documents.categories.create');
        Route::get('/documents-categories/{id}/edit', 'Admin\DocumentController@editCategoryPage')->name('admin.documents.categories.edit.page');
        Route::post('/documents-categories/{id}/edit', 'Admin\DocumentController@editCategory')->name('admin.documents.categories.edit');
        Route::post('/documents-categories/{id}/delete', 'Admin\DocumentController@deleteCategory')->name('admin.documents.categories.delete');

        Route::group(['prefix' => 'document'], function () {
            Route::get('/', 'Admin\DocumentController@indexDocuments')->name('admin.documents');
            Route::get('/data', 'Admin\DocumentController@documentsData')->name('admin.documents.data');
            Route::get('/create', 'Admin\DocumentController@documentCreatePage')->name('admin.documents.create.page');
            Route::post('/create', 'Admin\DocumentController@documentCreate')->name('admin.documents.create');
            Route::get('/{id}/edit', 'Admin\DocumentController@documentEditPage')->name('admin.documents.edit.page');
            Route::post('/{id}/edit', 'Admin\DocumentController@documentEdit')->name('admin.documents.edit');
            Route::post('/{id}/delete', 'Admin\DocumentController@documentDelete')->name('admin.documents.delete');
        });

        Route::group(['prefix' => 'reports'], function () {

            Route::group(['prefix' => 'enrollees'], function () {
                Route::get('/', 'Admin\EnrolleeController@reportPage')->name('admin.reports.enrollees');
                Route::post('/data', 'Admin\EnrolleeController@reportData')->name('admin.reports.enrollees.data');
                Route::post('/export/pdf', 'Admin\EnrolleeController@exportEnrolleeReport')->name('admin.reports.enrollees.export');
                Route::post('/export/excel', 'Admin\EnrolleeController@exportEnrolleeReportToExcel')->name('admin.reports.enrollees.export-to-excel');
            });
        });
        Route::get('/tochka-rosta', 'Admin\TochkaRostaController@index')->name('admin.tochka-rosta');
        Route::get('/tochka-rosta/data', 'Admin\TochkaRostaController@data')->name('admin.tochka-rosta.data');
        Route::get('/tochka-rosta/create', 'Admin\TochkaRostaController@create')->name('admin.tochka-rosta.create');
        Route::post('/tochka-rosta/store', 'Admin\TochkaRostaController@store')->name('admin.tochka-rosta.store');
        Route::get('/tochka-rosta/{slug}/edit', 'Admin\TochkaRostaController@edit')->name('admin.tochka-rosta.edit');
        Route::get('/tochka-rosta/{id}/photos', 'Admin\TochkaRostaController@photos')->name('admin.tochka-rosta.photos');
        Route::post('/tochka-rosta/{slug}/update', 'Admin\TochkaRostaController@update')->name('admin.tochka-rosta.update');
        Route::get('/tochka-rosta/{slug}/delete', 'Admin\TochkaRostaController@delete')->name('admin.tochka-rosta.delete');
        Route::post('/tochka-rosta/{id}/upload-photo', 'Admin\TochkaRostaController@uploadPhoto')->name('admin.tochka-rosta.upload-photo');
        Route::post('/tochka-rosta/{photo_id}/delete', 'Admin\TochkaRostaController@deleteTochkaPhoto')->name('admin.tochka-rosta.delete-photo');


        Route::get('/subjects', 'Admin\SubjectController@index')->name('admin.subjects');
        Route::get('/subjects/data', 'Admin\SubjectController@data')->name('admin.subjects.data');
        Route::get('/subjects/create', 'Admin\SubjectController@create')->name('admin.subjects.create');
        Route::post('/subjects/store', 'Admin\SubjectController@store')->name('admin.subjects.store');
        Route::get('/subjects/{id}/edit', 'Admin\SubjectController@edit')->name('admin.subjects.edit');
        Route::post('/subjects/{id}/update', 'Admin\SubjectController@update')->name('admin.subjects.update');
        Route::post('/subjects/{id}/delete', 'Admin\SubjectController@delete')->name('admin.subjects.delete');


        Route::get('/regulations', 'Admin\RegulationController@index')->name('admin.regulations');
        Route::get('/regulations/data', 'Admin\RegulationController@data')->name('admin.regulations.data');
        Route::get('/regulations/create', 'Admin\RegulationController@create')->name('admin.regulations.create');
        Route::post('/regulations/store', 'Admin\RegulationController@store')->name('admin.regulations.store');
        Route::get('/regulations/{id}/violations', 'Admin\RegulationController@violationsList')->name('admin.regulations.violations');
        Route::get('/regulations/{id}/create-violations', 'Admin\RegulationController@createViolation')->name('admin.regulations.create-violations');
        Route::post('/regulations/{id}/store-violations', 'Admin\RegulationController@storeViolation')->name('admin.regulations.store-violations');

        Route::group(['prefix' => 'helps'], function () {
            Route::get('training', 'Admin\HelpController@trainigHelp')->name('admin.helps.training');
            Route::get('training/{id}/print', 'Admin\HelpController@deliveryOrdered')->name('admin.helps.training.delivery');
        });

        Route::group(['prefix' => 'news-papers'], function () {
            Route::get('/', 'Admin\NewsPapersController@index')->name('admin.news-papers');
            Route::get('/data', 'Admin\NewsPapersController@data')->name('admin.news-papers.data');
            Route::get('/create', 'Admin\NewsPapersController@create')->name('admin.news-papers.create');
            Route::post('/store', 'Admin\NewsPapersController@store')->name('admin.news-papers.store');
            Route::get('/{id}/edit', 'Admin\NewsPapersController@edit')->name('admin.news-papers.edit');
            Route::post('/{id}/update', 'Admin\NewsPapersController@update')->name('admin.news-papers.update');
            Route::post('/{id}/delete', 'Admin\NewsPapersController@delete')->name('admin.news-papers.delete');
        });

        Route::group(['prefix' => 'references'], function () {
            Route::get('/archive', 'Admin\HelpController@archive')->name('admin.references.archive');
            Route::get('/data', 'Admin\HelpController@data')->name('admin.references.archive.data');
            Route::get('/export-money', 'Admin\HelpController@exportMoney')->name('admin.references.export.money');
        });

        Route::group(['prefix' => 'export'], function () {
            Route::get('/enrollees-to-asu-rso', 'Admin\ExportController@enrolleeToAsuRso')->name('admin.export.enrollees.asurso');
            Route::get('/parents', 'Admin\ExportController@parents')->name('admin.export.parents');
            Route::get('/base/{type}', 'Admin\ExportController@base')->name('admin.export.base');
        });

        Route::group(['prefix' => 'holidays'], function () {
            Route::get('/', 'Admin\HolidayController@index')->name('admin.holidays.index');
            Route::get('/data', 'Admin\HolidayController@data')->name('admin.holidays.data');
            Route::post('/store', 'Admin\HolidayController@store')->name('admin.holidays.store');
            Route::post('/{id}/update', 'Admin\HolidayController@update')->name('admin.holidays.update');
            Route::post('/{id}/destroy', 'Admin\HolidayController@destroy')->name('admin.holidays.destroy');
        });
    });

});
