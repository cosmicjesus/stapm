<?php

try {
    Breadcrumbs::for('client.index', function ($trail) {
        $trail->push('Главная', route('client.index'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.news', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Новости', route('client.news'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.news.one', function ($trail, $slug) {
        $news = \App\Models\News::findBySlugOrFail($slug);
        $trail->parent('client.news');
        $trail->push($news->title);
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.basic-information', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Основные сведения');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.management-structure', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Структура и органы управления образовательной организацией');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.documents', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Документы');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::for('client.education', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Образование', route('client.education'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::for('client.program_detail', function ($trail, $slug) {
        $program = \App\Models\ProfessionProgram::findBySlugOrFail($slug)->load('profession');
        $trail->parent('client.education');
        $trail->push($program->profession->code . " " . $program->profession->name, route('client.program_detail', ['slug' => $slug]));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::for('client.umk', function ($trail, $slug) {
        $trail->parent('client.program_detail', $slug);
        $trail->push('УМК');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.education.standarts', function ($trail) {
        $trail->parent('client.education');
        $trail->push('Образовательные стандарты');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.employees', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Руководство и педагогический состав', route('client.employees'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.employee_profile', function ($trail, $slug) {

        $employee = \App\Models\Employee::findBySlugOrFail($slug);

        $trail->parent('client.employees');
        $trail->push($employee->full_name);
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.material', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Материальное и техническое обеспечение');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::for('client.financial-and-economic-activity', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Финансово-хозяйственная деятельность',route('client.financial-and-economic-activity'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.financial-and-economic-activity.detail', function ($trail,$slug) {

        $category=\App\Models\CategoriesDocument::query()->where('slug',$slug)->firstOrFail();

        $trail->parent('client.financial-and-economic-activity');
        $trail->push('Финансово-хозяйственная деятельность за '.$category->name);
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::for('client.hot-line', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Горячая линия');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.paid-educational-services', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Платные образовательные услуги');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::for('client.scholarship', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Стипендия и иные виды материальной поддержки');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.acceptance-transfer', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Вакантные места для приема - перевода');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.admission', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Приемная комиссия');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.priem', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Информация о ходе приема', route('client.priem'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.priem.raiting', function ($trail, $slug) {

        $recruitProgram = \App\Models\PriemProgram::findBySlugOrFail($slug);
        $program = $recruitProgram->program;
        $value = "Рейтинг поданных заявлений по ";
        if ($program->type_id == 2) {
            $value .= "профессии ";
        } else {
            $value .= "спeциальности ";
        }
        $value .= $program->name_with_form;
        $trail->parent('client.priem');
        $trail->push($value);
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.open-door', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Дни открытых дверей');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.history', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Историческая справка');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.ovz', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Условия обучения для инвалидов и лиц с ограниченными возможностями здоровья');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.partnership', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Социальное партнерство');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.services.clinic', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Здравпункт');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.services.library', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Библиотека');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.services.el-library', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Электронная библиотека');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.services.canteen', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Столовая');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.services.information_security', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Информационная безопасность');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}


try {
    Breadcrumbs::for('client.extramural-department.index', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Основная информация');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.extramural-department.duties', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Обязанности студента заочника');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.code-of-honor', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Кодекс чести');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.memo-to-student', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Памятка студенту');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.anti-corruption', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Противодействие коррупции');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.order-references', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Заказ справок');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.holidays', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Выходные и праздничные дни');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.assistance', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Служба содействия трудоустройства выпускников');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.vospitanie', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Средства воспитания');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
//client.students.replaces

try {
    Breadcrumbs::for('client.students.replaces', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Замены занятий');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.services.psychologist', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Страничка психолога');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}


try {
    Breadcrumbs::for('client.feedback', function ($trail) {
        $trail->parent('client.index');
        $trail->push('Обратная связь');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.conferences.detail', function ($trail, $slug) {

        $conference = \App\Models\Conference::findBySlugOrFail($slug);
        $trail->parent('client.index');
        $trail->push($conference->title);
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}


//client.mediaAboutUs

try {
    Breadcrumbs::for('client.mediaAboutUs', function ($trail) {

        $trail->parent('client.index');
        $trail->push('СМИ о нас');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.may9', function ($trail) {

        $trail->parent('client.index');
        $trail->push('2020-Год памяти и славы: материалы в помощь');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.employees.information', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Сотруднику');
        $trail->push('Информация');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.services.psychologist.documents', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Психолого-педагогическое сопровождение');
        $trail->push('Документы');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.distance-learning', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Дистанционное обучение', route('client.distance-learning'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.dist-conc', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Мероприятия (конкурсы) для обучающихся с 6 по 30 апреля');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.distance-learning.time-table', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Расписание');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.distance-learning.hot-line', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Телефоны горячей линии для родителей и студентов');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.dist-dosug', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Досуговая деятельность');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.responsible-person', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Ответственные за организацию дистанционного обучения');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.dist.instructions', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Инструкции и рекомендации для обучающихся и родителей');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.dist.platforms', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Платформы');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
try {
    Breadcrumbs::for('client.dist.documents', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Об организации учебного процесса с применением электронного обучения и дистанционных
                            образовательных технологий');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.dist.military', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Военные сборы');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.dist.gia', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('ГИА');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.submission-form', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Онлайн подача документов');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.uploading-documents', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Догрузка документов');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
//client.uploading-documents
try {
    Breadcrumbs::for('client.dist.qualification-exam', function ($trail) {

        $trail->parent('client.distance-learning');
        $trail->push('Расписание квалификационных экзаменов');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}

try {
    Breadcrumbs::for('client.rooms', function ($trail) {

        $trail->parent('client.index');
        $trail->push('Cведения о наличии оборудованных учебных кабинетов');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {

}
