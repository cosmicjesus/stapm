<?php
Route::group(['domain' => env('DIST_DOMAIN')], function () {
    Route::group(['prefix' => 'api'], function () {
        Route::post('/login', 'Distance\LoginController@login');
        Route::group(['middleware' => 'jwt.auth'], function () {

            Route::get('/me', 'Distance\LoginController@profile');
        });
    });

    Route::get('/{any}', function () {
        return view('distance');
    })->where('any', '.*');
});
