<?php

use Illuminate\Http\Request;

Route::group(['domain' => env('V2_ADMIN_DOMAIN_FRONT'), 'middleware' => 'cors'], function () {
    Route::post('/login', 'Api\LoginController@login')->name('admin.api.login');
    //Route::get('/test', 'TestController@test');
    Route::group(['middleware' => 'jwt.auth'], function () {

        Route::get('/me', 'Api\LoginController@profile');
        Route::post('/logout', 'Api\LoginController@logout');
        /*Активность пользователей*/
        Route::get('/activity', 'Api\ActivityLogController@index');
        Route::get('/settings', 'Api\SettingsController@getSettings');
        Route::post('/settings', 'Api\SettingsController@updateSettings');

        Route::group(['prefix' => 'statistic'], function () {
            Route::get('/', 'Api\DashboardController@statistic');
            Route::get('/expired-passports', 'Api\DashboardController@expiredPassports');
            Route::post('/export-expired-passports', 'Api\DashboardController@exportExpiredPassport');
        });

        Route::group(['prefix' => 'profession'], function () {
            Route::get('/list', 'Api\ProfessionController@index');
            Route::post('/store', 'Api\ProfessionController@store');
            Route::put('/{id}/update', 'Api\ProfessionController@update');
            Route::delete('/{id}/delete', 'Api\ProfessionController@destroy');
        });
        Route::resource('positions', 'Api\PositionController', ['parameters' => ['position' => 'id']]);

        require base_path('routes/admin_api/profession_program.php');
        require base_path('routes/admin_api/profession_programs_academic_years.php');
        require base_path('routes/admin_api/students.php');
        require base_path('routes/admin_api/load_months.php');

        /*Роуты работы с пользователями*/
        Route::resource('users', 'Api\UserController', ['parameters' => ['user' => 'id']])->except('create');
        /*Роуты работы с пользователями*/


        /*Роуты работы с военкоматами*/
        Route::resource('recruitment-centers', 'Api\RecruitmentCenterController', ['parameters' => ['recruitment-center' => 'id']])->except('create');
        /*Роуты работы с военкоматами*/

        /*Роуты учебных планов*/
        require base_path('routes/admin_api/EducationPlan.php');
        /*Роуты учебных планов*/

        /*Роуты компонентов документов*/
        Route::resource('component-documents', 'Api\ComponentDocumentController');
        Route::post('component-documents/{id}/reload-file', 'Api\ComponentDocumentController@reloadFile');
        /*Роуты компонентов документов*/

        /*Роуты дисциплин*/
        Route::resource('subjects', 'Api\SubjectController', ['parametrs' => ['subject' => 'id']]);
        /*Роуты дисциплин*/

        /*Роуты для работы с сотрудниками*/
        require base_path('routes/admin_api/employee.php');
        /*Роуты для работы с сотрудниками*/


        require base_path('routes/admin_api/SelectionCommittee.php');

        /*Роуты работы с абитуриентами*/
        require base_path('routes/admin_api/entrants.php');
        /*Роуты работы с абитуриентами*/

        /*Общие роуты для студентов и абитуринетов*/
        require base_path('routes/admin_api/human.php');
        /*Общие роуты для студентов и абитуринетов*/

        Route::put('/contingent/{id}', 'Api\ContingentController@updateData');

        /*Роуты работы с документами*/
        Route::resource('documents', 'Api\DocumentController', ['parametrs' => ['document' => 'id']]);
        Route::post('/documents/{id}/reload-file', 'Api\DocumentController@reloadFile');
        /*Роуты работы с документами*/

        Route::group(['prefix' => 'references'], function () {
            Route::get('/list', 'Api\ReferencesController@referencesList');
            Route::post('/print-money-references', 'Api\ReferencesController@prinyMoneyList');
        });

        /*Роуты работы с приказами*/
        Route::resource('decrees', 'Api\DecreeController', ['parametrs' => ['decree' => 'id']]);
        Route::group(['prefix' => 'decrees'], function () {
            Route::post('/{id}/upload-file', 'Api\DecreeController@uploadFile');
        });
        /*Роуты работы с приказами*/

        /*Роуты работы с родителями*/
        Route::resource('parents', 'Api\ParentController', ['parametrs' => ['parent' => 'id']]);
        /*Роуты работы с родителями*/

        /*Роуты работы с учебными группами*/
        require base_path('routes/admin_api/training_groups.php');
        /*Роуты работы с учебными группами*/

        /*Роуты работы с учебными календарями*/
        require base_path('routes/admin_api/training_calendars.php');
        /*Роуты работы с учебными календарями*/

        /*Роуты для работы с нагрузкой*/
        require base_path('routes/admin_api/teacher_load.php');
        /*Роуты для работы с нагрузкой*/

        /*Роуты работы с отчетами*/
        Route::group(['prefix' => 'reports'], function () {
            Route::post('/count-contingent', 'Api\ReportController@countStudentsOnDate');
        });
        /*Роуты работы с отчетами*/

        /*Роуты для работы с предписаниями*/
        Route::resource('regulations', 'Api\RegulationController', ['parametrs' => ['regulation' => 'id']]);
        /*Роуты для работы с предписаниями*/

        /*Роуты для работы с новостями*/
        Route::resource('news', 'Api\NewsController', ['parametrs' => ['new' => 'id']]);
        /*Роуты для работы с новостями*/
        Route::resource('conferences', 'Api\ConferenceController', ['parametrs' => ['conference' => 'id']]);
        //conferences

//ReplaceClassController

        Route::resource('replace-classes', 'Api\ReplaceClassController', ['parametrs' => ['replace_class' => 'id']]);
        Route::group(['prefix' => 'export'], function () {
            Route::post('/base/{type}', 'Api\ExportController@base');
            Route::post('/recruits', 'Api\ExportController@recruits');
        });

        Route::group(['prefix' => 'services'], function () {
            Route::get('/professions', 'Api\ProfessionController@all');
            Route::get('/subjects', 'Api\SubjectController@all');
            Route::get('/profession-programs', 'Api\ProfessionProgramController@all');
            Route::get('/positions', 'Api\PositionController@all');
            Route::get('/document-categories', 'Api\DocumentCategoryController@all');
            Route::get('/training-groups', 'Api\TrainingGroupController@all');
            Route::get('/employees', 'Api\EmployeeController@all');
        });

        Route::group(['prefix' => 'cache'], function () {
            Route::get('subdivision-codes', 'Api\CacheController@subdivision');
            Route::get('personal-form-data', 'Api\CacheController@personalFormData');
            Route::get('birth-places', 'Api\CacheController@birthPlaces');
            Route::get('educational-organization', 'Api\CacheController@educationalOrganizations');
            Route::get('locations-of-educational-institutions', 'Api\CacheController@educationalInstitutions');
            Route::get('addresses-data', 'Api\CacheController@getAddressesData');
            Route::group(['prefix' => 'raw'], function () {
                Route::get('subdivision-codes', 'Api\CacheController@getRawSubdivision');
                Route::post('subdivision-codes', 'Api\CacheController@saveRawSubdivision');
            });
        });
    });
});
