<?php

require base_path('routes/distance.php');
Route::group(['domain' => 'samtapm.ru', 'middleware' => ['web', 'meta', 'site_bar']], function () {
    Route::get('/{any}', function () {
        return redirect()->route('client.index');
    })->where('any', '.*');
});
Route::group(['domain' => env('V2_ADMIN_DOMAIN_FRONT')], function () {
    Route::group(['prefix' => 'protected-files', ['middleware' => 'auth']], function () {
        Route::get('/students/{uuid}', 'Api\ProtectedFileController@getStudentFile')->name('api.students.files');
    });

    Route::group(['prefix' => 'export', ['middleware' => 'auth']], function () {
        Route::group(['prefix' => 'word', ['middleware' => 'auth']], function () {
            Route::get('t2-form/{id}', 'Api\ExportController@exportT2Form')->name('api.export.word.t2-form');
            Route::get('statement/{id}', 'Api\ExportController@exportStatement')->name('api.export.word.statement');
        });

        Route::group(['prefix' => 'pdf', ['middleware' => 'auth']], function () {
            Route::get('/list-for-military-service', 'Api\ExportController@listForMilitaryService')->name('api.export.military-list');
            Route::get('/report-of-manning', 'Api\SelectionCommitteeController@buildReportOfManning')->name('api.export.pdf.manning');
            Route::get('/entrant-report', 'Api\EntrantController@pdfEntrantReport')->name('api.export.pdf.entrant-report');
            Route::get('/students/{id?}/make-profile', 'Api\StudentController@profileToPdf')->name('api.export.pdf.student-profile-pdf');
        });
    });
    Route::get('/{any}', 'SpaController@index')->where('any', '.*');
});

Route::group(['domain' => env('V2_CLIENT_DOMAIN'), 'middleware' => ['meta', 'site_bar']], function () {

    Route::get('/test', 'TestController@test');

    Route::group(['prefix' => 'api'], function () {
        Route::get('/news/last/{count}', 'V2PublicPart\NewsController@last');
        Route::get('/news', 'V2PublicPart\NewsController@index')->name('api.news.list');
        Route::get('/annoncements', 'V2PublicPart\NewsController@annoncements')->name('api.annoncements');
        Route::get('/may9', 'V2PublicPart\NewsController@mayNews')->name('api.may9');
        Route::get('/regulations', 'V2PublicPart\IndexController@regulations')->name('api.regulations.list');
        Route::get('/replace-classes/{date}', 'V2PublicPart\ReplaceClassController@getReplaceClass')->name('client.students.replaces.data');
        Route::get('/profession-programs/active', 'V2PublicPart\EducationController@getOnlyActiveOnIndex')->name('client.programs.on-index');
        Route::group(['prefix' => 'employees'], function () {
            Route::get('/administration', 'V2PublicPart\EmployeeController@administration')->name('api.client.employees');
            Route::get('/pedagogical', 'V2PublicPart\EmployeeController@pedagogical')->name('api.client.pedagogical');
        });

        Route::get('active-recruitment-programs', 'Api\SelectionCommitteeController@activePrograms')->name('client.active-programs');

        Route::group(['prefix'=>'submission'],function(){
            Route::post('/create-statement','V2PublicPart\RecruitmentController@makeEnrollee')->name('client.api.create-statement');
            Route::post('/{id}/files','V2PublicPart\RecruitmentController@uploadFiles')->name('client.api.entrant.upload-files');
            Route::post('/check-entrant','V2PublicPart\RecruitmentController@checkEntrant')->name('client.api.entrant.check');
            Route::post('/check','V2PublicPart\RecruitmentController@check')->name('client.api.entrant.check-entrant');
        });

        Route::group(['prefix' => 'services'], function () {
            Route::get('/training-groups', 'Api\TrainingGroupController@all')->name('api.services.training-groups');
        });
    });

    Route::get('/', 'V2PublicPart\IndexController@index')->name('client.index');
    Route::get('/feedback', 'V2PublicPart\IndexController@feedback')->name('client.feedback');
    Route::get('/may9', 'V2PublicPart\IndexController@may9')->name('client.may9');
    Route::post('/feedback', 'V2PublicPart\IndexController@feedbackSend')->name('client.feedback.send');
    Route::get('/basic-information', 'V2PublicPart\IndexController@basicInformation')->name('client.basic-information');
    Route::get('/documents', 'V2PublicPart\IndexController@documents')->name('client.documents');
    Route::get('/media-about-us', 'V2PublicPart\IndexController@mediaAboutUs')->name('client.mediaAboutUs');
    Route::get('/prevention', 'V2PublicPart\IndexController@prevention')->name('client.prevention');

    Route::group(['prefix' => 'distance-learning'], function () {
        Route::get('/', 'V2PublicPart\DistanceLearningController@index')->name('client.distance-learning');
        Route::get('/time-table', 'V2PublicPart\DistanceLearningController@timetable')->name('client.distance-learning.time-table');
        Route::get('/hot-line', 'V2PublicPart\DistanceLearningController@hotLine')->name('client.distance-learning.hot-line');
        Route::get('/contests', function () {
            return view('client.mer_dist');
        })->name('client.dist-conc');
        Route::get('/leisure-activities', function () {
            return view('client.dist.dosug');
        })->name('client.dist-dosug');

        Route::get('/documents', function () {
            return view('client.dist.documents');
        })->name('client.dist.documents');

        Route::get('/military-training', function () {
            $data = [
                [
                    'day_name' => 'Понедельник',
                    'date' => '18.05.2020',
                    'group' => [
                        [
                            'name' => 12,
                            'lessons' => '1-4',
                            'link' => 'https://cloud.mail.ru/public/3XRR/3db63deMB'
                        ],
                        [
                            'name' => '12А',
                            'lessons' => '5-8',
                            'link' => 'https://cloud.mail.ru/public/4Bot/RtWEPPtQK'
                        ]
                    ]
                ],
                [
                    'day_name' => 'Вторник',
                    'date' => '19.05.2020',
                    'group' => [
                        [
                            'name' => 12,
                            'lessons' => '1-4',
                            'link' => 'https://cloud.mail.ru/public/5rSL/3RmHQXZeQ'
                        ],
                        [
                            'name' => '12А',
                            'lessons' => '5-8',
                            'link' => 'https://cloud.mail.ru/public/Lce8/3mfWf9s33'
                        ]
                    ]
                ],
                [
                    'day_name' => 'Среда',
                    'date' => '20.05.2020',
                    'group' => [
                        [
                            'name' => '12А',
                            'lessons' => '1-4',
                            'link' => 'https://cloud.mail.ru/public/ANsb/2NZEnvMJh'
                        ]
                    ]
                ],
                [
                    'day_name' => 'Четверг',
                    'date' => '21.05.2020',
                    'group' => [
                        [
                            'name' => '14',
                            'lessons' => '1-4',
                            'link' => 'https://cloud.mail.ru/public/3Rdv/34rzv4DXb'
                        ],
                        [
                            'name' => '16',
                            'lessons' => '5-8',
                            'link' => 'https://cloud.mail.ru/public/5CrJ/43nT9RtMy'
                        ]
                    ]
                ],
                [
                    'day_name' => 'Пятница',
                    'date' => '22.05.2020',
                    'group' => [
                        [
                            'name' => '14',
                            'lessons' => '5-8',
                            'link' => 'https://cloud.mail.ru/public/3Rdv/34rzv4DXb'
                        ],
                    ]
                ]
            ];

            return view('client.dist.military', compact('data'));

        })->name('client.dist.military');

        Route::get('/responsible-person', function () {
            $peoples = [
                [
                    'name' => 'Мальцев Николай Григорьевич',
                    'position' => 'Заместитель директора по учебно – производственной работе',
                    'otv' => 'За общую организацию дистанционного обучени',
                    'phone' => '+7906347299',
                    'email' => 'nik-malcev@yandex.ru',
                    'social' => ['viber', 'whatsup']
                ],
                [
                    'name' => 'Кривчун Наталья Васильевна',
                    'position' => 'Заместитель директора по учебной работе',
                    'otv' => 'За контроль выполнения учебного плана в полном объеме, заполнение формы расписания, предложенной министерством образования и науки Самарской области',
                    'phone' => '+79279006072',
                    'email' => 'nata100363@bk.ru',
                    'social' => ['viber']
                ],
                [
                    'name' => 'Ляпнев Александр Викторович',
                    'position' => 'Методист',
                    'otv' => 'За проведение учебной и производственной практик на период дистанционного обучения',
                    'phone' => '+79171681631',
                    'email' => 'alexvicl@mail.ru',
                    'social' => ['viber']
                ],
                [
                    'name' => 'Губарь Анна Сергеевна',
                    'position' => 'Заместитель директора по методической работе',
                    'otv' => 'За организация методической поддержки преподавателям и мастерам производственного обучения в период дистанционного обучения',
                    'phone' => '+79372099424',
                    'email' => 'mianna83@mail.ru',
                    'social' => ['viber']
                ],
                [
                    'name' => 'Черникова Ирина Михайловна',
                    'position' => 'Заместитель директора по воспитательной работе',
                    'otv' => 'За организацию работы социально – педагогической службы и психологическую поддержку в период дистанционного обучения',
                    'phone' => '+79270107427',
                    'email' => 'kieva-i@mail.ru',
                    'social' => ['viber']
                ], [
                    'name' => 'Силантьев Евгений Алексеевич',
                    'position' => 'Техник программист',
                    'otv' => 'За обеспечение технической поддержки информационных систем и платфор',
                    'phone' => '',
                    'email' => 'silantev-zhenya@mail.ru',
                    'social' => []
                ]
            ];
            return view('client.dist.responsible-person', compact('peoples'));
        })->name('client.responsible-person');
        Route::get('/instructions', function () {
            return view('client.dist.instructions');
        })->name('client.dist.instructions');

        Route::get('/platforms', function () {
            return view('client.dist.platforms');
        })->name('client.dist.platforms');

        Route::get('/gia', function () {
            return view('client.dist.gia');
        })->name('client.dist.gia');

        Route::get('/qualification-exam', function () {
            return view('client.docs',['category'=>'qualification_exam']);
        })->name('client.dist.qualification-exam');
        //instructions
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'V2PublicPart\NewsController@list')->name('client.news');
        Route::get('/{slug}', 'V2PublicPart\NewsController@one')->name('client.news.one');
    });

    Route::group(['prefix' => 'education'], function () {
        Route::get('/', 'V2PublicPart\EducationController@education')->name('client.education');
        Route::get('/standarts', 'V2PublicPart\EducationController@standarts')->name('client.education.standarts');

        Route::get('/{slug}', 'V2PublicPart\EducationController@detailProgram')->name('client.program_detail');
        Route::get('/{slug}/umk', 'V2PublicPart\EducationController@umk')->name('client.umk');
    });

    Route::group(['prefix' => 'employees'], function () {
        Route::get('/', 'V2PublicPart\EmployeeController@all')->name('client.employees');
        Route::get('/information', 'V2PublicPart\EmployeeController@information')->name('client.employees.information');
        Route::get('/{slug}', 'V2PublicPart\EmployeeController@profile')->name('client.employee_profile');
    });

    Route::get('/hot-line', 'V2PublicPart\IndexController@hotLine')->name('client.hot-line');
    Route::get('/rooms', 'V2PublicPart\IndexController@rooms')->name('client.rooms');
    Route::get('/material', 'V2PublicPart\IndexController@material')->name('client.material');
    Route::get('/management-structure', 'V2PublicPart\IndexController@structura')->name('client.management-structure');

    Route::group(['prefix'=>'financial-and-economic-activity'],function (){
        Route::get('/', 'V2PublicPart\IndexController@fhd')->name('client.financial-and-economic-activity');
        Route::get('/{slug}', 'V2PublicPart\IndexController@detailFhd')->name('client.financial-and-economic-activity.detail');
    });


    Route::get('/acceptance-transfer', 'V2PublicPart\IndexController@acceptanceTransfer')->name('client.acceptance-transfer');
    Route::get('/scholarship-and-other-material-support', 'V2PublicPart\IndexController@scholarship')->name('client.scholarship');
    Route::get('/paid-educational-services', 'V2PublicPart\IndexController@paidEducational')->name('client.paid-educational-services');

    Route::group(['prefix' => 'entrant'], function () {
        Route::get('/admission-сommittee', 'V2PublicPart\EntrantController@admission')->name('client.admission');
        Route::get('/open-door', 'V2PublicPart\EntrantController@openDoor')->name('client.open-door');
        Route::get('/ovz', 'V2PublicPart\EntrantController@ovz')->name('client.ovz');
        Route::get('/history', 'V2PublicPart\EntrantController@history')->name('client.history');
        Route::get('/reception', 'V2PublicPart\EntrantController@priem')->name('client.priem');
        Route::get('/reception/{slug}/raiting', 'V2PublicPart\EntrantController@raiting')->name('client.priem.raiting');
        Route::get('/partnership', 'V2PublicPart\EntrantController@partnership')->name('client.partnership');
        Route::get('/submission-form', function (){
            return view('client.entrant.submission-form');
        })->name('client.submission-form');

        Route::get('/uploading-documents',function (){
            return view('client.entrant.uploading-documents');
        })->name('client.uploading-documents');
    });

    Route::group(['prefix' => 'announcements'], function () {
        Route::get('/', 'PublicPart\AnnouncementController@index')->name('client.announcements.index');
        Route::get('/{slug}', 'PublicPart\AnnouncementController@show')->name('client.announcements.show');
    });

    Route::group(['prefix' => 'students'], function () {
//        Route::get('/tochka-rosta', 'PublicPart\TochkaRostaController@index')->name('client.tochka');
//        Route::get('/tochka-rosta/{slug}', 'PublicPart\TochkaRostaController@one')->name('client.tochka.one');
        Route::get('/code-of-honor', 'V2PublicPart\StudentController@codeOfHonor')->name('client.code-of-honor');
        Route::get('/memo-to-student', 'V2PublicPart\StudentController@memoToStudent')->name('client.memo-to-student');
        Route::get('/anti-corruption', 'V2PublicPart\StudentController@antiCorruption')->name('client.anti-corruption');
        Route::get('/order-references', 'V2PublicPart\ReferencesController@referencePage')->name('client.order-references');
        Route::get('/order-references/data-form', 'V2PublicPart\ReferencesController@getDataForForm')->name('client.order-references.data');
        Route::post('/order-references', 'V2PublicPart\ReferencesController@orderReference')->name('client.order-reference.order');
        //Route::get('/news-papers', 'PublicPart\StaticPagesController@newsPapers')->name('client.news-papers');
        Route::get('/holidays', 'V2PublicPart\StudentController@holidays')->name('client.holidays');
        Route::get('/employment-assistance-service', 'V2PublicPart\StudentController@assistance')->name('client.assistance');
        Route::get('/vospitanie', 'V2PublicPart\StudentController@vospitanie')->name('client.vospitanie');
        Route::get('/replace-classes', 'V2PublicPart\ReplaceClassController@index')->name('client.students.replaces');
    });

    Route::group(['prefix' => 'extramural-department'], function () {
        Route::get('/', 'V2PublicPart\ExtramuralController@index')->name('client.extramural-department.index');
        Route::get('/duties-of-part-time-students', 'V2PublicPart\ExtramuralController@duties')->name('client.extramural-department.duties');
    });

    Route::group(['prefix' => 'conferences-and-competitions'], function () {
        Route::get('/', function () {
            abort(404);
        });
        Route::get('/{slug}', 'V2PublicPart\ConferenceController@detail')->name('client.conferences.detail');
    });

    Route::group(['prefix' => 'services'], function () {
        Route::get('/clinic', 'V2PublicPart\ServicesController@clinic')->name('client.services.clinic');
        Route::get('/psychologist-page', 'V2PublicPart\ServicesController@psychologist')->name('client.services.psychologist');
        Route::get('/psychologist/documents', 'V2PublicPart\ServicesController@psychologistDocuments')->name('client.services.psychologist.documents');
        Route::get('/library', 'V2PublicPart\ServicesController@library')->name('client.services.library');
        Route::get('/electronic-library', 'V2PublicPart\ServicesController@elLibrary')->name('client.services.el-library');
        Route::get('/canteen', 'V2PublicPart\ServicesController@canteen')->name('client.services.canteen');
        Route::get('/information-security', 'V2PublicPart\ServicesController@informationSecurity')->name('client.services.information_security');
    });
});
