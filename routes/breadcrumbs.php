<?php

use App\Models\Employee;
use App\Models\News;
use DaveJamesMiller\Breadcrumbs\Facades\DuplicateBreadcrumbException;

include base_path('routes/v2breadrcumbs.php');

try {
    Breadcrumbs::register('basic-information', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Основные сведения');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('hot-line', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Горячая линия');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('education', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Образование');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('announcements.index', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Анонсы / события');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('services.information_security', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Информационная безопасность');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('assistance', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Служба содействия трудоустройству выпускников');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('announcements.show', function ($breadcrumbs, $slug) {
        $item = News::query()->whereIn('type', [2, 3])->where('slug',$slug)->first();
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Анонсы и события', route('announcements.index'));
        $breadcrumbs->push($item->title);
    });
} catch (DuplicateBreadcrumbException $e) {
}
//show
try {
    Breadcrumbs::register('umk', function ($breadcrumbs, $slug) {

        $model = \App\Models\ProfessionProgram::with(['profession', 'education_form'])->where('slug', $slug)->first();

        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Образование', route('education'));
        $breadcrumbs->push($model->profession->code . " " . $model->profession->name, route('program_detail', ['slug' => $slug]));
        $breadcrumbs->push('УМК ' . $model->profession->code . " " . $model->profession->name);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('program_detail', function ($breadcrumbs, $slug) {

        $model = \App\Models\ProfessionProgram::with(['profession', 'education_form'])->where('slug', $slug)->first();

        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Образование', route('education'));
        $breadcrumbs->push($model->profession->code . " " . $model->profession->name);
    });
} catch (DuplicateBreadcrumbException $e) {
}
//employee_profile
try {
    Breadcrumbs::register('employees', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Руководство и педагогический состав');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('holidays', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Выходные и праздничные дни');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('administrations', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Администрация');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('services.canteen', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Столовая');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('employee_profile', function ($breadcrumbs, $slug) {
        $employee = \App\Models\Employee::findBySlug($slug);
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Сотрудники', route('employees'));
        $breadcrumbs->push($employee->lastname . " " . $employee->firstname . " " . $employee->middlename);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('news', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Новости');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('news.one', function ($breadcrumbs, $slug) {
        $news = News::findBySlugOrFail($slug);
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Новости', route('news'));
        $breadcrumbs->push($news->title);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('acceptance-transfer', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Вакантные места для приема-перевода');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('education.standarts', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Образовательные стандарты');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('paid-educational-services', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Платные образовательные услуги');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('priem', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Информация о ходе приема');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('priem.raiting', function ($breadcrumbs, $slug) {
        $program = App\BusinessLogic\ProfessionProgram\ProfessionProgram::buildBySlug($slug);

        $value = "Рейтинг поданных заявлений по ";
        if ($program->getTypeId() == 2) {
            $value .= "профессии ";
        } else {
            $value .= "спeциальности ";
        }

        $value .= $program->getNameWithForm();
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Информация о ходе приема', route('priem'));
        $breadcrumbs->push($value);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('partnership', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Социальное партнерство');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('history', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Историческая справка');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('scholarship', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Стипендия и иные виды материальной поддержки');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('material', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Материально-техническое обеспечение');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('services.clinic', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Здравпункт');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('services.library', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Библиотека');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('services.el-library', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Электронная библиотека');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('vospitanie', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Средства воспитания');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('management-structure', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Структура управления');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('financial-and-economic-activity', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Финансово-хозяйственная деятельность');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('documents', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Документы');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('rooms', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Сведения о наличии оборудованных учебных кабинетов и объектов для проведения практических занятий');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admission', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Приемная комиссия');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('open-door', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Дни открытых дверей');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('ovz', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Условия обучения инвалидов и лиц с ограниченными возможностями здоровья');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('tochka', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Точка Роста');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('tochka.one', function ($breadcrumbs, $slug) {

        $model = \App\Models\TochkaRostaItem::findBySlugOrFail($slug);

        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Точка Роста', route('tochka'));
        $breadcrumbs->push($model->title);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('code-of-honor', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Кодекс чести студентов СТАПМ');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('memo-to-student', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Памятка студенту');
    });
} catch (DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('anti-corruption', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Противодействие коррупции');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('extramural-department.index', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Заочное отделение', route('extramural-department.index'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('extramural-department.duties', function ($breadcrumbs) {
        $breadcrumbs->parent('extramural-department.index');
        $breadcrumbs->push('Обязанности студента заочника');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('order-references', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Заказ справок');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('news-papers', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('home'));
        $breadcrumbs->push('Студенческая газета');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.index', function ($breadcrumbs) {
        $breadcrumbs->push('Главная');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.employees', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Сотрудники');
    });
} catch (DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('admin.employee.edit', function ($breadcrumbs, $slug) {

        $employee = Employee::findBySlug($slug);

        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Сотрудники', route('admin.employees'));
        $breadcrumbs->push($employee->lastname . " " . $employee->firstname . " " . $employee->middlename, route('admin.employee.profile', ['slug' => $slug]));
        $breadcrumbs->push('Редактирование');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('admin.employees.create', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Сотрудники', route('admin.employees'));
        $breadcrumbs->push('Добавление сотрудника');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.employee.profile', function ($breadcrumbs, $slug) {
        $model = \App\Models\Employee::findBySlug($slug);
        $employee = new \App\BusinessLogic\Employee\Employee($model);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Сотрудники', route('admin.employees'));
        $breadcrumbs->push($employee->getFullName());

    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.programs', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('ОПОП');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.programs.createPage', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('ОПОП', route('admin.programs'));
        $breadcrumbs->push('Добавление ОПОП');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.programs.edit', function ($breadcrumbs, $slug) {

        $program = App\BusinessLogic\ProfessionProgram\ProfessionProgram::buildBySlug($slug);

        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('ОПОП', route('admin.programs'));
        $breadcrumbs->push('Редактирование ' . $program->getNameWithForm());
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.programs.detail', function ($breadcrumbs, $slug) {
        $model = \App\Models\ProfessionProgram::findBySlug($slug);

        $program = new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($model);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('ОПОП', route('admin.programs'));
        $breadcrumbs->push($program->getName() . " | " . $program->getEducationForm());
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.positions', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Должности');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.position.create', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Должности', route('admin.positions'));
        $breadcrumbs->push('Добавление должности');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.position.edit', function ($breadcrumbs, $id) {
        $position = \App\Models\Position::find($id);

        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Должности', route('admin.positions'));
        $breadcrumbs->push('Редактирование должности ' . $position->name);
    });
} catch (DuplicateBreadcrumbException $e) {
}
//admin.education-plans.createPage
try {
    Breadcrumbs::register('admin.education-plans', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Учебные планы');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.education-plans.createPage', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Учебные планы', route('admin.education-plans'));
        $breadcrumbs->push('Добавление учебного плана');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.education-plans.editPage', function ($breadcrumbs, $id) {

        $plan = \App\Models\EducationPlan::findOrFail($id);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Учебные планы', route('admin.education-plans'));
        $breadcrumbs->push($plan->name);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.news', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Новости');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.news.create', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Новости', route('admin.news'));
        $breadcrumbs->push('Добавить новость');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.news.edit', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Новости', route('admin.news'));
        $breadcrumbs->push('Редактирование новости');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.administrations', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Администрация техникума');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.administrations.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Администрация техникума', route('admin.administrations'));
        $breadcrumbs->push('Добавить сотрудника');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.administrations.edit.page', function ($breadcrumbs, $id) {
        $model = \App\Models\AdministrationEmployee::with('employee')->findOrFail($id);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Администрация техникума', route('admin.administrations'));
        $breadcrumbs->push('Редактирование записи сотрудника ' . $model->employee->lastname . " " . $model->employee->firstname . " " . $model->employee->middlename);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.priem', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Таблица приема');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.priem.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Таблица приема', route('admin.priem'));
        $breadcrumbs->push('Добавить программу');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.priem.edit.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Таблица приема', route('admin.priem'));
        $breadcrumbs->push('Редактирование записи');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.groups', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Учебные группы');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.groups.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Учебные группы', route('admin.groups'));
        $breadcrumbs->push('Добавить группу');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.documents.categories', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Категории документов');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.documents.categories.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Категории документов', route('admin.documents.categories'));
        $breadcrumbs->push('Добавить категорию');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.documents.categories.edit.page', function ($breadcrumbs, $id) {
        $model = \App\Models\CategoriesDocument::find($id);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Категории документов', route('admin.documents.categories'));
        $breadcrumbs->push('Редактирование категории «' . $model->name . '»');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.documents', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Документы');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.documents.edit.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Документы', route('admin.documents'));
        $breadcrumbs->push('Редактирование документа');
    });
} catch (DuplicateBreadcrumbException $e) {
}


try {
    Breadcrumbs::register('admin.documents.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Документы', route('admin.documents'));
        $breadcrumbs->push('Добавить документ');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.enrollees', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Абитуриенты');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.enrollee.profile', function ($breadcrumbs, $id) {

        $enrollee = \App\Models\Student::query()->findOrFail($id);
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Абитуриенты', route('admin.enrollees'));
        $breadcrumbs->push(massStrReplace('{last_name} {first_name} {middle_name}', [
                'last_name' => $enrollee->lastname,
                'first_name' => $enrollee->firstname,
                'middle_name' => $enrollee->middlename
            ])
        );
    });
} catch (DuplicateBreadcrumbException $e) {
}

Breadcrumbs::register('admin.enrollee.edit', function ($breadcrumbs, $id) {
    $enrollee = \App\Models\Student::findORFail($id);
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Абитуриенты', route('admin.enrollees'));
    $breadcrumbs->push(massStrReplace('{last_name} {first_name} {middle_name}', [
            'last_name' => $enrollee->lastname,
            'first_name' => $enrollee->firstname,
            'middle_name' => $enrollee->middlename
        ])
    );
    $breadcrumbs->push('Редактирование');
});

try {
    Breadcrumbs::register('admin.enrollees.create.page', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Абитуриенты', route('admin.enrollees'));
        $breadcrumbs->push('Добавить абитуриента');
    });
} catch (DuplicateBreadcrumbException $e) {
}
try {
    Breadcrumbs::register('admin.reports.enrollees', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Отчет по абитуриентам');
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.students', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Студенты');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.students.create', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Студенты', route('admin.students'));
        $breadcrumbs->push('Добавить студента');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.students.profile', function ($breadcrumbs, $slug) {
        $model = \App\Models\Student::findBySlugOrFail($slug);

        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Студенты', route('admin.students'));
        $breadcrumbs->push($model->lastname . " " . $model->firstname . " " . $model->middlename);
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.students.edit', function ($breadcrumbs, $slug) {
        $model = \App\Models\Student::findBySlugOrFail($slug);

        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Студенты', route('admin.students'));
        $breadcrumbs->push($model->lastname . " " . $model->firstname . " " . $model->middlename, route('admin.students.profile', ['slug' => $model->slug]));
        $breadcrumbs->push('Редактирование');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.users.index', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Пользователи');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.users.create', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Пользователи', route('admin.users.index'));
        $breadcrumbs->push('Добавить пользователя');
    });
} catch (DuplicateBreadcrumbException $e) {
}

try {
    Breadcrumbs::register('admin.users.edit', function ($breadcrumbs) {
        $breadcrumbs->push('Главная', route('admin.index'));
        $breadcrumbs->push('Пользователи', route('admin.users.index'));
        $breadcrumbs->push('Редактировать пользователя');
    });
} catch (DuplicateBreadcrumbException $e) {
}


Breadcrumbs::register('admin.tochka-rosta', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Точка Роста');
});


Breadcrumbs::register('admin.tochka-rosta.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Точка Роста', route('admin.tochka-rosta'));
    $breadcrumbs->push('Добавить событие');
});

Breadcrumbs::register('admin.tochka-rosta.photos', function ($breadcrumbs, $id) {
    $model = \App\Models\TochkaRostaItem::findOrFail($id);
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Точка Роста', route('admin.tochka-rosta'));
    $breadcrumbs->push('Фотографии события «' . $model->title . '»');
});


Breadcrumbs::register('admin.subjects', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Дисциплины');
});

Breadcrumbs::register('admin.subjects.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Дисциплины', route('admin.subjects'));
    $breadcrumbs->push('Добавить дисциплину');
});

Breadcrumbs::register('admin.subjects.edit', function ($breadcrumbs, $id) {

    $subject = \App\Models\Subject::findOrFail($id);
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Дисциплины', route('admin.subjects'));
    $breadcrumbs->push('Редактирование дисциплины «' . $subject->name . '»');
});

Breadcrumbs::register('admin.regulations', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Предписания органов осуществляющих надзор');
});

Breadcrumbs::register('admin.regulations.violations', function ($breadcrumbs, $id) {

    $regulation = \App\BusinessLogic\Regulation\Regulation::findById($id);

    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Предписания органов осуществляющих надзор', route('admin.regulations'));
    $breadcrumbs->push('Предписание ' . " " . $regulation->getNameOfTheSupervisory() . " от " . $regulation->getDate());
});

Breadcrumbs::register('admin.regulations.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Предписания органов осуществляющих надзор', route('admin.regulations'));
    $breadcrumbs->push('Добавить запись');
});

Breadcrumbs::register('admin.regulations.create-violations', function ($breadcrumbs, $id) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Предписания органов осуществляющих надзор', route('admin.regulations'));
    $breadcrumbs->push('Добавить запись');

});

Breadcrumbs::register('admin.social-partners', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Социальные партнеры');
});

Breadcrumbs::register('admin.social-partners.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Социальные партнеры', route('admin.social-partners'));
    $breadcrumbs->push('Добавить');
});

Breadcrumbs::register('admin.meta-tags', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Мета-теги страниц');
});

Breadcrumbs::register('admin.meta-tags.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Мета-теги страниц', route('admin.meta-tags'));
    $breadcrumbs->push('Добавить');
});


Breadcrumbs::register('admin.meta-tags.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Мета-теги страниц', route('admin.meta-tags'));
    $breadcrumbs->push('Обновление');
});

Breadcrumbs::register('admin.site-settings', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Настройки сайта');
});

Breadcrumbs::register('admin.news-papers', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Газета техникума');
});

Breadcrumbs::register('admin.news-papers.create', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Газета техникума', route('admin.news-papers'));
    $breadcrumbs->push('Добавить выпуск');
});

Breadcrumbs::register('admin.references.archive', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Справки');
});

Breadcrumbs::register('admin.holidays.index', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('Праздники');
});

Breadcrumbs::register('admin.qr', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.index'));
    $breadcrumbs->push('QR Коды');
});
