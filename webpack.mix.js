const mix = require('laravel-mix');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

mix.webpackConfig({
    plugins: [
        new BrowserSyncPlugin(
            {
                host: 'localhost',
                port: 3000,
                proxy: 'http://stapm.test',
                files: [
                    './**/*.css',
                    './app/**/*',
                    './config/**/*',
                    './resources/views/**/*',
                    './resources/assets/**/*',
                    './resources/styles/**/*',
                    './resources/js/**/*',
                    './routes/**/*'
                ],
                watchOptions: {
                    usePolling: true,
                    interval: 500
                },
                open: true
            },
            {
                reload: false
            }
        )
    ]
})

var SOURCEPATHS = {
    sassSource: 'resources/assets/sass/**/*.scss',
    sassAdmin: 'resources/assets/admin/style/*.scss',
    sassApp: 'src/scss/*.scss',
    pugSource: 'src/templates/**/*.pug',
    jsSource: 'resources/assets/js/main.js',
    jsSourceAdm: 'resources/assets/admin/scripts/admin-scripts.js',
    jsSourceWatch: 'resources/assets/js/*.js',
    jsSourceWatchAdmin: 'resources/assets/admin/scripts/*.js',
    imgSource: 'src/img/**'
}
let adminvensors = {
    js: [
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/babel-polyfill/dist/polyfill.min.js",
        "node_modules/bootstrap/dist/js/bootstrap.js",
        "resources/assets/admin/vendor/js/adminlte.min.js",
        "node_modules/lodash/lodash.js",
        "node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
        "node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js",
        "node_modules/datatables.net/js/jquery.dataTables.js",
        "node_modules/datatables.net-bs/js/dataTables.bootstrap.js",
        "node_modules/datatables.net-buttons/js/dataTables.buttons.js",
        "node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js",
        "node_modules/datatables.net-buttons/js/buttons.print.js",
        "node_modules/moment/min/moment-with-locales.js",
        "node_modules/sweetalert2/dist/sweetalert2.all.js",
        "resources/assets/admin/vendor/js/bootstrap3-wysihtml5.all.js",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js",
        "node_modules/select2/dist/js/select2.full.js",
        'resources/assets/admin/vendor/js/dataTables.checkboxes.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.min.js',
        'node_modules/jquery-validation/dist/additional-methods.js',
        'node_modules/jquery-validation/dist/localization/messages_ru.js',
        'resources/assets/admin/vendor/js/jquery.inputmask.bundle.min.js',
        "node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js",
        "node_modules/axios/dist/axios.min.js"

    ],
    style: [
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "node_modules/font-awesome/css/font-awesome.min.css",
        "resources/assets/admin/vendor/style/AdminLTE.min.css",
        "resources/assets/admin/vendor/style/_all-skins.min.css",
        "node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
        "node_modules/datatables.net-bs/css/dataTables.bootstrap.css",
        "node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.css",
        "node_modules/sweetalert2/dist/sweetalert2.css",
        "resources/assets/admin/vendor/style/bootstrap3-wysihtml5.min.css",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
        "node_modules/select2/dist/css/select2.min.css",
        "resources/assets/admin/vendor/style/dataTables.checkboxes.css",
        'resources/assets/admin/vendor/style/inputmask.css',
        "node_modules/easy-autocomplete/dist/easy-autocomplete.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.themes.css"
    ]
}

let clientvendors = {
    js: [
        "resources/assets/jquery.js",
        "node_modules/babel-polyfill/dist/polyfill.min.js",
        "node_modules/lodash/lodash.js",
        "node_modules/datatables.net/js/jquery.dataTables.js",
        "node_modules/datatables.net-responsive/js/dataTables.responsive.js",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js",
        "resources/assets/jquery.cookie.min.js",
        "node_modules/owl.carousel/dist/owl.carousel.min.js",
        "resources/assets/vendor/js/tabulous.min.js",
        "node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js",
        "node_modules/axios/dist/axios.min.js",
        'resources/assets/js/main.js'
    ],
    style: [
        "node_modules/normalize-css/normalize.css",
        "node_modules/font-awesome/css/font-awesome.min.css",
        "node_modules/datatables.net-dt/css/jquery.dataTables.css",
        "node_modules/datatables.net-responsive-dt/css/responsive.dataTables.css",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
        "node_modules/owl.carousel/dist/assets/owl.carousel.min.css",
        "node_modules/owl.carousel/dist/assets/owl.carousel.default.min.css",
        "resources/assets/vendor/style/bootstrap.min.css",
        "node_modules/blueprint-css/dist/blueprint.min.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.themes.css"

    ]
}
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.autoload({
    jquery: ['$', 'global.jQuery',"jQuery","global.$","jquery","global.jquery"]
});
mix.js([
    'resources/js/app.js',
    'resources/js/scripts.js'
], 'public/client/js/app.js').extract();
//mix.js('resources/js/scripts.js', 'public/client/js');
mix.sass('resources/styles/vendor.scss', 'public/client/styles');
mix.sass('resources/styles/style.scss', 'public/client/styles').options({
    implementation: require('node-sass'),
    postCss: [
        require('autoprefixer')({
            grid: true
        }),
        require('postcss-css-variables')()
    ],
    processCssUrls: false
});

if (mix.inProduction()) {
    mix.version();
}
