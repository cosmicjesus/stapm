<?php
return [
    'format' => [220, 296],
    'author'           => '',
    'subject'          => '',
    'keywords'         => '',
    'creator'          => '',
    'display_mode'     => 'fullpage',
    'margin_left'          =>   10,       // margin_left
    'margin_right'         =>   10,      // margin right
    'margin_top'           =>   1,        // margin top
    'margin_bottom'        =>   1,     // margin bottom
    'margin_header'        =>   1,     // margin header
    'margin_footer'        =>   1,     // margin footer,
    'tempDir' => base_path ('storage/app/temp/')
];
