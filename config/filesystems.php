<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],
        'program_files' => [
            'driver' => 'local',
            'root' => storage_path('app/public/programs'),
            'url' => '/storage/programs',
            'visibility' => 'public',
        ],
        'replace_class' => [
            'driver' => 'local',
            'root' => storage_path('app/public/replace_class'),
            'url' => '/storage/replace_class',
            'visibility' => 'public',
        ],
        'files' => [
            'driver' => 'local',
            'root' => storage_path('app/public/files'),
            'url' => '/storage/files',
            'visibility' => 'public',
        ],
        'education_plans' => [
            'driver' => 'local',
            'root' => storage_path('app/public/programs'),
            'url' => '/storage/programs',
            'visibility' => 'public',
        ],
        'component_documents' => [
            'driver' => 'local',
            'root' => storage_path('app/public/components'),
            'url' => '/storage/components',
            'visibility' => 'public',
        ],
        'employees' => [
            'driver' => 'local',
            'root' => storage_path('app/public/employees'),
            'url' => '/storage/employees',
            'visibility' => 'public',
        ],
        'student' => [
            'driver' => 'local',
            'root' => storage_path('app/private/students'),
            'url' => '/storage/students',
            'visibility' => 'public',
        ],
        'news' => [
            'driver' => 'local',
            'root' => storage_path('app/public/news'),
            'url' => '/storage/news',
            'visibility' => 'public',
        ],
        'regulations' => [
            'driver' => 'local',
            'root' => storage_path('app/public/regulations'),
            'url' => '/storage/regulations',
            'visibility' => 'public',
        ],
        'tochka-rosta' => [
            'driver' => 'local',
            'root' => storage_path('app/public/tochka'),
            'url' => '/storage/tochka',
            'visibility' => 'public',
        ],
        'documents' => [
            'driver' => 'local',
            'root' => storage_path('app/public/documents'),
            'url' => '/storage/documents',
            'visibility' => 'public',
        ],
        'social-partners' => [
            'driver' => 'local',
            'root' => storage_path('app/public/social-partners'),
            'url' => '/storage/social-partners',
            'visibility' => 'public',
        ],
        'news-papers-files' => [
            'driver' => 'local',
            'root' => storage_path('app/public/news-papers'),
            'url' => '/storage/news-papers',
            'visibility' => 'public',
        ],
        'decrees' => [
            'driver' => 'local',
            'root' => storage_path('app/public/decrees'),
            'url' => '/storage/decrees',
            'visibility' => 'private',
        ],
        't2-cards' => [
            'driver' => 'local',
            'root' => storage_path('app/t2-cards'),
            'url' => '/storage/t2-cards',
            'visibility' => 'private',
        ],
        'statements' => [
            'driver' => 'local',
            'root' => storage_path('app/statements'),
            'url' => '/storage/statements',
            'visibility' => 'private',
        ],
        'sync' => [
            'driver' => 'ftp',
            'host' => env('FTP_SYNC_HOST'),
            'username' => env('FTP_SYNC_LOGIN'),
            'password' => env('FTP_SYNC_PASSWORD'),
            'port' => 21
        ],
        'old' => [
            'driver' => 'ftp',
            'host' => 'cposta.beget.tech',
            'username' => 'cposta_old',
            'password' => '&9X2VRax',
            'port' => 21
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
