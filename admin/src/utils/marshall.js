import axios from 'axios'
import {Message} from 'element-ui'
import store from '@/store'
import {getToken, setToken} from '@/utils/auth'
import {MessageBox} from 'element-ui'
// create an axios instance
const service = axios.create({
    baseURL: process.env.BASE_API, // api 的 base_url
    timeout: 60000 // request timeout
})

// request interceptor
service.interceptors.request.use(
    config => {
        //config.params.token = getToken();

        if (store.getters.token) {

            config.headers['X-Token'] = getToken();
            config.headers['Authorization'] = 'Bearer ' + getToken();
        }
        return config
    },
    error => {

        Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    response => {
        if (response.headers.authorization) {
            setToken(response.headers.authorization);
            response.data.token = response.headers.authorization;
        }

        return response.data;
    },
    error => {
        console.log(11);
        console.dir(error);
        if (error.response.data.errors) {
            return Promise.reject(error.response.data.errors);
        }
        let message = _.isUndefined(error.response.data.error.message) ? '' : error.response.data.error.message;
        if (error.response.data && error.response.data.errors) {
            message = error.response.data.errors;
        } else if (error.response.data && error.response.data.error && !_.isObject(error.response.data.error)) {
            message = error.response.data.error;
        }

        Message({
            message: message,
            type: 'error',
            duration: 5 * 1000,
        });
        return Promise.reject(error.response.data);
    }
);

export default service
