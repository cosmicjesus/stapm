<template>
    <el-collapse-item :name="subject.id">
        <template slot="title">
            {{subject.index}} {{subject.name}}
        </template>
        <div class="collapse-content">
            <div class="filter-container">
                <div class="filter-item">
                    <el-button type="primary" icon="el-icon-edit" round size="small" @click="showEditIndexForm"
                               v-if="!isEditIndex">
                    </el-button>
                </div>
                <div v-if="isEditIndex" class="filter-item" style="width: 50%">
                    <el-row :gutter="10">
                        <el-col :span="20">
                            <el-input v-model="subject.index" placeholder="Индекс"></el-input>
                        </el-col>
                        <el-col :span="4">
                            <el-button type="primary" round size="small" @click="editIndex">
                                Применить
                            </el-button>
                        </el-col>
                    </el-row>
                </div>
                <div class="filter-item">
                    <el-button type="primary" icon="el-icon-plus" round @click="handleAddDocument(subject)">
                        Добавить документ
                    </el-button>
                </div>
                <div class="filter-item">
                    <el-button type="danger" icon="el-icon-delete" round @click="handleDeleteSubject(subject)">
                        Удалить дисциплину
                    </el-button>
                </div>
            </div>
            <el-table
                    :data="files"
                    style="width: 100%">
                <el-table-column
                        prop="id"
                        :label="$t('table.id')"
                        width="70">
                </el-table-column>
                <el-table-column prop="name" :label="$t('table.name')">
                </el-table-column>
                <el-table-column prop="link" :label="$t('table.link')" width="180">
                    <template slot-scope="scope">
                        <div>
                            <a :href="scope.row.url" class="link" target="_blank">Открыть</a>
                        </div>
                    </template>
                </el-table-column>
                <el-table-column label="Последнее обновление">
                    <template slot-scope="scope">
                        <div>
                            {{scope.row.updated_at|dateFormat('HH:mm:ss DD.MM.YYYY')}}
                        </div>
                    </template>
                </el-table-column>
                <el-table-column prop="action" :label="$t('table.actions')">
                    <template slot-scope="scope">
                        <div>
                            <el-button type="success" icon="el-icon-refresh" circle
                                       @click="handleChangeFile(scope.row.id)">
                            </el-button>
                            <input type="file" :ref='scope.row.id' style="display: none"
                                   @change="handleUpdateFile(scope.row,scope.$index)">
                            <el-button type="danger" icon="el-icon-delete"
                                       @click="handleDeleteFile(scope.row,scope.$index)" circle>
                            </el-button>
                        </div>
                    </template>
                </el-table-column>
            </el-table>
        </div>
    </el-collapse-item>
</template>

<script>
    import AddDocumentInSubject from './AddDocumentInSubject'
    import {
        deleteFile,
        updateFile,
        deleteSubject,
        updateProgramSubject
    } from "@/api/education/profession-program-files";
    import {buttons} from "@/constants";
    import {Message} from 'element-ui';

    export default {
        name: "subjects-list-item",
        props: ['subject', 'index'],
        data() {
            return {
                showContent: false,
                files: [],
                isEditIndex: false
            }
        },
        beforeUpdate() {
            this.$bus.off(`update-subject-document-list-${this.subject.id}`);
            this.$bus.on(`update-subject-document-list-${this.subject.id}`, data => {
                this.subject.files.push(data);
            });
            this.files = this.subject.files;
        },
        mounted() {
            this.files = this.subject.files;
        },
        methods: {
            showEditIndexForm() {
                this.isEditIndex = true;
            },
            editIndex() {
                const loading = this.$loading({
                    lock: true,
                    text: 'Идет обновление индекса записи',
                    spinner: 'el-icon-loading',
                    background: 'rgba(0, 0, 0, 0.7)'
                });
                updateProgramSubject({
                    id: this.subject.id,
                    index: this.subject.index
                }).then(response => {
                    this.isEditIndex = false;
                    loading.close();
                }, error => {
                    loading.close();
                    this.isEditIndex = false;
                })
            },
            handleAddDocument(subject) {
                this.$modal.show(AddDocumentInSubject, {
                    subject
                }, {
                    name: 'AddDocumentInSubject',
                    height: 0,
                    width: 0,
                    'z-index': 100
                })
            },
            handleDeleteSubject(subject) {
                this.$swal({
                    title: 'Подтвердите удаление',
                    type: 'question',
                    html: `<b>Вы действительно хотите удалить дисциплину «${subject.name}»? Это удалит все документы, которые были загруженны для дисциплины</b>`,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: buttons.delete,
                    cancelButtonText: buttons.cancel,
                }).then(result => {

                    if (result.value) {
                        let data = {
                            profession_program_id: this.subject.profession_program_id,
                            subject_id: this.subject.id
                        }

                        deleteSubject(data).then(response => {
                            Message({
                                message: response.data.message,
                                type: 'success',
                                duration: 5 * 1000
                            })
                            this.$emit('removeSubject', this.index);
                        });
                    }
                });
            },
            handleChangeFile(id) {
                this.$refs[id].click();
            },
            handleUpdateFile(file, index) {
                let input = {
                    id: file.id,
                    file: this.$refs[file.id].files[0],
                    program_id: file.program_id
                };
                const loading = this.$loading({
                    lock: true,
                    text: 'Документ обновляется',
                    spinner: 'el-icon-loading',
                    background: 'rgba(0, 0, 0, 0.7)'
                });
                updateFile(input).then(response => {
                    this.files[index].url = response.data.url;
                    this.files[index].updated_at = response.data.updated_at;
                    loading.close();
                    this.$notify({
                        title: 'Успешно',
                        message: response.data.message,
                        type: 'success'
                    });
                }, error => {
                    loading.close();
                });
                this.$refs[file.id].type = 'text';
                this.$refs[file.id].type = 'file';
            },
            handleDeleteFile(file, index) {
                this.$swal({
                    title: 'Подтвердите удаление',
                    type: 'question',
                    html: `<b>Вы действительно хотите удалить «${file.name}» id ${file.id}?</b>`,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: buttons.delete,
                    cancelButtonText: buttons.cancel,
                }).then(result => {
                    if (result.value) {
                        this.loading = true;
                        const loading = this.$loading({
                            lock: true,
                            text: 'Документ удаляется',
                            spinner: 'el-icon-loading',
                            background: 'rgba(0, 0, 0, 0.7)'
                        });
                        deleteFile(file.id).then(response => {
                            if (response.data.success) {
                                loading.close();
                                this.$notify({
                                    title: 'Успешно',
                                    message: response.data.message,
                                    type: 'success'
                                });
                                this.files.splice(index, 1);
                            } else {
                                loading.close();
                                this.loading = false;
                                this.$swal({
                                    title: 'Удаление невозможно',
                                    type: 'warning',
                                    html: `<b>${response.data.message}</b>`,
                                });
                            }
                        }, error => {
                            loading.close();
                        })
                    }
                });
            }
        }
    }
</script>

<style scoped lang="scss">
    .text {
        font-size: 14px;
    }

    .item {
        margin-bottom: 18px;
    }

    .clearfix:before,
    .clearfix:after {
        display: table;
        content: "";
    }

    .clearfix:after {
        clear: both
    }

    .el-card__body {
        padding: 0 !important;
    }

    .box-card {
        margin-bottom: 10px;

        &-content {
            & .active {
                padding: 20px;
            }
        }
    }
</style>