import '@/styles/bootstrap.min.css'

import Vue from 'vue'
import * as uiv from 'uiv'

Vue.use(uiv, {prefix: 'uiv'})

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import i18n from './lang' // Internationalization
import './icons' // icon
import './errorLog' // error log
import './permission' // permission control
//import './mock' // simulation data
import VueFilter from 'vue-filter';
import VeeValidate from 'vee-validate'

import * as filters from './filters' // global filters
import 'font-awesome/css/font-awesome.css';

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import VModal from 'vue-js-modal'
import Vuetify from 'vuetify'
import {DateTime} from 'luxon';

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import VueSweetalert2 from 'vue-sweetalert2';

import lodash from 'lodash';

import VueBus from 'vue-bus';


Object.defineProperty(Vue.prototype, '_', {value: lodash});

import Chart from 'chart.js';
import VueLodash from 'vue-lodash'
import VueTheMask from 'vue-the-mask'

Vue.use(VueTheMask)
Vue.use(VueSweetalert2);
Vue.use(VueLodash);
Vue.use(VueFilter);
Vue.use(VueBus);
Vue.use(Vuetify, {
    theme: {
        primary: '#3f51b5',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#b71c1c'
    }
});

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'

Vue.use(VueFormWizard)

Vue.use(VeeValidate);
Vue.component('v-icon', Icon);


Vue.use(Element, {
    size: Cookies.get('size') || 'medium', // set element-ui default size
    i18n: (key, value) => i18n.t(key, value),
})

// register global utility filters.
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

import * as config from './config';
import onlyLetter from "./directive/only-letter/onlyLetter";

Object.keys(config).forEach(key => {
    Object.defineProperty(Vue.prototype, `$${key}`, {value: config[key]});
})

import checkPermission from '@/utils/permission'
import createUniqueString from '@/utils/createUniqueString'
import {exportCSVFile} from '@/utils/export'

Object.defineProperty(Vue.prototype, `checkPermission`, {value: checkPermission});
Object.defineProperty(Vue.prototype, `createUniqueString`, {value: createUniqueString});
Object.defineProperty(Vue.prototype, `exportCSVFile`, {value: exportCSVFile});


Vue.config.productionTip = false
import {directives} from './directives';

Object.keys(directives).forEach(key => {
    Vue.directive(directives[key].name, directives[key])
})

import Dropdown from 'bp-vuejs-dropdown';


Vue.use(Dropdown);

Vue.use(VModal, {dynamic: true, injectModalsContainer: true})

import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});
import route from './routes/route.js';

Object.defineProperty(Vue.prototype, `routeByName`, {value: route});
new Vue({
    el: '#app',
    router,
    store,
    i18n,
    render: h => h(App)
})
