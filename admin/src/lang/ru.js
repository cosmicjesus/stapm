export default {
    route: {
        dashboard: 'Главная',
        introduction: 'Introduction',
        documentation: 'Documentation',
        guide: 'Guide',
        permission: 'Permission',
        pagePermission: 'Page Permission',
        directivePermission: 'Directive Permission',
        icons: 'Icons',
        components: 'Components',
        componentIndex: 'Introduction',
        tinymce: 'Tinymce',
        markdown: 'Markdown',
        jsonEditor: 'JSON Editor',
        dndList: 'Dnd List',
        splitPane: 'SplitPane',
        avatarUpload: 'Avatar Upload',
        dropzone: 'Dropzone',
        sticky: 'Sticky',
        countTo: 'CountTo',
        componentMixin: 'Mixin',
        backToTop: 'BackToTop',
        dragDialog: 'Drag Dialog',
        dragKanban: 'Drag Kanban',
        charts: 'Charts',
        keyboardChart: 'Keyboard Chart',
        lineChart: 'Line Chart',
        mixChart: 'Mix Chart',
        example: 'Example',
        nested: 'Nested Routes',
        menu1: 'Menu 1',
        'menu1-1': 'Menu 1-1',
        'menu1-2': 'Menu 1-2',
        'menu1-2-1': 'Menu 1-2-1',
        'menu1-2-2': 'Menu 1-2-2',
        'menu1-3': 'Menu 1-3',
        menu2: 'Menu 2',
        Table: 'Table',
        dynamicTable: 'Dynamic Table',
        dragTable: 'Drag Table',
        inlineEditTable: 'Inline Edit',
        complexTable: 'Complex Table',
        treeTable: 'Tree Table',
        customTreeTable: 'Custom TreeTable',
        tab: 'Tab',
        form: 'Form',
        createArticle: 'Create Article',
        editArticle: 'Edit Article',
        articleList: 'Article List',
        errorPages: 'Error Pages',
        page401: '401',
        page404: '404',
        errorLog: 'Error Log',
        excel: 'Excel',
        exportExcel: 'Export Excel',
        selectExcel: 'Export Selected',
        uploadExcel: 'Upload Excel',
        zip: 'Zip',
        exportZip: 'Export Zip',
        theme: 'Theme',
        clipboardDemo: 'Clipboard',
        i18n: 'I18n',
        externalLink: 'External Link',
        contingent: 'Контингент',
        employees: 'Сотрудники',
        'employee.card': 'Карточка сотрудника',
        positions: 'Должности',
        professions: 'Профессии',
        programs: 'ОПОП',
        subjects: 'Дисциплины',
        education: 'Образование',
        'program.card': 'Карточка ОПОП',
        'education-plans': 'Учебные планы',
        'documents': 'Документы',
        'documents.files': 'Файлы',
        'students': 'Студенты',
        'student.profile': 'Карточка студента',
        references: 'Справки',
        site: 'Сайт',
        regulations: 'Предписания',
        decrees: 'Приказы',
        users: 'Пользователи',
        'users.list': 'Пользователи',
        userslist: 'Пользователи',
        userscreate: 'Добавить пользователя',
        activitylist: 'Активность',
        'settingspage': 'Настройки',
        'news': 'Новости',
        'replaces_classes': 'Замены занятий',
        'selection-committees': 'Приемная комиссия',
        'committeeslist': 'Список приемок',
        recruitment_programs: 'Программы',
        entrants: 'Абитуриенты',
        conferences_list: 'Конференции',
        conferences_create: 'Добавление конкурса/конференции',
        cache_subdivision: 'Коды подразделений',
        recruitment_centers: 'Военкоматы',
        training_groups: 'Учебные группы',
        training_calendars: 'Учебные календари',
        detail_recruitment_program: 'Карточка программы приема',
        committees_report: 'Отчет',
        transfer_training_groups: 'Перевод учебных групп',
        'training_group_detail': 'Карточка учебной группы',
        'teaching-load':'Учебная нагрузка',
        'teaching-load-index':'Группы',
        'teaching-load-months':'Месяца'
    },
    navbar: {
        logOut: 'Log Out',
        dashboard: 'Dashboard',
        github: 'Github',
        screenfull: 'Screenfull',
        theme: 'Theme',
        size: 'Global Size'
    },
    login: {
        title: 'Login Form',
        logIn: 'Log in',
        username: 'Username',
        password: 'Password',
        any: 'any',
        thirdparty: 'Or connect with',
        thirdpartyTips: 'Can not be simulated on local, so please combine you own business simulation! ! !'
    },
    documentation: {
        documentation: 'Documentation',
        github: 'Github Repository'
    },
    permission: {
        roles: 'Your roles',
        switchRoles: 'Switch roles'
    },
    guide: {
        description: 'The guide page is useful for some people who entered the project for the first time. You can briefly introduce the features of the project. Demo is based on ',
        button: 'Show Guide'
    },
    components: {
        documentation: 'Documentation',
        tinymceTips: 'Rich text editor is a core part of management system, but at the same time is a place with lots of problems. In the process of selecting rich texts, I also walked a lot of detours. The common rich text editors in the market are basically used, and the finally chose Tinymce. See documentation for more detailed rich text editor comparisons and introductions.',
        dropzoneTips: 'Because my business has special needs, and has to upload images to qiniu, so instead of a third party, I chose encapsulate it by myself. It is very simple, you can see the detail code in @/components/Dropzone.',
        stickyTips: 'when the page is scrolled to the preset position will be sticky on the top.',
        backToTopTips1: 'When the page is scrolled to the specified position, the Back to Top button appears in the lower right corner',
        backToTopTips2: 'You can customize the style of the button, show / hide, height of appearance, height of the return. If you need a text prompt, you can use element-ui el-tooltip elements externally',
        imageUploadTips: 'Since I was using only the vue@1 version, and it is not compatible with mockjs at the moment, I modified it myself, and if you are going to use it, it is better to use official version.'
    },
    table: {
        dynamicTips1: 'Fixed header, sorted by header order',
        dynamicTips2: 'Not fixed header, sorted by click order',
        dragTips1: 'The default order',
        dragTips2: 'The after dragging order',
        title: 'Title',
        importance: 'Imp',
        remark: 'Remark',
        search: 'Search',
        add: 'Add',
        export: 'Export',
        reviewer: 'reviewer',
        id: 'ID',
        name: 'Наименование',
        type: 'Тип',
        date: 'Дата',
        link: 'Ссылка',
        author: 'Author',
        readings: 'Readings',
        status: 'Status',
        actions: 'Действия',
        edit: 'Edit',
        publish: 'Publish',
        draft: 'Draft',
        delete: 'Delete',
        cancel: 'Cancel',
        confirm: 'Confirm'
    },
    form: {
        apply: 'Применить',
        cancel: 'Отмена',
        add: 'Добавить',
        update: 'Обновить',
        delete: 'Удалить',
        ok: 'Ок',
        change: 'Выбрать',
        changeFile: 'Выберите файл',
        retire: 'Отчислить',
        make_report: 'Сформировать',
        reinstate: 'Восстановить',
        print: 'Печать',
        save: 'Сохранить',
        transfer: 'Перевести',
        generate: 'Сгенерировать',
        enrollment: 'Зачислить'
    },
    label: {
        name: 'Наименование',
        positionTypes: 'Тип должности',
        educationForm: 'Форма обучения',
        profession: 'Профессия',
        duration: 'Срок обучения',
        education_form: 'Форма обучения',
        program: 'Программа',
        accreditation: 'Срок действия аккредитации',
        sort: 'Сортировка',
        active: 'Показывать',
        changeDate: 'Выберите дату',
        program_type: 'Тип программы',
        qualification: 'Квалификация',
        file: "Файл",
        files: "Файлы",
        subject: 'Дисциплина',
        type: 'Тип',
        position: 'Должность',
        category: 'Категория',
        lastname: 'Фамилия',
        firstname: 'Имя',
        middlename: 'Отчество',
        birthday: 'Дата рождения',
        category_date: 'Дата окончания категории',
        general_experience: 'Общий стаж работы',
        speciality_experience: 'Стаж работы по специальности',
        scientific_degree: 'Ученая степень',
        academic_title: 'Ученое звание',
        start_work_date: 'Дата начала работы',
        gender: 'Пол',
        age: 'Возраст',
        direction_of_preparation: 'Направление подготовки',
        education_level: 'Уровень образования',
        organization_name: 'Наименование учреждения',
        organization_place: 'Населеный пункт',
        categories: 'Категории',
        training_group: 'Группа',
        status: 'Статус',
        phone: 'Телефон',
        privilegedCategory: 'Льготные категории',
        snils: 'СНИЛС',
        inn: 'ИНН',
        medical_policy_number: 'Номер полиса ОМС',
        date_of_actual_transfer: 'Дата фактического зачисления',
        documentType: 'Тип документа',
        series: 'Серия',
        issuanceDate: 'Дата выдачи',
        subdivisionCode: 'Код подразделения',
        issued: 'Кем выдан',
        birthPlace: 'Место рождения',
        number: 'Номер',
        graduation_date: 'Дата окончания предыдущего образования',
        graduation_organization_name: 'Образовательная организация',
        avg: 'Средний балл',
        area: 'Район',
        city: 'Город',
        index: 'Индекс',
        region: 'Область',
        street: 'Улица',
        housing: 'Корпус',
        settlement: 'Населенный пункт',
        house_number: 'Дом',
        apartment_number: 'Квартира',
        reason: 'Причина',
        decree_number: 'Номер приказа',
        decree_date: 'Дата приказа',
        language: 'Изучаемый язык',
        nationality: 'Гражданство',
        'relation_ship_type': 'Кем приходится',
        'place_of_work': 'Сведения о работе',
        enrollment_date: 'Дата зачисления',
        report_date: 'Дата формирования отчета',
        disability: 'Инвалидность',
        login: 'Логин',
        password: 'Пароль',
        military_document_type: 'Тип документа воинского учета',
        military_document_number: 'Номер документа воинского учета',
        fitness_for_military_service: 'Категория годности к ВС',
        military_recruitment_office: 'Отдел ВК',
        military_specialty: 'Специальность',
        comment: 'Комментарий',
        path: 'Путь к файлу',
        start_date: 'Дата начала',
        end_date: 'Дата конца',
        has_original: 'Оригинал документа',
        has_certificate: 'Медсправка',
        need_hostel: 'Потребность в общежитии',
        passport: 'Паспортные данные',
        health_category: 'Категория здоровья',
        email: 'E-mail',
        preferential_categories: 'Льготные категории',
        blank_data: 'Незаполненные данные',
        how_find_out: 'Откуда узнали',
        invited_student_id: 'Пригласивший студент',
        document_give_date: 'Дата подачи документов',
        totalRow: 'Строка с итогами',
        contract_target_set: 'Целевой набор',
        registration_address: 'Адрес регистрации'
    },
    filter: {
        name: 'Наименование',
        positionType: 'Тип должности'
    },
    errorLog: {
        tips: 'Please click the bug icon in the upper right corner',
        description: 'Now the management system are basically the form of the spa, it enhances the user experience, but it also increases the possibility of page problems, a small negligence may lead to the entire page deadlock. Fortunately Vue provides a way to catch handling exceptions, where you can handle errors or report exceptions.',
        documentation: 'Document introduction'
    },
    excel: {
        export: 'Export',
        selectedExport: 'Export Selected Items',
        placeholder: 'Please enter the file name(default excel-list)'
    },
    zip: {
        export: 'Export',
        placeholder: 'Please enter the file name(default file)'
    },
    theme: {
        change: 'Change Theme',
        documentation: 'Theme documentation',
        tips: 'Tips: It is different from the theme-pick on the navbar is two different skinning methods, each with different application scenarios. Refer to the documentation for details.'
    },
    tagsView: {
        refresh: 'Refresh',
        close: 'Close',
        closeOthers: 'Close Others',
        closeAll: 'Close All'
    }
}
