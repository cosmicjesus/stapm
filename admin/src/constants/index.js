import moment from 'moment';
import lodash from 'lodash'
import {DateTime} from 'luxon';

const positionTypes = [
    {id: 1, name: 'Руководящие работники'},
    {id: 2, name: 'Педагогические работники'},
    {id: 3, name: 'Учебно-вспомогательный персонал'},
    {id: 4, name: 'Обслуживающий персонал'}
];

const educationForms = [
    {id: 1, name: "Очная"},
    {id: 2, name: "Очно-заочная"},
    {id: 3, name: "Заочная"}
];

const professionProgramTypes = [
    {id: 1, name: "Программа подготовки специалистов среднего звена"},
    {id: 2, name: "Программа подготовки квалифицированных рабочих и служащих"}
];

const subjectTypes = [
    {id: 1, name: 'Дисциплина'},
    {id: 2, name: 'МДК'},
    {id: 3, name: 'Учебная практика'},
    {id: 4, name: 'Производственная практика'},
    {id: 5, name: 'Профессиональный модуль'}
];
const documentAdditionalTypes = {
    'calendar': 'Календарные графики',
    'ppssz': 'ППССЗ/ППКРС',
    'akt': 'Акты согласований'
};

const categories = [
    {id: 1, name: 'Без категории'},
    {id: 2, name: 'Экзамен на соответствие'},
    {id: 3, name: 'Первая категория'},
    {id: 4, name: 'Высшая категория'}
];

const genders = [
    {id: 0, name: 'Мужской'},
    {id: 1, name: 'Женский'}
];

const buttons = {
    delete: 'Удалить',
    cancel: 'Отмена'
};

const employeePositionType = [
    {id: 0, name: 'Штатный'},
    {id: 1, name: 'Внутренний совместитель'},
    {id: 2, name: 'Внешний совместитель'}
];

const educations = [
    {id: 1, "name": "Без основного общего образования"},
    {id: 2, "name": "Основное общее образование"},
    {id: 3, "name": "Среднее общее образование"},
    {id: 4, "name": "Неполное среднее профессиональное образование"},
    {id: 5, "name": "СПО по программам подготовки квалифицированных рабочих (служащих)"},
    {id: 6, "name": "СПО по программам подготовки специалистов среднего звена"},
    {id: 7, "name": "Высшее образование - бакалавриат"},
    {id: 8, "name": "Высшее образование - незаконченное высшее образование"},
    {id: 9, "name": "Высшее образование - подготовка кадров высшей квалификации"},
    {id: 10, "name": "Высшее образование - специалитет, магистратура"},
    {id: 11, "name": "Профессиональная переподготовка"}
];

const humanStatuses = [
    {key: 'student', value: 'Обучается'},
    {key: 'expelled', value: 'Отчислен'},
    {key: 'inArmy', value: 'В РА'},
    {key: 'academic', value: 'В академе'},
    {key: 'unconfirmed_enrollee', value: 'Неподтвержденный абитуриент'},
    {key: 'enrollee', value: 'Абитуриент'}
];

const studentStatuses = [
    {id: 1, name: 'Только обучающиеся'},
    {id: 2, name: 'Только в академе'},
    {id: 3, name: 'Только в РА'},
    {id: 4, name: 'Только в РА и академе'}
];

const privilegedCategories = [
    {id: 'other', name: 'Другая'},
    {id: 'invalid', name: 'Инвалид'},
    {id: 'needy_family', name: 'Малообеспеченная семья'},
    {id: 'incomplete_family', name: 'Неполная семья'},
    {id: 'full_state_support', name: 'Полное государственное обеспечение'},
    {id: 'without_parental_care', name: 'Сирота/без родительской опеки'}
];

const healthCategories = [
    {key: 'hearingLoss', name: "Нарушения слуха"},
    {key: 'sightDisorders', name: "Нарушения зрения"},
    {key: 'speechDisorders', name: "Нарушения речи"},
    {key: 'musculoskeletalDisorders', name: "Нарушения опорно-двигательного аппарата"},
    {key: 'mentalDisorders', name: "Задержка психического развития"},
    {key: 'intellectualDisability', name: "Умственная отсталость"},
    {key: 'autism', name: "Расстройства аутистического спектра"},
    {key: 'complexDefects', name: "Сложные дефекты"},
    {key: 'somaticDefects', name: "Соматические нарушения"},
];

const documentsTypes = [
    {id: 1, name: "Вид на жительство"},
    {id: 2, name: "Удостоверение беженца РФ"},
    {id: 3, name: "Разрешение на вр. проживание в РФ"},
    {id: 21, name: "Паспорт РФ"},
    {id: 22, name: "Военный билет"},
    {id: 23, name: "Вр. военный билет"},
    {id: 24, name: "Вр. удостоверение беженца РФ"},
    {id: 25, name: "Временное удостоверение личности гражданина РФ"},
    {id: 26, name: "Документ о вр. убежище на территории РФ"},
    {id: 27, name: "Другой"},
    {id: 28, name: "Загранпаспорт гражданина РФ"},
    {id: 29, name: "Иностранное свид-во о рождении"},
    {id: 30, name: "Иностранный паспорт"},
    {id: 31, name: "Свид-во о рождении"},
    {id: 32, name: "Свидетельство о предоставлении вр. убежища"},
    {id: 33, name: "Свидетельство о ходатайстве беженца"},
    {id: 34, name: "Удостоверение беженца РФ"},
    {id: 35, name: "Удостоверение военнослужащего"},
    {id: 36, name: "Удостоверение личности лица без гражданства РФ"},
    {id: 37, name: "Удостоверение личности отдельных категорий лиц"}
];

const reasons = {
    enrollment: [
        {key: 'onConditionsOfFreeAdmission', name: 'На условиях свободного приема'},
        {key: 'OtherEducationForm', name: 'Переведен с других форм обучения в данной ПОО'},
        {
            key: 'OtherEducationOrganizations',
            name: 'Переведен из других профессиональных образовательных организаций и образовательных организаций высшего образования'
        },
        {key: 'BackFromArmy', name: 'Возвратился из рядов вооруженных сил'},
        {key: 'BackFromPrevExpelled', name: 'Возвратился из числа ранее отчисленных'},
        {key: 'BackFromAcademicVacation', name: 'Возвратился из академического отпуска'},
        {key: 'OtherReasons', name: 'Другие причины'},
    ],
    allocation: [
        {key: 'academic', name: 'Академический отпуск'},
        {key: 'endCollege', name: 'В связи с окончанием Учреждения'},
        {key: 'voluntarilyLeftTheEducationalInstitution', name: 'Добровольно оставил образовательное учреждение'},
        {key: 'deductedOnFailure', name: 'Отчислен по неуспеваемости'},
        {key: 'inArmy', name: 'Призван в ряды вооруженных сил'},
        {
            key: 'transferOtherCollege',
            name: 'Переведен в другие образовательные учреждения среднего и высшего профессионального образования'
        },
        {
            key: 'transferIntoOtherFormsOfTraining',
            name: 'Переведен на другие формы обучения в данной образовательной организации'
        },
        {key: 'dead', name: 'Умер'}
    ]
};

const languages = [
    {key: 'english', name: 'Английский язык'},
    {key: 'france', name: 'Французский язык'},
    {key: 'german', name: 'Немецкий язык'},
];


const countries = [
    {key: 'russia', name: 'Россия'},
    {key: 'ukraine', name: 'Украина'},
    {key: 'belarus', name: 'Беларусь'},
    {key: 'kazakhstan', name: 'Казахстан'},
    {key: 'moldova', name: 'Молдова'},
    {key: 'armenia', name: 'Армения'},
    {key: 'azerbaijan', name: 'Азербайджан'},
    {key: 'uzbekistan', name: 'Узбекистан'},
    {key: 'kyrgyzstan', name: 'Киргизия'},
    {key: 'tajikistan', name: 'Таджикистан'},
    {key: 'turkmenistan', name: 'Туркменистан'},
    {key: 'georgia', name: 'Грузия'},
    {key: 'pendostan', name: 'США'},
];

//Инвалидности
const disabilities = [
    {id: 0, name: 'Первая группа'},
    {id: 1, name: 'Вторая группа'},
    {id: 2, name: 'Третья группа'},
    {id: 3, name: 'Ребенок-инвалид'}
];


const programSubjectSections = [
    {id: 1, name: 'Общеобразовательный цикл'},
    {id: 2, name: 'Общий гуманитарный и социально-экономический учебные циклы'},
    {id: 3, name: 'Математический и общий естественно-научный учебный цикл'},
    {id: 4, name: 'Общепрофессиональные дисциплины'},
    {id: 5, name: 'Профессиональные модули и практики'}
];

const decreeTypes = [
    {key: 'enrollment', name: 'Зачисление'},
    {key: 'transfer', name: 'Перевод'},
    {key: 'internal_transfer', name: 'Внутренний перевод'},
    {key: 'internal_transfer_next_year', name: 'Перевод на следующий учебный год'},
    {key: 'internal_transfer_next_term', name: 'Перевод на следующий период обучения'},
    {key: 'reinstate', name: 'Восстановление'},
    {key: 'graduation', name: 'Завершение обучения'},
    {key: 'allocation', name: 'Отчисление'}
];

const relationShipTypes = [
    {key: 'mother', name: 'мать'},
    {key: 'father', name: 'отец'},
    {key: 'relative', name: 'родственник'},
    {key: 'adoptiveParent', name: 'приёмный родитель'},
    {key: 'stepparent', name: 'усыновитель'},
    {key: 'guardian', name: 'опекун'},
    {key: 'trustee', name: 'попечитель'},
];
const excelTableLabels = {
    iteration: '№',
    lastname: 'Фамилия',
    firstname: 'Имя',
    middlename: 'Отчество',
    name: 'Наименование',
    positionTypes: 'Тип должности',
    educationForm: 'Форма обучения',
    profession: 'Специальность',
    need_hostel: 'Потребность в общежитии',
    duration: 'Срок обучения',
    education_form: 'Форма обучения',
    program: 'Программа',
    accreditation: 'Срок действия аккредитации',
    sort: 'Сортировка',
    active: 'Показывать',
    changeDate: 'Выберите дату',
    program_type: 'Тип программы',
    qualification: 'Квалификация',
    file: "Файл",
    subject: 'Дисциплина',
    type: 'Тип',
    position: 'Должность',
    category: 'Категория',
    birthday: 'Дата рождения',
    category_date: 'Дата окончания категории',
    general_experience: 'Общий стаж работы',
    speciality_experience: 'Стаж работы по специальности',
    scientific_degree: 'Ученая степень',
    academic_title: 'Ученое звание',
    start_work_date: 'Дата начала работы',
    gender: 'Пол',
    age: 'Возраст',
    direction_of_preparation: 'Направление подготовки',
    education_level: 'Уровень образования',
    organization_name: 'Наименование учреждения',
    organization_place: 'Населеный пункт',
    categories: 'Категории',
    training_group: 'Группа',
    status: 'Статус',
    phone: 'Телефон мобильный',
    privilegedCategory: 'Льготная категория',
    snils: 'СНИЛС',
    inn: 'ИНН',
    medical_policy_number: 'Номер полиса ОМС',
    date_of_actual_transfer: 'Дата фактического зачисления',
    document_type: 'Тип паспорта',
    document_series: 'Серия',
    document_issuanceDate: 'Дата выдачи',
    document_subdivisionCode: 'Код подразделения',
    document_subdivisionCode2: 'Код подразделения',
    document_issued: 'Кем выдан',
    birthPlace: 'Место рождения',
    document_number: 'Номер',
    graduation_date: 'Дата окончания предыдущего образования',
    avg: 'Средний балл',
    area: 'Район',
    city: 'Город',
    index: 'Индекс',
    region: 'Область',
    street: 'Улица',
    housing: 'Корпус',
    settlement: 'Населенный пункт',
    house_number: 'Дом',
    apartment_number: 'Квартира',
    reason: 'Причина',
    decree_number: 'Номер приказа',
    decree_date: 'Дата приказа',
    language: 'Изучаемый язык',
    nationality: 'Гражданство',
    'relation_ship_type': 'Кем приходится',
    'place_of_work': 'Сведения о работе',
    enrollment_date: 'Дата зачисления',
    report_date: 'Дата формирования отчета',
    control_information: 'Контрольная информация',
    organization_code: 'Код организации',
    country: 'Страна',
    home_phone: 'Телефон домашний',
    work_phone: 'Телефон служебный',
    comment: 'Примечание',
    latin_name: 'Имя в латинской транскрипции',
    latin_lastname: 'Фамилия в латинской транскрипции',
    email: 'Адрес электронной почты',
    average_score: 'Средний балл',
    privileget_category: 'Льготная категория',
    count_class: 'Кол-во классов',
    graduation_organization_name: 'Предыдущая образовательная организация',
    military_document_type: 'Тип документа воинского учета',
    military_document_number: 'Номер документа воинского учета',
    fitness_for_military_service: 'Категория годности в ВС',
    military_recruitment_office: 'Наименование военного комиссарита',
    military_specialty: 'Гражданская специальность',
    address_registration: 'Адрес регистрации',
    group: 'Группа'

};
const referensePeriods = [
    {key: 0, name: 'за 3 месяца'},
    {key: 1, name: 'за 4 месяца'},
    {key: 2, name: 'за 6 месяцев'},
    {key: 3, name: 'за календарный год'}
];

const referensesParams = [
    {key: 'date', name: 'Дата'},
    {key: 'count', name: 'Кол-во'},
    {key: 'decree', name: 'Приказ'},
    {key: 'end_date', name: 'Окончание обучения'},
    {key: 'start_date', name: 'Начало обучения'},
    {key: 'end_training', name: 'Окончание обучения'},
    {key: 'start_training', name: 'Начало обучения'},
    {key: 'decree_id', name: 'Id Приказа'},
    {key: 'student_id', name: 'Id Студента'},
    {key: 'period', name: 'Период'}
];

const referencesType = [
    {key: 'money', name: 'Справка о стипендии'},
    {key: 'military', name: 'Справка в военкомат'},
    {key: 'training', name: 'Справка об обучении'}
];

let buildAcademicYears = function (start_year = null) {
    let years = [];
    let minMaxYears = {
        max: 0,
        min: 0
    };
    if (start_year) {
        minMaxYears.max = start_year + 1;
        minMaxYears.min = start_year - 7;
    } else {
        let nowYear = moment().year();
        minMaxYears.max = nowYear + 1;
        minMaxYears.min = nowYear - 7;
    }

    for (let i = minMaxYears.min; i <= minMaxYears.max; i++) {

        let start_date = DateTime.fromObject({year: i, month: 9, day: 1}).toISODate();
        let end_date = DateTime.fromObject({year: i + 1, month: 7, day: 31}).toISODate();

        let term = {
            key: i,
            label: `${i} - ${i + 1}`,
            start_date,
            end_date
        };

        years.push(term);
    }

    return _.orderBy(years, 'key', 'desc');
};

const academicYears = buildAcademicYears();
const newsTypes = [
    {id: 1, text: 'Новости'},
    {id: 2, text: 'События/Анонсы'},
    {id: 3, text: 'Новости и события'},
    {id: 4, text: 'Новости и раздел 9 мая'},
];

const militaryDocumentTypes = [
    {key: 'recruitId', text: 'Удостоверение гражданина подлежащего призыву на военную службу'},
    {key: 'militaryId', text: 'Военный билет'},
    {key: 'temporary_military_id', text: 'Вр. военный билет'},
    {key: 'military_identification_card', text: 'Удостоверение военнослужащего'}
];

const fitnessForMilitaryServices = [
    {key: 'fit', text: 'А - Годен к военной службе'},
    {key: 'fitWithSmallLimitations', text: 'Б - Годен к военной службе с незначительными ограничениями'},
    {key: 'limitedlyFit', text: 'В - Ограниченно годен к военной службе'},
    {key: 'temporarilyUnfit', text: 'Г - Временно не годен к военной службе'},
    {key: 'unfit', text: 'Д - Не годен к военной службе'}
];

const militaryStatuses = [
    {key: 'NotWorthMilitaryPost', name: 'Не состоит на воинском учете'},
    {key: 'Recruit', name: 'Призывник'},
    {key: 'ServedInTheArmy', name: 'Прошел службу'},
    {key: 'ReleasedFromMilitaryService', name: 'Освобожден от ВС'},
    {key: 'Militarian', name: 'Военнообязанный'}
];

const permissionGroups = {
    god: {
        name: 'Уровень бога',
        permissions: {
            'super.admin': 'Бог'
        }
    },
    students: {
        name: 'Работа со студентами',
        permissions: {
            'student.view': 'Просмотр списка и дел студентов',
            'student.edit': 'Редактирование дел студентов',
            'student.create': 'Добавление студентов',
            'student.actions': 'Действия со студентами',
            'student.military': 'Редактирование данных воинского учета',
            'student.make-reference': 'Генерация справок для студентов',
            'student.decrees': 'Загрузка приказов'
        }
    },
    employees: {
        name: 'Работа с сотрудниками',
        permissions: {
            'employees.view': 'Просмотр списка и дел сотруников',
            'employees.edit': 'Редактирование дела сотрудника'
        }
    },
    enrollees: {
        name: 'Работа с абитуриентами',
        permissions: {
            'enrollees.view': 'Просмотр списка и дел абитуриентов',
            'enrollees.edit': 'Редактирование дел абитуриентов',
            'enrollees.create': 'Добавление абитуриентов',
            'enrollees.actions': 'Действия с абитуриентами'
        }
    },
    "education": {
        "name": "Образование",
        "permissions": {
            'opop.view': 'Просмотр ОПОП',
            'opop.actions': 'Действия с ОПОП',
            'eduplans.view': 'Просмотр учебных планов',
            'eduplans.actions': 'Действия с учебными планами',
            'profession.view': 'Просмотр профессий',
            'profession.actions': 'Действия с профессиями',
            'subjects.view': 'Просмотр дисциплин',
            'subjects.actions': 'Действия с дисциплинами',
            'training_calendars.view': 'Просмотр учебных календарей',
            'training_calendars.actions': 'Действия с учебными календарями',
            'education.load':'Работа с учебной нагрузкой'
        }
    },
    "site": {
        "name": "Работа с сайтом",
        "permissions": {
            "news.actions": "Работа с новостями",
            "documents.actions": "Работа с документами",
        }
    }
};

// const trainingPeriods = [
//     {key: 'semester', name: 'Семестр'},
//     {key: 'course', name: 'Курс'},
//     {key: 'trimester', name: 'Триместр'}
// ];

const trainingPeriods = {
    'semester': 'semester',
    'course': 'course',
    'trimester': 'trimester'
};
const activityEntities = {
    'report': 'Отчеты',
    'export': 'Выгрузки',
    'subject': 'Дисциплина',
    'student': 'Cтудент',
    'student_file': 'Файлы студента',
    'education_plan': 'Учебный план',
    'enrollee': 'Абитуриент',
    'component_document': 'Компонет документа',
    'profession_program': 'Профессиональная программа',
    'program_subject': 'Дисциплины в ОПОП',
    'program_document': 'Документ ОПОП',
    'employee_education': 'Образование сотрудников',
    'recruitment_program': 'Программа приема',
    'employee': 'Сотрудник',
    entrant: 'Абитуриент'
};

const activityEvents = {
    'get_report': 'Формирование отчета',
    'export': 'Выгрузка данных',
    'login': 'Вход в систему',
    'updated': 'Обновление',
    'created': 'Добавление',
    'deleted': 'Удаление',
    'show_profile': 'Просмотр профиля'
};

const programFileNames = [
    {text: 'Рабочая программа'},
    {text: 'Рабочая программа [исходник]'},
    {text: 'Рабочая программа [Дуальное обучение]'},
    {text: 'Методические указания'},
    {text: 'Методические указания для ЛПЗ'},
    {text: 'Методические указания для практических работ'},
    {text: 'Методические указания для курсовых работ'},
    {text: 'КОС'},
];

const fileNames = [
    {text: 'Титульный лист'},
    {text: 'Сводные данные'},
    {text: 'Тело УП'},
    {text: 'Сводные данные и тело УП'},
    {text: 'Список кабинетов'},
    {text: 'Пояснительная записка'},
    {text: 'Содержание РП'},
    {text: 'Тело РП'},
    {text: 'Паспорт РП'},
    {text: 'Структура и содержание РП'},
    {text: 'Тематический план РП'},
    {text: 'Реализация РП'},
    {text: 'Контроль и оценка РП'},
];

const howFindOutSourses = {
    openDoorsDay: 'openDoorsDay',
    internet: 'internet',
    fromRelative: 'fromRelative',
    fromEmployee: 'fromEmployee',
    fromStudent: 'fromStudent',
    other: 'other',
}

const yesAndNo = {
    1: 1,
    0: 0
};

const transferTargets = {
    academicTerm: "AcademicTerm",
    academicYear: "AcademicYear",
    graduation: "Graduation"
};

const termTypes = {
    semester: "semester",
    trimester: "trimester",
    course: "course"
};

const militaryRanks = {
    conscript: "Conscript",
    commonSoldier: "CommonSoldier",
    lanceCorporal: "LanceCorporal",
    lanceSergeant: "LanceSergeant",
    sergeant: "Sergeant",
    staffSergeant: "StaffSergeant",
    pettyOfficer: "PettyOfficer",
    ensign: "Ensign",
    seniorEnsign: "SeniorEnsign",
    sublieutenant: "Sublieutenant",
    lieutenant: "Lieutenant",
    seniorLieutenant: "SeniorLieutenant",
    captain: "Captain",
    major: "Major",
    lieutenantColonel: "LieutenantColonel",
    colonel: "Colonel",
    majorGeneral: "MajorGeneral",
    lieutenantGeneral: "LieutenantGeneral",
    colonelGeneral: "ColonelGeneral",
    generalOfTheArmy: "GeneralOfTheArmy",
    marshal: "Marshal"
};

const militaryCompositions = {
    soldiersAndSailors: "SoldiersAndSailors",
    ensignAndWarrantOfficers: "EnsignAndWarrantOfficers",
    officersAndMarshals: "OfficersAndMarshals"
};

const groupOfAccounting = {
    army: 'Army',
    fleet: 'Fleet'
};

const reserveCategories = {
    firstCategory: 'FirstCategory',
    secondCategory: 'SecondCategory'
};
export const months = {
    september: 'september',
    october: 'october',
    november: 'november',
    december: 'december',
    january: 'january',
    february: 'february',
    march: 'march',
    april: 'april',
    may: 'may',
    june: 'june',
    july: 'july',
    august: 'august',
}

export const days = {
    monday: 'Monday',
    Tuesday: 'Tuesday',
    Wednesday: 'Wednesday',
    Thursday: 'Thursday',
    Friday: 'Friday',
    Saturday: 'Saturday',
    sunday: 'Sunday'
};
export {
    groupOfAccounting,
    reserveCategories,
    militaryRanks,
    militaryCompositions,
    termTypes,
    transferTargets,
    programFileNames,
    yesAndNo,
    howFindOutSourses,
    trainingPeriods,
    militaryStatuses,
    programSubjectSections,
    fileNames,
    activityEntities,
    activityEvents,
    permissionGroups,
    militaryDocumentTypes,
    fitnessForMilitaryServices,
    newsTypes,
    academicYears,
    referencesType,
    referensePeriods,
    referensesParams,
    excelTableLabels,
    positionTypes,
    buttons,
    educationForms,
    professionProgramTypes,
    subjectTypes,
    documentAdditionalTypes,
    categories,
    genders,
    employeePositionType,
    educations,
    humanStatuses,
    studentStatuses,
    privilegedCategories,
    healthCategories,
    documentsTypes,
    reasons,
    languages,
    countries,
    disabilities,
    decreeTypes,
    relationShipTypes
}
