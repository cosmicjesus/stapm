const positionTypes = {
    1: 'Руководящие работники',
    2: 'Педагогические работники',
    3: 'Учебно-вспомогательный персонал',
    4: 'Обслуживающий персонал'
};

export default positionTypes;