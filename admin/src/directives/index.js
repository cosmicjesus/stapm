import onlyLetter from './only-letter';
import waves from './waves';

export const directives = {onlyLetter, waves};