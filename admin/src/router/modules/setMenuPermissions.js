export function setMenuPermissions(permissions) {
    return ['super.admin', ...permissions]
}

