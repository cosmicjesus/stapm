import Layout from '@/views/layout/Layout'
import {setMenuPermissions} from "./setMenuPermissions";

const loadGroupsTable = () => import('@/views/teaching_load/index');
const loadGroupsDetail = () => import('@/views/teaching_load/detail');
const monthsIndex = () => import('@/views/teaching_load/months/index');


const teachingLoad = {
    path: '/teaching-load',
    component: Layout,
    redirect: '/teaching-load',
    alwaysShow: true, // will always show the root menu
    meta: {
        title: 'teaching-load',
        icon: 'book',
        noCache: true,
        roles: ['super.admin', 'education.load'] // you can set roles in root nav
    },
    children: [
        {
            path: '/months',
            component: monthsIndex,
            name: 'teaching-load-months',
            meta: {
                title: 'teaching-load-months',
                icon: 'list',
                roles: setMenuPermissions(['education.load'])
            }
        },
        {
            path: '/teaching-load',
            component: loadGroupsTable,
            name: 'teaching-load-index',
            meta: {
                title: 'teaching-load-index',
                icon: 'list',
                roles: setMenuPermissions(['education.load'])
            }
        },
        {
            path: '/teaching-load/:id',
            component: loadGroupsDetail,
            name: 'teaching-load.detail',
            hidden:true,
            meta: {
                title: 'teaching-load.index',
                icon: 'list',
                roles: setMenuPermissions(['education.load'])
            }
        }
    ]
};

export default teachingLoad;
