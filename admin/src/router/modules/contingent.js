import Layout from '@/views/layout/Layout'
import {setMenuPermissions} from "./setMenuPermissions";

const contingentRoutes = {
    path: '/contingent',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    meta: {
        title: 'contingent',
        icon: 'user',
        noCache: true
        //roles: ['super.admin'] // you can set roles in root nav
    },
    children: [
        {
            path: 'recruitment-centers',
            component: () => import('@/views/contingent/recruitment-center/recruitment-centers-list'),
            name: 'recruitment_centers',
            meta: {
                title: 'recruitment_centers',
                icon: 'list',
                roles: ['super.admin']
            }
        },
        {
            path: 'references-list',
            component: () => import('@/views/references/references-list'),
            name: 'ReferencesList',
            meta: {
                title: 'references',
                icon: 'list',
                roles: ['super.admin']
            }
        },
        {
            path: 'decrees-list',
            component: () => import('@/views/decrees/decrees-list'),
            name: 'decrees-list',
            meta: {
                title: 'decrees',
                icon: 'list',
                roles: ['super.admin', 'student.decrees']
            }
        },
        {
            path: 'employees',
            component: () => import('@/views/contingent/employees/employees-list'),
            name: 'EmployeesList',
            meta: {
                title: 'employees',
                icon: 'user',
                roles: setMenuPermissions(['employees.view', 'employees.view']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'employees/:id/profile',
            component: () => import('@/views/contingent/employees/employee-profile'),
            name: 'employee.profile',
            props: true,
            hidden: true,
            meta: {
                title: 'employee.card',
                icon: 'list',
                roles: setMenuPermissions(['employees.view', 'employees.view']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'entrants',
            component: () => import('@/views/contingent/entrants/entrants-page'),
            name: 'entrants.list',
            meta: {
                title: 'entrants',
                icon: 'user',
                roles: setMenuPermissions(['entrants.view', 'enrollees.edit', 'enrollees.create', 'enrollees.actions']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'entrants/:id/profile',
            component: () => import('@/views/contingent/entrants/entrant-profile'),
            name: 'entrant.profile',
            props: true,
            hidden: true,
            meta: {
                title: 'entrants.card',
                icon: 'list',
                roles: setMenuPermissions(['enrollees.view', 'employees.view']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'entrants/create',
            component: () => import('@/views/contingent/entrants/create-entrant-page'),
            name: 'entrants.create',
            hidden: true,
            meta: {
                title: 'entrants.create',
                icon: 'user',
                roles: setMenuPermissions(['enrollees.create', 'enrollees.actions']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'students',
            component: () => import('@/views/contingent/students/students-page'),
            name: 'students.list',
            meta: {
                title: 'students',
                icon: 'user',
                roles: setMenuPermissions(['student.view', 'student.edit', 'student.create', 'student.actions', 'student.military', 'student.make-reference']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'students/:id/profile',
            component: () => import('@/views/contingent/students/student-profile'),
            name: 'student.profile',
            hidden: true,
            meta: {
                title: 'student.profile',
                icon: 'user',
                roles: setMenuPermissions(['student.view', 'student.edit', 'student.create', 'student.actions', 'student.military', 'student.make-reference']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'training-groups',
            component: () => import('@/views/contingent/training-groups/index'),
            name: 'training_groups.list',
            meta: {
                title: 'training_groups',
                icon: 'user',
                roles: setMenuPermissions([]) // or you can only set roles in sub nav
            }
        },
        {
            path: 'training-groups/:id',
            component: () => import('@/views/contingent/training-groups/detail'),
            name: 'training_group_detail',
            hidden: true,
            meta: {
                title: 'training_group_detail',
                icon: 'user',
                roles: setMenuPermissions([]) // or you can only set roles in sub nav
            }
        },
        {
            path: 'transfer-training-groups',
            component: () => import('@/views/contingent/transfer-training-groups/transfer-training-groups'),
            name: 'transfer_training_groups',
            meta: {
                title: 'transfer_training_groups',
                icon: 'user',
                roles: setMenuPermissions([]) // or you can only set roles in sub nav
            }
        },
        {
            path: 'positions',
            component: () => import('@/views/contingent/positions/PositionsList'),
            name: 'PositionsList',
            meta: {
                title: 'positions',
                icon: 'list',
                //roles: ['super.admin'] // or you can only set roles in sub nav
            }
        }
    ]
};

export default contingentRoutes;