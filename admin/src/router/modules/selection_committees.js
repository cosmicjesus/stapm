import Layout from '@/views/layout/Layout'

const committeesRouter = {
    path: '/selection-committees',
    component: Layout,
    redirect: 'noredirect',
    name: 'selection-committees',
    meta: {
        title: 'selection-committees',
        icon: 'book'
    },
    children: [
        {
            path: 'list',
            component: () => import('@/views/selection-committees/committees-list'),
            name: 'committees.list',
            meta: {title: 'committeeslist', noCache: true, icon: 'list'}
        },
        {
            path: 'recruitment-programs',
            component: () => import('@/views/recruitment-programs/recruitment-programs-list'),
            name: 'committees.list1',
            meta: {title: 'recruitment_programs', noCache: true, icon: 'list'},
        },
        {
            path: 'recruitment-programs/:id',
            component: () => import('@/views/recruitment-programs/detail-recruitment-program'),
            name: 'detail_recruitment_program',
            hidden: true,
            alwaysShow: false,
            meta: {title: 'detail_recruitment_program', noCache: true, icon: 'list'}
        },
        {
            path: 'report',
            component: () => import('@/views/committee-report/report-page'),
            name: 'committees_report',
            meta: {title: 'committees_report', noCache: true, icon: 'list'},
        },
    ]
};

export default committeesRouter