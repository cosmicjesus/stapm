import Layout from '@/views/layout/Layout'
import AppMain from '@/views/layout/components/AppMain'
import {setMenuPermissions} from "./setMenuPermissions";

const educationRoutes = {
    path: '/education',
    component: Layout,
    redirect: '/education',
    alwaysShow: true, // will always show the root menu
    meta: {
        title: 'education',
        icon: 'book',
        noCache: true
        //roles: ['super.admin'] // you can set roles in root nav
    },
    children: [
        {
            path: 'professions',
            component: () => import('@/views/education/professions/page'),
            name: 'PageProfessions',
            meta: {
                title: 'professions',
                icon: 'list',
                roles: setMenuPermissions(['profession.view', 'profession.actions']) // or you can only set roles in sub nav
            }
        },
        {
            path: 'profession-programs',
            component: () => import('@/views/education/profession-programs/profession-programs-list'),
            name: 'opop',
            meta: {
                title: 'programs',
                icon: 'list',
                roles: setMenuPermissions(['opop.view', 'opop.actions'])
            },
            children: []
        },
        {
            path: 'profession-programs/:id',
            component: () => import('@/views/education/profession-programs/DetailPage'),
            props: true,
            name: 'opop.detail',
            hidden: true,
            alwaysShow: false,
            meta: {
                title: 'program.card',
                icon: 'list',
                roles: setMenuPermissions(['opop.view', 'opop.actions'])
            }
        },
        {
            path: '/education-plans',
            redirect: '/education-plans/list',
            component: AppMain,
            meta: {
                title: 'education-plans',
                icon: 'list',
                roles: setMenuPermissions(['eduplans.view', 'eduplans.actions'])
            },
            children: [
                {
                    path: '/education-plans/list',
                    component: () => import('@/views/education/education-plans/list'),
                    name: 'education-plans',
                    meta: {
                        title: 'education-plans',
                        icon: 'list',
                        roles: setMenuPermissions(['eduplans.view', 'eduplans.actions'])
                    },
                },
                {
                    path: '/education-plans/:id',
                    component: () => import('@/views/education/education-plans/detail-plan'),
                    name: 'education-plans.detail',
                    hidden: true,
                    meta: {
                        title: 'education-plans.detail',
                        icon: 'list',
                        roles: setMenuPermissions(['eduplans.view', 'eduplans.actions'])
                    },
                }
            ]
        },
        {
            path: 'training-calendars',
            component: () => import('@/views/education/training-calendars/calendars-page'),
            name: 'training_calendars',
            meta: {
                title: 'training_calendars',
                icon: 'list',
                roles: setMenuPermissions(['training_calendars.view', 'training_calendars.actions'])
            },
            children: []
        },
        {
            path: 'subjects',
            component: () => import('@/views/education/subjects/subjects-list'),
            name: 'subjects',
            meta: {
                title: 'subjects',
                icon: 'list',
                roles: setMenuPermissions(['subjects.view', 'subjects.actions'])
            },
            children: []
        }
    ]
};

export default educationRoutes;