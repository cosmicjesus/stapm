import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'
import AppMain from '@/views/layout/components/AppMain'

/* Router Modules */
import componentsRouter from './modules/components'
import chartsRouter from './modules/charts'
import tableRouter from './modules/table'
import nestedRouter from './modules/nested'
import checkPermission from '@/utils/permission'
import selectionCommittees from './modules/selection_committees';
import contingent from './modules/contingent';
import education from './modules/education';
import teachingLoad from "./modules/teaching_load";

const setMenuPermissions = function (permissions) {
    return ['super.admin', ...permissions]
}
/** note: Submenu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
 **/
export const constantRouterMap = [
    {
        path: '/redirect',
        component: Layout,
        hidden: true,
        children: [
            {
                path: '/redirect/:path*',
                component: () => import('@/views/redirect/index')
            }
        ]
    },
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true
    },
    {
        path: '/auth-redirect',
        component: () => import('@/views/login/authredirect'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/errorPage/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/errorPage/401'),
        hidden: true
    },
    {
        path: '',
        component: Layout,
        redirect: 'dashboard',
        children: [
            {
                path: 'dashboard',
                component: () => import('@/views/dashboard/index'),
                name: 'dashboard',
                meta: {title: 'dashboard', icon: 'book', noCache: true}
            }
        ]
    }
]

export default new Router({
    mode: process.env.HISTORY_MODE, // require service support
    scrollBehavior: () => ({y: 0}),
    routes: constantRouterMap
})

export const asyncRouterMap = [
    // {
    //     path: '/permission',
    //     component: Layout,
    //     redirect: '/permission/index',
    //     alwaysShow: true, // will always show the root menu
    //     meta: {
    //         title: 'permission',
    //         icon: 'lock',
    //         roles: ['super.admin'] // you can set roles in root nav
    //     },
    //     children: [
    //         {
    //             path: 'page',
    //             component: () => import('@/views/permission/page'),
    //             name: 'PagePermission',
    //             meta: {
    //                 title: 'pagePermission',
    //                 roles: ['super.admin'] // or you can only set roles in sub nav
    //             }
    //         },
    //         {
    //             path: 'directive',
    //             component: () => import('@/views/permission/directive'),
    //             name: 'DirectivePermission',
    //             meta: {
    //                 title: 'directivePermission'
    //                 // if do not set roles, means: this page does not require permission
    //             }
    //         }
    //     ]
    // },
    // {
    //     path: '/users',
    //     component: () => import('@/views/users/users-list'),
    //     //alwaysShow: true, // will always show the root menu
    //     meta: {
    //         title: 'users.access',
    //         icon: 'user',
    //         noCache: true,
    //         //roles: ['super.admin'] // you can set roles in root nav
    //     },
    // },
    {
        path: '/users',
        component: Layout,
        redirect: '/users/list',
        meta: {
            icon: 'user',
            noCache: true,
            roles: ['super.admin']
        },
        children: [
            {
                path: '/users/list',
                component: () => import('@/views/users/users-list'),
                name: 'users.list',
                meta: {title: 'userslist', icon: 'user', noCache: true},
                roles: ['super.admin']
            },
            {
                path: '/users/create',
                component: () => import('@/views/users/create-user'),
                name: 'users.create',
                hidden: true,
                meta: {title: 'userscreate', icon: 'user', noCache: true},
                roles: ['super.admin']
            }
        ]
    },
    {
        path: '/activity',
        component: Layout,
        redirect: '/activity/list',
        meta: {
            icon: 'users',
            noCache: true,
            roles: ['super.admin']
        },
        children: [
            {
                path: '/activity/list',
                component: () => import('@/views/activity/activity'),
                name: 'activity.list',
                meta: {title: 'activitylist', icon: 'users', noCache: true},
                roles: ['super.admin']
            }
        ]
    },
    {
        path: '/settings',
        component: Layout,
        redirect: '/settings/page',
        meta: {
            icon: 'wrench',
            noCache: true,
            roles: ['super.admin']
        },
        children: [
            {
                path: '/settings/page',
                component: () => import('@/views/settings/settings-page'),
                name: 'settings.page',
                meta: {title: 'settingspage', icon: 'wrench', noCache: true},
                roles: ['super.admin']
            }
        ]
    },
    {
        path: '/cache',
        component: Layout,
        alwaysShow: true,
        meta: {
            title: 'Кеш',
            icon: 'wrench',
            noCache: true,
            roles: ['super.admin']
        },
        children: [
            {
                path: '/cache/subdivision-codes',
                component: () => import('@/views/cache/subdivision-codes'),
                name: 'cache_subdivision',
                meta: {title: 'cache_subdivision', icon: 'wrench', noCache: true},
                roles: ['super.admin']
            }
        ]
    },
    contingent,
    education,
    teachingLoad,
    selectionCommittees,
    {
        path: '/site',
        component: Layout,
        alwaysShow: true,
        meta: {
            title: 'site',
            icon: 'globe',
            noCache: true
        },
        children: [
            {
                path: 'documetns/files',
                component: () => import('@/views/documents/documents/documents-list'),
                name: 'documents.files',
                meta: {
                    title: 'documents',
                    icon: 'book',
                    noCache: true,
                    roles: setMenuPermissions(['documents.actions'])
                }
            },
            {
                path: 'regulations',
                component: () => import('@/views/site/regulations/regulations-list'),
                name: 'regulations',
                meta: {
                    title: 'regulations',
                    icon: 'book',
                    noCache: true,
                    roles: setMenuPermissions(['documents.actions'])
                }
            },
            {
                path: 'regulations/:id',
                component: () => import('@/views/site/regulations/regulation-detail'),
                name: 'regulation.detail',
                props: true,
                hidden: true,
                meta: {
                    title: 'regulation.detail',
                    icon: 'book',
                    noCache: true,
                    roles: setMenuPermissions(['documents.actions'])
                }
            },
            {
                path: 'news',
                component: () => import('@/views/site/news/news-list'),
                name: 'news',
                meta: {
                    title: 'news',
                    icon: 'list',
                    noCache: true,
                    roles: setMenuPermissions(['news.actions'])
                }
            },
            {
                path: 'news/create',
                component: () => import('@/views/site/news/create-news'),
                name: 'news.create',
                hidden: true,
                meta: {
                    title: 'news.create',
                    icon: 'list',
                    noCache: true,
                    roles: setMenuPermissions(['news.actions'])
                }
            },
            {
                path: 'replaces-classes',
                component: () => import('@/views/replacing-classes/replacing-classes-list'),
                name: 'replaces.classes',
                hidden: false,
                meta: {
                    title: 'replaces_classes',
                    icon: 'list',
                    noCache: true,
                    roles: setMenuPermissions([])
                }
            },
            {
                path: 'conferences',
                component: () => import('@/views/site/conferences/conferences-list'),
                name: 'conferences_list',
                hidden: false,
                meta: {
                    title: 'conferences_list',
                    icon: 'list',
                    noCache: true,
                    roles: setMenuPermissions([])
                }
            },
            {
                path: 'conferences/create',
                component: () => import('@/views/site/conferences/create-conference'),
                name: 'conferences_create',
                hidden: true,
                meta: {
                    title: 'conferences_create',
                    icon: 'list',
                    noCache: true,
                    roles: setMenuPermissions([])
                }
            }
        ]
    },

    /** When your routing table is too long, you can split it into small modules**/
    // componentsRouter,
    // chartsRouter,
    // nestedRouter,
    // tableRouter,

    // {
    //     path: '/example',
    //     component: Layout,
    //     redirect: '/example/list',
    //     name: 'Example',
    //     meta: {
    //         title: 'example',
    //         icon: 'book'
    //     },
    //     children: [
    //         {
    //             path: 'create',
    //             component: () => import('@/views/example/create'),
    //             name: 'CreateArticle',
    //             meta: {title: 'createArticle', icon: 'edit'}
    //         },
    //         {
    //             path: 'edit/:id(\\d+)',
    //             component: () => import('@/views/example/edit'),
    //             name: 'EditArticle',
    //             meta: {title: 'editArticle', noCache: true},
    //             hidden: true
    //         },
    //         {
    //             path: 'list',
    //             component: () => import('@/views/example/list'),
    //             name: 'ArticleList',
    //             meta: {title: 'articleList', icon: 'list'}
    //         }
    //     ]
    // },
    // {
    //     path: '/tab',
    //     component: Layout,
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/tab/index'),
    //             name: 'Tab',
    //             meta: {title: 'tab', icon: 'book'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/error',
    //     component: Layout,
    //     redirect: 'noredirect',
    //     name: 'ErrorPages',
    //     meta: {
    //         title: 'errorPages',
    //         icon: 'book'
    //     },
    //     children: [
    //         {
    //             path: '401',
    //             component: () => import('@/views/errorPage/401'),
    //             name: 'Page401',
    //             meta: {title: 'page401', noCache: true}
    //         },
    //         {
    //             path: '404',
    //             component: () => import('@/views/errorPage/404'),
    //             name: 'Page404',
    //             meta: {title: 'page404', noCache: true}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/error-log',
    //     component: Layout,
    //     redirect: 'noredirect',
    //     children: [
    //         {
    //             path: 'log',
    //             component: () => import('@/views/errorLog/index'),
    //             name: 'ErrorLog',
    //             meta: {title: 'errorLog', icon: 'bug'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/excel',
    //     component: Layout,
    //     redirect: '/excel/export-excel',
    //     name: 'Excel',
    //     meta: {
    //         title: 'excel',
    //         icon: 'book'
    //     },
    //     children: [
    //         {
    //             path: 'export-excel',
    //             component: () => import('@/views/excel/exportExcel'),
    //             name: 'ExportExcel',
    //             meta: {title: 'exportExcel'}
    //         },
    //         {
    //             path: 'export-selected-excel',
    //             component: () => import('@/views/excel/selectExcel'),
    //             name: 'SelectExcel',
    //             meta: {title: 'selectExcel'}
    //         },
    //         {
    //             path: 'upload-excel',
    //             component: () => import('@/views/excel/uploadExcel'),
    //             name: 'UploadExcel',
    //             meta: {title: 'uploadExcel'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/zip',
    //     component: Layout,
    //     redirect: '/zip/download',
    //     alwaysShow: true,
    //     meta: {title: 'zip', icon: 'book'},
    //     children: [
    //         {
    //             path: 'download',
    //             component: () => import('@/views/zip/index'),
    //             name: 'ExportZip',
    //             meta: {title: 'exportZip'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/theme',
    //     component: Layout,
    //     redirect: 'noredirect',
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/theme/index'),
    //             name: 'Theme',
    //             meta: {title: 'theme', icon: 'book'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/clipboard',
    //     component: Layout,
    //     redirect: 'noredirect',
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/clipboard/index'),
    //             name: 'ClipboardDemo',
    //             meta: {title: 'clipboardDemo', icon: 'book'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: '/i18n',
    //     component: Layout,
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/i18n-demo/index'),
    //             name: 'I18n',
    //             meta: {title: 'i18n', icon: 'book'}
    //         }
    //     ]
    // },
    //
    // {
    //     path: 'external-link',
    //     component: Layout,
    //     children: [
    //         {
    //             path: 'https://github.com/PanJiaChen/vue-element-admin',
    //             meta: {title: 'externalLink', icon: 'book'}
    //         }
    //     ]
    // },

    {path: '*', redirect: '/404', hidden: true}
]
