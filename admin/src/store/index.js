import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import errorLog from './modules/errorLog'
import permission from './modules/permission'
import tagsView from './modules/tagsView'
import user from './modules/user'
import getters from './getters'
import createStudentForm from './modules/createStudentForm'
import professionPrograms from './modules/professionPrograms'
import trainingGroup from './modules/trainingGroup'
import student from './modules/student'
import educationPlan from './modules/education_plan'
import employee from "./modules/employee";
import recruitmentProgram from './modules/recruitmentProgram';
import teacherLoad from "./modules/teaching_load";
import academicYearMonths from "./modules/academicYearMonths";
import entrant from "./modules/entrant";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        app,
        errorLog,
        permission,
        tagsView,
        user,
        createStudentForm,
        professionPrograms,
        trainingGroup,
        student,
        educationPlan,
        employee,
        recruitmentProgram,
        teacherLoad,
        academicYearMonths,
        entrant
    },
    getters
})

export default store
