import Vue from 'vue'

const trainingGroup = {
    state: {
        currentGroup: {},
        formState: {
            profession_program_id: null,
            calendar_id:null,
            education_plan_id: null,
            employee_id: null,
            unique_code: null,
            pattern: '{Курс}',
            max_people: 25,
            inUse: false
        },
        currentFormState: {
            profession_program_id: null,
            calendar_id:null,
            education_plan_id: null,
            employee_id: null,
            unique_code: null,
            pattern: '{Курс}',
            max_people: 25,
            inUse: false
        },
    },
    mutations: {
        SET_CURRENT_TRAINING_GROUP_FORM_STATE(state, formData) {
            if (_.isNull(formData)) {
                Object.assign(state.currentFormState, state.formState);
            } else {
                Object.assign(state.currentFormState, formData);
            }
        },
        SET_CURRENT_GROUP(state, group) {
            state.currentGroup = group;
        }
    },
    actions: {
        SET_CURRENT_GROUP({commit}, group) {
            commit('SET_CURRENT_GROUP', group);
        },
        SET_CURRENT_TRAINING_GROUP_FORM_STATE({commit}, group) {
            commit('SET_CURRENT_TRAINING_GROUP_FORM_STATE', group);
        }
    }
};

export default trainingGroup;
