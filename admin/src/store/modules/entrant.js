const entrant = {
    state: {
        currentEntrant: {},
        currentEntrantPrograms: [],
        recruitmentProgramsList: [],
    },
    mutations: {
        SET_RECRUITMENT_PROGRAM_LIST(state, programsList) {
            state.recruitmentProgramsList = programsList;
        },
        SET_CURRENT_ENTRANT(state, entrant) {
            state.currentEntrantPrograms = entrant.recruitment_programs;
        },
        ADD_RECRUITMENT_PROGRAM(state, program) {
            const programs = state.currentEntrantPrograms;
            programs.push(program);
            state.currentEntrantPrograms = programs;
        },
        REMOVE_RECRUITMENT_PROGRAM(state, id) {
            const programs = state.currentEntrantPrograms;
            const index = programs.findIndex(program => program.id === id);
            programs.splice(index, 1);
            state.currentEntrantPrograms = programs;
        },
        UPDATE_RECRUITMENT_PROGRAM(state, program) {

            console.log(program);

            let programs = [...[], ...state.currentEntrantPrograms];
            let currentProgramIndex = programs.findIndex(curProgram => curProgram.id === program.id);

            console.log(currentProgramIndex);
            programs[currentProgramIndex] = program;

            state.currentEntrantPrograms = programs;
        }
    },
    actions: {
        SET_CURRENT_ENTRANT({commit}, entrant) {
            commit('SET_CURRENT_ENTRANT', entrant)
        },
        ADD_RECRUITMENT_PROGRAM({commit}, program) {
            commit('ADD_RECRUITMENT_PROGRAM', program);
        },
        SET_RECRUITMENT_PROGRAM_LIST({commit}, programs) {
            commit('SET_RECRUITMENT_PROGRAM_LIST', programs);
        },
        REMOVE_RECRUITMENT_PROGRAM({commit}, id) {
            commit('REMOVE_RECRUITMENT_PROGRAM', id);
        },
        UPDATE_RECRUITMENT_PROGRAM({commit}, program) {
            commit('UPDATE_RECRUITMENT_PROGRAM', program);
        }
    }
};

export default entrant;
