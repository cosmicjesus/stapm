import {getCurrentPeriod} from "../../utils/training-periods";

const teacherLoad = {
    state: {
        currentAcademicYear: getCurrentPeriod(),
        currentAcademicYearGroups: [],
        currentAcademicYearGroupsIds: [],
        currentGroup: {},
        currentGroupSubjects: [],
        currentSubject: {},
        currentMonth: {},
        currentMonthLessons: []
    },
    mutations: {
        SET_CURRENT_YEAR: (state, year) => {
            state.currentAcademicYear = year;
        },
        SET_CURRENT_YEAR_GROUPS: (state, groups) => {
            state.currentAcademicYearGroups = groups;
            state.currentAcademicYearGroupsIds = groups.map(group => group.group_id);
        },
        SET_CURRENT_GROUP: (state, group) => {
            state.currentGroup = group;
            state.currentGroupSubjects = group.subjects;
        },
        SET_CURRENT_SUBJECT: (state, subject) => {
            state.currentSubject = subject;
        },
        SET_CURRENT_MONTH: (state, month) => {
            state.currentMonth = month;
        },
        SET_CURRENT_MONTH_LESSONS: (state, lessons) => {
            state.currentMonthLessons = _.sortBy(lessons, ['date', 'number']);
        },
        ADD_LESSON_IN_MONTH: (state, lesson) => {
            let lessons =state.currentMonthLessons.slice();
            lessons.push(lesson);
            state.currentMonthLessons = _.sortBy(lessons, ['date', 'number']);
        },
        ADD_SUBJECT: (state, subject) => {
            const subjects = state.currentGroupSubjects;

            subjects.push(subject);
            state.currentGroupSubjects = _.sortBy(subjects, 'name');
        }
    },
    actions: {
        SET_CURRENT_YEAR: ({commit}, year) => {
            commit('SET_CURRENT_YEAR', year);
        },
        SET_CURRENT_YEAR_GROUPS: ({commit}, groups) => {
            commit('SET_CURRENT_YEAR_GROUPS', groups);
        },
        SET_CURRENT_GROUP: ({commit}, group) => {
            commit('SET_CURRENT_GROUP', group);
        },
        ADD_SUBJECT: ({commit}, subject) => {
            commit('ADD_SUBJECT', subject);
        },
        SET_CURRENT_SUBJECT: ({commit}, subject) => {
            commit('SET_CURRENT_SUBJECT', subject);
        },
        SET_CURRENT_MONTH: ({commit}, month) => {
            commit('SET_CURRENT_MONTH', month);
        },
        SET_CURRENT_MONTH_LESSONS: ({commit}, lessons) => {
            commit('SET_CURRENT_MONTH_LESSONS', lessons);
        },
        ADD_LESSON_IN_MONTH: ({commit}, lesson) => {
            commit('ADD_LESSON_IN_MONTH', lesson);
        },
    }
};

export default teacherLoad;
