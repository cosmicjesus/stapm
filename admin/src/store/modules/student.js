const student = {
    state: {
        student: {},
        decrees: {},
        militaryReference: {},
        selectedEntrants: []
    },
    mutations: {
        SET_STUDENT(state, student) {
            state.student = student;
            if (student.decrees) {
                state.decrees = student.decrees;
            }
        },
        UPDATE_STUDENT(state, data) {
            state.student = {...state.student, ...data};
        },
        SET_MILITARY_REFERENCE(state, data) {
            state.militaryReference = data;
        },
        SELECTED_ENTRANTS(state, entrants) {
            state.selectedEntrants = entrants;
        }
    },
    actions: {
        SET_STUDENT({commit}, student) {
            commit('SET_STUDENT', student);
        },
        UPDATE_STUDENT({commit}, student) {
            commit('UPDATE_STUDENT', student);
        },
        SET_MILITARY_REFERENCE({commit}, reference) {
            commit('SET_MILITARY_REFERENCE', reference);
        },
        SET_ENTRANTS({commit}, entrants) {
            commit('SELECTED_ENTRANTS', entrants);
        }
    }
};

export default student;