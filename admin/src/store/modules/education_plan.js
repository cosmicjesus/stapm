import Vue from 'vue'

import {allEducationPlans} from "../../api/education/education-plans";

const educationPlan = {
    state: {
        currentPlan: {},
        plansToSelect: []
    },
    mutations: {
        SET_PLANS_TO_SELECT(state, plans) {
            state.plansToSelect = plans;
        }
    },
    actions: {
        SET_EDUCATION_PLANS_TO_SELECT({commit}) {
            allEducationPlans()
                .then(response => response.data)
                .then(data => {
                    commit('SET_PLANS_TO_SELECT', data);
                });
        }
    }
};

export default educationPlan;
