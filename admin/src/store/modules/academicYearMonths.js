const academicYearMonths = {
    state: {
        months: []
    },
    mutations: {
        SET_YEAR_MONTHS: (state, months) => {
            state.months = months;
        }
    },
    actions: {
        SET_YEAR_MONTHS({commit}, months) {
            commit('SET_YEAR_MONTHS', months);
        }
    }
};

export default academicYearMonths;
