import routes from './routes.json'
import {getToken} from "../utils/auth";


export default function (routeName, replacements = {}, onlyPath = false) {
    var uri = routes[routeName]

    if (uri === undefined)
        console.error('Cannot find route:', routeName)

    Object.keys(replacements)
        .forEach(key => uri = uri.replace(new RegExp('{' + key + '\\??}'), replacements[key]))

    // finally, remove any leftover optional parameters (inc leading slash)

    let isDevelopment = process.env.NODE_ENV === "development";

    let token = getToken();

    let path = uri.replace(/\/{[a-zA-Z]+\?}/, '');

    if (onlyPath) {
        return path.replace('/api', '');
    }

    if (isDevelopment) {
        if (onlyPath) {
            return path.replace('/api', '');
        }
        return `http://admin.stapm.test${path}?token=${token}`;
    } else {
        return `${path}?token=${token}`;
    }
}
