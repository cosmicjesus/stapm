import request from '@/utils/request'

const url = '/recruitment-programs';

export function getOneProgram(id) {
    return request({
        url: `${url}/${id}`,
        method: 'get'
    });
}

export function storeRecruitmentPrograms(data) {
    return request({
        url,
        method: 'post',
        data
    });
}

export function updateRecruitmentPrograms(id, data) {
    return request({
        url: `${url}/${id}`,
        method: 'put',
        data
    });
}

export function getProgramsActiveCommittee() {
    return request({
        url: `active-recruitment-programs`,
        method: 'get'
    })

}