import request from '@/utils/request'

const url = '/reports';

export function countContingent(data) {
    return request({
        url: `${url}/count-contingent`,
        method: 'post',
        data
    })
}