import request from '@/utils/request';

const url = `/export`;

export function baseToBank(data) {
    return request({
        url: `${url}/base/bank`,
        method: 'post',
        data
    })
}

export function baseFull() {
    return request({
        url: `${url}/base/full`,
        method: 'post'
    })
}

export function recruits(){
    return request({
        url: `${url}/recruits`,
        method: 'post'
    })
}