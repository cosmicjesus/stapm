import request from '@/utils/request'

const url = '/component-documents';

export function reloadFile(id, file) {
    let data = new FormData();

    data.append('file', file);

    return request({
        url: `${url}/${id}/reload-file`,
        method: 'post',
        data
    });
}

export function update(id, data) {
    return request({
        url: `${url}/${id}`,
        method: 'put',
        data
    });
}

export function destroy(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    });
}