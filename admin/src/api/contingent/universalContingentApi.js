import request from '@/utils/request';

const url = '/contingent';

export function updateData(id, data) {
    return request({
        url: `${url}/${id}`,
        method: 'put',
        data
    });
}