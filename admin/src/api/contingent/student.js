import request from '@/utils/request'

const url = '/students';

export function fetchAll(params) {
    return request({
        url,
        method: 'get',
        params
    });
}

export function one(id) {
    return request({
        url: `${url}/${id}`,
        method: 'get'
    });
}

export function updateStudent(id, data) {
    return request({
        url: `${url}/${id}`,
        method: 'put',
        data
    });
}

export function updatePersonal(data) {
    return request({
        url: `${url}/${data.id}/personal`,
        method: 'put',
        data
    })
}

export function makeTrainingReference(data) {
    return request({
        url: `${url}/${data.id}/make-training-reference`,
        method: 'post',
        //responseType: 'base64',
        data
    })
}

export function makeVoenkomatReference(data) {
    return request({
        url: `${url}/${data.student_id}/make-voenkomat-reference`,
        method: 'post',
        //responseType: 'base64',
        data
    })
}

export function updatePassport(user_id, data) {
    return request({
        url: `${url}/${user_id}/update-passport`,
        method: 'post',
        data
    })
}

export function checkUnique(data) {
    return request({
        url: `${url}/check-unique`,
        method: 'post',
        data
    })
}

export function createStudent(data) {
    return request({
        url,
        method: 'post',
        data
    })
}

export function createDecree(data) {
    return request({
        url: `${url}/${data.student_id}/add-decree`,
        method: 'post',
        data
    })
}

export function updateAddresses(id, data) {
    return request({
        url: `${url}/${id}/update-addresses`,
        method: 'post',
        data
    })
}

export function updateEducation(id, data) {
    return request({
        url: `${url}/${id}/update-education`,
        method: 'post',
        data
    })
}

export function retireStudents(data) {
    return request({
        url: `${url}/retire-students`,
        method: 'post',
        data
    })
}

export function transferStudents(data) {
    return request({
        url: `${url}/transfer-students`,
        method: 'post',
        data
    })
}

export function reinstateStudents(data) {
    return request({
        url: `${url}/reinstate-students`,
        method: 'post',
        data
    })
}

export function exportToAsuRso(id) {
    return request({
        url: `${url}/${id}/export-to-asurso`,
        method: 'get'
    })
}

export function uploadPhoto(id, file) {

    let data = new FormData();

    data.append('file', file);

    return request({
        url: `${url}/${id}/upload-photo`,
        method: 'post',
        data
    });
}

export function deletePhoto(id) {
    return request({
        url: `${url}/${id}/delete-photo`,
        method: 'post'
    });
}

export function createStudentFile(id, input) {
    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }
    return request({
        url: `${url}/${id}/add-file`,
        method: 'post',
        data
    });
}

export function deleteFile(id) {
    return request({
        url: `/student-files/${id}`,
        method: 'delete',
    });
}

export function massPrintMilitaryReferences(data) {
    return request({
        url: `${url}/print-references/military`,
        method: 'post',
        data
    })
}