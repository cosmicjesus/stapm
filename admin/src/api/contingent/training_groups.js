import request from '@/utils/request'
import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

const url = '/training-groups';

export function index(params) {
    return request({
        url,
        method: 'get',
        params
    });
}

export function show(id) {
    return marshall({
        url: route('admin.training-groups.show', {training_group: id}, true),
        method: 'get'
    });
}

export function store(data) {
    return marshall({
        url: route('admin.training-groups.store', {}, true),
        method: 'post',
        data
    });
}

export function graduation(data) {
    return marshall({
        url: route('admin.training-groups.graduation', {}, true),
        method: 'post',
        data
    });
}

export function getTransferMap() {
    return marshall({
        url: route('admin.training-groups.transfer-map', {}, true),
        method: 'get'
    });
}

export function transferNextTerm(data) {
    return marshall({
        url: route('admin.training-groups.transfer-next-term', {}, true),
        method: 'post',
        data
    });
}

export function transferNextYear(data) {
    return marshall({
        url: route('admin.training-groups.transfer-next-year', {}, true),
        method: 'post',
        data
    });
}
