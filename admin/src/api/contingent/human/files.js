import marshall from '@/utils/marshall';
import route from '@/routes/route.js';

export function store(human_id, file) {
    let data = new FormData();
    for (let index in file) {
        data.append(index, file[index]);
    }
    return marshall({
        url: route('api.human.files.store', {human_id}, true),
        method: 'post',
        data
    });
}

export function deleteFile(params) {
    return marshall({
        url: route('api.human.files.destroy', params, true),
        method: 'delete',
    });
}
