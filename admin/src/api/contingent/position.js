import request from '@/utils/request'

export function fetchList(params) {
    return request({
        url: '/positions',
        method: 'get',
        params
    })
}

export function createPosition(data) {
    return request({
        url: '/positions',
        method: 'post',
        data
    })
}

export function updatePosition(data) {
    return request({
        url: `/positions/${data.id}`,
        method: 'put',
        data
    })
}

export function deletePosition(id) {
    return request({
        url: `/positions/${id}`,
        method: 'delete'
    })
}