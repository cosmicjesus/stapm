import marshall from '@/utils/marshall';
import route from '@/routes/route.js';

export function store(data) {
    return marshall({
        url: route('api.employee.previous-jobs.store', {id: data.employee_id}, true),
        method: 'post',
        data
    });
}
