import request from '@/utils/request'

const url = '/recruitment-centers';

export function index() {
    return request({
        url,
        method: 'get'
    });
}

export function store(data) {
    return request({
        url,
        method: 'post',
        data
    });
}

export function update(data) {
    return request({
        url: `${url}/${data.id}`,
        method: 'put',
        data
    });
}

export function destroy(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    });
}