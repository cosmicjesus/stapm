import marshall from '@/utils/marshall';
import request from '@/utils/request';
import route from '@/routes/route.js';

export function store(entrant_id, data) {
    return marshall({
        url: route('api.entrants.recruitment-programs.store', {entrant_id}, true),
        method: 'post',
        data
    });
}

export function update(params, data) {
    return marshall({
        url: route('api.entrants.recruitment-programs.update', params, true),
        method: 'put',
        data
    });
}

export function deleteProgram(params) {
    return marshall({
        url: route('api.entrants.recruitment-programs.destroy', params, true),
        method: 'delete',
    });
}
