import request from '@/utils/request';

const url = '/decrees';

export function fetchAll(params) {
    return request({
        url,
        method: 'get',
        params
    });
}

export function uploadFile(input) {
    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }
    return request({
        url: `${url}/${input.id}/upload-file`,
        method: 'post',
        data
    });
}

export function updateDecree(data) {
    return request({
        url: `${url}/${data.id}`,
        method: 'put',
        data
    })
}

export function detailDecree(id) {
    return request({
        url: `${url}/${id}`,
        method: 'get',
    });
}