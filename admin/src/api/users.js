import request from '@/utils/request'

const url = '/users';

export function fetchUsers() {
    return request({
        url,
        method: 'get',
    })
}

export function createUser(data) {
    return request({
        url,
        method: 'post',
        data
    })
}

export function updateUser(data) {
    return request({
        url: `${url}/${data.id}`,
        method: 'put',
        data
    })
}

export function deleteCredentials(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    })
}