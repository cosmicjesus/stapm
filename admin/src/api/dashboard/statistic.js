import request from '@/utils/request'

const url = '/statistic';

export function getStatistic() {
    return request({
        url,
        method: 'get',
    })
}

export function expiredPassports() {
    return request({
        url: `${url}/expired-passports`,
        method: 'get',
    })
}

export function printExpiredPassports(data) {
    return request({
        url: `${url}/export-expired-passports`,
        method: 'post',
        data
    })
}