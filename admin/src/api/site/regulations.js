import request from '@/utils/request'

const url = '/regulations';

export function regulationsList(params) {
    return request({
        url,
        method: 'get',
        params
    })
}

export function getRegulation(id) {
    return request({
        url: `${url}/${id}`,
        method: 'get'
    });
}