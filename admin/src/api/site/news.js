import request from '@/utils/request';

const url = '/news'

export function fetchAll(params) {
    return request({
        url,
        method: 'get',
        params
    })
}

export function create(input, inputFiles, images) {
    let data = new FormData();

    for (let index in input) {
        data.append(index, input[index]);
    }
    data.append('files', []);
    for (let index in inputFiles) {
        data.append(`files[${index}][name]`, inputFiles[index].name);
        data.append(`files[${index}][file]`, inputFiles[index].file);
    }

    data.append('images', []);
    for (let index in images) {
        data.append(`images[${index}]`, images[index]);
    }
    return request({
        url,
        method: 'post',
        data
    })
}

export function deleteNews(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    });
}