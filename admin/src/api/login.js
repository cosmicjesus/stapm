import request from '@/utils/request'

export function loginByUsername(login, password) {
  const data = {
    login,
    password
  }
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/me',
    method: 'get',
    params: { token }
  })
}

