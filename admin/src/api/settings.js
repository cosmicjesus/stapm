import request from '@/utils/request'

const url = '/settings';

export function getSettings() {
    return request({
        url,
        method: 'get'
    });
}

export function setSettings(data) {
    return request({
        url,
        method: 'post',
        data
    });
}