import request from '@/utils/request'

export function addDocumentInSubject(input) {
    let program_id = input.profession_program_id;
    let subject_id = input.subject_id;
    let data = new FormData();
    _.forEach(input, (item, index) => {
        data.append(index, input[index]);
    });
    return request({
        url: `/profession-programs/${program_id}/subjects/${subject_id}/add-file`,
        method: 'post',
        data
    });
}

export function getProgramSubjectFiles(params) {
    return request({
        url: `/profession-programs/${params.program_id}/subjects/${params.current_subject_id}/files`,
        method: 'get'
    });
}

export function deleteFile(id) {
    return request({
        url: `/profession-programs/program-files/${id}/delete`,
        method: 'delete'
    });
}

export function updateFile(input) {
    let data = new FormData();
    data.append('id', input.id);
    data.append('program_id', input.program_id);
    data.append('file', input.file);
    console.dir(data);
    return request({
        url: `/profession-programs/program-files/${input.id}/update-file`,
        method: 'post',
        data
    });
}

export function addSubject(data) {
    return request({
        url: `/profession-programs/${data.program_id}/add-subject`,
        method: 'post',
        data
    });
}

export function deleteSubject(data) {
    let subject_id = data.subject_id;
    let profession_program_id = data.profession_program_id;
    return request({
        url: `/profession-programs/${profession_program_id}/subjects/${subject_id}/delete`,
        method: 'delete'
    });
}

export function updateBasicDocument(input) {
    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }
    return request({
        url: `/profession-programs/basic-documents/${input.id}/update`,
        method: 'post',
        data
    });
}

export function addAdditional(input) {
    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }
    return request({
        url: `/profession-programs/${input.program_id}/add-additional`,
        method: 'post',
        data
    });
}

export function updateProgramSubject(data) {
    return request({
        url: `/program-subject/${data.id}`,
        method: 'put',
        data
    })
}

export function uploadComponent(input) {
    let data = new FormData();

    for (let index in input) {
        data.append(index, input[index]);
    }

    return request({
        url: `program-documents/${input.id}/create-component`,
        method: 'post',
        data
    });
}

export function buildFromComponents(id) {
    return request({
        url: `/program-documents/${id}/build`,
        method: 'post'
    })
}