import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

export function index(params) {
    return marshall({
        url: route('api.academic-years.months.index', {}, true),
        method: 'get',
        params
    })
}

export function store(data) {
    return marshall({
        url: route('api.academic-years.months.store', {}, true),
        method: 'post',
        data
    })
}
