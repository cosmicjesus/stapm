import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

export function getGroups(year) {
    return marshall({
        url: route('api.teacher-load.groups', {year}, true),
        method: 'get'
    })
}

export function getGroupsByYear(year) {
    return marshall({
        url: route('api.training-groups.by-year', {year}, true),
        method: 'get'
    })
}

export function addGroupsInLoad(data) {
    return marshall({
        url: route('api.teacher-load.add-group', {}, true),
        method: 'post',
        data
    })
}

export function getGroupLoad(id) {
    return marshall({
        url: route('api.teacher-load.group', {id}, true),
        method: 'get'
    })
}

export function addSubjectInGroup(data) {
    return marshall({
        url: route('api.teacher-load.group.add-subject', {id:data.id}, true),
        method: 'post',
        data
    })
}

export function getDetailSubject(params) {
    return marshall({
        url: route('api.teacher-load.group.get-subject', params, true),
        method: 'get',
    })
}

export function getMonthItem(params) {
    return marshall({
        url: route('api.teacher-load.group.get-month-item', params, true),
        method: 'get',
    })
}

export function addLesson(params,data) {
    return marshall({
        url: route('api.teacher-load.group.add-lesson', params, true),
        method: 'post',
        data
    })
}
