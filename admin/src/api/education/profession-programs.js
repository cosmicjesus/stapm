import request from '@/utils/request'
import route from '@/routes/route.js'
import marshall from '@/utils/marshall'

export function fetchList(query) {
    return request({
        url: '/profession-programs',
        method: 'get',
        params: query
    })
}

export function createProgram(data) {
    return request({
        url: '/profession-programs',
        method: 'post',
        data
    });
}

export function updateProgram(data) {
    return request({
        url: `/profession-programs/${data.id}`,
        method: 'put',
        data
    });
}

export function getOne(id) {
    return request({
        url: `/profession-programs/${id}`,
        method: 'get'
    });
}

export function getSubjectsByAcademicYear(params) {
    return request({
        url: route('api.profession-programs.subject-by-year', {
            profession_program_id: params.profession_program_id,
            academic_year_id: params.academic_year_id
        }, true),
        method: 'get'
    });
}

export function putImage(id, image) {
    let data = new FormData();

    data.append('image', image);

    return request({
        url: `/profession-programs/${id}/put-image`,
        method: 'post',
        data
    });
}

export function deleteSubjectFromProgram(params) {
    return request({
        url: `/profession-programs/${params.profession_program_id}/subjects/${params.subject_id}/delete`,
        method: 'delete'
    });
}

export function getEducationPlanByProgramId(profession_program_id) {
    return marshall({
        url: route('api.profession-program.education-plans', {profession_program_id}, true),
        method: 'get'
    });
}