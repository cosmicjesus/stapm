var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('gulp-browserify');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var merge = require('merge-stream');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var injectPartials = require('gulp-inject-partials');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var htmlmin = require('gulp-htmlmin');
var gulpPug = require('gulp-pug');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify-es').default;
var mainBowerFiles = require('main-bower-files');
var filter = require('gulp-filter');
var order = require('gulp-order');
let postcss = require('gulp-postcss');
let pref = require('autoprefixer');
var env = process.env.NODE_ENV || 'production';

var SOURCEPATHS = {
    sassSource: 'resources/assets/sass/**/*.scss',
    sassAdmin: 'resources/assets/admin/style/*.scss',
    sassApp: 'src/scss/*.scss',
    pugSource: 'src/templates/**/*.pug',
    jsSource: 'resources/assets/js/main.js',
    jsSourceAdm: 'resources/assets/admin/scripts/admin-scripts.js',
    jsSourceWatch: 'resources/assets/js/*.js',
    jsSourceWatchAdmin: 'resources/assets/admin/scripts/*.js',
    imgSource: 'src/img/**'
}
let adminvensors = {
    js: [
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/babel-polyfill/dist/polyfill.min.js",
        "node_modules/bootstrap/dist/js/bootstrap.js",
        "resources/assets/admin/vendor/js/adminlte.min.js",
        "node_modules/lodash/lodash.js",
        "node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
        "node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js",
        "node_modules/datatables.net/js/jquery.dataTables.js",
        "node_modules/datatables.net-bs/js/dataTables.bootstrap.js",
        "node_modules/datatables.net-buttons/js/dataTables.buttons.js",
        "node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js",
        "node_modules/datatables.net-buttons/js/buttons.print.js",
        "node_modules/moment/min/moment-with-locales.js",
        "node_modules/sweetalert2/dist/sweetalert2.all.js",
        "resources/assets/admin/vendor/js/bootstrap3-wysihtml5.all.js",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js",
        "node_modules/select2/dist/js/select2.full.js",
        'resources/assets/admin/vendor/js/dataTables.checkboxes.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.min.js',
        'node_modules/jquery-validation/dist/additional-methods.js',
        'node_modules/jquery-validation/dist/localization/messages_ru.js',
        'resources/assets/admin/vendor/js/jquery.inputmask.bundle.min.js',
        "node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js",
        "node_modules/axios/dist/axios.min.js"

    ],
    style: [
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "node_modules/font-awesome/css/font-awesome.min.css",
        "resources/assets/admin/vendor/style/AdminLTE.min.css",
        "resources/assets/admin/vendor/style/_all-skins.min.css",
        "node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
        "node_modules/datatables.net-bs/css/dataTables.bootstrap.css",
        "node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.css",
        "node_modules/sweetalert2/dist/sweetalert2.css",
        "resources/assets/admin/vendor/style/bootstrap3-wysihtml5.min.css",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
        "node_modules/select2/dist/css/select2.min.css",
        "resources/assets/admin/vendor/style/dataTables.checkboxes.css",
        'resources/assets/admin/vendor/style/inputmask.css',
        "node_modules/easy-autocomplete/dist/easy-autocomplete.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.themes.css"
    ]
}

let clientvendors = {
    js: [
        "resources/assets/jquery.js",
        "node_modules/babel-polyfill/dist/polyfill.min.js",
        "node_modules/lodash/lodash.js",
        "node_modules/datatables.net/js/jquery.dataTables.js",
        "node_modules/datatables.net-responsive/js/dataTables.responsive.js",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js",
        "resources/assets/jquery.cookie.min.js",
        "node_modules/owl.carousel/dist/owl.carousel.min.js",
        "resources/assets/vendor/js/tabulous.min.js",
        "node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js",
        "node_modules/axios/dist/axios.min.js"
    ],
    style: [
        "node_modules/normalize-css/normalize.css",
        "node_modules/font-awesome/css/font-awesome.min.css",
        "node_modules/datatables.net-dt/css/jquery.dataTables.css",
        "node_modules/datatables.net-responsive-dt/css/responsive.dataTables.css",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
        "node_modules/owl.carousel/dist/assets/owl.carousel.min.css",
        "node_modules/owl.carousel/dist/assets/owl.carousel.default.min.css",
        "resources/assets/vendor/style/bootstrap.min.css",
        "node_modules/blueprint-css/dist/blueprint.min.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.css",
        "node_modules/easy-autocomplete/dist/easy-autocomplete.themes.css"

    ]
}
var APPPATH = {
    root: '/',
    css: 'public/css',
    js: 'public/js',
    fonts: 'public/fonts',
    img: 'public/img'
}

gulp.task('clean-html', function () {
    return gulp.src(APPPATH.root + '/*.html', {read: false, force: true})
        .pipe(clean());
});
gulp.task('clean-scripts', function () {
    return gulp.src(APPPATH.js + '/main.js', {read: false, force: true})
        .pipe(clean());
});


gulp.task('clean-scripts-adm', function () {
    return gulp.src(APPPATH.js + '/admin-scripts.js', {read: false, force: true})
        .pipe(clean());
});

gulp.task('clean-scripts-admin:vendor', function () {
    return gulp.src('public/js/admin-vendor.js', {read: false, force: true})
        .pipe(clean());
});

gulp.task('clean-style-admin:vendor', function () {
    return gulp.src('public/css/admin-vendor.css', {read: false, force: true})
        .pipe(clean());
});

gulp.task('clean-images', function () {
    return gulp.src(APPPATH.img + '/**', {read: false, force: true})
        .pipe(clean());
});

gulp.task('scripts:vendor', function () {
    return gulp.src(vendors)
        .pipe(filter('**.js'))
        .pipe(order(vendors))
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('dist/js'));
})

gulp.task('vendor:js', function () {
    return gulp.src(clientvendors.js)
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(APPPATH.js));
});

gulp.task('vendor:css', function () {
    return gulp.src(clientvendors.style)
        .pipe(cssmin())
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(APPPATH.css));
});

gulp.task('sass', function () {

    var config = {
        outputStyle: 'expanded'
    };

    // if(env === 'development'){
    // 	config.sourceComments = "map";
    // }

    if (env === 'production') {
        config.outputStyle = "compressed";
    }

    return gulp.src(SOURCEPATHS.sassSource)
        .pipe(postcss([pref()], {
            syntax: require('postcss-scss')
        }))
        //.pipe(autoprefixer())
        .pipe(sass(config).on('error', sass.logError))
        .pipe(gulp.dest(APPPATH.css))
});

gulp.task('admin-sass', function () {

    var config = {};


    config.outputStyle = "compressed";

    return gulp.src(SOURCEPATHS.sassAdmin)
        .pipe(postcss([pref()], {
            syntax: require('postcss-scss')
        }))
        .pipe(sass(config).on('error', sass.logError))
        .pipe(gulp.dest(APPPATH.css))
});

gulp.task('images', ['clean-images'], function () {
    return gulp.src(SOURCEPATHS.imgSource)
        .pipe(newer(APPPATH.img))
        .pipe(imagemin())
        .pipe(gulp.dest(APPPATH.img));
});


gulp.task('scripts:admin:vendor', ['clean-scripts-admin:vendor'], function () {
    gulp.src(adminvensors.js)
    // .pipe(browserify({ debug: env === 'development' }))
    // .pipe(babel({
    //     presets: ['env']
    // }))
        .pipe(uglify())
        .pipe(concat('admin-vendor.js'))
        .pipe(gulp.dest('public/js/'))
});

gulp.task('style:admin:vendor', ['clean-style-admin:vendor'], function () {
    gulp.src(adminvensors.style)
        .pipe(concat('admin-vendor.css'))
        .pipe(gulp.dest('public/css/'))
});


gulp.task('scripts', ['clean-scripts'], function () {
    gulp.src(SOURCEPATHS.jsSource)
        .pipe(browserify({debug: env === 'development'}))
        .pipe(babel({
            presets: ['env'],
        }))
        .pipe(gulpIf(env === 'production', uglify()))
        .pipe(gulp.dest(APPPATH.js))
});

gulp.task('scripts-admin', ['clean-scripts-adm'], function () {
    gulp.src(SOURCEPATHS.jsSourceWatchAdmin)
    //        .pipe(browserify({ debug: env === 'development' }))
        .pipe(concat('admin-scripts.js'))
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulpIf(env === 'production', uglify()))
        .pipe(gulp.dest(APPPATH.js))
});

/** Задачи на продакшн **/
gulp.task('compress', function () {
    gulp.src(SOURCEPATHS.jsSource)
        .pipe(browserify())
        .pipe(minify())
        .pipe(gulp.dest(APPPATH.js))
});

gulp.task('compresscss', function () {

    var sassFiles;
    sassFiles = gulp.src(SOURCEPATHS.sassSource)
        .pipe(autoprefixer())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))

        .pipe(concat('style.css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(APPPATH.css));
});


/** Задачи на продакшн **/

gulp.task('pug', function () {
    return gulp.src(SOURCEPATHS.pugSource)
        .pipe(gulpPug({
            pretty: true
        }))
        .pipe(clean())
        .pipe(gulp.dest(APPPATH.root))
});
/*
gulp.task('copy', ['clean-html'], function() {
  gulp.src(SOURCEPATHS.htmlSource)
      .pipe(gulp.dest(APPPATH.root))
});
*/

gulp.task('serve', ['sass'], function () {
    browserSync.init([APPPATH.css + '/*.css', APPPATH.js + '/*.js', 'resources/views/**/*.php', 'app/**/*.php'], {
        server: {
            baseDir: APPPATH.root,
            proxy: 'stpm.dev'
        }
    })
});

gulp.task('watch', ['sass', 'admin-sass', 'clean-scripts', 'vendor:css', 'vendor:js', 'scripts', 'scripts-admin', 'scripts:admin:vendor', 'style:admin:vendor'], function () {
    gulp.watch([SOURCEPATHS.sassSource], ['sass']);
    gulp.watch([SOURCEPATHS.sassAdmin], ['admin-sass']);
    //gulp.watch([SOURCEPATHS.htmlSource], ['copy']);
    gulp.watch([SOURCEPATHS.jsSourceWatch], ['scripts']);
    gulp.watch([SOURCEPATHS.jsSourceWatchAdmin], ['scripts-admin']);
    //gulp.watch([SOURCEPATHS.imgSource], ['images', 'clean-images']);
    gulp.watch('resources/views/**/*.php', browserSync.reload);
    gulp.watch('app/**/*.php', browserSync.reload);
});

gulp.task('default', ['watch']);
gulp.task('build', ['sass', 'clean-scripts', 'vendor:css', 'vendor:js', 'scripts', 'scripts-admin', 'scripts:admin:vendor', 'style:admin:vendor']);

gulp.task('production', ['compresscss', 'compress']);
