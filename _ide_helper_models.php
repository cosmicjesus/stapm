<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles)
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $login
 * @property int|null $user_profile_id
 * @property string|null $deleted_at
 * @property bool $banned
 * @property-read \App\Models\Employee $profile
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBanned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserProfileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EmployeeEducation
 *
 * @property int $id
 * @property int $employee_id
 * @property int $education_id
 * @property string|null $qualification
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $direction_of_preparation
 * @property-read \App\Models\Education $education
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereDirectionOfPreparation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $graduation_organization_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereGraduationOrganizationName($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \App\Models\Employee $employee
 */
	class EmployeeEducation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EducationPlan
 *
 * @property int $id
 * @property int $profession_program_id
 * @property string $name
 * @property int $count_period
 * @property \Illuminate\Support\Carbon $start_training
 * @property \Illuminate\Support\Carbon $end_training
 * @property string|null $file
 * @property string|null $full_path
 * @property bool $active
 * @property int $in_revision
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property array|null $extra_options
 * @property-read \App\Models\ProfessionProgram $professionProgram
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroup[] $training_groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereCountPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereEndTraining($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereExtraOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereFullPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereInRevision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereStartTraining($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComponentDocument[] $components
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 */
	class EducationPlan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\News
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $image
 * @property \Illuminate\Support\Carbon $publication_date
 * @property \Illuminate\Support\Carbon|null $visible_to
 * @property bool $active
 * @property string $preview
 * @property string $full_text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NewsFile[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NewsImage[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereVisibleTo($value)
 * @mixin \Eloquent
 */
	class News extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PositionType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class PositionType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SocialPartnerFile
 *
 * @property int $id
 * @property int $social_partner_id
 * @property string $name
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereSocialPartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class SocialPartnerFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Document
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $name
 * @property string|null $path
 * @property int $sort
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array|null $category_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoriesDocument[] $categories
 * @property-read \App\Models\CategoriesDocument $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCategoryIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Document extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\NewsPaper
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $publication_date
 * @property int $number
 * @property string|null $cover_path
 * @property string|null $publication_path
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereCoverPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper wherePublicationPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class NewsPaper extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdministrationEmployee
 *
 * @property int $id
 * @property int $employee_id
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee $employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class AdministrationEmployee extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProgramDocument
 *
 * @property int $id
 * @property string $title
 * @property string $path
 * @property int $programtagle_id
 * @property string $programtagle_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereProgramtagleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereProgramtagleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComponentDocument[] $components
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $programtagle
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereActive($value)
 */
	class ProgramDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EmployeeSubject
 *
 * @property int $id
 * @property int $employee_id
 * @property int $subject_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class EmployeeSubject extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Position
 *
 * @property int $id
 * @property string $name
 * @property int $position_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmployeePosition[] $employee_position
 * @property-read mixed $can_remove
 * @property-read \App\Models\PositionType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position wherePositionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Position extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ReplacingClassesImage
 *
 * @property int $id
 * @property int $replacing_class_id
 * @property string $path
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereReplacingClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ReplacingClass $parentEntity
 */
	class ReplacingClassesImage extends \Eloquent {}
}

namespace App{
/**
 * App\ProfessionProgramType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereUpdatedAt($value)
 */
	class ProfessionProgramType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EducationForm
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereUpdatedAt($value)
 */
	class EducationForm extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RegulationFile
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $regulation_type
 * @property int $regulation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereRegulationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereRegulationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class RegulationFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProgramBasicDocument
 *
 * @property int $id
 * @property int $profession_program_id
 * @property string|null $standart
 * @property string|null $annotation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereStandart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class ProgramBasicDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\NewsFile
 *
 * @property int $id
 * @property int $news_id
 * @property string $name
 * @property string|null $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class NewsFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ComponentDocument
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property int $sort
 * @property string $filelable_type
 * @property int $filelable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFilelableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFilelableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $file_type
 * @property int $file_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFileType($value)
 */
	class ComponentDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property string $name
 * @property string|null $path
 * @property int $sort
 * @property int $active
 * @property string|null $objectable_type
 * @property int|null $objectable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereObjectableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereObjectableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class File extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Regulation
 *
 * @property int $id
 * @property string $name_of_the_supervisory
 * @property string|null $comment
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationFile[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationItem[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereNameOfTheSupervisory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Regulation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingGroupAcademicYear
 *
 * @property int $id
 * @property int|null $training_group_id Айдишник группы
 * @property int|null $training_calendar_id Айдишник учебного календаря
 * @property int|null $education_plan_period_id Айдишник периода учебного плана
 * @property int $number Номер
 * @property int $year Академический год
 * @property bool $active Флаг активности
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingCalendar $calendar
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroupAcademicYear[] $terms
 * @property-read \App\Models\TrainingGroup|null $trainingGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereEducationPlanPeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereTrainingGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereYear($value)
 * @mixin \Eloquent
 * @property-read \App\Models\TrainingGroupAcademicYearTerm $activeTerm
 * @property-read mixed $next_academic_year
 * @property int $is_past Флаг того, пройден ли период
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereIsPast($value)
 */
	class TrainingGroupAcademicYear extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingCalendar
 *
 * @property int $id
 * @property string $name
 * @property string $year
 * @property string $term_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingCalendarsItem[] $periods
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereTermType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereYear($value)
 * @mixin \Eloquent
 */
	class TrainingCalendar extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProfessionProgram
 *
 * @property-read \App\Models\EducationForm $education_form
 * @property-read mixed $full_name
 * @property-read \App\Models\Profession $profession
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereSlug($slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_id
 * @property int $education_form_id
 * @property int|null $type_id
 * @property string $slug
 * @property int $trainin_period
 * @property \Illuminate\Support\Carbon|null $licence
 * @property string|null $qualification
 * @property string|null $description
 * @property int $sort
 * @property bool $on_site
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $presentation_path
 * @property string|null $video_presentation_path
 * @property string|null $reduction
 * @property-read \App\Models\ProgramBasicDocument $documents
 * @property-read mixed $name_with_form
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramDocument[] $otherDocuments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EducationPlan[] $plans
 * @property-read \App\Models\PriemProgram $priem
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read \App\ProfessionProgramType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereEducationFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereLicence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereOnSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram wherePresentationPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereProfessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereReduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTraininPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereVideoPresentationPath($value)
 * @property bool $top_fifty
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTopFifty($value)
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereImage($value)
 * @property bool $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereActive($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProfessionProgramAcademicYear[] $academicYears
 */
	class ProfessionProgram extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StudentDecree
 *
 * @property int $id
 * @property int $student_id
 * @property int $decree_id
 * @property string|null $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $type
 * @property string|null $reason
 * @property string|null $group_from
 * @property string|null $group_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereGroupFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereGroupTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class StudentDecree extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EmployeePosition
 *
 * @property int $id
 * @property int $employee_id
 * @property int $position_id
 * @property string $start_date
 * @property int $experience
 * @property string|null $date_layoff
 * @property int $type
 * @property int $sort
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereDateLayoff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class EmployeePosition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProfessionProgramsAcademicYear
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_program_id ID программы
 * @property int $year_number Номер года
 * @property int $active Флаг активности
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ProfessionProgram $profession_program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereYearNumber($value)
 */
	class ProfessionProgramsAcademicYear extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TochkaRostaItem
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property \Illuminate\Support\Carbon $publication_date
 * @property string $preview
 * @property string $full_text
 * @property bool $active
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TochkaRostaPhoto[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem wherePreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class TochkaRostaItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CategoryDocument
 *
 * @property int $id
 * @property int $category_document_id
 * @property int $document_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereCategoryDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class CategoryDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Conference
 *
 * @property int $id
 * @property string $title
 * @property string $date_of_event
 * @property string $full_text
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereDateOfEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereSlug($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference findSimilarSlugs($attribute, $config, $slug)
 */
	class Conference extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Decree
 *
 * @property int $id
 * @property string $number
 * @property \Illuminate\Support\Carbon $date
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $file_path
 * @property int|null $term
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Decree extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingGroup
 *
 * @property int $id
 * @property int $profession_program_id
 * @property int $education_plan_id
 * @property int|null $employee_id
 * @property string $unique_code
 * @property string $pattern
 * @property int $max_people
 * @property int $course
 * @property int $period
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $term_type
 * @property string|null $expelled_decree_number
 * @property string|null $expelled_decree_date
 * @property-read \App\Models\Employee $curator
 * @property-read \App\Models\EducationPlan $education_plan
 * @property-read mixed $full_name
 * @property-read \App\Models\ProfessionProgram $program
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereEducationPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereMaxPeople($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup wherePattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereTermType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $expelled_decree_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingCalendar[] $calendars
 * @property int|null $training_calendar_item_id ID элемента календаря
 * @property-read \App\Models\TrainingCalendarsItem|null $activePeriod
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereTrainingCalendarItemId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroupAcademicYear[] $academicYears
 * @property-read \App\Models\TrainingGroupAcademicYear $currentAcademicYear
 */
	class TrainingGroup extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Student
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property int|null $education_id
 * @property int|null $profession_program_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $passport_issuance_date
 * @property float|null $avg
 * @property \Illuminate\Support\Carbon|null $graduation_date
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property bool|null $has_original
 * @property bool $has_certificate
 * @property int $need_hostel
 * @property string $status
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $phone
 * @property int $person_with_disabilities
 * @property int|null $exemption
 * @property string|null $graduation_organization_name
 * @property string|null $graduation_organization_place Населеный пункт уч.заведения
 * @property bool|null $contract_target_set
 * @property \Illuminate\Support\Carbon|null $date_of_actual_transfer
 * @property array|null $passport_data Паспорт
 * @property string|null $snils СНИЛС
 * @property string|null $inn
 * @property string|null $language Изучаемый язык
 * @property string|null $nationality Гражданство
 * @property array|null $addresses
 * @property int|null $privileged_category
 * @property int|null $health_category
 * @property int|null $disability
 * @property \Illuminate\Support\Carbon|null $enrollment_date
 * @property bool $long_absent
 * @property string|null $medical_policy_number
 * @property array|null $ratings_map
 * @property string|null $military_document_type Тип документа воинского учета
 * @property string|null $military_document_number Номер документа воинского учета
 * @property string|null $fitness_for_military_service Категория годности к военной службе
 * @property string|null $military_recruitment_office Название военкомата
 * @property string|null $military_specialty Специальность
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Decree[] $decrees
 * @property-read \App\Models\Education $education
 * @property-read mixed $full_name
 * @property-read \App\Models\TrainingGroup $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserParent[] $parents
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyStudents()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereAddresses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereAvg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereContractTargetSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDateOfActualTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDisability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEnrollmentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereExemption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereFitnessForMilitaryService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationOrganizationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationOrganizationPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHasCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHasOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHealthCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLongAbsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMedicalPolicyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryRecruitmentOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitarySpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNeedHostel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePassportData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePassportIssuanceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePersonWithDisabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePrivilegedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRatingsMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSnils($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePhoto($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudentFile[] $files
 * @property int|null $selection_committee_id
 * @property int|null $recruitment_program_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSelectionCommitteeId($value)
 * @property array|null $preferential_categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePreferentialCategories($value)
 * @property string|null $email Email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEmail($value)
 * @property string|null $military_status
 * @property int|null $recruitment_center_id
 * @property-read \App\Models\RecruitmentCenter $recruitmentCenter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRecruitmentCenterId($value)
 * @property string|null $how_find_out Откуда узнали
 * @property int|null $invited_student_id ID Пригласившего студента
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHowFindOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereInvitedStudentId($value)
 * @property-read \App\Models\Student $invitedStudent
 * @property int|null $nominal_number Поименный номер
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNominalNumber($value)
 * @property int|null $reinstate_group_id ID группу в которую будет восстановлен студент
 * @property int|null $enrollment_decree_id ID приказа о зачислении
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEnrollmentDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereReinstateGroupId($value)
 */
	class Student extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Holiday
 *
 * @property int $id
 * @property string $description
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Holiday extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MetaTag
 *
 * @property int $id
 * @property string $route
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class MetaTag extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingCalendarsItem
 *
 * @property int $id
 * @property int|null $training_calendar_id
 * @property int $number
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property \Illuminate\Support\Carbon|null $session_start_date
 * @property \Illuminate\Support\Carbon|null $session_end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingCalendar $calendar
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereSessionEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereSessionStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class TrainingCalendarsItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserParent
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property string $relation_ship_type
 * @property string|null $phone
 * @property string|null $place_of_work
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $childrens
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent wherePlaceOfWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereRelationShipType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $snils
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereSnils($value)
 * @property-read mixed $full_name
 */
	class UserParent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Entrant
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Decree[] $decrees
 * @property-read \App\Models\Education $education
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudentFile[] $files
 * @property-read mixed $full_name
 * @property-read \App\Models\TrainingGroup $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserParent[] $parents
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyStudents()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSlug($slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property string|null $photo
 * @property int|null $education_id
 * @property int|null $profession_program_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $passport_issuance_date
 * @property float|null $avg
 * @property \Illuminate\Support\Carbon|null $graduation_date
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property bool|null $has_original
 * @property bool $has_certificate
 * @property int $need_hostel
 * @property string $status
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $phone
 * @property int $person_with_disabilities
 * @property int|null $exemption
 * @property string|null $graduation_organization_name
 * @property string|null $graduation_organization_place Населеный пункт уч.заведения
 * @property bool|null $contract_target_set
 * @property \Illuminate\Support\Carbon|null $date_of_actual_transfer
 * @property array|null $passport_data Паспорт
 * @property string|null $snils СНИЛС
 * @property string|null $inn
 * @property string|null $language Изучаемый язык
 * @property string|null $nationality Гражданство
 * @property array|null $addresses
 * @property int|null $privileged_category
 * @property int|null $health_category
 * @property int|null $disability
 * @property \Illuminate\Support\Carbon|null $enrollment_date
 * @property bool $long_absent
 * @property string|null $medical_policy_number
 * @property array|null $ratings_map
 * @property string|null $military_document_type Тип документа воинского учета
 * @property string|null $military_document_number Номер документа воинского учета
 * @property string|null $fitness_for_military_service Категория годности к военной службе
 * @property string|null $military_recruitment_office Название военкомата
 * @property string|null $military_specialty Специальность
 * @property int|null $selection_committee_id
 * @property int|null $recruitment_program_id
 * @property array|null $preferential_categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereAddresses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereAvg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereContractTargetSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereDateOfActualTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereDisability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEnrollmentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereExemption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereFitnessForMilitaryService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationOrganizationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationOrganizationPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHasCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHasOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHealthCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLongAbsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMedicalPolicyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryRecruitmentOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitarySpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNeedHostel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePassportData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePassportIssuanceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePersonWithDisabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePreferentialCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePrivilegedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRatingsMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereSelectionCommitteeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereSnils($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereUpdatedAt($value)
 * @property string|null $email Email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEmail($value)
 * @property-read \App\Models\PriemProgram|null $recruitment_program
 * @property string|null $military_status
 * @property int|null $recruitment_center_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRecruitmentCenterId($value)
 * @property-read \App\Models\RecruitmentCenter $recruitmentCenter
 * @property string|null $how_find_out Откуда узнали
 * @property int|null $invited_student_id ID Пригласившего студента
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHowFindOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereInvitedStudentId($value)
 * @property-read \App\Models\Student $invitedStudent
 * @property int|null $nominal_number Поименный номер
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNominalNumber($value)
 * @property int|null $reinstate_group_id ID группу в которую будет восстановлен студент
 * @property int|null $enrollment_decree_id ID приказа о зачислении
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEnrollmentDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereReinstateGroupId($value)
 */
	class Entrant extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Help
 *
 * @property int $id
 * @property int $student_id
 * @property int $number
 * @property \Illuminate\Support\Carbon|null $date_of_application
 * @property \Illuminate\Support\Carbon|null $date_of_issue
 * @property array|null $params
 * @property string $type
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Student $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereDateOfApplication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereDateOfIssue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Help extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Employee
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property int $gender
 * @property \Illuminate\Support\Carbon $birthday
 * @property string|null $photo
 * @property int $category
 * @property \Illuminate\Support\Carbon|null $category_date
 * @property string $status
 * @property string|null $start_work_date
 * @property string|null $give_passport
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $general_experience
 * @property string|null $phones
 * @property string|null $emails
 * @property int $specialty_exp
 * @property string|null $scientific_degree
 * @property string|null $academic_title
 * @property string|null $dismiss_date
 * @property-read \App\Models\AdministrationEmployee $administrator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QualificationCourse[] $courses
 * @property-read \App\User $credentials
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Education[] $educations
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Position[] $positions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereAcademicTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCategoryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDismissDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereEmails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGeneralExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGivePassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereScientificDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereSpecialtyExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereStartWorkDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Employee extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ReplacingClass
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $date_of_replacing
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReplacingClassesImage[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereDateOfReplacing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class ReplacingClass extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SelectionCommittee
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PriemProgram[] $priem_programs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $last_day_of_work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereLastDayOfWork($value)
 */
	class SelectionCommittee extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StudentFile
 *
 * @property int $id
 * @property int|null $student_id
 * @property string|null $name
 * @property string|null $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $uuid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereUuid($value)
 */
	class StudentFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Activity
 *
 * @property int $id
 * @property string|null $log_name
 * @property string|null $event
 * @property string $description
 * @property int|null $subject_id
 * @property string|null $subject_type
 * @property int|null $causer_id
 * @property string|null $causer_type
 * @property \Illuminate\Support\Collection|null $properties
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $causer
 * @property-read mixed $changes
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $subject
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Activitylog\Models\Activity causedBy(\Illuminate\Database\Eloquent\Model $causer)
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Activitylog\Models\Activity forSubject(\Illuminate\Database\Eloquent\Model $subject)
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Activitylog\Models\Activity inLog($logNames)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCauserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCauserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereLogName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereSubjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Activity extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProgramSubject
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_program_id
 * @property int $subject_id
 * @property string|null $index
 * @property string|null $file
 * @property int $in_revision
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramDocument[] $files
 * @property-read \App\Models\Subject $subject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereInRevision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property int|null $section_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereSectionId($value)
 * @property int|null $year
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereYear($value)
 * @property int|null $program_academic_year_id ID учебного года профпрограммы
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereProgramAcademicYearId($value)
 * @property-read \App\Models\ProfessionProgram $professionProgram
 */
	class ProgramSubject extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RecruitmentCenter
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class RecruitmentCenter extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TochkaRostaPhoto
 *
 * @property int $id
 * @property int $news_id
 * @property string $path
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class TochkaRostaPhoto extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RegulationItem
 *
 * @property int $id
 * @property int $regulation_id
 * @property string $name_of_violation
 * @property string|null $result
 * @property string $status
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationFile[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereNameOfViolation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereRegulationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class RegulationItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Education
 *
 * @property int $id
 * @property string $name
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Education whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Education extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubjectType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $reduction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereReduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereUpdatedAt($value)
 */
	class SubjectType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StudentParent
 *
 * @property int $id
 * @property int $student_id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property int|null $relationshipType
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $gender
 * @property-read \App\Models\Student $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereRelationshipType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class StudentParent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SocialPartner
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image_path
 * @property string $site_url
 * @property int $sort
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $contract_type
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SocialPartnerFile[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereContractType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereSiteUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class SocialPartner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CategoriesDocument
 *
 * @property int $id
 * @property string $name
 * @property int $sort
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoriesDocument[] $childs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $filesCount
 * @property-read \App\Models\CategoriesDocument $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class CategoriesDocument extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\QualificationCourse
 *
 * @property int $id
 * @property int $employee_id
 * @property string $title
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property int $count_hours
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereCountHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class QualificationCourse extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PriemProgram
 *
 * @property int $id
 * @property int $profession_program_id
 * @property int $reception_plan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $count_with_documents
 * @property int $count_with_out_documents
 * @property int $number_of_enrolled
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCountWithDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCountWithOutDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereNumberOfEnrolled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereReceptionPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $selection_committee_id
 * @property-read \App\Models\SelectionCommittee|null $selectionCommittee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereSelectionCommitteeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $entrants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property bool $student_source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereStudentSource($value)
 */
	class PriemProgram extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Profession
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereUpdatedAt($value)
 */
	class Profession extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\NewsImage
 *
 * @property int $id
 * @property int $news_id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class NewsImage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subject
 *
 * @property-read \App\Models\SubjectType $type
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $type_id
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProfessionProgram[] $programs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereUpdatedAt($value)
 */
	class Subject extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingGroupAcademicYearTerm
 *
 * @property int $id
 * @property int|null $academic_year_id ID академического года
 * @property int|null $calendar_item_id ID элемента учебного календаря
 * @property int|null $number Номер периода
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingGroupAcademicYear|null $academicYear
 * @property-read \App\Models\TrainingCalendarsItem|null $calendarItem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereAcademicYearId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereCalendarItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $active Флаг активности
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereActive($value)
 * @property-read mixed $next_term
 * @property int $is_past Флаг того, пройден ли период
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereIsPast($value)
 */
	class TrainingGroupAcademicYearTerm extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TrainingGroupCalendar
 *
 * @property int $id
 * @property int|null $training_group_id ID группы
 * @property int|null $training_calendar_id ID календаря
 * @property bool $active Флаг активности
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereTrainingGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class TrainingGroupCalendar extends \Eloquent {}
}

