'use strict'

module.exports = {
    //You can set the vue-loader configuration by yourself.
    loaders: [
        {test: /\.css$/, loader: 'style-loader!css-loader'},
        {test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader'}
    ]
};
