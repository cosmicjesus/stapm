import moment from "moment";
import {DateTime} from "luxon";

export function getCurrentPeriod() {
    let date = new Date;
    return date.getMonth() < 8 ? date.getFullYear() - 1 : date.getFullYear()
}

export function buildAcademicYears(start_year = null) {
    let years = [];
    let minMaxYears = {
        max: 0,
        min: 0
    };
    if (start_year) {
        minMaxYears.max = start_year + 2;
        minMaxYears.min = start_year - 7;
    } else {
        let nowYear = getCurrentPeriod();
        minMaxYears.max = nowYear + 2;
        minMaxYears.min = nowYear - 7;
    }

    for (let i = minMaxYears.min; i <= minMaxYears.max; i++) {

        let start_date = DateTime.fromObject({year: i, month: 9, day: 1}).toFormat('yyyy-MM-dd HH:mm:ss');
        let end_date = DateTime.fromObject({year: i + 1, month: 7, day: 31}).toFormat('yyyy-MM-dd HH:mm:ss');

        let term = {
            year: i,
            label: `${i} - ${i + 1}`,
            start_date,
            end_date
        };

        years.push(term);
    }

    return _.orderBy(years, 'key', 'desc');
};