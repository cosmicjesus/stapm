import {all} from '@/api/contingent/employee';

const employee = {
    state: {
        currentEmployee: {},
        currentEmployeePositions: [],
        currentEmployeeCourses: [],
        currentEmployeeEducations: [],
        currentEmployeeSubjects: [],
        currentEmployeePreviousJobs: [],
        currentEmployeeMilitaryRecord:{},
        employeesToSelect: []
    },
    mutations: {
        SET_EMPLOYEES_TO_SELECT(state, employees) {
            state.employeesToSelect = employees;
        },
        SET_CURRENT_EMPLOYEE(state, employee) {
            state.currentEmployee = employee;
            state.currentEmployeePositions = employee.positions;
            state.currentEmployeeCourses = employee.courses;
            state.currentEmployeeEducations = employee.education;
            state.currentEmployeeSubjects = employee.subjects;
            state.currentEmployeePreviousJobs = employee.previous_exp;
            state.currentEmployeeMilitaryRecord = employee.military_record;
        },
        UPDATE_CURRENT_EMPLOYEE(state, payload) {
            state.currentEmployee = Object.assign(state.currentEmployee, payload);
        },
        UPDATE_CURRENT_EMPLOYEE_POSITIONS(state, positions) {
            state.currentEmployeePositions = positions;
        },
        UPDATE_CURRENT_EMPLOYEE_COURSES(state, course) {
            state.currentEmployeeCourses.push(course);
        },
        ADD_CURRENT_EMPLOYEE_EDUCATION(state, educations) {
            state.currentEmployeeEducations = educations;
        },
        ADD_CURRENT_EMPLOYEE_SUBJECTS(state, subjects) {
            state.currentEmployeeSubjects = subjects;
        },
        UPDATE_CURRENT_MILITARY_DATA(state, data) {
            state.currentEmployeeMilitaryRecord = data;
        },
        ADD_CURRENT_EMPLOYEE_PREVIOUS_JOB(state, job) {
            state.currentEmployeePreviousJobs.push(job);
            state.currentEmployeePreviousJobs = _.orderBy(state.currentEmployeePreviousJobs, 'start_date');
        }
    },
    actions: {
        SET_EMPLOYEES_TO_SELECT({commit}) {
            all()
                .then(response => response.data)
                .then(data => {
                    commit('SET_EMPLOYEES_TO_SELECT', data);
                });
        },
        SET_CURRENT_EMPLOYEE({commit}, employee) {
            commit('SET_CURRENT_EMPLOYEE', employee);
        },
        UPDATE_CURRENT_EMPLOYEE({commit}, payload) {
            commit('UPDATE_CURRENT_EMPLOYEE', payload);
        },
        UPDATE_CURRENT_EMPLOYEE_POSITIONS({commit}, positions) {
            commit('UPDATE_CURRENT_EMPLOYEE_POSITIONS', positions);
        },
        UPDATE_CURRENT_EMPLOYEE_COURSES({commit}, course) {
            commit('UPDATE_CURRENT_EMPLOYEE_COURSES', course);
        },
        ADD_CURRENT_EMPLOYEE_EDUCATION({commit}, educations) {
            commit('ADD_CURRENT_EMPLOYEE_EDUCATION', educations);
        },
        ADD_CURRENT_EMPLOYEE_SUBJECTS({commit}, subjects) {
            commit('ADD_CURRENT_EMPLOYEE_SUBJECTS', subjects);
        },
        ADD_CURRENT_EMPLOYEE_PREVIOUS_JOB({commit}, job) {
            commit('ADD_CURRENT_EMPLOYEE_PREVIOUS_JOB', job);
        },
        UPDATE_CURRENT_MILITARY_DATA({commit}, data) {
            commit('UPDATE_CURRENT_MILITARY_DATA', data);
        }
    }
};

export default employee
