const createStudentForm = {
    state: {
        form: null
    },
    mutations: {
        SET_MODEL: (state, model) => {
            state.form = model;
        },
        CLEAR_MODEL: (state) => {
            state.form = null;
        }
    },
    actions: {
        SetModel: ({commit}, model) => {
            commit('SET_MODEL', model);
        },
        ClearModel: ({commit}) => {
            commit('CLEAR_MODEL');
        }
    }
}

export default createStudentForm