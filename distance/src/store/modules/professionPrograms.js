import {getCurrentPeriod} from "../../utils/training-periods";
import Vue from 'vue'

const professionPrograms = {
    state: {
        currentYear: getCurrentPeriod(),
        profession_program_id: null,
        academic_years: [],
        currentYearSubjects: {},
        currentSubject: {},
        programsToSelect: []
    },
    mutations: {
        SET_PROGRAMS_TO_SELECT(state, programs) {
            state.programsToSelect = programs;
        },
        UPDATE_CURRENT_YEAR(state, year) {
            state.currentYear = year;
        },
        UPDATE_CURRENT_YEAR_SUBJECTS(state, data) {
            state.currentYearSubjects = data;
        },
        UPDATE_PROFESSION_PROGRAM_ID(state, id) {
            state.profession_program_id = id;
        },
        UPDATE_ACADEMIC_YEARS(state, year) {
            if (_.isArray(year)) {
                state.academic_years = [...[], ...year];
            } else {
                let years = state.academic_years;
                years.push(year);

                state.academic_years = _.sortBy(years, 'year_number');
            }
        },
        SET_CURRENT_SUBJECT(state, subject) {
            state.currentSubject = subject;
        },
        UPDATE_CURRENT_SUBJECT(state, subject) {
            state.currentSubject = {...state.currentSubject, ...subject}
        },
        PUSH_SUBJECT_IN_CURRENT_YEAR(state, subject) {
            state.currentYearSubjects.push(subject);
        },
        UPDATE_SUBJECT_IN_CURRENT_YEAR(state, subject) {
            _.forEach(state.currentYearSubjects, (item, index) => {
                if (item.id === subject.id) {
                    let currentSubject = state.currentYearSubjects[index];

                    let data = {...currentSubject, ...subject};

                    Vue.set(state.currentYearSubjects, index, data);

                }
            });
        },
        DELETE_SUBJECT_IN_CURRENT_YEAR(state, subject_id) {
            _.forEach(state.currentYearSubjects, (item, index) => {
                if (!_.isUndefined(item)) {
                    if (item.id === subject_id) {
                        Vue.delete(state.currentYearSubjects, index);
                    }
                }
            });
        },
        SORT_SUBJECT_IN_YEAR(state) {
            state.currentYearSubjects = _.orderBy(state.currentYearSubjects, ['section_id', 'index'], ['asc', 'asc'])
        }
    },
    actions: {
        SET_PROGRAMS_TO_SELECT({commit}, programs) {
            commit('SET_PROGRAMS_TO_SELECT', programs);
        },
        SET_CURRENT_YEAR({commit}, year) {
            commit('UPDATE_CURRENT_YEAR', year);
        },
        SET_PROFESSION_PROGRAM_ID({commit}, id) {
            commit('UPDATE_PROFESSION_PROGRAM_ID', id);
        },
        SET_ACADEMIC_YEARS({commit}, years) {
            commit('UPDATE_ACADEMIC_YEARS', years);
        },
        SET_ACADEMIC_YEARS_SUBJECTS({commit}, data) {
            commit('UPDATE_CURRENT_YEAR_SUBJECTS', data);
            commit('SORT_SUBJECT_IN_YEAR');
        },
        PUSH_ACADEMIC_YEARS_SUBJECTS({commit}, subject) {
            commit('PUSH_SUBJECT_IN_CURRENT_YEAR', subject);
            commit('SORT_SUBJECT_IN_YEAR');
        },
        UPDATE_SUBJECT_IN_CURRENT_YEAR({commit}, subject) {
            commit('UPDATE_SUBJECT_IN_CURRENT_YEAR', subject);
            commit('UPDATE_CURRENT_SUBJECT', subject);
            commit('SORT_SUBJECT_IN_YEAR');
        },
        SET_CURRENT_SUBJECT({commit}, subject) {
            commit('SET_CURRENT_SUBJECT', subject);
        },
        DELETE_SUBJECT_IN_CURRENT_YEAR({commit}, subject_id) {
            commit('DELETE_SUBJECT_IN_CURRENT_YEAR', subject_id);
            commit('SET_CURRENT_SUBJECT', null);
            commit('SORT_SUBJECT_IN_YEAR');
        }
    }
};

export default professionPrograms;