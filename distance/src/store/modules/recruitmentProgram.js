const recruitmentProgram = {
    state: {
        program: {},
        statistic: {},
        graphic_statistic: {}
    },
    mutations: {
        SET_RECRUITMENT_PROGRAM(state, program) {
            state.program = program;
            state.statistic = program.statistic;
            state.graphic_statistic = program.graphic_statistic;
        }
    },
    actions: {
        SET_RECRUITMENT_PROGRAM({commit}, program) {
            commit('SET_RECRUITMENT_PROGRAM', program);
        }
    }
}

export default recruitmentProgram;