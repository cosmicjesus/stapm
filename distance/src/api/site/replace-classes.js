import request from '@/utils/request';

const url = '/replace-classes';

export function getAllReplaces(params) {
    return request({
        url,
        method: 'get',
        params
    });
}

export function createReplaces(input) {

    let data = new FormData();

    data.append('date_of_replacing', input.date_of_replacing);

    for (let index in input.files) {
        data.append(`files[${index}]`, input.files[index]);
    }

    return request({
        url,
        method: 'post',
        data
    });
}