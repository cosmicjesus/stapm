import request from '@/utils/request';
import route from '@/routes/route';

export function index() {
    return request({
        url: '/conferences',
        method: 'get'
    });
}

export function store(input, files) {
    let data = new FormData();

    for (let index in input) {
        data.append(index, input[index]);
    }

    //data.append('files', []);
    for (let index in files) {
        data.append(`files[${index}][name]`, files[index].name);
        data.append(`files[${index}][path]`, files[index].file);
    }

    return request({
        url: '/conferences',
        method: 'post',
        data
    });

}