import request from '@/utils/request'

const routePrefix = '/subjects';

export function fetchList(params) {
    return request({
        url: routePrefix,
        method: 'get',
        params
    });
}

export function createSubject(data) {
    return request({
        url: routePrefix,
        method: 'post',
        data
    });
}

export function updateSubject(data) {
    return request({
        url: `${routePrefix}/${data.id}`,
        method: 'put',
        data
    });
}

export function deleteSubject(id) {
    return request({
        url: `${routePrefix}/${id}`,
        method: 'delete'
    });
}