import request from '@/utils/request'

export function fetchList(query) {
    return request({
        url: '/profession/list',
        method: 'get',
        params: query
    })
}

export function createProfession(data) {
    return request({
        url: '/profession/store',
        method: 'post',
        data
    })
}

export function editProfession(data) {
    return request({
        url: `/profession/${data.id}/update`,
        method: 'put',
        data
    })
}

export function deleteProfession(id) {
    return request({
        url: `/profession/${id}/delete`,
        method: 'delete',
    })
}