import request from '@/utils/request'
import route from '@/routes/route.js'

export function store(data) {
    return request({
        url: route('api.programs-academic-years.store', {profession_program_id: data.profession_program_id}, true),
        method: 'post',
        data
    })
}

//api.profession-programs.copy-from-another-year

export function copySubjectsFromAnotherYear(data) {
    let {profession_program_id, academic_year_id} = data;
    return request({
        url: route('api.profession-programs.copy-from-another-year', {profession_program_id, academic_year_id}, true),
        method: 'post',
        data
    })
}