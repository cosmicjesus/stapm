import request from '@/utils/request';
import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

const url = '/training-calendars';

export function all() {
    return marshall({
        url: route('api.training-calendars.all', {}, true),
        method: 'get'
    })
}

export function list(params) {
    return request({
        url,
        method: 'get',
        params
    });
}

export function store(data) {
    return request({
        url,
        method: 'post',
        data
    });
}
