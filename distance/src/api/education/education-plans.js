import request from '@/utils/request'
import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

const prefix = '/education-plans';

export function getList(params) {
    return request({
        url: prefix,
        method: 'get',
        params
    })
}

export function getOne(id) {
    return request({
        url: `${prefix}/${id}`,
        method: 'get',
    })
}

export function createEducationPlan(input) {
    let data = new FormData();

    for (let index in input) {
        if (_.isObject(input[index]) && index === 'options') {
            let options = input[index];
            for (let item in options) {
                data.append(`options[${item}]`, options[item]);
            }
        } else {
            data.append(index, input[index]);
        }
    }

    return request({
        url: prefix,
        method: 'post',
        data
    })
}

export function updateEducationPlan(data) {
    return request({
        url: `${prefix}/${data.id}`,
        method: 'put',
        data
    })
}

export function reloadFile(input) {
    let data = new FormData();

    for (let index in input) {
        data.append(index, input[index]);
    }

    return request({
        url: `${prefix}/${input.id}/refresh-file`,
        method: 'post',
        data
    })
}

export async function allEducationPlans() {
    return marshall({
        url: route('api.education-plans.all', {}, true),
        method: 'get'
    });
}

export function deletePlan(id) {
    return request({
        url: `${prefix}/${id}`,
        method: 'delete'
    })
}

export function buildFile(id) {
    return request({
        url: `${prefix}/${id}/build`,
        method: 'post'
    })
}

export function createComponent(input) {

    let data = new FormData();

    for (let index in input) {
        data.append(index, input[index]);
    }

    return request({
        url: `${prefix}/${data.id}/create-component`,
        method: 'post',
        data
    });
}