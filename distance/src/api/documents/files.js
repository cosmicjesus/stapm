import request from '@/utils/request'

const url = '/documents';

export function fetchAll(params) {
    return request({
        url,
        method: 'get',
        params
    })
}

export function createFile(input) {
    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }
    return request({
        url,
        method: 'post',
        data
    })
}

export function deleteDocument(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    })
}

export function reloadFile(input) {

    let data = new FormData();
    for (let index in input) {
        data.append(index, input[index]);
    }

    return request({
        url: `${url}/${input.id}/reload-file`,
        method: 'post',
        data
    })
}

export function editDocument(data) {
    return request({
        url:`${url}/${data.id}`,
        method: 'put',
        data
    })
}