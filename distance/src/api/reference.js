import request from '@/utils/request'

const url = '/references';

export function list(params) {
    return request({
        url: `${url}/list`,
        method: 'get',
        params
    })
}

export function printMoneyReference(data) {
    return request({
        url: `${url}/print-money-references`,
        method: 'post',
        data
    })
}