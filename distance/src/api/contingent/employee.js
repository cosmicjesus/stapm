import request from '@/utils/request';
import marshall from '@/utils/marshall';
import route from '@/routes/route.js';

const routePrefix = '/employees';

export function fetchAll(params) {
    return request({
        url: routePrefix,
        method: 'get',
        params
    })
}

export function all() {
    return marshall({
        url: route('api.employees.all', {}, true),
        method: 'get'
    });
}

export function createEmployee(data) {
    return request({
        url: routePrefix,
        method: 'post',
        data
    })
}

export function getOne(id) {
    return request({
        url: `${routePrefix}/${id}`,
        method: 'get'
    });
}

export function addPositionToEmployee(data) {
    return request({
        url: `${routePrefix}/${data.employee_id}/add-position`,
        method: 'post',
        data
    });
}

export function updatePersonalInfo(data) {
    return request({
        url: `${routePrefix}/${data.id}`,
        method: 'put',
        data
    });
}

export function updateEmployeePosition(data) {
    return request({
        url: `/employee-position/${data.id}/update`,
        method: 'put',
        data
    });
}

export function addEducationToEmployee(data) {
    return request({
        url: `${routePrefix}/${data.employee_id}/add-education`,
        method: 'post',
        data
    });
}

export function updateEmployeeEducation(data) {
    return request({
        url: `/employee-education/${data.id}/update`,
        method: 'put',
        data
    });
}

export function deleteEmployeeEducation(id) {
    return request({
        url: `/employee-education/${id}/delete`,
        method: 'delete',
    });
}

export function addSubjectToEmployee(data) {
    return request({
        url: `${routePrefix}/${data.employee_id}/add-subject`,
        method: 'post',
        data
    });
}

export function deleteEmployeeSubject(id) {
    return request({
        url: `/employee-subject/${id}/delete`,
        method: 'delete',
    });
}

export function addCourse(data) {
    return request({
        url: `${routePrefix}/${data.employee_id}/add-course`,
        method: 'post',
        data
    });
}

export function deleteCourse(id) {
    return request({
        url: `/employee-course/${id}/delete`,
        method: 'delete',
    });
}

export function militaryRecord(data) {
    const {id,employeeMilitaryData}=data;
    return marshall({
        url: route('api.employee.military-record', {id}, true),
        method: 'post',
        data:employeeMilitaryData
    });
}
