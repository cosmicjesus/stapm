import request from '@/utils/request'

const url = '/parents';

export function createParent(data) {
    return request({
        url,
        method: 'post',
        data
    })
}

export function deleteParent(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    })
}