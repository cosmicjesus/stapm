import request from '@/utils/request'

import marshall from '@/utils/marshall'
import route from '@/routes/route.js';

const url = '/entrants';

export function fetchAll(params) {
    return request({
        url,
        method: 'get',
        params
    })
}

export function one(id) {
    return request({
        url: `${url}/${id}`,
        method: 'get'
    });
}

export function checkUnique(data) {
    return request({
        url: `${url}/check-unique`,
        method: 'post',
        data
    });
}

export function store(data) {
    return request({
        url,
        method: 'post',
        data
    });
}

export function update(id, data) {
    return request({
        url: `${url}/${id}`,
        method: 'put',
        data
    });
}

export function deleteEntrants(ids) {
    return marshall({
        url: route('api.entrants.delete-entrants', {}, true),
        method: 'post',
        data: {ids}
    });
}

//api.entrants.refresh-count

export function refreshEntrantsCount() {
    return marshall({
        url: route('api.entrants.refresh-count', {}, true),
        method: 'post'
    });
}

export function enrollment(data) {
    return marshall({
        url: route('api.entrants.enrollment', {}, true),
        method: 'post',
        data
    });
}