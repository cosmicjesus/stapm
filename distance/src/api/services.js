import request from '@/utils/request';
import marshall from '@/utils/marshall'
const prefix = '/services';

export function getProfessions() {
    return request({
        url: `${prefix}/professions`,
        method: 'get'
    })
}

export function getSubjects() {
    return request({
        url: `${prefix}/subjects`,
        method: 'get'
    })
}

export function getProfessionPrograms() {
    return request({
        url: `${prefix}/profession-programs`,
        method: 'get'
    })
}

export function getPositions() {
    return request({
        url: `${prefix}/positions`,
        method: 'get'
    })
}

export function getCategories() {
    return request({
        url: `${prefix}/document-categories`,
        method: 'get'
    })
}

export function getTrainingGroups() {
    return request({
        url: `${prefix}/training-groups`,
        method: 'get'
    })
}

export function getEmployees() {
    return request({
        url: `${prefix}/employees`,
        method: 'get'
    })
}

export function activities(params) {
    return request({
        url: '/activity',
        method: 'get',
        params
    });
}