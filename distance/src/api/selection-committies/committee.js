import request from '@/utils/request'
import route from '@/routes/route.js';

const url = '/selection-committees';

export function fetchAll() {
    return request({
        url,
        method: 'get'
    })
}

export function store(data) {
    return request({
        url,
        method: 'post',
        data
    });
}

export function deleteCommittee(id) {
    return request({
        url: `${url}/${id}`,
        method: 'delete'
    });
}

export function getActiveCommittee() {
    return request({
        url: `/selection-committe/only-active`,
        method: 'get'
    });
}

export function getPrograms(id) {
    return request({
        url: `${url}/${id}/programs`,
        method: 'get'
    });
}

export function makeEntrantReport(params) {
    return request({
        url: `/selection-committee/entrant-report`,
        method: 'get',
        params
    });
}