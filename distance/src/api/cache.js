import request from '@/utils/request'

const url = '/cache';

export function subdivision() {
    return request({
        url: `${url}/subdivision-codes`,
        method: 'get'
    });
}

export function personalFormData() {
    return request({
        url: `${url}/personal-form-data`,
        method: 'get'
    });
}

export function birthPlaces() {
    return request({
        url: `${url}/birth-places`,
        method: 'get'
    });
}

export function educationalOrganization() {
    return request({
        url: `${url}/educational-organization`,
        method: 'get'
    });
}

export function locationsOfEducationalInstitutions() {
    return request({
        url: `${url}/locations-of-educational-institutions`,
        method: 'get'
    });
}

export function addressesData() {
    return request({
        url: `${url}/addresses-data`,
        method: 'get'
    });
}

export function rawSubdivision() {
    return request({
        url: `${url}/raw/subdivision-codes`,
        method: 'get'
    });
}

export function saveSubdivision(data) {
    return request({
        url: `${url}/raw/subdivision-codes`,
        method: 'post',
        data
    });
}
