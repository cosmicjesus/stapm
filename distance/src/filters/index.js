// set function parseTime,formatTime to filter
export {parseTime, formatTime} from '@/utils'
import {DateTime} from 'luxon';

import {
    referensePeriods,
    referensesParams,
    positionTypes,
    educationForms,
    professionProgramTypes,
    subjectTypes,
    documentAdditionalTypes,
    categories,
    genders,
    employeePositionType,
    educations,
    humanStatuses,
    privilegedCategories,
    documentsTypes,
    reasons,
    decreeTypes,
    relationShipTypes,
    militaryDocumentTypes,
    fitnessForMilitaryServices,
} from '@/constants'

import * as constants from '@/constants';
import moment from 'moment';
import 'moment/locale/ru'

moment().locale('ru');

const endings = {
    days: ['день', 'дня', 'дней'],
    years: ['год', 'года', 'лет'],
    months: ['месяц', 'месяца', 'месяцев'],
};

export function pluralize(number, endingArray, withNumber = true) {
    let defNumber = number;

    number = number % 100;
    let constEndings = endingArray;
    if (_.isString(endingArray)) {
        if (!_.isUndefined(endings[endingArray])) {
            constEndings = endings[endingArray];
        }else{
            throw Error('Не найдены окончания по заданному ключу');
        }
    }

    let ending = undefined;
    if (number >= 11 && number <= 19) {
        ending = constEndings[2];
    } else {
        let i = number % 10;
        switch (i) {
            case (1):
                ending = constEndings[0];
                break;
            case (2):
            case (3):
            case (4):
                ending = constEndings[1];
                break;
            default:
                ending = constEndings[2];
        }
    }

    if (withNumber) {
        return defNumber + " " + ending;
    } else {
        return ending;
    }
}

export function numberFormatter(num, digits) {
    const si = [
        {value: 1E18, symbol: 'E'},
        {value: 1E15, symbol: 'P'},
        {value: 1E12, symbol: 'T'},
        {value: 1E9, symbol: 'G'},
        {value: 1E6, symbol: 'M'},
        {value: 1E3, symbol: 'k'}
    ];
    for (let i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value + 0.1).toFixed(digits).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, '$1') + si[i].symbol
        }
    }
    return num.toString()
}

export function toThousandFilter(num) {
    return (+num || 0).toString().replace(/^-?\d+/g, m => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','))
}

export function dateFormat(date, format = 'DD.MM.Y') {
    if (_.isNull(date) || _.isUndefined(date)) {
        return null;
    }
    return moment(date).format(format)
}

export function snils(snils) {
    if (snils) {
        return `${snils.substring(0, 3)}-${snils.substring(3, 6)}-${snils.substring(6, 9)}  ${snils.substring(9)}`;
    }
}

export function calculatePeriod(start_period, end_period = null, blocks = ['months', 'days', 'years']) {
    let start = DateTime.fromISO(start_period);
    let end = _.isNull(end_period) ? DateTime.local() : DateTime.fromISO(end_period);

    return end.diff(start, blocks).toObject();
}

export function positionType(type) {
    let types = {};
    for (let positionType of positionTypes) {
        types[positionType.id] = positionType.name
    }
    return types[type] != null ? types[type] : type;
}

export function educationForm(form) {
    let forms = {};
    for (let educationForm of educationForms) {
        forms[educationForm.id] = educationForm.name
    }
    return forms[form] != null ? forms[form] : type;
}

export function professionProgramType(type) {
    let types = {};
    for (let programType of professionProgramTypes) {
        types[programType.id] = programType.name
    }
    return types[type] != null ? types[type] : type;
}

export function subjectType(type) {
    let types = {};
    for (let subjectType of subjectTypes) {
        types[subjectType.id] = subjectType.name
    }
    return types[type] != null ? types[type] : type;
}

export function logic(value) {
    return value ? 'Да' : 'Нет';
}

export function qualificationCategory(category) {
    let categoriesList = {};
    for (let category of categories) {
        categoriesList[category.id] = category.name
    }
    return categoriesList[category] != null ? categoriesList[category] : category;
}

export function additionalDocumentType(type) {
    let types = {};
    for (let documentType in documentAdditionalTypes) {
        types[documentType] = documentAdditionalTypes[documentType]
    }
    return types[type] != null ? types[type] : type;
}

export function userPositionType(type) {
    let types = {};
    for (let positionType of employeePositionType) {
        types[positionType.id] = positionType.name
    }
    return types[type] != null ? types[type] : type;
}

export function gender(gender) {
    let gendersList = {};
    for (let gender of genders) {
        gendersList[gender.id] = gender.name
    }
    return gendersList[gender] != null ? gendersList[gender] : gender;
}

export function education(education_id) {
    let edicationList = {};
    for (let education of educations) {
        edicationList[education.id] = education.name
    }
    return edicationList[education_id] != null ? edicationList[education_id] : education_id;
}

export function humanStatus(status) {
    let statuses = {};

    for (let humanStatus of humanStatuses) {
        statuses[humanStatus.key] = humanStatus.value;
    }

    return statuses[status] != null ? statuses[status] : status;
}

export function privilegedCategory(category) {
    let categoriesList = {};
    for (let item of privilegedCategories) {
        categoriesList[item.id] = item.name;
    }

    return categoriesList[category] != null ? categoriesList[category] : category;
}

export function empty(value) {
    return (_.isNull(value) || _.isUndefined(value) || _.isEmpty(value)) ? '' : value;
}

export function documentType(type) {
    let typesList = {};
    for (let documentType of documentsTypes) {
        typesList[documentType.id] = documentType.name;
    }

    return typesList[type] != null ? typesList[type] : type;
}

export function reason(reasonKey) {
    let flatReasonsArray = _.flatMap(reasons, function (arrItem) {
        return arrItem;
    });

    let reasonsList = {};

    for (let reason of flatReasonsArray) {
        reasonsList[reason.key] = reason.name;
    }

    return reasonsList[reasonKey] != null ? reasonsList[reasonKey] : reasonKey;
}

export function decreeType(type) {
    let decreeTypesList = {};
    for (let decreeType of decreeTypes) {
        decreeTypesList[decreeType.key] = decreeType.name;
    }

    return decreeTypesList[type] != null ? decreeTypesList[type] : type;
}

export function referensePeriod(periodKey) {
    let periodsList = {};
    for (let referensePeriod of referensePeriods) {
        periodsList[referensePeriod.key] = referensePeriod.name;
    }

    return periodsList[periodKey] != null ? periodsList[periodKey] : periodKey;
}

export function referenseParam(param) {
    let paramsList = {};
    for (let referensesParam of referensesParams) {
        paramsList[referensesParam.key] = referensesParam.name;
    }

    return paramsList[param] != null ? paramsList[param] : param;
}

export function referenseStatus(status) {
    let statusList = {
        'issued': 'Выдана',
        'booked': 'Заказана с сайта'
    };

    return statusList[status] != null ? statusList[status] : status;
}

export function referenseType(type) {
    let typeList = {
        'military': 'Справка в военкомат',
        'money': 'Справка о стипендии',
        'training': 'Справка об обучении'
    };

    return typeList[type] != null ? typeList[type] : type;
}

export function addressType(type) {
    let addressTypes = {
        'registration': 'Адрес регистрации',
        'residential': 'Адрес проживания',
        'place_of_stay': 'Адрес регистрации по месту пребывания',
    };

    return addressTypes[type] != null ? addressTypes[type] : type;
}

export function relationShipType(type) {
    let relationsList = {};

    for (let relationShipType of relationShipTypes) {
        relationsList[relationShipType.key] = relationShipType.name;
    }

    return relationsList[type] != null ? relationsList[type] : type;
}

export function decreesType(type) {
    let types = {
        'enrollment': 'Зачисление',
        'allocation': 'Отчисление',
        'transfer': 'Перевод в другую группу',
        'internal_transfer': 'Внутренний перевод',
        'reinstate': 'Восстановление',
        'internal_transfer_next_year': 'Перевод на следующий учебный год',
        'internal_transfer_next_term': 'Перевод на следующий период обучения',
        'graduation': 'Завершение обучения'

    };

    return types[type] != null ? types[type] : type;
}

export function newsType(type) {
    let types = {};

    for (let newsType of constants.newsTypes) {
        types[newsType.id] = newsType.text;
    }

    return types[type] != null ? types[type] : type;
}

export function language(language) {
    if (_.isNull(language)) {
        return 'Не указано';
    }
    let languages = {};

    for (let languageItem of constants.languages) {
        languages[languageItem.key] = languageItem.name;
    }

    return languages[language] != null ? languages[language] : type;
}

export function militaryDocumentType(type) {
    if (_.isNull(type)) {
        return 'Не указано';
    }
    let types = {};

    for (let documentType of constants.militaryDocumentTypes) {
        types[documentType.key] = documentType.text;
    }

    return types[type] != null ? types[type] : type;
}

//fitnessForMilitaryServices

export function fitnessForMilitary(type) {
    if (_.isNull(type)) {
        return 'Не указано';
    }
    let types = {};

    for (let fit of constants.fitnessForMilitaryServices) {
        types[fit.key] = fit.text;
    }

    return types[type] != null ? types[type] : type;
}

export function userPermission(permission) {
    let permissions = {};

    for (let group in constants.permissionGroups) {
        permissions = {...permissions, ...constants.permissionGroups[group].permissions}
    }

    let ref = permissions[permission];

    return ref != null ? ref : permission;
}

export function healthCategory(category) {
    if (!category) {
        return 'Не указано';
    }

    let categoriesObject = {};

    constants.healthCategories.forEach(item => {
        categoriesObject[item.key] = item.name;
    });

    let ref = categoriesObject[category];

    return ref != null ? ref : category;
}

export function militaryStatus(status) {
    if (!status) {
        return 'Не указано';
    }

    let statusesObject = {};

    constants.militaryStatuses.forEach(item => {
        statusesObject[item.key] = item.name;
    });

    let ref = statusesObject[status];

    return ref != null ? ref : status;
}

export function nationality(nationality) {
    if (!nationality) {
        return 'Не указано';
    }

    let nationalityObject = {};

    constants.countries.forEach(item => {
        nationalityObject[item.key] = item.name;
    });

    let ref = nationalityObject[nationality];

    return ref != null ? ref : nationality;
}

export function disability(disability) {

    if (!disability) {
        return 'Нет';
    }


    let disabilitiesObject = {};

    constants.disabilities.forEach(item => {
        disabilitiesObject[item.id] = item.name;
    });

    let ref = disabilitiesObject[disability];

    return ref != null ? ref : disability;
}

export function activityEvent(event) {
    let events = constants.activityEvents;

    let ref = events[event];

    return ref != null ? ref : event;
}

export function personShortName(fullname) {
    let splitName = fullname.split(' ', 3);
    if (splitName.length > 1) {
        return `${splitName[0]} ${splitName[1][0]}.${splitName[2][0]}.`;
    }
    return fullname;
}

export function programSubjectSection(section) {
    let subjectSections = [];

    constants.programSubjectSections.forEach((item, key) => {
        subjectSections[item.id] = item.name;
    });

    let ref = subjectSections[section];

    return ref != null ? ref : section;
}

export function trainingPeriodType(period) {
    let periods = {};
    periods[constants.trainingPeriods.semester] = 'Семестр';
    periods[constants.trainingPeriods.course] = 'Курс';
    periods[constants.trainingPeriods.trimester] = 'Триместр';
    let ref = periods[period];
    return ref != null ? ref : period;
}

export function yesAndNo(val) {
    let data = {};
    data[constants.yesAndNo[1]] = 'Да';
    data[constants.yesAndNo[0]] = 'Нет';
    let ref = data[val];
    return ref != null ? ref : val;
}

export function educationYear(year) {
    return `${year} - ${year + 1}`;
}

export function transferTitle(transfer) {
    switch (transfer.target) {
        case constants.transferTargets.academicTerm:
            return `Перевод на ${transfer.termNumber} ${termType(transfer.termType)} текущего курса`;
        case constants.transferTargets.academicYear:
            return `Перевод на ${educationYear(transfer.year)} учебный год (${pluralTermType(transfer.termType)})`;
        case constants.transferTargets.graduation:
            return "Выпускные группы"
    }
}

export function termType(term) {
    let terms = {};

    terms[constants.termTypes.semester] = 'семестр';
    terms[constants.termTypes.trimester] = 'триместр';
    terms[constants.termTypes.course] = 'курс';

    let ref = terms[term];

    return ref !== null ? ref : term;
}

export function pluralTermType(term) {
    let terms = {};

    terms[constants.termTypes.semester] = 'семестры';
    terms[constants.termTypes.trimester] = 'триместры';
    terms[constants.termTypes.course] = 'курс';

    let ref = terms[term];

    return ref !== null ? ref : term;
}

export function enumirate(constant, filter) {
    let items = [];
    _.map(constant, (index, item) => {
        let data = {
            value: index,
            text: eval(filter + `('${index}')`)
        };
        items.push(data);
    });
    return _.sortBy(items, 'text');
}

export function howFindOutSourse(source) {
    let sources = {};

    sources[constants.howFindOutSourses.openDoorsDay] = 'День открытых дверей';
    sources[constants.howFindOutSourses.internet] = 'Сайт/интернет';
    sources[constants.howFindOutSourses.fromRelative] = 'От родственника';
    sources[constants.howFindOutSourses.fromEmployee] = 'От сотрудника';
    sources[constants.howFindOutSourses.fromStudent] = 'От студента';
    sources[constants.howFindOutSourses.other] = 'Иное';

    let ref = sources[source];

    return ref != null ? ref : source;
}

export function militaryRank(rank) {
    let ranks = {};
    ranks[constants.militaryRanks.conscript] = "Подлежит призыву";
    ranks[constants.militaryRanks.commonSoldier] = "Рядовой (матрос)";
    ranks[constants.militaryRanks.lanceCorporal] = "Ефрейтор (старший матрос)";
    ranks[constants.militaryRanks.lanceSergeant] = "Младший сержант (старшина 2-й статьи)";
    ranks[constants.militaryRanks.sergeant] = "Сержант (старшина 1-й статьи)";
    ranks[constants.militaryRanks.staffSergeant] = "Старший сержант (главный старшина)";
    ranks[constants.militaryRanks.pettyOfficer] = "Старшина (главный корабельный старшина)";
    ranks[constants.militaryRanks.ensign] = "Прапорщик (мичман)";
    ranks[constants.militaryRanks.seniorEnsign] = "Старший прапорщик (старший мичман)";
    ranks[constants.militaryRanks.sublieutenant] = "Младший лейтенант";
    ranks[constants.militaryRanks.lieutenant] = "Лейтенант";
    ranks[constants.militaryRanks.seniorLieutenant] = "Старший лейтенант";
    ranks[constants.militaryRanks.major] = "Майор (капитан 3-го ранга)";
    ranks[constants.militaryRanks.lieutenantColonel] = "Подполковник (капитан 2-го ранга)";
    ranks[constants.militaryRanks.colonel] = "Полковник (капитан 1-го ранга)";
    ranks[constants.militaryRanks.majorGeneral] = "Генерал-майор (контр-адмирал)";
    ranks[constants.militaryRanks.lieutenantGeneral] = "Генерал-лейтенант (вице-адмирал)";
    ranks[constants.militaryRanks.colonelGeneral] = "Генерал-полковник (адмирал)";
    ranks[constants.militaryRanks.generalOfTheArmy] = "Генерал армии (адмирал флота)";
    ranks[constants.militaryRanks.marshal] = "Маршал Российской Федерации (Адмирал Флота РФ)";
    ranks[constants.militaryRanks.captain] = "Капитан (капитан-лейтенант)";
    let ref = ranks[rank];
    return ref != null ? ref : rank;
}

export function militaryComposition(composition) {
    let compositions = {};
    compositions[constants.militaryCompositions.soldiersAndSailors] = "Солдаты, матросы, сержанты, старшины";
    compositions[constants.militaryCompositions.ensignAndWarrantOfficers] = "Прапорщики и мичманы";
    compositions[constants.militaryCompositions.officersAndMarshals] = "Офицеры и маршалы";
    let ref = compositions[composition];
    return ref != null ? ref : composition;
}

export function groupOfAccounting(group) {
    let groups = {};
    groups[constants.groupOfAccounting.army] = "РА";
    groups[constants.groupOfAccounting.fleet] = "ВМФ";
    let ref = groups[group];
    return ref != null ? ref : group;
}
export function reserveCategory(category) {
    let categories = {};
    categories[constants.reserveCategories.firstCategory] = "Первая категория";
    categories[constants.reserveCategories.secondCategory] = "Вторая категория";
    let ref = categories[category];
    return ref != null ? ref : category;
}

import ruLocale from '../lang/ru';

export function activityProp(prop, log_name) {
    let props = {
        'subject': {type_id: 'Тип дисциплины', name: ruLocale.label.name},
        'student': {
            ...ruLocale.label,
            ...{
                id: 'ИД',
                profession_program_id: 'ID проф. программы',
                group_id: 'ID группы',
                passport_issuance_date: 'Дата выдачи паспорта',
                passport_data: 'Данные паспорта',
                addresses: 'Адреса',
                education_id: 'Образование',
                has_original: 'Оригинал док. об образовании',
                need_hostel: 'Нуждается в общежитии',
                has_certificate: 'Наличие мед.справки',
                graduation_organization_name: 'Предыдущая обр. организация',
                graduation_organization_place: 'Местоположение обр. организации',
                ratings_map: 'Карта оценок ср. балла',
                'threes': 'Тройки',
                'fours': 'Четверки',
                'fives': 'Пятерки',
                privileged_category: 'Льготная категория',
                person_with_disabilities: 'Лицо с ОВЗ',
                long_absent: 'Длительно отсутствующий',
                start_date: 'Дата подачи документов',
                contract_target_set: 'Договор целевого набора',
                recruitment_program_id: 'ID программы набора',
                selection_committee_id: 'ID Приемной комиссии',
                nominal_number: 'Поименный номер'
            }
        },
        how_find_out: {
            openDoorsDay: 'День открытых дверей',
            internet: 'Сайт/интернет',
            fromRelative: 'От родственника',
            fromEmployee: 'От сотрудника',
            fromStudent: 'От студента',
            other: 'Иное',
        },
        'recruitment_program': {
            count_with_out_documents: 'Кол-во без оригиналов',
            count_with_documents: 'Кол-во с оригиналами',
            number_of_enrolled: 'Кол-во зачисленных',
            reception_plan: 'План приема',
        },
        'component_document': {...{}, ...ruLocale.label},
        'program_document': {
            ...{
                id: 'ИД',
                profession_program_id: 'ID проф. программы',
                title: 'Название',
                start_date: 'Дата вступления',
                created_at: 'Дата создания',
            }, ...ruLocale.label
        },
        'program_subject': {
            ...{},
            ...ruLocale.label
        },
        entrant: {}
    };
    props.entrant = props.student;
    if (props.hasOwnProperty(log_name)) {
        let log_props = props[log_name];
        if (log_props.hasOwnProperty(prop)) {
            let ref = log_props[prop];
            return ref !== null ? ref : prop;
        }
    }
    return prop;
}

export function extraOption(option) {
    let options = {
        dual_training: 'ДО',
        adapted_program: 'АОП'
    };

    let ref = options[option];

    return ref != null ? ref : option;
}

import ruLocare from '@/lang/ru';

export function tableColumn(column) {
    let labels = ruLocale.label;

    let ref = labels[column];

    return ref !== undefined ? ref : column;
}

export function activityPropValue(prop_value, log_name, prop_name) {
    if (prop_value === '-') {
        return prop_value;
    }
    let props = {
        'subject': {
            type_id: _.zipObject(constants.subjectTypes.map(type => type.id), constants.subjectTypes.map(type => type.name))
        },
        'student': {
            fitness_for_military_service: _.zipObject(constants.fitnessForMilitaryServices.map(category => category.key), constants.fitnessForMilitaryServices.map(category => category.text)),
            military_document_type: _.zipObject(constants.militaryDocumentTypes.map(document => document.key), constants.militaryDocumentTypes.map(document => document.text)),
            education_id: _.zipObject(constants.educations.map(education => education.id), constants.educations.map(education => education.name)),
            language: _.zipObject(constants.languages.map(language => language.key), constants.languages.map(language => language.name)),
            status: _.zipObject(constants.humanStatuses.map(status => status.key), constants.humanStatuses.map(status => status.value)),
            gender: _.zipObject(constants.genders.map(gender => gender.id), constants.genders.map(gender => gender.name)),
            privileged_category: _.zipObject(constants.privilegedCategories.map(category => category.id), constants.privilegedCategories.map(category => category.name)),
            nationality: _.zipObject(constants.countries.map(country => country.key), constants.countries.map(country => country.name)),
            how_find_out: _.zipObject(_.map(constants.howFindOutSourses, item => item), _.map(constants.howFindOutSourses, item => howFindOutSourse(item)))
        },
        entrant: {}
    };
    props.entrant = props.student;
    if (props.hasOwnProperty(log_name)) {
        let log_props = props[log_name];
        if (log_props.hasOwnProperty(prop_name)) {
            let section = log_props[prop_name];
            let ref = section[prop_value];
            return ref !== null ? ref : prop_value;
        }
    }
    return prop_value;
}

export function activityEntity(log_name) {
    let entities = constants.activityEntities;
    let ref = entities[log_name];
    return ref != null ? ref : log_name;
}
