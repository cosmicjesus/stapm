const onlyLetter = {
    bind(el) {
        console.log('only-letter bind');
    },
    update(el) {
        console.log('field update');
    }
};
export default onlyLetter;