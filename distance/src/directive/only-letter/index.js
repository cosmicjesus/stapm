import onlyLetter from 'onlyLetter';

const install = function(Vue) {
    Vue.directive('only-letter', onlyLetter)
}

if (window.Vue) {
    window['only-letter'] = onlyLetter
    //Vue.use(install); // eslint-disable-line
    Vue.directive('only-letter', onlyLetter)
}

// onlyLetter.install = install
// export default onlyLetter