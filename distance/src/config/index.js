import {Loading} from 'element-ui';

let datepickerConfig = {
    firstDayOfWeek: 1,
    shortcuts: [{
        text: 'Сегодня',
        onClick(picker) {
            picker.$emit('pick', new Date());
        }
    }],
};

export function pickerConfig(config = {}) {
    return {...datepickerConfig, ...config};
}

let lockscreenConfig = {
    fullscreen: true,
    lock: true,
    text: 'Идет загрузка, пожалуйста, подождите',
    spinner: 'el-icon-loading',
    background: 'rgba(240, 240, 240, 0.8)'
};

export function lockScreen(config = {}) {
    if (config.text) {
        config.text = `${config.text}, пожалуйста, подождите`
    }
    return Loading.service({...lockscreenConfig, ...config});
}

let swalConfigs = {
    error: {
        title: 'Внимание! Ошибка!',
        type: 'error',
        html: 'Произошла ошибка',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ок'
    }
};

export function swalModal(configType, configParams = {}) {
    let config = {...swalConfigs[configType], ...configParams};
    return this.$swal(config);
}