import request from '../utils/request';

export function fetchAll() {
    return request({
        url: route('api.regulations.list').url(),
        method: 'get'
    });
}
