import request from '../utils/request';

const url = `/news`;

export function getNews(count) {
    return request({
        url: `${url}/last/${count}`,
        method: 'get'
    })
}

export function getAllNews(params) {
    return request({
        url: route('api.news.list').url(),
        method: 'get',
        params
    })
}

export function getAnnoncements() {
    return request({
        url: route('api.annoncements').url(),
        method: 'get'
    })
}

export function getWarNews() {
    return request({
        url: route('api.may9').url(),
        method: 'get'
    })
}
