import request from '../utils/request';

const url = 'employees';

export function administration() {
    return request({
        url: `${url}/administration`,
        method: 'get'
    })
}

export function pedagogical(params) {
    return request({
        url: `${url}/pedagogical`,
        method: 'get',
        params
    })
}