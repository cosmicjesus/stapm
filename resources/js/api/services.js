import request from '../utils/request';

export function trainingGroups() {
    return request({
        url: route('api.services.training-groups').url(),
        method: 'get'
    });
}

export function makeReference(data) {
    return request({
        url: route('client.order-reference.order').url(),
        method: 'post',
        data
    });
}

export function sendMail(data) {
    return request({
        url: route('client.feedback.send').url(),
        method: 'post',
        data
    });
}

export function replaceClass(date = null) {
    return request({
        url: `/replace-classes/${date}`,
        method: 'get'
    });
}

export function getActivePrograms() {
    return request({
        url: route('client.programs.on-index').url(),
        method: 'get'
    });
}


export function getActiveRecruitmentPrograms() {
    return request({
        url: route('client.active-programs').url(),
        method: 'get'
    });
}

export function makeEnrollee(data) {
    return request({
        url: route('client.api.create-statement').url(),
        method: 'post',
        data
    });
}

export function uploadFiles(id,files) {
    return request({
        url: route('client.api.entrant.upload-files',{id}).url(),
        method: 'post',
        files
    });
}

export function checkEntrant(data) {
    return request({
        url: route('client.api.entrant.check').url(),
        method: 'post',
        data
    });
}

export function getEntrantId(data) {
    return request({
        url: route('client.api.entrant.check-entrant').url(),
        method: 'post',
        data
    });
}
