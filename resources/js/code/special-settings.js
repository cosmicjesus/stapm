import Cookies from 'js-cookie';

$(function () {
    /*Специальный настройки*/
    'use strict';

    const minFontSize = 14;
    const maxFontSize = 24;

    $(document)
        .on('click', '.special-settings button', function (event) {
            event.preventDefault();
            setSpecialVersion($(this).data());
        })
        .on('click', '[data-spec-off]', function (event) {
            event.preventDefault();
            unsetSpecialVersion();
        })
        .on('click', '[data-reset-settings]', function (event) {
            event.preventDefault();
            setDefaultsSpecialVersion();
        })
        .on('click', 'a.decrement-font-size', function (event) {
            event.preventDefault();
            let bodyFontsize = getBodyFontSize();
            if (bodyFontsize > minFontSize) {
                let decrement = --bodyFontsize;
                setSpecialVersion({'fontsize': decrement});
            }

        })
        .on('click', 'a.increment-font-size', function (event) {
            event.preventDefault();
            let bodyFontsize = getBodyFontSize();
            if (bodyFontsize < maxFontSize) {
                let incementSize = ++bodyFontsize;
                setSpecialVersion({'fontsize': incementSize});
            }

        })
        .on('click', '.special-vision-js', function (event) {
            event.preventDefault();
            setDefaultsSpecialVersion();
        })
        .on('click', '.special-panel-control', function (event) {
            let $body = $('body');
            let isShow = $body.hasClass('special-panel-show');
            let $specialPanel = $('.special-settings');
            if (isShow) {
                setSpecialVersion({'panel': 'hide'});
                let panelHeight = $specialPanel.height();
                $specialPanel.css({
                    'margin-top': `-${panelHeight}px`
                });
            } else {
                setSpecialVersion({'panel': 'show'});
                $specialPanel.css({
                    'margin-top': `0px`
                });
            }
        });

    jQuery(document).ready(function ($) {
        let body = document.querySelector('body');
        //console.log(body.classList.contains('special-panel-show'));
        let $specialPanel = $('.special-settings');
        // if (isShow) {
        //     let panelHeight = $specialPanel.height();
        //     $specialPanel.css({
        //         'margin-top': `-${panelHeight}px`
        //     });
        // }
        setSpecialVersion();
    });

    /**
     * Установка параметров отображения спецверсии
     * @param {object} data Объект с данными для формирования параметров
     */
    function setSpecialVersion(data) {
        let
            // получаем текущее значение переменной $.cookie.json;
            cookieJson = Cookies.getJSON('special'),
            $body = $('body'),
            // Получаем текущее значение атрибута class и html.
            htmlCurrentClass = $body.prop('class'),
            // Удаляем старые классы у html, оставляем только чужие классы, это важно,
            // т.к. классы добавляем не только мы.
            clearSpecialClasses = htmlCurrentClass.replace(/special-([0-9,a-z,A-Z,-]+)/g, ''),
            // Признак включенной спецверсии, он же специальный класс, который добавится к html
            $aaVersion = {'version': 'on'},
            // Переменная под новые классы.
            htmlClass = '';

        // Работаем с куками в json-формате
        // Если переданы данные
        if (data) {
            // Объединяем существующие куки с новыми данными из ссылки.
            var $newCookies = {...Cookies.getJSON('special'), ...data, ...$aaVersion};
            console.dir(cookieJson, $newCookies);
            // Записываем новую куку
            Cookies.set('special', $newCookies, {
                expires: 1,
                path: '/',
                secure: false
            });
        }

        // Удаляем ненужные классы a-current.
        $('.setting__buttons > button.active').removeClass('active');

        // Если есть кука — работаем.
        if (Cookies.get('special')) {
            // Пробегаем по массыву из нашей куки
            let cookies = Cookies.getJSON('special');
            for (let key in cookies) {
                htmlClass += ' special-' + key + '-' + cookies[key];
                // Добавляем нужные классы a-current.
                let $item = $('.setting-' + key + '_' + cookies[key]);
                $item.addClass('active');
                let title = $item.prop('title');
                $item.parents('.setting__item').find('.setting__status').html(title);
            }
            if (cookies['fontsize']) {
                setFontSize(cookies['fontsize'])
            }
            $body
            // Заменяем текущий атрибут на очищенный от лишних классов.
                .prop('class', clearSpecialClasses)
                // Добавляем вновь сформированные классы.
                .addClass(htmlClass);

            // Возвращаем формат куков как было до нас.
        }
        let body = document.querySelector('body');
        let isHide = body.classList.contains('special-panel-hide');
        let specialPanel = document.querySelector('.special-settings');
        if (isHide) {
            let panelHeight = specialPanel.clientHeight;
            let parentHeight = specialPanel.parentNode.clientHeight;
            //console.log(specialPanel, panelHeight);
            specialPanel.style['margin-top'] = `-${panelHeight + parentHeight}px`;
        }
        return false;
    }

    /**
     * Отключение специальной версии сайта.
     */
    function unsetSpecialVersion() {
        let
            // Получаем значение класса тега body.
            $body = $('body'),
            htmlCurrentClass = $body.prop('class'),
            // Очищаем от классов спецверсии
            clearSpecialClasses = htmlCurrentClass.replace(/special-([0-9,a-z,A-Z,-]+)/g, '');
        // Заменяем текущий атрибут на очищенный от лишних классов.
        $body.prop('class', clearSpecialClasses);
        // Удаляем куки
        setFontSize(16);
        Cookies.remove('special', {path: '/'});
    }

    /**
     * Установка дефолтных значений для спецверсии.
     * @param {object} params Объект с данными для формирования параметров.
     */
    function setDefaultsSpecialVersion(params) {
        // Задаём значения по умолчанию
        let $specialDefaults = {
            'settings': 'on',
            'panel': 'show',
            'color': 'black-on-white',
            'fontsize': '16',
            'font': 'times',
            'kerning': 'normal',
            'interval': 'sesquialteral',
            'image': 'on'
        };
        // Объединяем значения по умолчанию с переданными данными.
        let $setDefaulParams = {...$specialDefaults, ...params};

        // Вызываем setSpecialVersion, где и происходит весь процесс.
        setSpecialVersion($setDefaulParams);
    }

    function setFontSize(fontsize) {
        let $html = $('body');

        $html.css('font-size', `${fontsize}px`);
        $('.setting__font-status').text(fontsize);
    }

    function getBodyFontSize() {
        let $html = $('body');

        return parseInt($html.css('font-size'));
    }
});