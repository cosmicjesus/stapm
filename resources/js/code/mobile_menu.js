
let items = document.querySelectorAll('.mobile-menu__item');

// items.forEach(function (n) {
//     n.onclick = function () {
//         this.classList.toggle('open')
//     };
// });

let menu = document.querySelector("#mobile-menu");
let body = document.querySelector('body');
let $menu = $('#mobile-menu');

let openMenu = function () {
    let windowWidth = document.body.clientWidth;
    body.classList.add('no-scroll');
    body.style.overflow = 'hidden';
    body.style.width = `${windowWidth}px`;
    $menu.fadeIn(400).addClass('active');
};

let closeMenu = function () {
    body.style.overflow = 'visible';
    body.style.width = 'auto';
    body.classList.remove('no-scroll');
    $menu.removeClass('active').fadeOut(400);
};
$('.mobile-btn').on('click', function (event) {
    let isOpen = menu.classList.contains('active');
    if (isOpen) {
        closeMenu();
    } else {
        openMenu();
    }
});
// let menuBtn = document.getElementsByClassName('mobile-btn');
//
// menuBtn[0].addEventListener('click', function (e) {
//     let menu = document.querySelector("#mobile-menu");
//     let isOpen = menu.classList.contains('active');
//     console.log(isOpen);
//     // if (isOpen) {
//     //     vm.closeMenu(menu);
//     // } else {
//     //     vm.openMenu(menu);
//     // }
// })
