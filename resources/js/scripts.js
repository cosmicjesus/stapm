require("@fancyapps/fancybox");

require('./code/mobile_menu');
require('./code/special-settings');

$('[data-fancybox]').fancybox({
    youtube: {
        controls: 0,
        showinfo: 0
    },
    vimeo: {
        color: 'f00'
    }
});
let items = document.querySelectorAll('.program-subject-item');

items.forEach(function (n) {
    n.onclick = function () {
        this.classList.toggle('open')
    };
});