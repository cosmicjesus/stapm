import moment from 'moment';

export function pluralize(number, endingArray, withNumber = true) {
    let defNumber = number;

    number = number % 100;

    let ending = undefined;
    if (number >= 11 && number <= 19) {
        ending = endingArray[2];
    } else {
        let i = number % 10;
        switch (i) {
            case (1):
                ending = endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                ending = endingArray[1];
                break;
            default:
                ending = endingArray[2];
        }
    }

    if (withNumber) {
        return defNumber + " " + ending;
    } else {
        return ending;
    }
}

// function pluralize(time, label) {
//   if (time === 1) {
//     return time + label
//   }
//   return time + label + 's'
// }

// export function timeAgo(time) {
//   const between = Date.now() / 1000 - Number(time)
//   if (between < 3600) {
//     return pluralize(~~(between / 60), ' minute')
//   } else if (between < 86400) {
//     return pluralize(~~(between / 3600), ' hour')
//   } else {
//     return pluralize(~~(between / 86400), ' day')
//   }
// }

/* 数字 格式化*/
export function numberFormatter(num, digits) {
    const si = [
        {value: 1E18, symbol: 'E'},
        {value: 1E15, symbol: 'P'},
        {value: 1E12, symbol: 'T'},
        {value: 1E9, symbol: 'G'},
        {value: 1E6, symbol: 'M'},
        {value: 1E3, symbol: 'k'}
    ]
    for (let i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value + 0.1).toFixed(digits).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, '$1') + si[i].symbol
        }
    }
    return num.toString()
}

export function toThousandFilter(num) {
    return (+num || 0).toString().replace(/^-?\d+/g, m => m.replace(/(?=(?!\b)(\d{3})+$)/g, ','))
}

export function dateFormat(date, format = 'DD.MM.Y') {
    if (_.isNull(date) || _.isUndefined(date)) {
        return null;
    }
    return moment(date).format(format)
}

export function regulationStatus(status) {
    let statuses = {
        success: 'Нарушение устранено',
        inWork: 'В работе',
        fail: 'Нарушение не устранено'
    }

    return statuses[status] != null ? statuses[status] : status;
}

export function qualificationCategory(categoryKey) {
    const categories = [
        {id: 1, name: 'Без категории'},
        {id: 2, name: 'Экзамен на соответствие'},
        {id: 3, name: 'Первая категория'},
        {id: 4, name: 'Высшая категория'}
    ];

    let categoriesList = {};

    for (let category of categories) {
        categoriesList[category.id] = category.name
    }

    return categoriesList[categoryKey] != null ? categoriesList[categoryKey] : categoryKey;
}


export function educationLevel(education_id) {
    const educations = [
        {id: 1, "name": "Без основного общего образования"},
        {id: 2, "name": "Основное общее образование"},
        {id: 3, "name": "Среднее общее образование"},
        {id: 4, "name": "Неполное среднее профессиональное образование"},
        {id: 5, "name": "СПО по программам подготовки квалифицированных рабочих (служащих)"},
        {id: 6, "name": "СПО по программам подготовки специалистов среднего звена"},
        {id: 7, "name": "Высшее образование - бакалавриат"},
        {id: 8, "name": "Высшее образование - незаконченное высшее образование"},
        {id: 9, "name": "Высшее образование - подготовка кадров высшей квалификации"},
        {id: 10, "name": "Высшее образование - специалитет, магистратура"},
        {id: 11, "name": "Профессиональная переподготовка"}
    ];

    let edicationList = {};
    for (let education of educations) {
        edicationList[education.id] = education.name
    }
    return edicationList[education_id] != null ? edicationList[education_id] : education_id;

}

export function positionType(type) {
    const employeePositionType = [
        {id: 0, name: 'Штатный'},
        {id: 1, name: 'Внутренний совместитель'},
        {id: 2, name: 'Внешний совместитель'}
    ];

    let types = {};
    for (let positionType of employeePositionType) {
        types[positionType.id] = positionType.name
    }
    return types[type] != null ? types[type] : type;
}

// export function calculatePeriod(start_period, end_period = null, blocks = ['months', 'days', 'years']) {
//     let start = DateTime.fromISO(start_period);
//     let end = _.isNull(end_period) ? DateTime.local() : DateTime.fromISO(end_period);
//
//     return end.diff(start, blocks).toObject();
// }

