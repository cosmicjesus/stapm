import $ from 'jquery'
import lodahs from 'lodash';
import Vue from 'vue';
import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-chalk/index.css'
import VueScrollTo from 'vue-scrollto'
import VueScriptComponent from 'vue-script-component'


Vue.use(Element, {
    size: 'medium', // set element-ui default size
    locale
});

import NewsIndex from './components/news-index';
import NewsList from './components/news-list';
import NewsDetail from './components/news-detail';
import Annoncements from './components/annoncements';
import Regulations from './components/regulations';
import Employees from './components/employees';
import Administration from './components/administration';
import EmployeeProfile from './components/employee-profile';
import EmployeeCourses from './components/employee-courses';
import Pedagogical from './components/pedagogical';
import RegulationItem from './components/regulation-item';
import StudentReference from './components/student-reference';
import IndexActivePrograms from './components/index-active-programs';
import FeedbackForm from './components/feedback-form';
import ReplaceClass from './components/replace-class';
import SubmissionForm from './components/submission-form';
import UploadingDocuments from './components/uploading-documents';
import TimePeriod from './components/TimePeriod/index'
import Sticky from "../../admin/src/components/Sticky/index";
import * as filters from "./filters";
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import * as config from "../../admin/src/config";
Vue.use(VueFormWizard)

import VueTheMask from 'vue-the-mask'
import {Loading} from "element-ui";

Vue.use(VueTheMask)

Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
});


let datepickerConfig = {
    firstDayOfWeek: 1,
    shortcuts: [{
        text: 'Сегодня',
        onClick(picker) {
            picker.$emit('pick', new Date());
        }
    }],
};

export function pickerConfig(config = {}) {
    return {...datepickerConfig, ...config};
}

Object.defineProperty(Vue.prototype, `$pickerOptions`, {value: pickerConfig});


let lockscreenConfig = {
    fullscreen: true,
    lock: true,
    text: 'Идет загрузка, пожалуйста, подождите',
    spinner: 'el-icon-loading',
    background: 'rgba(240, 240, 240, 0.8)'
};

export function lockScreen(config = {}) {
    if (config.text) {
        config.text = `${config.text}, пожалуйста, подождите`
    }
    return Loading.service({...lockscreenConfig, ...config});
}

Object.defineProperty(Vue.prototype, `$lockScreen`, {value: lockScreen});

new Vue({
    el: '#app',
    components: {
        SubmissionForm,
        NewsIndex,
        NewsList,
        NewsDetail,
        Sticky,
        VueScriptComponent,
        Regulations,
        RegulationItem,
        Employees,
        Pedagogical,
        Administration,
        EmployeeProfile,
        TimePeriod,
        EmployeeCourses,
        StudentReference,
        Annoncements,
        FeedbackForm,
        ReplaceClass,
        IndexActivePrograms,
        UploadingDocuments
    },
    mounted: function () {
    },
    methods: {}
});
