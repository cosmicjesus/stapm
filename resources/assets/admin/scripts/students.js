$(function () {
    let body = $('body');
    let studentsTable = $('#students-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.students.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.group = $('select[name=group]').val();
                d.status = $('select[name=status]').val();
                d.facility = $('select[name=facility]').val();
                d.sortByGroup = $('input[name=sort_by_group]').prop('checked');
                d.onlyLongAbsent = $('input[name=only_long_absent]').prop('checked');
            }
        },
        columnDefs: [
            {
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }
        ],
        select: {
            'style': 'multi'
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', sortable: false, sorting: false},
            {data: 'id', name: 'id', sortable: false},
            {data: 'full_name', name: 'full_name'},
            {data: 'birthday', name: 'birthday'},
            {data: 'group', name: 'group'},
            {data: 'status', name: 'status'},
            {data: 'privilegy', name: 'privilegy'},
            {data: 'phone', name: 'phone'},
            {data: 'actions', name: 'actions', sortable: false}
        ]
    });

    $('#filter-students-table-js').on('submit', function (e) {
        studentsTable.draw();
        e.preventDefault();
    });

    $('.transfer-students-js').on('click', function (event) {
        event.preventDefault();
        let checked = studentsTable.column(0).checkboxes.selected();
        if (!checked.length) {
            return false;
        }
        let students = '';
        let form = $('form[name=transfer-form]');
        let ids = '';
        $.each(checked, function (key, value) {
            let row = JSON.parse(value);
            ids += `<input type="hidden" name="ids" value="${row.id}">`;
            students += `<tr data-id="${row.id}"><td>${row.full_name}</td></tr>`;
        })
        $('.students-list-js').html(students);
        form.find('.student-ids').html(ids);
    });

    $('#transfer-form').on('submit', function (event) {
        event.preventDefault();

        let form = $(this);
        let validation = validate(form);

        if (!validation) {
            return false;
        }

        let data = {};
        let ids = [];

        $.each(form[0], function (key, item) {
            if (item.name === 'ids') {
                ids.push(item.value);
            } else {
                data[item.name] = item.value;
            }
        });
        data['ids'] = ids;
        $.ajax({
            url: route('admin.students.transfer').url(),
            data: data,
            type: 'POST',
            success: function (data) {
                if (data.success) {
                    swal({
                        title: 'Успешно',
                        html: data.message,
                        type: 'success'
                    }).then(() => {
                        studentsTable.draw();
                        $('#transfer-modal').modal('hide');
                    })
                } else {
                    swal({
                        title: 'Ошибка',
                        html: data.message,
                        type: 'error'
                    })
                }
            },
            error: function (data) {
                swal({
                    title: 'Ошибка',
                    html: data,
                    type: 'error'
                })
            }
        });

    });

    $('.allocation-student-js').on('click', function (event) {
        event.preventDefault();
        let checked = studentsTable.column(0).checkboxes.selected();
        if (!checked.length) {
            return false;
        }
        let students = '';
        let form = $('form[name=allocation-form]');
        let ids = '';
        $.each(checked, function (key, value) {
            let row = JSON.parse(value);
            ids += `<input type="hidden" name="ids" value="${row.id}">`;
            students += `<tr data-id="${row.id}"><td>${row.full_name}</td></tr>`;
        });
        $('.students-list-js').html(students);
        form.find('.student-ids').html(ids);
    });

    $('#allocation-form').on('submit', function (event) {
        event.preventDefault();

        let form = $(this);
        let validation = validate(form);

        if (!validation) {
            return false;
        }

        let data = {};
        let ids = [];

        $.each(form[0], function (key, item) {
            if (item.name === 'ids') {
                ids.push(item.value);
            } else {
                data[item.name] = item.value;
            }
        });
        data['ids'] = ids;
        $.ajax({
            url: route('admin.students.allocation').url(),
            data: data,
            type: 'POST',
            success: function (data) {
                if (data.success) {
                    swal({
                        title: 'Успешно',
                        html: data.message,
                        type: 'success'
                    }).then(() => {
                        studentsTable.draw();
                        $('#allocation-modal').modal('hide');
                    })
                } else {
                    swal({
                        title: 'Ошибка',
                        html: data.message,
                        type: 'error'
                    })
                }
            },
            error: function (data) {
                swal({
                    title: 'Ошибка',
                    html: data,
                    type: 'error'
                })
            }
        });

    });

    let addStudentForm = $('#add-student-form');

    addStudentForm.validate();

    addStudentForm.on('submit', function (event) {
        let form = $(this);
        if (form.find('input.error').length > 0) {
            console.log('Не валидна');
            return false;
        }
        event.preventDefault();
        let url = _.first(form).action;
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: function (response) {
                swal({
                    title: 'Студент добавлен',
                    type: 'success',
                    input: 'checkbox',
                    inputValue: 0,
                    inputPlaceholder: 'Добавить еще одного студента',
                    confirmButtonText: 'Продолжить'
                }).then(({value}) => {
                    if (value) {
                        _.first(form).reset();
                    } else {
                        let url = route('admin.students');
                        document.location.href = url.url();
                    }
                });
            },
            error: (response) => {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }

        })
    });

    let studentEditForm = $('#student-edit-form');

    studentEditForm.validate();

});