$(function () {
    let groupsTable = $('#groups-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.groups.data').url(),
            data: function (d) {
                d.program_id = $('select[name=group-program]').val();
                d.course = $('select[name=course]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sortable: false},
            {data: 'step', name: 'step'},
            {data: 'name', name: 'name'},
            {data: 'program', name: 'program'},
            {data: 'plan', name: 'plan'},
            {data: 'max', name: 'max'},
            {data: 'countStudents', name: 'countStudents'},
            {data: 'curator', name: 'curator'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    $('#filter-groups-form-js').on('submit', function (e) {
        groupsTable.draw();
        e.preventDefault();
    });

    $('#curator_select').select2({
        width: '100%',
        placeholder: {id: null, text: 'Select an option'},
    });

    $('.group-program-js').on('change', function () {
        let id = parseInt($(this)[0].value);
        $('.edu_plan_options').addClass('hide');
        $(`.edu_plan_options[data-program="${id}"]`).removeClass('hide');
    });

    $('.group-name-pattern-js').on('keypress', function () {
        let value = $(this)[0].value;
        console.log($(this)[0].value);
        console.log(value.replace('{Курс}', 1));
        $('.group-name').html(value.replace('{Курс}', 1));
    });

    groupsTable.on('click', '.delete-group-js', function () {

        let url = $(this).data('url');
        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                text: data.message,
                                type: 'success'
                            }).then(() => {
                                groupsTable.draw();
                            });
                        } else {
                            swal({
                                title: 'Ошибка',
                                text: data.message,
                                type: 'error'
                            })
                        }
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    groupsTable.on('click', '.group-transfer-js', function () {

        let url = $(this).data('url');
        let unique = $(this).data('unique');
        swal({
            title: 'Вы уверены?',
            text: `Вы хотите перевести группу ${unique} на следующий период обучения?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Перевести',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: data.message,
                                type: 'success'
                            }).then(() => {
                                groupsTable.draw()
                            })
                        } else {
                            swal({
                                title: 'Ошибка',
                                html: data.message,
                                type: 'error'
                            })
                        }
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    $('#decree_date_picker').datepicker(datepicker_conf());

    groupsTable.on('click', '.group-release-js', function () {
        let elem = $(this);
        let id = elem.data('id');
        $('input#group_id').val(id);
    });

    $('form#releaseGroupForm').on('submit', function (event) {
        event.preventDefault();

        let form = $(this);
        let url = route('admin.groups.release').url();
        let data = form.serialize();
        $.ajax({
            url,
            data,
            type: 'POST',
            success: function (data) {
                if (data.success) {
                    swal({
                        title: 'Успешно',
                        html: `<b>${data.message}</b>`,
                        type: 'success'
                    }).then(() => {
                        $('#releaseGroupModal').modal('hide');
                        groupsTable.draw()
                    });

                } else {
                    swal({
                        title: 'Ошибка',
                        html: data.message,
                        type: 'error'
                    })
                }
            },
            error: function (data) {

                swal({
                    title: 'Ошибка',
                    html: data.responseJSON.message,
                    type: 'error'
                })
            }
        });
    })
});