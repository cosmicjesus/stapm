$(function () {
    let subjects_table = $('#subjects-table');

    let oTable = subjects_table.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.subjects.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.type_id = $('select[name=type_id]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sortable: false},
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    $('#search-form').on('submit', function (e) {
        oTable.draw();
        e.preventDefault();
    });

    let addSubjectForm = $('form#add-subject-form');

    addSubjectForm.validate();

    addSubjectForm.on('submit', function (event) {
        let form = $(this);
        if (form.find('input.error').length > 0) {
            return false;
        }
        event.preventDefault();
        let url = _.first(form).action;
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: function (response) {
                if (response.success) {
                    swal({
                        title: 'Успешно',
                        type: 'success',
                        html: `<b>${response.message}</b>`,
                        confirmButtonText: 'Добавить еще дисциплину',
                        cancelButtonText: 'Отмена',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        showCancelButton: true,
                        buttonsStyling: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33'
                    }).then(({value}) => {
                        if (value) {
                            _.first(form).reset();
                        } else {
                            let url = route('admin.subjects');
                            document.location.href = url.url();
                        }
                    });
                } else {
                    swal({
                        title: 'Внимание!',
                        html: `<b>${response.message}</b>`,
                        type: 'info'
                    })
                }

            },
            error: (response) => {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    })

    let updateSubjectForm = $('form#update-subject-form');

    updateSubjectForm.validate();

    subjects_table.on('click', '.delete-subject-js', function (event) {
        event.preventDefault();

        let url = $(this).data('url');
        let name = $(this).data('name');
        swal({
            title: 'Вы уверены?',
            text: `Вы хотите удалить дисциплину ${name}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: data.message,
                                type: 'success'
                            }).then(() => {
                                oTable.draw()
                            })
                        } else {
                            swal({
                                title: 'Ошибка',
                                html: `<b>${data.message}</b>`,
                                type: 'error'
                            })
                        }
                    },
                    error: function (response) {
                        swal({
                            title: 'Ошибка',
                            html: response.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });

    });
});