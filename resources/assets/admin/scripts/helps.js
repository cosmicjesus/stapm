let setStartPicker = function () {
    let elem = $(this);
    let value = elem.val();
    let data = moment(value);
    $('#end_period').datepicker('setDate', data.add(1, 'w').format('D.MM.Y'));
};

$(function () {
    $('.generate-helps-of-training-js').on('click', function (event) {
        event.preventDefault();
        let form = $(this).parents('.help-form');
        let data = form.serialize();

        $.ajax({
            url: route('admin.helps.training').url(),
            data,
            dataType: 'JSON',
            method: 'get',
            success: (response) => {
                $('.references-js').html(response.result);
            }
        })
    });

    let helpsTable = $('#reference-table');

    let HelpsDT = helpsTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.references.archive.data').url(),
            data: function (d) {
                d.all_helps = $('input[name=all_helps]').prop('checked');
                d.type = $('select[name=referenceType]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id',sortable: false, sorting: false},
            {data: 'student', name: 'student'},
            {data: 'type', name: 'type'},
            {data: 'dates', name: 'dates'},
            {data: 'params', name: 'params'},
            {data: 'status', name: 'status'},
            {data: 'actions', name: 'actions'}
        ]
    });

    $('#filter-references-table-js').on('submit', function (e) {
        HelpsDT.draw();
        e.preventDefault();
    });

    HelpsDT.on('click', '.make-help-js', function (event) {
        let params = $(this).data('params');
        let id = $(this).data('id');
        let url = route('admin.helps.training.delivery', {id}).url();
        let form = $('form#makeHelpForm');
        form.attr('action', url);

        form.find('input[name=start_training]').val(params.start_training);
        form.find('input[name=end_training]').val(params.end_training);
        form.find('input[name=date]').val(params.date);
        form.find('input[name=decree]').val(params.decree);
        form.find('input[name=student_id]').val(params.student_id);
        form.find('input[name=count]').val(params.count);

        $.ajax({
            url: route('admin.student.get-decrees', {id: params.student_id}).url(),
            success: function (response) {
                form.find('select[name=decree_id]')[0].innerHTML = response.options;
                form.find('select[name=decree_id]')[0].value = params.decree_id;
            }
        })

    });
    $(document).on('change', 'select[name=decree_id]', function () {
        let select = $(this);
        let value = select.val();
        let optionText = _.first(select.find(`option[value=${value}]`)).text;
        $('input[name=decree]').val(optionText)
    });
    $('form#makeHelpForm').on('submit', function (event) {
        $('#makeHelp').modal('hide');

        setTimeout(function () {
            HelpsDT.draw()
        }, 10000)
    })

    let startPeriod = $('#start_period');
    let endPeriod = $('#end_period');

    let startPicker = startPeriod.datepicker(datepicker_conf());
    let endPicker = endPeriod.datepicker(datepicker_conf());

    $('.open-money-reference-modal-js').on('click', function (event) {
        let now = moment().day(2);
        let startDate = moment().day(2).add(-1, 'w');
        startPicker.datepicker('setDate', startDate.format('D.MM.Y'));
        endPicker.datepicker('setDate', now.format('D.MM.Y'));
    });
    $('form#moneyReferenceForm').on('submit', function (event) {
        $('#moneyReferenceModal').modal('hide');
        setTimeout(function () {
            HelpsDT.draw()
        }, 8000)
    })
    // startPicker.on('changeDate', function () {
    //     let elem = $(this);
    //     let value = elem.val();
    //     let data = moment(value);
    //     endPicker.datepicker('setDate', data.add(1, 'w').format('D.MM.Y'));
    // });
    //
    // endPicker.on('changeDate', function () {
    //     let elem = $(this);
    //     let value = elem.val();
    //     let data = moment(value);
    //     startPicker.datepicker('setDate', data.add(-1, 'w').format('D.MM.Y'));
    // })
});