let eduPlans = $('#eduplan-table').DataTable({
    dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
    processing: true,
    serverSide: true,
    colReorded: false,
    ajax: {
        url: route('admin.education-plans.data').url(),
        data: function (d) {
            d.name = $('input[name=name]').val();
            d.program_id = $('select[name=program_id]').val();
        }
    },
    language: {
        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
    },
    columns: [
        {data: 'id', name: 'id', sorting: false, sortable: false},
        {data: 'name', name: 'name'},
        {data: 'program', name: 'program'},
        {data: 'period', name: 'period'},
        {data: 'file', name: 'file'},
        {data: 'actions', name: 'actions', sorting: false, sortable: false}
    ]
});

$('form#filter_plans').on('submit', function (event) {
    event.preventDefault();
    eduPlans.draw();
});
let start_training = $('#start_training');
let end_date = $('#end_training');

function setEndDate(startDate, period) {
    let date = moment(startDate, 'D.M.YYYY').add(period, 'months').add(-1, 'days').format('DD.MM.YYYY');
    end_date.val(date);

}

function getProgramOption(val) {
    return $('#program').find(`option[value=${val}]`);
}

start_training.datepicker({
    format: 'dd.mm.yyyy',
    language: 'ru',
    weekStart: 1,
    autoclose: true
});

start_training.on('changeDate', function () {
    let id = $('#program').val();
    let period = getProgramOption(id).data('period');
    setEndDate(this.value, period);
});

end_date.datepicker({
    format: 'dd.mm.yyyy',
    language: 'ru',
    weekStart: 1,
    autoclose: true
});


$('#program').on('change', function () {
    let elem = $(this);
    let period = getProgramOption(elem.val()).data('period');
    let startDate = $('#start_training').val();
    setEndDate(startDate, period);
})


eduPlans.on('click', '.delete-plan-js', function () {

    let elem = this;
    let id = elem.dataset.id;
    swal({
        title: 'Вы уверены?',
        text: "Удаленную информацию будет невозможно восстановить",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Удалить',
        cancelButtonText: 'Отмена',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true,
        reverseButtons: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: route('admin.education-plans.delete', {id: id}, false),
                type: 'POST',
                success: function (data) {
                    swal({
                        title: 'Успешно',
                        text: data,
                        type: 'success'
                    })
                    eduPlans.draw();
                },
                error: function (data) {
                    swal({
                        title: 'Ошибка',
                        text: data,
                        type: 'error'
                    })
                }
            });
        }
    });
});