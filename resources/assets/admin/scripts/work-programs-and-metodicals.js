$(function () {
    $('#subject_select').select2({
        width: '100%',
        placeholder: {id: null, text: 'Select an option'},

    });


    $('.add-subject-in-program-js').on('click', function (event) {
        event.preventDefault();
        let elem = $(this);
        let url = elem.data('url');
        $('#program_url').val(url);
    })
    $('#add_subject_in_prog').on('submit', function (event) {
        event.preventDefault();
        let form = $(this);
        let data = form.serialize();
        let route = form.find('#program_url').val();
        $.ajax({
            url: route,
            method: 'POST',
            data,
            dataType: 'JSON',
            statusCode: {
                200: () => {
                    location.reload();
                },
                422: (response) => {
                    showErrors(response);
                }
            }
        })
    })


    $('select#title').select2({
        width: '100%',
        tags: true
    });
    let add_doc_modal = $('#add_doc_in_subject_form');
    $('.add-doc-on-subject-js').on('click', function (event) {
        event.preventDefault();
        let elem = $(this);
        let url = elem.data('url');
        let name = elem.data('subject');
        let program = elem.data('program');
        $('.add-doc-title').html(`Добавление документа для дисциплины ${name}`);
        add_doc_modal.attr('action', url);
        add_doc_modal.find('#program_id').val(program);
    });

    $('#add_doc_in_subject_form').on('submit', function (event) {
        event.preventDefault();

        let elem = $(this);
        let form = new FormData();

        $.each(elem[0], function (key, value) {
            if (value.type === 'file') {
                form.append('file', value.files[0]);
            } else {
                form.append(value.name, value.value)
            }
        });
        $.ajax({
            url: elem.attr('action'),
            type: 'POST',
            data: form,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: () => {
                $('button').addClass('disabled')
            },
            statusCode: {
                200: (response) => {
                    $('button').removeClass('disabled');
                    location.reload();
                    add_doc_modal.modal('hide');
                },
                422: (response) => {
                    $('button').removeClass('disabled')
                    swal({
                        'type': 'error',
                        'title': 'Произошла ошибка',
                        'text': response.responseJSON.message
                    });
                }
            }
        })

    });

    $('.reload-program-doc-js').on('click', function (event) {
        event.preventDefault();
        let elem = $(this);

        let parent = elem.parents('.program-document-action');
        let input = parent.find('.reload-program-document');

        input.trigger('click');
    });

    $('input[type=file]#program-file').on('change', function () {
        let form = $(this).parents('form')[0];
        console.log(form);

        let route = form.action;

        let file = new FormData();

        file.append('file', form[1].files[0]);

        $.ajax({
            url: route,
            type: 'POST',
            data: file,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: () => {
                $('button,a').addClass('disabled');
            },
            success: (response) => {
                $('button,a').removeClass('disabled');
                //location.reload();
                let row = $(this).parents('.program-document');
                console.log(row);
                row.find('a.program-document-link').attr('href', response.path);
                swal({
                    'type': 'success',
                    'title': 'Документ обновлен'
                });
            },
            error: (response) => {
                $('button,a').removeClass('disabled');
                swal({
                    'type': 'error',
                    'title': 'Произошла ошибка',
                    'text': response.responseJSON.message
                });
            }
        });
    });
    $('.delete-program-document-js').on('click', function (event) {
        event.preventDefault();
        let url = $(this).data('url');
        let sub_Name = $(this).data('sub-name');
        let subName = sub_Name ? `дисциплины ${sub_Name}` : '';
        let name = $(this).data('file-name');
        swal({
            title: 'Вы уверены?',
            text: `Вы хотите удалить ${name}  ${subName}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        let row = $(this).parents('.program-document');

                        swal({
                            title: 'Успешно',
                            text: data,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    $(".delete-subject-on-program-js").on('click', function (event) {
        event.preventDefault();

        let url = $(this).data('url');
        let name = $(this).data('name');
        let message = `Дисциплина ${name} была удалена`;
        swal({
            title: 'Вы уверены?',
            text: `Вы хотите удалить дисциплину ${name}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: message,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    $('.add-other-document-js').on('click', function (event) {
        event.preventDefault();
        let url = $(this).data('url');
        $('#add_prog_other_document_form').attr('action', url);
    });

    $('#add_prog_other_document_form').on('submit', function (event) {
        event.preventDefault();
        let elem = $(this);
        let form = new FormData();
        let url = elem.attr('action');

        $.each(elem[0], function (key, value) {
            if (value.type === 'file') {
                form.append('file', value.files[0]);
            } else {
                form.append(value.name, value.value)
            }
        });
        console.log(elem.serialize());
        $.ajax({
            url,
            type: 'POST',
            data: form,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: () => {
                $('button').addClass('disabled')
            },
            statusCode: {
                200: (response) => {
                    $('button').removeClass('disabled');
                    location.reload();
                    $('#add_prog_other_document').modal('hide');
                },
                422: (response) => {
                    $('button').removeClass('disabled')
                    swal({
                        'type': 'error',
                        'title': 'Произошла ошибка',
                        'text': response.responseJSON.message
                    });
                },
                500: (response) => {
                    $('button').removeClass('disabled')
                    swal({
                        'type': 'error',
                        'title': 'Произошла ошибка',
                        'text': response.responseJSON.message
                    });
                }
            }
        })


    })
});