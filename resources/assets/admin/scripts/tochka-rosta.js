class Upload {
    constructor(form) {
        this.uploadUrl = form.action;
    }

    upload(file) {
        let data = new FormData();
        data.append('file', file);

        $.ajax({
            url: this.uploadUrl,
            type: 'POST',
            data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            // xhr: function () {
            //     var xhr = $.ajaxSettings.xhr();
            //     xhr.upload.addEventListener('progress', function (evt) {
            //         if (evt.lengthComputable) {
            //             let percentComplete = Math.ceil(evt.loaded / evt.total * 100);
            //
            //         }
            //     }, false);
            //     return xhr;
            // },
            success: (response) => {
                let tpl = `<div class="col-md-4 col-xs-6">
                        <div class="tochka-photo">
                            <a href="${response.path}" data-fancybox data-caption="">
                                <img src="${response.path}" alt="" class="img tochka-img">
                            </a>
                            <div class="actions">
                                <a href="${response.deleteUrl}" class="delete-tochka-photo-js">Удалить фото</a>
                            </div>
                        </div>
                    </div>`
                $('.photos').append(tpl);
            }
        });
    }
}

$(function () {
    let table = $('#tochka-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: route('admin.tochka-rosta.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.type = $('select[name=type]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'image', name: 'image'},
            {data: 'publication_date', name: 'publication_date'},
            {data: 'preview', name: 'preview'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions'}
        ]
    });

    let uploadForm, upload;
    if (document.forms.uploadTochaPhotosForm) {
        console.log('form init')
        uploadForm = document.forms.uploadTochaPhotosForm;
        upload = new Upload(uploadForm);

        let dropzone = $('.dropzone');
        let body = $('body');


        body[0].ondragover = function () {
            dropzone.html('Опустите ваши файлы сюда')
        };
        body[0].ondragleave = function () {
            dropzone.html('Перетащите сюда файлы при помощи мыши<br> или нажмите и выберите файлы на компьютере');
        }

        dropzone[0].ondragover = function () {
            dropzone.addClass('hover');
            return false;
        };

        dropzone[0].ondragleave = function () {
            dropzone.removeClass('hover');
            return false;
        };

        dropzone[0].ondrop = function (event) {
            event.preventDefault();
            dropzone.removeClass('hover');
            dropzone.addClass('drop');
            let files = event.dataTransfer.files;
            for (let file in files) {
                if (parseInt(file) >= 0) {
                    upload.upload(files[file]);
                }
            }
            dropzone.removeClass('drop');
            location.reload();
        };


        dropzone.on('click', function (event) {
            $('.photos-input-js').trigger('click');
        });

        $('.photos-input-js').on('change', function () {
            let files = uploadForm.photos.files;
            for (let file in files) {
                if (parseInt(file) >= 0) {
                    //uploadFucntion(uploadUrl, files[file]);
                    upload.upload(files[file]);
                }
            }
        });
    }


    $('body').on('click', '.delete-tochka-photo-js', function (event) {
        event.preventDefault();

        let parent = $(this).parents('.photos');
        console.log(parent);
        let url = $(this).attr('href');
        swal({
            title: 'Вы уверены?',
            html: "<h3>Удаленную фотографию невозможно восстановить</h3>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            html: data,
                            type: 'success'
                        });

                        location.reload();
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })

});