$(function () {
    let holidayTable = $('#holidays-table');

    let holidayDT = holidayTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.holidays.data').url(),
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sortable: false},
            {data: 'description', name: 'description'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });
    $('#start_holiday').inputmask("99.99.9999").datepicker(datepicker_conf());
    $('#end_holiday').inputmask("99.99.9999").datepicker(datepicker_conf());
    $('form#addHolidayForm').on('submit', function (event) {
        event.preventDefault();

        let form = $(this);
        let data = form.serialize();
        let url = route('admin.holidays.store').url();
        axios.post(url, data)
            .then(function (response) {
                $('#addHolidayModal').modal('hide');
                holidayDT.draw();
            })
            .catch(function (error) {
                swal({
                    title: 'Ошибка',
                    html: `<b>${error.response.data.message}</b>`,
                    type: 'error'
                })
            });
    });

    $(document).on('click', '.delete-holiday-js', function (event) {
        event.preventDefault();
        let name = $(this).data('name');
        let url = $(this).data('route');

        swal({
            title: 'Вы уверены?',
            html: `<span style="font-size: 14px">Вы действительно хотите удалить праздник «${name}»</span>`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data.message,
                            type: 'success'
                        }).then(() => {
                            holidayDT.draw();
                        });
                    },
                    error: function (data) {
                        console.log(data);
                        swal({
                            title: 'Ошибка',
                            text: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })
});