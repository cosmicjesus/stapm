$(function () {
    let oTable = $('#programs-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.programs.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.email = $('input[name=email]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sortable: false},
            {data: 'name', name: 'name'},
            {data: 'form', name: 'form'},
            {data: 'type', name: 'type'},
            {data: 'period', name: 'period'},
            {data: 'presentation', name: 'presentation'},
            {data: 'video_presentation', name: 'video_presentation'},
            {data: 'sort', name: 'sort'},
            {data: 'on_site', name: 'on_site'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    $('#search-form').on('submit', function (e) {
        oTable.draw();
        e.preventDefault();
    });
});

let licence = $('#licence');
licence.datepicker(datepicker_conf());

$('#period').on('click blur', function () {
    let year = Math.floor(this.value / 12);
    let year_string = year >= 1 ? `${year} ${getNumEnding(year, ['год', 'года', 'лет'])}` : '';
    let month = this.value % 12;
    let month_string = month >= 1 ? `${month} ${getNumEnding(month, ['месяц', 'месяца', 'месяцев'])}` : '';
    let message = `${year_string} ${month_string}`;
    $('.period-string-js').html(message);
});