$(function () {
    let addEnroleeForm = $('form#partner-add-form');

    addEnroleeForm.validate({
        rules: {
            name: {
                required: true
            },
            logo: {
                required: true
            },
            site_url: {
                required: true
            },
            sort: {
                required: true
            },
            // description: {
            //     required: true
            // },
        },
        messages: {
            name: {
                required: 'Необходимо указать название'
            },
            logo: {
                required: 'Необходимо загрузить логотип'
            },
            site_url: {
                required: 'Необходимо указать адрес сайта'
            },
            sort: {
                required: 'Укажите порядок'
            },
            description: {
                required: 'Необходимо заполнить описание'
            }
        }
    });

    $(document).on('click', '.delete-partner-js', function (event) {
        event.preventDefault();

        let elem = $(this);
        let url = elem.data('url');
        let name = elem.data('name');

        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            html: data.message,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });
});