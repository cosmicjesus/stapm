$(function () {
    $('.delete-adm-js').on('click', function () {

        let elem = this;
        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).data('url'),
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });
});