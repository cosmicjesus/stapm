$(function () {
    let regulationsTable = $('#regulations-table')
        .DataTable({
            dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: route('admin.regulations.data').url()
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'comment', name: 'comment'},
                {data: 'date', name: 'date'},
                {data: 'count_files', name: 'count_files'},
                {data: 'count_violations', name: 'count_violations'},
                {data: 'active', name: 'active'},
                {data: 'actions', name: 'actions', sortable: false},
            ]
        });

    let addRegulationForm = $('#add-regulation-form');

    addRegulationForm.validate();

    let addRegulationViolationForm = $('#add-violation-form');
    addRegulationViolationForm.validate()
});