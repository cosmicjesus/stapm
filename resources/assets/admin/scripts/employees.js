$(function () {
    let EmployeesTable = $('#employees-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url: route('admin.employees.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.position = $('select[name=position]').val();
                d.category = $('select[name=category]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sorting: false, sortable: false},
            {data: 'full_name', name: 'full_name'},
            {data: 'birthday', name: 'birthday'},
            {data: 'age', name: 'age'},
            {data: 'category', name: 'category'},
            {data: 'positions', name: 'positions'},
            {data: 'count_courses', name: 'count_courses'},
            {data: 'actions', name: 'actions'}

            // {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    $('#search-employees').on('submit', function (e) {
        EmployeesTable.draw();
        e.preventDefault();
    });


    let birthday = $('#birthday');
    let category = $('#category_date');
    let start_date = $('#start_date');

    birthday.datepicker(datepicker_conf());

    category.datepicker({
        format: 'dd.mm.yyyy',
        language: 'ru',
        weekStart: 1,
        toValue: function (date, format, language) {
            return moment(date).format('D.MM.YYYY')
        }
    });
    category.attr('value', moment().format('D.MM.YYYY'));

    start_date.datepicker(datepicker_conf({
        endDate: moment().format('D.MM.YYYY')
    }));

    $('select#education').change(function () {
        let val = $(this).val();
        let type = $(this).find(`option[value=${val}]`).data('edu-type');
        if (type == 'medium' || type == 'high') {
            $('.qualification').css({
                'display': 'block'
            })
        } else {
            $('.qualification').css({
                'display': 'none'
            });
            $('.qualification-js').val('');
        }
    });

    $('.employee-photo:not(.delete-empl-photo)').click(function () {
        $('#user_photo').trigger('click');
    });

    $('#user_photo').on('change', function () {
        let elem = $(this);
        let form = $('form[name=user_photo]');
        let file = new FormData();
        file.append('file', form[0][1].files[0]);

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: file,
            cache: false,
            dataType: 'json',
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData: false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType: false,
            beforeSend: function () {

            },
            success: function (respond, status, jqXHR) {
                $('.profile-user-img').attr('src', respond.path);
                $('.employee-photo').addClass('delete-empl-photo');
            },
            error: function (jqXHR, status, errorThrown) {
                // console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
            }
        });
    });

    $('.delete-empl-photo').click(function () {
        let slug = $(this).data('slug');

        swal({
            title: 'Вы уверены?',
            text: "Удаленную фотографию невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: route('admin.employee.photo.delete', {slug}, false),
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data,
                            type: 'success'
                        });
                        window.location.reload();
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });


    let layoff_modal = $('#layoff_modal');
    let layoff_date = $('#layoff_date');

    let layoff_picker = layoff_date.datepicker(datepicker_conf());

    $('.layoff-position-js').on('click', function (event) {
        event.preventDefault();
        let start_date_val = $(this).data('start-date');
        let id = $(this).data('id');
        let start_date = moment(start_date_val, 'DD.MM.YYYY').add({days: 1}).format('DD.MM.YYYY');
        layoff_date.datepicker('setStartDate', start_date);

        layoff_modal.find('#id').val(id);
    });

    $('#layoff_form').on('submit', function (event) {
        event.preventDefault();
        let data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            data,
            dataType: 'JSON',
            method: 'post',
            statusCode: {
                200: (response) => {
                    layoff_modal.modal('hide');
                    $('.errors-block').html('');
                    location.reload();
                },
                422: (response) => {
                    showErrors(response);
                }
            }
        })
    });

    let add_position_modal = $('#add_position_modal');
    let add_position_form = $('#add_position_form');

    $('.add-position-js').on('click', function () {
        let employee_id = $(this).data('id');
        add_position_modal.find('#employee_id').val(employee_id);
    });

    add_position_form.on('submit', function (event) {
        event.preventDefault();
        let data = $(this).serialize();
        let id = $(this).find('#employee_id').val();
        $.ajax({
            url: route('admin.employee.add.position', {id}, false),
            data,
            dataType: 'JSON',
            method: 'post',
            statusCode: {
                200: (response) => {
                    add_position_modal.modal('hide');
                    $('.errors-block').html('');
                    location.reload();
                },
                422: (response) => {
                    showErrors(response);
                }
            }
        })
    });


    $('.category-select-js').on('change', function () {
        let value = $(this).val();
        console.log(value);
        let block = $('.qcategory-picker-js');
        let picker = $('input[name=category_date]');
        if (value < 1) {
            block.css({
                'display': 'none'
            });
            picker.val(null);
        } else {
            block.css({
                'display': 'block'
            });
            picker.val(moment().format('DD.MM.YYYY'));
        }
    });

    $('.add-education-js').on('click', function (event) {
        event.preventDefault();
        let employeeId = $(this).data('employee-id');
        $('input#employee_id').attr('value', employeeId);
    });

    $('form#add_education_form').on('submit', function (event) {
        event.preventDefault();

        let data = $(this).serialize();
        let url = route('admin.employees.add-education').url();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: (response) => {
                swal({
                    title: 'Внимание!',
                    html: `<b>${response.message}</b>`,
                    type: 'success'
                }).then(() => {
                    $("#add_education").modal('hide');
                    $('.errors-block').html('');
                    $('.education-table-body-js').append(response.template)

                })
            }, error: (response) => {
                swal({
                    title: 'Ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        })

    });

    $(document).on('click', '.edit-employee-education-js', function (event) {
        event.preventDefault();

        let qualification = $(this).data('qualification');
        let direction = $(this).data('direction');
        let route = $(this).data('route');
        let name = $(this).data('name');

        $('.qualification-field-js').val(qualification);
        $('.direction-field-js').val(direction);
        let form = $('form#edit-education-form');
        form.attr('action', route);
        form.find('h4').html(name);
    });

    $('form#edit-education-form').on('submit', function (event) {
        event.preventDefault();

        let data = $(this).serialize();
        let url = $(this).attr('action');

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: (response) => {
                swal({
                    title: 'Внимание!',
                    html: `<b>${response.message}</b>`,
                    type: 'success'
                }).then(() => {
                    $("#edit-education-modal").modal('hide');
                    $('.errors-block').html('');
                    let id = response.education_id;
                    $('tr[data-edu-id=' + id + ']').html(response.template)

                })
            }, error: (response) => {
                swal({
                    title: 'Ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        })
    });

    $(document).on('click', '.delete-employee-education-js', function (event) {
        event.preventDefault();

        let id = $(this).data('id');
        let employee_id = $(this).data('employee-id');
        let url = route('admin.employee.delete-education', {employee_id, id}).url();
        let row = $(`tr[data-edu-id=${id}]`);
        swal({
            title: 'Вы уверены?',
            html: `<b>Вы действительно хотите удалить уровень образования</b>`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: `${data.message}`,
                                type: 'success'
                            }).then(() => {
                                row.remove();
                            });
                        } else {
                            swal({
                                title: 'Внимание!',
                                html: `${data.message}`,
                                type: 'warning'
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });

    });

    $('#employee-subject-form').on('submit', function (event) {
        event.preventDefault();
        let data = $(this).serialize();
        let url = $(this).attr('action');

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: (response) => {
                if (response.success) {
                    let res = JSON.parse(response.data);
                    let tpl = `<tr>
                                    <td>${res.subject}</td><td>
                                   <button class="btn btn-danger delete-employee-subject-js" data-url="${res.delete_url}" data-name="${res.subject}">
                                           <i class="fa fa-trash"></i>
                                   </button>
                                   </td>
                               </tr>`;
                    $('.employee-subjects-table').append(tpl);
                    resetSelect2();
                } else {
                    swal({
                        title: 'Внимание!',
                        html: response.message,
                        type: 'info'
                    }).then(() => {
                        resetSelect2();
                    })
                }
            },
            error: (response) => {
                swal({
                    title: 'Ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    });


    $(document).on('click', '.delete-empl-position-js', function (event) {
        let elem = $(this);
        let id = elem.data('id');
        let name = elem.data('name');
        let employee_id = elem.data('employee_id');
        let url = route('admin.employee.position.delete', {employee_id, id}).url();
        let row = $(`tr[data-position-id=${id}]`);
        swal({
            title: 'Вы уверены?',
            html: `<b>Вы действительно хотите удалить должность «${name}»?<br>Это действие необратимо удалит должность у сотрудника.<br>Используйте эту возможность только при ошибочном добавлении должности</b>`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: data.message,
                                type: 'success'
                            }).then(() => {
                                row.remove();
                            });
                        } else {
                            swal({
                                title: 'Внимание!',
                                html: `${data.message}`,
                                type: 'warning'
                            });
                        }

                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    $(document).on('click', '.delete-employee-subject-js', function (event) {

        event.preventDefault();
        let url = $(this).data('url');
        let name = $(this).data('name');
        swal({
            title: 'Вы уверены?',
            html: `<b>Вы действительно хотите удалить дисциплину «${name}»</b>`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data.message,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })

    $('#employees-table').on('click', '.dismiss-employee-js', function (event) {
        let elem = $(this);
        let url = elem.data('url');
        let name = elem.data('name');

        swal({
            title: 'Вы уверены?',
            html: `<b>Вы действительно хотите уволить сотрудника ${name}?</b>`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: data.message,
                                type: 'success'
                            }).then(() => {
                                EmployeesTable.draw();
                            });
                        } else {
                            swal({
                                title: 'Внимание!',
                                html: `${data.message}`,
                                type: 'warning'
                            });
                        }

                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });

    });
});

