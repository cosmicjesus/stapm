$(function () {
    $('.delete-category-js').on('click', function (event) {
        event.preventDefault();
        let name = $(this).data('name');
        let url = $(this).data('url');

        swal({
            title: 'Вы уверены?',
            text: `Вы действительно хотите удалить категорию «${name}»`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                text: data.message,
                                type: 'success'
                            }).then(() => {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: 'Ошибка',
                                text: data.message,
                                type: 'error'
                            })
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        swal({
                            title: 'Ошибка',
                            text: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    let documentsTable = $('#documents-table');

    let DocumentsDT = documentsTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: route('admin.documents.data').url(),
            data: function (d) {
                d.category = $('select[name=category]').val();
            }
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'category', name: 'category'},
            {data: 'file', name: 'file'},
            {data: 'sort', name: 'sort'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions'}
        ]
    });

    $('#filter-documents-table-js').on('submit', function (e) {
        DocumentsDT.draw();
        e.preventDefault();
    })

    documentsTable.on('click', '.delete-document-js', function (event) {
        event.preventDefault();
        let name = $(this).data('name');
        let url = $(this).data('url');

        swal({
            title: 'Вы уверены?',
            text: `Вы действительно хотите удалить документ «${name}»`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data.message,
                            type: 'success'
                        }).then(() => {
                            DocumentsDT.draw();
                        });
                    },
                    error: function (data) {
                        console.log(data);
                        swal({
                            title: 'Ошибка',
                            text: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });
});