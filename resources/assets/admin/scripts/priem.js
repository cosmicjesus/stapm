(function () {
    let body = $('body');
    body.on('click', '.edit-priem-js', function (event) {
        event.preventDefault();
        let elem = $(this);
        let params = elem.data('params');
        let name = elem.data('name');
        $('.edit_priem_modal_title').html(name);

        $('input#reception').val(params.reception);
        $('input#count_with_documents').val(params.count_with_documents);
        $('input#count_with_out_documents').val(params.count_with_out_documents);
        $('form#edit-priem-form').attr('action', params.edit_url);
    });

    body.on('submit', 'form#edit-priem-form', function (event) {
        event.preventDefault();

        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                $('.priem-table-js').html(response.template);
                $('#editPriem').modal('hide');
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: `<b>${response.responseJSON.message}</b>`,
                    type: 'error'
                })
            }
        });
    });

    $('.delete-priem-js').on('click', function () {

        let elem = this;
        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).data('url'),
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data,
                            type: 'success'
                        }).then(() => {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });
})();