$(function () {
    let uploadButton = $('.upload-doc-js');
    let file = '';

    $('.upload-doc-js').on('click', function (event) {
        event.preventDefault();
        let type = $(this).data('type');
        let parent = $(this).parents(`.actions-${type}`);
        let fileInput = parent.find(`#file-${type}`);
        console.log(parent, fileInput, type);
        fileInput.addClass('pop').trigger('click');
    })

    $('input[type=file].docs').on('change', function () {

        let elem = $(this);
        let form = elem.parents('form');
        let route = form.attr('action');
        let file = new FormData();

        file.append('file', form[0][1].files[0]);
        console.log(form[0][1].files[0], route, file.getAll('file'));

        $.ajax({
            url: route,
            type: 'POST',
            data: file,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: () => {
                uploadButton.addClass('disabled');
            },
            success: (response) => {
                uploadButton.removeClass('disabled');
                location.reload();
            },
            error: (response) => {
                uploadButton.removeClass('disabled');
                swal({
                    'type': 'error',
                    'title': 'Произошла ошибка',
                    'text': response.responseJSON.message
                });
            }
        })
    })
});