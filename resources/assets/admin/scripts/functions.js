Object.map = function (obj, fn) {

    const ret = {};

    Object.keys(obj).forEach(function (k) {
        ret[k] = fn.call(null, k, obj[k]);
    });

    return ret;
};

const datepicker_options = {
    autoclose: true,
    format: 'dd.mm.yyyy',
    language: 'ru',
    weekStart: 1
};

function datepicker_conf(config = {}) {
    return Object.assign(config, datepicker_options);
}

function getNumEnding(number, endingArray) {
    let num = number % 100;
    if (num >= 11 && num <= 19) {
        var ending = endingArray[2];
    }
    else {
        let i = num % 10;
        switch (i) {
            case (1):
                ending = endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                ending = endingArray[1];
                break;
            default:
                ending = endingArray[2];
        }
    }
    return ending;
}

function showErrors(responce) {
    let res = responce.responseJSON.errors;
    let errors = [];
    Object.map(res, (k, v) => {

        errors.push(`<li>${v}</li>`);

    });
    $('.errors').html(errors);
    $('.errors-block').removeClass('hide').addClass('show');
}

function modal(modalID, callback) {
    $('.modal').on('submit', modalID, callback);
}

function resetSelect2() {
    $.each($('.select-2-js'), function (key, input) {
        input.value = null;
        $('.select2-selection__rendered').attr('title', '').text('');
    });
}

function validate(form) {
    let errorsFields = form.find(".form-group");

    $.each(errorsFields, function (key, element) {
        element.classList.remove('has-error')
    });

    let errors = 0;
    let fields = form.find('.required');
    $.each(fields, function (key, element) {
        if (isEmpty(element.value)) {
            console.log(element.parentNode);
            element.parentNode.classList.add('has-error');
            errors++;
        }
    });
    if (errors > 0) {
        return false;
    }

    return true;
}

function isEmpty(str) {
    return (typeof str === "undefined" || str === null || str === "");
}

function randomLoaderMessage() {
    let messages = [
        'Загрузка... Пожалуйста подождите',
        'Наводим ракеты на Америку. Пожалуйста подождите',
        'Вбрасываем бюллетени. Пожалуйста подождите',
        'Разгоняем митинг. Пожалуйста подождите',
        'Бомбим террористов в Сирии. Пожалуйста подождите',
        'Подделываем результаты выборов в Америке. Пожалуйста подождите',
        'Преодолеваем пространство. Пожалуйста подождите',
        'Подглядываем в ваш компьютер. Пожалуйста подождите',
        'Воруем сохраненные пароли. Пожалуйста подождите',
        'Охлаждаемся. Пожалуйста подождите',
    ];

    let index = Math.floor(Math.random() * messages.length);
    $('.preload-text').html(messages[index]);
}

function showLoader() {
    randomLoaderMessage();
    $('#before-load').find('i').fadeIn();
    $('#before-load').fadeIn();
}

function hideLoader() {
    $('#before-load').find('i').fadeOut().end().delay(400).fadeOut('slow');
}

function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        let randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
}

$(window).on('load', function () {
    hideLoader();
});

async function getFormData() {
    try {
        const response = await axios.get(route('admin.form-data').url());
        let data = response.data;
        $('#issuanceDate').inputmask("99.99.9999");
        $('#firstname').easyAutocomplete({
            data: data.names,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#middlename').easyAutocomplete({
            data: data.middlenames,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#graduation_organization_name').easyAutocomplete({
            data: data.graduation_organizations_name,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#graduation_organization_place').easyAutocomplete({
            data: data.graduation_organizations_place,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });
        $('#birthPlace').easyAutocomplete({
            data: data.birthPlaces,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });
        $('#place_of_stay-region,#residential-region,#registration-region').easyAutocomplete({
            data: data.regions,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#place_of_stay-area,#residential-area,#registration-area').easyAutocomplete({
            data: data.area,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#place_of_stay-city,#residential-city,#registration-city').easyAutocomplete({
            data: data.cities,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#place_of_stay-settlement,#residential-settlement,#registration-settlement').easyAutocomplete({
            data: data.settlement,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });

        $('#place_of_stay-street,#residential-street,#registration-street').easyAutocomplete({
            data: data.streets,
            list: {
                maxNumberOfElements: 10,
                match: {
                    enabled: true
                }
            },
            theme: "bootstrap"
        });


        //birthPlace
    } catch (error) {
        console.error(error);
    }
}

$(function () {
    randomLoaderMessage();
    resetSelect2();
    getFormData();

    $('body').on('hidden.bs.modal', '.modal', function () {
        $.each($('.modal form'), function (key, form) {
            form.reset();
        });
        resetSelect2();
    });

    $('#default-picker').datepicker(datepicker_conf());
    $('#date_count_enrollees_report').datepicker(datepicker_conf());

    $('form.asyncValidate').on('submit', function (event) {
        let form = $(this);
        let errorsFields = form.find(".form-group");

        $.each(errorsFields, function (key, element) {
            element.classList.remove('has-error')
        });

        let errors = 0;
        let fields = form.find('.required');
        $.each(fields, function (key, element) {
            if (isEmpty(element.value)) {
                console.log(element.parentNode);
                element.parentNode.classList.add('has-error');
                errors++;
            }
        });
        if (errors > 0) {
            return false;
        }
    });

    $('select.required').on('change', function () {
        let elem = $(this)[0];
        if (!isEmpty(elem.value)) {
            elem.parentNode.classList.remove('has-error');
        }
    })

    $('input.required').on('keydown', function () {
        let elem = $(this)[0];
        let parentClass = elem.parentNode.classList;
        if (!isEmpty(elem.value)) {
            parentClass.remove('has-error');
        } else {
            parentClass.add('has-error');
        }
    })

    $('.random-password-js').on('click', function () {
        $('.password-field-js').val(randomString(8));
    });

    $('#add-user-form').on('submit', function () {
        let permissions = $('.permission-checkbox-js');
        let hasPermissions = false;

        $.each(permissions, function (key, permission) {
            if (permission.checked) {
                hasPermissions = true;
            }
        });

        if (hasPermissions) {
            permissions.removeClass('has-error');
            return true;
        }

        permissions.parents('.form-group').addClass('has-error');

        return false;
    });

    $('option').mousedown(function (e) {
        e.preventDefault();
        $(this).toggleClass('selected');

        $(this).prop('selected', !$(this).prop('selected'));
        return false;
    });

    $('.delete-user-js').on('click', function (e) {
        e.preventDefault();

        let url = $(this).data('url');
        let user = $(this).data('user');
        swal({
            title: 'Вы уверены?',
            html: `Вы действительно хотите удалить данные для входа пользователю ${user}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                text: data.message,
                                type: 'success'
                            }).then(() => {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: 'Ошибка',
                                text: data.message,
                                type: 'error'
                            })
                        }
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })

});
