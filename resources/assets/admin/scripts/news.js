$(function () {
    let news = $('#news-table');
    let publication_date = $('#publication_date');
    let visible_to = $('#visible_to');
    let preview = $('#preview');
    let full_text = $('#full_text');

    let newsDt = news.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: route('admin.news.data').url()
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', orderable: false},
            {data: 'title', name: 'title'},
            {data: 'image', name: 'image'},
            {data: 'publication_date', name: 'publication_date'},
            {data: 'visible_to', name: 'visible_to'},
            {data: 'section', name: 'section'},
            {data: 'preview', name: 'preview'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    publication_date.datepicker(datepicker_conf());
    visible_to.datepicker(datepicker_conf());
    preview.wysihtml5();
    full_text.wysihtml5();


    $(document).on('click', '.delete-news-js', function () {

        let elem = this;
        let url = elem.dataset.route;
        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            text: data,
                            type: 'success'
                        })
                        newsDt.draw();
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            text: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })

    $('.add-news-files-js').on('click', function () {
        $('.files').append('<div class="file-item">\n' +
            '                <div class="form-group col-xs-5">\n' +
            '                    <label for="">Наименование</label>\n' +
            '                    <input type="text" name="names[]" class="form-control">\n' +
            '                </div>\n' +
            '                <div class="form-group col-xs-5">\n' +
            '                    <label for="">Файл</label>\n' +
            '                    <input type="file" name="files[]" class="form-control">\n' +
            '                </div>\n' +
            '                <div class="col-xs-2">\n' +
            '                    <button class="btn btn-danger delete-news-files-js" style="margin:15px auto;">\n' +
            '                        <i class="fa fa-trash"></i>\n' +
            '                    </button>\n' +
            '                </div>\n' +
            '            </div>');
    });

    $(document).on('click', '.delete-news-files-js', function () {
        let element = $(this);
        element.parents('.file-item').remove();
    })

    $('.edit-news-document-js').on('click', function () {
        let parent = $(this).parents('.news-file');

        let input_block = parent.find('.news-file-edit');

        let documentNameLink = parent.find('.news-document-link');

        input_block.css({
            'display': 'block'
        });
        parent.find('#rename-news-file').css({
            'display': 'block'
        });

        documentNameLink.css({
            'display': 'none'
        });

        $(this).css({
            'display': 'none'
        })
    });

    $('.rename-news-file-js').on('click', function (e) {
        let parent = $(this).parents('.rename-news-file');
        let name = parent.find('input[name=news-document-name]').val();
        let url = parent.data('url');

        $.ajax({
            url,
            data: {name},
            type: 'POST',
            success: function (data) {
                console.log(data);
                let link = parent.parents('.news-file').find('.news-document-link');
                link.css({
                    'display': 'block'
                });
                link.html(name);
                parent.css({
                    'display': 'none'
                })
                parent.parents('.news-file').find('.edit-news-document-js').css({
                    'display': 'block'
                });
            },
            error: function (data) {
                swal({
                    title: 'Ошибка',
                    html: data.responseJSON.message,
                    type: 'error'
                })
            }
        });

    });

    $('.delete-news-document-js').on('click', function () {

        let elem = $(this);
        let name = elem.data('name');
        let url = elem.data('url');

        swal({
            title: 'Вы уверены?',
            text: `Вы действительно хотите удалить документ «${name}»`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url,
                    type: 'POST',
                    success: function (data) {
                        swal({
                            title: 'Успешно',
                            html: `${data.message}`,
                            type: 'success'
                        }).then(() => {
                            elem.parents('.news-file').remove();
                        });
                    },
                    error: function (data) {
                        console.log(data);
                        swal({
                            title: 'Ошибка',
                            text: data.responseJSON.message,
                            type: 'error'
                        })
                    }
                });
            }
        });
    })

});