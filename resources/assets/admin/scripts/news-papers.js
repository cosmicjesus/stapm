$(function () {
    let papersTable = $('#news-papers-table');

    let newsPaperDt = papersTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: route('admin.news-papers.data').url()
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', orderable: false},
            {data: 'number', name: 'number'},
            {data: 'cover', name: 'cover'},
            {data: 'file', name: 'file'},
            {data: 'publication_date', name: 'publication_date'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });

    let addForm = $('#news-papers-add-form');

    addForm.validate();
});