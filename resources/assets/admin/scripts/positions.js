let positions_table=$('#positions-table');

let oTable = positions_table.DataTable({
    dom: "<'row'<'col-xs-12't>>" +
    "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
    processing: true,
    serverSide: true,
    ajax: {
        url: route('admin.positions.data').url(),
        data: function (d) {
            d.name = $('input[name=name]').val();
            d.type = $('select[name=type]').val();
        }
    },
    language: {
        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
    },
    columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'type', name: 'type'},
        {data: 'count_employees', name: 'count_employees'},
        {data: 'actions', name: 'actions', sortable: false},
    ]
});

$('#search-form').on('submit', function (e) {
    oTable.draw();
    e.preventDefault();
});

positions_table.on('click','.delete-position-js',function () {

    let elem=this;

    let id=elem.dataset.id;
    swal({
        title: 'Вы уверены?',
        text: "Удаленную информацию будет невозможно восстановить",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Удалить',
        cancelButtonText: 'Отмена',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true,
        reverseButtons: false
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:route('admin.position.delete',{id:id},false),
                type: 'POST',
                success: function(data) {
                    swal({
                        title:'Успешно',
                        text:data,
                        type:'success'
                    });
                    oTable.draw();
                },
                error: function(data) {
                    swal({
                        title:'Ошибка',
                        text:data,
                        type:'error'
                    })
                }
            });
        }
    });
});