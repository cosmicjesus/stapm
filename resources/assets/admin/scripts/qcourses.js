$(function () {
    let course_start = $('#course_start_date');
    let course_end = $('#course_end_date');
    let open_course_modal = $('.add-course--modal-js');
    let form = $('#add_course');
    let add_course_modal = $('#add_course_form');

    course_start.datepicker(datepicker_conf());
    course_end.datepicker(datepicker_conf());

    open_course_modal.on('click', function () {
        let employee_id = $(this).data('id');
        $('#employee_id').val(employee_id);
    });

    let coursesTable = $('#qcourses');
    let courseUrl=coursesTable.data('url');

    let courseDt=coursesTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: courseUrl
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        order: [[ 1, "asc" ]],
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'period', name: 'period'},
            {data: 'hours', name: 'hours'},
            {data: 'actions', name: 'actions', sortable: false},
        ]
    });


    form.on('submit', function (event) {
        event.preventDefault();
        let data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            data,
            dataType: 'JSON',
            method: 'post',
            statusCode: {
                200: (response) => {
                    console.log(response, 200);
                    courseDt.draw();
                    add_course_modal.modal('hide');
                    $('.errors-block').html('');
                },
                422: (response) => {
                    console.log(response, 422);
                    showErrors(response);
                }
            }
        })
    });

    coursesTable.on('click','.delete-course',function () {

        let elem=this;
        let id=elem.dataset.id;
        swal({
            title: 'Вы уверены?',
            text: "Удаленную информацию будет невозможно восстановить",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result)=>{
            if(result.value){
                $.ajax({
                    url:route('admin.course.delete',{id},false),
                    type: 'POST',
                    success: function(data) {
                        swal({
                            title:'Успешно',
                            text:data,
                            type:'success'
                        })
                        courseDt.draw();
                    },
                    error: function(data) {
                        swal({
                            title:'Ошибка',
                            text:data,
                            type:'error'
                        })
                    }
                });
            }
        });
    });

});