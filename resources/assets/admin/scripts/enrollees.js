$(function () {

    async function getCodes() {
        try {
            const response = await axios.get(route('admin.enrollees.getCodes').url());
            let data = response.data;
            let params = {
                data,
                getValue: "code",
                template: {
                    type: "description",
                    fields: {
                        description: "place"
                    }
                },
                list: {
                    maxNumberOfElements: 10,
                    match: {
                        enabled: true
                    }
                },
                theme: "bootstrap"
            };
            $("#subdivisionCode").easyAutocomplete({
                data,
                getValue: "code",
                template: {
                    type: "description",
                    fields: {
                        description: "place"
                    }
                },
                list: {
                    maxNumberOfElements: 10,
                    match: {
                        enabled: true
                    },
                    onClickEvent: function () {
                        var value = $("#subdivisionCode").getSelectedItemData().place;

                        $('#issued').val(value);
                    }
                },
                theme: "bootstrap"
            });
        } catch (error) {
            console.error(error);
        }
    }

    getCodes();
    let body = $('body');
    let EnrolleesTable = $('#enrollees-table').DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        "scrollX": true,
        ajax: {
            url: route('admin.enrollees.data').url(),
            data: function (d) {
                d.name = $('input[name=name]').val();
                d.profession = $('select[name=professions]').val();
                d.documents = $('select[name=documents]').val();
                d.medical = $('select[name=medical]').val();
                d.contract_target_set = $('input[name=contract_target_set]').prop('checked');
                d.no_snils = $('input[name=no_snils]').prop('checked');
                d.no_inn = $('input[name=no_inn]').prop('checked');
                d.no_passport = $('input[name=no_passport]').prop('checked');
                d.no_addresses = $('input[name=no_addresses]').prop('checked');
                d.need_hostel = $('input[name=need_hostel]').prop('checked');
            }
        },
        columnDefs: [
            {
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }
        ],
        select: {
            'style': 'multi'
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', sortable: false, sorting: false},
            {data: 'actions', name: 'actions', sortable: false},
            {data: 'missingData', name: 'missingData', sortable: false},
            {data: 'errors', name: 'errors', sortable: false},
            {data: 'id', name: 'id', sortable: false},
            {data: 'full_name', name: 'full_name'},
            {data: 'program', name: 'program'},
            {data: 'hasOriginal', name: 'hasOriginal'},
            {data: 'medical', name: 'medical'},
            {data: 'need_hostel', name: 'need_hostel'},
            {data: 'passport', name: 'passport'},
            {data: 'snils', name: 'snils'},
            {data: 'inn', name: 'inn'},
            {data: 'language', name: 'language'},
            {data: 'avg', name: 'avg'},
            {data: 'phone', name: 'phone'}
        ]
    });

    $('#filter-enrollees-table-js').on('submit', function (e) {
        EnrolleesTable.draw();
        e.preventDefault();
    });
    $('#birthday').inputmask("99.99.9999");
    $('#start_date').inputmask("99.99.9999");

    $('.has_original_check-js').on('click', function () {
        let elem = $(this);
        let checkbox = _.first(elem.find('input[name=has_original]'));
        let isChecked = checkbox.checked;

        if (isChecked) {
            $('.graduation_organization_name-js').fadeIn();
        } else {
            $('.graduation_organization_name-js').fadeOut();
            $('#graduation_organization_name').val(null);
        }
    });

    $('#graduation_date').datepicker(datepicker_conf());
    $('#graduation_date').inputmask("99.99.9999");
    $('#decree_date').datepicker(datepicker_conf());

    let calcAvg = function () {
        let countTherees = parseInt($('input#threes').val());
        let countFours = parseInt($('input#fours').val());
        let countFives = parseInt($('input#fives').val());
        let totalCountRatings = countTherees + countFours + countFives;
        console.log(countFives, countFours, countTherees);

        let totalThreesSum = countTherees * 3;
        let totalFoursSum = countFours * 4;
        let totalFivesSum = countFives * 5;
        let totalSum = totalThreesSum + totalFoursSum + totalFivesSum;
        let avg = totalSum / totalCountRatings;
        $('.avg-elemets-js').html(totalCountRatings);
        if (avg > 0) {
            $('.avg-no-round-js').html(avg);
            $('.avg-js').html(Math.round(avg * 10000) / 10000);
        } else {
            $('.avg-js').html(0);
        }

    };
    body.on('click', '.number-of-ratings-js', function () {
        calcAvg()
    });
    body.on('keyup', '.number-of-ratings-js', function (event) {
        console.dir(event.target.value);
        calcAvg()
    });

    $('.add-avg-js').on('click', function () {
        let avg = $('.avg-js').text();
        if (parseInt(avg) > 0) {
            $('#avg').val(avg);
        }

        $('#avg-calc-modal').modal('hide');
    })

    body.on('click', '.enrollment-js', function (event) {
        event.preventDefault();

        let checked = EnrolleesTable.column(0).checkboxes.selected();
        if (!checked.length) {
            return false;
        }
    });

    $('.delete-enrollees-js').on('click', function (event) {

        event.preventDefault();

        let checked = EnrolleesTable.column(0).checkboxes.selected();
        if (!checked.length) {
            return false;
        }
        let ids = [];

        $.each(checked, function (key, rowId) {
            let row = JSON.parse(rowId);
            ids.push(row.id);
        });
        let data = {};
        data['ids'] = ids;
        swal({
            title: 'Вы уверены?',
            text: `Вы хотите удалить ${ids.length} абитуриентов?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Удалить',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: true,
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: route('admin.enrollees.delete').url(),
                    data,
                    type: 'POST',
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: 'Успешно',
                                html: data.message,
                                type: 'success'
                            }).then(() => {
                                EnrolleesTable.draw()
                            })
                        } else {
                            swal({
                                title: 'Ошибка',
                                html: data.message,
                                type: 'error'
                            })
                        }
                    },
                    error: function (data) {
                        swal({
                            title: 'Ошибка',
                            html: data,
                            type: 'error'
                        })
                    }
                });
            }
        });
    });

    $('.enrollment-js').on('click', function (event) {
        event.preventDefault();
        let checked = EnrolleesTable.column(0).checkboxes.selected();
        let enrollees = '';
        let form = $('form[name=decree]');
        let ids = '';
        $.each(checked, function (key, value) {
            let row = JSON.parse(value);
            ids += `<input type="hidden" name="ids" value="${row.id}">`;
            enrollees += `<tr data-id="${row.id}"><td>${row.full_name}</td></tr>`;
        })
        $('.enrollees-list-js').html(enrollees);
        form.find('.enrollee-ids').html(ids);
    });

    $('#enrollment-form').on('submit', function (event) {
        event.preventDefault();

        let form = $(this);
        let validation = validate(form);

        if (!validation) {
            return false;
        }

        let data = {};
        let ids = [];

        $.each(form[0], function (key, item) {
            if (item.name === 'ids') {
                ids.push(item.value);
            } else {
                data[item.name] = item.value;
            }
        });
        data['ids'] = ids;
        $.ajax({
            url: route('admin.enrollees.enrollment').url(),
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if (data.success) {
                    swal({
                        title: 'Успешно',
                        html: data.message,
                        type: 'success'
                    }).then(() => {
                        EnrolleesTable.draw();
                        $('#enrollment-modal').modal('hide');
                    })
                } else {
                    swal({
                        title: 'Ошибка',
                        html: data.message,
                        type: 'error'
                    })
                }
            },
            error: function (data) {
                swal({
                    title: 'Ошибка',
                    html: data,
                    type: 'error'
                })
            }
        });

    })

    $('.export-enrollee-js').on('click', function () {
        let button = $(this);
        let route = button.data('route');
        let form = $("form#enrolees-report-filter");
        form.attr('action', route);
        form.trigger('submit');
        console.log(route);
    });

    $('.build-enrollee-report-js').on('click', function (event) {
        event.preventDefault();

        let form = $("form#enrolees-report-filter");

        let columns = {};

        let data = {};
        $.each(_.first(form), function (key, form_item) {
            if (form_item.type === 'checkbox') {
                if (form_item.name === 'columns[]') {
                    columns[form_item.value] = form_item.checked;
                }
                else {
                    data[form_item.name] = form_item.checked;
                }
            } else {
                data[form_item.name] = form_item.value == '' ? null : form_item.value;
            }
        });

        data['columns'] = columns;
        $.ajax({
            url: route('admin.reports.enrollees.data').url(),
            data,
            type: 'POST',
            success: function (response) {
                if (response.success) {
                    $('.noreport').css({'display': 'none'});
                    $('.report').css({'display': 'block'});
                    $('.export-enrollee-report-js').removeClass('hide');
                    $('.export-enrollee-report-to-excel-js').removeClass('hide');

                    $('.report-content').html(response.data);
                    //$('.count-enrollees-js').html(response.count)
                } else {
                    $('.noreport').css({'display': 'block'});
                    $('.report').css({'display': 'none'});
                    $('.noreport-title').html('Нет результатов');
                    $('.noreport-text').html(response.message);
                    $('.export-enrollee-report-js').addClass('hide')
                }

            },
            error: function () {
                $('button').removeAttr('disabled');
                $('select').removeAttr('disabled');
                $('input').removeAttr('disabled');
            }
        })

    });

    let addEnroleeForm = $('form#enrollee-add-form');

    addEnroleeForm.validate({
        rules: {
            lastname: {
                required: true
            },
            firstname: {
                required: true
            },
            profession: {
                required: true
            },
            birthday: {
                required: true
            },
            phone: {
                required: true,
                maxlength: 24
            },
            snils: {
                minlength: 11,
                maxlength: 11
            },
            "passport[documentType]": {
                required: true
            },
            "passport[series]": {
                maxlength: 8
            },
            "passport[number]": {
                required: true,
                maxlength: 16
            },
            "passport[issuanceDate]": {
                required: true
            },
            "passport[subdivisionCode]": {
                required: true
            },
            "passport[issued]": {
                required: true,
                maxlength: 128
            },
            "passport[birthPlace]": {
                required: true,
                maxlength: 128
            },
            //has_original
            graduation_organization_name: {
                required: {
                    param: true,
                    depends: function (element) {
                        return $("#has_original").is(":checked");
                    }
                },
                maxlength: 256
            },
            graduation_organization_place: {
                required: {
                    param: true,
                    depends: function (element) {
                        return $("#has_original").is(":checked");
                    }
                },
                maxlength: 256
            }
        },
        messages: {
            "passport[series]": {
                maxlength: 'Серия документа не может быть больше 8 символов'
            },
            "passport[number]": {
                maxlength: 'Номер документа не может быть больше 16 символов'
            },
            "passport[issued]": {
                required: 'Необходимо указать, кем выдан документ',
                maxlength: 'Данное поле не может быть больше 128 символов'
            },
            "passport[birthPlace]": {
                required: 'Необходимо указать место рождения',
                maxlength: 'Данное поле не может быть больше 128 символов'
            },
            snils: {
                minlength: 'Длина СНИЛС не может быть меньше 11 символов'
            },
            lastname: {
                required: 'Необходимо указать фамилию'
            },
            firstname: {
                required: 'Необходимо указать имя'
            },
            profession: {
                required: 'Необходимо выбрать профессию'
            },
            birthday: {
                required: 'Укажите дату рождения'
            },
            phone: {
                required: 'Укажите контактный телефон'
            },
            graduation_organization_name: {
                required: 'Необходимо указать наименование предыдущей образовательной организации'
            },
            graduation_organization_place: {
                required: 'Необходимо указать населенный пункт предыдущей образовательной организации'
            }
        }
    });

    addEnroleeForm.on('submit', function (event) {
        event.preventDefault();
        let form = $(this);
        let valid = form.data('validator');
        let countInvalidElements = Object.values(valid.invalid).filter(v => v).length;
        if (countInvalidElements) {
            console.log('Форма не валидна');
            return false;
        }

        let url = form.attr('action');
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'post',
            success: function (response) {
                if (response.success) {
                    swal({
                        title: 'Успешно',
                        type: 'success',
                        html: `<b>${response.message}</b>`,
                        confirmButtonText: 'Добавить еще абитуриента',
                        cancelButtonText: 'Отмена',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        showCancelButton: true,
                        buttonsStyling: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33'
                    }).then(({value}) => {
                        if (value) {
                            _.first(form).reset();
                            $('.graduation_organization_name-js').css({'display': 'none'});
                            getCodes();
                            // $('#place_of_stay').css({'display': 'block'});
                            // $('#residential').css({'display': 'block'});
                            // setTimeout(function () {
                            //     getCodes();
                            // })
                        } else {
                            _.first(form).reset();
                            let url = route('admin.enrollees');
                            document.location.href = url.url();
                        }
                    });
                } else {
                    swal({
                        title: 'Внимание!',
                        html: `<b>${response.message}</b>`,
                        type: 'info'
                    })
                }

            },
            error: (response) => {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }

        })
    });

    function buildRow(tableRow) {
        let row = [];
        $.each(tableRow, function (key, column) {
            let columnObj = {
                text: column.innerText,
                alignment: 'center'
            };
            row.push(columnObj)
        });

        return row;
    }

    $('#issuanceDate').datepicker(datepicker_conf());

    body.on('click', '.edit-passport-js', function () {
        $('.save-passport-js').css({'display': 'block'});
        $('.exit-passport-js').css({'display': 'block'});
        $('.edit-passport-js').css({'display': 'none'});
        $('.passport-data').css({'display': 'none'});
        $('.passport-form').css({'display': 'block'});
    });

    body.on('click', '.save-passport-js', function () {
        $('form#edit-passport-form').trigger('submit');
    });

    body.on('submit', 'form#edit-passport-form', function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                $('.passport-js').html(response.template);
                $('.edit-passport-js').css({'display': 'block'});
                $(".save-passport-js").css({'display': 'none'});
                $('.passport-data').css({'display': 'block'});
                $('.passport-form').css({'display': 'none'});
                $('.exit-passport-js').css({'display': 'none'});
                getCodes();
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    });

    body.on('click', '.exit-passport-js', function () {
        $('.edit-passport-js').css({'display': 'block'});
        $(".save-passport-js").css({'display': 'none'});
        $('.passport-data').css({'display': 'block'});
        $('.passport-form').css({'display': 'none'})
        $(this).css({'display': 'none'});
    });

    let updatePassportForm = $('form#edit-passport-form');

    updatePassportForm.validate({
        rules: {
            number: {
                required: true
            },
            issuanceDate: {
                required: true
            },
            subdivisionCode: {
                required: true
            },
            issued: {
                required: true
            },
            birthPlace: {
                required: true
            }
        }
    });

    body.on('click', '.edit-education-js', function () {
        $('.save-education-js').css({'display': 'block'});
        $('.edit-education-js').css({'display': 'none'});
        $('.education-data').css({'display': 'none'});
        $('.education-form').css({'display': 'block'});
    });

    body.on('click', '.save-education-js', function () {
        $('form#edit-enroll-education-form').trigger('submit');
    });

    body.on('submit', 'form#edit-enroll-education-form', function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                $('.education-js').html(response.template);
                $('.edit-education-js').css({'display': 'block'});
                $(".save-education-js").css({'display': 'none'});
                $('.education-data').css({'display': 'block'});
                $('.education-form').css({'display': 'none'});
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    });

    body.on('click', '.edit-additional-js', function () {
        $('.save-additional-js').css({'display': 'block'});
        $('.edit-additional-js').css({'display': 'none'});
        $('.additional-data').css({'display': 'none'});
        $('.additional-form').css({'display': 'block'});
    });

    body.on('click', '.save-additional-js', function () {
        $('form#edit-additional-form').trigger('submit');
    });

    body.on('submit', 'form#edit-additional-form', function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();
        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                $('.additional-js').html(response.template);
                $('.edit-additional-js').css({'display': 'block'});
                $(".save-additional-js").css({'display': 'none'});
                $('.additional-data').css({'display': 'block'});
                $('.additional-form').css({'display': 'none'});
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    });

    body.on('click', '.edit-addresses-js', function () {
        $('.edit-addresses-js').css({'display': 'none'});
        $('.addresses-data').css({'display': 'none'});
        $('.addresses-form').css({'display': 'block'});
    });

    body.on('click', 'input#isAddressSimilar', function () {
        let elem = $(this);
        if (elem.prop('checked')) {
            $('div#residential').hide();
        } else {
            $('div#residential').show();
        }
    });

    body.on('click', 'input#isAddressPlaceOfStay', function () {
        let elem = $(this);
        if (elem.prop('checked')) {
            $('div#place_of_stay').hide();
            $('div#place_of_stay input').hide();
        } else {
            $('div#place_of_stay').show();
            $('div#place_of_stay input').show();
        }
    });

    body.on('submit', 'form#update-addresses-form', function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();
        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                //console.log(response.hh);
                $('.addresses-js').html(response.template);
                $('.edit-addresses-js').css({'display': 'block'});
                $('.addresses-data').css({'display': 'block'});
                $('.addresses-form').css({'display': 'none'});
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    });

    body.on('click', '.add-parent-js', function () {
        $(this).toggle();
        $('.parents-form').css({'display': 'block'});
        $('.parents-data').css({'display': 'none'});
    });

    body.on('click', '.close-add-parent-form-js', function (event) {
        event.preventDefault();
        $('.parents-form').toggle();
        $('.add-parent-js').toggle();
        $('.parents-data').toggle();
    })

    let addParentForm = $('form#add-parents-form');

    addParentForm.validate({
        rules: {
            lastname: {
                required: true
            },
            firstname: {
                required: true
            },
            gender: {
                required: true
            },
            relationshipType: {
                required: true
            }
        }
    });

    body.on('submit', 'form#add-parents-form', function (event) {
        event.preventDefault();

        let form = $(this);
        let url = form.attr('action');
        let data = form.serialize();

        $.ajax({
            url,
            data,
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {
                //console.log(response.hh);
                $('.parents-js').html(response.template);
                //$('.parents-form').css({'display': 'none'});
                $('.add-parent-js').toggle();
                //$('.parents-data').toggle({'display': 'block'});
            },
            error: function (response) {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        });
    })
});