$(function () {
    let metodicalTable = $('table#program-metodicals');
    let url = metodicalTable.data('url');

    let metodicalDT = metodicalTable.DataTable({
        dom: "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        colReorded: false,
        ajax: {
            url,
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },
        columns: [
            {data: 'id', name: 'id', sorting: false, sortable: false},
            {data: 'name', name: 'name'},
            {data: 'file', name: 'file'},
            {data: 'actions', name: 'actions', sorting: false, sortable: false}
        ]
    });

    $('form#add-metodical-form').on('submit', function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let data = new FormData();
        $.each(form[0], function (key, value) {
            if (value.type === 'file') {
                data.append('file', value.files[0]);
            } else {
                data.append(value.name, value.value)
            }
        });

        $.ajax({
            url,
            type: 'POST',
            data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                metodicalDT.draw();
                form[0].reset();
            },
            error: (response) => {
                swal({
                    title: 'Произошла ошибка',
                    html: response.responseJSON.message,
                    type: 'error'
                })
            }
        })
    })
});