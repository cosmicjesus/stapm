$('.btn--filter').on('click', function(event) {
    event.preventDefault();
    let target = $(this).data('target');
    $('.btn--filter').removeClass('btn--active');
    $('.tab').removeClass('active');
    $(target).addClass('active');
    $(this).addClass('btn--active');
});