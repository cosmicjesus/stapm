$(function () {
    async function initAutacomploete() {
        try {
            const response = await axios.get(route('order-references.data').url());
            let studentData = response.data;
            $("#student-lastname").easyAutocomplete({
                data: studentData.lastnames,
                list: {
                    maxNumberOfElements: 10,
                    match: {
                        enabled: true
                    }
                }
            });

            $("#student-firstname").easyAutocomplete({
                data: studentData.firstnames,
                list: {
                    maxNumberOfElements: 10,
                    match: {
                        enabled: true
                    }
                }
            });

            $("#student-middlename").easyAutocomplete({
                data: studentData.middlenames,
                list: {
                    maxNumberOfElements: 10,
                    match: {
                        enabled: true
                    }
                }
            });
        } catch (error) {
            console.error(error);
        }
    }

    initAutacomploete();
    $('form.order-references').on('submit', function (event) {
        event.preventDefault();
        let form = $(this);
        let data = form.serialize();

        //return false;
        $.ajax({
            url: route('order-reference.order').url(),
            method: "POST",
            data,
            beforeSend: function () {
                $('button').prop('disable', true);
                $('input').prop('disable', true);
                $('select').prop('disable', true);

            },
            success: function (response) {
                $('button').prop('disable', false);
                $('input').prop('disable', false);
                $('select').prop('disable', false);
                console.dir(response);
                if (response.success) {
                    $('.order-error').html(response.tpl);
                    document.getElementById('order-references').reset();
                    $('.type-training-js').hide();
                    $('.type-money-js').hide();
                    setTimeout(function () {
                        $(document).find('.alert').remove()
                    }, 10000)
                } else {
                    $('.order-error').html(response.tpl)
                }
            },
            error: function (error) {
                let errors = error.responseJSON.errors;
                let errorArray = [];
                _.map(errors, function (item) {
                    errorArray.push(item);
                });
                errorArray = _.flatten(errorArray);
                let errorAlert = document.createElement('div');
                errorAlert.classList = 'alert alert-danger text-center';
                let list = "<ul>";
                errorArray.forEach(function (item) {
                    list += `<li>${item}</li>`
                });
                list += "</ul>";
                errorAlert.innerHTML = list;

                $('.order-error').html(errorAlert);
                $('button').prop('disable', false);
                $('input').prop('disable', false);
                $('select').prop('disable', false);
            }
        })
    });

    $('.type-checkbox-js').on('click', function (event) {
        let element = $(this);
        let value = element.val();

        $(`.type-${value}-js`).toggle();
        console.log(element.val());
    });
});