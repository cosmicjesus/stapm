require('./menu');
require('./mobile-menu');
require('./data-tables');
require('./special-settings');
require('./tabs');
require('./references');

console.log('initial');

let owl = $(".owl-carousel").owlCarousel({
    items: 1,
    slideSpeed: 500,
    autoplay: true,
    autoplayTimeout: 4500,
    loop: true,
    mouseDrag: true,
    singleItem: true,
    dots: true,
    margin: 85,
    itemElement: 'div',
    stageElement: 'div',
    dotsClass: 'owl-dots',
}).data('owlCarousel');
$('#tabs').tabulous();

