let main_menu_item = $('.menu_items_item');
$('.menu_items_item_link').on('click', function (event) {
    event.preventDefault();
})
// main_menu_item.on('mouseenter', function () {
//     let item = $(this);
//     let link = item.find('.menu_items_item_link');
//     link.addClass('menu_items_item_link--hover');
//     let sub_menu = item.find('.menu_items_item_sub_items');
//     sub_menu.addClass('menu_items_item_sub_items--active');
// });
//
// main_menu_item.on('mouseleave', function () {
//     let item = $(this);
//     let link = item.find('.menu_items_item_link');
//     link.removeClass('menu_items_item_link--hover');
//     let sub_menu = item.find('.menu_items_item_sub_items');
//     sub_menu.removeClass('menu_items_item_sub_items--active');
// });

$('.accordion_head').on('click', function (event) {
    event.preventDefault();
    let parent = $(this).parents('.accordion');
    $('.accordion_body').removeClass('open');
    $('.accordion_icon').removeClass('fa-chevron-down').addClass('fa-chevron-right');
    parent.find('.accordion_body').addClass('open');
    parent.find('.accordion_icon').addClass('fa-chevron-down');
});