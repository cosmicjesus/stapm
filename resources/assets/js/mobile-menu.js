let menu=$('.mobile-menu');
let body=$('body');
let button=$('.mobile-menu-js');
let width=document.body.clientWidth;
let menuLink=$('.mobile-menu__link-js');
let open=()=>{
    body.addClass('no-scroll');
    body.css({
        'overflow':'hidden',
        'width':document.body.clientWidth
    })
    menu.addClass('mobile-menu--active');
}

let close =()=>{
    body.removeClass('no-scroll');
    body.css({
        'overflow':'visible',
        'width':'auto'
    })
    menu.removeClass('mobile-menu--active');
}

button.click((e)=>{
    e.preventDefault();
    menu.hasClass('mobile-menu--active')?close():open();
});

let closeSub=(element,subBlock)=>{
    element.removeClass('mobile-menu__sub--active');
    element.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
    subBlock.slideUp(400);
};

let openSub=(element,subBlock)=>{
    element.addClass('mobile-menu__sub--active');
    element.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
    subBlock.slideDown(400);
};

menuLink.click(function(e){
   let elem=$(this);
   let open=elem.hasClass('mobile-menu__sub--active');
   let sub=elem.parents('.mobile-menu__items-item-js').find('.mobile-menu__sub');
   open?closeSub(elem,sub):openSub(elem,sub);
});