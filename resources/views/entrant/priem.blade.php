@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title') на {{\Carbon\Carbon::now()->format('d.m.Y')}}</h1>
    @if(count($programs->getPrograms())>0)
        <div class="table-scroll">
            <table class="table table-striped employees-table">
                <thead>
                <tr>
                    <th>Программа</th>
                    <th>План приема</th>
                    <th>Подано заявлений без оригинала документа</th>
                    <th>Подано заявлений с оригиналом документа</th>
                </tr>
                </thead>
                <tbody>
                @foreach($programs->getPrograms() as $program)
                    <tr>
                        <td><a class="education_link"
                               href="{{$program->getDetailUrl()}}">{{$program->getNameWithForm()}}</a></td>
                        <td>{{$program->getPriem()->getReceptionPlan()}}</td>
                        <td>{{$program->getCountEnrolleesWithOutDocuments(true)}}</td>
                        <td>{{$program->getCountEnrolleesWithDocuments(true)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Всего:</td>
                    <td>{{$programs->getCountAll()}}</td>
                    <td>{{$programs->getCountWithOutDocuments()}}</td>
                    <td>{{$programs->getCountWithDocuments()}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @else
        <h2>Отсутствуют программы для набора студентов</h2>
    @endif

    @if(\App\Helpers\Settings::realAvgRait())
        {{Widget::run('App\Widgets\RealAvgRaiting')}}
    @else
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['enrollment_order']])}}
    @endif
@endsection