@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title') на {{\Carbon\Carbon::now()->format('d.m.Y')}}</h1>
    <div class="raiting">
        @if(isset($enrollees['regular']))
            @include('entrant.inc.table',['enrollees'=>$enrollees['regular'],'isContract'=>false])
        @endif
        @if(isset($enrollees['contract']))
            @include('entrant.inc.table',['enrollees'=>$enrollees['contract'],'isContract'=>true])
        @endif
    </div>
@endsection