<footer class="footer">
    <div class="footer_wrapper content">
        {{Widget::Menu(['template'=>'footer'])}}
        <div class="footer_informations">
            <div class="footer_informations_main">
                <p class="footer_informations_main_college_name">государственное бюджетное профессиональное
                    образовательное учреждение Самарской области «Самарский техникум авиационного и промышленного
                    машиностроения имени Д.И. Козлова», 1954-{{\Carbon\Carbon::now()->format('Y')}}</p>
            </div>
            <div class="footer_informations_contact">
                <p class="footer_informations_contact_text">Самара, Старый переулок, д.6<br/>
                    <b> 8(846) 955-22-20 – </b>приемная
                    директора<br>
                    <b>8(846) 955-22-11 – </b>приемная комиссия<br>
                    <b>8(846) 955-08-14</b> – заместитель директора по УОР<br>
                    <b>8(846) 228-55-86</b> – общежитие<br></p>
            </div>
        </div>
        <div class="footer_repository">
            {{Widget::GitInfo(['template'=>'client'])}}
        </div>
    </div>
</footer>
{{Widget::Menu(['template'=>'mobile'])}}
<span class="menu-hamburger mobile-menu-js"><i class="fa fa-bars"></i></span>
@if(env('APP_MODE')=='develop')
    <script type="text/javascript" src="/js/vendor.js?{{rand(1,9999999)}}"></script>
    <script type="text/javascript" src="/js/main.js?{{rand(1,9999999)}}"></script>
@else
    <script type="text/javascript" src="/js/vendor.js?{{lastCommitId()}}"></script>
    <script type="text/javascript" src="/js/main.js?{{lastCommitId()}}"></script>
@endif
<script>
    $(document).ajaxSend(function () {

        $('button').attr('disabled', 'disabled');
        $('select').attr('disabled', 'disabled');
        $('input').attr('disabled', 'disabled');
    });
    $(document).ajaxComplete(function () {
        $('button').removeAttr('disabled');
        $('select').removeAttr('disabled');
        $('input').removeAttr('disabled');

    });
</script>
