<div class="special-settings">
    <div class="content">
        <div class="special-settings_wrapper">
            <div class="special-settings_block setting-fontsize">
                <span class="special-settings_block-name">Размер:</span>
                <a href="#" class="setting-fontsize_small" data-fontsize="small" title="Маленький размер шрифта">А</a>
                <a href="#" class="setting-fontsize_normal a-current" data-fontsize="normal"
                   title="Нормальный размер шрифта">А</a>
                <a href="#" class="setting-fontsize_big" data-fontsize="big" title="Большой размер шрифта">А</a>
            </div>
            <div class="special-settings_block setting-color">
                <span class="special-settings_block-name">Цвет:</span>
                <a href="#" class="setting-color_black a-current" data-color="black" title="Черным по белому">
                    <span class="setting-color_text">А</span>
                </a>
                <a href="#" class="setting-color_yellow" data-color="yellow" title="Желтым по черному">
                    <span class="setting-color_text">А</span>
                </a>
                <a href="#" class="setting-color_blue" data-color="blue" title="Большой размер шрифта">
                    <span class="setting-color_text">А</span>
                </a>
            </div>
            <div class="special-settings_block setting-image">
                <span class="special-settings_block-name">Изображения</span>
                <a href="#" class="setting-image_link setting-image_grayscale" data-image="grayscale"
                   title="Черно-белые изображения">ЧБ</a>
                <a href="#" class="setting-image_link setting-image_on a-current" data-image="on"
                   title="Изображения вкл">Вкл</a>
                <a href="#" class="setting-image_link setting-image_off" data-image="off"
                   title="Изображения выкл">Выкл</a>
            </div>
            <span class="special-settings_block"><a href="/?set-aa=normal" data-spec-off><i
                            class="fa fa-long-arrow-left" aria-hidden="true"></i> Обычная версия сайта</a></span>
        </div>
    </div>
</div>
@if(\App\Helpers\Settings::ShowOldSiteMessage())
    <div class="top-message">
        <div class="content">
            <h1>Это новая версия сайта{{env('OLD_SITE_MESSAGE')}}</h1><a href="https://old.stapm.ru">Старая
                версия</a><br>
        </div>
    </div>
@endif
<header>
    <div class="top_section" itemscope itemtype="http://schema.org/Organization">
        <div class="top_section_wrapper content">
            <div class="top_section_address">
                <a class="top_section_address_link" href="{{route('basic-information')}}#map" title="Адрес техникума. Нажмите, чтобы открыть карту">
                    <i class="fa fa-map-marker top_section_address_link_marker"></i>
                    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="postalCode">443052</span>
                        <span itemprop="addressLocality">Самара</span>,
                        <span itemprop="streetAddress">Старый переулок 6</span>
                    </span>
                </a>
            </div>
            <div class="top_section_buttons"><a class="top_section_buttons_item special-vision-js" href="#"
                                                title="Версия сайта для слабовидящих">
                    <i class="fa fa-low-vision"></i></a>
                {{--<span class="top_section_buttons_item top_section_buttons_item__mobile_menu mobile-menu-js"><i class="fa fa-bars"></i></span></div>--}}
            </div>
        </div>
        <div class="header">
            <div class="header_wrapper content">
                <div class="header_main_info">
                    <a class="header_main_info_logo" href="{{route('home')}}">
                        <img class="header_main_info_logo_img"
                                                                                   src="/img/logo.gif"></a>
                    <a class="header_main_info_college_name" href="{{route('home')}}">
                        <span class="h1"
                              itemprop="name">Государственное бюджетное профессиональное образовательное учреждение Самарской области<br> «Самарский техникум авиационного и промышленного машиностроения<br/>
                            имени Д.И.Козлова»</span>
                    </a></div>
                <div class="header_contact_info">
                    <div class="header_contact_info_enrollment"><i
                                class="fa fa-clock-o fa-2x header_contact_info_enrollment_clock_icon"></i>
                        <div class="header_contact_info_enrollment_text"><a
                                    class="header_contact_info_enrollment_text_link" href="{{route('admission')}}">Приемная
                                комиссия</a>
                            <br/>
                            <time itemprop="openingHours" datetime="Mo,Fr 9:00−17:00">9:00 до 17:00 Пн-Пт</time><br>
                        </div>
                    </div>
                    <div class="header_contact_info_phones">
                        {{--<i class="fa fa-phone fa-2x header_contact_info_phones_icon fa-rotate-270"></i>--}}
                        <div class="header_contact_info_phones_text">
                            <b><span itemprop="telephone">8(846) 955-22-20</span></b> – приемная директора<br>
                            <b><span itemprop="telephone">8(846) 955-22-11</span></b> – приемная комиссия<br>
                            <b><span itemprop="telephone">8(846) 955-08-14</span></b> – заместитель директора по УОР<br>
                            <b><span itemprop="telephone">8(846) 228-55-86</span></b> – общежитие<br>
                            <b>e-mail:</b>
                            <a href="mailto:cpostapm@mail.ru">
                                <span itemprop="email">cpostapm@mail.ru</span>
                            </a><br>
                            <b>e-mail приемной комиссии:</b><br>
                            <a href="mailto:cpostapm@mail.ru"><span itemprop="email">priem@stapm.ru</span></a>
                            {{--<br><b>Группа</b> <a--}}
                            {{--href="https://vk.com/stapmvk" target="_blank">в контакте</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{Widget::Menu(['template'=>'top'])}}
    </div>
</header>