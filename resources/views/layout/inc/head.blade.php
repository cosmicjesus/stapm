<head>
    @if(Route::currentRouteName()=='home')
        <title>{{env('COLLEGE_NAME')}}</title>
    @else
        <title>@yield('title') | {{env('COLLEGE_NAME')}}</title>
    @endif
    @routes
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="5d31af80892b0a40"/>
    <meta name="robots" content="index, nofollow"/>
    <meta name="yandex" content="noyaca"/>
    <meta name="copyright" lang="ru" content="{{env('COLLEGE_NAME')}}"/>
    <meta charset="UTF-8">
    {!! Meta::tag('description') !!}
    {!! Meta::tag('keywords') !!}
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href="/css/vendor.css?{{rand(1,9999999)}}" rel="stylesheet" type="text/css">
    <link href="/css/style.css?{{rand(1,9999999)}}" rel="stylesheet" type="text/css">
</head>