<!DOCTYPE html>
<html>
    @include("layout.inc.head")
    <body>
    @include("layout.inc.header")
    @yield('content')
    @include("layout.inc.footer")
    </body>
</html>