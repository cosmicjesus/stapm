<!DOCTYPE html>
<html>
    @include("layout.inc.head")
    <body>
    @include("layout.inc.header")
    <div class="container-main content">
        {{Widget::Menu(['isInner'=>true])}}
        <div class="container">
            {!! Breadcrumbs::render() !!}
            <div class="information">
                @yield('content')
            </div>
        </div>
    </div>
    @include("layout.inc.footer")
    </body>
</html>