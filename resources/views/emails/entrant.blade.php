<html lang="ru">
<head>
    <title>Уведомление об успешной подаче документов в СТАПМ</title>
</head>
<body>
<div class="content" style="display:flex;align-content: center;width: 100%">
    <div class="email-body"
         style="width:500px;margin:0 auto;align-self: center;border: 15px solid #157fc4;background: #ffffff">
        <div class="img" style="margin-top: 10px">
            <img src="https://stapm.ru/img/default-news-img.jpg" alt="Лого" style="margin: 0 auto;
    display: block;
    width: 100px;">
        </div>
        <div class="text" style="padding: 10px;text-align: justify">
            <h4 style="text-align: center">@if($entrant->gender==1)Уважаемая@elseУважаемый@endif {{$entrant->lastname}} {{$entrant->firstname}} {{$entrant->middlename}}</h4>
            <p>Вы подали документы в {{env('COLLEGE_NAME')}}, для участия в конкурсе аттестатов по профессии/специальности.</p>
            <p>Номер вашего заявления <b>{{$entrant->id}}</b>.</p>
            <p>Догрузить недостающие документы Вы можете в соответствующей форме, перейдя по <a target="_blank" href="{{route('client.uploading-documents')}}">ссылке</a></p>
            <p>Если у вас возникнут какие-либо вопросы, Вы можете позвонить по телефонам <b>955-22-11, 955-08-14</b> или задать их на нашем сайте в форме Онлайн чата
                , а так же отправив письмо на priem@stapm.ru, c указанием номера вашего заявления в теме письма.</p>
        </div>
    </div>
</div>
</body>
</html>
