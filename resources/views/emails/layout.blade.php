<html lang="ru">
<head>
    <title>@yield('title')</title>
</head>
<body>
<div class="content" style="display:flex;align-content: center;width: 100%;max-width: 600px">
    <div class="email-body"
         style="width:500px;margin:0 auto;align-self: center;border: 15px solid #157fc4;background: #ffffff">
        <div class="img">
            <img src="https://stapm.ru/img/default-news-img.jpg" alt="Лого" style="margin: 0 auto;
    display: block;
    width: 100px;">
        </div>
        <div class="text" style="padding: 10px;text-align: justify">
            @yield('content')
        </div>
    </div>
</div>
</body>
</html>
