@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title') на {{\Carbon\Carbon::now()->format('d.m.Y')}}</h1>
    @foreach($programs as $program)
        <h2 class="documents-title">{{$program->getNameWithForm()}}</h2>
        @if(count($program->getActiveGroups()))
            <div class="table-scroll">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>Группа</th>
                        <th>Курс</th>
                        <th>Максимальное кол-во человек</th>
                        <th>Кол-во вакантных мест</th>
                    </tr>
                    @foreach($program->getActiveGroups() as $group)
                        <tr>
                            <th>{{$group->getName()}}</th>
                            <th>{{$group->getCourse()}}</th>
                            <th>{{$group->getMaxPeople()}}</th>
                            <th>{{$group->getCountVacancyTransfer()}}</th>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    @endforeach
@endsection