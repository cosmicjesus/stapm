@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    @if(count($programs)>0)
        <div class="table-scroll">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th>Профессия/специальность</th>
                    <th>Форма</th>
                    <th>Срок действия аккредитации</th>
                    <th>Срок обучения</th>
                    <th></th>
                </tr>
                @foreach($programs as $program)
                    <tr>
                        <td><a class="education_link" href="{{$program->getDetailUrl()}}">{{$program->getName()}}</a>
                        </td>
                        <td>{{$program->getEducationForm()}}</td>
                        <td>{{$program->getLicence()}}</td>
                        <td>{{$program->getPeriod()}}</td>
                        <td><a class="btn btn--blue" href="{{$program->getUmkUrl()}}">УМК</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <h2>Профессиональные программы отсутствуют</h2>
    @endif
    <ul>
        <li>
            <a href="/documents/info_o_priem.pdf?{{rand(1,200)}}" target="_blank">Информация о результатах приема на 01.10.2018</a>
        </li>
        <li>
            <a href="/documents/info_o_perevod.pdf?{{rand(1,200)}}" target="_blank">Информация о результатах перевода, восстановления и
                отчисления на 01.10.2018</a>
        </li>
    </ul>
@endsection