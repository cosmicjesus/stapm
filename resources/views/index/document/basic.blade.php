@foreach($categories as $category)
    <h2 class="documents-title">{{ $category->getName() }}</h2>
    @if($category->hasActiveFiles())
        <ul>
            @foreach($category->getActiveFiles() as $file)
                <li><a href="{{$file->getPath()}}" class="link" target="_blank">{{$file->getName()}}</a></li>
            @endforeach
        </ul>
    @endif
    @each('index.document.sub',$category->getChilds(),'category')
@endforeach