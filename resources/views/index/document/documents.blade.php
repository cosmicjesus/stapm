@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    @if(count($categories))
        @include('index.document.basic',['categories'=>$categories])
    @else
        <h2>Документы отсутствуют</h2>
    @endif
    <h2 class="documents-title">Предписания органов осуществляющих надзор</h2>
    <div id="tabs" class="tabs">
        <ul class="tabs_header">
            @foreach($yearList as $item)
                <li class="tabs_header_item">
                    <a href="#{{$item}}" title="" class="tabs_header_link">{{$item}} год</a>
                </li>
            @endforeach
        </ul>
        @if(count($regulations))
            <div class="tabs_container" id="tabs_container">
                @foreach($regulations as $key=> $regulation)
                    <div id="{{$key}}">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Наименование надзорного органа</th>
                                <th>Документ, отражающий результаты проверки:название (акт, предписание, требование и
                                    др.), дата, номер
                                </th>
                                <th>Нарушения, выявленные в результате проверки</th>
                                <th>Меры, принятые для устранения нарушения, результаты</th>
                            </tr>
                            @foreach($regulation as $item)
                                <tr>
                                    <td rowspan="{{$item->getCountViolations()>0?$item->getCountViolations():1}}">{{$item->getNameOfTheSupervisory()}}</td>
                                    <td rowspan="{{$item->getCountViolations()>0?$item->getCountViolations():1}}">
                                        @if($item->getCountFiles())
                                            <ul>
                                                @foreach($item->getFiles() as $file)
                                                    <li><a href="{{$file->getPath()}}">{{$file->getName()}}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                        {{$item->getComment()}}
                                    </td>
                                    @if($item->getCountViolations())
                                        <td>{{$item->getFirstViolation()->getNameOfViolation()}}</td>
                                        <td>{{$item->getFirstViolation()->getResult()}}
                                            <br>
                                            <ul>
                                                @foreach($item->getFirstViolation()->getFiles() as $file)
                                                    <li><a href="{{$file->getPath()}}" target="_blank">{{$file->getName()}}</a></li>
                                                @endforeach
                                            </ul>

                                        </td>
                                    @else
                                        <td>Нарушений нет</td>
                                        <td>-</td>
                                    @endif
                                </tr>
                                @if($item->getCountViolations()>1)
                                    @foreach($item->getViolations() as $value)
                                        <tr>
                                            <td>{{$value->getNameOfViolation()}}</td>
                                            <td>{{$value->getResult()}}
                                                <br>
                                                <ul>
                                                @foreach($value->getFiles() as $file)
                                                    <li><a href="{{$file->getPath()}}" target="_blank">{{$file->getName()}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                        </table>
                    </div>
                @endforeach
            </div>
        @endif
    </div>

@endsection