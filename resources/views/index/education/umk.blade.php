@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    @if($program->getSubjects()->count()>0)
        <h4>Рабочие программы</h4>
        <div class="programs">
            <ul class="list list--no-list-style no-padding--left">
                @foreach($program->getSubjects() as $subject)
                    <li class="list-item">
                        <div class="accordion">
                            @if($subject->getFiles())
                                <div class="accordion_head">
                                    <a href="#" class="accordion_link">
                                        <i class="fa fa-chevron-right accordion_icon"></i>{{$subject->getName()}}</a>
                                </div>
                                <div class="accordion_body">
                                    <div class="files">
                                        <ul class="list">
                                            @foreach($subject->getFiles() as $file)
                                                <li><a href="{{$file->getFileUrl()}}"
                                                       target="_blank">{{$file->getTitle()}}</a></li>
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>

                        </div>
                        @else
                            {{$subject->getName()}}
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div id="metodical-block">
            <h4>Методические указания</h4>
            <div class="metodicals">
                @if($program->getMetodicals()->count())
                    <ul class="list">
                        @foreach($program->getMetodicals() as $metodical)
                            <li><a href="{{$metodical->getFileUrl()}}"
                                   target="_blank">{{$metodical->getTitle()}}</a></li>
                        @endforeach
                    </ul>
                @else
                    <h5>Методические указания временно отсутствуют</h5>
                @endif
            </div>
        </div>
    @else
        <h2>У профессиональной программы нет загруженных рабочих программ</h2>
    @endif
@endsection