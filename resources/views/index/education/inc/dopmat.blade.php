<td>{{$cellName}}</td>
<td>
    @if($files)
        <ul class="list list--no-list-style no-padding--left">
            @foreach($files as $file)
                <li class="list-item">
                    <a href="{{$file->getFileUrl()}}" class="list-item-link">{{$file->getTitle()}}</a>
                </li>
            @endforeach
        </ul>
    @else
        Не загруженно
    @endif
</td>