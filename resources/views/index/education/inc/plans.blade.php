<td >Учебные планы</td>
<td >
    @if(count($plans))

        <ul class="list list--no-list-style no-padding--left">
            @foreach($plans as $plan)
                @if($plan->getActiveStatus())
                    @if($plan->getStatus()=='has_file')
                        <li class="list-item">
                            <a class="list-item-link" href="{{$plan->getFilePath()}}"
                               target="_blank">{{$plan->getNameOnDate()}}</a>
                        </li>
                    @elseif($plan->getStatus()=='revision')
                        <li class="list-item">
                            {{$plan->getNameOnDate()}} [На доработке]
                        </li>
                    @elseif($plan->getStatus()=='no_file')
                        <li class="list-item">
                            {{$plan->getNameOnDate()}}
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    @else
        У профессиональной программы нет учебных планов
    @endif
</td>