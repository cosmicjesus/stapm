@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="standarts">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Наименование профессии/специальности</th>
                <th>Ссылка на файл</th>
            </tr>
            @foreach($programs as $program)
                <tr>
                    <td>{{$program->getNameWithForm()}}</td>
                    <td><a href="{{$program->getDocuments()->getStandart()}}" target="_blank">Открыть</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection