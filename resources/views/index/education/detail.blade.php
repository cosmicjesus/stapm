@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    @if(!is_null($program->getDescription()))
        <p>
            {{$program->getDescription()}}
        </p>
    @endif
    <div class="table-scroll">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td>Форма обучения</td>
                <td>{{$program->getEducationForm()}}</td>
            </tr>
            <tr>
                <td>Язык на котором ведется обучение</td>
                <td>Русский</td>
            </tr>
            @if(!is_null($program->getQualification()))
                <tr>
                    <td>Квалификация</td>
                    <td>{{$program->getQualification()}}</td>
                </tr>
            @endif
            <tr>
                <td>Срок действия аккредитации</td>
                <td>{{$program->getLicence()}}</td>
            </tr>
            <tr>
                <td>Срок обучения</td>
                <td>{{$program->getPeriod()}}</td>
            </tr>
            <tr>
                <td>Уровень образования</td>
                <td>Среднее профессиональное образование</td>
            </tr>
            <tr>
                <td>Образовательный стандарт</td>
                <td>
                    @if($program->getDocuments()->getStandart())
                        <a href="{{$program->getDocuments()->getStandart()}}" target="_blank" class="link">Открыть</a>
                    @else
                        Не загруженно
                    @endif
                </td>
            </tr>
            <tr>
                <td>Аннотации рабочих программ</td>
                <td>
                    @if($program->getDocuments()->getAnnotation())
                        <a href="{{$program->getDocuments()->getAnnotation()}}" target="_blank" class="link">Открыть</a>
                    @else
                        Не загруженно
                    @endif
                </td>
            </tr>
            <tr>
                <td>УМК</td>
                <td><a href="{{$program->getUmkUrl()}}" class="link">Открыть</a></td>
            </tr>
            <tr>
                <td>Количество студентов</td>
                <td>{{$program->getCountStudents()}}</td>
            </tr>
            <tr>
                <td>Количество студентов по группам</td>
                <td>
                    @if(count($program->getActiveGroups()))
                        <table class="table table-striped">
                            <tr>
                                <th>Наименование</th>
                                <th>Курс</th>
                                <th>Кол-во студентов</th>
                            </tr>
                            @foreach($program->getActiveGroups() as $group)
                                <tr>
                                    <td>{{$group->getName()}}</td>
                                    <td>{{$group->getCourse()}}</td>
                                    <td>{{$group->getCountStudents()}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </td>
            </tr>
            <tr>
                @include('index.education.inc.plans',['plans'=>$program->getEducationPlans()])
            </tr>
            <tr>
                @include('index.education.inc.dopmat',['files'=>$program->getCalendars(),'cellName'=>'Календарные графики'])
            </tr>
            <tr>
                @include('index.education.inc.dopmat',['files'=>$program->getAkts(),'cellName'=>'Акты согласования'])
            </tr>
            <tr>
                @include('index.education.inc.dopmat',['files'=>$program->getPpssz(),'cellName'=>'ППССЗ/ППКРС'])
            </tr>
            </tbody>
        </table>
        <p>[ДО] - Дуальное обучение</p>
        <p>[АОП] - Адаптированная образовательная программа</p>
    </div>
@endsection