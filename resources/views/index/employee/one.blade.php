@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="employee_info">
        <div class="employee_personal">
            <div class="employee_photo">
                <img src="{{$employee->getPhoto()}}" alt="{{$employee->getFullName()}}">
            </div>
            <div class="employee_personal_work-info table-scroll">
                <table class="table table-striped">
                    <tr>
                        <td>Квалификационная категория</td>
                        <td>{{$employee->getCategory()}}</td>
                    </tr>
                    <tr>
                        <td>Ученая степень</td>
                        <td>{{$employee->getScientificDegree()}}</td>
                    </tr>
                    <tr>
                        <td>Ученое звание</td>
                        <td>{{$employee->getAcademicTitle()}}</td>
                    </tr>
                    <tr>
                        <td>Общий стаж работы</td>
                        <td>{{$employee->getGeneralExperience()}}</td>
                    </tr>
                    <tr>
                        <td>Стаж работы по специальности</td>
                        <td>{{$employee->getSpecialtyExp()}}</td>
                    </tr>
                    <tr>
                        <td>Стаж работы в образовательной организации</td>
                        <td>{{$employee->getExpInCollege()}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="other-info">
            <div class="employee-educations table-scroll">
                <h3>Образование</h3>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Направление подготовки</th>
                        <th>Квалификация</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee->getEducations() as $education)
                        <tr>
                            <td>{{$education->getName()}}</td>
                            <td>{{$education->getDirectionOfPreparation()}}</td>
                            <td>{{$education->getQualification()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="employee-positions table-scroll">
                <h3>Должности</h3>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Период работы</th>
                        <th>Стаж</th>
                        <th>Тип</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee->getPositions() as $position)
                        @if(is_null($position->getLayoffDate()))
                            <tr>
                                <td>{{$position->getName()}}</td>
                                <td>{{$position->getPeriod()}}</td>
                                <td>{{$position->getExp()}}</td>
                                <td>{{$position->getType()}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if(count($employee->getSubjects()))
                <div class="employee-subjects table-scroll">
                    <h3>Преподаваемые дисциплины</h3>
                    <table class="table table-striped">
                        <tbody>
                        @foreach($employee->getSubjects() as $subject)
                            <tr>
                                <td>{{$subject->getName()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            <div class="employee-courses table-scroll">
                <h3>Повышение квалификации</h3>
                <table class="table table-striped" id="qcourses">
                    <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Период прохождения</th>
                        <th>Кол-во часов</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee->getCourses() as $course)
                        <tr>
                            <td>{{$course->getTitle()}}</td>
                            <td>{{$course->getPeriod()}}</td>
                            <td>{{$course->getHours()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection