@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    @if(count($employees)>0)
        <div class="employees">
            @foreach($employees as $employee)
                <div class="employee-card">
                    <div class="employee-card-photo">
                        <img class="employee-photo" src="{{$employee->getPhoto()}}">
                    </div>
                    <div class="employee-card-info">
                        <div class="employee-personal">
                            <a class="employee-personal-link"
                               href="{{$employee->getUrl()}}">{{$employee->getFullName()}}</a>
                        </div>
                        <div class="employee-positions">
                            @foreach($employee->getPositions() as $position)
                                @if(is_null($position->getLayoffDate()))
                                    <p>{{$position->getName()}}</p>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <h2>Сотрудники отсутствуют</h2>
    @endif
@endsection