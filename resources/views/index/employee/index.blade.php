@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <div class="information-top">
        <h1 class="information_title">@yield('title')</h1>
        <div class="button-group">
            <button class="btn btn--blue btn--filter btn--active" data-target="#administration-block">
                Руководство
            </button>
            <button class="btn btn--filter btn--blue" data-target="#employees-block">
                Пед.состав
            </button>
        </div>
    </div>
    <div class="tab active" id="administration-block">
        @if(count($administration)>0)
            <div class="employees">
                @foreach($administration as $employee)
                    <div class="employee-card">
                        <div class="employee-card-photo">
                            <img class="employee-photo" src="{{$employee->getPhoto()}}">
                        </div>
                        <div class="employee-card-info">
                            <div class="employee-personal">
                                <a class="employee-personal-link"
                                   href="{{$employee->getUrl()}}">{{$employee->getFullName()}}</a>
                            </div>
                            <div class="employee-positions">
                                @foreach($employee->getPositions(true) as $position)
                                    @if(is_null($position->getLayoffDate()))
                                        <p style="margin: 2px">{{$position->getName()}}</p>
                                    @endif
                                @endforeach
                                @if($employee->getEmails())
                                    <p style="margin: 2px"><b>Email:</b>{{$employee->getEmails()}}</p>
                                @endif
                                @if($employee->getPhones())
                                    <p style="margin: 2px"><b>Телефон:</b>{{$employee->getPhones()}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <h2>Сотрудники отсутствуют</h2>
        @endif
    </div>

    <div class="tab" id="employees-block">
        @if(count($employees)>0)
            <div class="table-scroll">
                <table class="table table-striped employees-table" id="employees">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Квалификационная категория</th>
                        <th>Должности</th>
                        <th>Общий стаж работы</th>
                        <th>Стаж работы по специальности</th>
                        <th>Ученая степень</th>
                        <th>Ученое звание</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <td><a class="education_link"
                                   href="{{$employee->getUrl()}}">{{$employee->getFullName()}}</a>
                            </td>
                            <td>{{$employee->getCategory()}}</td>
                            <td>
                                <ul class="list list--no-list-style no-padding--left">
                                    @foreach($employee->getPositions(true) as $position)
                                        @if(is_null($position->getLayoffDate()))
                                            <li>{{$position->getName()}}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td>{{$employee->getGeneralExperience()}}</td>
                            <td>{{$employee->getSpecialtyExp()}}</td>
                            <td>{{$employee->getScientificDegree()}}</td>
                            <td>{{$employee->getAcademicTitle()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h2>Сотрудники отсутствуют</h2>
        @endif
    </div>
@endsection