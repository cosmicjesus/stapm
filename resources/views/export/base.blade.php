<table>
    <thead>
    <tr>
        <th>Номер</th>
        @if($type=='bank')
            <th>Код организации</th>
        @endif
        @if($type=='full')
            <th>Специальность</th>
        @endif
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        @if($type=='full')
            <th>Средний балл</th>
        @endif
        <th>Пол</th>
        @if($type=='full')
            <th>Пред. место учебы</th>
            <th>Год окончания</th>
            <th>Кол-во классов</th>
            <th>Язык</th>
        @endif
        <th>Страна</th>
        <th>Индекс</th>
        <th>Регион</th>
        <th>Район</th>
        <th>Город</th>
        <th>Населенный пункт</th>
        <th>Улица</th>
        <th>Дом</th>
        <th>Корпус</th>
        <th>Квартира</th>
        <th>Тип паспорта</th>
        <th>Серия паспорта</th>
        <th>Номер паспорта</th>
        <th>Код подразделения</th>
        <th>Когда выдан</th>
        <th>Кем выдан</th>
        <th>Дата рождения</th>
        <th>Код подразделения</th>
        <th>Место рождения</th>
        <th>Гражданство</th>
        <th>Телефон домашний</th>
        <th>Телефон служебный</th>
        <th>Телефон мобильный</th>
        <th>Адрес электронной почты</th>
        @if($type=='bank')
            <td>Контрольная информация</td>
            <td>Примечание</td>
            <td>Имя в латинской транскрипции</td>
            <td>Фамилия в латинской транскрипции</td>
        @endif
        <th>ИНН</th>
        <th>СНИЛС</th>
        @if($type=='full')
            <th>Социальный статус</th>
            <th>Потребность в общежитии</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <td>{{$loop->iteration}}</td>
            @if($type=='bank')
                <td>{{env('ORGANIZATION_CODE',547)}}</td>
            @endif
            @if($type=='full')
                <td>{{$student->getGroup()->getName()}}</td>
            @endif
            <td>{{$student->getLastName()}}</td>
            <td>{{$student->getFirstName()}}</td>
            <td>{{$student->getMIddlename()}}</td>
            @if($type=='full')
                <td>{{$student->getAvg()}}</td>
            @endif
            <td>{{$student->getGenderToStr()}}</td>
            @if($type=='full')
                <td>{{$student->getGraduationOrganizationName()}}</td>
                <td>{{$student->getGraduationDate('Y')}}</td>
                <td>9</td>
                <td>{{$student->getLanguage()}}</td>
            @endif
            <td>{{$student->getNationality()}}</td>
            <td>{{$student->getAddresses('registration','index')}}</td>
            <td>{{$student->getAddresses('registration','region')}}</td>
            <td>{{$student->getAddresses('registration','area')}}</td>
            <td>{{$student->getAddresses('registration','city')}}</td>
            <td>{{$student->getAddresses('registration','settlement')}}</td>
            <td>{{$student->getAddresses('registration','street')}}</td>
            <td>{{$student->getAddresses('registration','house_number')}}</td>
            <td>{{$student->getAddresses('registration','housing')}}</td>
            <td>{{$student->getAddresses('registration','apartment_number')}}</td>
            <td>{{$student->getPassportData('documentType')}}</td>
            <td>{{$student->getPassportData('series')}}</td>
            <td>{{$student->getPassportData('number')}}</td>
            <td>{{$student->getPassportData('subdivisionCode')}}</td>
            <td>{{$student->getPassportData('issuanceDate')}}</td>
            <td>{{$student->getPassportData('issued')}}</td>
            <td>{{$student->getBirthday()}}</td>
            <td>{{$student->getPassportData('subdivisionCode')}}</td>
            <td>{{$student->getPassportData('birthPlace')}}</td>
            <td>{{$student->getNationality()}}</td>
            <td></td>
            <td></td>
            <td>{{$student->getPhone()}}</td>
            <td>{{$student->getEmail()}}</td>
            @if($type=='bank')
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            <td>{{$student->getInn()}}</td>
            <td>{{$student->getSnils()}}</td>
            @if($type=='full')
                <td>{{$student->getPrivelegeCategory()}}</td>
                <td>{{$student->getNeedHostel()?'Нуждается в общежитии':'Не требуется'}}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>