<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">

<div class="report-wrapper">
    @foreach($data as $key =>$item)
        <h3>{{$key=='ppssz'?'СПО':'НПО'}}</h3>
        <table class="table table-bordered table-striped text-center">
            <tr>
                <th></th>
                <th>Принято</th>
                <th>Из них женщины</th>
                <th>Всего</th>
                <th>Из них женщины</th>
            </tr>
            <tr>
                <td></td>
                <td>{{$item['enrollment']['total']}}</td>
                <td>{{$item['enrollment']['woman']}}</td>
                <td>{{$item['students']['total']}}</td>
                <td>{{$item['students']['woman']}}</td>
            </tr>
            @foreach($item['gridAge'] as $age=>$value)
                <tr>
                    <td>{{$age}}</td>
                    <td>{{$value['enrollment_total_count']}}</td>
                    <td>{{$value['enrollment_woman_count']}}</td>
                    <td>{{$value['students_total_count']}}</td>
                    <td>{{$value['students_woman_count']}}</td>
                </tr>
            @endforeach
        </table>
    @endforeach
</div>