<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
<style>
    th {
        padding: 5px;
    }

    td {
        padding: 5px;
    }
</style>

<div class="report-wrapper">
    @foreach($data as $key =>$students)
        <h2>{{$key}}</h2>
        <h2>Куратор: {{$groups[$key]}}</h2>
        <table class="table table-bordered table-striped text-center">
            <tr>
                <th>№</th>
                <th>ФИО</th>
                <th>Дата рождения</th>
                <th>Возраст</th>
                <th>Телефон</th>
                <th>Данные о родителях</th>
                <th>Адрес регистрации</th>
            </tr>
            @foreach($students as $student)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$student->getFullName()}}</td>
                    <td>{{$student->getBirthday()}}</td>
                    <td>{{$student->getAgeOnDate(\Carbon\Carbon::now()->format('d.m.Y'))}}</td>
                    <td>{{$student->getPhone()}}</td>
                    <td>
                        @foreach($student->getParents() as $parent)
                            <p>{{$parent->getRelationShipTypeStr()}} : {{$parent->getFullName()}}<br>
                                Телефон:{{$parent->getPhone()}}</p>
                            <hr>
                        @endforeach
                    </td>
                    <td>
                        {{$student->buildAddress('registration')}}
                    </td>
                </tr>
            @endforeach
        </table><br>
    @endforeach
</div>