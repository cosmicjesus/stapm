<html lang="ru">
<head>
    <title> Список студентов у которых закончился срок действия паспорта</title>
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">

    <style>
        body {
            font-family: "Times New Roman";
            color: black !important;
        }

        .format-border {
            border-bottom: 1px solid black !important;
        }

        .table {
            color: black;
            text-align: center;
            font-size: 12px;
        }

        td {
            padding: 5px;
            font-size: 14px;
        }

        th {
            padding: 5px;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div style="padding: 10px; text-align: center">
    <h3>
        Список студентов у которых закончился срок действия паспорта
    </h3>
    <table class="table table-striped table-bordered">
        <tr>
            <th>ФИО</th>
            <th>Группа</th>
            <th>Дата рождения</th>
            <th>Дата окончания</th>
        </tr>
        @foreach($data as $item)
            <tr>
                <td>{{$item['full_name']}}</td>
                <td>{{$item['group']}}</td>
                <td>
                    {{$item['birthday']}}
                </td>
                <td>{{$item['passport_end']}}</td>
            </tr>
        @endforeach
    </table>
</div>
</body>
</html>