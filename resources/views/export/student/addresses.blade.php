@php
    /**
    * @var $student \App\AppLogic\Student\Student
    */
@endphp
<div class="addresses">
    <h4>Адреса</h4>
    <p>Адрес регистрации : {{$student->buildAddress('registration')}}</p>
    <p>Адрес проживания : {{$student->buildAddress('residential')}}</p>
</div>