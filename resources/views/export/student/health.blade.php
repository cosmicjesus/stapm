<div class="health">
    <h4>Здоровье и льготы</h4>
    <p>
        <span>
            <b>
                Лицо с ОВЗ:
            </b>
            {{$health['person_with_disabilities']?'Да':'Нет'}}
        </span>
        <span>
            <b>
                Категория здоровья:
            </b>
            {{getHealthCategory($health['health_category'])}}
        </span>
        <span>
            <b>
                Инвалидность:
            </b>
            {{getDisability($health['disability'])}}
        </span>
        <span>
            <b>
                Номер полиса ОМС:
            </b>
            {{$health['medical_policy_number']}}
        </span>
        <br>
        <span>
            <b>
                Льготы:
            </b>
            @if(count($health['preferential_categories']))
                @foreach($health['preferential_categories'] as $category)
                    {{getPrivilegeCategory($category)}}@if(!$loop->last), @endif
                @endforeach
            @else
                Без льгот
            @endif
        </span>
    </p>
</div>