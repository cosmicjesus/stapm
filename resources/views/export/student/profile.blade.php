@php
    /**
    * @var $student \App\AppLogic\Student\Student
    */
@endphp
<style>
    /*.userpic {*/
    /*    border: 1px solid #ddd;*/
    /*    height: 197px;*/
    /*    overflow: hidden;*/
    /*    position: relative;*/
    /*    width: 150px;*/
    /*    background: #FFF url("./img/nophoto.png") no-repeat center;*/
    /*    margin: 0 auto;*/
    /*}*/


    /*.userpic img {*/
    /*    width: 100%;*/
    /*    height: 100%;*/
    /*}*/
</style>
<div class="profile" style="height: 1px">
    <table class="table" style="margin-bottom: 0px">
        <tr>
            <td rowspan="2">
                <div class="userpic" style="width: 150px;height: 197px">
                    @if($student->hasPhoto())
                        <img style="width: 150px;height: 197px" src="{{$student->getPhoto()}}"
                             alt="{{$student->getFullName()}}">
                    @else
                        <img style="width: 150px;height: 197px" src="/img/nophoto.png"
                             alt="{{$student->getFullName()}}">
                    @endif
                </div>
            </td>
            <td>
                <h2>{{$student->getFullName()}} № {{$student->getNominalNumber()}}</h2>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered table-fullwidth">
                    <tr>
                        <td>Дата рождения</td>
                        <td>{{dateFormat($student->getBirthday())}}</td>
                        <td>Гражданство: {{getNationality($student->getNationality())}}</td>
                    </tr>
                    <tr>
                        <td>Место рождения</td>
                        <td>{{$student->getPassportData('birthPlace')}}</td>
                        <td>Документ {{getDocumentType($student->getPassportData('documentType'))}}</td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td>{{$student->getPhone()}}</td>
                        <td>
                            Серия {{$student->getPassportData('series')}} Номер {{$student->getPassportData('number')}}
                            <br>
                            Код подразделения {{$student->getPassportData('subdivisionCode')}}
                        </td>
                    </tr>
                    <tr>
                        <td>СНИЛС</td>
                        <td>{{$student->getSnils()}}</td>
                        <td rowspan="2">
                            Выдан {{$student->getPassportData('issued')}}<br>
                            {{dateFormat($student->getPassportData('issuanceDate'))}}
                        </td>
                    </tr>
                    <tr>
                        <td>ИНН</td>
                        <td>{{$student->getInn()}}</td>
                    </tr>
                    <tr>
                        <td>Живет в общежитии</td>
                        <td>{{$student->getNeedHostelStatus()?'Да':'Нет'}}</td>
                        <td>Целевой набор: {{$student->isContractTargetSetStatus()?'Да':'Нет'}}</td>
                    </tr>
                    <tr>
                        <td>Откуда узнали</td>
                        <td>{{howFindOutSource($student->getHowFindOut())}}</td>
                        <td>
                            @if(is_array($student->getInvitedStudent()))
                                {{$student->getInvitedStudent()['full_name']}}
                                [{{$student->getInvitedStudent()['training_group']['name']}}]
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @include('export.student.addresses',['student'=>$student])
    @include('export.student.parents',['parents'=>$student->getParents()])
    @include('export.student.education',['student'=>$student])
    @include('export.student.health',['health'=>$student->getHealthData()])
    @if($student->isMale())
        @include('export.student.military',['military'=>$student->getMilitaryData()])
    @endif
    <div class="comment">
        <h4>Комментарий к профилю</h4>
        <p>{{$student->getComment()}}</p>
    </div>
</div>