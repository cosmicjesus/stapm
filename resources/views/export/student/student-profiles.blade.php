<html lang="ru">
<head>
    <title></title>
    <link rel="stylesheet" href="/pdf/styles.css?{{rand(1,9999)}}">
    <style>
        * {
            color: black !important;
            font-size: 12px;
            font-family: "Times New Roman", Times, serif;
            line-height: 1;
        }

        .table {
            color: black;
            text-align: center;
        }

        td, th {
            padding: 5px;
            font-size: 14px;
        }
        h4{
            font-size: 20px;
            margin-bottom: 4px;
        }
        p {
            font-size: 14px;
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>
</head>
<body style="color: black !important;
            font-family: 'Times New Roman', Times, serif;">
<div style="padding: 10px;">
    @foreach($students as $student)
        @include('export.student.profile',['student'=>$student])
    @endforeach
</div>
</body>
</html>