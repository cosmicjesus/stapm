@php
    /**
    * @var $student \App\AppLogic\Student\Student
    */
@endphp
<div class="education">
    <h4>Сведения об образовании</h4>
    <p><span><b>Уровень образования:</b> </span>{{educationLevel($student->getEducationData('education_id'))}}
        <span><b>Дата окончания:</b>{{dateFormat($student->getEducationData('graduation_date'))}} </span>
        <span><b>Средний балл:</b>{{$student->getEducationData('avg')}} </span>
        <br>
        <span><b>Предыдущая образовательная организация:</b> </span>{{$student->getEducationData('graduation_organization_name')}}
        <span><b>Изучаемый язык:</b> </span>{{getLanguage($student->getLanguage())}}
    </p>
</div>