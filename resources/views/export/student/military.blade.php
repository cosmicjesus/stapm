<div class="military">
    <h4>
        Воинский учет
    </h4>
    <p>
        <span>
            <b>
                Статус воинского учета:
            </b>
            {{militaryStatus($military['military_status'])}}
        </span>
        <br>
        <span>
            <b>
                Тип документа воинского учета:
            </b>
            {{militaryDocumentType($military['military_document_type'])}}
        </span>
        <br>
        <span>
            <b>
                Номер документа воинского учета:
            </b>
            {{$military['military_document_number']}}
        </span>
        <br>
        <span>
            <b>
                Категория годности к ВС:
            </b>
            {{fitnessForMilitaryServices($military['fitness_for_military_service'])}}
        </span>
        <br>
        <span>
            <b>
                Военный комиссариат:
            </b>
            {{$military['recruitment_center']['name']}}
        </span>
    </p>
</div>