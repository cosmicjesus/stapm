@php
    /**
    * @var $parent \App\AppLogic\User\UserParent
    */
@endphp
<div class="parents">
    <h4>Cведения о родителях</h4>
    <table class="table table-bordered">
        <tr>
            <th>ФИО</th>
            <th>Кем приходися</th>
            <th>Телефон</th>
            <th>Кем работает</th>
        </tr>
        @foreach($parents as $parent)
            <tr>
                <td>{{$parent['full_name']}}</td>
                <td>{{getRelationShipType($parent['relation_ship_type'])}}</td>
                <td>{{$parent['phone']}}</td>
                <td>{{$parent['place_of_work']}}</td>
            </tr>
        @endforeach
    </table>
</div>