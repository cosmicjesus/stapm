<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
<style>
    .table {
        color: black;
        text-align: center;
        font-size: 12px;
    }

    td {
        padding: 5px;
        font-size: 14px;
    }
</style>
<div style="padding: 10px; text-align: center">
    <h3>Сведения о ходе комплектования групп<br>
        приема на {{$data['date']}}
    </h3>
    <table class="table table-bordered">
        <tr>
            <td rowspan="2">
                № п/п
            </td>
            <td rowspan="2">
                Код профессии/<br>специальности
            </td>
            <td rowspan="2">
                Наименование профессии/специальности
            </td>
            <td rowspan="2">Подано заявлений<br> (с копиями документов)</td>
            <td rowspan="2">Подано заявлений (оригиналы документов)</td>
            <td colspan="3">
                в том числе по формам обучения
            </td>
        </tr>
        <tr>
            <td>Очная</td>
            <td>Очно-заочная</td>
            <td>Заочная</td>
        </tr>
        @foreach($data['data'] as $program)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$program['code']}}</td>
                <td>{{$program['name']}}</td>
                <td>{{$program['countEnrolleesWithOutDocuments']}}</td>
                <td>{{$program['countEnrolleesWithDocuments']}}</td>
                @foreach($program['forms'] as $form)
                    <td>{{$form}}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td colspan="3" style="text-align: right">Итого</td>
            <td>{{$data['countWithOutDocuments']}}</td>
            <td>{{$data['countWithDocuments']}}</td>
            @foreach($data['total_count_on_forms'] as $form)
                <td>{{$form}}</td>
            @endforeach
        </tr>
    </table>
</div>