@if($withStyle)
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
@endif
<div class="report-wrapper">

    @if($withStyle)
        <div class="report_header">
            @if(isset($data['program']))
                <h2>Отчет по абитуриентам</h2>
                <h2>{{$data['program']['type']}} {{$data['program']['name']}}</h2>
            @else
                <h2>Общий отчет по абитуриентам</h2>
            @endif
        </div>
    @endif
    <table class="table table-bordered table-striped text-center">
        <tr class="table-head-js">
            <th style="padding: 5px">№ п/п</th>
            <th style="padding: 5px">ФИО</th>
            @if(in_array('gender',$columns))<th style="padding: 5px">Пол</th>@endif
            @if(in_array('profession',$columns))<th style="padding: 5px">Профессия</th>@endif
            @if(in_array('start_date',$columns))<th style="padding: 5px">Дата подачи документов</th>@endif
            @if(in_array('phone',$columns))<th style="padding: 5px">Номер телефона</th>@endif
            @if(in_array('original',$columns))<th style="padding: 5px">Оригинал документа</th>@endif
            @if(in_array('need_hostel',$columns))<th style="padding: 5px">Нуждается в общежитии</th>@endif
            <th style="padding: 5px">Средний балл</th>
        </tr>
        @foreach($data['enrollees'] as $enrollee)
            <tr>
                <td style="padding: 8px">{{$loop->iteration}}</td>
                <td style="padding: 8px">{{$enrollee['fullname']}}</td>
                @if(in_array('gender',$columns))<td style="padding: 8px">{{$enrollee['gender']}}</td>@endif
                @if(in_array('profession',$columns))<td style="padding: 8px">{{$enrollee['program']}}</td>@endif
                @if(in_array('start_date',$columns))<td style="padding: 8px">{{$enrollee['start_date']}}</td>@endif
                @if(in_array('phone',$columns))<td style="padding: 8px">{{$enrollee['phone']}}</td>@endif
                @if(in_array('original',$columns))<td style="padding: 8px">{{$enrollee['original_doc']}}</td>@endif
                @if(in_array('need_hostel',$columns))<td style="padding: 8px">{{$enrollee['need_hostel']}}</td>@endif
                <td style="padding: 8px">{{$enrollee['avg']}}</td>
            </tr>
        @endforeach
        <tr>
            <td style="padding: 8px" colspan="{{$countColumns+2}}">Средний балл @if(isset($data['program']['type']))
                    по {{$data['program']['type']}}@endif</td>
            <td style="padding: 8px">{{$data['total_avg']}}</td>
        </tr>
    </table>
</div>