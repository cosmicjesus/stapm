@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <div class="buttons text-right">
                <button class="btn btn-success export-enrollee-js export-enrollee-report-to-excel-js hide"
                        data-route="{{route('admin.reports.enrollees.export-to-excel')}}">
                    <i class="fa fa-file-excel-o"></i> В Эксель
                </button>
                <button class="btn btn-primary export-enrollee-js export-enrollee-report-js hide"
                        data-route="{{route('admin.reports.enrollees.export')}}">
                    <i class="fa fa-print"></i> Печать
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="col-md-8">
        <div class="noreport">
            <h2 class="noreport-title">Параметры отчета не заданы</h2>
            <p class="noreport-text">Выберите необходимые параметры отчёта с помощью формы и нажмите кнопку
                «Сформировать
                отчёт»</p>
        </div>
        <div class="report">
            <div class="report-content">

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <form action="" id="enrolees-report-filter" class="" target="_blank"
              method="post">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="profession">Профессия</label>
                    <select name="profession" id="profession" class="form-control">
                        <option value="">--Профессия--</option>
                        @foreach($programs->getPrograms() as $program)
                            <option value="{{$program->getId()}}">{{$program->getNameWithForm()}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="documents">Документ об образовании</label>
                    <select name="documents" id="documents" class="form-control">
                        <option value="" selected>---Все абитуриенты---</option>
                        <option value="1">Только оригиналы</option>
                        <option value="2">Только не оригиналы</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="sort_avg">Сортировка по среднему баллу</label>
                    <select name="sort_avg" id="sort_avg" class="form-control">
                        <option value="">--Не сортировать--</option>
                        <option value="desc">По убыванию</option>
                        <option value="asc">По возрастанию</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="contract_target_set">Целевой набор</label>
                    <select name="contract_target_set" id="contract_target_set" class="form-control">
                        <option value="">--Не указанно--</option>
                        <option value="1">Да</option>
                        <option value="0">Нет</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="only_need_hostel">Только нуждающихся в общежитии</label>
                    <select name="only_need_hostel" id="only_need_hostel" class="form-control">
                        <option value="">Нет</option>
                        <option value="1">Да</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <h4>Показывать столбцы</h4>
            </div>
            @foreach($columns as $key=>$column)
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" name="columns[]" value="{{$key}}"
                                       checked> {{$column}}
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-xs-12 text-center">
                <button type="button" class="btn btn-primary build-enrollee-report-js">Сформировать отчёт</button>
            </div>
        </form>
    </div>
@endsection