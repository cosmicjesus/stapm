<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
<style>
    .table {
        color: black;
        text-align: center;
        font-size: 12px;
    }

    td,th {
        text-align: center;
        padding: 5px;
        font-size: 14px;
    }
</style>
<div style="padding: 10px; text-align: center">
    <h3>Средний балл каждой группы первого курса
    </h3>
    <table class="table table-bordered">
        <tr>
            <th>Название группы</th>
            <th>Кол-во человек</th>
            <th>Балл</th>
        </tr>
        @foreach($groups as $group)
            <tr>
                <td>{{$group->getName()}}</td>
                <td>{{$group->getCountStudents()}}</td>
                <td>{{$group->getGroupAvg()}}</td>
            </tr>
        @endforeach
    </table>
</div>