@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.news-papers.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить выпуск
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="news-papers-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Номер</th>
                <th>Обложка</th>
                <th>Файл</th>
                <th>Дата публикации</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
@endsection