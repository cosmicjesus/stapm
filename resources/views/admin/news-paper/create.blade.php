@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.news-papers.store','id'=>'news-papers-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <label>Дата публикации</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="default-picker" type="text" name="publication_date"
                       value="{{\Carbon\Carbon::now()->day(1)->format('d.m.Y')}}" required>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('cover','Обложка',['class'=>'control-label'])}}
            {{Form::file('cover',['required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('file','Файл газеты (PDF)',['class'=>'control-label'])}}
            {{Form::file('file',['required'=>'required'])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true)}}
                Показывать на сайте
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Загрузите основные файлы</p>
    <p>В поле дата нужно указать первое число месяца, когда газета была издана</p>
    <p>Номер газеты будет подставлен автоматически</p>
@endsection