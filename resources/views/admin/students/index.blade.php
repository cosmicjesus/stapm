@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            @if(checkPermissions('students.actions'))
                <div class="btn-group pull-left">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Действия <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="#" class="allocation-student-js" data-toggle="modal"
                               data-target="#allocation-modal">Отчислить</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" class="transfer-students-js" data-target="#transfer-modal">Перевод
                                в другую группу</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{route('admin.export.base',['type'=>'bank'])}}" target="_blank">Экспорт файла для
                                банка</a></li>
                        <li><a href="{{route('admin.export.base',['type'=>'full'])}}" target="_blank">Экспорт полной
                                базы</a></li>
                        <li><a href="{{route('admin.export.parents')}}" target="_blank">Экспорт родителей</a></li>
                        <li class="divider"></li>
                        <li><a href="{{route('admin.student.count.report')}}" target="_blank">Цифры для СПО-1</a></li>
                        <li><a href="{{route('admin.student.report-on-date')}}" target="_blank">Несовершенолетние студенты</a></li>
                        <li><a href="{{route('admin.groups.avg')}}" target="_blank">Средний балл по группам</a></li>
                    </ul>
                </div>
            @endif
            @if(checkPermissions('students.create'))
                <a href="{{route('admin.students.create')}}" class="btn btn-success pull-right">
                    <i class="fa fa-plus"></i> Добавить студента
                </a>
            @endif
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="filter-students-table-js">
            <div class="col-sm-10 col-xs-12">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="ФИО">
                    </div>

                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <select name="group" id="group" class="form-control">
                            <option value="" selected>---Все группы---</option>
                            @foreach($groups as $group)
                                <option value="{{$group->getId()}}">{{$group->getName()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <select name="status" id="status" class="form-control">
                            <option value="" selected>---Все студенты---</option>
                            <option value="active">Только обучающиеся</option>
                            <option value="inArmy">Только в РА</option>
                            <option value="academic">Только в АО</option>
                            <option value="armyAndAcademic">Только студенты в РА и АО</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <select name="facility" id="facility" class="form-control">
                            <option value="" selected>---Тут список льгот---</option>
                            @foreach (getPrivilegeCategories() as $key=> $facility)
                                <option value="{{$key}}">{{$facility}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="checkbox">
                        <label for="sort_by_group">
                            <input type="checkbox" name="sort_by_group" id="sort_by_group">
                            Сортировать по группам
                        </label>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="checkbox">
                        <label for="only_long_absent">
                            <input type="checkbox" name="only_long_absent" id="only_long_absent">
                            Только длительно отсутствующие
                        </label>
                    </div>

                </div>
            </div>
            <div class="col-sm-2 col-xs-12">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="students-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>ФИО</th>
                <th>Дата рождения</th>
                <th>Группа</th>
                <th>Статус</th>
                <th>Льготы</th>
                <th>Телефон</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>

@endsection