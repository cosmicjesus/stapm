<div class="row">
    <div class="col-xs-12">
        <form action="{{route('admin.student.spravka-v-voenkomat',['id'=>$student->getId()])}}" class="help-form" method="get" target="_blank">
            <input type="hidden" name="student_id" value="{{$student->getId()}}">
            <div class="form-group">
                <label for="" class="control-label">Период обучения</label>
                <div class="input-group input-daterange">
                    <div class="input-group-addon">C</div>
                    @if($student->getDateOfActualTransfer())
                        <input name="start_training" type="text" class="form-control" id="default-picker"
                               value="{{$student->getDateOfActualTransfer()}}">
                    @else
                        <input name="start_training" type="text" class="form-control" id="default-picker"
                               value="{{$student->getGroup()->getEducationPlan()->getStartTrainig()}}">
                    @endif
                    <div class="input-group-addon">по</div>
                    <input name="end_training" type="text" class="form-control" id="end_training"
                           value="{{$student->getGroup()->getEducationPlan()->getEndTrainig()}}">
                </div>
            </div>
            <div class="form-group">
                <label for="decree">Приказ</label>
                <select name="decree" id="decree" class="form-control">
                    @foreach($student->getDecrees() as $decree)
                        <option value="{{$decree->getNumber()}}">{{$decree->getNumber()}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="voenkomat_name">Название военкомата</label>
                <input type="text" class="form-control" id="voenkomat_name" name="voenkomat_name" required>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-danger" type="submit">
                        <i class="fa fa-file-pdf-o"></i> Печать
                    </button>
                </div>
            </div>
        </form>
        <div class="references-js">

        </div>
    </div>
</div>