@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.students.update',$student->slug],'id'=>'student-edit-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
            {{Form::text('lastname',$student->lastname,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('firstname','Имя',['class'=>'control-label'])}}
            {{Form::text('firstname',$student->firstname,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
            {{Form::text('middlename',$student->middlename,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label>Дата рождения</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="birthday" type="text" name="birthday"
                           value="{{$student->birthday->format('d.m.Y')}}" required>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                {{Form::label('gender','Пол',['class'=>'control-label'])}}
                {{Form::select('gender',genders(),$student->gender,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <label>Дата выдачи паспорта</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="birthday" type="text" name="passport_issuance_date"
                       @if(!is_null($student->passport_issuance_date)) value="{{$student->passport_issuance_date->format('d.m.Y')}}" @endif>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Дата фактического зачисления</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="default-picker" type="text" name="date_of_actual_transfer"
                       @if(!is_null($student->date_of_actual_transfer)) value="{{$student->date_of_actual_transfer->format('d.m.Y')}}" @endif>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('avg','Средний балл')}}
            <div class="input-group">
                {{Form::number('avg',$student->avg,['class'=>'form-control','min'=>2,'max'=>5,'step'=>'0.0001'])}}
                <span class="input-group-btn">
                    <a class="btn btn-primary avg-calc-modal-js" data-toggle="modal" data-target="#avg-calc-modal">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('phone','Телефон',['class'=>'control-label'])}}
            {{Form::text('phone',$student->phone,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('medical_policy_number','Номер полиса ОМС',['class'=>'control-label'])}}
            {{Form::text('medical_policy_number',$student->medical_policy_number,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('snils','СНИЛС',['class'=>'control-label'])}}
            {{Form::text('snils',$student->snils,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('inn','ИНН',['class'=>'control-label'])}}
            {{Form::text('inn',$student->inn,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="privileged_category">Льготная категория</label>
            {{Form::select('privileged_category',getPrivilegeCategories(),$student->privileged_category,['class'=>'form-control','placeholder'=>'--Без льгот--'])}}
        </div>
            <div class="form-group">
                <label for="education_id">Образование</label>
                {{Form::select('education_id',getEducationsOnSelect(),$student->education_id,['class'=>'form-control'])}}
            </div>
        <div class="form-group">
            <label for="long_absent">
                <input type="checkbox" @if($student->long_absent) checked @endif name="long_absent" id="long_absent">
                Длительно отсутствующий
            </label>
        </div>
        <div class="form-group">
            <label for="has_certificate">
                <input type="checkbox" @if($student->has_certificate) checked @endif name="has_certificate"
                       id="has_certificate">
                Медсправка
            </label>
        </div>
        <div class="form-group">
            {{Form::label('comment','Дополнительная информация')}}
            {{Form::textarea('comment',$student->comment,['class'=>'form-control'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Обновите нужную информацию студента и нажмите кнопку Обновить</p>
@endsection