@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.students.store','id'=>'add-student-form','name'=>'add-student-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
            {{Form::text('lastname',old('lastname'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('firstname','Имя',['class'=>'control-label'])}}
            {{Form::text('firstname',old('firstname'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
            {{Form::text('middlename',old('middlename'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label>Дата рождения</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="birthday" type="text" name="birthday" required
                           @if(old('birthday')) value="{{old('birthday')}}" @endif>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                {{Form::label('gender','Пол',['class'=>'control-label'])}}
                {{Form::select('gender',genders(),old('gender'),['class'=>'form-control','required'=>'required'])}}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('education','Образование')}}
            {{Form::select('education',$educations,null,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <label for="group_id" class="control-label">Группа</label>
            <select name="group_id" id="group_id" class="form-control" required>
                @foreach($groups as $group)
                    <option value="{{$group->getId()}}">{{$group->getName()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('graduation_organization_name','Предыдущая образовательная организация')}}
            {{Form::text('graduation_organization_name',null,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <label>Дата окончания предыдущего обучения</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="graduation_date" type="text" name="graduation_date" required
                       @if(old('graduation_date')) value="{{old('graduation_date')}}" @endif>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="person_with_disabilities">
                {{Form::checkbox('person_with_disabilities',old('person_with_disabilities'),false,['id'=>'person_with_disabilities'])}}
                Является лицом с ОВЗ
            </label>
        </div>
        <div class="form-group">
            <label for="need_hostel">
                {{Form::checkbox('need_hostel',old('need_hostel'),false,['id'=>'need_hostel'])}}
                Нуждается в общежитии
            </label>
        </div>
        <div class="form-group">
            <label for="contract_target_set">
                {{Form::checkbox('contract_target_set',old('contract_target_set'),false,['id'=>'contract_target_set'])}}
                Договор целевого набора
            </label>
        </div>
        <div class="form-group">
            {{Form::label('avg','Средний балл')}}
            <div class="input-group">
                {{Form::number('avg',null,['class'=>'form-control','min'=>2,'max'=>5,'step'=>'0.001','required'=>'required'])}}
                <span class="input-group-btn">
                    <a class="btn btn-primary avg-calc-modal-js" data-toggle="modal" data-target="#avg-calc-modal">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('phone','Телефон',['class'=>'control-label'])}}
            {{Form::text('phone',old('phone'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <label for="decree_date">Дата выдачи паспорта</label>
            <div class="input-group date">
                <input class="form-control required" id="decree_date" type="text"
                       name="passport_issuance_date" required
                       value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="reason" class="control-label">Причина</label>
            <select name="reason" id="reason" class="form-control" required>
                @foreach(getReasons('enrollment') as $key => $reason)
                    <option value="{{$key}}">{{$reason}}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="col-xs-6">
                {{Form::label('decree_number','Номер приказа',['class'=>'control-label'])}}
                {{Form::text('decree_number',old('decree_number'),['class'=>'form-control','required'=>'required'])}}
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="decree_date">Дата приказа</label>
                    <div class="input-group date">
                        <input class="form-control required" id="decree_date" type="text"
                               name="decree_date" required
                               value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection