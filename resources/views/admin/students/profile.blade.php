@extends('admin.layouts.layout')

@section('content')

    <div class="col-md-4">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">

                {{--<h3 class="profile-username text-center">{{$student->getFullName()}}</h3>--}}

                {{--<p class="text-muted text-center">Software Engineer</p>--}}

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Пол</b> <a class="pull-right">{{$student->getGenderToStr()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Дата рождения</b> <a class="pull-right">{{$student->getBirthday()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Возраст</b> <a class="pull-right">{{$student->getAge()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Средний балл</b> <a class="pull-right">{{$student->getAvg()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Дата выдачи паспорта</b> <a class="pull-right">{{$student->getPassportIssuanceDate()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Группа</b> <a class="pull-right">{{$student->getGroup()->getName()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Статус</b> <a class="pull-right">{{$student->getStatus()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Длительно отсутствующий</b> <a class="pull-right">{{$student->isLongAbsent()?'Да':'Нет'}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Номер полиса ОМС</b> <a class="pull-right">{{$student->getMedicalPolicyNumber()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>СНИЛС</b> <a class="pull-right">{{$student->getSnils()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>ИНН</b> <a class="pull-right">{{$student->getInn()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Медсправка</b> <a class="pull-right">{{$student->getMedCertificateStr()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Льготная кат.</b> <a class="pull-right">{{$student->getPrivelegeCategory()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Дата фактического зачисления</b> <a
                                class="pull-right">{{$student->getDateOfActualTransfer()}}</a>
                    </li>
                </ul>
                @if(checkPermissions(['students.actions']))
                    <a href="{{$student->getEditLink()}}" class="btn btn-primary btn-block"><b>Редактировать</b></a>
                @endif
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Образование</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12" style="margin-bottom: 5px">
                        {{--<strong><i class="fa fa-book margin-r-5"></i> Образование</strong>--}}
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Уровень образования</th>
                                </tr>
                                <tr>
                                    <td>{{$student->getEducation()}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-8">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#movement" data-toggle="tab">Движение студента</a></li>
                <li><a href="#helps" data-toggle="tab">Справка об обучении</a></li>
                <li><a href="#voenkomat-ref" data-toggle="tab">Справка в военкомат</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="movement">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>Тип приказа</th>
                                <th>№ приказа</th>
                                <th>Текст</th>
                                <th>Причина</th>
                            </tr>
                            @foreach($student->getDecrees() as $decree)
                                <tr>
                                    <td>{{$decree->getType()}}</td>
                                    <td>{{$decree->getNumber()}}</td>
                                    <td>{{$decree->getDecreeText()}}</td>
                                    <td>{{$decree->getReason()}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="helps">
                    @include('admin.students.inc.helps',['student'=>$student])
                </div>
                <div id="voenkomat-ref" class="tab-pane">
                    @include('admin.students.inc.voenkomat',['student'=>$student])
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Документ уд.личность</h3>
                    <button class="btn btn-danger exit-passport-js" style="float: right;margin-left: 5px;display: none">
                        <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-success save-passport-js" style="float: right;display: none">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-primary edit-passport-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information passport-js">
                                @include('admin.enrollees.inc.passport',['enrollee'=>$student])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Адреса</h3>
                    <button class="btn btn-primary edit-addresses-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information addresses-js">
                                @include('admin.enrollees.inc.addresses',['enrollee'=>$student])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->

@endsection