@extends('admin.layouts.add')

@section('form')
    {{Form::open(['url'=>$document->getEditUrl(),'id'=>'category-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',$document->getName(),['class'=>'form-control','placeholder'=>'Наименование категории'])}}
        </div>
        <div class="form-group">
            {{Form::label('category_ids[]','Категория',['class'=>'control-label'])}}
            <select name="category_ids[]" id="category_ids[]" class="form-control" multiple="multiple" size="5">
                @foreach($categories as $key=>$category)
                    <option @if(in_array($key,$category_ids)) selected @endif value="{{$key}}">{{$category}}</option>
                @endforeach
            </select>

        </div>
        <div class="form-group">
            {{Form::label('file','Файл',['class'=>'control-label'])}}
            <br><a href="{{$document->getPath()}}" target="_blank">Имеющийся файл</a>
            {{Form::file('file',['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',$document->getSort(),['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),$document->getActive())}}
                Показывать на сайте
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Задайте наименование и порядок сортировки документа</p>
    <p>Выберите файл и укажите категорию</p>
@endsection