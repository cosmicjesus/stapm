@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.documents.create.page')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить документ
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 10px;padding-right: 16px">
        <form id="filter-documents-table-js" name="filter-documents-table">
            <div class="col-sm-10">
                <div class="col-xs-12 col-sm-5">
                    <div class="form-group">
                        <select name="category" id="category" class="form-control">
                            <option value="" selected>---Все категории---</option>
                            @foreach($categories as $category)
                                <option value="{{$category->getId()}}">{{$category->getNameWithParent()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table id="documents-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование</th>
                <th>Категории</th>
                <th>Файл</th>
                <th>Сортировка</th>
                <th>Показывать</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection