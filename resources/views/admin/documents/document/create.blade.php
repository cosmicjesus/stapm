@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.documents.create','id'=>'category-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Наименование категории'])}}
        </div>
        <div class="form-group">
            {{Form::label('category_ids[]','Категория',['class'=>'control-label'])}}
            {{Form::select('category_ids[]',$categories,null,['class'=>'form-control','placeholder'=>'Выберите категорию','multiple'=>'multiple','size'=>5])}}
        </div>
        <div class="form-group">
            {{Form::label('file','Файл',['class'=>'control-label'])}}
            {{Form::file('file',['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',old('sort')?old('sort'):500,['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true)}}
                Показывать на сайте
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Задайте наименование и порядок сортировки документа</p>
    <p>Выберите файл и укажите категорию</p>
@endsection