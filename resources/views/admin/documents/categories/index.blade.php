@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.documents.categories.create.page')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить категорию
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование</th>
                <th>Родительская категория</th>
                <th>Порядок сортировки</th>
                <th>Кол-во документов</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($categories))
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->getId()}}</td>
                        <td>{{$category->getName()}}</td>
                        <td>{{$category->getParent()}}</td>
                        <td>{{$category->getSort()}}</td>
                        <td>{{$category->getCountFiles()}}</td>
                        <td>{{$category->getActiveStr()}}</td>
                        <td>
                            <div class="table-buttons">
                                <a href="{{$category->getEditUrl()}}" class="btn btn-warning btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="#"data-name="{{$category->getName()}}" data-url="{{$category->getDeleteUrl()}}" class="btn btn-danger btn-xs delete-category-js">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr class="text-center">
                    <td colspan="7">Всего:{{$categories->count()}}</td>
                </tr>
            @else
                <tr class="text-center">
                    <td colspan="7">Записи отсутствуют</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

@endsection