@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.documents.categories.create','id'=>'category-add-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Наименование категории'])}}
        </div>
        <div class="form-group">
            {{Form::label('category_id','Родительская категория',['class'=>'control-label'])}}
            {{Form::select('category_id',$categories,null,['class'=>'form-control','placeholder'=>'Родительская категория'])}}
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',old('sort')?old('sort'):500,['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true)}}
                Показывать на сайте
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Задайте наименование и порядок сортировки категории</p>
    <p>Если нужно выберите родительскую категорию</p>
@endsection