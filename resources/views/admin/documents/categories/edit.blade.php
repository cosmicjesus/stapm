@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.documents.categories.edit',$category->id],'id'=>'category-add-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',$category->name,['class'=>'form-control','placeholder'=>'Наименование категории'])}}
        </div>
        <div class="form-group">
            {{Form::label('category_id','Родительская категория',['class'=>'control-label'])}}
            {{Form::select('category_id',$categories,$category->parent_id,['class'=>'form-control','placeholder'=>'Родительская категория'])}}
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',$category->sort,['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),$category->active)}}
                Показывать на сайте
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Измените наименование и порядок сортировки категории</p>
    <p>Если нужно выберите или удалите родительскую категорию</p>
@endsection