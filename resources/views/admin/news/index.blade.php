@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.news.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить новость
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="search-employees">
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Заголовок">
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="news-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Заголовок</th>
                <th>Изображение</th>
                <th>Дата публикации</th>
                <th>Показывать до</th>
                <th>Показывать в разделе</th>
                <th>Превью</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
@endsection