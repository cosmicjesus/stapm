@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.news.update',$news->getSlug()],'id'=>'news-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            {{Form::label('title','Заголовок',['class'=>'control-label'])}}
            {{Form::text('title',$news->getTitle(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="" class="control-label">Даты показа</label>
            <div class="input-group input-daterange">
                <div class="input-group-addon">C</div>
                <input name="publication_date" type="text" class="form-control" id="publication_date"
                       @if(old('publication_date')) value="{{old('publication_date')}}"
                       @else value="{{$news->getPublicationDate()}}" @endif >
                <div class="input-group-addon">по</div>
                <input name="visible_to" type="text" class="form-control" id="visible_to" value="">
            </div>
        </div>
        <div class="form-group">

            <a href="{{$news->getImage()}}" data-fancybox data-caption="{{$news->getTitle()}}">Загруженное
                изображение</a><br><br>

            {{Form::label('file','Изображение',['class'=>'control-label'])}}
            {{Form::file('file')}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),$news->getActiveStatus())}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            {{Form::label('type','Показывать в разделе')}}
            {{Form::select('type',getNewsSections(),$news->getNewsSection(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('preview','Превью',['class'=>'control-label'])}}
            {{Form::textarea('preview',$news->getPreview(),['class'=>'form-control','id'=>'preview'])}}
        </div>
        <div class="form-group">
            {{Form::label('full_text','Полный текст',['class'=>'control-label'])}}
            {{Form::textarea('full_text',$news->getFullText(),['class'=>'form-control','id'=>'full_text'])}}
        </div>

        <div class="files">
            <h4>Файлы</h4>
            @if($news->getFiles())
                @foreach($news->getFiles() as $file)
                    <div class="news-file">
                        <div class="row">
                            <div class="col-xs-9">
                                <a href="{{$file->getPath()}}" class="news-document-link">{{$file->getName()}}</a>
                                <div class="news-file-edit">
                                    <div data-url="{{$file->getEditUrl()}}" class="rename-news-file"
                                         id="rename-news-file">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <input type="text" value="{{$file->getName()}}"
                                                       name="news-document-name"
                                                       class="news-document-name-js form-control">
                                            </div>
                                            <div class="col-xs-2 text-center">
                                                <button class="btn btn-success btn-xs rename-news-file-js"
                                                        type="button">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <button class="btn btn-warning btn-xs edit-news-document-js" type="button">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </div>
                            <div class="col-xs-1">
                                <button class="btn btn-warning btn-xs" type="button">
                                    <i class="fa fa-refresh"></i>
                                </button>
                            </div>
                            <div class="col-xs-1">
                                <button class="btn btn-danger btn-xs delete-news-document-js" type="button"
                                        data-url="{{$file->getDeleteUrl()}}"
                                        data-name="{{$file->getName()}}"
                                >
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                @endforeach
            @else
                <h5>Нет дополнительных файлов</h5>
            @endif
        </div>
        <button class="btn btn-primary pull-left add-news-files-js" type="button">
            <i class="fa fa-plus"> Прикрепить файл</i>
        </button>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection
