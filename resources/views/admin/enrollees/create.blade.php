@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.enrollees.create','id'=>'enrollee-add-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
            {{Form::text('lastname',old('lastname'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('firstname','Имя',['class'=>'control-label'])}}
            {{Form::text('firstname',old('firstname'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
            {{Form::text('middlename',old('middlename'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label>Дата рождения</label>
                {{--<div class="input-group date">--}}
                <input class="form-control pull-right" id="birthday" type="text" name="birthday"
                       @if(old('birthday')) value="{{old('birthday')}}" @endif>
                {{--<div class="input-group-addon">--}}
                {{--<i class="fa fa-calendar"></i>--}}
                {{--</div>--}}
            </div>

            <div class="col-xs-6">
                {{Form::label('gender','Пол',['class'=>'control-label'])}}
                {{Form::select('gender',genders(),old('gender'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('nationality','Гражданство')}}
            {{Form::select('nationality',getCountrues(),'russia',['class'=>'form-control','placeholder'=>'Не задано'])}}
        </div>
        <div class="form-group">
            {{Form::label('education','Образование')}}
            {{Form::select('education',$educations,null,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="profession" class="control-label">Профессия</label>
            <select name="profession" id="program" class="form-control" required>
                <option value="">--Выберите профессию--</option>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}">{{$program->getNameWithForm()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="has_original" class="has_original_check-js">
                {{Form::checkbox('has_original',old('has_original'),false,['id'=>'has_original'])}}
                Оригинал документа об образовании
            </label>
        </div>
        <div style="padding: 0 10px">
            <div class="form-group graduation_organization_name-js" style="display: none;">
                {{Form::label('graduation_organization_name','Предыдущая образовательная организация')}}
                {{Form::text('graduation_organization_name',null,['class'=>'form-control'])}}
            </div>
            <div class="form-group graduation_organization_name-js" style="display: none;">
                {{Form::label('graduation_organization_place','Населенный пункт')}}
                {{Form::text('graduation_organization_place',null,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <label for="has_certificate">
                {{Form::checkbox('has_certificate',old('has_certificate'),false,['id'=>'has_certificate'])}}
                Имеется медицинская справка
            </label>
        </div>
        <div class="form-group">
            <label for="person_with_disabilities">
                {{Form::checkbox('person_with_disabilities',old('person_with_disabilities'),false,['id'=>'person_with_disabilities'])}}
                Является лицом с ОВЗ
            </label>
        </div>
        <div class="form-group">
            <label for="need_hostel">
                {{Form::checkbox('need_hostel',old('need_hostel'),false,['id'=>'need_hostel'])}}
                Нуждается в общежитии
            </label>
        </div>
        <div class="form-group">
            <label for="contract_target_set">
                {{Form::checkbox('contract_target_set',old('contract_target_set'),false,['id'=>'contract_target_set'])}}
                Договор целевого набора
            </label>
        </div>
        <div class="form-group">
            {{Form::label('snils','СНИЛС',['class'=>'control-label'])}}
            {{Form::text('snils',old('snils'),['class'=>'form-control','placeholder'=>'Номер СНИЛС вводить без черточек'])}}
        </div>
        <div class="form-group">
            {{Form::label('inn','ИНН',['class'=>'control-label'])}}
            {{Form::text('inn',old('inn'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('avg','Средний балл')}}
            <div class="input-group">
                {{Form::number('avg',null,['class'=>'form-control','min'=>2,'max'=>5,'step'=>'0.0001'])}}
                <span class="input-group-btn">
                    <a class="btn btn-primary avg-calc-modal-js" data-toggle="modal" data-target="#avg-calc-modal">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('language','Изучаемый язык')}}
            {{Form::select('language',getLanguages(),'english',['class'=>'form-control','placeholder'=>'- Не задано -'])}}
        </div>
        <div class="form-group">
            {{Form::label('phone','Телефон',['class'=>'control-label'])}}
            {{Form::text('phone',old('phone'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label>Дата подачи документов</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="start_date" type="text" name="start_date" required
                           @if(old('start_date')) value="{{old('start_date')}}"
                           @else value="{{\Carbon\Carbon::now()->format('d.m.Y')}}" @endif>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <label>Дата окончания пред. обучения</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="graduation_date" type="text" name="graduation_date"
                           required
                           @if(old('graduation_date')) value="{{old('graduation_date')}}"
                           @else value="{{\Carbon\Carbon::now()->day(30)->month(6)->format('d.m.Y')}}" @endif>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <!--Паспортные данные-->
        <div class="col-md-12">
            <hr>
            <h4>Данные паспорта</h4>
            <hr>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('passport[documentType]','Тип документа')}}
                {{Form::select('passport[documentType]',getDocumentsType(),21,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('passport[series]','Серия')}}
                {{Form::text('passport[series]',null,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('passport[number]','Номер')}}
                {{Form::text('passport[number]',null,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="passport[issuanceDate]">Дата выдачи</label>
                {{--<div class="input-group date">--}}
                <input class="form-control pull-right" id="issuanceDate" type="text"
                       name="passport[issuanceDate]"
                       value="{{Carbon\Carbon::now()->format('d.m.Y')}}">
                {{--<div class="input-group-addon">--}}
                {{--<i class="fa fa-calendar"></i>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('passport[subdivisionCode]','Код подразделения')}}
                {{Form::text('passport[subdivisionCode]',null,['class'=>'form-control','id'=>'subdivisionCode'])}}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('passport[issued]','Кем выдан')}}
                {{Form::textarea('passport[issued]',null,['class'=>'form-control','rows'=>2,'id'=>'issued'])}}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('passport[birthPlace]','Место рождения')}}
                {{Form::text('passport[birthPlace]',null,['class'=>'form-control','id'=>'birthPlace'])}}
            </div>
        </div>
        <div class="col-xs-12">
            <hr>
            <h4>Адреса</h4>
            <hr>
        </div>
        <div class="registration address">
            <div class="col-xs-12">
                <h4 style="display: inline-block">Адрес регистрации</h4>
                <div class="pull-right">
                    <label for="isAddressSimilar">
                        {{Form::checkbox('isAddressSimilar',null,false,['id'=>'isAddressSimilar'])}}
                        Совпадает с адресом проживания
                    </label><br>
                    <label for="isAddressPlaceOfStay">
                        {{Form::checkbox('isAddressPlaceOfStay',null,false,['id'=>'isAddressPlaceOfStay'])}}
                        Совпадает с рег. по месту приб.
                    </label>
                </div>
                <div class="form-group">
                    {{Form::label('registration[region]','Область')}}
                    {{Form::text('registration[region]',null,['class'=>'form-control','id'=>'registration-region'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[area]','Район')}}
                    {{Form::text('registration[area]',null,['class'=>'form-control','id'=>'registration-area'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[settlement]','Населенный пункт')}}
                    {{Form::text('registration[settlement]',null,['class'=>'form-control','id'=>'registration-settlement'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[city]','Город')}}
                    {{Form::text('registration[city]',null,['class'=>'form-control','id'=>'registration-city'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[index]','Индекс')}}
                    {{Form::text('registration[index]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[street]','Улица')}}
                    {{Form::text('registration[street]',null,['class'=>'form-control','id'=>'registration-street'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[house_number]','Дом')}}
                    {{Form::text('registration[house_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[housing]','Корпус')}}
                    {{Form::text('registration[housing]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[apartment_number]','Квартира')}}
                    {{Form::text('registration[apartment_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
        </div>

        <div id="residential" class="address">
            <div class="col-xs-12">
                <h4>Адрес проживания</h4>
                <div class="form-group">
                    {{Form::label('residential[region]','Область')}}
                    {{Form::text('residential[region]',null,['class'=>'form-control','id'=>'residential-region'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[area]','Район')}}
                    {{Form::text('residential[area]',null,['class'=>'form-control','id'=>'residential-area'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[settlement]','Населенный пункт')}}
                    {{Form::text('residential[settlement]',null,['class'=>'form-control','id'=>'residential-settlement'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[city]','Город')}}
                    {{Form::text('residential[city]',null,['class'=>'form-control','id'=>'residential-city'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[index]','Индекс')}}
                    {{Form::text('residential[index]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[street]','Улица')}}
                    {{Form::text('residential[street]',null,['class'=>'form-control','id'=>'residential-street'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[house_number]','Дом')}}
                    {{Form::text('residential[house_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[housing]','Корпус')}}
                    {{Form::text('residential[housing]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[apartment_number]','Квартира')}}
                    {{Form::text('residential[apartment_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
        </div>

        <div id="place_of_stay" class="address">
            <div class="col-xs-12">
                <h4>Адрес регистрации по месту пребывания</h4>
                <div class="form-group">
                    {{Form::label('place_of_stay[region]','Область')}}
                    {{Form::text('place_of_stay[region]',null,['class'=>'form-control','id'=>'place_of_stay-region'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[area]','Район')}}
                    {{Form::text('place_of_stay[area]',null,['class'=>'form-control','id'=>'place_of_stay-area'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[settlement]','Населенный пункт')}}
                    {{Form::text('place_of_stay[settlement]',null,['class'=>'form-control','id'=>'place_of_stay-settlement'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[city]','Город')}}
                    {{Form::text('place_of_stay[city]',null,['class'=>'form-control','id'=>'place_of_stay-city'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[index]','Индекс')}}
                    {{Form::text('place_of_stay[index]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[street]','Улица')}}
                    {{Form::text('place_of_stay[street]',null,['class'=>'form-control','id'=>'place_of_stay-street'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[house_number]','Дом')}}
                    {{Form::text('place_of_stay[house_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[housing]','Корпус')}}
                    {{Form::text('place_of_stay[housing]',null,['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[apartment_number]','Квартира')}}
                    {{Form::text('place_of_stay[apartment_number]',null,['class'=>'form-control'])}}
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection