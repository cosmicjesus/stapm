<div class="passport-data">
    @if(!is_null($enrollee->getPassportData()))

        <p class="big">
            <span>{{getDocumentType($enrollee->getPassportData('documentType'))}}</span>
            <span>{{$enrollee->getPassportData('series')}}</span>
            <span>№ {{$enrollee->getPassportData('number')}},</span>
            <span>выдан {{$enrollee->getPassportData('issuanceDate')}}</span>
            <span>{{$enrollee->getPassportData('issued')}}</span>
        </p>
        <p>
            <span data-prefix="Код подразделения: ">{{$enrollee->getPassportData('subdivisionCode')}}</span>
            <span data-prefix="Место рождения: ">{{$enrollee->getPassportData('birthPlace')}}</span>
        </p>
    @else
        <p>Данные не заполнены</p>
    @endif
</div>

<div class="passport-form">
    <form action="{{$enrollee->getUpdatePassportUrl()}}" name="edit-passport-form" id="edit-passport-form"
          method="post">
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('documentType','Тип документа')}}
                {{Form::select('documentType',getDocumentsType(),$enrollee->getPassportData('documentType')?$enrollee->getPassportData('documentType'):21,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('series','Серия')}}
                {{Form::text('series',$enrollee->getPassportData('series'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('number','Номер')}}
                {{Form::text('number',$enrollee->getPassportData('number'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="issuanceDate">Дата выдачи</label>
                {{--<div class="input-group date">--}}
                <input class="form-control pull-right" id="issuanceDate" type="text" name="issuanceDate"
                       value="{{$enrollee->getPassportData('issuanceDate')}}">
                {{--<div class="input-group-addon">--}}
                {{--<i class="fa fa-calendar"></i>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('subdivisionCode','Код подразделения')}}
                {{Form::text('subdivisionCode',$enrollee->getPassportData('subdivisionCode'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('issued','Кем выдан')}}
                {{Form::textarea('issued',$enrollee->getPassportData('issued'),['class'=>'form-control','rows'=>2])}}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('birthPlace','Место рождения')}}
                {{Form::text('birthPlace',$enrollee->getPassportData('birthPlace'),['class'=>'form-control'])}}
            </div>
        </div>
    </form>
</div>
