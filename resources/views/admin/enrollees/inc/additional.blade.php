<div class="additional-data">
    <p>
        <span data-prefix="Подан оригинал документа: ">{{$enrollee->getHasOriginalStr()}}</span>
        <span data-prefix="Подана медицинская справка: ">{{$enrollee->getMedCertificateStr()}}</span>
        <span data-prefix="Дата подачи документов: ">{{$enrollee->getStartDate()}}</span>
        <span data-prefix="Нуждается в общежитии: ">{{$enrollee->getNeedHostelStr()}}</span>
        <span data-prefix="Договор целевого набора: ">{{$enrollee->getContractTargetSetStr()}}</span>
        <span data-prefix="Льготная категория: ">{{$enrollee->getPrivelegeCategory()}}</span>
    </p>
</div>

<div class="additional-form">
    <form action="{{$enrollee->getUpdateAdditionalUrl()}}" id="edit-additional-form" name="edit-additional-form"
          method="post">
        <div class="form-group">
            {{Form::label('privileged_category','Льготная категория')}}
            {{Form::select('privileged_category',getPrivilegeCategories(),$enrollee->getPrivelegeCategory(true),['class'=>'form-control','placeholder'=>'Нет льгот'])}}
        </div>
        <div class="form-group">
            <label for="graduation_date">Дата подачи документов</label>
            <input class="form-control pull-right" id="start_date1" type="text" name="start_date"
                   required value="{{$enrollee->getStartDate()}}">
        </div>
        <div class="form-group">
            <label for="has_original">
                {{Form::checkbox('has_original',null,$enrollee->getHasOriginal(),['id'=>'has_original'])}}
                Подан оригинал документа
            </label>
        </div>
        <div class="form-group">
            <label for="has_certificate">
                {{Form::checkbox('has_certificate',null,$enrollee->getMedCertificate(),['id'=>'has_certificate'])}}
                Подана медицинская справка
            </label>
        </div>
        <div class="form-group">
            <label for="need_hostel">
                {{Form::checkbox('need_hostel',null,$enrollee->getNeedHostel(),['id'=>'need_hostel'])}}
                Нуждается в общежитии
            </label>
        </div>
        <div class="form-group">
            <label for="contract_target_set">
                {{Form::checkbox('contract_target_set',null,$enrollee->getContractTargetSet(),['id'=>'contract_target_set'])}}
                Договор целевого набора
            </label>
        </div>
    </form>
</div>
@push('scripts')
    <script>
        $('#start_date1').datepicker(datepicker_conf())
    </script>
@endpush