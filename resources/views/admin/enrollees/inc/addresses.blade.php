<div class="addresses-data">
    <div class="col-xs-12">
        <h4>Адрес регистрации</h4>
        <p>
            <span data-prefix="Область: ">{{$enrollee->getAddresses('registration','region')}}</span>
            <span data-prefix="Район: ">{{$enrollee->getAddresses('registration','area')}}</span>
            <span data-prefix="Город: ">{{$enrollee->getAddresses('registration','city')}}</span>
            <span data-prefix="Населенный пункт: ">{{$enrollee->getAddresses('registration','settlement')}}</span>
            <span data-prefix="Индекс: ">{{$enrollee->getAddresses('registration','index')}}</span>
            <span data-prefix="Улица: ">{{$enrollee->getAddresses('registration','street')}}</span>
            <span data-prefix="Дом: ">{{$enrollee->getAddresses('registration','house_number')}}</span>
            <span data-prefix="Корпус: ">{{$enrollee->getAddresses('registration','housing')}}</span>
            <span data-prefix="Квартира: ">{{$enrollee->getAddresses('registration','apartment_number')}}</span>
        </p>
    </div>
    <br>
    <div class="col-xs-12">
        <h4>Адрес проживания</h4>
        <p>
            <span data-prefix="Область: ">{{$enrollee->getAddresses('residential','region')}}</span>
            <span data-prefix="Район: ">{{$enrollee->getAddresses('residential','area')}}</span>
            <span data-prefix="Город: ">{{$enrollee->getAddresses('residential','city')}}</span>
            <span data-prefix="Населенный пункт: ">{{$enrollee->getAddresses('residential','settlement')}}</span>
            <span data-prefix="Индекс: ">{{$enrollee->getAddresses('residential','index')}}</span>
            <span data-prefix="Улица: ">{{$enrollee->getAddresses('residential','street')}}</span>
            <span data-prefix="Дом: ">{{$enrollee->getAddresses('residential','house_number')}}</span>
            <span data-prefix="Корпус: ">{{$enrollee->getAddresses('residential','housing')}}</span>
            <span data-prefix="Квартира: ">{{$enrollee->getAddresses('residential','apartment_number')}}</span>
        </p>
    </div>
    <br>
    <div class="col-xs-12">
        <h4>Адрес регистрации по месту пребывания</h4>
        <p>
            <span data-prefix="Область: ">{{$enrollee->getAddresses('place_of_stay','region')}}</span>
            <span data-prefix="Район: ">{{$enrollee->getAddresses('place_of_stay','area')}}</span>
            <span data-prefix="Город: ">{{$enrollee->getAddresses('place_of_stay','city')}}</span>
            <span data-prefix="Населенный пункт: ">{{$enrollee->getAddresses('place_of_stay','settlement')}}</span>
            <span data-prefix="Индекс: ">{{$enrollee->getAddresses('place_of_stay','index')}}</span>
            <span data-prefix="Улица: ">{{$enrollee->getAddresses('place_of_stay','street')}}</span>
            <span data-prefix="Дом: ">{{$enrollee->getAddresses('place_of_stay','house_number')}}</span>
            <span data-prefix="Корпус: ">{{$enrollee->getAddresses('place_of_stay','housing')}}</span>
            <span data-prefix="Квартира: ">{{$enrollee->getAddresses('place_of_stay','apartment_number')}}</span>
        </p>
    </div>
</div>
<div class="addresses-form">
    <form action="{{$enrollee->getUpdateAddressesUrl()}}" method="post" name="update-addresses-form"
          id="update-addresses-form">
        <div class="registration address">
            <div class="col-xs-12">
                <h4 style="display: inline-block">Адрес регистрации</h4>
                <div class="pull-right">
                    <label for="isAddressSimilar">
                        {{Form::checkbox('isAddressSimilar',null,$enrollee->getAddresses('isAddressSimilar'),['id'=>'isAddressSimilar'])}}
                        Совпадает с адресом проживания
                    </label><br>
                    <label for="isAddressPlaceOfStay">
                        {{Form::checkbox('isAddressPlaceOfStay',null,$enrollee->getAddresses('isAddressPlaceOfStay'),['id'=>'isAddressPlaceOfStay'])}}
                        Совпадает с рег. по месту приб.
                    </label>
                </div>
                <div class="form-group">
                    {{Form::label('registration[region]','Область')}}
                    {{Form::text('registration[region]',$enrollee->getAddresses('registration','region'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[area]','Район')}}
                    {{Form::text('registration[area]',$enrollee->getAddresses('registration','area'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[settlement]','Населенный пункт')}}
                    {{Form::text('registration[settlement]',$enrollee->getAddresses('registration','settlement'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[city]','Город')}}
                    {{Form::text('registration[city]',$enrollee->getAddresses('registration','city'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[index]','Индекс')}}
                    {{Form::text('registration[index]',$enrollee->getAddresses('registration','index'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[street]','Улица')}}
                    {{Form::text('registration[street]',$enrollee->getAddresses('registration','street'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[house_number]','Дом')}}
                    {{Form::text('registration[house_number]',$enrollee->getAddresses('registration','house_number'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[housing]','Корпус')}}
                    {{Form::text('registration[housing]',$enrollee->getAddresses('registration','housing'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('registration[apartment_number]','Квартира')}}
                    {{Form::text('registration[apartment_number]',$enrollee->getAddresses('registration','apartment_number'),['class'=>'form-control'])}}
                </div>
            </div>
        </div>

        <div id="residential" class="address"
             @if($enrollee->getAddresses('isAddressSimilar'))style="display: none" @endif>
            <div class="col-xs-12">
                <h4>Адрес проживания</h4>
                <div class="form-group">
                    {{Form::label('residential[region]','Область')}}
                    {{Form::text('residential[region]',$enrollee->getAddresses('residential','region'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[area]','Район')}}
                    {{Form::text('residential[area]',$enrollee->getAddresses('residential','area'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[settlement]','Населенный пункт')}}
                    {{Form::text('residential[settlement]',$enrollee->getAddresses('residential','settlement'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[city]','Город')}}
                    {{Form::text('residential[city]',$enrollee->getAddresses('residential','city'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[index]','Индекс')}}
                    {{Form::text('residential[index]',$enrollee->getAddresses('residential','index'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[street]','Улица')}}
                    {{Form::text('residential[street]',$enrollee->getAddresses('residential','street'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[house_number]','Дом')}}
                    {{Form::text('residential[house_number]',$enrollee->getAddresses('residential','house_number'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[housing]','Корпус')}}
                    {{Form::text('residential[housing]',$enrollee->getAddresses('residential','housing'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('residential[apartment_number]','Квартира')}}
                    {{Form::text('residential[apartment_number]',$enrollee->getAddresses('residential','apartment_number'),['class'=>'form-control'])}}
                </div>
            </div>
        </div>

        <div id="place_of_stay" class="address" @if($enrollee->getAddresses('isAddressPlaceOfStay'))style="display: none" @endif>
            <div class="col-xs-12">
                <h4>Адрес регистрации по месту пребывания</h4>
                <div class="form-group">
                    {{Form::label('place_of_stay[region]','Область')}}
                    {{Form::text('place_of_stay[region]',$enrollee->getAddresses('place_of_stay','region'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[area]','Район')}}
                    {{Form::text('place_of_stay[area]',$enrollee->getAddresses('place_of_stay','area'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[settlement]','Населенный пункт')}}
                    {{Form::text('place_of_stay[settlement]',$enrollee->getAddresses('place_of_stay','settlement'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[city]','Город')}}
                    {{Form::text('place_of_stay[city]',$enrollee->getAddresses('place_of_stay','city'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[index]','Индекс')}}
                    {{Form::text('place_of_stay[index]',$enrollee->getAddresses('place_of_stay','index'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[street]','Улица')}}
                    {{Form::text('place_of_stay[street]',$enrollee->getAddresses('place_of_stay','street'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[house_number]','Дом')}}
                    {{Form::text('place_of_stay[house_number]',$enrollee->getAddresses('place_of_stay','house_number'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[housing]','Корпус')}}
                    {{Form::text('place_of_stay[housing]',$enrollee->getAddresses('residential','housing'),['class'=>'form-control'])}}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{Form::label('place_of_stay[apartment_number]','Квартира')}}
                    {{Form::text('place_of_stay[apartment_number]',$enrollee->getAddresses('place_of_stay','apartment_number'),['class'=>'form-control'])}}
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-xs-offset-4 text-center">
            <button class="btn btn-success" type="submit">
                Сохранить
            </button>
        </div>
    </form>
</div>