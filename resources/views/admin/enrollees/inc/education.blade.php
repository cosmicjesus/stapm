<div class="education-data">
    <p>
        <span data-prefix="Образование: ">{{$enrollee->getEducation()}}</span>
        <span data-prefix="Дата окончания: ">{{$enrollee->getGraduationDate()}}</span>
        @if($enrollee->getGraduationOrganizationName())
            <span data-prefix="Организация: ">{{$enrollee->getGraduationOrganizationName()}}</span>
        @endif

        @if($enrollee->getGraduationOrganizationPlace())
            <span data-prefix="Населенный пункт: ">{{$enrollee->getGraduationOrganizationPlace()}}</span>
        @endif
    </p>
</div>

<div class="education-form">
    <form action="{{$enrollee->getUpdateEducationUrl()}}" id="edit-enroll-education-form" name="edit-enroll-education-form" method="post">
        <div class="form-group">
            {{Form::label('education_id','Уровень образования')}}
            <select name="education_id" id="education_id" class="form-control">
                @foreach(getEducations() as $education)
                    <option value="{{$education->id}}"
                            @if($enrollee->getEducationId()===$education->id) selected @endif>{{$education->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="graduation_date">Дата окончания предыдущего обучения</label>
            <input class="form-control pull-right" id="graduation_date" type="text" name="graduation_date"
                   required value="{{$enrollee->getGraduationDate()}}">
        </div>
        <div class="form-group">
            {{Form::label('graduation_organization_name','Организация')}}
            {{Form::text('graduation_organization_name',$enrollee->getGraduationOrganizationName(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('graduation_organization_place','Населенный пункт')}}
            {{Form::text('graduation_organization_place',$enrollee->getGraduationOrganizationPlace(),['class'=>'form-control'])}}
        </div>
    </form>
</div>