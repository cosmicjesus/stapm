<div class="parents-data">
    @if($enrollee->getParents()->count()>0)
        <table class="table table-bordered table-striped">
            <tr>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Родство</th>
                <th></th>
            </tr>
            @foreach($enrollee->getParents() as $parent)
                <tr>
                    <td>{{$parent->getFullName()}}</td>
                    <td>{{$parent->getPhone()}}</td>
                    <td>{{$parent->getRelationShipTypeStr()}}</td>
                    <td></td>
                </tr>
            @endforeach
        </table>
    @else
        <h4>Данные не заполнены</h4>
    @endif
</div>

<div class="parents-form" style="display: none">
    <form action="{{$enrollee->getAddParentUrl()}}" id="add-parents-form" name="add-parents-form"
          method="post">
        <div class="form-group">
            {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
            {{Form::text('lastname',null,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('firstname','Имя',['class'=>'control-label'])}}
            {{Form::text('firstname',null,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
            {{Form::text('middlename',null,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('gender','Пол',['class'=>'control-label'])}}
            {{Form::select('gender',genders(),0,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('relationshipType','Родство',['class'=>'control-label'])}}
            {{Form::select('relationshipType',getRelationShipTypes(),0,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('phone','Телефон',['class'=>'control-label'])}}
            {{Form::text('phone',null,['class'=>'form-control'])}}
        </div>
        <div class="col-md-12 text-center">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-plus"></i> Добавить
            </button>
            <button class="btn btn-danger close-add-parent-form-js" type="button">
                <i class="fa fa-times-circle-o"></i> Отмена
            </button>
        </div>
    </form>
</div>