@extends('admin.layouts.layout')

@section('content')

    <div class="col-md-4">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Основные сведения</h3>
            </div>
            <div class="box-body box-profile">
                {{--<h3 class="profile-username text-center">{{$enrollee->getFullName()}}</h3>--}}

                {{--<p class="text-muted text-center">Software Engineer</p>--}}

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Пол</b> <a class="pull-right">{{$enrollee->getGenderToStr()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Дата рождения</b> <a class="pull-right">{{$enrollee->getBirthday()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Возраст</b> <a class="pull-right">{{$enrollee->getAge()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Гражданство</b> <a class="pull-right">{{$enrollee->getNationality()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Профессия</b>
                        <br>
                        <a class="">{{$enrollee->getProgram()->getNameWithForm()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Целевой набор</b> <a class="pull-right">{{$enrollee->getContractTargetSet()?'Да':'Нет'}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Средний балл</b> <a class="pull-right">{{$enrollee->getAvg()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>СНИЛС</b> <a class="pull-right">{{$enrollee->getSnils()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>ИНН</b> <a class="pull-right">{{$enrollee->getInn()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Изучаемый язык</b> <a class="pull-right">{{$enrollee->getLanguage()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Инвалидность</b> <a class="pull-right">{{$enrollee->getDisability()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Категория здоровья</b> <a class="pull-right">{{$enrollee->getHealthCategory()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Телефоны</b> <a class="pull-right">{{$enrollee->getPhone()}}</a>
                    </li>
                </ul>
                @if(checkPermissions(['enrollees.actions']))
                    <a href="{{$enrollee->getEditLink()}}" class="btn btn-primary btn-block"><b>Редактировать</b></a>
                @endif
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-8">
        <!--Дополнительные сведения-->
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Дополнительные сведения</h3>
                    <button class="btn btn-success save-additional-js" style="float: right;display: none">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-primary edit-additional-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information additional-js">
                                @include('admin.enrollees.inc.additional',['enrollee'=>$enrollee])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Дополнительные сведения-->
        <!--Образование-->
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Образование</h3>
                    <button class="btn btn-success save-education-js" style="float: right;display: none">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-primary edit-education-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information education-js">
                                @include('admin.enrollees.inc.education',['enrollee'=>$enrollee])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Образование-->
        <!--Документ уд.личность-->
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Документ уд.личность</h3>
                    <button class="btn btn-danger exit-passport-js" style="float: right;margin-left: 5px;display: none">
                        <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-success save-passport-js" style="float: right;display: none">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-primary edit-passport-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information passport-js">
                                @include('admin.enrollees.inc.passport',['enrollee'=>$enrollee])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Документ уд.личность-->
        <!--Родители-->
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Родители</h3>
                    <button class="btn btn-primary add-parent-js" style="float: right">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information parents-js">
                                @include('admin.enrollees.inc.parents',['enrollee'=>$enrollee])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Родители-->
        <!--Адреса-->
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Адреса</h3>
                    <button class="btn btn-primary edit-addresses-js" style="float: right">
                        <i class="fa fa-edit"></i>
                    </button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="information addresses-js">
                                @include('admin.enrollees.inc.addresses',['enrollee'=>$enrollee])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Адреса-->
    </div>
@endsection