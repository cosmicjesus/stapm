@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.enrollee.update',$enrollee->id],'id'=>'enrollee-edit-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
            {{Form::text('lastname',$enrollee->lastname,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('firstname','Имя',['class'=>'control-label'])}}
            {{Form::text('firstname',$enrollee->firstname,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
            {{Form::text('middlename',$enrollee->middlename,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label>Дата рождения</label>
                <input class="form-control pull-right" id="birthday" type="text" name="birthday"
                       value="{{$enrollee->birthday->format('d.m.Y')}}" required>
            </div>
            <div class="col-xs-6">
                {{Form::label('gender','Пол',['class'=>'control-label'])}}
                {{Form::select('gender',genders(),$enrollee->gender,['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('nationality','Гражданство')}}
            {{Form::select('nationality',getCountrues(),$enrollee->nationality,['class'=>'form-control','placeholder'=>'Не задано'])}}
        </div>
        <div class="form-group">
            <label for="profession_program_id" class="control-label">Профессия</label>
            <select name="profession_program_id" id="program" class="form-control" required>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}"
                            @if($enrollee->profession_program_id==$program->getId()) selected @endif>{{$program->getNameWithForm()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('snils','СНИЛС',['class'=>'control-label'])}}
            {{Form::text('snils',$enrollee->snils,['class'=>'form-control','placeholder'=>'Номер СНИЛС вводить без черточек','max'=>11])}}
        </div>
        <div class="form-group">
            {{Form::label('inn','ИНН',['class'=>'control-label'])}}
            {{Form::text('inn',$enrollee->inn,['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('language','Изучаемый язык')}}
            {{Form::select('language',getLanguages(),$enrollee->language,['class'=>'form-control','placeholder'=>'- Не задано -'])}}
        </div>
        <div class="form-group">
            {{Form::label('health_category','Категория здоровья')}}
            {{Form::select('health_category',getHealthCategories(),$enrollee->health_category,['class'=>'form-control','placeholder'=>'- Не задано -'])}}
        </div>
        <div class="form-group">
            {{Form::label('disability','Инвалидность')}}
            {{Form::select('disability',getDisabilities(),$enrollee->disability,['class'=>'form-control','placeholder'=>'- Не задано -'])}}
        </div>
        <div class="form-group">
            {{Form::label('avg','Средний балл')}}
            <div class="input-group">
                {{Form::number('avg',$enrollee->avg,['class'=>'form-control','min'=>2,'max'=>5,'step'=>'0.0001'])}}
                <span class="input-group-btn">
                    <a class="btn btn-primary avg-calc-modal-js" data-toggle="modal" data-target="#avg-calc-modal">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </a>
                </span>
            </div>

        </div>
        <div class="form-group">
            {{Form::label('phone','Телефон',['class'=>'control-label'])}}
            {{Form::text('phone',$enrollee->phone,['class'=>'form-control','required'=>'required'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection