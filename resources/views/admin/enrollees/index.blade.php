@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            @if(checkPermissions('enrollees.actions'))
                <div class="btn-group pull-left">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Действия <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" class="enrollment-js" data-toggle="modal" data-target="#enrollment-modal">Зачислить</a>
                        </li>
                        <li><a href="#" class="delete-enrollees-js">Удалить</a></li>
                        <li class="divider"></li>
                        <li><a href="{{route('admin.export.enrollees.asurso')}}" target="_blank">Сгенерировать файл
                                импорта для АСУ РСО</a></li>
                    </ul>
                </div>
            @endif
            @if(checkPermissions('enrollees.create'))
                <a href="{{route('admin.enrollees.create.page')}}" class="btn btn-success pull-right">
                    <i class="fa fa-plus"></i> Добавить абитуриента
                </a>
            @endif
        </div>
    </div>
    <div class="row" style="margin-top: 10px;padding-right: 16px">
        <form id="filter-enrollees-table-js">
            <div class="col-sm-10">
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="ФИО">
                    </div>

                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <select name="professions" id="professions" class="form-control">
                            <option value="" selected>---Все специальности---</option>
                            @foreach($programs as $program)
                                <option value="{{$program->getId()}}">{{$program->getNameWithForm()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <select name="documents" id="documents" class="form-control">
                            <option value="" selected>---Все абитуриенты---</option>
                            <option value="1">Только с оригиналами</option>
                            <option value="2">Только без оригиналов</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <select name="medical" id="medical" class="form-control">
                            <option value="" selected>---Наличие медсправки---</option>
                            <option value="1">C медсправкой</option>
                            <option value="2">Без медсправки</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="contract_target_set">
                            <input type="checkbox" name="contract_target_set" id="contract_target_set"> Только целевики
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="no_snils">
                            <input type="checkbox" name="no_snils" id="no_snils"> Нет СНИЛС
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="no_inn">
                            <input type="checkbox" name="no_inn" id="no_inn"> Нет ИНН
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="no_passport">
                            <input type="checkbox" name="no_passport" id="no_passport"> Нет данных паспорта
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="no_addresses">
                            <input type="checkbox" name="no_addresses" id="no_addresses"> Нет адресов
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="need_hostel">
                            <input type="checkbox" name="need_hostel" id="need_hostel"> Нуждается в общежитии
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">

        <table id="enrollees-table" class="table table-bordered table-striped display nowrap" style="width: 100%">
            <thead>
            <tr>
                <th></th>
                <th>Действия</th>
                <th>Незаполненные <br>
                    данные
                </th>
                <th>Ошибки</th>
                <th>ID</th>
                <th>ФИО</th>
                <th>Программа</th>
                <th>Оригинал документа</th>
                <th>Медсправка</th>
                <th>Нуждается в общежитии</th>
                <th>Паспортные данные</th>
                <th>СНИЛС</th>
                <th>ИНН</th>
                <th>Изучаемый язык</th>
                <th>Ср. балл</th>
                <th>Телефон</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection