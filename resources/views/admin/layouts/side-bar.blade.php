<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Навигация</li>
            <li></li>
            {{Widget::run('Admin\AdminMenu\AdminMenu')}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>