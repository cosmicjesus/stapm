@extends('admin.layouts.layout')

@section('content')
    <div class="col-sm-8 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                @yield('title')
            </div>
            @yield('form')
        </div>
    </div>
    <div class="col-sm-4 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4>Справка по работе с формой</h4>
            </div>
            <div class="box-body">
                @yield('description')
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
@endsection