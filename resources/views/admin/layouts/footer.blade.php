<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 1954-{{\Carbon\Carbon::now()->format('Y')}}
        <a href="{{env('APP_URL')}}" target="_blank" title="Перейти на сайт">СТАПМ</a>.</strong>
</footer>