@extends('admin.layouts.layout')

@section('content')
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
               @yield('content_inner_header')
            </div>
            <div class="box-body">
                @yield("content_inner")
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                @yield('content_inner_footer')
            </div>
        </div>
    </div>
@endsection