@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="#" class="btn btn-danger open-money-reference-modal-js" data-toggle="modal" data-target="#moneyReferenceModal">
                <i class="fa fa-file-pdf"></i> Список справок на стипендию
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="filter-references-table-js">
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="all_helps">
                        <input type="checkbox" name="all_helps" id="all_helps" value="0"> Показать все
                    </label>

                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <select name="referenceType" id="referenceType" class="form-control">
                        <option value="">--Все справки--</option>
                        <option value="training">Только об обучении</option>
                        <option value="money">Только о стипендии</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="reference-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Студент</th>
                <th>Тип</th>
                <th>Даты</th>
                <th>Параметры справки</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection