@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.tochka-rosta.store','id'=>'add-tochka-rosta-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            {{Form::label('title','Заголовок',['class'=>'control-label'])}}
            {{Form::text('title',old('title'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="" class="control-label">Дата публикации</label>
            <div class="input-group date">
                <input name="publication_date" type="text" class="form-control" id="publication_date"
                       @if(old('publication_date')) value="{{old('publication_date')}}"
                       @else value="{{\Carbon\Carbon::now()->format('d.m.Y')}}" @endif >
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('file','Изображение',['class'=>'control-label'])}}
            {{Form::file('file')}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true)}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            {{Form::label('preview','Превью',['class'=>'control-label'])}}
            {{Form::textarea('preview',old('preview'),['class'=>'form-control','id'=>'preview'])}}
        </div>
        <div class="form-group">
            {{Form::label('full_text','Полный текст',['class'=>'control-label'])}}
            {{Form::textarea('full_text',old('full_text'),['class'=>'form-control','id'=>'full_text'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection
