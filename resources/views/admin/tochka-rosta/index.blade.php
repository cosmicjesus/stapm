@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.tochka-rosta.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить событие
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="tochka-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Заголовок</th>
                <th>Изображение</th>
                <th>Дата публикации</th>
                <th>Превью</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
@endsection