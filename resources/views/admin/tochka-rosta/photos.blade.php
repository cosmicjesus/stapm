@extends('admin.layouts.inner')

@section('content_inner_header')

@endsection

@section('content_inner')
    <form action="{{$item->getUploadPhotoUrl()}}" enctype="multipart/form-data" name="uploadTochaPhotosForm"
          id="upload-tochka-photos-form">
        <div class="dropzone">
            Перетащите сюда файлы при помощи мыши<br> или нажмите и выберите файлы на компьютере
        </div>
        <input type="file" style="display: none" multiple name="photos" class="photos-input-js" accept="image/*">
    </form>
    @if($item->getPhotos())
        <div class="row">
            <div class="photos">
                @foreach($item->getPhotos() as $photo)
                    <div class="col-md-4 col-xs-6">
                        <div class="tochka-photo">
                            <a href="{{$photo->getPath()}}" data-fancybox data-caption="{{$item->getTitle()}}">
                                <img src="{{$photo->getPath()}}" alt="{{$item->getTitle()}}" class="img tochka-img">
                            </a>
                            <div class="actions">
                                <a href="{{$photo->getDeleteUrl()}}" class="delete-tochka-photo-js">Удалить фото</a>
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection