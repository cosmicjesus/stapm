@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.users.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить пользователя
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="users-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Логин</th>
                <th>Присоедененный профиль</th>
                <th>Права</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->login}}</td>
                    <td>
                        @if(!is_null($user->profile))
                            <a href="{{route('admin.employee.profile',['slug'=>$user->profile->slug])}}">{{$user->profile->lastname." ".$user->profile->firstname." ".$user->profile->middlename}}</a>
                        @else
                            Нет
                        @endif
                    </td>
                    <td>
                        <ul>
                            @foreach($user->permissions as $permission)
                                <li>{{$permission->ru_name}}</li>
                            @endforeach
                        </ul>

                    </td>
                    <td>
                        <div class="table-buttons">
                            <a href="{{route('admin.users.edit',['id'=>$user->id])}}" class="btn btn-warning btn-xs"><i
                                        class="fa fa-edit"></i></a>
                            <button class="btn btn-danger btn-xs delete-user-js"
                                    data-url="{{route('admin.users.delete',['id'=>$user->id])}}"
                                    @if(!is_null($user->profile))
                                    data-user="{{$user->profile->lastname." ".$user->profile->firstname." ".$user->profile->middlename}}"
                                    @endif>
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection