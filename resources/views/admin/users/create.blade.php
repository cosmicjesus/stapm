@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.users.store','id'=>'add-user-form','class'=>'asyncValidate'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('login','Логин')}}
            {{Form::text('login',null,['class'=>'form-control required'])}}
        </div>
        <div class="form-group">
            {{Form::label('password','Пароль')}}
            <div class="input-group">
                {{Form::text('password',null,['class'=>'form-control password-field-js required'])}}
                <div class="input-group-addon" style="cursor: pointer">
                    <i class="fa fa-repeat random-password-js"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="employee_id" class="control-label">Сотрудник</label>
            <select name="employee_id" id="program" class="form-control">
                <option value="">--Выберите сотрудника--</option>
                @foreach($employees as $employee)
                    <option value="{{$employee->id}}">{{$employee->lastname." ".$employee->firstname." ".$employee->middlename}}</option>
                @endforeach
            </select>
        </div>
            <div class="form-group">
                @foreach($permissions as $key=>$permission)
                    <h4>{{$key}}</h4>
                    <div class="row">
                        @foreach($permission as $item)
                            <div class="col-md-6 col-xs-12 col-lg-4">
                                <label for="{{$item->name}}">
                                    {{Form::checkbox('permissions[]',$item->name,false,['id'=>$item->name,'class'=>'permission-checkbox-js'])}} {{$item->ru_name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                @endforeach
            </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Заполните основные поля и выберите пользователя</p>
@endsection