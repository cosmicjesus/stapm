@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.users.update',$user->id],'id'=>'add-user-form','class'=>'asyncValidate'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('login','Логин')}}
            {{Form::text('login',$user->login,['class'=>'form-control required'])}}
        </div>
        <div class="form-group">
            {{Form::label('password','Пароль')}}
            <div class="input-group">
                {{Form::text('password',null,['class'=>'form-control password-field-js'])}}
                <div class="input-group-addon" style="cursor: pointer">
                    <i class="fa fa-repeat random-password-js"></i>
                </div>
            </div>
        </div>
        {{--<div class="form-group">--}}
        {{--<label for="">Права доступа</label>--}}
        {{--<div class="row">--}}
        {{--@foreach($permissions as $permission)--}}
        {{--<div class="col-md-6 col-xs-12 col-lg-4">--}}
        {{--<label for="{{$permission->name}}">--}}
        {{--{{Form::checkbox('permissions[]',$permission->name,in_array($permission->name,$user_permissions),['id'=>$permission->name,'class'=>'permission-checkbox-js'])}} {{$permission->ru_name}}--}}
        {{--</label>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="form-group">
            @foreach($permissions as $key=>$permission)
                <h4>{{$key}}</h4>
                <div class="row">
                    @foreach($permission as $item)
                        <div class="col-md-6 col-xs-12 col-lg-4">
                            <label for="{{$item->name}}">
                                {{Form::checkbox('permissions[]',$item->name,in_array($item->name,$user_permissions),['id'=>$item->name,'class'=>'permission-checkbox-js'])}} {{$item->ru_name}}
                            </label>
                        </div>
                    @endforeach
                </div>
                <hr>
            @endforeach
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Измените нужную информацию</p>
    <p>Если не нужно менять пароль то оставьте соответствующее поле пустым</p>
@endsection