@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.education-plans.createPage')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить план
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="filter_plans" name="filter_plans">
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Название">
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <select name="program_id" id="program_id" class="form-control">
                        <option value="" selected>---Все программы---</option>
                        @foreach($programs as $program)
                            <option value="{{$program->getId()}}">{{$program->getNameWithForm()}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table id="eduplan-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование</th>
                <th>Программа</th>
                <th>Период обучения</th>
                <th>Файл</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
