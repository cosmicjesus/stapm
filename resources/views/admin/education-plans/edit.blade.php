@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.education-plans.edit',$plan->getId()],'id'=>'eduplan-edit-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="program" class="control-label">Профессиональная программа</label>
            <select name="program" id="program" class="form-control">
                <option value="">--Выберите ОПОП--</option>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}" data-period="{{$program->getPeriodToInt()}}"
                            @if((old('program')==$program->getId())||($program->getId()==$plan->getProgramModel()->getId())) selected @endif>{{$program->getName()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('name','Название',['class'=>'control-label'])}}
            {{Form::text('name',$plan->getName(),['class'=>'form-control','id'=>'name'])}}
        </div>
        <div class="form-group">
            {{Form::label('period','Количество семестров',['class'=>'control-label'])}}
            {{Form::number('period',old('period')?old('period'):$plan->getCountPeriod(),['class'=>'form-control','id'=>'period','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="" class="control-label">Период обучения</label>
            <div class="input-group input-daterange">
                <div class="input-group-addon">C</div>
                <input name="start_training" type="text" class="form-control" id="start_training"
                       value="{{$plan->getStartTrainig()}}">
                <div class="input-group-addon">по</div>
                <input name="end_training" type="text" class="form-control" id="end_training"
                       value="{{$plan->getEndTrainig()}}">
            </div>
        </div>

        <div class="form-group">
            @if($plan->getFilePath())
                <a href="{{$plan->getFilePath()}}" target="_blank">Загруженный файл</a><br>
            @endif
            {{Form::label('file','Файл',['class'=>'control-label'])}}
            {{Form::file('file')}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',1,$plan->getActiveStatus())}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            <label for="in_revision">
                {{Form::checkbox('in_revision',1,$plan->getRevisionStatus())}}
                На доработке
            </label>
        </div>
        <div class="form-group">
            <label for="dual_training">
                {{Form::checkbox('options[dual_training]',old('options[dual_training]'),$plan->getExtraOption('dual_training'),['id'=>'dual_training'])}}
                Дуальное обучение
            </label>
        </div>
        <div class="form-group">
            <label for="adapted_program">
                {{Form::checkbox('options[adapted_program]',old('options[adapted_program]'),$plan->getExtraOption('adapted_program'),['id'=>'adapted_program'])}}
                Адаптированная ОП
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Измените нужную информацию</p>
    <p>Ecли нужно заменить файл, то выберите его в соответствующем поле</p>
@endsection