@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.education-plans.create','id'=>'eduplan-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="program" class="control-label">Профессиональная программа</label>
            <select name="program" id="program" class="form-control">
                <option value="">--Выберите ОПОП--</option>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}" data-period="{{$program->getPeriodToInt()}}"
                            @if(old('program')==$program->getId()) selected @endif>{{$program->getNameWithForm()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('name','Название',['class'=>'control-label'])}}
            {{Form::text('name',old('name'),['class'=>'form-control','id'=>'name'])}}
        </div>
        <div class="form-group">
            {{Form::label('period','Количество семестров',['class'=>'control-label'])}}
            {{Form::number('period',old('period')?old('period'):6,['class'=>'form-control','id'=>'period','min'=>1])}}
        </div>
        <div class="form-group">
            <label for="" class="control-label">Период обучения</label>
            <div class="input-group input-daterange">
                <div class="input-group-addon">C</div>
                <input name="start_training" type="text" class="form-control" id="start_training"
                       value="{{\Carbon\Carbon::create(\Carbon\Carbon::now()->format('Y'),9,01)->format('d.m.Y')}}">
                <div class="input-group-addon">по</div>
                <input name="end_training" type="text" class="form-control" id="end_training">
            </div>
        </div>

        <div class="form-group">
            {{Form::label('file','Файл',['class'=>'control-label'])}}
            {{Form::file('file')}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true,['id'=>'active'])}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            <label for="dual_training">
                {{Form::checkbox('options[dual_training]',old('options[dual_training]')?1:old('options[dual_training]'),false,['id'=>'dual_training'])}}
                Дуальное обучение
            </label>
        </div>
        <div class="form-group">
            <label for="adapted_program">
                {{Form::checkbox('options[adapted_program]',old('options[adapted_program]')?1:old('options[adapted_program]'),false,['id'=>'adapted_program'])}}
                Адаптированная ОП
            </label>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection