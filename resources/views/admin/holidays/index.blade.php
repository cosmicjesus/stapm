@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="#" data-toggle="modal" data-target="#addHolidayModal" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить праздник
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="holidays-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Дата начала</th>
                <th>Дата окончания</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
