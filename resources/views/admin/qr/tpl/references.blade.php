@extends('admin.qr.layout')
@section('content')
    <div class="text-center">
        <h2>Уважаемые студенты!</h2>
        <h4>Вы можете заказать справку об обучении на сайте по адресу
            <br>
            <br>
            <span style="font-size: 20px">
            {{route('order-references')}}
            </span>
            <br>
            <br>
            или перейти на страницу отсканировав код ниже</h4>
        <div style="margin: 0 auto">
            <img style="display: block;margin: 0 auto" src="
             data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(500,500)->generate(route('order-references')))!!}
                    " alt="">
        </div>
    </div>
@endsection