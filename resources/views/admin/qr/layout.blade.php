<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
<div class="report-wrapper">
    <div style="clear: both;border-bottom: 2px solid black">
        <div style="float:left;width: 70%">
            <div style="display: block;margin: 0 auto;font-size: 16px;text-align: center">
                Государственное бюджетное профессиональное образовательное учреждение Самарской области
                «Самарский техникум авиационного и промышленного машиностроения
                имени Д.И.Козлова»
            </div>
        </div>
        <div style="float:right;width: 29%;text-align: right">
            <img src="/img/default-news-img.jpg" style="width: 120px" alt="Лого">
        </div>
    </div>
    @yield('content')
</div>