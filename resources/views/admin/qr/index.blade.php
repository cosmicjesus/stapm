@extends('admin.layouts.inner')

@section('content_inner_header')

@endsection

@section('content_inner')
    <div class="table-responsive priem-table-js">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Описание</th>
                <th>Действия</th>
            </tr>
            @foreach($rows as $row)
                <tr>
                    <td>{{$row['name']}}</td>
                    <td>
                        <a class="btn btn-xs btn-success" href="{{$row['route']}}" target="_blank">
                            <i class="fa fa-print"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection