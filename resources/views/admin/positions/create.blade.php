@extends('admin.layouts.layout')

@section('content')
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                @yield('title')
            </div>
            <div class="box-body">
                {{request()->has('name')}}
                @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {{Form::open(['route'=>'admin.position.create.post'])}}
                <div class="form-group">
                    {{Form::label('name','Haименование',['class'=>'control-label'])}}
                    {{Form::text('name',old('name'),['class'=>'form-control'])}}
                </div>
                <div class="form-group @if(request()->has('name')) has-error @endif">
                    {{Form::label('type','Тип',['class'=>'control-label'])}}
                    {{Form::select('type',$types,old('type'),['class'=>'form-control'])}}
                </div>
                <div>
                    {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
                    {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
                </div>
                {{Form::close()}}
            </div>

            <div class="box-footer">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4>Справка по работе с формой</h4>
            </div>
            <div class="box-body">
                <p>Укажите наименование должности и выберите тип</p>
            </div>

            <div class="box-footer">
            </div>
        </div>
    </div>
    </div>
@endsection