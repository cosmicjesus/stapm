@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.position.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить должность
            </a>
        </div>

    </div>
    <div class="row" style="margin-top: 10px">
        <form id="search-form">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" placeholder="Наименование">
            </div>

        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <select name="type" id="type" class="form-control">
                    <option value="" selected>---Все типы---</option>
                    @foreach(positionTypes() as $key => $type)
                        <option value="{{$key}}">{{$type}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <button type="submit" class="btn btn-primary">
                Применить
            </button>
        </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <table id="positions-table" class="table table-bordered table-striped" style="width: 100% !important;">
        <thead>
        <tr>
            <th>ID</th>
            <th>Наименование</th>
            <th>Тип</th>
            <th>Назначено сотрудников</th>
            <th>Действия</th>
        </tr>
        </thead>
    </table>
@endsection