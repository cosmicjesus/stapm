@extends('admin.layouts.layout')

@section('content')

    <div class="col-md-5">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                {{Form::open(['method'=>'post','name'=>'user_photo','route'=>['admin.employee.photo',$employee->getSlug()],'files'=>true])}}
                <div class="employee-photo {{$employee->hasPhoto()?'delete-empl-photo':''}}"
                     data-slug="{{$employee->getSlug()}}">
                    <img class="profile-user-img img-responsive" src="{{$employee->getPhoto()}}"
                         alt="{{$employee->getFullName()}}">
                </div>
                @if(checkPermissions(['employees.actions']))
                    {{Form::file('photo',['style'=>'display:none','id'=>'user_photo'])}}
                @endif
                {{Form::close()}}
                <h3 class="profile-username text-center">{{$employee->getFullName()}}</h3>

                {{--<p class="text-muted text-center">Software Engineer</p>--}}

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Дата рождения</b> <a class="pull-right">{{$employee->getBirthday()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Возраст</b> <a class="pull-right">{{$employee->getAge()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Категория</b> <a class="pull-right">{{$employee->getCategory()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Общий стаж работы</b> <a class="pull-right">{{$employee->getGeneralExperience()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Стаж работы по специальности</b> <a class="pull-right">{{$employee->getSpecialtyExp()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Ученая степень</b> <a class="pull-right">{{$employee->getScientificDegree()}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Ученое звание</b> <a class="pull-right">{{$employee->getAcademicTitle()}}</a>
                    </li>
                    @if($employee->getEmails())
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right">{{$employee->getEmails()}}</a>
                        </li>
                    @endif
                    @if($employee->getPhones())
                        <li class="list-group-item">
                            <b>Телефон</b> <a class="pull-right">{{$employee->getPhones()}}</a>
                        </li>
                    @endif
                </ul>
                @if(checkPermissions(['employees.actions']))
                    <a href="{{$employee->getEditLink()}}" class="btn btn-primary btn-block"><b>Редактировать</b></a>
                @endif
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-7">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#positions" data-toggle="tab">Должности</a></li>
                <li><a href="#timeline" data-toggle="tab">Курсы</a></li>
                <li><a href="#subjects" data-toggle="tab">Преподаваемые дисциплины</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="positions">
                    @if(checkPermissions(['employees.actions']))
                        <a href="#" data-toggle="modal" data-target="#add_position_modal"
                           data-id="{{$employee->getId()}}"
                           class="btn btn-primary add-position-js">
                            <i class="fa fa-plus"></i> Добавить должность
                        </a>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>Наименование</th>
                                <th>Дата назначения</th>
                                <th>Стаж работы</th>
                                <th>Тип</th>
                                <th></th>
                            </tr>
                            @foreach($employee->getPositions() as $position)
                                <tr data-position-id="{{$position->getId()}}">
                                    <td>{{$position->getName()}}</td>
                                    <td>{{$position->getPeriod()}}</td>
                                    <td>{{$position->getExp()}}</td>
                                    <td>{{$position->getType()}}</td>
                                    <td>
                                        @if((count($employee->getPositions())>1)&& checkPermissions(['employees.actions']))
                                            <div class="table-buttons">
                                                @if(is_null($position->getLayoffDate()))
                                                    <a href="#" data-target="#layoff_modal" data-toggle="modal" data-
                                                       class="btn btn-xs btn-primary layoff-position-js"
                                                       data-start-date="{{$position->getStartDate()}}"
                                                       data-id="{{$position->getId()}}">
                                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                                <button class="btn btn-xs btn-danger delete-empl-position-js"
                                                        data-id="{{$position->getId()}}"
                                                        data-name="{{$position->getName()}}"
                                                        data-employee_id="{{$employee->getId()}}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                    @if(checkPermissions(['employees.actions']))
                        <a href="#" class="btn btn-primary add-course--modal-js" data-id="{{$employee->getId()}}"
                           data-toggle="modal" data-target="#add_course_form"><i class="fa fa-plus"></i> Добавить
                            курс</a>
                    @endif
                    <div class="table-responsive">
                        <table id="qcourses"
                               data-url="{{route('admin.employee.getcourses',['slug'=>$employee->getId()])}}"
                               class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Наименование</th>
                                <th>Период прохождения</th>
                                <th>Кол-во часов</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="subjects">
                    <div class="row">
                        <div class="col-xs-12">
                            @if(checkPermissions(['employees.actions']))
                                <form class="form-horizontal" name="employee-subject" id="employee-subject-form"
                                      action="{{$employee->getAddSubjectUrl()}}">
                                    <input type="hidden" value="{{$employee->getId()}}" name="employee_id">
                                    <div class="form-group">
                                        <div class="col-xs-10">
                                            <select name="subject_id" id="subject_select" class="select-2-js" required>
                                                @foreach(getSubjects() as $subject)
                                                    <optgroup label="{{$subject['title']}}">
                                                        @foreach($subject['items'] as $item)
                                                            <option value="{{$item['id']}}">{{$item['name']}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-2">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-plus"></i> <span
                                                        class="hidden-sm hidden-xs">Добавить</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped employee-subjects-table">
                                    @if($employee->getSubjects())
                                        @foreach($employee->getSubjects() as $subject)
                                            <tr class="employee-subject-row">
                                                <td>{{$subject->getName()}}</td>
                                                <td>
                                                    <button class="btn btn-danger delete-employee-subject-js"
                                                            data-url="{{$subject->getDeleteUrl()}}"
                                                            data-name="{{$subject->getName()}}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Образование</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12" style="margin-bottom: 5px">
                        {{--<strong><i class="fa fa-book margin-r-5"></i> Образование</strong>--}}
                        @if(checkPermissions(['employees.actions']))
                            <a href="#" data-toggle="modal" data-target="#add_education"
                               class="btn btn-success pull-right btn-xs add-education-js"
                               data-employee-id="{{$employee->getId()}}"><i class="fa fa-plus"></i>
                                Добавить уровень
                                образования
                            </a>
                        @endif
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Уровень образования</th>
                                    <th>Направление подготовки</th>
                                    <th>Квалификация</th>
                                    <th></th>
                                </tr>
                                <tbody class="education-table-body-js">
                                @foreach($employee->getEducations() as $education)
                                    <tr data-edu-id="{{$education->getId()}}">
                                        <td>{{$education->getName()}}</td>
                                        <td>{{$education->getDirectionOfPreparation()}}</td>
                                        <td>{{$education->getQualification()}}</td>
                                        <td>
                                            <div class="table-buttons">
                                                @if(checkPermissions(['employees.actions']))
                                                    <a class="btn btn-xs btn-warning edit-employee-education-js"
                                                       data-toggle="modal"
                                                       data-target="#edit-education-modal"
                                                       data-name="{{$education->getName()}}"
                                                       data-qualification="{{$education->getQualification()}}"
                                                       data-direction="{{$education->getDirectionOfPreparation()}}"
                                                       data-route="{{route('admin.employee.education.update',['id'=>$education->getId()])}}"
                                                    >
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-danger delete-employee-education-js"
                                                            data-id="{{$education->getId()}}"
                                                            data-employee-id="{{$employee->getId()}}"><i
                                                                class="fa fa-trash"></i>
                                                    </button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <!-- /.col -->

@endsection