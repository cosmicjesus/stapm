@if($new)
    <tr data-edu-id="{{$education->id}}">
        <td>{{$education->education->name}}</td>
        <td>{{$education->direction_of_preparation}}</td>
        <td>{{$education->qualification}}</td>
        <td>
            <div class="table-buttons">
                @if(checkPermissions(['employees.actions']))
                    <a class="btn btn-xs btn-warning edit-employee-education-js"
                       data-toggle="modal"
                       data-target="#edit-education-modal"
                       data-name="{{$education->education->name}}"
                       data-qualification="{{$education->qualification}}"
                       data-direction="{{$education->direction_of_preparation}}"
                       data-route="{{route('admin.employee.education.update',['id'=>$education->id])}}"
                    >
                        <i class="fa fa-edit"></i>
                    </a>
                    <button class="btn btn-xs btn-danger delete-employee-education-js" data-id="{{$education->id}}"
                            data-employee-id="{{$education->employee_id}}"><i class="fa fa-trash"></i>
                    </button>
                @endif
            </div>
        </td>
    </tr>
@else
    <td>{{$education->education->name}}</td>
    <td>{{$education->direction_of_preparation}}</td>
    <td>{{$education->qualification}}</td>
    <td>
        <div class="table-buttons">
            @if(checkPermissions(['employees.actions']))
                <a class="btn btn-xs btn-warning edit-employee-education-js"
                   data-toggle="modal"
                   data-target="#edit-education-modal"
                   data-name="{{$education->education->name}}"
                   data-qualification="{{$education->qualification}}"
                   data-direction="{{$education->direction_of_preparation}}"
                   data-route="{{route('admin.employee.education.update',['id'=>$education->id])}}"
                >
                    <i class="fa fa-edit"></i>
                </a>
                <button class="btn btn-xs btn-danger delete-employee-education-js" data-id="{{$education->id}}"
                        data-employee-id="{{$education->employee_id}}"><i class="fa fa-trash"></i>
                </button>
            @endif
        </div>
    </td>
@endif