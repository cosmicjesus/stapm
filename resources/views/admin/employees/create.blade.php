@extends('admin.layouts.layout')

@section('content')
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                @yield('title')
            </div>
            {{Form::open(['route'=>'admin.employees.create.post','id'=>'employee-add-form'])}}
            <div class="box-body">
                @if(count($errors))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group">
                    {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
                    {{Form::text('lastname',old('lastname'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('firstname','Имя',['class'=>'control-label'])}}
                    {{Form::text('firstname',old('firstname'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
                    {{Form::text('middlename',old('middlename'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        <label>Дата рождения</label>
                        <div class="input-group date">
                            <input class="form-control pull-right" id="birthday" type="text" name="birthday"
                                   @if(old('birthday')) value="{{old('birthday')}}" @endif>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        {{Form::label('gender','Пол',['class'=>'control-label'])}}
                        {{Form::select('gender',genders(),old('gender'),['class'=>'form-control'])}}
                    </div>
                </div>
                <div class="form-group @if(request()->has('name')) has-error @endif">
                    {{Form::label('education','Образование',['class'=>'control-label'])}}
                    <select name="education" id="education" class="form-control">
                        @foreach($educations as $education)
                            <option value="{{$education->id}}"
                                    data-edu-type="{{$education->type}}">{{$education->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group qualification" style="display: none">
                    {{Form::label('qualification','Квалификация',['class'=>'control-label'])}}
                    {{Form::text('qualification',old('qualification'),['class'=>'form-control qualification-js'])}}
                </div>

                <div class="form-group">
                    <div class="col-xs-6">
                        {{Form::label('category','Категория',['class'=>'control-label'])}}
                        {{Form::select('category',categories(),old('category'),['class'=>'form-control'])}}
                    </div>
                    <div class="col-xs-6">
                        <label>Дата окн. категории</label>
                        <div class="input-group date">
                            <input class="form-control pull-right" id="category_date" type="text" name="category_date"
                                   @if(old('category_date')) value="{{old('category_date')}}" @endif>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group @if(request()->has('name')) has-error @endif">
                    {{Form::label('position','Должность',['class'=>'control-label'])}}
                    {{Form::select('position',$positions,old('position'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        {{Form::label('type','Тип',['class'=>'control-label'])}}
                        {{Form::select('type',types(),old('type'),['class'=>'form-control'])}}
                    </div>
                    <div class="col-xs-6">
                        <label>Дата назначения</label>
                        <div class="input-group date">
                            <input class="form-control pull-right" id="start_date" type="text" name="start_date"
                                   @if(old('start_date')) value="{{old('start_date')}}" @endif>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div>

                </div>

            </div>

            <div class="box-footer">
                {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
                {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4>Справка по работе с формой</h4>
            </div>
            <div class="box-body">
                <p>Заполните основные сведения о сотруднике.</p>
                <p>Если у сотрудника нет категории то дату окончания можно не указывать.</p>
                <p>При добавлении нужно указывать должность на которую был назначени сотрудник при приеме на работу</p>
            </div>

            <div class="box-footer">
            </div>
        </div>
    </div>
@endsection