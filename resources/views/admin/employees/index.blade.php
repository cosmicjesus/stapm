@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            @if(checkPermissions(['employees.create']))
                <a href="{{route('admin.employees.create')}}" class="btn btn-success pull-right">
                    <i class="fa fa-plus"></i> Добавить сотрудника
                </a>
            @endif
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="search-employees">
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <input type="text" name="name" id="name" class="form-control" placeholder="ФИО">
                </div>

            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <select name="position" id="position" class="form-control">
                        <option value="" selected>---Все должности---</option>
                        @foreach($positions as $position)
                            <option value="{{$position->id}}">{{$position->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <select name="category" id="category" class="form-control">
                        <option value="" selected>---Все категории---</option>
                        @foreach(categories() as $key =>$category)
                            <option value="{{$key}}">{{$category}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="employees-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>ФИО</th>
                <th>Дата рождения</th>
                <th>Возраст</th>
                <th>Категория</th>
                <th>Должности</th>
                <th>Кол-во курсов</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>

@endsection