@extends('admin.layouts.layout')

@section('content')
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                @yield('title')
            </div>
            {{Form::open(['route'=>['admin.employee.update',$employee->slug],'id'=>'employee-add-form'])}}
            <div class="box-body">
                @if(count($errors))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group">
                    {{Form::label('lastname','Фамилия',['class'=>'control-label'])}}
                    {{Form::text('lastname',$employee->lastname,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('firstname','Имя',['class'=>'control-label'])}}
                    {{Form::text('firstname',$employee->firstname,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('middlename','Отчество',['class'=>'control-label'])}}
                    {{Form::text('middlename',$employee->middlename,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('exp','Общий стаж работы (в годах)',['class'=>'control-label'])}}
                    {{Form::number('exp',$employee->general_experience,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('specialty_exp','Стаж по специальности (в годах)',['class'=>'control-label'])}}
                    {{Form::number('specialty_exp',$employee->specialty_exp,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('scientific_degree','Ученая степень',['class'=>'control-label'])}}
                    {{Form::text('scientific_degree',$employee->scientific_degree,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('academic_title','Ученое звание',['class'=>'control-label'])}}
                    {{Form::text('academic_title',$employee->academic_title,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('emails','Электронные адреcа',['class'=>'control-label'])}}
                    {{Form::text('emails',$employee->emails,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('phones','Телефоны',['class'=>'control-label'])}}
                    {{Form::text('phones',$employee->phones,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        <label>Дата рождения</label>
                        <div class="input-group date">
                            <input class="form-control pull-right" id="birthday" type="text" name="birthday"
                                   value="{{\Carbon\Carbon::createFromFormat('Y-m-d',$employee->birthday)->format('d.m.Y')}}">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        {{Form::label('gender','Пол',['class'=>'control-label'])}}
                        {{Form::select('gender',genders(),$employee->gender,['class'=>'form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        {{Form::label('category','Категория',['class'=>'control-label'])}}
                        {{Form::select('category',categories(),$employee->category,['class'=>'form-control category-select-js'])}}
                    </div>
                    <div class="col-xs-6 qcategory-picker-js">
                        <label>Дата окн. категории</label>
                        <div class="input-group date">
                            <input class="form-control pull-right" id="birthday" type="text" name="category_date"
                                   @if(!is_null($employee->category_date)) value="{{\Carbon\Carbon::createFromFormat('Y-m-d',$employee->category_date)->format('d.m.Y')}}" @endif>
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>

            <div class="box-footer">
                {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
                {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
    <div class="col-sm-6 col xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4>Справка по работе с формой</h4>
            </div>
            <div class="box-body">
                <p>Обновите нужную информацию сотрудника и нажмите кнопку Обновить</p>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
@endsection