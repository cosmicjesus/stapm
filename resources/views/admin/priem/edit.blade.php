@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.priem.edit',$priem->id]])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="program" class="control-label">Программа</label>
            <input type="text" class="form-control" id="program" value="{{$program->getName()." | ".$program->getEducationForm()}}" disabled="">
        </div>
        <div class="form-group">
            {{Form::label('reception','План приема',['class'=>'control-label'])}}
            {{Form::number('reception',$priem->reception_plan,['class'=>'form-control','min'=>1])}}
        </div>
            <div class="form-group">
                {{Form::label('count_with_documents','Кол-во заявлений c документами(статичное)',['class'=>'control-label'])}}
                {{Form::number('count_with_documents',$priem->count_with_documents,['class'=>'form-control','min'=>0])}}
            </div>
            <div class="form-group">
                {{Form::label('count_with_out_documents','Кол-во заявлений без документов(статичное)',['class'=>'control-label'])}}
                {{Form::number('count_with_out_documents',$priem->count_with_out_documents,['class'=>'form-control','min'=>0])}}
            </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Измените план приема</p>
@endsection