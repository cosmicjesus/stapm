@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.priem.create'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="program" class="control-label">Программа</label>
            <select name="profession_program_id" id="program" class="form-control">
                <option value="">--Выберите программу--</option>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}"
                            @if(old('profession_program_id')==$program->getId()) selected @endif>{{$program->getName()." | ".$program->getEducationForm()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('reception','План приема',['class'=>'control-label'])}}
            {{Form::number('reception',old('reception')?old('reception'):25,['class'=>'form-control','min'=>1])}}
        </div>
        <div class="form-group">
            {{Form::label('count_with_documents','Кол-во заявлений c документами(статичное)',['class'=>'control-label'])}}
            {{Form::number('count_with_documents',old('count_with_documents')?old('count_with_documents'):0,['class'=>'form-control','min'=>0])}}
        </div>
        <div class="form-group">
            {{Form::label('count_with_out_documents','Кол-во заявлений без документов(статичное)',['class'=>'control-label'])}}
            {{Form::number('count_with_out_documents',old('count_with_out_documents')?old('count_with_out_documents'):0,['class'=>'form-control','min'=>0])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Выберите программу и план приема</p>
@endsection