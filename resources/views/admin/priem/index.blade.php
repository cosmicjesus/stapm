@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a data-target="#countEnrolleeReport" data-toggle="modal" class="btn btn-danger">
                <i class="fa fa-file-pdf-o"></i> Сведения о комплектовании групп
            </a>
            <a href="{{route('admin.priem.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить программу
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll priem-table-js">
        @include('admin.priem.inc.table',['programs'=>$programs])
    </div>

@endsection