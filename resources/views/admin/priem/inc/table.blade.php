<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Программа</th>
        <th>План приема</th>
        <th>С оригиналами</th>
        <th>Без оригиналов</th>
        <th>С оригиналами(фиксированно)</th>
        <th>Без оригиналов(фиксированно)</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if(count($programs))
        @foreach($programs as $program)
            <tr>
                <td>{{$program->getPriem()->getId()}}</td>
                <td><a href="{{$program->getDetailUrlAdmin()}}">{{$program->getNameWithForm()}}</a></td>
                <td>{{$program->getPriem()->getReceptionPlan()}}</td>
                <td>{{$program->getCountEnrolleesWithDocuments()}}</td>
                <td>{{$program->getCountEnrolleesWithOutDocuments()}}</td>
                <td>{{$program->getPriem()->getStaticCountWithDocuments()}}</td>
                <td>{{$program->getPriem()->getStaticCountWithOutDocuments()}}</td>
                <td>
                    <div class='table-buttons'>
                        <a href="#"
                           data-params="{{$program->getPriem()->getParams()}}"
                           data-name="{{$program->getNameWithForm()}}" data-toggle="modal"
                           data-target="#editPriem" class="btn btn-xs btn-warning edit-priem-js"><i
                                    class="fa fa-edit"></i></a>
                        <a href="#" data-url="{{$program->getPriem()->getDeleteUrl()}}"
                           class="btn btn-xs btn-danger delete-priem-js"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr class="text-center">
            <td colspan="6">Записи отсутствуют</td>
        </tr>
    @endif
    </tbody>
</table>