<link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
<link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
<style>
    .table {
        color: black;
        text-align: center;
        font-size: 12px;
    }

    td {
        padding: 5px;
        font-size: 14px;
    }

    th {
        padding: 5px;
        font-size: 14px;
    }
</style>
<div style="padding: 10px; text-align: center">
    <h3>
        Список студентов заказавших справок о стипендии <br>на {{\Carbon\Carbon::now()->format('H:i:s d.m.Y')}}
    </h3>
    <table class="table table-striped table-bordered">
        <tr>
            <th>ФИО</th>
            <th>Группа</th>
            <th>Период</th>
            <th>Количество</th>
        </tr>
        @foreach($references as $reference)
            <tr>
                <td>{{$reference->getStudent()}}</td>
                <td>{{$reference->getStudentGroup()}}</td>
                <td>
                    {{getReferencePeriod($reference->getParam('period'))}}
                </td>
                <td>{{$reference->getParam('count')}}</td>
            </tr>
        @endforeach
    </table>
</div>
