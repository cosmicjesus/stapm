@if($withStyle)
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
@endif
@foreach($students as $student)
    @for($i = 0; $i < $repeat; $i++)
        <div class="reference">
            <h4 class="reference-title">Справка №{{$student['number']}}</h4>
            <div class="reference-content">
                <table>
                    <tr>
                        <td>Дана в том, что</td>
                        <td style="width: 83%"
                            class="format-border">{{$student['full_name']}}</td>
                    </tr>
                </table>
                <p>действительно обучается в ГБПОУ Самарской области «Самарский техникум авиационного и промышленного
                    машиностроения имени Д.И.Козлова» <span style="font-size: 8px">(Государственное бюджетное профессиональное образовательное учреждение
                Самарской области «Самарский техникум авиационного и промышленного машиностроения имени Д.И. Козлова»
                образовано в соответствии с постановлением Правительства Самарской области от 02.07.2015 № 396 в
                результате реорганизации государственного бюджетного образовательного учреждения среднего
                профессионального образования Самарского техникума авиационного и промышленного машиностроения имени
                Д.И. Козлова и государственного бюджетного образовательного учреждения среднего профессионального
                образования «Самарский техникум космического машиностроения» путем присоединения государственного
                бюджетного образовательного учреждения среднего профессионального образования «Самарский техникум
                космического машиностроения» к государственному бюджетному образовательному учреждению среднего
                профессионального образования Самарскому техникуму авиационного и промышленного машиностроения имени
                    Д.И. Козлова.)</span></p>
                <p class="format-border">c {{$student['start_training']}}
                    по {{$student['end_training']}}</p>
                <p class="format-border">По {{$student['program_type']}} {{$student['program_name']}}</p>
                <table>
                    <tr>
                        <td class="format-border">Форма
                            обучения: {{$student['education_form']}}</td>
                        <td class="format-border">Бюджет Субъекта РФ</td>
                        <td class="format-border">Курс {{$student['course']}}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Приказ о зачислении за №</td>
                        <td style="width: 72%" class="format-border">{{$student['decree']}}</td>
                    </tr>
                </table>
                <div class="director">
                    <p>Лицензия на осуществление образовательной
                        деятельности <span class="format-border">{{\App\Helpers\Settings::licenceNum()}}
                            от {{\App\Helpers\Settings::licenceDate()}}</span></p>
                    <p>Свидетельство о государственной
                        аккредитации <span class="format-border">{{\App\Helpers\Settings::svidAccredNum()}}
                            от {{\App\Helpers\Settings::svidAccredDate()}}</span></p>
                    <p>Справка дана для предъявления по месту требования</p>
                </div>
                <div class="director">
                    <table>
                        <tr>
                            <td style="padding-left: 30px">Директор</td>
                            <td style="text-align: right">В.Ф.Климов</td>
                        </tr>
                    </table>
                </div>
                <p style="padding-top: 20px">{{$student['date']}}</p>
            </div>
        </div>
    @endfor
@endforeach
