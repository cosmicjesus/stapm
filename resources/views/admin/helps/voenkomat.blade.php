<html>

<head>
    @if($withStyle)
        <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
        <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
    @endif
    <style>
        body {
            font-family: "Times New Roman";
            color: black !important;
        }

        .format-border {
            border-bottom: 1px solid black !important;
        }
    </style>
</head>
<body>
<div class="reference" style="border-bottom: none;">
    <div style="width: 40%">
        Министерство образования и науки
        Самарской области
        ГБПОУ Самарской области
        «Самарский техникум
        авиационного и промышленного
        машиностроения имени Д. И. Козлова»
        443052, Самарская область,
        г. Самара, Старый переулок, д. 6
        тел./факс 955–22–20 (955-08-14)<br>
        исх. № ____________<br>
        от ________________<br>
        на № ______________
    </div>
    <div class="reference-content">
        <h2 style="text-align: center; font-family: 'Times New Roman'; font-size: 16px;font-weight: bold">СПРАВКА</h2>
        <table>
            <tr>
                <td>Выдана гражданину</td>
                <td style="width: 80%"
                    class="format-border">{{$student->getFullName()}}</td>
            </tr>
        </table>
        <p><span class="format-border">{{$student->getBirthday('Y')}}</span> года рождения в том, что он в
            <span class="format-border">{{\Carbon\Carbon::createFromFormat('d.m.Y',$input['start_training'])->format('Y')}}</span>
            г. поступил,
            имея <span class="format-border">{{mb_strtolower($student->getEducation())}}</span> в государственное
            бюджетное профессиональное образовательное учреждение
            Самарской области «Самарский техникум авиационного и промышленного машиностроения имени Д.И. Козлова»
        </p>
        <p class="format-border">Приказ №{{$input['decree']}}</p>
        <p> и в настоящее время обучается на <span class="format-border">{{$student->getGroup()->getCourse()}}</span>
            курсе по очной форме обучения,
            по направлению <br>подготовки (профессии/специальности) <span class="format-border"
                                                                          style="width: 100%">{{$student->getProgram()->getName()}}</span>
        </p>
        <p>имеющему государственную аккредитацию: Свидетельство о Государственной аккредитации регистрационный № 482-16
            от {{\App\Helpers\Settings::licenceDate()}}. Серия {{\App\Helpers\Settings::svidAccredNum()}}, выданное
            Министерством образования и науки Самарской области.
            Действительна до 14.06.2019 г.</p>
        <p> Лицензия на осуществление образовательной деятельности № 6367
            от {{\App\Helpers\Settings::svidAccredDate()}}. Серия {{\App\Helpers\Settings::licenceNum()}}, выданное
            Министерством образования и науки Самарской области.
            Действительна бессрочно.</p>
        <p>
            Срок окончания образовательного учреждения <span
                    class="format-border">{{$student->getGroup()->getEducationPlan()->getEndTrainig()}}</span>
            г.</p>
        <p>Срок окончания обучения по программам послевузовского образования и защиты квалификационной работы «______»
            ______________________г.</p>
        <p>Наличие военной кафедры да/<span class="format-border">нет</span>.</p>
        <p>Справка выдана для предъявления в военный комиссариат <span
                    class="format-border">{{$input['voenkomat_name']}}</span></p>
        <table style="margin-top: 45px;font-size: 14px;line-height:1.5">
            <tr>
                <td style="width: 30%; text-align: left">М.П</td>
                <td>Руководитель ( зам. руководителя ) образовательного, научного
                    учреждения<br>
                    <table>
                        <tr class="format-border">
                            <td class="format-border" style="width: 50%"></td>
                            <td class="format-border">Н.Г.Мальцев</td>
                        </tr>
                    </table>
                    Закончил контракт и приступил к обучению по программе подготовки<br>
                    офицера запаса с _______________________ г.<br>
                    Окончил обучение ____________________ г.<br>
                </td>
            </tr>
            <tr>
                <td>М.П</td>
                <td>Начальник ( заместитель начальника ) военной кафедры:
                    _________________________________________________________________________
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>