@extends('admin.layouts.layout')

@section('title','Главная')

@section('content')
    <div class="col-md-6 col-xs-12">
        @if(isset($statistic['employees'])&&count($statistic['employees']['endCategory']))
            <div class="box box-warning box-solid collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Сотрудники у которых заканчивается категория</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>ФИО</th>
                            <th>Дата окончания</th>
                        </tr>
                        @foreach($statistic['employees']['endCategory'] as $employee)
                            <tr>
                                <td>{{$employee->getFullName()}}</td>
                                <td>{{$employee->getCategoryDate()}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="box-footer">
                    <a href="{{route('admin.employees')}}">Список сотрудников</a>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
        @if(isset($statistic['students']['endPassport'])&&count($statistic['students']['endPassport'])&&checkPermissions('students.view'))
            <div class="box box-warning box-solid collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Студенты у которых закончился срок действия паспорта</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>ФИО</th>
                                <th>Дата рождения</th>
                                <th>Группа</th>
                                <th>Дата окончания</th>
                            </tr>
                            @foreach($statistic['students']['endPassport'] as $student)
                                <tr>
                                    <td>
                                        <a href="{{$student->getProfileUrl()}}">{{$student->getFullName()}} {{$student->isLongAbsent()?'(Длительно отсутствующий)':''}}</a>
                                    </td>
                                    <td>{{$student->getBirthday()}}</td>
                                    <td>{{$student->getGroup()->getName()}}</td>
                                    <td>{{$student->getDocumentEndDate()}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{route('admin.students')}}">Список студентов</a>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
        @if(isset($statistic['enrolleesWithOutDocuments'])&&count($statistic['enrolleesWithOutDocuments']))
            <div class="box box-warning box-solid collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Абитуриенты не подавшие оригинал документа</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>ФИО</th>
                                <th>Профессия</th>
                                <th>Дата подачи документов</th>
                            </tr>
                            @foreach($statistic['enrolleesWithOutDocuments'] as $enrollee)
                                <tr>
                                    <td><a href="{{$enrollee->getProfileUrl()}}">{{$enrollee->getFullName()}}</a></td>
                                    <td>{{$enrollee->getProgram()->getNameWithForm()}}</td>
                                    <td>{{$enrollee->getStartDate()}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{route('admin.enrollees')}}">Список абитуриентов</a>
                </div>
                <!-- /.box-body -->
            </div>
        @endif
    </div>
    <div class="col-md-6 col-xs-12">
        <ul class="shortcuts">
            <li class="shortcuts-item">
                <a href="{{route('admin.employees')}}"
                   class="shortcuts-link shortcuts-link--green">{{$statistic['count_employees']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="{{route('admin.groups')}}"
                   class="shortcuts-link shortcuts-link--green">{{$statistic['count_training_groups']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="{{route('admin.enrollees')}}"
                   class="shortcuts-link shortcuts-link--green">{{$statistic['count_enrollees']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="{{route('admin.students')}}"
                   class="shortcuts-link shortcuts-link--green">{{$statistic['count_students']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="#"
                   class="shortcuts-link shortcuts-link--green">{{$statistic['count_parents']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="{{route('admin.programs')}}"
                   class="shortcuts-link shortcuts-link--blue">{{$statistic['count_ed_programs']}}</a>
            </li>
            <li class="shortcuts-item">
                <a href="{{route('admin.education-plans')}}"
                   class="shortcuts-link shortcuts-link--blue">{{$statistic['count_ed_plans']}}</a>
            </li>
        </ul>
    </div>
@endsection