@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.groups.create.page')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить группу
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <form id="filter-groups-form-js">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <select name="group-program" id="program_id" class="form-control">
                        <option value="" selected>---Все программы---</option>
                        @foreach($programs as $program)
                            <option value="{{$program->getId()}}">{{$program->getName()}}
                                | {{$program->getEducationForm()}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <select name="course" id="course" class="form-control">
                        <option value="" selected>---Все курсы---</option>
                        @foreach([1,2,3,4,5] as $course)
                            <option value="{{$course}}">{{$course}} курс</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <button type="submit" class="btn btn-primary">
                    Применить
                </button>
            </div>
        </form>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="groups-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Фаза обучения</th>
                <th>Наименование</th>
                <th>Программа</th>
                <th>План</th>
                <th>Максимальное кол-во обучающихся</th>
                <th>Кол-во студентов</th>
                <th>Куратор</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
