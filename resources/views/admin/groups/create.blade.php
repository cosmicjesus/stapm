@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.groups.create','class'=>'asyncValidate'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('pattern','Шаблон названия',['class'=>'control-label'])}}
            {{Form::text('pattern','{Курс}',['class'=>'form-control required group-name-pattern-js','placeholder'=>'Шаблон'])}}
        </div>
        <div class="form-group">
            {{Form::label('unique_code','Уникальное название',['class'=>'control-label'])}}
            {{Form::text('unique_code',null,['class'=>'form-control required','placeholder'=>'Уникальное название'])}}
        </div>
        <div class="form-group">
            <label for="program_id">Программа</label>
            <select name="program_id" id="program_id" class="form-control required group-program-js">
                <option value="">--Выберите программу--</option>
                @foreach($programs as $program)
                    <option value="{{$program->getId()}}"
                            @if(old('program_id')==$program->getId()) selected @endif>{{$program->getName()}}
                        | {{$program->getEducationForm()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="education_plan">Учебный план</label>
            <select name="education_plan" id="education_plan" class="form-control required">
                <option value="">--Выберите учебный план--</option>
                @foreach($plans as $plan)
                    <option value="{{$plan->getId()}}" data-program="{{$plan->getProgramId()}}" class="edu_plan_options"
                            @if(old('education_plan')==$plan->getId()) selected @endif
                            @if(!empty(request()->old())) class='hide' @endif>{{$plan->getName()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('max_people','Максимальное количество студентов')}}
            {{Form::number('max_people',25,['class'=>'form-control required','min'=>15])}}
        </div>
        <div class="form-group">
            {{Form::label('curator','Куратор',['class'=>'control-label'])}}
            {{Form::select('curator',getEmployees(),null,['class'=>'form-control required','id'=>'curator_select'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Заполните основные поля</p>
    <p>Выберите профессиональную программу, учебный план и куратора</p>
    <p>В поле "Шаблон" тэг {Курс} в последстии заменится номер курса.</p>
@endsection