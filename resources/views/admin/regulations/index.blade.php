@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.regulations.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить запись
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-responsive">
        <table id="regulations-table" class="table table-bordered table-striped" style="width: 100% !important;">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование органа осуществляющего надзор</th>
                <th>Комментарий</th>
                <th>Дата</th>
                <th>Кол-во файлов</th>
                <th>Кол-во нарушений</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
@endsection