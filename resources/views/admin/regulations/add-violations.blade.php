@extends('admin.layouts.add')

@section('form')
    {{Form::open(['url'=>$route,'id'=>'add-violation-form','name'=>'add-violation-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name_of_violation','Наименование нарушения',['class'=>'control-label'])}}
            {{Form::text('name_of_violation',old('name_of_violation'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('result','Результат')}}
            {{Form::textarea('result',old('result'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="files">
        </div>
        <div class="col-xs-12">
            <button class="btn btn-primary pull-left add-news-files-js" type="button">
                <i class="fa fa-plus"> Прикрепить файл</i>
            </button>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection