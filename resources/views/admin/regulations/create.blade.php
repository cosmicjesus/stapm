@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.regulations.store','id'=>'add-regulation-form','name'=>'add-regulation-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name_of_the_supervisory','Наименование органа осуществляющего надзор',['class'=>'control-label'])}}
            {{Form::text('name_of_the_supervisory',old('name_of_the_supervisory'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('date','Год',['class'=>'control-label'])}}
            {{Form::number('date',\Carbon\Carbon::now()->format('Y'),['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true,['id'=>'active'])}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            {{Form::label('comment','Комментарий')}}
            {{Form::textarea('comment',null,['class'=>'form-control'])}}
        </div>
        <div class="files">
        </div>
        <div class="col-xs-12">
            <button class="btn btn-primary pull-left add-news-files-js" type="button">
                <i class="fa fa-plus"> Прикрепить файл</i>
            </button>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection