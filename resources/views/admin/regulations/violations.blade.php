@extends('admin.layouts.layout')

@section('content')
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a href="#index" data-toggle="tab">Основная информация</a></li>
                <li class="active"><a href="#violations" data-toggle="tab">Нарушения</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="index">

                </div>
                <div class="tab-pane active" id="violations">
                    <div class="col-xs-12" style="margin: 10px 0">
                        <div class="pull-right">
                            <a href="{{$regulation->getAddViolationUrl()}}" class="btn btn-success"><i
                                        class="fa fa-plus"></i> Добавить нарушение</a>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>Наименование нарушения</th>
                            <th>Результат</th>
                            <th></th>
                        </tr>
                        @if($regulation->getCountViolations())
                            @foreach($regulation->getViolations(false) as $violation)
                                <tr>
                                    <td>{{$violation->getId()}}</td>
                                    <td>{{$violation->getNameOfViolation()}}</td>
                                    <td>{{$violation->getResult()}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Нет нарушений</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection