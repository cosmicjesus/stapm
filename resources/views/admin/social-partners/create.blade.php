@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.social-partners.store','id'=>'partner-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',old('name'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('logo','Лого',['class'=>'control-label'])}}
            {{Form::file('logo')}}
        </div>
        <div class="form-group">
            <label for="active">
                {{Form::checkbox('active',old('active'),true)}}
                Показывать на сайте
            </label>
        </div>
        <div class="form-group">
            {{Form::label('site_url','Адрес сайта',['class'=>'control-label'])}}
            {{Form::text('site_url',old('site_url'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('contract_type','Тип договора',['class'=>'control-label'])}}
            {{Form::text('contract_type',old('contract_type'),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label>Дата подписания</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="default-picker" type="text" name="start_date"
                       value="{{\Carbon\Carbon::now()->format('d.m.Y')}}" required>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Дата окончания</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="birthday" type="text" name="end_date"
                       value="{{\Carbon\Carbon::now()->format('d.m.Y')}}" required>
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок',['class'=>'control-label'])}}
            {{Form::number('sort',500,['class'=>'form-control','min'=>1,])}}
        </div>
        <div class="form-group">
            {{Form::label('description','Описание',['class'=>'control-label'])}}
            {{Form::textarea('description',old('description'),['class'=>'form-control','id'=>'preview'])}}
        </div>
        <div class="files">
        </div>
        <div class="col-xs-12">
            <button class="btn btn-primary pull-left add-news-files-js" type="button">
                <i class="fa fa-plus"> Прикрепить файл</i>
            </button>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection
