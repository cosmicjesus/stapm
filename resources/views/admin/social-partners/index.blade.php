@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.social-partners.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить партнера
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование</th>
                <th>Описание</th>
                <th>Ссылка на сайт</th>
                <th>Порядок</th>
                <th>Показывать на сайте</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($partners))
                @foreach($partners as $partner)
                    <tr>
                        <td>{{$partner->getId()}}</td>
                        <td>{{$partner->getName()}}</td>
                        <td>{!!$partner->getDescription()!!}</td>
                        <td><a href="{{$partner->getSiteUrl()}}" target="_blank">{{$partner->getSiteUrl()}}</a></td>
                        <td>{{$partner->getSort()}}</td>
                        <td>{{$partner->getActiveStr()}}</td>
                        <td>
                            <div class='table-buttons'>
                                <a href="{{$partner->getEditUrl()}}" class="btn btn-xs btn-warning"><i
                                            class="fa fa-edit"></i></a>
                                <a href="#" data-url="{{$partner->getDeleteUrl()}}" data-name="{{$partner->getName()}}"
                                   class="btn btn-xs btn-danger delete-partner-js"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td colspan="6">Записи отсутствуют</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

@endsection