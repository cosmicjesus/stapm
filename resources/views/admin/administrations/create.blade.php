@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.administrations.create','id'=>'eduplan-add-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="program" class="control-label">Сотрудник</label>
            <select name="employee_id" id="program" class="form-control">
                <option value="">--Выберите сотрудника--</option>
                @foreach($employees as $employee)
                    <option value="{{$employee->getId()}}"
                            @if(old('employee_id')==$employee->getId()) selected @endif>{{$employee->getFullName()}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',old('sort')?old('sort'):500,['class'=>'form-control','min'=>1])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Выберите сотрудника и задайте порядок сортировки на странице "Администрация техникума"</p>
@endsection