@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.administrations.create.page')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить сотрудника
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>ФИО</th>
                <th>Порядок сортировки</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($employees))
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->getAdministrator()->getId()}}</td>
                        <td><a href="{{$employee->getProfileUrl()}}">{{$employee->getFullName()}}</a></td>
                        <td>{{$employee->getAdministrator()->getSort()}}</td>
                        <td>
                            <div class='table-buttons'>
                                <a href="{{$employee->getAdministrator()->getEditUrl()}}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                <a href="#" data-url="{{$employee->getAdministrator()->getDeleteUrl()}}" class="btn btn-xs btn-danger delete-adm-js"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td colspan="4">Записи отсутствуют</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

@endsection