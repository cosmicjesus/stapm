@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.administrations.edit',$item->id],'id'=>'eduplan-add-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('sort','Сотрудник',['class'=>'control-label'])}}
            {{Form::text('sort',$item->employee->lastname." ".$item->employee->firstname." ".$item->employee->middlename,['class'=>'form-control','disabled'=>'disabled'])}}
        </div>
        <div class="form-group">
            {{Form::label('sort','Порядок сортировки',['class'=>'control-label'])}}
            {{Form::number('sort',$item->sort,['class'=>'form-control','min'=>1])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Задайте порядок сортировки на странице "Администрация техникума"</p>
@endsection