@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.programs.create','id'=>'program-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('profession','Профессия',['class'=>'control-label'])}}
                {{Form::select('profession',$professions,old('profession'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                {{Form::label('form','Форма',['class'=>'control-label'])}}
                {{Form::select('form',$forms,old('form'),['class'=>'form-control'])}}
            </div>
            <div class="col-xs-6">
                <label>Срок действия аккредитации</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="licence" type="text" name="licence"
                           @if(old('licence')) value="{{old('licence')}}"@endif>
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                {{Form::label('period','Срок обучения',['class'=>'control-label'])}}
                {{Form::number('period',old('period')?old('period'):10,['class'=>'form-control','min'=>10,'id'=>'period'])}}
            </div>
            <div class="col-xs-6 period-string">
                <span class="period-string-js">{{old('period')?convertTrainingPeriod(old('period')):convertTrainingPeriod(10)}}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('type','Тип',['class'=>'control-label'])}}
                {{Form::select('type',$types,old('type'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('qualification','Квалификация',['class'=>'control-label'])}}
                {{Form::text('qualification',old('qualification'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('description','Описание',['class'=>'control-label'])}}
                {{Form::textarea('description',old('description'),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('sort','Порядок',['class'=>'control-label'])}}
                {{Form::number('sort',old('sort')?old('sort'):500,['class'=>'form-control','min'=>1,'id'=>'sort'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('presentation','Профессиограмма')}}
                {{Form::file('presentation')}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('video_presentation','Видео презентация',['class'=>'control-label'])}}
                <div class="input-group">
                    <span class="input-group-addon">https://www.youtube.com/watch?v=</span>
                    {{Form::text('video_presentation',old('video_presentation'),['class'=>'form-control'])}}
                </div>

            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label for="on_site" class="control-label">{{Form::checkbox('on_site',old('on_site'),true)}} Показывать
                    на сайте</label>
            </div>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Заполнитие основную информацию об ОПОП</p>
    <p>Если у ОПОП нет аккредитации, то поле Срок действия аккредитации можно оставить пустым</p>
@endsection
