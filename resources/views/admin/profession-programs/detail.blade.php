@extends('admin.layouts.layout')

@section('content')
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a href="#index" data-toggle="tab">Основные документы</a></li>
                <li class="active"><a href="#other" data-toggle="tab">Остальные документы</a></li>
                <li><a href="#subjects" data-toggle="tab">Дисциплины</a></li>
                <li><a href="#metodicals" data-toggle="tab">Методички</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="index">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Образовательный стандарт</td>
                            <td>
                                <div class="actions-standart">
                                    {{Form::open(['files'=>true,'route'=>['admin.program.document.upload','id'=>$program->getId(),'type'=>'standart']])}}
                                    @if(is_null($program->getDocuments()->getStandart()))
                                        <a href="#" class="btn btn-primary upload-doc-js" data-type="standart">
                                            <i class="fa fa-upload"></i>
                                        </a>
                                    @else
                                        <a href="{{$program->getDocuments()->getStandart()}}" class="btn btn-primary">
                                            Открыть
                                        </a>
                                        <a href="#" class="btn btn-primary upload-doc-js" data-type="standart">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    @endif
                                    {{Form::file('file',['id'=>'file-standart','style'=>'display:none','data-type'=>'standart','class'=>'docs'])}}

                                    {{Form::close()}}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Аннотации рабочих программ</td>
                            <td>
                                <div class="actions-annotation">
                                    {{Form::open(['files'=>true,'route'=>['admin.program.document.upload','id'=>$program->getId(),'type'=>'annotation']])}}
                                    @if(is_null($program->getDocuments()->getAnnotation()))
                                        <a href="#" class="btn btn-primary upload-doc-js" data-type="annotation">
                                            <i class="fa fa-upload"></i>
                                        </a>
                                    @else
                                        <a href="{{$program->getDocuments()->getAnnotation()}}" class="btn btn-primary">
                                            Открыть
                                        </a>
                                        <a href="#" class="btn btn-primary upload-doc-js" data-type="annotation">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    @endif
                                    {{Form::file('file',['id'=>'file-annotation','style'=>'display:none','data-type'=>'annotation','class'=>'docs'])}}

                                    {{Form::close()}}
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane active" id="other">
                    <a href="#" class="btn btn-primary add-other-document-js" data-toggle="modal"
                       data-target="#add_prog_other_document" data-url="{{$program->getAddOtherDocumentUrl()}}">
                        <i class="fa fa-plus"></i>Добавить документ
                    </a>
                    <div class="calendars">
                        @include('admin.profession-programs.inc.other-document',
                            [
                                'title'=>'Календарные графики',
                                'files'=>$program->getCalendars(),
                                'noFiles'=>'Календари отсутствуют'
                            ]
                        )
                    </div>
                    <div class="akts">
                        @include('admin.profession-programs.inc.other-document',
                            [
                                'title'=>'Акты согласования',
                                'files'=>$program->getAkts(),
                                'noFiles'=>'Акты отсутствуют'
                            ]
                        )
                    </div>
                    <div class="ppssz">
                        @include('admin.profession-programs.inc.other-document',
                            [
                                'title'=>'ППССЗ/ППКСР',
                                'files'=>$program->getPpssz(),
                                'noFiles'=>'Записи отсутствуют'
                            ]
                        )
                    </div>
                </div>
                <div class="tab-pane" id="subjects">
                    <div class="buttons">
                        <a href="#" data-toggle="modal" data-target="#add_subject_in_program"
                           class="btn btn-primary add-subject-in-program-js"
                           data-url="{{$program->getAddSubjectInProgramUrl()}}">
                            <i class="fa fa-plus"></i>Добавить дисциплину/модуль
                        </a>
                    </div>
                    @if(count($program->getSubjects()))
                        <div class="row">
                            @foreach($program->getSubjects() as $subject)
                                @include('admin.profession-programs.inc.subjects',['subject'=>$subject])
                            @endforeach
                        </div>
                    @else
                        <h3>Дисциплины отсутствуют</h3>
                    @endif
                </div>
                <div class="tab-pane" id="metodicals">
                    @include('admin.profession-programs.inc.metodicals',[
                    'metodicals'=>$program->getMetodicals(),
                    'getUrl'=>$program->getMetodicalUrl(),
                    'createUrl'=>$program->getCreateMetodicalUrl()
                    ]
                    )
                </div>
            </div>
        </div>
    </div>
@endsection