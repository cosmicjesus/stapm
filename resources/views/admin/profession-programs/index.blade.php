@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.programs.createPage')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить программу
            </a>
        </div>
        @endsection

        @section('content_inner')
            <div class="table-responsive">
            <table id="programs-table" class="table table-bordered table-striped" style="width: 100% !important;">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Профессия</th>
                    <th>Форма</th>
                    <th>Тип</th>
                    <th>Срок обучения</th>
                    <th>Профессиограмма</th>
                    <th>Видеопрезентация</th>
                    <th>Порядок</th>
                    <th>Показывать на сайте</th>
                    <th></th>
                </tr>
                </thead>
            </table>
            </div>
@endsection