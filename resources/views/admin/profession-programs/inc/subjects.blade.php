<div class="col-lg-4 col-md-6 col-xs-12">
    <div class="box box-default collapsed-box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">{{$subject->getName()}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="buttons">
                <a href="#" class="btn btn-primary add-doc-on-subject-js"
                   data-program="{{$subject->getProfessionProgramId()}}"
                   data-url="{{$subject->getAddDocumentUrl()}}"
                   data-subject="{{$subject->getName()}}"
                   data-toggle="modal"
                   data-target="#add_doc_in_subject">
                    <i class="fa fa-plus"></i>Добавить документ
                </a>
                <a href="#" class="btn btn-danger delete-subject-on-program-js" data-url="{{$subject->getDeleteUrl()}}" data-name="{{$subject->getName()}}">
                    <i class="fa fa-trash"></i> Удалить дисциплину
                </a>
            </div>
            @if($subject->getFiles()->count())
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Файл</th>
                        <th></th>
                    </tr>
                    @foreach($subject->getFiles() as $file)
                        <tr class="program-document">
                            <td><a href="{{$file->getFileUrl()}}" target="_blank" class="program-document-link">{{$file->getTitle()}}</a></td>
                            <td class="program-document-action">
                                <a href="#" class="btn btn-xs btn-primary reload-program-doc-js"><i class="fa fa-refresh"></i></a>
                                <a href="#" data-url="{{$file->getDeleteUrl()}}" data-sub-name="{{$subject->getName()}}" data-file-name="{{$file->getTitle()}}"
                                   class="btn btn-xs btn-danger delete-program-document-js"><i class="fa fa-trash"></i></a>
                                {{Form::open(['url'=>$file->getReloadUrl(),'files'=>true,'class'=>'hide'])}}
                                {{Form::file('file',['id'=>'program-file','class'=>'reload-program-document'])}}
                                {{Form::close()}}
                            </td>
                        </tr>
                    @endforeach
                </table>

            @else
                Нет загруженных документов
            @endif
        </div>
        <!-- /.box-body -->
    </div>
</div>