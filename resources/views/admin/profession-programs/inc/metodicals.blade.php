<div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-xs-12">
        {{Form::open(['url'=>$createUrl,'name'=>'add-metodical-form','id'=>'add-metodical-form'])}}
        <div class="form-group">
            {{Form::label('name','Наименование')}}
            {{Form::text('name',null,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            {{Form::label('file','Файл')}}
            {{Form::file('file',['class'=>'form-control'])}}
        </div>
        {{Form::submit('Добавить',['class'=>'btn btn-primary','required'=>'required'])}}
        {{Form::close()}}
    </div>
    <div class="col-xs-12" style="padding-top: 20px">
        <div class="table-responsive">
            <table id="program-metodicals" class="table table-bordered table-striped" data-url="{{$getUrl}}" style="width: 100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Наименование</th>
                    <th>Файл</th>
                    <th>Действия</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>