<h3>{{$title}}</h3>
@if($files)
    <table class="table table-bordered table-striped">
        <tr>
            <td>Файл</td>
            <td></td>
        </tr>
        @foreach($files as $file)
            <tr class="program-document">
                <td><a href="{{$file->getFileUrl()}}" target="_blank" class="program-document-link">{{$file->getOtherName()}}</a></td>
                <td class="program-document-action">
                    <a href="#" class="btn btn-xs btn-primary reload-program-doc-js"><i class="fa fa-refresh"></i></a>
                    <a href="#" data-url="{{$file->getDeleteUrl()}}"  data-file-name="{{$file->getOtherName()}}"
                       class="btn btn-xs btn-danger delete-program-document-js"><i class="fa fa-trash"></i></a>
                    {{Form::open(['url'=>$file->getReloadOtherUrl(),'files'=>true,'class'=>'hide'])}}
                    {{Form::file('file',['id'=>'program-file','class'=>'reload-program-document'])}}
                    {{Form::close()}}
                </td>
            </tr>
        @endforeach
    </table>
@else
    <h3>{{$noFiles}}</h3>
@endif