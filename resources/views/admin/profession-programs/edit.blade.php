@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.programs.update',$program->getSlug()],'id'=>'program-add-form','files'=>true])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('profession','Профессия',['class'=>'control-label'])}}
                {{Form::select('profession',$professions,$program->getProfessionId(),['class'=>'form-control','disabled'=>'disables'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                {{Form::label('form','Форма',['class'=>'control-label'])}}
                {{Form::select('form',$forms,$program->getEducationFormId(),['class'=>'form-control','disabled'=>'disables'])}}
            </div>
            <div class="col-xs-6">
                <label>Срок действия аккредитации</label>
                <div class="input-group date">
                    <input class="form-control pull-right" id="licence" type="text" name="licence"
                           value="{{$program->getLicence()}}">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                {{Form::label('period','Срок обучения',['class'=>'control-label'])}}
                {{Form::number('period',$program->getPeriodToInt(),['class'=>'form-control','min'=>10,'id'=>'period'])}}
            </div>
            <div class="col-xs-6 period-string">
                <span class="period-string-js">{{$program->getPeriod()}}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('type','Тип',['class'=>'control-label'])}}
                {{Form::select('type',$types,$program->getTypeId(),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('qualification','Квалификация',['class'=>'control-label'])}}
                {{Form::text('qualification',$program->getQualification(),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('description','Описание',['class'=>'control-label'])}}
                {{Form::textarea('description',$program->getDescription(),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('sort','Порядок',['class'=>'control-label'])}}
                {{Form::number('sort',$program->getSort(),['class'=>'form-control','min'=>1,'id'=>'sort'])}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label for="on_site" class="control-label">
                    {{Form::checkbox('on_site',1,$program->onSite())}}
                    Показывать на сайте</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                {{Form::label('presentation','Профессиограмма')}}
                {{Form::file('presentation')}}
                <br>
                @if($program->getPresentationPath())
                    {!! $program->getPresentationPath() !!}
                @endif<br>
            </div>
        </div>
        <div class="form-group" style="margin-top: 10px">
            <div class="col-xs-12">
                {{Form::label('video_presentation','Видео презентация',['class'=>'control-label'])}}
                <div class="input-group">
                    <span class="input-group-addon">https://www.youtube.com/watch?v=</span>
                    {{Form::text('video_presentation',$program->getVideoPresentationCode(),['class'=>'form-control'])}}
                </div>
                <br>
                @if($program->getVideoPresentationUrl())
                    <a data-fancybox href=" {{$program->getVideoPresentationUrl()}}&amp;autoplay=1&amp;showinfo=0">
                        Имеющаяся презентация
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Измените нужную информацию</p>
    <p>Если у ОПОП нет аккредитации, то поле Срок действия аккредитации можно оставить пустым</p>
@endsection
