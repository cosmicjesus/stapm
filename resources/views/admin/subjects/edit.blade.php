@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>['admin.subjects.update',$subject->id],'id'=>'update-subject-form','name'=>'update-subject-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('name','Наименование',['class'=>'control-label'])}}
            {{Form::text('name',$subject->name,['class'=>'form-control','required'=>'required'])}}
        </div>
        <div class="form-group">
            <label for="type_id" class="control-label">Тип</label>
            <select name="type_id" id="type_id" class="form-control" required>
                @foreach(getSubjectTypes() as $type)
                    <option value="{{$type['id']}}"
                            @if($subject->type_id==$type['id']) selected @endif>{{$type['name']}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')

@endsection