@extends('admin.layouts.add')

@section('form')
    {{Form::open(['route'=>'admin.meta-tags.store'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('route','Путь',['class'=>'control-label'])}}
            {{Form::text('route',null,['class'=>'form-control','required'=>'required','placeholder'=>'Путь'])}}
        </div>
        <div class="form-group">
            {{Form::label('title','Заголовок страницы',['class'=>'control-label'])}}
            {{Form::text('title',null,['class'=>'form-control','required'=>'required','placeholder'=>'Заголовок страницы'])}}
        </div>
        <div class="form-group">
            {{Form::label('description','Заголовок страницы',['class'=>'control-label'])}}
            {{Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Краткое описание страницы'])}}
        </div>
        <div class="form-group">
            {{Form::label('keywords','Ключевые слова',['class'=>'control-label'])}}
            {{Form::textarea('keywords',null,['class'=>'form-control','placeholder'=>'Слово1, слово2'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Добавить',['class'=>'btn btn-primary'])}}
        {{Form::reset('Сброс',['class'=>'btn btn-danger'])}}
    </div>
    {{Form::close()}}
@endsection

@section('description')
    <p>Заполните основные поля</p>
    <p>Укажите краткое описание страницы</p>
    <p>Ключевые слова нужно перечислять через запятую</p>
@endsection