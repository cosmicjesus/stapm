@extends('admin.layouts.inner')

@section('content_inner_header')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('admin.meta-tags.create')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i> Добавить теги
            </a>
        </div>
    </div>
@endsection

@section('content_inner')
    <div class="table-scroll">
        <table class="table table-bordered table-striped" id="meta-tags-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Путь</th>
                <th>Заголовок</th>
                <th>Описание</th>
                <th>Ключевые слова</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($tags))
                @foreach($tags as $tag)
                    <tr>
                        <td>{{$tag->id}}</td>
                        <td>{{$tag->route}}</td>
                        <td>{{$tag->title}}</td>
                        <td>{{$tag->description}}</td>
                        <td>{{$tag->keywords}}</td>
                        <td>
                            <div class='table-buttons'>
                                <a href="{{route('admin.meta-tags.edit',['id'=>$tag->id])}}"
                                   class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#meta-tags-table').DataTable({
                dom: "<'row'<'col-xs-12't>>" +
                    "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>>",
                language: {
                    url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
                },
            });
        })
    </script>
@endpush