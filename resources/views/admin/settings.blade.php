@extends('admin.layouts.add')
@section('form')
    {{Form::open(['route'=>'admin.site-settings.update','id'=>'site-settings-form'])}}
    <div class="box-body">
        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            {{Form::label('licence_num','Серия и номер лицензии')}}
            {{Form::text('licence_num',\App\Helpers\Settings::licenceNum(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label>Дата выдачи лицензии</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="default-picker" type="text" name="licence_date"
                       value="{{\App\Helpers\Settings::licenceDate()}}">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="form-group">
            {{Form::label('accred_num','Серия и номер свидетельства о аккредитации')}}
            {{Form::text('accred_num',\App\Helpers\Settings::svidAccredNum(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label>Дата выдачи свидетельства о аккредитации</label>
            <div class="input-group date">
                <input class="form-control pull-right" id="birthday" type="text" name="accred_date"
                       value="{{\App\Helpers\Settings::svidAccredDate()}}">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 form-group">
                {{Form::label('count_place_on_man','Кол-во мест в общежитии для юношей')}}
                {{Form::number('count_place_on_man',\App\Helpers\Settings::countPlaceOnMan(),['class'=>'form-control'])}}
            </div>
            <div class="col-xs-6 form-group">
                {{Form::label('count_place_on_woman','Кол-во мест в общежитии для девушек')}}
                {{Form::number('count_place_on_woman',\App\Helpers\Settings::countPlaceOnWoman(),['class'=>'form-control'])}}
            </div>
        </div>
        <div class="form-group">
            <label for="show_old_site_message">
                Заказ справок с сайта
            </label>
            {{Form::select('order_helps_from_site',['1'=>'Да','0'=>'Нет'],\App\Helpers\Settings::orderHelpsFromSite(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="show_old_site_message">
                Показывать сообщение о старой версии сайта
            </label>
            {{Form::select('show_old_site_message',['1'=>'Да','0'=>'Нет'],\App\Helpers\Settings::ShowOldSiteMessage(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="show_old_site_message">
                Показывать статические значения количества абитуриентов
            </label>
            {{Form::select('show_static_count_enrollees',['1'=>'Да','0'=>'Нет'],\App\Helpers\Settings::ShowStaticCountEnrollees(),['class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label for="show_old_site_message">
               Показывать динамический рейтинг среднего балла
            </label>
            {{Form::select('real_avg_rait',['1'=>'Да','0'=>'Нет'],\App\Helpers\Settings::realAvgRait(),['class'=>'form-control'])}}
        </div>
    </div>

    <div class="box-footer">
        {{Form::submit('Обновить',['class'=>'btn btn-primary'])}}
    </div>
    {{Form::close()}}
@endsection
