<!DOCTYPE html>
<html lang="ru">
@include("client.layout.inc.head")
<body>
<div id="app">
    @include('client.layout.special-settings')
    @include("client.layout.inc.header")
    @yield('content_section')
    @include("client.layout.inc.footer")
</div>
<script type="text/javascript" src="{{mix('client/js/manifest.js')}}"></script>
<script type="text/javascript" src="{{mix('client/js/vendor.js')}}"></script>
{{--<script type="text/javascript" src="{{mix('client/js/scripts.js')}}"></script>--}}
<script type="text/javascript" src="{{mix('client/js/app.js')}}"></script>
</body>
</html>
