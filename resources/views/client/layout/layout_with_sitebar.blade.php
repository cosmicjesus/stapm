@extends('client.layout.layout')
@section('content_section')
    <div class="container-main content" id="main-content">
        @include('client.layout.inc.sitebar')
        <div class="container-information">
            {{Breadcrumbs::view('partials.v2breadcrumbs')}}
            <div class="information">
                @yield('content')
            </div>
        </div>
    </div>
@endsection