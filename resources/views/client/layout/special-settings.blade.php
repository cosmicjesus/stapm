<div class="content">
    <div class="special-settings">
        <div class="setting__item">
            <div class="setting__title text-center">
                Интервал
            </div>
            <div class="setting__buttons">
                <button class="setting__btn setting__btn-outline setting-interval_normal active" data-interval="normal"
                        title="Одинарный">
                    1.0
                </button>
                <button class="setting__btn setting__btn-outline setting-interval_sesquialteral"
                        data-interval="sesquialteral" title="Полуторный">
                    1.5
                </button>
                <button class="setting__btn setting__btn-outline setting-interval_double" data-interval="double"
                        title="Двойной">
                    2.0
                </button>
            </div>
            <div class="setting__status text-center">
                Одинарный
            </div>
        </div>
        <div class="setting__item">
            <div class="setting__title text-center">
                Размер шрифта
            </div>
            <div class="setting__buttons">
                <a class="setting__btn setting__btn-outline decrement-font-size"
                        title="Уменьшить шрифт">
                    <i class="fa fa-minus"></i>
                </a>
                <a class="setting__btn setting__btn-outline increment-font-size"
                        title="Увеличить шрифт">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="setting__status setting__font-status text-center">
                16 пунктов
            </div>
        </div>
        <div class="setting__item">
            <div class="setting__title text-center">
                Изображения
            </div>
            <div class="setting__buttons">
                <button class="setting__btn setting__btn-outline active setting-image_on" data-image="on"
                        title="Включить">
                    Вкл
                </button>
                <button class="setting__btn setting__btn-outline setting-image_off" data-image="off" title="Выключить">
                    Выкл
                </button>
                <button class="setting__btn setting__btn-outline setting-image_grayscale" data-image="grayscale"
                        title="Черно-белые">
                    Ч/Б
                </button>
            </div>
            <div class="setting__status text-center">
                Вкл
            </div>
        </div>
        <div class="setting__item">
            <div class="setting__title text-center">
                Цветовая схема
            </div>
            <div class="setting__buttons">
                <button class="setting__btn setting__btn-outline btn--black-on-white active setting-color_black-on-white"
                        data-color="black-on-white"
                        title="Черным по белому">
                    <span>А</span>
                </button>
                <button class="setting__btn setting__btn-outline btn--black-on-yellow setting-color_yellow-on-black"
                        data-color="yellow-on-black"
                        title="Желтым по черному">
                    <span>А</span>
                </button>
                <button class="setting__btn setting__btn-outline btn--blue-on-blue setting-color_blue-on-blue"
                        data-color="blue-on-blue"
                        title="Синим по голубому">
                    <span>А</span>
                </button>
            </div>
            <div class="setting__status text-center">
                Черным по белому
            </div>
        </div>
        <div class="setting__item">
            <div class="setting__title text-center">
                Дейстия
            </div>
            <div class="setting__buttons">
                <button class="setting__btn setting__btn-outline"
                        title="Сброс настроек" data-reset-settings>
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                </button>
                <button class="setting__btn setting__btn-outline" data-color="yellow-on-black"
                        title="Выключить спецверсию" data-spec-off>
                    Выкл. спецверсию
                </button>
            </div>
        </div>
        <button class="special-panel-control">
            <i class="fa fa-caret-up" aria-hidden="true"></i>
        </button>
    </div>
</div>