<header>
    <div class="top-section__container" ref="top_section">
        <div class="content">
            <div class="top-section_grid_container" itemscope itemtype="http://schema.org/Organization">
                <div class="top-section_address">
                    <a href="{{route('client.basic-information')}}#map" class="top-section_link">
                        <i class="fa fa-map"></i>
                        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <span itemprop="postalCode">443052</span>
                            <span itemprop="addressLocality">Самара</span>,
                            <span itemprop="streetAddress">Старый переулок 6</span>
                        </span>
                    </a>
                </div>
                <div class="top-section_buttons">
                    <a href="" class="top-section_link special-vision-js">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                    </a>
                    <a href="" class="top-section_link">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="header container-white">
        <div class="content">
            <div class="header_wrapper">
                <div class="header_info">
                    <div class="header_logo">
                        <a href="{{route('client.index')}}">
                            <img class="img-responsive"
                                 src="/img/logo.gif" alt="{{env('COLLEGE_NAME')}}">
                        </a>
                    </div>
                    <div class="header_college_name">
                        <a class="college_name_link" href="{{route('client.index')}}">
                        <span class="h1"
                              itemprop="name">Государственное бюджетное профессиональное образовательное учреждение Самарской области<br> «Самарский техникум авиационного и промышленного машиностроения<br/>
                            имени Д.И.Козлова»</span>
                        </a>
                    </div>
                </div>
                <div class="header_contacts">
                    <div class="header_operating_mode">
                        <div class="operating_mode_icon">
                            <i class="fa fa-clock-o fa-2x fa-spin"></i>
                        </div>
                        <div class="operating_mode_data pl-10">
                            <a href="{{route('client.admission')}}" class="link header_enrollment_link">Приемная
                                комиссия</a><br>
                            <time itemprop="openingHours" datetime="Mo,Fr 9:00−17:00">9:00 до 17:00 Пн-Пт</time><br>
{{--                            <time itemprop="openingHours" datetime="Sun 13:00">9:00 до 13:00 Cуббота</time><br>--}}
                            <time itemprop="openingHours">Cб, Вс-выходной</time>
                        </div>
                    </div>
                    <div class="header_phones">
                        <div class="header_contact_info_phones_text">
                            <b><span itemprop="telephone">8(846) 955-22-20</span></b> – приемная директора<br>
                            <b><span itemprop="telephone">8(846) 955-22-11</span></b> – приемная комиссия<br>
                            <b><span itemprop="telephone">8(846) 955-08-14</span></b> – заместитель директора по УОР<br>
                            <b><span itemprop="telephone">8(846) 955-06-11</span></b> – общежитие<br>
                            <b>e-mail:</b>
                            <a href="mailto:poo_stapm_su@samara.edu.ru" class="link">
                                <span itemprop="email" style="word-break: break-all">poo_stapm_su@samara.edu.ru</span>
                            </a><br>
                            <b>e-mail приемной комиссии:</b><br>
                            <a href="mailto:priem@stapm.ru" class="link">
                                <span itemprop="email">priem@stapm.ru</span>
                            </a><br>
                            <a href="{{route('client.feedback')}}">
                                Обратная связь
                            </a>
                            <br>
                            <a href="http://3d-tour.stapm.ru/" target="_blank">
                                3d Тур по техникуму
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <sticky>
        {{Widget::run('App\Widgets\ClientMenu',['template'=>'top'])}}
    </sticky>
</header>
