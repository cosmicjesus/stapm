<head>
    @if(Route::currentRouteName()=='client.index')
        <title>{{env('COLLEGE_NAME')}}</title>
    @else
        <title>@yield('title') | {{env('COLLEGE_NAME')}}</title>
    @endif
    @routes
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="5d31af80892b0a40"/>
    @if(env('APP_ENV')!='production')
        <meta name=“robots” content=“noindex,nofollow”>
    @else
        <meta name="robots" content="index, nofollow"/>
    @endif
    <meta name="yandex" content="noyaca"/>
    <meta name="copyright" lang="ru" content="{{env('COLLEGE_NAME')}}"/>
    <meta charset="UTF-8">
    {!! Meta::tag('description') !!}
    {!! Meta::tag('keywords') !!}
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,900&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="{{mix('client/styles/vendor.css')}}" type="text/css">
    <link rel="stylesheet" href="{{mix('client/styles/style.css')}}" type="text/css">
@stack('headscripts')
@if(env('APP_ENV')=='production')
    <!-- Yandex.Metrika counter -->
        <script type="text/javascript"> (function (m, e, t, r, i, k, a) {
                m[i] = m[i] || function () {
                    (m[i].a = m[i].a || []).push(arguments)
                };
                m[i].l = 1 * new Date();
                k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
            })(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            ym(52542934, "init", {
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            }); </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/52542934" style="position:absolute; left:-9999px;" alt=""/></div>
        </noscript> <!-- /Yandex.Metrika counter -->
@endif
