<footer class="footer">
    <div class="footer__wrapper content">
        <div class="footer__information">
            <div class="footer__college_name">
                <p>государственное бюджетное профессиональное
                    образовательное учреждение Самарской области «Самарский техникум авиационного и промышленного
                    машиностроения имени Д.И. Козлова»,<br>1954-{{date('Y')}}</p>
            </div>
            <div class="footer__contacts">
                <p>
                    443052, Cамара, Старый переулок, д.6<br/>
                    <b> 8(846) 955-22-20 – </b>приемная
                    директора<br>
                    <b>8(846) 955-22-11 – </b>приемная комиссия<br>
                    <b>8(846) 955-08-14</b> – заместитель директора по УОР<br>
                    <b>8(846) 228-55-86</b> – общежитие<br>
                </p>
            </div>
        </div>
    </div>
</footer>
{{Widget::run('App\Widgets\ClientMenu',['template'=>'mobile'])}}
<span class="mobile-btn" ref="mobile_menu_btn">
    <i class="fa fa-bars"></i>
</span>