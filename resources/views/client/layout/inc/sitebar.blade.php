<aside class="sitebar">
    <div class="sitebar-menu">
        <ul class="sitebar-menu__items">
            @foreach($siteBarItems as $siteBarItem)
                <li class="sitebar-menu__item @if($siteBarItem['url']==URL::current()) active @endif">
                    <a class="sitebar-menu__link" href="{{$siteBarItem['url']}}">{{$siteBarItem['name']}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</aside>