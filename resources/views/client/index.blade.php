@extends('client.layout.layout_index')
@section('title','СТАПМ')
@section('content')
    <section class="college_info container-white">
        <div class="college_info_wrapper content">
            <div class="college_info__header">
                <h1>{{env('COLLEGE_NAME')}}</h1>
            </div>
            <div class="college_info__text">
                <p>В соответствии с приказом Министерства авиационной промышленности от 20 августа 1954 г. За №567 на
                    базе завода № п/я 208 в сентябре 1954 г. Было организовано техническое училище №5 с
                    непосредственным подчинением Куйбышевскому областному управлению трудовых резервов. Первого октября
                    1954 г. был произведен первый звонок для учащихся численностью 250 человек.</p>
                <a class="btn btn--blue btn--long" href="{{route('client.history')}}">
                    Узнать больше
                </a>
            </div>
            <div class="college_info__image">
                <iframe src="//vk.com/video_ext.php?oid=-14002569&id=456239986&hash=efa5b0d2c6c9cd10&hd=2" width="853" height="230" frameborder="0" allowfullscreen></iframe>
{{--                <img class="img-responsive" src="/img/stapm.jpg" alt="{{env('COLLEGE_NAME')}}">--}}
            </div>
        </div>
    </section>
    <section>
        <div class="distance" style="text-align: center;
    margin: 10px 0;
    background-color: white;
">
            <a href="{{route('client.submission-form')}}" style="display: block;width: 100%">
                <img src="/img/online_submission.png" alt="Онлайн подача документов" style="margin: 0 auto" class="img-responsive">
            </a>
        </div>
    </section>
    <section>
        <div class="distance" style="text-align: center;
    margin: 10px 0;
    background-color: white;
">
            <a href="http://3d-tour.stapm.ru/" style="display: block;width: 100%" target="_blank">
                <img src="/img/360tour.png" alt="Виртуальный тур" style="margin: 0 auto" class="img-responsive">
            </a>
        </div>
    </section>
    <section>
        <div class="distance" style="text-align: center;
    margin: 10px 0;
    background-color: white;
">
            <a href="{{route('client.distance-learning')}}" style="display: block;width: 100%">
                <img src="/img/1banner_dist.png" alt="Дистанционное обучение" style="width: 400px; margin: 0 auto" class="img-responsive">
            </a>
        </div>
    </section>
    @if(env('SHOW_PROGRAM_ON_INDEX_PAGE',false))
        <section>
            <index-active-programs></index-active-programs>
        </section>
    @endif
    <div class="news-index">
        <div class="content">
            <div class="news-index__header">
                <div class="news-index__title">
                    <h3>Объявления</h3>
                </div>
                <div class="news-index__link">

                </div>
            </div>
            <annoncements></annoncements>
        </div>
    </div>

    <div class="news-index">
        <div class="content">
            <div class="news-index__header">
                <div class="news-index__title">
                    <h3>Новости</h3>
                </div>
                <div class="news-index__link">
                    <a class="link" href="{{route('client.news')}}">Все новости</a>
                </div>
            </div>
            <news-index></news-index>
        </div>
    </div>
@endsection
