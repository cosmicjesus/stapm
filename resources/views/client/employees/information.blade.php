@extends('client.layout.layout_with_sitebar')
@section('title','Информация для сотрудников')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="employee-information">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>'employee_information'])}}
    </div>
@endsection
