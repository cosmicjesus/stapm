@extends('client.layout.layout_with_sitebar')
@section('title','Дистанционное обучение')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="distance">
        <p>В соответствии с распоряжением Министерства образования и науки Самарской области от 03.04.2020 № 338-р «Об
            организации образовательной деятельности в образовательных организациях, расположенных на территории
            Самарской области, в условиях распространения новой коронавирусной инфекции (COVID-19) образовательный
            процесс с 06.04.2020 в ГБПОУ «СТАПМ им. Д.И.Козлова» будет организован дистанционно.</p>
        <br>
        <a href="/storage/documents/1585984470/WmIIfmNJNNVV6klcfOVvgN3V6otqJFU0804trO6Y.pdf" target="_blank"
           class="link">
            Положение об организации учебного процесса с применением электронного обучения и дистанционных
            образовательных технологий</a>
        <br>
        <a href="https://stapm.ru/news/kak-s-polzoy-provesti-vremya-v-samoizolyacii" target="_blank" class="link">
            Как с пользой провести время в самоизоляции</a>
        <br>
        <a href="{{route('client.services.psychologist')}}" target="_blank" class="link">
            Психологическая помощь во время режима самоизоляции</a>
        <br>
        <a href="{{route('client.dist-conc')}}" class="link">Мероприятия (конкурсы) для обучающихся с 6 по 30 апреля</a><br>
        <a href="{{route('client.services.el-library')}}" class="link">Полезные интернет-ресурсы</a>
        <p>
            Уважаемые коллеги! Постарайтесь, по возможности, быть на связи со своими студентами, чтобы оказать им своевременную помощь при возникновении сложной ситуации.
        </p>
        <hr>
        <div class="phones">
            <h4>Телефоны горячей линии для родителей и студентов</h4>
            <ul>
                <li><img src="/img/viber.jpg" alt="" width="50px"> +79084188800/+79270107427 - по вопросам
                    психологической поддержки
                </li>
                <li><img src="/img/viber.jpg" alt="" width="50px"> +79279006072 -
                    По вопросам связанным с учебной деятельностью
                </li>
                <li>
                    <img src="/img/viber.jpg" alt="" width="50px"> +79171681631 - По вопросам связанным с практикой
                    студентов
                </li>
                <li>
                    <img src="/img/viber.jpg" alt="" width="50px">/<img src="/img/whatsup.jpg" alt="" width="50px">
                    +79063472996 - По общим вопросам организации дистанционного обучения<br>. <b>Email</b>:nik-malcev@yandex.ru
                </li>
            </ul>
        </div>

        <div class="timetable">
            <div class="for-courses">
                <h4>Расписание по курсам</h4>
                <ul data-v-20f4ba10="">
                    <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                              href="/storage/news/214/files/YiVRDh0vQDWfG1cSFXciMfdQPn7VhrHOGuvfRGCV.pdf"
                                              target="_blank" class="link">1 курс</a></li>
                    <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                              href="/storage/news/214/files/lHB4M6purWwJl1n8lSDnykWX5lnqTU0xCTmsVGeT.pdf"
                                              target="_blank" class="link">2 курс</a></li>
                    <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                              href="/storage/news/214/files/8GfT8nCWA6qWlMf40fv7RWsrQjiNR0rpJfIW2QCd.pdf"
                                              target="_blank" class="link">3 курс</a></li>
                    <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                              href="/storage/news/214/files/7HWBqCK53bVVE69DVS8UYJZg1tQZxjFUqzco4Is3.pdf"
                                              target="_blank" class="link">2,3,4 курс (Производственная практика)</a>
                    </li>
                </ul>
            </div>
            @foreach($groups as $key=>$groupsList)
                <div class="course_{{$key}}">
                    <h4>{{$key}} курс</h4>
                    <hr>
                    <div class="rasp">
                        @foreach($groupsList as $groupKey=>$group)
                            <h4>Расписание для группы {{$group}}</h4>
                            <div class="{{$groupKey}}">
                                {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>"time_table_{$groupKey}"])}}
                            </div>
                        @endforeach
                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
@endsection
