@if($isContract)
    <h3>По договору дуального обучения</h3>
@endif
<div class="table-scroll">
    <table class="table table-striped table-bordered">
        <tr>
            <th>№ п/п</th>
            <th>ФИО</th>
            <th>Оригинал документа</th>
            <th>Средний балл</th>
        </tr>
        @foreach($enrollees as $enrollee)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$enrollee['full_name']}}</td>
                <td>{{$enrollee['has_original']?'Да':"Нет"}}</td>
                <td>{{$enrollee['avg']}}</td>
            </tr>
        @endforeach
    </table>
</div>