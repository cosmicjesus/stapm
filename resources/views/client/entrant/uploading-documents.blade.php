@extends('client.layout.with_sitebar')
@section('sub_content')
   <p>При помощи этой формы Вы сможете догрузить недостающие документы. Заполните ваше ФИО и дату рождения, добавьте файлы и нажмите кнопку "Загрузить"</p>
   <ul>
      <li>Для загрузки допустимы файлы формата PDF, JPEG, PNG</li>
      <li>Максимальный размер файла не должен превышать 5Мб</li>
   </ul>
   <uploading-documents></uploading-documents>
@endsection
