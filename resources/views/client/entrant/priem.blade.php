@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title') на {{\Carbon\Carbon::now()->format('d.m.Y')}}</h1>
    @if(count($data['programs']))
        <div class="table-scroll">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Программа</th>
                    <th>Срок обучения</th>
                    <th>План приема</th>
                    <th>Подано заявлений без оригинала документа</th>
                    <th>Подано заявлений с оригиналом документа</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['programs'] as $program)
                    <tr>
                        <td>
                            <a href="{{route('client.program_detail',$program['program_slug'])}}">{{$program['program_name']}}</a>
                        </td>
                        <td>{{convertTrainingPeriod($program['duration'])}}</td>
                        <td>{{$program['reception_plan']}}</td>
                        <td>{{$program['data']['count_with_out_documents']}}</td>
                        <td>{{$program['data']['count_with_documents']}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Всего:</td>
                    <td>{{$data['total']['reception_plan']}}</td>
                    <td>{{$data['total']['count_with_out_documents']}}</td>
                    <td>{{$data['total']['count_with_documents']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @else
        <div class="text-center">
            <h3>Внимание, прием завершен</h3>
            <p>В соответствии с пунктом 19 порядка приема граждан по образовательным программам, прием документов
                завершен. <br>Для получения информации о вакантных местах для приема в порядке перевода, Вам необходимо
                перейти на страницу
                <a href="{{route('client.acceptance-transfer')}}">Вакантные места для приема и перевода</a></p>
        </div>
    @endif

    @if(setting('showEnrolleesRatins',false))
        @if(count($data['programs']))
            <ul>
                @foreach($data['programs'] as $program)
                    <li>
                        <a href="{{route('client.priem.raiting',['slug'=>$program['slug']])}}">
                            Рейтинг поданных заявлени
                            по {{$program['type']==1?'специальности':'профессии'}} {{$program['program_name']}} ({{convertTrainingPeriod($program['duration'])}})</a>
                    </li>
                @endforeach
            </ul>
        @endif
    @else
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['enrollment_order']])}}
    @endif
@endsection
