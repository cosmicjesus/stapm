@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <p>За {{getNumEnding(date('Y') - 1954,['год','года','лет'],true)}} наш техникум подготовил и выпустил более 20 тысяч
        молодых квалифицированных рабочих
        для крупнейших предприятий города.<br>
    </p>
    @if(count($partners))

        <div class="news">
            @foreach($partners as $partner)
                <div class="news__item">
                    <div class="news__title">
                        <a class="news__title__link" href="{{$partner->getSiteUrl()}}" target="_blank">{{$partner->getName()}}</a></div>

                        <div class="news__image">
                            <img class="img-responsive" src="{{$partner->getImage()}}">
                        </div>
                        <div class="news__preview">
                            {!! $partner->getDescription() !!}
                            <p>Наименование договора:{{$partner->getContractType()}}</p>
                            <p>Дата подписания:{{$partner->getStartDate()}}</p>
                            <p>Срок действия:{{$partner->getEndDate()}}</p>
                            <p><a href="{{$partner->getSiteUrl()}}" target="_blank">Ссылка на сайт</a></p>
                            @if($partner->getFiles())
                                <p>
                                <ul>
                                    @foreach($partner->getFiles() as $file)
                                        <li><a href="{{$file->getPath()}}">{{$file->getName()}}</a></li>
                                    @endforeach
                                </ul>
                                </p>
                            @endif
                        </div>
                </div>
            @endforeach
        </div>

    @else
        <h3 class="text-center">Нет информации о социальных партнерах</h3>
    @endif
@endsection