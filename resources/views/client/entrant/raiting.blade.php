@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title') на {{\Carbon\Carbon::now()->format('d.m.Y')}}</h1>
    <div class="raiting">
        <table class="table table-bordered table-striped">
            <tr>
                <th>№ п/п</th>
                <th>ФИО</th>
                <th>Статус</th>
                <th>Оригинал документа об образовании</th>
                <th>Ср.балл</th>
                <th>Сумма баллов по профильным предметам</th>
                <th>Приоритетная специальность</th>
            </tr>
            @foreach($entrants as $key =>$entrant)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$entrant['full_name']}}</td>
                    <td
                            @if($entrant['status']=='enrollee')
                           class="td-success"
                            @else
                            class="td-warning"
                            @endif

                    >
                        @if($entrant['status']=='enrollee')
                            Данные обработаны
                        @else
                            Идет обработка данных
                        @endif
                    </td>
                    <td>{{$entrant['has_original']?'Да':'Нет'}}</td>
                    <td>{{$entrant['average_score']}}</td>
                    <td>{{$entrant['sum_of_grades_of_specialized_disciplines']}}</td>
                    <td>{{$entrant['is_priority']?'Да':'Нет'}}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
