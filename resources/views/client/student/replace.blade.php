@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>'replace_docs'])}}
    <replace-class></replace-class>
@endsection
