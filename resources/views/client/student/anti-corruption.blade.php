@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="anti-corruption">
        <p>Сайт Генеральной прокуратуры Российской Федерации <a href="https://genproc.gov.ru" target="_blank">https://genproc.gov.ru</a></p>
        
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['college_anti_corruption']])}}
    </div>
@endsection