@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="vospitanie">
        <h4>Средства воспитания</h4>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['sredstva_vops']])}}
        <h4>Социальный педагог</h4>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['social_teacher']])}}
    </div>
@endsection