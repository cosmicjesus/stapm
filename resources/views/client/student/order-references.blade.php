@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="order-references" bp="grid">

        @if(!\App\Helpers\Settings::orderHelpsFromSite())
            <div bp="10 offset-2" class="alert alert-warning" style="text-align: center">
                <h4>Заказ справок через сайт временно недоступен</h4>
                <h4>Для заказа справки, обратитесь в кабинет 8А</h4>
            </div>
        @endif
        <div class="order-error" bp="10 offset-2">

        </div>
        <div bp="10 offset-2">
            <form action="" name="order-references" id="order-references" class="order-references">
                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <div class="form-group">
                    <label for="student-lastname">Фамилия</label>
                    <input name="lastname" id="student-lastname" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="student-firstname">Имя</label>
                    <input name="firstname" id="student-firstname" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="student-middlename">Отчество</label>
                    <input name="middlename" id="student-middlename" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="group_id">Группа</label>
                    <select name="group_id" id="group_id" class="form-control">
                        @foreach($groups as $key =>$value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label for="training">
                            <input type="checkbox" class="type-checkbox-js" name="types[]" id="training"
                                   value="training"> Справка об обучении
                        </label>
                    </div>
                    <div class="form-group training-types type-training-js">
                        <p>Выберите нужное количество справок</p>
                        <label for="training[count]">Количество</label>
                        <input type="number" id="training[count]" name="training[count]" min="1" max="5"
                               class="form-control" value="1"
                               required>
                    </div>
                    <br>
                    <div class="checkbox">
                        <label for="money">
                            <input type="checkbox" class="type-checkbox-js" name="types[]" id="money" value="money">
                            Справка о стипендии
                        </label>
                    </div>
                    <div class="form-group training-types type-money-js">
                        <p>Выберите период и количество справок</p>
                        <label for="money[period]">Период</label>
                        <select name="money[period]" id="money[period]" class="form-control">
                            @foreach(getReferencesPeriod() as $key=> $period)
                                <option value="{{$key}}">{{$period}}</option>
                            @endforeach
                        </select>
                        <label for="money[count]">Количество</label>
                        <input type="number" id="money[count]" name="money[count]" min="1" max="5"
                               class="form-control" value="1"
                               required>
                    </div>
                </div>
                {{--<div class="form-group">--}}
                {{--<label for="count">Количество</label>--}}
                {{--<input type="number" id="count" name="count" min="1" max="5" class="form-control" value="1"--}}
                {{--required>--}}
                {{--</div>--}}
                @if(\App\Helpers\Settings::orderHelpsFromSite())
                    <div>
                        <button class="btn btn--blue">
                            Заказать
                        </button>
                    </div>
                @endif
            </form>
        </div>
    </div>
@endsection