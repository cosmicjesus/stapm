@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="documents">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>$category])}}
    </div>
@endsection
