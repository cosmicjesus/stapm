@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <p>
        В техникуме для создания необходимых условий для организации питания обучающихся и работников техникума заключен
        договор с индивидуальным предпринимателем, в рамках которого студенты, в том числе инвалиды и лица с ОВЗ и преподаватели имеют возможность получить
        горячее питание: первые и вторые блюда ,широкий ассортимент мучных кондитерских изделий, разнообразные салаты
        нескольких видов, горячие и прохладительные напитки по доступным ценам.
        Обеденный зал 240 кв метров имеет 60 посадочных
        мест, с возможностью расширения до 120 мест. <br>В 2015 г. выполнен косметический ремонт обеденного зала,
        установлено новая линия раздачи (мармит первых блюд)<br>
        В столовой возможна оплата как наличными, так и банковскими картами и устройствами с бесконтактной оплатой
        <br>Режим работы с 8.30 до 16.30
    </p>
    <div style="display: flex">
        <div>
            <a rel="photos" class="/foto/food4.JPG" data-fancybox data-caption='Столовая'>
                <img class="img-responsive" src="/foto/food4.JPG" alt="Общежитие"/>
            </a>
        </div>
        <div>
            <a rel="photos" class="/foto/food6.JPG" data-fancybox data-caption='Столовая'>
                <img class="img-responsive" src="/foto/food6.JPG" alt="Общежитие"/>
            </a>
        </div>
        <div>
            <a rel="photos" class="/foto/food10.JPG" data-fancybox data-caption='Столовая'>
                <img class="img-responsive" src="/foto/food10.JPG" alt="Общежитие"/>
            </a>
        </div>
        <div>
            <a rel="photos" class="/foto/food1.JPG" data-fancybox data-caption='Столовая'>
                <img src="/foto/food1.JPG" class="img-responsive"/>
            </a>
        </div>
    </div>
    {{--<table>--}}
    {{--<tr>--}}
    {{--<td><img width="200px" src="/foto/food4.JPG" alt="Общежитие"/></td>--}}
    {{--<td><img width="200px" src="/foto/food6.JPG" alt="Общежитие"/></td>--}}
    {{--<td><img width="200px" src="/foto/food10.JPG" alt="Общежитие"/></td>--}}
    {{--</tr>--}}
    {{--</table>--}}
@endsection