@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <img src="/foto/medik.jpg" class="img" alt="Медик" style="float: left;width: 250px;margin-right: 5px">
    <p>В нашем техникуме имеется медицинский пункт, который обслуживает сотрудников и учащихся, оказывая
        лечебно-профилактическую помощь и контролирует соблюдение санитарно-гигиенических норм в учебном процессе.<br>
        Заведующая медпунктом: <span class="bold">Несветаева Людмила Николаевна</span><br>
        <span class="bold">Режим работы: в рабочие дни ежедневно с 9.00 до 15.30.</span></p><br><br><br>
@endsection