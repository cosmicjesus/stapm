@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="extramural-duties">
        <ul>
            <li>Выполнять учебный план в соответствии с учебным графиком</li>
            <li>Аккуратно выполнять все требования учебной части</li>
            <li>Возвращать вовремя книги полученные из библиотеки;</li>
            <li>Представлять по требованию техникума необходимые справки и документы</li>
            <li>Являться в техникум по вызову, давать ответы на запросы техникума и сообщать о причинах вынужденных перерывов в учебной работе</li>
            <li>Своевременно выполнять требования преподавателей</li>
            <li>Следить за тем, чтобы в зачетной книжке своевременно делались записи о сданных зачетах и экзаменах, а также своевременно заверять у заведующим</li>
            <li>отделением каждую страницу зачетной книжки</li>
            <li>Немедленно извещать учебную часть о перемене адреса жительства</li>
            <li>В письмах, которые студент заочник адресует в техникум, должны быть указаны: фамилия, имя, отчество(полностью), специальность, шифр и домашний адрес с указанием индекса</li>
            <li>Своевременно сообщать и высылать соответствующие документы об изменении фамилии или имени</li>
        </ul>
    </div>
@endsection