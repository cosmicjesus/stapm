@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="conference">
        <h4>Дата проведения: {{dateFormat($conference['date_of_event'])}}</h4>
        {!! $conference['full_text'] !!}
        <div class="news-detail__files">
            <h3>Дополнительные файлы</h3>
            <ul>
                @foreach($conference['files'] as $file)
                    <li><a href="{{$file['path']}}" target="_blank">{{$file['name']}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection