@extends('client.layout.layout_with_sitebar')
@section('title','Расписание')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="timetable">
        <div class="for-courses">
            <h4>Расписание по курсам</h4>
            <ul data-v-20f4ba10="">
                <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                          href="/storage/news/214/files/YiVRDh0vQDWfG1cSFXciMfdQPn7VhrHOGuvfRGCV.pdf"
                                          target="_blank" class="link">1 курс</a></li>
                <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                          href="/storage/news/214/files/lHB4M6purWwJl1n8lSDnykWX5lnqTU0xCTmsVGeT.pdf"
                                          target="_blank" class="link">2 курс</a></li>
                <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                          href="/storage/news/214/files/8GfT8nCWA6qWlMf40fv7RWsrQjiNR0rpJfIW2QCd.pdf"
                                          target="_blank" class="link">3 курс</a></li>
                <li data-v-20f4ba10=""><a data-v-20f4ba10=""
                                          href="/storage/news/214/files/7HWBqCK53bVVE69DVS8UYJZg1tQZxjFUqzco4Is3.pdf"
                                          target="_blank" class="link">2,3,4 курс (Производственная практика)</a>
                </li>
            </ul>
        </div>
        @foreach($groups as $key=>$groupsList)
            <div class="course_{{$key}}">
                <h4>{{$key}} курс</h4>
                <hr>
                <div class="rasp">
                    @foreach($groupsList as $groupKey=>$group)
                        <h4>Расписание для группы {{$group}}</h4>
                        <div class="{{$groupKey}}">
                            {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>"time_table_{$groupKey}"])}}
                        </div>
                    @endforeach
                </div>
                <hr>
            </div>
        @endforeach
    </div>
@endsection
