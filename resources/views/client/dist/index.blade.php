@extends('client.layout.layout_with_sitebar')
@section('title','Дистанционное обучение')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="distance">
        <p>В соответствии с распоряжением Министерства образования и науки Самарской области от 03.04.2020 № 338-р «Об
            организации образовательной деятельности в образовательных организациях, расположенных на территории
            Самарской области, в условиях распространения новой коронавирусной инфекции (COVID-19) образовательный
            процесс с 06.04.2020 в ГБПОУ «СТАПМ им. Д.И.Козлова» будет организован дистанционно.
        </p>
        <p>
            Уважаемые коллеги! Постарайтесь, по возможности, быть на связи со своими студентами, чтобы оказать им
            своевременную помощь при возникновении сложной ситуации.
        </p>
        <section>
            <div class="distance" style="text-align: center;
    margin: 10px 0;
    background-color: white;
">
                <a href="{{route('client.submission-form')}}" style="display: block;width: 100%">
                    <img src="/img/submission_banner.png" alt="Онлайн подача документов" style="margin: 0 auto" class="img-responsive">
                </a>
            </div>
        </section>
        <br>
        @foreach($sections as $items)
            <div class="content_row">
                @foreach($items as $item)
                    <div class="item">
                        <a href="{{$item['url']}}"
                           class="link">
                            <div class="head">
                                <h3> {{$item['label']}}</h3>
                            </div>
                            <div class="body">
                                <i class="fa {{$item['icon']}} icon"></i>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        @endforeach

    </div>
@endsection
