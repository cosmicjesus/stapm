@extends('client.layout.layout_with_sitebar')
@section('title','Ответственные лица за организацию дистанционного обучения')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="hot-line">
        <table class="table table-bordered table-striped">
            <tr>
                <th>ФИО</th>
                <th>Должность</th>
                <th>За что ответственный</th>
                <th>Контакты</th>
                <th>Соцсети</th>
            </tr>
            @foreach($peoples as $people)
                <tr>
                    <td>{{$people['name']}}</td>
                    <td>{{$people['position']}}</td>
                    <td>{{$people['otv']}}</td>
                    <td>{{$people['phone']}}<br>{{$people['email']}}</td>
                    <td>
                        @foreach($people['social'] as $social)
                            <img src="/img/{{$social}}.jpg" alt="" width="50px">
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
