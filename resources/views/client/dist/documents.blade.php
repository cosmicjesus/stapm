@extends('client.layout.layout_with_sitebar')
@section('title','Об организации учебного процесса с применением электронного обучения и дистанционных
                            образовательных технологий')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="documents">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>'dist_docs'])}}
    </div>
@endsection
