@extends('client.layout.layout_with_sitebar')
@section('title','Платформы')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="instructions">
      <table class="table table-bordered table-striped">
          <tr>
              <td>
                  <img src="https://af12.mail.ru/cgi-bin/readmsg?id=15866906331201132683;0;0;3&mode=attachment&email=silantev-zhenya@mail.ru" alt="">
              </td>
              <td>
                  Российская электронная школа
              </td>
              <td>
                  <a href="https://resh.edu.ru/" target="_blank">Перейти</a>
              </td>
          </tr>
          <tr>
              <td>
                  <img src="https://af12.mail.ru/cgi-bin/readmsg?id=15866906331201132683;0;0;2&mode=attachment&email=silantev-zhenya@mail.ru" class="img-responsive" alt="" width="400">
              </td>
              <td>
                  Облачное хранилище майл для студентов
              </td>
              <td>
                  <a href="https://cloud.mail.ru/public/MFj1/4Dgo1CdEJ/" target="_blank">Перейти</a>
              </td>
          </tr>
          <tr>
              <td>
                  <img src="https://im0-tub-ru.yandex.net/i?id=039102a8d7bf4f2b4e452da9deb6b7e3&n=13&exp=1" alt="">
              </td>
              <td>
                 Google форма для отметки о получении задания
              </td>
              <td>
                  <a href="https://docs.google.com/forms/d/e/1FAIpQLSdx__SU8lZ-fGdTrana0X8bS6pi7rvLpl5VlMA9vN2xMyYb0Q/viewform" target="_blank">Перейти</a>
              </td>
          </tr>
      </table>
    </div>
@endsection
