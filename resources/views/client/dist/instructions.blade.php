@extends('client.layout.layout_with_sitebar')
@section('title','Инструкции и рекомендации для обучающихся и родителей')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="instructions">
        <a href="https://cloud.mail.ru/public/2Cub/22ou9c3yX/" target="_blank">Видеолекции по профилактике травматизма на железной дороге</a>
        <br>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>"dist_instructions"])}}
    </div>
@endsection
