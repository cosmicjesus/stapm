@extends('client.layout.layout_with_sitebar')
@section('title','Досуговая деятельность')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="hot-line">
        <table border="0">
            <tbody>
            <tr>
                <td>
                    <p>Узнай про историю Великой Победы, подвиг соотечественников, хронику сражений, историю городов
                        -героев на сайте</p>
                    <p><a href="https://www.may9.ru/">https://www.may9.ru/</a></p>
                </td>
            </tr>
            <tr>
                <td>
                    <h3>Перечень фильмов рекомендуемых к просмотру</h3>
                    <ul>
                        <li><a href="https://www.youtube.com/watch?v=ov7bKyahGL4" target="_blank">Судьба Человека</a></li>
                        <li><a href="https://www.youtube.com/watch?v=isL62qzE1E4" target="_blank">Брестская крепость</a></li>
                        <li><a href="https://www.youtube.com/watch?v=I37qKN_Bugk" target="_blank">28 панфиловцев</a></li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Память народа: Подлинные документы о второй мировой войне</p>
                    <p><a href="https://pamyat-naroda.ru/">https://pamyat-naroda.ru/</a></p>
                </td>
            </tr>
            </tbody>
        </table>
        <table border="0">
            <tbody>
            <tr>
                <td>
                    <p><strong>&laquo;В Питере прекрасно все&hellip; от зданий до людей&raquo;</strong></p>
                    <p>В Санкт &ndash; Петербурге действительно прекрасно все, особенно музеи.<br/>Старинные картины,
                        почувствовать глубину, оценить настроение автора, понять смысл картины без описания к ней, а
                        ведь это не так-то и просто. Сможешь ли ты, стать великим ценителем искусства?<br/>Как можно
                        посетить музей, не выходя из дома? Мы знаем ответ на это вопрос.<br/>Переходи по ссылке и
                        окажись в Государственном музеи Санкт &ndash; Петербурга в один клик.</p>
                    <p><a href="https://bit.ly/2IOQDjq">https://bit.ly/2IOQDjq</a></p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;<strong>&laquo;В твоем чайнике, революция&raquo;</strong>
                    <p>Да, думаю не скоро мы еще сможем попасть за границу, в Америку&hellip;.<br/>Но не надо
                        отчаиваться, окунуться в мир интересного, можно прямо не вставая с дивана.<br/>Предлагаем вам
                        отправиться в Британские галереи декоративно-прикладного творчества и дизайна.<br/>Ведь именно
                        там нашли тайны, которые хранят в себе обычный чайник, кружка и вся посуда.<br/>По узорам на
                        них, они прочитали невероятные вещи&hellip;.<br/>Заинтересованы?! Скорей кликай и погружайся в
                        мир искусства Британской галереи декоративно-прикладного творчества и дизайна.<br/>Ах, да&hellip;.
                        Не забудь перевести страницу на русский, поверь, такого ты не знал&hellip;.</p>
                    <p><a href="https://www.metmuseum.org/primer/british-galleries#poster-intro">https://www.metmuseum.org/primer/british-galleries#poster-intro</a>
                    </p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;<strong>&laquo;..из туманов возник золотой Будапешт С яркой краской роскошных
                        фасадов..&raquo;</strong>
                    <p>И правда фасады Будапешта впечатляют своей красотой и силой, но не только фасады этого города
                        могу впечатлить тебя.<br/>Художники в Будапешете создают не малые &laquo;драгоценности&raquo;
                        так называют они картины. Ведь искусство живописи разного народа, это и есть драгоценность. Все
                        в нашем мире любят драгоценности, и ты не исключение&hellip;Да,да, ты, за экраном монитора&hellip;
                        я тебе говорю, готов погрузить в культуру Будапешта и оценить их
                        &laquo;драгоценности&raquo;.<br/>Тогда не медли, кликай мышкой по ссылке и переходи в одну из
                        галерей &laquo;драгоценностей&raquo; Будапешта.</p>
                    <p><a href="https://bit.ly/3d08L80">https://bit.ly/3d08L80</a></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>&laquo;Нью-Йорк город мечты&raquo;</strong><br/>Мечтать всегда нужно и хочется каждому
                        человеку, а ведь если ваша мечта это посетить музей НьюЙорка, оценить качество картин и просто
                        &laquo;порадовать глаз&raquo;?<br/>ТСПК легко исполни ваши мечты всего в один клик.<br/>Ведь мы
                        предлагаем посетить один из самых интересных музеев МЕТРОПОЛИТЕН&hellip;<br/>Стало интересно?
                        Скорей переходи по ссылке&hellip;<br/>Ах, да&hellip;. Не забудь перевести страницу на русский,
                        поверь, такого ты не знал&hellip;.</p>
                    <p><a href="https://www.metmuseum.org/art/online-features/met-360-project">https://www.metmuseum.org/art/online-features/met-360-project</a>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>&laquo;Амстердам.. Город вдохновляющий тебя&raquo;</strong><br/>Амстердам, каждый из вас
                        думаю представляет этот прекрасный город, эти улицы&hellip; И они вдохновляют и восхищают
                        вас!<br/>Говорят, что там выставка Ван Гога, самого знаменитого художника не менее знаменитых
                        картин.<br/>Но не один Серега не зовет вас на выставку Ван Гога&hellip;<br/>Это легко исправить.
                        ТСПК позаботился о тебе и приглашает тебя на выставку прямо сейчас.<br/>Переходи по ссылке и не
                        скучай)</p>
                    <p>
                        <a href="https://artsandculture.google.com/streetview/KwF-AdF1REQl6w?sv_lng=4.8813324&amp;sv_lat=52.358454&amp;sv_h=63.89657847583723&amp;sv_p=-9.650640227187353&amp;sv_pid=Nhu3UwMqQHZJBpIMBcAafg&amp;sv_z=0.653696837514858">Перейти
                            по ссылке</a></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>&laquo;Ах Эрмитаж, Ах Эрмитаж&raquo;</strong><br/>Если ты не был(а) еще ни разу в
                        Эмрмитаже, а из-за карантина еще не известно, когда сможешь оказаться там.<br/>Предлагаем не
                        отчаиваться и погрузиться в мир искусства и прекрасного, прямо сейчас, не выходя из дома.<br/>Проходи
                        по ссылке и ты сможешь просмотреть Эрмитаж изнутри и прочувствовать всю историю прекрасного</p>
                    <p><a href="https://bit.ly/33nCpQg">https://bit.ly/33nCpQg</a></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Самый пышный балет П.И. Чайковского&nbsp;</strong>-&nbsp; "Спящая красавица" -
                        блистательную сказку о принцессе Авроре (Светлана Захарова) и принце Дезире (Дэвид Холберг),
                        доброй фее Сирени (Мария Аллаш) и страшной злой фее Карабос (Алексей Лопаревич).</p>
                    <p><a href="https://www.youtube.com/channel/UCUimBc08CcsoCvP_WlYJl0Q">https://www.youtube.com/channel/UCUimBc08CcsoCvP_WlYJl0Q</a>
                    </p>
                </td>
            </tr>
            </tbody>
        </table>
        <p><strong>Виртуальные экскурсии:</strong></p>
        <p><strong> Ссылки на виртуальные музеи, галереи и театры&nbsp;мира&nbsp; </strong></p>
        <p>&nbsp;</p>
        <p>Как посетить самые известные музеи, галереи и театры мира без очереди и без билетов? Как за одни выходные
            побывать в Лувре, Прадо и Эрмитаже? Как успеть после техникума на экскурсию, чтобы рассмотреть хорошенько
            череп неандертальца или роспись на древнегреческой вазе?&nbsp; Как посмотреть картины знаменитых художников?
            Ответ на все вопросы один &ndash; отправиться в виртуальный тур по лучшим музеям мира.</p>
        <p>&nbsp;</p>
        <p><a href="https://yadi.sk/i/YNH0ibrp5xqB2g"><strong>Виртуальные музеи, галереи и театры</strong></a></p>
        <ul>
            <li><strong>Эрмитаж&nbsp;</strong><a href="https://bit.ly/33nCpQg">https://bit.ly/33nCpQg</a>
            </li>
            <li>
                А тут
                пятичасовое путешествие по&nbsp;<strong>Эрмитажу,</strong>снятое на iPhone 11 Pro одним дублем в 4К <a
                        href="https://bit.ly/39VHDoI">https://bit.ly/39VHDoI</a>
            </li>
        </ul>
        <ul>
            <li><strong>Третьяковская галерея</strong><a
                        href="https://artsandculture.google.com/partner/the-state-tretyakov-gallery">Третьяковская
                    галерея</a></li>
            <li><strong>Музей истории искусств</strong>(Kunsthistorisches Museum), Вена <a
                        href="https://bit.ly/3d08Zfm">музей истории искусств Вена</a></li>
        </ul>
        <ul>
            <li><strong>цифровые архивы Уффиц</strong>и&nbsp;<a href="https://www.uffizi.it/en/pages/digital-archives">https://www.uffizi.it/en/pages/digital-archives</a>
            </li>
            <li><strong>Лувр</strong> <a href="https://bit.ly/2WciGBi">Лувр</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                        href="https://www.louvre.fr/en/media-en-ligne">Лувр видео</a></li>
            <li><strong>Государственный Русский музе</strong>й (Санкт-Петербург) <a href="https://bit.ly/2IOQDjq">Русский
                    музей Санкт-Петербург</a></li>
        </ul>
        <ul>
            <li><strong>Британский музей,</strong>онлайн-коллекция одна из самых масштабных, более 3,5 млн экспонатов <a
                        href="https://www.britishmuseum.org/">https://www.britishmuseum.org</a></li>
            <li><strong>Британский музей</strong>, виртуальные экскурсии по музею и экспозициям на официальном YouTube
                канале <a
                        href="https://www.youtube.com/user/britishmuseum">https://www.youtube.com/user/britishmuseum</a>
            </li>
        </ul>
        <ul>
            <li><strong>Прадо,</strong>фото более 11 тысяч произведений, поиск по художникам (с алфавитным указателем) и
                тематический поиск <a href="https://www.museodelprado.es/">https://www.museodelprado.es</a>
            </li>
        </ul>
        <ul>
            <li><strong>музеи Ватикана и Сикстинская капелла</strong> <a
                        href="http://www.vatican.va/various/cappelle/sistina_vr/index.html">http://www.vatican.va/various/cappelle/sistina_vr/index.html</a>
            </li>
        </ul>
        <ul>
            <li><strong>Метрополитен-музе</strong>й, Нью-Йорк&nbsp; <a href="https://www.metmuseum.org/">https://www.metmuseum.org</a>
            </li>
        </ul>
        <ul>
            <li><strong>онлайн-коллекция нью-йоркского музе</strong>я современного искусства (МоМА), около 84 тысяч
                работ <a href="https://www.moma.org/collection/?=undefined&amp;page=3&amp;direction=fwd">https://www.moma.org/collection/?=undefined&amp;page=3&amp;direction=fwd</a>
            </li>
        </ul>
        <ul>
            <li><strong>онлайн-коллекция музея Гуггенхайм</strong> <a
                        href="https://www.guggenheim.org/collection-online">https://www.guggenheim.org/collection-online</a>
            </li>
        </ul>
        <ul>
            <li><strong>музей Сальвадора Дали</strong><a href="https://bit.ly/33iHVmX">https://bit.ly/33iHVmX</a></li>
            <li><strong>видео-галерея NASA,</strong>недлинные видео в высоком разрешении&nbsp; <a
                        href="https://www.nasa.gov/content/ultra-high-definition-video-gallery">https://www.nasa.gov/content/ultra-high-definition-video-gallery</a>
            </li>
        </ul>
        <ul>
            <li><strong>Смитсоновский музей</strong> <a href="https://www.si.edu/exhibitions/online">https://www.si.edu/exhibitions/online</a>
            </li>
        </ul>
        <ul>
            <li><strong>Национальный музей в Кракове</strong> <a
                        href="https://bit.ly/3d29dT0">https://bit.ly/3d29dT0</a></li>
        </ul>
        <ul>
            <li><strong>Музей изобразительных искусств в Будапеште</strong> <a href="https://bit.ly/3d08L80">https://bit.ly/3d08L8</a>
            </li>
        </ul>
    </div>
@endsection
