@extends('client.layout.layout_with_sitebar')
@section('title','Военные сборы')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="documents">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Дата</th>
                <th>Группа</th>
                <th>Номер занятия</th>
                <th>Ресурсы</th>
            </tr>
            @foreach($data as $item)
                @foreach($item['group'] as $elem)
                    <tr>
                        @if(count($item)>1&&$loop->iteration==1)
                            <td rowspan="{{count($item['group'])}}">
                                {{$item['day_name']}}
                                <br>
                                {{$item['date']}}
                            </td>
                        @endif
                        <td>
                            {{$elem['name']}}
                        </td>
                        <td>
                            {{$elem['lessons']}}
                        </td>
                        <td>
                            <a href="{{$elem['link']}}" target="_blank">Открыть</a>
                        </td>
                    </tr>
                @endforeach
            @endforeach
        </table>
    </div>
@endsection
