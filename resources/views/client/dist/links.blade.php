@extends('client.layout.layout_with_sitebar')
@section('title','Полезные ссылки')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="hot-line">
        <ul>
            <li><img src="/img/viber.jpg" alt="" width="50px"> +79084188800/+79270107427 - по вопросам
                психологической поддержки
            </li>
            <li><img src="/img/viber.jpg" alt="" width="50px"> +79279006072 -
                По вопросам связанным с учебной деятельностью
            </li>
            <li>
                <img src="/img/viber.jpg" alt="" width="50px"> +79171681631 - По вопросам связанным с практикой
                студентов
            </li>
            <li>
                <img src="/img/viber.jpg" alt="" width="50px">/<img src="/img/whatsup.jpg" alt="" width="50px">
                +79063472996 - По общим вопросам организации дистанционного обучения<br>. <b>Email</b>:nik-malcev@yandex.ru
            </li>
        </ul>
    </div>
@endsection
