@extends('client.layout.layout_with_sitebar')
@section('title','Государственная итоговая аттестация')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="documents">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>'gia'])}}
    </div>
@endsection
