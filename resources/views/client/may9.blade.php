@extends('client.layout.layout_with_sitebar')
@section('title','2020-Год памяти и славы: материалы в помощь')
@section('content')
    <h1 class="information_title">2020-Год памяти и славы: материалы в помощь</h1>
    <div class="may9">
        <h2>Уважаемые коллеги и студенты!</h2>
        <p>
            Вот и наступил 2020 год. Позади праздничные новогодние каникулы, а впереди огромная работа по
            патриотическому воспитанию.
            2020 год Указом Президента России объявлен Годом памяти и славы в целях сохранения исторической памяти и в
            ознаменование 75-летия Победы в Великой Отечественной войне
            <br>
            <a href="https://skazka.caduk.ru/DswMedia/ukazprezidentaoprovedeniigodapamyatiislavyi.png" target="_blank">Указ
                президента РФ</a>
        </p>
        <img src="foto/9may_logo.jpg" alt="9 мая" class="img-responsive">
        <p>
            «75 лет Победы пройдет красной нитью по всему 2020 году, <b>начал работу официальный сайт 9мая.рф или <a href="https://may9.ru/" target="_blank" class="link">MAY9.RU</a>»</b>.
            На этом сайте можно черпать всю информацию о 75-летии Победы. Сайт станет и новостным, и историческим
            агрегатором, там будет все о памятных, праздничных, образовательных и иных акциях, которые приурочены к Дню
            Победы, там будет огромный фотоархив, описание ключевых событий войны, будут собраны все записи
            Левитана».Пресс-служба президента РФ уточнила, что MAY9.RU будет «регулярно пополняться актуальными
            новостями, историко-документальными материалами, информацией о старте новых акций». В пресс-службе также
            напомнили, что сайт Победы функционирует с 2005 года, обновляя контент каждые пять лет.
        </p>
        <p>

        <h3>Перечень фильмов рекомендуемых к просмотру</h3>
        <ul>
            <li><a href="https://www.youtube.com/watch?v=ov7bKyahGL4" target="_blank">Судьба Человека</a></li>
            <li><a href="https://www.youtube.com/watch?v=isL62qzE1E4" target="_blank">Брестская крепость</a></li>
            <li><a href="https://www.youtube.com/watch?v=I37qKN_Bugk" target="_blank">28 панфиловцев</a></li>
        </ul>
        </p>
        <div>
            {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>'may9'])}}
        </div>
        <div class="annoncements-index">
            @foreach($news as $item)
                <div class="annoncement container-white">
                    <div class="annoncement__title">
                        <a href="{{$item['url']}}" class="link">{{$item['title']}}</a>
                    </div>
                    <div class="annoncement__image">
                        <img src="{{$item['image']}}" alt="{{$item['title']}}" class="img-responsive">
                    </div>
                    <div class="annoncement__text">
                        {!! $item['preview'] !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

