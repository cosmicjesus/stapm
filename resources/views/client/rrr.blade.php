@extends('client.layout.layout_with_sitebar')
@section('title',' Профилактика')
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="prevention">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>"prevention"])}}
    </div>
@endsection
