@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="documents">
        <div class="documents__basic">
            @if(count($categories))
                @include('client.index.documents.basic',['categories'=>$categories])
            @else
                <h2>Документы отсутствуют</h2>
            @endif
        </div>
        <h2>Предписания органов осуществляющих надзор</h2>
        <regulations></regulations>
    </div>
@endsection