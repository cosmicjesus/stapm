@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="structura">
        <img src="/img/struktura.png" alt="Структура управления" class="img">

        <div class="files">
            {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['structure_files']])}}
        </div>

        <h2 style="text-align: center">Обособленных структурных подразделений в техникуме нет</h2>
        <h3>Информация об органах управления образовательной организации</h3>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['control_college']])}}
    </div>
@endsection