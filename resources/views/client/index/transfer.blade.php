@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="transfer">
        @foreach($programs as $program)
            <div class="transfer-item">
                <h3>{{$program['name']}}</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Название группы</th>
                        <th>Курс</th>
                        <th>Фаза обучения</th>
                        <th>Максимальное кол-во обучающихся</th>
                        <th>Кол-во свободных мест</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($program['groups'] as $group)
                        <tr>
                            <td>{{$group['name']}}</td>
                            <td>{{$group['course']}}</td>
                            <td>{{$group['step']}}</td>
                            <td>{{$group['max_people']}}</td>
                            <td>{{$group['count_vacancy_transfer']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
@endsection
