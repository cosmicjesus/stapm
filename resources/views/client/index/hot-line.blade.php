@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>

    <p class="zagl_b">Администрация техникума</p>
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <tr>
                <td>Директор - Климов Валерий Федорович</td>
                <td>8(846) 955-22-20</td>
            </tr>
            <tr>
                <td>
                    Заместитель директора по учебно – производственной работе - Мальцев Николай Григорьевич<br>
                    Секретарь учебной части – Кирсанова Татьяна Николаевна
                </td>
                <td>8(846) 955-08-14</td>
            </tr>
            <tr>
                <td>Зам. директора. по учебной работе - Кривчун Наталья Васильевна<br>
                    Зам. директора. по методической работе - Губарь Анна Сергеевна</td>
                <td>8(846) 955-06-09</td>
            </tr>

            <tr>
                <td>Методист – Ляпнев Александр Викторович<br>
                    Мастер П/О- Калашников Владимир Николаевич
                </td>
                <td>8(846) 955-22-14</td>
            </tr>

            <tr>
                <td>
                    Зам. директора по социально- педагогической работе -
                    Черникова Ирина Михайловна<br>
                    Социальный педагог – Ларина Светлана Сергеевна<br>
                    Педагог дополнительного образования – Самохина Вера Борисовна
                </td>
                <td>8(846) 955-22-11</td>
            </tr>

            <tr>
                <td>Главный бухгалтер - Кузуб Анастасия Михайловна</td>
                <td>8(846) 955-13-93</td>
            </tr>

            <tr>
                <td>Комендант общежития - Чукаева Канзиля Кенжегалиевна</td>
                <td>8(846) 955-06-11</td>
            </tr>
        </table>
    </div>

    <h4>Расписание</h4>
    Вы можете узнать расписание по телефону 8(846) 955-20-71(учебная часть).
    <table class="table table-bordered table-striped">
        <tr>
            <td>Заведующий учебной частью – Мальцева Елена Александровна</td>
        </tr>
        <tr>
            <td>Заведующий очным отделением – Шошева Эльнара Экмановна</td>
        </tr>

        <tr>
            <td>Заведующий заочным/вечерним отделением – Бедченко Юлия Анатольевна</td>
        </tr>
    </table>
    <div class="phones">
        <h4>Телефоны горячей линии</h4>
        <ul>
            <li><img src="/img/viber.jpg" alt="" width="50px"> +79084188800/+79270107427 - по вопросам психологической поддержки</li>
            <li><img src="/img/viber.jpg" alt="" width="50px"> +79279006072 -
                По вопросам связанным с учебной деятельностью
            </li>
            <li>
                <img src="/img/viber.jpg" alt="" width="50px"> +79171681631 - По вопросам связанным с практикой студентов
            </li>
            <li>
                <img src="/img/viber.jpg" alt="" width="50px">/<img src="/img/whatsup.jpg" alt="" width="50px">  +79063472996 - По общим вопросам
            </li>
        </ul>
    </div>


@endsection
