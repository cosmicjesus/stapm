@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="profession-programs">
        <div class="table-scroll">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Профессия/специальность</th>
                    <th>Форма</th>
                    <th>Срок действия аккредитации</th>
                    <th>Срок обучения</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($programs as $program)
                    <tr>
                        <td><a class="link"
                               href="{{route('client.program_detail',$program['slug'])}}">{{$program['profession']['name']}}</a>
                            @if($program['top_fifty'])
                                <img src="/img/top50blue.png" alt="Логотип ТОП50" style="height: 40px">
                            @endif
                        </td>
                        <td>{{educationForms($program['educationForm'])}}</td>
                        <td style="width: 100px">{{dateFormat($program['licence'])}}</td>
                        <td>{{convertTrainingPeriod($program['duration'])}}</td>
                        <td><a href="{{route('client.umk',$program['slug'])}}" class="btn btn--blue">УМК</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <ul>
        <li>
            <a class="link" href="/files/info_o_priem.pdf?{{rand(1,200)}}" target="_blank">Информация о результатах
                приема на
                01.10.2018</a>
        </li>
        <li>
            <a class="link" href="/files/info_o_perevod.pdf?{{rand(1,200)}}" target="_blank">Информация о результатах
                перевода,
                восстановления и
                отчисления на 01.10.2018</a>
        </li>
    </ul>
@endsection