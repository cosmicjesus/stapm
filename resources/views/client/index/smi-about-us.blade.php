@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="media-about-us administration">
        @foreach($medias as $media)
            <div class="employee__card">
                <div class="employee__photo">
                    <img src="/img/channel_logos/{{$media->source}}.png" alt="{{$media->title}}" class="img-responsive">
                </div>
                <div class="employee__information">
                    @if($media->is_youtube)
                        <a data-fancybox
                           class="link"
                           href='https://www.youtube.com/watch?v={{$media->url}}&amp;autoplay=1&amp;showinfo=0'>
                            {{$media->title}}
                        </a>
                    @else
                        <a href="{{$media->url}}" class="link" target="_blank">{{$media->title}}</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection
