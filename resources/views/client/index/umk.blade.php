@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">УМК {{$program['name']}}</h1>
    <div class="profession-program-umk">
        <div class="umk__working-programs">
            <h4>Рабочие программы</h4>
            @if(count($umk))
                <el-tabs type="card">
                    @foreach($umk as $key =>$subjects)
                        <el-tab-pane label="{{$key}} год">
                            @foreach($subjects as $subject)
                                <details>
                                    <summary>{{$subject['index']}} {{$subject['name']}}</summary>
                                    @if(count($subject['files']))
                                        <div class="">
                                            <ul>
                                                @foreach($subject['files'] as $file)
                                                    <li>
                                                        @if(is_null($file['url']))
                                                            {{$file['name']}}
                                                        @else
                                                            <a href="{{$file['url']}}" class="link"
                                                               target="_blank">{{$file['name']}}</a>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </details>
                            @endforeach
                        </el-tab-pane>
                    @endforeach
                </el-tabs>
            @else
                <h3 class="text-center">Для выбранной программы пока не сформирован УМК</h3>
            @endif
        </div>
    </div>
@endsection