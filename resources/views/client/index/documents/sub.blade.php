@if($category->hasActiveFiles())
    <div class="table-scroll">
        <table class="table table-striped table-bordered">
            @foreach($category->getActiveFiles() as $file)
                @if($loop->first)
                    <tr>
                        <td rowspan="{{$category->getActiveFiles()->count()}}" class="w-20">{{$category->getName()}}</td>
                        <td>
                            <a href="{{$file->getPath()}}" class="link" target="_blank">
                                {{$loop->index+1}}) {{$file->getName()}}</a>
                        </td>
                    </tr>
                @else
                    <tr>
                        <td>
                            <a href="{{$file->getPath()}}" class="link" target="_blank">{{$loop->index+1}}
                                ) {{$file->getName()}}</a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
@endif