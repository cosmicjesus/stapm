@foreach($categories as $category)
    <h2 class="documents__title">{{ $category->getName() }}</h2>
    @if($category->hasActiveFiles())
        <ul>
            @foreach($category->getActiveFiles() as $file)
                <li><a href="{{$file->getPath()}}" class="link" target="_blank">{{$file->getName()}}</a></li>
            @endforeach
        </ul>
    @endif
    @each('client.index.documents.sub',$category->getChilds(),'category')
@endforeach