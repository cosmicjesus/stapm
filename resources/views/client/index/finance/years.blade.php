@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="finance">
        @foreach($categories as $categoriesSections)
            <div class="content_row">
                    @foreach($categoriesSections as $categoriesSection)
                        <div class="item">
                            <a href="{{route('client.financial-and-economic-activity.detail',$categoriesSection['slug'])}}"
                               class="link">
                                <div class="head">
                                    <h3> {{$categoriesSection['name']}}</h3>
                                </div>
                                <div class="body">
                                    <i class="fa fa-calendar icon"></i>
                                </div>
                            </a>
                        </div>
                    @endforeach
            </div>
        @endforeach
    </div>
@endsection
