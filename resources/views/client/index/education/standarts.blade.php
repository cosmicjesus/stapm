@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="education-standarts">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Наименование программы</th>
                <th>Ссылка на стандарт</th>
            </tr>
            @foreach($programs as $program)
                <tr>
                    <td><a class="link" href="{{route('client.program_detail', $program['slug'])}}"
                           title="{{$program['name']}}">{{$program['name']}}</a></td>
                    <td>
                        @if($program['has_standart'])
                            <a class="link" href="{{$program['standart_link']}}" target="_blank"
                               title="Открыть стандарт {{$program['name']}}">Открыть</a>
                        @else
                            <span>Не загружено</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection