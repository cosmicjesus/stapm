@extends('client.layout.layout_with_sitebar')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="profession-program">
        <div class="profession-program__description">
            <p>{{$program->getDescription()}}</p>
        </div>
        <div class="profession-program__detail table-scroll">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>Форма обучения</td>
                    <td>{{educationForms($program->getEducationFormId())}}</td>
                </tr>
                <tr>
                    <td>Язык, на котором ведется обучение</td>
                    <td>Русский</td>
                </tr>
                <tr>
                    <td>Квалификация</td>
                    <td>{{$program->getQualification()}}</td>
                </tr>
                <tr>
                    <td>Срок действия аккредитации</td>
                    <td>{{dateFormat($program->getLicence())}}</td>
                </tr>
                <tr>
                    <td>Срок обучения</td>
                    <td>{{convertTrainingPeriod($program->getDuration())}}</td>
                </tr>
                <tr>
                    <td>Уровень образования</td>
                    <td>Среднее профессиональное образование</td>
                </tr>
                <tr>
                    <td>Образовательный стандарт</td>
                    <td>
                        @if($program->hasStandart())
                            <a href="{{$program->getStandartUrl()}}" target="_blank">Открыть</a>
                        @else
                            <span>Не загружено</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Аннотации рабочих программ</td>
                    <td>
                        @if($program->hasAnnotation())
                            <a href="{{$program->getAnnotationUrl()}}" class="link" target="_blank">Открыть</a>
                        @else
                            <span>Не загружено</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>УМК</td>
                    <td><a href="{{route('client.umk',['slug'=>$program->getSlug()])}}">Открыть</a></td>
                </tr>
                <tr>
                    <td>Количество студентов</td>
                    <td>{{$program->getCountStudents()}}</td>
                </tr>
                <tr>
                    <td>Количество студентов по группам</td>
                    <td>
                        <div class="table-scroll">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Группа</th>
                                    <th>Курс</th>
                                    <th>Кол-во студентов</th>
                                </tr>
                                @foreach($program->getTrainingGroups() as $group)
                                    <tr>
                                        <td>{{$group->getName()}}</td>
                                        <td>{{$group->getCourse()}}</td>
                                        <td>{{$group->getCountStudents()}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Учебные планы</td>
                    <td>
                        <ul>
                            @foreach($program->getEducationPlans() as $plan)
                                <li>
                                    @if($plan->hasFile())
                                        <a href="{{$plan->getFile()}}" class="link"
                                           target="_blank"
                                           title="{{$plan->getNameFromStartDate()}}">{{$plan->getNameFromStartDate()}}
                                            @foreach($plan->getExtraOptions() as $key => $option)
                                                @if($option)
                                                    {{educationPlanExtraOption($key)}}
                                                @endif
                                            @endforeach
                                        </a>
                                    @else
                                        <span>{{$plan->getNameFromStartDate()}}</span>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @foreach($program->getAdditionalDocuments() as $key=>$section)
                    <tr>
                        <td>{{additionalDocumentsType($key)}}</td>
                        <td>
                            <ul>
                                @foreach($section as $file)
                                    <li><a href="{{$file['url']}}" target="_blank"
                                           title="{{$file['name']}}">{{$file['name']}}</a></li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach

            </table>
            <p>[ДО] - Дуальное обучение</p>
            <p>[АОП] - Адаптированная образовательная программа</p>
        </div>
    </div>
@endsection