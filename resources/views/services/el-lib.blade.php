@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="el-lib">
        <div class="library-items">
            @foreach($libs as $lib)
                <div class="library-item">
                    @if(!is_null($lib['img_link']))
                        <img src="{{$lib['img_link']}}" alt="{{$lib['name']}}">
                    @endif
                    <h4><a href="{{$lib['link']}}" target='_blank'>{{$lib['name']}}</a></h4>
                    <p>{{$lib['description']}}</p>
                </div>
            @endforeach
        </div>
    </div>
@endsection