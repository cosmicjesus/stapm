@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <div class="news-detail">
        <div class="news-detail-top">
            <div class="news-detail__date">{{dateFormat($item->getPublicationDate())}}</div>
            <div class="news-detail-return-to-news"><a class="news-detail-return-to-news__link"
                                                       href="{{route('news')}}">К списку новостей</a></div>
        </div>
        <div class="news-detail-title">
            <h1 class="dark-text">{{$item->getTitle()}}</h1>
        </div>
        <div class="news-detail-content" bp="12">
            <div bp="grid">
                <div class="news-detail-img" bp="12@sm 4@md">
                    <img class="news-detail-image" src="{{$item->getImage()}}">
                </div>
                <div bp="12@sm 8@md">
                    {!! $item->getFullText() !!}
                    @if(count($item->getFiles()))
                        <h3>Дополнительные файлы</h3>
                        <div class="news-files">
                            <ul>
                                @foreach($item->getFiles() as $file)
                                    <li><a href="{{$file['path']}}" target="_blank">{{$file['name']}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                @if($item->hasImages())
                    <div bp="12">
                        <h3>Дополнительные изображения</h3>
                        @foreach($item->getImages() as $image)
                            <div bp="4" style="margin-bottom: 10px;width: 46%;">
                                <img bp="4" src="{{$image['path']}}" alt="{{$item->getTitle()}}" class="img">
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        {{-- <div class="news-navigation" bp="12">
             <div class="news-next">
                 @if($item->getNext())
                     <a href="{{route('news.one',$item->getNext()->getSlug())}}">
                         <i class="fa fa-caret-left"></i> {{$item->getNext()->getPublicationDate()}} {{$item->getNext()->getTitle()}}
                     </a>
                 @endif
             </div>
             <div class="news-previous">
                 @if($item->getPrevious())
                     <a href="{{route('news.one',$item->getPrevious()->getSlug())}}">{{$item->getPrevious()->getPublicationDate()}} {{$item->getPrevious()->getTitle()}}
                         <i class="fa fa-caret-right"></i>
                     </a>
                 @endif
             </div>
         </div>--}}
    </div>
@endsection