@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="news-list">
        @if(count($news))
            @foreach($news as $item)
                <div class="news-item">
                    <div class="news-item__title">
                        <a class="news-item__title-link" href="{{$item->getDetailUrl($delail_route)}}">{{$item->getTitle()}}</a></div>
                    <div class="news-item__date">{{$item->getPublicationDate()}}</div>
                    <div class="news-item-content">
                        <div class="news-item-content__image">
                            <img class="news-item-content__image-img" src="{{$item->getImage()}}">
                        </div>
                        <div class="news-item-content__preview">
                            {!! $item->getPreview() !!}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    {{$news->links('partials.paginate')}}

@endsection