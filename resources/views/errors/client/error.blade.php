
    <div class="error-page">
        <h2 class="headline text-red">{{$error_code}}</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> {{$error_message}}</h3>

            <p>
               Советуем вам вернуться на <a href="{{route('admin.index')}}">главную страницу</a>.
            </p>
        </div>
    </div>
