@extends('layout.layout_index')
@section('title','Ошибка 404 Страница не найдена')

@section('content')
    <div class="error-wrapper">
        <div class="content box box-white">
            <div class="error-block">
                <div class="error-content">
                    <h2 class="headline text-red">Ошибка {{$error_code}}</h2>
                    <h3><i class="fa fa-warning text-red"></i> {{$error_message}}</h3>

                    <p>
                        Советуем вам вернуться на <a href="{{route('home')}}">главную страницу</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection