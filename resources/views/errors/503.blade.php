<!DOCTYPE html>
<html lang="ru">
@include("client.layout.inc.head")
<body style="background-color: white">
@yield('Ведутся технические работы')

<div class="content" style="height: 100vh; display: flex;flex-direction: row">
    <div style="align-self: center;align-items: center;text-align: center;width: 100%;" >
        <h1>Ведутся технические работы</h1>
        <h3>Время завершения работ {{env('TIME_END_PROFILACTIC','00:00')}}</h3>
        <div style="align-self: center">
            <div>
                <img src="/img/minion.png" alt="Ведутся технические работы">
            </div>
        </div>
    </div>
</div>
</body>
</html>