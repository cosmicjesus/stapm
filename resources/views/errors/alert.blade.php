<div class="alert alert-{{$type}}" style="text-align: center">
    <h4>{!! $message!!}</h4>
    @if(isset($trainingReference)&&$trainingReference==true)
        <br>
        <h4>{{str_replace("{day}",$day,"Получить справку об обучении вы можете {day}, в кабинете 8А c 11:30 до 16:00")}}</h4>
    @endif
    @if(isset($moneyMessage)&&$moneyMessage==true)
        <br>
        <h4>Справку о стипендии вы можете получить в ближайший вторник</h4>
    @endif
</div>