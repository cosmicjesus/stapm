@extends('client.layout.layout_index')
@section('title','Ошибка 404 Страница не найдена')

@section('content')
    <div class="error__wrapper">
        <div class="content container container-white">
            <div class="error__content">
                <h2 class="headline text-red">Ошибка {{$exception->getStatusCode()}}</h2>
                <h3><i class="fa fa-warning text-red"></i> {{$exception->getMessage()}}</h3>

                <p>
                    Советуем вам вернуться на <a class="link" href="{{route('client.index')}}">главную страницу</a>.
                </p>
            </div>
        </div>
    </div>
@endsection