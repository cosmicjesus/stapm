@extends('layout.layout_index')
@section('title','СТАПМ')

@section('content')
    <div class="body-wrapper">
        <div class="college_info">
            <div class="college_info_wrapper content">
                <div class="college_info_text">
                    <h1>Самарский техникум авиационного и промышленного машиностроения</h1>
                    <div class="college_info_text_image"><img class="college_info_text_image_img" src="/img/stapm.jpg">
                    </div>
                    <p class="college_info_text_description">В соответствии с приказом Министерства авиационной
                        промышленности от 20 августа 1954 г. За №567на базе завода № п/я 208 в сентябре 1954 г. Было
                        организовано техническое училище № 5 с непосредственным подчинением Куйбышевскому областному
                        управлению трудовых резервов. Первого октября 1954 г. был произведен первый звонок для учащихся
                        численностью 250 человек.</p><a class="btn btn--blue btn--long" href="{{route('history')}}">Узнать
                        больше</a>
                </div>
                <div class="college_info_image"><img class="college_info_image_img" src="/img/stapm.jpg"></div>
            </div>
        </div>
        <div class="announcements">
            <div class="news-index_header content">
                <div class="news-index_header_title">
                    <h2>Анонсы / События</h2>
                </div>
                <div class="news-index_header_all_news">
                    <a class="news-index_header_all_news_link" href="{{route('announcements.index')}}">
                        Все события<i class="fa fa-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="announcements-wrapper content">
                @if($annonces->count())
                    <div bp="grid">
                        @foreach($annonces as $annonce)
                            @if($loop->first)
                                <div bp='12' class="announcements-item box box-white">
                                    <div class="announcement">
                                        <div class="announcement-image">
                                            <img src="{{$annonce->getImage()}}" alt="{{$annonce->getTitle()}}"
                                                 class="img img-center">
                                        </div>
                                        <div class="announcement-title">
                                            <h3 class="announcement-title-link">
                                                <a href="{{$annonce->getDetailUrl('announcements.show')}}">{{$annonce->getTitle()}}</a>
                                            </h3>
                                        </div>
                                        <div class="announcement-preview">
                                            {!!  $annonce->getPreview()!!}
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div bp='12 4@lg 4@md' class="announcements-item box box-white">
                                    <div class="announcement-title">
                                        <h3 class="announcement-title-link">
                                            <a href="{{$annonce->getDetailUrl('announcements.show')}}">{{$annonce->getTitle()}}</a>
                                        </h3>
                                    </div>
                                    <div class="announcement-preview">
                                        {!!  $annonce->getPreview()!!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @else
                    <h4 class="text-center">Событий нет</h4>
                @endif
            </div>
        </div>
    </div>
    <div class="news-index">
        <div class="news-index_header content">
            <div class="news-index_header_title">
                <h2>Новости</h2>
            </div>
            <div class="news-index_header_all_news"><a class="news-index_header_all_news_link"
                                                       href="{{route('news')}}">Все новости<i
                            class="fa fa-caret-right"></i></a></div>
        </div>
        {{Widget::run('App\Widgets\Client\NewsIndex')}}
    </div>
    </div>
@endsection