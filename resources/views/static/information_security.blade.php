@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="information_security">
        <p> В ГБПОУ
            «Самарский техникум авиационного и промышленного машиностроения
            имени Д.И.Козлова» имеется  доступ в интернет, предоставленный компанией
            "Ростелеком". В сеть интернет организован безлимитный доступ, что позволяет предоставить
            его всем сотрудникам, педагогам и студентам техникума.</p>
        <div class="video" style="text-align: center">
            <iframe width="853" height="480" style="margin: 0 auto;max-width: 100%"
                    src="https://www.youtube.com/embed/K2fQZJGkbjk?rel=0&amp;showinfo=0"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['information_security']])}}
    </div>
@endsection