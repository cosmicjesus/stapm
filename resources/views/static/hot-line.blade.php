@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>

    <p class="zagl_b">Администрация техникума</p>
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <tr>
                <td>Директор - Климов Валерий Федорович</td>
                <td>8(846) 955-22-20</td>
            </tr>
            <tr>
                <td>
                    Заместитель директора по учебно – производственной работе - Мальцев Николай Григорьевич<br>
                    Секретарь учебной части – Кирсанова Татьяна Николаевна
                </td>
                <td>8(846) 955-08-14</td>
            </tr>
            <tr>
                <td>Зам. директора. по учебной работе - Кривчун Наталья Васильевна</td>
                <td>8(846) 955-06-09</td>
            </tr>

            <tr>
                <td>Методист – Ляпнев Александр Викторович<br>
                    Мастер П/О- Калашников Владимир Николаевич
                </td>
                <td>8(846) 955-22-14</td>
            </tr>

            <tr>
                <td>
                    Зам. директора по социально- педагогической работе -
                    Черникова Ирина Михайловна<br>
                    Социальный педагог – Ларина Светлана Сергеевна<br>
                    Педагог дополнительного образования – Самохина Вера Борисовна
                </td>
                <td>8(846) 955-22-11</td>
            </tr>

            <tr>
                <td>Главный бухгалтер - Кузуб Анастасия Михайловна</td>
                <td>8(846) 955-13-93</td>
            </tr>

            <tr>
                <td>Комендант общежития - Чукаева Канзиля Кенжегалиевна</td>
                <td>8(846) 228-55-86</td>
            </tr>
        </table>
    </div>

    <h4>Расписание</h4>
    Вы можете узнать расписание по телефону 8(846) 955-20-71(учебная часть).
    <table class="table table-bordered table-striped">
        <tr>
            <td>Заведующий учебной частью – Мальцева Елена Александровна</td>
        </tr>
        {{--<tr>
            <td>Диспетчер по расписанию - Советкина Наталия Сергеевна</td>
        </tr>--}}
        <tr>
            <td>Заведующий очным отделением – Ищенко Татьяна Алексеевна</td>
        </tr>

        <tr>
            <td>Заведующий заочным/вечерним отделением – Бедченко Юлия Анатольевна</td>
        </tr>
    </table>


@endsection