@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="fhd">
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['fhd']])}}
    </div>

@endsection