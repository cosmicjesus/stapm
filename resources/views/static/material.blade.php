@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="material">
        <table class="table table-bordered">
            <tr>
                <td>№ п/п</td>
                <td>Адрес объекта (места осуществления образовательной деятельности)</td>
                <td>Назначение объекта</td>
                <td>Общая площадь объекта, м2</td>
                <td>Копии документов, подтверждающих право собственности или иное законное основание пользования
                    объектом, используемого для осуществления образовательного процесса
                </td>
            </tr>
            <tr>
                <td rowspan='11'>1</td>
                <td rowspan='11'>443052, г. Самара, Старый переулок, 6(Учебный корпус №1)</td>
                <td>Учебные кабинеты -17</td>
                <td>1097,7</td>
                <td rowspan='11'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_first_housing']])}}
                </td>
            </tr>
            <tr>
                <td>Методический кабинет - 1</td>
                <td> 75,8</td>
            </tr>
            <tr>
                <td>Спортивный зал - 1</td>
                <td>271,4</td>
            </tr>
            <tr>
                <td>Актовый зал - 1</td>
                <td>210,0</td>
            </tr>
            <tr>
                <td>Библиотека - 1</td>
                <td> 83,8</td>
            </tr>
            <tr>
                <td>Читальный зал - 1</td>
                <td>118,8</td>
            </tr>
            <tr>
                <td>Медицинский пункт -1</td>
                <td>28,5</td>
            </tr>
            <tr>
                <td>Музей истории - 1</td>
                <td> 52,2</td>
            </tr>
            <tr>
                <td>Столовая -1шт</td>
                <td>240,6</td>
            </tr>
            <tr>
                <td>Административные помещения - 8</td>
                <td>191,0</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td>2086,40</td>
            </tr>
            <tr>
                <td rowspan='5'>2</td>
                <td rowspan='5'>443052, г. Самара, Стационарный переулок, 11(Учебный корпус №2)</td>
                <td>Учебные кабинеты -10</td>
                <td>708,4</td>
                <td rowspan='5'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_second_housing']])}}
                </td>
            </tr>
            <tr>
                <td>Лабораторные - 6</td>
                <td> 401,5</td>
            </tr>
            <tr>
                <td>Административные помещения - 5</td>
                <td> 108,1</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td> 845,8</td>
            </tr>
            <tr>
                <td>Подвальное помещение</td>
                <td> 503,3</td>
            </tr>
            <tr>
                <td rowspan='4'>3</td>
                <td rowspan='4'>443052, г. Самара, Старый переулок, 6(Учебный корпус №1)</td>
                <td>Мастерские - 5</td>
                <td>2087,6</td>
                <td rowspan='4'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_first_housing_labs']])}}
                </td>
            </tr>
            <tr>
                <td>Лабораторные - 3</td>
                <td> 289,2</td>
            </tr>
            <tr>
                <td>Административные помещения - 4</td>
                <td> 101,6</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td> 941,3</td>
            </tr>
            <tr>
                <td rowspan='3'>4</td>
                <td rowspan='3'>443052, г. Самара, Стационарный переулок, 11(Учебный корпус №2)</td>
                <td>Мастерские - 4</td>
                <td>1028,1</td>
                <td rowspan='3'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_second_housing_labs']])}}
                </td>
            </tr>
            <tr>
                <td>Административные помещения - 3</td>
                <td> 52,9</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td> 1465,1</td>
            </tr>
            <tr>
                <td rowspan='5'>5</td>
                <td rowspan='5'>443052, г. Самара, Стационарный переулок, 11(Универсальный общественно-бытовой корпус)
                </td>
                <td>Спортивный зал - 1</td>
                <td>265,8</td>
                <td rowspan='5'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_universal_public_housing']])}}
                </td>
            </tr>
            <tr>
                <td>Библиотека - 1</td>
                <td> 150,4</td>
            </tr>
            <tr>
                <td>Актовый зал - 1</td>
                <td> 188,8</td>
            </tr>
            <tr>
                <td>Столовая - 1</td>
                <td> 199,1</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td> 1364,5</td>
            </tr>
            <tr>
                <td rowspan='4'>6</td>
                <td rowspan='4'>443052, г. Самара, Старый переулок, 6</td>
                <td>Жилые помещения</td>
                <td>1521,1</td>
                <td rowspan='4'>
                    {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['doc_first_housing']])}}
                </td>
            </tr>
            <tr>
                <td> Административные помещения</td>
                <td> 122,6</td>
            </tr>
            <tr>
                <td>Бытовые и вспомогательные помещения</td>
                <td> 3106,6</td>
            </tr>
            <tr>
                <td>Подвальное помещение</td>
                <td> 497,4</td>
            </tr>

        </table>
        <ul>
            <li><a href="{{route('rooms')}}">Cведения о наличии оборудованных учебных кабинетов</a></li>
            <li><a href="{{route('rooms')}}">Объектов для проведения практических занятий </a></li>
            <li><a href="{{route('services.library')}}">Cведения о наличии Библиотек</a></li>
            <li><a href="{{route('rooms')}}#sports">Cведения о наличии спортивных объектов</a></li>
            <li><a href="{{route('services.clinic')}}">Сведения об охране здоровья обучающихся, в том числе инвалидов и лиц с ОВЗ</a></li>
            <li><a href="{{route('services.canteen')}}">Сведения об условиях питания сотрудников и обучающихся, в том числе инвалидов и лиц с ОВЗ</a></li>
            <li><a href="{{route('services.information_security')}}">Сведения об информационной безопасности в сети Интернет</a></li>
            <li><a href="http://spo.asurso.ru">Сведения о доступе к информационным системам и
                    информационно-телекоммуникационным сетям, в том числе для инвалидов и лиц с ОВЗ</a></li>
            <li><a href="http://window.edu.ru/">Сведения о об электронных образовательных ресурсах, к которым
                    обеспечивается доступ обучающихся(Федеральный уровень), в том числе для инвалидов и лиц с ОВЗ</a></li>
            <li><a href="http://samara.edu.ru/">Сведения о об электронных образовательных ресурсах, к которым
                    обеспечивается доступ обучающихся(Региональный уровень), в том числе для инвалидов и лиц с ОВЗ</a></li>
            {{--<li><a href="http://stapm.ru/training.php">Средства образования</a></li>--}}
            <li><a href="{{route('vospitanie')}}">Средства воспитания, в том числе для инвалидов и лиц с ОВЗ</a></li>
            <li><a href="{{route('ovz')}}">Условия обучения инвалидов и лиц с ограниченными возможностями здоровья.</a>
            </li>
        </ul>
    </div>
@endsection