<html lang="ru">
<head>
    <title>Отчет по абитуриентам</title>
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <style>
        .table {
            color: black;
            text-align: center;
            font-size: 12px;
        }

        td, th {
            padding: 5px;
            font-size: 14px;
        }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
    </style>
</head>
<body>
<div style="padding: 10px;">
    <h3 style="text-align: center">Отчет по абитуриентам</h3>
    <div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>№П/П</th>
                <th>ФИО</th>
                @if($columns['gender'])
                    <th>Пол</th>@endif
                @if($columns['birthday'])
                    <th>Дата рождения</th>@endif
                @if($columns['program'])
                    <th>Программа</th>@endif
                @if($columns['document_give_date'])
                    <th>Дата подачи документов</th>@endif
                @if($columns['has_original'])
                    <th>Оригинал документа</th>@endif
                @if($columns['contract_target_set'])
                    <th>Целевой набор</th>@endif
                @if($columns['phone'])
                    <th>Телефон</th>@endif
                @if($columns['need_hostel'])
                    <th>Потребность в общежитии</th>@endif
                @if($columns['registration_address'])
                    <th>Адрес регистрации</th>@endif
                @if($columns['preferential_categories'])
                    <th>Льготные категории</th>@endif
                @if($columns['files'])
                    <th>Файлы</th>
                @endif
                <th>Средний балл</th>
            </tr>
            </thead>
            <tbody>
            @foreach($entrants as $key => $entrant)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$entrant['full_name']}}</td>
                    @if($columns['gender'])
                        <td>{{gender($entrant['gender'])}}</td>@endif
                    @if($columns['birthday'])
                        <td>{{dateFormat($entrant['birthday'])}}</td>@endif
                    @if($columns['program'])
                        <td>
                            <ul>
                                @foreach($entrant['recruitment_programs'] as $program)
                                    <li>@if($program['is_priority'])
                                            <span style="font-weight: bold">
                                            +
                                        </span>@endif{{$program['program_name']}} ({{convertTrainingPeriod($program['duration'])}})</li>
                                @endforeach
                            </ul>
                        </td>@endif
                    @if($columns['document_give_date'])
                        <td>{{dateFormat($entrant['start_date'])}}</td>@endif
                    @if($columns['has_original'])
                        <td>{{$entrant['has_original']?'Да':'Нет'}}</td>@endif
                    @if($columns['contract_target_set'])
                        <td>{{$entrant['contract_target_set']?'Да':'Нет'}}</td>@endif
                    @if($columns['phone'])
                        <td>{{$entrant['phone']}}</td>@endif
                    @if($columns['need_hostel'])
                        <td>{{$entrant['need_hostel']?'Да':'Нет'}}</td>@endif
                    @if($columns['registration_address'])
                        <td>{{$entrant['addresses']['registration']}}</td>@endif
                    @if($columns['preferential_categories'])
                        <td>
                            @if(count($entrant['health']['preferential_categories']))
                                <ul>
                                    @foreach($entrant['health']['preferential_categories'] as $key =>$category)
                                        <li>{{getPrivilegeCategory($category)}}</li>
                                    @endforeach
                                </ul>
                            @else
                                <span>
                                Льгот нет
                            </span>
                            @endif
                        </td>@endif
                    @if($columns['files'])
                        <td>
                            @if(count($entrant['files']))
                                <ul>
                                @foreach($entrant['files'] as $file)
                                    <li>{{$file['name']}}</li>
                                @endforeach
                                </ul>
                            @else
                                <span>Нет загруженных файлов</span>
                            @endif
                        </td>
                    @endif
                    <td>{{$entrant['avg']}}</td>
                </tr>
            @endforeach
            @if($columns['totalRow'])
                <tr>
                    <td colspan="{{$totalRowSize}}"></td>
                    <td>{{$totalAverage}}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
