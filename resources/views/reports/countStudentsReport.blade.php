<html>
<head>
    <title>Количество контингента на {{$date->format('d.m.Y')}}</title>
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <style>
        .table {
            color: black;
            text-align: center;
            font-size: 12px;
        }

        td, th {
            padding: 5px;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div style="padding: 10px;">
    <h3 style="text-align: center">Контингент на {{$date->format('d.m.Y')}}</h3>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Группа</th>
            <th>Общее кол-во студентов</th>
            <th>Обучающихся</th>
            <th>Кол-во длит.отсутствующих</th>
            <th>В РА</th>
            <th>В академическом отпуске</th>
        </tr>
        @foreach($data as $course =>$groups)
            <tr>
                <th colspan="6">{{$course}} курс</th>
            </tr>
            @foreach($groups as $group)
                <tr>
                    <td>{{$group['group_name']}}</td>
                    <td>{{$group['totalStudentsCount']}}</td>
                    <td>{{$group['students']}}</td>
                    <td>{{$group['longTimeAbsent']}}</td>
                    <td>{{$group['inArmy']}}</td>
                    <td>{{$group['academic']}}</td>
                </tr>
            @endforeach
            <tr>
            <tr>
                <td>Всего по курсу</td>
                <td>{{$groups->sum('totalStudentsCount')}}</td>
                <td>{{$groups->sum('students')}}</td>
                <td>{{$groups->sum('longTimeAbsent')}}</td>
                <td>{{$groups->sum('inArmy')}}</td>
                <td>{{$groups->sum('academic')}}</td>
            </tr>
        @endforeach
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <th colspan="6">Общая статистика</th>
        </tr>
        <tr>
            <th>Общее кол-во студентов</th>
            <th>Обучающихся</th>
            <th>Кол-во длит.отсутствующих</th>
            <th>В РА</th>
            <th>В академическом отпуске</th>
        </tr>
        <tr>
            <td>{{$totalSums['totalStudentsCount']}}</td>
            <td>{{$totalSums['students']}}</td>
            <td>{{$totalSums['longTimeAbsent']}}</td>
            <td>{{$totalSums['inArmy']}}</td>
            <td>{{$totalSums['academic']}}</td>
        </tr>
    </table>
</div>
</body>
</html>