<html lang="ru">
<head>
    <title>Сведения о ходе комплектования групп приема на {{$data['date']}}</title>
    <link rel="stylesheet" href="/pdf/styles.css?{{rand(1,9999)}}">
    {{--    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">--}}
    <style>
        * {
            color: black !important;
            font-family: "Times New Roman", Times, serif;
        }

        .table {
            color: black;
            text-align: center;
        }

        td, th {
            padding: 5px;
            font-size: 12px;
        }
    </style>
</head>
<body style="color: black !important;
            font-family: 'Times New Roman', Times, serif;">
<div style="padding: 10px;">
    <h3 style="text-align: center;font-size: 14px; font-weight: bold;color: black !important;">Cведения о ходе
        комплектования групп приема<br>
        по образовательным программам среднего профессионального образования<br>
        за счет бюджетных ассигнований бюджета Самарской области<br>
        на {{$data['academicYear']['start']}}/{{$data['academicYear']['end']}} учебный год<br>
        (по состоянию на {{$data['date']}})</h3>
    <div style="font-size: 14px">
        <p>Наименование образовательной организации: <u>государственное бюджетное профессиональное образовательное
                учреждение Самарской области
                «Самарский техникум авиационного и промышленного машиностроения
                имени Д.И.Козлова»</u></p>
    </div>
    <table class="table table-bordered text-center table-striped table-text-center">
        <thead>
        <tr>
            <th rowspan="2">
                № п/п
            </th>
            <th rowspan="2">
                Код профессии/<br>специальности
            </th>
            <th rowspan="2">
                Наименование профессии/специальности
            </th>
            <th rowspan="2">
                КЦП на {{$data['academicYear']['start']}} год
            </th>
            <th colspan="2">Подано заявлений</th>
            <th rowspan="2">Принято</th>
            <th colspan="3">
                в том числе по формам обучения
            </th>
        </tr>
        <tr>
            <th>С копиями документов</th>
            <th>С оригиналами документов</th>
            <th>Очная</th>
            <th>Очно-заочная</th>
            <th>Заочная</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data['data'] as $program)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$program['code']}}
                </td>
                <td>
                    {{$program['name']}}
                </td>
                <td>
                    {{$program['reception_plan']}}
                </td>
                <td>{{$program['count_with_out_documents']}}</td>
                <td>{{$program['count_with_documents']}}</td>
                <td>{{$program['number_of_enrolled']}}</td>
                @foreach($program['forms'] as $form)
                    <td>{{$form}}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td colspan="3">
                Итого
            </td>
            <td>{{$data['total']['reception_plan']}}</td>
            <td>{{$data['total']['count_with_out_documents']}}</td>
            <td>{{$data['total']['count_with_documents']}}</td>
            <td>{{$data['total']['number_of_enrolled']}}</td>
            @foreach($data['total']['forms'] as $form)
                <td>{{$form}}</td>
            @endforeach
        </tr>
        </tbody>
    </table>

</div>
</body>
</html>