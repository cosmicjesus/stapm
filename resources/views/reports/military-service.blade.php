<html lang="ru">
<head>
    <title>{{$title}}</title>
    <link rel="stylesheet" href="/pdf/styles.css?{{rand(1,9999)}}">
    {{--    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">--}}
    <style>
        * {
            color: black !important;
            font-family: "Times New Roman", Times, serif;
        }

        .table {
            color: black;
            text-align: center;
        }

        td, th {
            padding: 5px;
            font-size: 12px;
        }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
    </style>
</head>
<body style="color: black !important;
            font-family: 'Times New Roman', Times, serif;">
    <div style="padding: 10px;">
        <h3 style="text-align: center;font-size: 18px; font-weight: bold;color: black !important;">{{$title}}</h3>
    </div>
    <div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>№</th>
                <th>ФИО</th>
                <th>Дата рождения</th>
                <th>Возраст</th>
                <th>Статус ВУ</th>
                <th>Отдел ВК</th>
                <th>Категория годности</th>
                <th>Состав</th>
                <th>Звание</th>
                <th>Группа учета</th>
                <th>Документ ВУ</th>
                <th>Номер документа ВУ</th>
                <th>Спец. учет</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$employee->full_name}}</td>
                    <td>{{$employee->birthday->format('d.m.Y')}}</td>
                    <td>{{$employee->age}}</td>
                    <td>{{$employee->military_status?militaryStatus($employee->military_status):'Не указано'}}</td>
                    <td>{{$employee->recruitmentCenter->name??'Не указано'}}</td>
                    <td>{{$employee->fitness_for_military_service?shortFitForMilitaryServices($employee->fitness_for_military_service):'Не указано'}}</td>
                    <td>{{$employee->military_composition?getMilitaryComposition($employee->military_composition):'Не указано'}}</td>
                    <td>{{$employee->military_rank?getMilitaryRank($employee->military_rank):'Не указано'}}</td>
                    <td>{{$employee->group_of_accounting?groupOfAccounting($employee->group_of_accounting):'Не указано'}}</td>
                    <td>{{$employee->military_document_type?militaryDocumentType($employee->military_document_type):'Не указано'}}</td>
                    <td>{{$employee->military_document_number}}</td>
                    <td>{{$employee->is_on_special_accounting?'Да':'Нет'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>