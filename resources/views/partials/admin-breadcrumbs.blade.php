@if ($breadcrumbs)
    <ol class="breadcrumb custom-breadcrumbs">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url)
            <li>
                <a href="{{$breadcrumb->url}}">{{$breadcrumb->title}}</a>
            </li>
            @else
                <li class="active">{{$breadcrumb->title}}</li>
            @endif
        @endforeach
</ol>
@endif