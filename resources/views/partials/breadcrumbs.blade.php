@if ($breadcrumbs)
    <div class="breadcrumbs">
        <ul class="breadcrumbs-items">
                @foreach ($breadcrumbs as $breadcrumb)
                    @if ($breadcrumb->url)
                    <li class="breadcrumbs-items_item">
                        <a class="breadcrumbs-items_item-link" href="{{$breadcrumb->url}}">{{$breadcrumb->title}}</a></li>
                    <li class="breadcrumbs-items_item">/</li>
                    @else
                    <li class="breadcrumbs-items_item">{{$breadcrumb->title}}</li>
                    @endif
                @endforeach
        </ul>
    </div>
@endif