@if ($breadcrumbs)
    <div class="breadcrumbs clearfix">
        <ul class="breadcrumbs__items">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url)
                    @if($loop->last)
                        <li class="breadcrumbs__item">{{$breadcrumb->title}}</li>
                    @else
                        <li class="breadcrumbs__item">
                            <a class="breadcrumbs__link" href="{{$breadcrumb->url}}">{{$breadcrumb->title}}</a></li>
                    @endif
                    @if(!$loop->last)
                        <li class="breadcrumbs__item">/</li>
                    @endif
                @else
                    <li class="breadcrumbs__item">{{$breadcrumb->title}}</li>
                @endif
            @endforeach
        </ul>
    </div>
@endif