<div id="releaseGroupModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выпуск учебной группы</h4>
            </div>
            {{Form::open(['name'=>'releaseGroupForm','id'=>'releaseGroupForm'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Укажите дату и номер приказа о выпуске. Студенты, которые сейчас привязанны к группе будут
                            отчислены с причиной «В связи с окончанием Учреждения»</p>
                        <p>Так же с сайте будет скрыт учебный план группы</p>
                        {{Form::hidden('group_id',null,['id'=>'group_id'])}}
                        <div class="form-group">
                            <label for="default-picker">Дата приказа</label>
                            <div class="input-group date">
                                <input class="form-control pull-right" id="decree_date_picker" type="text"
                                       name="decree_date"
                                       value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('decree_number','Номер приказа',['class'=>'control-label'])}}
                            {{Form::text('decree_number',null,['class'=>'form-control','required'=>'required'])}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Выпустить</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>