<div id="add_doc_in_subject" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_doc_in_subject_form','method'=>'POST','files'=>true])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title add-doc-title"></h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('program_id','',['id'=>'program_id']) !!}
                <div class="form-group">
                    <label for="title" class="control-label">Наименование</label>
                    <select name="title" id="title" class="select-2-js">
                        @foreach(getDocTitles() as $docTitle)
                            <option value="{{$docTitle}}">{{$docTitle}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{Form::label('file','Файл',['class'=>'control-label'])}}
                    {{Form::file('file',['id'=>'subject_file'])}}
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>