<div id="edit-education-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Изменение информации образования</h4>
            </div>
            {{Form::open(['name'=>'edit-education-form','id'=>'edit-education-form','class'=>'asyncValidate'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('direction_of_preparation','Направление подготовки',['class'=>'control-label'])}}
                    {{Form::text('direction_of_preparation',null,['class'=>'form-control direction-field-js'])}}
                </div>
                <div class="form-group">
                    {{Form::label('qualification','Квалификация',['class'=>'control-label'])}}
                    {{Form::text('qualification',null,['class'=>'form-control qualification-field-js'])}}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>