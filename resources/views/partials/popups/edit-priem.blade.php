<div id="editPriem" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title edit_priem_modal_title"></h4>
            </div>
            <form action="" name="edit-priem-form" method="post" id="edit-priem-form">
                <div class="modal-body">
                    <div id="modal-notification">
                        <div class="alert alert-danger errors-block hide">
                            <ul class="errors"></ul>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('reception','План приема')}}
                        {{Form::number('reception',null,['class'=>'form-control','min'=>1,'step'=>'1','required'=>'required'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('count_with_documents','Кол-во заявлений c оригиналами документов(статичное)')}}
                        {{Form::number('count_with_documents',null,['class'=>'form-control','min'=>0,'step'=>'1','required'=>'required'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('count_with_out_documents','Кол-во заявлений c копиями документов(статичное)')}}
                        {{Form::number('count_with_out_documents',null,['class'=>'form-control','min'=>0,'step'=>'1','required'=>'required'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary add-avg-js">Обновить</button>
                </div>
            </form>
        </div>
    </div>
</div>