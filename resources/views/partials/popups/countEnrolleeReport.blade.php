<div id="countEnrolleeReport" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Генерация отчета о комплектовании групп</h4>
            </div>
            {{Form::open(['name'=>'countEnrolleeReportForm','id'=>'countEnrolleeReportForm','route'=>'admin.priem.make-report','target'=>'_blank'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Укажите дату отчета и нужно ли сохранить файл в категории Приемная комиссия, на странице
                            документов</p>
                        <div class="form-group">
                            <label for="default-picker">Дата отчета</label>
                            <div class="input-group date">
                                <input class="form-control pull-right" id="date_count_enrollees_report" type="text"
                                       name="report_date"
                                       value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="checkbox">
                            <label for="put_in_documents">
                                {{Form::checkbox('put_in_documents',old('put_in_documents'),true,['id'=>'put_in_documents'])}}
                                Сохранить на странице документов
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Сгенерировать</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>