<div id="add_prog_other_document" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_prog_other_document_form','method'=>'POST','files'=>true])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавление документа</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('program_url','',['id'=>'program_url']) !!}
                <div class="form-group">
                    {{Form::label('type','Категория документа')}}
                    {{Form::select('type',['calendar'=>'Календарный график','akt'=>'Акт согласования','ppssz'=>'ППССЗ/ППКРС'],null,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    <label>Дата начала действия</label>
                    <div class="input-group">
                        <input class="form-control" id="default-picker" type="text" name="date" value="{{\Carbon\Carbon::createFromDate(null,9,1)->format('d.m.Y')}}">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    {{Form::label('file','Файл',['class'=>'control-label'])}}
                    {{Form::file('file',['id'=>'document_file'])}}
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary layoff-send-js">Добавить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>