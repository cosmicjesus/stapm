<div id="moneyReferenceModal" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выгрузка списка справок о стипендии</h4>
            </div>
            {{Form::open(['name'=>'moneyReferenceForm','id'=>'moneyReferenceForm','url'=>route('admin.references.export.money'),'method'=>'get','target'=>'_blank'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Укажите, за какой период нужно выгрузить справки.</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="" class="control-label">Период</label>
                            <div class="input-group input-daterange">
                                <div class="input-group-addon">C</div>
                                <input name="start_period" type="text" class="form-control" id="start_period" required>
                                <div class="input-group-addon">по</div>
                                <input name="end_period" type="text" class="form-control" id="end_period" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Сгенерировать</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>