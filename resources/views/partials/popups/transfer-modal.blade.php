<div id="transfer-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Перевод студентов</h4>
            </div>
            {{Form::open(['name'=>'transfer-form','id'=>'transfer-form','class'=>'asyncValidate'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Проверьте списочный состав студентов, выберите группу и укажите номер и дату приказа о
                            переводе</h4>
                    </div>
                    <div class="col-xs-8">
                        <div class="scroller">
                            <table class="table table-striped students-list-js">

                            </table>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="enrollment-form">
                            <div class="student-ids">

                            </div>
                            <div class="form-group">
                                {{Form::label('group','Группа',['class'=>'control-label'])}}
                                {{Form::select('group',getTrainigGroups(),null,['class'=>'form-control required'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('decree_number','Номер приказа',['class'=>'control-label'])}}
                                {{Form::text('decree_number',null,['class'=>'form-control required'])}}
                            </div>
                            <div class="form-group">
                                <label for="decree_date">Дата приказа</label>
                                <div class="input-group date">
                                    <input class="form-control pull-right required" id="decree_date" type="text"
                                           name="decree_date"
                                           value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Перевести</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>