<div id="add_education" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_education_form','method'=>'POST'])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавление уровня образования</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('employee_id','',['id'=>'employee_id']) !!}
                <div class="form-group">
                    <label for="education_id" class="control-label">Уровень образования</label>
                    <select name="education_id" id="education_id" class="form-control">
                        @foreach(getEducations() as $education)
                            <option value="{{$education->id}}">{{$education->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{Form::label('direction_of_preparation','Направление подготовки',['class'=>'control-label'])}}
                    {{Form::text('direction_of_preparation',null,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('qualification','Квалификация',['class'=>'control-label'])}}
                    {{Form::text('qualification',null,['class'=>'form-control'])}}
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>