<div id="add_subject_in_program" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_subject_in_prog','method'=>'POST'])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавление дисциплины в программу</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('program_url','',['id'=>'program_url']) !!}
                <div class="form-group">
                    <label for="subject_select" class="control-label">Дисциплина</label>
                    <select name="subject_id" id="subject_select" class="select-2-js">
                        @foreach(getSubjects() as $subject)
                            <optgroup label="{{$subject['title']}}">
                                @foreach($subject['items'] as $item)
                                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary layoff-send-js">Добавить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>