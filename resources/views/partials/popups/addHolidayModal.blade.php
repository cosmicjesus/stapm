<div id="addHolidayModal" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавить праздник</h4>
            </div>
            {{Form::open(['name'=>'addHolidayForm','id'=>'addHolidayForm'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Для добавления праздничных дней задайте название праздника, даты начала и окончания. Если
                            праздник однодневный, дату окончания можно не задавать.</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            {{Form::label('description','Название')}}
                            {{Form::text('description',null,['class'=>'form-control','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="" class="control-label">Период</label>
                            <div class="input-group input-daterange">
                                <div class="input-group-addon">C</div>
                                <input name="start_date" type="text" class="form-control" id="start_holiday" required>
                                <div class="input-group-addon">по</div>
                                <input name="end_date" type="text" class="form-control" id="end_holiday">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>