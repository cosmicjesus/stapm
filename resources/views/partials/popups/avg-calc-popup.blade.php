<div id="avg-calc-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Рассчет среднего балла</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div style="font-size: 16px;">
                    <span>Кол-во элементов <b><span class="avg-elemets-js">0</span></b></span><br>
                    <span>Средний балл(Без округления) <b><span class="avg-no-round-js">0</span></b></span><br>
                    <span>Средний балл <b><span class="avg-js">0</span></b></span>
                </div>
                <div class="form-group">
                    {{Form::label('threes','Кол-во троек')}}
                    {{Form::number('threes',0,['class'=>'form-control number-of-ratings-js','min'=>0,'step'=>'1'])}}
                </div>
                <div class="form-group">
                    {{Form::label('fours','Кол-во четвёрок')}}
                    {{Form::number('fours',0,['class'=>'form-control number-of-ratings-js','min'=>0,'step'=>'1'])}}
                </div>
                <div class="form-group">
                    {{Form::label('fives','Кол-во пятерок')}}
                    {{Form::number('fives',0,['class'=>'form-control number-of-ratings-js','min'=>0,'step'=>'1'])}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary add-avg-js">Добавить</button>
            </div>
        </div>
    </div>
</div>
</div>