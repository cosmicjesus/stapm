<div id="makeHelp" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выдача справки</h4>
            </div>
            {{Form::open(['name'=>'makeHelpForm','id'=>'makeHelpForm','target'=>'_blank','method'=>'GET'])}}
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p>Проверьте информацию и нажмите кнопку «Напечатать»</p>
                        <div class="form-group">
                            <label for="" class="control-label">Период обучения</label>
                            <div class="input-group input-daterange">
                                <div class="input-group-addon">C</div>
                                <input name="start_training" type="text" class="form-control" id="default-picker"
                                       value="">
                                <div class="input-group-addon">по</div>
                                <input name="end_training" type="text" class="form-control" id="end_training" value="">
                            </div>
                        </div>
                        <input type="hidden" name="decree" id="decree">
                        <input type="hidden" name="student_id" id="student_id">
                        <input type="hidden" name="count" id="count">
                        <div class="form-group">
                            <label for="decree_id">Приказ</label>
                            <select name="decree_id" id="decree_id" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="birthday">Дата</label>
                            <input class="form-control pull-right" id="birthday" type="text" name="date" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Напечатать</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
