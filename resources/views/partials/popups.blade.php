<div id="position" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Добавить должность</h4>
            </div>
            <div class="alert alert-danger" style="display: none">

            </div>
            <form class='form-horizontal' id='position_form'>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('name','Наименование',['class'=>'col-xs-3 control-label'])}}
                        <div class="col-xs-9">
                            {{Form::text('name',null,['class'=>'form-control','id'=>'position-name'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('type','Тип',['class'=>'col-xs-3 control-label'])}}
                        <div class="col-xs-9">
                            {{Form::select('type',positionTypes(),null,['class'=>'form-control'])}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save-js">Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="add_course_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_course','method'=>'POST','url'=>route('admin.employee.course.add')])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Добавление курса</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('employee_id','',['id'=>'employee_id']) !!}
                <div class="form-group">
                    {!! Form::label('title','Наименование',['class'=>'control-label']) !!}
                    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Наименование']) !!}
                </div>

                <div class="form-group">
                    <label for="" class="control-label">Период прохождения</label>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon">C</div>
                        <input name="start_date" type="text" class="form-control" id="course_start_date"
                               value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                        <div class="input-group-addon">по</div>
                        <input name="end_date" type="text" class="form-control" id="course_end_date">
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('count_hours','Количество часов',['class'=>'control-label']) !!}
                    {!! Form::number('count_hours',6,['class'=>'form-control','min'=>6]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description','Краткое описание',['class'=>'control-label']) !!}
                    {!! Form::textarea('description',null,['class'=>'form-control','min'=>6]) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary add-course-js">Добавить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div id="layoff_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'layoff_form','method'=>'POST','url'=>route('admin.employee.layoff.position')])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Снятие сотрудника с должности</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                    <p>Укажите дату, когда сотрудник был снят с должности</p>
                </div>
                {!! Form::hidden('id','',['id'=>'id']) !!}
                <div class="form-group">

                    <input name="layoff_date" type="text" class="form-control" id="layoff_date"
                           value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary layoff-send-js">Обновить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div id="add_position_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['id'=>'add_position_form','method'=>'POST'])!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Назначение на должность</h4>
            </div>
            <div class="modal-body">
                <div id="modal-notification">
                    <div class="alert alert-danger errors-block hide">
                        <ul class="errors"></ul>
                    </div>
                </div>
                {!! Form::hidden('employee_id','',['id'=>'employee_id']) !!}
                <div class="form-group">
                    {{Form::label('position_id','Должность',['class'=>'control-label'])}}
                    {{Form::select('position_id', getPositions(), null, ['placeholder' => 'Выберите должность','class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('start_date','Дата назначения')}}
                    <div class="input-group date">
                        <input name="start_date" type="text" class="form-control" id="start_date"
                               value="{{\Carbon\Carbon::now()->format('d.m.Y')}}">
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{Form::label('type_id','Должность',['class'=>'control-label'])}}
                    {{Form::select('type_id', types(), 0, ['class'=>'form-control'])}}
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary layoff-send-js">Назначить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('partials.popups.add-subject-in-program')
@include('partials.popups.add-doc-in-subject')
@include('partials.popups.add-other-document')
@include('partials.popups.avg-calc-popup')
@include('partials.popups.enrollment-modal')
@include('partials.popups.allocation-modal')
@include('partials.popups.transfer-modal')
@include('partials.popups.edit-education-modal')
@include('partials.popups.add-education')
@include('partials.popups.countEnrolleeReport')
@include('partials.popups.release-group')
@include('partials.popups.make-helps')
@include('partials.popups.edit-priem')
@include('partials.popups.addHolidayModal')
@include('partials.popups.money-references')