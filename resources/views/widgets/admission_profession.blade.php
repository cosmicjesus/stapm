@if(count($programs))
    <div class="table-scroll">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Наименование профессии/специальности</th>
                <th>Форма получения образования</th>
                <th>Уровень образования, необходимый для поступления</th>
                <th>Информация о вступительных испытаниях</th>
                <th>Количество мест для приема</th>
                <th>Профессиограмма</th>
                <th>Видеопрезентация</th>
                <th>Источник финансирования</th>
            </tr>
            @foreach($programs as $program)
                <tr>
                    <td>{{$program->getName()}}</td>
                    <td>{{$program->getEducationForm()}}</td>
                    <td>
                        @if($program->getEducationFormId()==1)
                            основное общее или среднее общее образование
                        @else
                            среднее общее (полное) образование
                        @endif
                    </td>
                    <td>не проводятся</td>
                    <td>{{$program->getPriem()->getReceptionPlan()}}</td>
                    <td>{!! $program->getPresentationPath()!!}</td>
                    <td>
                        @if($program->getVideoPresentationUrl())
                            <a data-fancybox
                               href='{{$program->getVideoPresentationUrl()}}&amp;autoplay=1&amp;showinfo=0'>
                                Открыть
                            </a>
                        @endif
                    </td>
                    <td>Бюджет субъекта РФ (Самарской области)</td>
                </tr>
            @endforeach
            <tr>
                <td style="text-align: center" colspan="8">Внимание! Прием абитуриентов на 2018/2019 учебный год по
                    договорам об

                    оказании платных образовательных услуг не осуществляется.
                </td>
            </tr>

        </table>
    </div>
@else
    <h2>На данный момент нет программ набора студентов</h2>
@endif