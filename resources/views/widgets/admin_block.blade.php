<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        @if($user->getPhoto())
            <img src="{{$user->getPhoto()}}" class="user-image" alt="{{$user->getName()}}">
        @endif
        <span class="hidden-xs">{{$user->getName()}}</span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            @if($user->getPhoto())
                <img src="{{$user->getPhoto()}}" class="img-circle" alt="User Image">
            @endif
            <p>
                {{$user->getName()}}
            </p>
        </li>
        <!-- Menu Body -->
        {{--<li class="user-body">--}}
            {{--<div class="row">--}}
                {{--<div class="col-xs-4 text-center">--}}
                    {{--<a href="#">Followers</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-4 text-center">--}}
                    {{--<a href="#">Sales</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-4 text-center">--}}
                    {{--<a href="#">Friends</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /.row -->--}}
        {{--</li>--}}
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Профиль</a>
            </div>
            <div class="pull-right">
                {{Form::open(['method'=>'POST','route'=>['logout']])}}
                    {{Form::submit('Выход',['class'=>'btn btn-default btn-flat'])}}
                {{Form::close()}}
            </div>
        </li>
    </ul>
</li>