<nav class="menu">
    <div class="menu_wrapper content">
        @foreach($items as $menu_item)
            <div class="menu_item">
                <span class="menu-full_name">{{$menu_item['name']}}</span>
                <span class="menu-short_name">{{$menu_item['short_name']}}</span>
                <div class="menu-sub_items">
                    <div class="content">
                        <div class="menu-sub_items-wrapper">
                            <div class="content">
                                <ul class="sub-items-list clearfix">
                                    @foreach($menu_item['sub'] as $item)
                                        <li><a class="sub_item-link" href="{{$item['url']}}">{{$item['name']}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</nav>