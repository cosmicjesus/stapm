<div class="mobile-menu__wrapper" id="mobile-menu">
    <div class="mobile-menu">
        <ul class="mobile-menu__items">
            @foreach($items as $menuItem)
                <li class="mobile-menu__item">
                    <details>
                        <summary class="mobile-menu__item-name">
                            {{$menuItem['name']}}
                        </summary>
                        <div class="mobile-sub-items">
                            <div class="content">
                                <ul>
                                    @foreach($menuItem['sub'] as $sub)
                                        <li>
                                            <a href="{{$sub['url']}}"
                                               tabindex="{{$loop->iteration+1}}">{{$sub['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </details>
                </li>
            @endforeach
        </ul>
    </div>
</div>