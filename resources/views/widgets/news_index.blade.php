<div class="news-index_wrapper content">
    @if(!empty($news['last']))
        <div class="news-index_last_news">
            <div class="news-index_last_news_date">{{$news['last']->getPublicationDate()}}</div>
            <div class="news-index_last_news_image"><img class="news-index_last_news_image_img"
                                                         src="{{$news['last']->getImage()}}"></div>
            <div class="news-index_last_news_text">
                <h2 class="news-index_last_news_text_title">
                    <a class="news-index_last_news_text_title_link" href="{{$news['last']->getDetailUrl()}}">
                        {{$news['last']->getTitle()}}
                    </a>
                </h2>
                {!! $news['last']->getPreview()!!}
            </div>
        </div>
    @endif
    @if(count($news['other']))
        <div class="news-index_news_list">
            @foreach($news['other'] as $news)
                <div class="news-index_news_list_item">
                    <div class="news-index_news_list_item_date">{{$news->getPublicationDate()}}</div>
                    <div class="news-index_news_list_item_text">
                        <h3 class="news-index_news_list_item_text_title">
                            <a class="news-index_news_list_item_text_title_link" href="{{$news->getDetailUrl()}}">
                                {{$news->getTitle()}}
                            </a>
                        </h3>
                        {!! $news->getPreview() !!}
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>