<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning">{{$count}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">У вас {{getNumEnding($count,['уведомление','уведомления','уведомлений'],true)}}</li>
        @if($count>0)
            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                    @if($notifications['countBookedReference']>0)
                        <li>
                            <a href="{{route('admin.references.archive')}}">
                                <i class="fa fa-users text-aqua"></i>
                                {{getNumEnding($notifications['countBookedReference'],[
                                'новая справка заказана',
                                'новые справки заказаны',
                                'новых справок заказано'
                                ],true)}} с сайта
                            </a>
                        </li>
                    @endif
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into--}}
                    {{--the--}}
                    {{--page and may cause design problems--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-users text-red"></i> 5 new members joined--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-shopping-cart text-green"></i> 25 sales made--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-user text-red"></i> You changed your username--}}
                    {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </li>
        @else
        @endif
        {{--<li class="footer"><a href="#">View all</a></li>--}}
    </ul>
</li>