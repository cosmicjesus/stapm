@foreach($menu as $item)
    @if ($item['extended'] && count($item['children']))
        @if(checkPermissions($item['visible']))
            <li class="treeview @if($item['selected']) active @endif">
                <a href="#">
                    <i class="fa {{$item['icon']?$item['icon']:'fa-user'}}"></i>
                    <span>{{$item['title']}}</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    @foreach($item['children'] as $child)
                        @if(checkPermissions($child['visible']))
                            <li @if($child['selected']) class="active" @endif>
                                <a href="{{route($child['route'])}}">
                                    <i class="fa fa-circle-o"></i>{{$child['title']}}
                                    @if($child['count'])
                                        <span class="pull-right-container">
                                    <span class="label label-primary pull-right">{{$child['count']}}</span>
                                </span>
                                    @endif
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>
        @endif
    @else
        @if(checkPermissions($item['visible']))
            <li>
                <a href="{{$item['custom_route']?$item['route']:route($item['route'])}}"
                   @if($item['target']) target="{{$item['target']}}" @endif>
                    <i class="fa {{$item['icon']?$item['icon']:'fa-user'}}"></i> <span>{{$item['title']}}</span>
                </a>
            </li>
        @endif
    @endif
@endforeach