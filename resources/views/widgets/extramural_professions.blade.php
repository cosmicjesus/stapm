@if(count($programs))
    <table class="table table-striped table-bordered">
        <tr>
            <th>Профессия/специальность</th>
            <th>База приема</th>
            <th>Срок обучения</th>
        </tr>
        @foreach($programs as $program)
            <tr>
                <td>{{$program->getName()}}</td>
                <td>Среднее (полное) общее образование</td>
                <td>{{$program->getPeriod()}}</td>
            </tr>
        @endforeach
    </table>
@endif