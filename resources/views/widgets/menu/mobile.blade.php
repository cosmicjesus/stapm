<div class="mobile-menu">
    <div class="mobile-menu-wrapper">
        <ul class="mobile-menu__items">
            @foreach($menu as $item)
                <li class="mobile-menu__items-item mobile-menu__items-item-js"><a
                            class="mobile-menu__link mobile-menu__link-js"
                            href="{{ !is_null($item['link'])?$item['link']:'#'}}">{{$item['name']}} @if(count($item['sub']))
                            <i class="fa fa-chevron-down menu-icon"></i>@endif</a>
                    @if(count($item['sub']))
                        <div class="mobile-menu__sub">
                            <div class="content">
                                <ul class="mobile-menu__sub-items">
                                    @foreach ($item['sub'] as $sub)
                                        @if(isset($sub['link']))
                                            <li class="mobile-menu__sub-items-item">
                                                <a class="mobile-menu__sub-items-link" href="{{$sub['link']}}">{{$sub['name']}}</a>
                                            </li>
                                            @else
                                        <li class="mobile-menu__sub-items-item">
                                            <a class="mobile-menu__sub-items-link" href="{{!empty($sub['route_name'])?route($sub['route_name']):'#'}}">{{$sub['name']}}</a>
                                        </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                </li>
            @endforeach
        </ul>
    </div>
</div>