<div class="side-bar">
    <aside class="side-bar_nav">
        <ul class="side-bar_nav-items">
            @foreach($menu as $item)
                @if(isset($item['link']))
                    <li class="side-bar_nav-items_item">
                        <a class="side-bar_nav-items_item-link" href="{{$item['link']}}">
                            {{$item['name']}}
                        </a>
                    </li>
                @else
                    <li class="side-bar_nav-items_item @if(!empty($item['route_name'])&&(route($item['route_name'])==URL::current())) side-bar_nav-items_item--active @endif">
                        <a class="side-bar_nav-items_item-link"
                           href="{{!empty($item['route_name'])?route($item['route_name']):'#'}}">
                            {{$item['name']}}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </aside>
</div>