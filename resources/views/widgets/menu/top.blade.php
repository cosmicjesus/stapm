<div class="menu">
    <div class="menu_wrapper content">
        <ul class="menu_items">
            @foreach($menu as $item)
                <li class="menu_items_item">
                    <div class="menu_items_item_link">{{$item['name']}}
                        @if(count($item['sub'])&& env('SHOW_ICON_ON_TOP_MENU',false))<i class="fa fa-chevron-down menu-icon"></i>@endif</div>
                    @if(count($item['sub']))
                        <div class="menu_items_item_sub_items">
                            <div class="content">
                                <div class="menu_items_item_sub_items_wrapper">
                                    <div class="content">
                                        @foreach($item['sub'] as $sub)
                                            @if(isset($sub['link']))
                                                <a class="menu_items_item_sub_items_item"
                                                   href="{{$sub['link']}}">{{$sub['name']}}</a>
                                            @else
                                                <a class="menu_items_item_sub_items_item"
                                                   href="{{!is_null($sub['route_name'])?route($sub['route_name']):'#'}}">{{$sub['name']}}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>