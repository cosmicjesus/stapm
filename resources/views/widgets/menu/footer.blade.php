<div class="footer-menu">
    @foreach($menu as $item)
        @if(count($item['sub']))
            <div class="footer-menu-item">
                <h3 class="footer-menu-title">{{$item['name']}}</h3>
                <ul class="footer-menu-sub-items">
                    @foreach($item['sub'] as $sub)
                        <li class="footer-menu-sub-item">
                            @if(isset($sub['link']))
                                <a class="footer-menu-sub-item-link"
                                   href="{{$sub['link']}}">{{$sub['name']}}</a>
                            @else
                                <a class="footer-menu-sub-item-link"
                                   href="{{$sub['route_name']?route($sub['route_name']):'#'}}">{{$sub['name']}}</a>

                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endforeach
</div>