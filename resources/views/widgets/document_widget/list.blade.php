@if($documents->count())
    <ul>
        @foreach($documents as $document)
            <li><a href="{{$document->getPath()}}" target="_blank" class="link" title="Открыть {{$document->getName()}}">{{$document->getName()}}</a></li>
        @endforeach
    </ul>
@endif