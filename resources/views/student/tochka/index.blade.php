@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="tochka-rosta">
        <img src="/img/tochka-banner.jpg" alt="Баннер точки роста" class="img">
        <p>«Точка роста» — это мобильный студенческий навигатор, который позволит всегда быть в курсе событий,
            организуемых в учебном заведении (вузе или ссузе) и на территории Самарской области.</p>
        <p><a href="https://vk.com/club165131874" target="_blank">СТАПМ - Точка роста в ВК</a></p>
        <h1 class="information_title">События</h1>
        <div class="news-list">
            @if(count($items))
                @foreach($items as $item)
                    <div class="news-item">
                        <div class="news-item__title">
                            <a class="news-item__title-link" href="{{$item->getDetailUrl()}}">{{$item->getTitle()}}</a>
                        </div>
                        <div class="news-item__date">{{$item->getPublicationDate()}}</div>
                        <div class="news-item-content">
                            <div class="news-item-content__image">
                                <img class="news-item-content__image-img" src="{{$item->getImage()}}">
                            </div>
                            <div class="news-item-content__preview">
                                {!! $item->getPreview() !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        {{$items->links('partials.paginate')}}
    </div>
@endsection