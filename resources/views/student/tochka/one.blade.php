@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <div class="news-detail">
        <div class="news-detail-top">
            <div class="news-detail__date">{{$item->getPublicationDate()}}</div>
            <div class="news-detail-return-to-news">
                <a class="news-detail-return-to-news__link" href="{{route('tochka')}}">К списку событий</a>
            </div>
        </div>
        <div class="news-detail-title">
            <h1 class="dark-text">{{$item->getTitle()}}</h1>
        </div>
        <div class="news-detail-content">
            <div class="news-detail-img">
                <img class="news-detail-image" src="{{$item->getImage()}}" align="left">
            </div>
            {!! $item->getFullText() !!}
        </div>
        @if($item->getPhotos())
            <div class="owl-carousel owl-theme owl-loaded">
                <div class="owl-stage-outer">
                    <div class="owl-stage">
                        @foreach($item->getPhotos() as $photo)
                            <div class="owl-item">
                                <img src="{{$photo->getPath()}}" alt="" class="img">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="owl-dots">
                    @foreach($item->getPhotos() as $photo)
                        <div class="owl-dot"><span></span></div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
@endsection