@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="assistance">
        <p>Дорогие выпускники и уважаемые работодатели!<br>
            По всем вопросам, касающимся трудоустройства, вы можете обращаться к руководителю Центра содействия
            трудоустройству выпускников
            <b>Ляпневу Александру Викторовичу</b></p>
        <ul>
            <li>рабочий телефон: 8(846) 955-22-14,</li>
            <li>эл.почта : alexvicl@mail.ru</li>
        </ul>

        Наш центр расположен по адресу: г. Самара, Старый переулок, д.6, кабинет {{env('TRUD_CAB',31)}}
        <hr>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['trud_files']])}}
        <hr>
        <h2>Вакансии</h2>
        <hr>
        {{Widget::run('App\Widgets\DocumentWidget',['category_slugs'=>['vacancy']])}}
        <hr>
    </div>
@endsection