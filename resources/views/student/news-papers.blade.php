@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="news-papers">
        <div bp="grid 4@lg 4@md">
            @foreach($newsPapers as $newsPaper)
                <div bp="1">
                    <div class="employee-card-info">
                        <div class="employee-personal" style="text-align: center">
                            <a class="employee-personal-link"
                               href="{{$newsPaper->getPublication()}}">{{$newsPaper->getTitle()}}</a>
                        </div>
                    </div>
                    <div class="employee-card-photo">
                        <a href="{{$newsPaper->getCover()}}" data-fancybox data-caption="{{$newsPaper->getTitle()}}">
                            <img class="employee-photo" src="{{$newsPaper->getCover()}}">
                        </a>
                    </div>
                    <div class="employee-card-info">
                        <div class="employee-personal">
                            <a class="employee-personal-link"
                               href="{{$newsPaper->getPublication()}}">Читать полностью</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection