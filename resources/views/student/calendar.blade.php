@extends('layout.layout_other')
@section('title',Breadcrumbs::current()->title)
@section('content')
    <h1 class="information_title">@yield('title')</h1>
    <div class="calendar">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Период</th>
                <th>Наименование</th>
            </tr>
            @foreach($holidays as $holiday)
                <tr>
                    <td>{{$holiday->getPeriod()}}</td>
                    <td>{{$holiday->getDescription()}}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection