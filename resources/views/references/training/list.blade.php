<html lang="ru">
<head>
    <title>Справки об обучении</title>
        <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">
</head>
<body>
@foreach($items as $item)
    @include('references.training.reference',['data'=>$item])
@endforeach
</body>
</html>