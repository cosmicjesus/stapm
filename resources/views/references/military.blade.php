<html lang="ru">
<head>
    <title>Справка в военкомат</title>
    <link rel="stylesheet" href="/css/admin-vendor.css?{{rand(1,9999)}}">
    <link rel="stylesheet" href="/css/admin-style.css?{{rand(1,9999)}}">

    <style>
        body {
            font-family: "Times New Roman";
            color: black !important;
        }

        .format-border {
            border-bottom: 1px solid black !important;
        }
    </style>
</head>
<body>
@foreach($students as $student)
    @include('references.tpl.military',['student'=>$student])
@endforeach
</body>
</html>
