<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Неверный логин или пароль.',
    'throttle' => 'Слишком много попыток входа. Попробуйте, пожалуйста, через :seconds секунд.',
    'no_login' => 'Пользователь с таким логином не найден',
    'no_email' => 'Не найдена почта, привязанная к логину. Пожалуйста, позвоните вашему менеджеру для восстановления пароля.',

];
