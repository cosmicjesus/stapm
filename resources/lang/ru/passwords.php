<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть как минимум 6 знаков в длину и совпадать с подтверждением.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отправили вам письмо с ссылкой для восстановления на почту ":mail"!',
    'token' => 'Неверный токен восстановления пароля.',
    'user' => "Не можем найти пользователя с такой почтой.",

];
