<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'unauthenticated' => 'Необходима аутентификация.',
    'unauthorized' => 'Неавторизованный запрос.',
    'page_not_found' => 'Запрошенная страница не найдена.',
    'relation_not_found' => 'Запрошенное отношение не существует.',
    'validation_failed' => 'Переданные данные не прошли проверку.',

];