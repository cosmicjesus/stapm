<?php

namespace App\Http\Controllers;

use App\AppLogic\CategoryDocument\CategoryDocument;
use App\Models\CategoriesDocument;
use App\Models\Document;
use App\Models\News;
use App\Models\ProfessionProgram;
use App\Models\Regulation;
use App\Models\SocialPartner;
use App\Widgets\Client\NewsIndex;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $date = Carbon::now()->format('Y-m-d');
        $annonces = News::query()->where('active', '=', 1)
            ->whereIn('type', [2, 3])
            ->where(function ($query) use ($date) {
                $query->where('visible_to', '>=', $date)
                    ->orWhereNull('visible_to');
            })
            ->orderBy('publication_date', 'DESC')
            ->take(4)
            ->get()
            ->map(function ($item) {
                return new \App\BusinessLogic\News\News($item);
            });
        return view('index', ['annonces' => $annonces]);
    }

    public function documents()
    {
        $regulationsModel = Regulation::query()->where('active', true)->orderBy('date');


        $yearList = $regulationsModel->select('date')
            ->distinct()
            ->get()
            ->map(function ($year) {
                return $year->date;
            })->toArray();

        $regulations = Regulation::query()
            ->where('active', true)
            ->orderBy('date')
            ->get()
            ->groupBy('date')
            ->map(function ($regulation) {
                return $regulation->map(function ($item) {
                    return new \App\BusinessLogic\Regulation\Regulation($item);
                });
            });


        $categories = CategoriesDocument::query()->where('active', true)
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get()
            ->map(function ($category) {
                return new CategoryDocument($category);
            });

        return view('index.document.documents', [
            'categories' => $categories,
            'yearList' => $yearList,
            'regulations' => $regulations
        ]);
    }

    public function acceptanceTransfer()
    {
        $programs = ProfessionProgram::query()
            ->whereHas('groups', function ($q) {
               return $q->where('status', 'teach');
            })
            ->with('profession')
            ->get()
            ->sortBy('profession.code')
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });

        return view('index.transfer', ['programs' => $programs]);
    }
}
