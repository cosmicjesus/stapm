<?php


namespace App\Http\Controllers;


class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @param null $data
     * @param null $message
     * @param int $code
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($data = NULL, $message = NULL, $code = 200)
    {
        // $response = [
        //     'success' => true
        // ];

        $response = $data;

        // if ($data) {
        //     $response['data'] = $data;
        // }
        if ($message) {
            $response['message'] = $message;
        }

        return response()->json($response, $code);
    }


    /**
     * return error response.
     *
     * @param $error
     * @param int $code
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $code = 404) // , $errorMessages = []
    {
        $response = [
            // 'success' => false,
            'message' => $error
        ];

        // if(!empty($errorMessages)){
        //   $response['data'] = $errorMessages;
        // }
        // $response = Json::response($data, $message)

        return response()->json($response, $code);
    }
}