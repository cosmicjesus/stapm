<?php

namespace App\Http\Controllers\PublicPart;


use App\BusinessLogic\Employee\EmployeeService;
use App\Http\Controllers\Controller;
use App\Models\Employee;

class EmployeeController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new EmployeeService();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {
        $employees = $this->service
            ->onlyWork()
            ->onlyTeachers()
            ->onlyFullTime()
            ->orderByFullName()
            ->map();

        $administration = Employee::query()
            ->whereHas('administrator')
            ->get()
            ->sortBy(function ($employee) {
                return $employee->administrator->sort;
            })
            ->map(function ($employee) {
                return new \App\BusinessLogic\Employee\Employee($employee);
            });

        return view('index.employee.index', ['employees' => $employees, 'administration' => $administration]);
    }

    public function one($slug)
    {
        $employee = $this->service->getBySlug($slug);

        return view('index.employee.one', ['employee' => $employee]);
    }

    public function administrations()
    {

        $employees = Employee::query()
            ->whereHas('administrator')
            ->get()
            ->sortBy(function ($employee) {
                return $employee->administrator->sort;
            })
            ->map(function ($employee) {
                return new \App\BusinessLogic\Employee\Employee($employee);
            });

        return view('index.employee.administration', ['employees' => $employees]);
    }

}