<?php

namespace App\Http\Controllers\PublicPart;

use App\BusinessLogic\News\NewsService;
use App\Http\Controllers\Controller;
use App\Models\News;
use Meta;
use Illuminate\Support\Facades\Request;

class NewsController extends Controller
{
    protected $service;

    public function __construct()
    {
        try {
            $this->service = new NewsService();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {
        $news = $this->service->onlyNews()->onlyVisible()->orderBy('publication_date', 'DESC')->paginate(10);
        $delail_route = 'news.one';
        return view('news.list', ['news' => $news,'delail_route' => $delail_route]);
    }

    public function one($slug)
    {
        try {

            $news = News::findBySlugOrFail($slug);
            $item = new \App\AppLogic\News\News($news);
            Meta::set('description', $item->getPreview());
            return view('news.one', ['item' => $item]);
        } catch (\Exception $exception) {
            return redirect()->route('news');
        }

    }
}