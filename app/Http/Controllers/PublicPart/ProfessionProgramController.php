<?php

namespace App\Http\Controllers\PublicPart;

use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use App\BusinessLogic\ProfessionProgram\ProfessionProgramService;
use App\Http\Controllers\Controller;
use App\Models\ProfessionProgram as ProgramModel;
use Illuminate\Http\Request;
use Meta;

class ProfessionProgramController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ProfessionProgramService();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {
        $programs = $this->service->convert($this->service
            ->where('on_site', '=', true)
            ->orderBy('sort', 'desc')
            ->all());
        return view('index.education', ['programs' => $programs]);
    }

    public function detail($slug)
    {
        try {
            $program = $this->service->getBySlug($slug);
            Meta::set('description', $program->getDescription());
            return view('index.education.detail', ['program' => $program]);
        } catch (\Exception $exception) {
            abort(404);
        }

    }

    public function umk($slug)
    {
        try {
            $program = $this->service->getBySlug($slug);
            Meta::set('description', $program->getDescription());
            return view('index.education.umk', ['program' => $program]);
        } catch (\Exception $exception) {
            return redirect()->route('education');
        }
    }

    public function standarts()
    {
        $programs = ProgramModel::query()
            ->whereHas('profession')
            ->where('education_form_id', 1)
            ->where('on_site',true)
            ->get()
            ->sortBy('profession.code')
            ->map(function ($program) {
                return new ProfessionProgram($program);
            });

        return view('index.education.standarts', ['programs' => $programs]);
    }
}
