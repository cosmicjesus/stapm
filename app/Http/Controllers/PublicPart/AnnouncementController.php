<?php

namespace App\Http\Controllers\PublicPart;

use App\BusinessLogic\News\NewsService;
use App\Models\News;
use Meta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Exception\NotFoundException;

class AnnouncementController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new NewsService();
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function index()
    {
        $announcements = $this->service
            ->onlyAnnouncements()
            ->onlyVisible()
            ->orderBy('publication_date', 'DESC')
            ->paginate(15);
        $delail_route = 'announcements.show';
        return view('news.list', ['news' => $announcements, 'delail_route' => $delail_route]);
    }

    public function show($slug)
    {
        try {
            $announcement = News::query()->where('slug', $slug)->first();
            $item = new \App\AppLogic\News\News($announcement);
            Meta::set('description', $item->getPreview());
            return view('news.one', ['item' => $item]);
        } catch (\Exception $exception) {
            new NotFoundException();
        }
    }
}
