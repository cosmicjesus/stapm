<?php


namespace App\Http\Controllers\PublicPart;


use App;
use App\BusinessLogic\News\News;
use App\Models\NewsPaper;
use Carbon\Carbon;

class StaticPagesController
{
    public function basicInformation()
    {
        return view('static.about');
    }

    public function hotLine()
    {
        return view('static.hot-line');
    }

    public function rooms()
    {
        return view('static.rooms');
    }

    public function material()
    {
        return view('static.material');
    }

    public function vospitanie()
    {
        return view('static.vospitanie');
    }

    public function structura()
    {
        return view('static.structura');
    }

    public function fhd()
    {
        return view('static.fhd');
    }

    public function codeOfHonor()
    {
        return view('student.code-of-honor');
    }

    public function memoToStudent()
    {
        return view('student.memo-to-student');
    }

    public function antiCorruption()
    {
        return view('student.anti-corruption');
    }

    public function newsPapers()
    {
        $nowYear = Carbon::now()->year;
        $startYear = $nowYear - 3;
        $dates = [
            0 => Carbon::create($startYear, 9, 1)->format('Y-m-d'),
            1 => Carbon::create($nowYear, 12, 31)->format('Y-m-d')
        ];
        $newsPapers = NewsPaper::query()
            ->whereBetween('publication_date', $dates)
            ->where('active', true)
            ->orderBy('publication_date', 'DESC')
            ->get()
            ->map(function ($newspaper) {
                return new \App\BusinessLogic\NewsPaper\NewsPaper($newspaper);
            });

        return view('student.news-papers', ['newsPapers' => $newsPapers]);
    }

    public function clinic()
    {
        return view('services.clinic');
    }

    public function library()
    {
        return view('services.library');
    }

    public function elLib()
    {
        $libs = [
            [
                'link' => 'http://www.academia-moscow.ru/',
                'img_link' => 'http://www.academia-moscow.ru/bitrix/templates/index/img/logo.png',
                'name' => 'Издательский центр «Академия»',
                'description' => 'Электронные учебники (ЭУ) «Академии» – это электронные версии печатных учебников, созданных в соответствии с программами ФГОС. Интерактивные возможности ЭУ позволяют вместить и эффективно использовать большие объемы информации. 
	        <br><b><i>Получить пароль для доступа к электронным учебникам можно при личном обращении в центральную библиотеку техникума.<br> Зав. библиотекой Рубан Ольга Михайловна.</i></b>'
            ],
            [
                'link' => 'http://xn--90ax2c.xn--p1ai/',
                'img_link' => 'http://xn--90ax2c.xn--p1ai/local/templates/adaptive/img/logo-header.png',
                'name' => 'Национальная электронная библиотека',
                'description' => 'Национальная электронная библиотека (НЭБ) - это информационная система, бесплатно предоставляющая пользователям интернета доступ к электронным фондам библиотек, участвующих в проекте, посредством единого веб-портала.
Зарегистрированный пользователь получает возможность пользоваться расширенным функционалом портала – делать свои закладки и заметки к произведениям, сохранять цитаты, поисковые запросы и другие функции, которые делают более комфортным процесс работы с порталом НЭБ.'
            ],
            [
                'link' => 'https://www.litres.ru/',
                'img_link' => 'https://www.litres.ru/static/litres/i/header/menu/logo.png',
                'name' => 'ЛитРес',
                'description' => 'Огромная библиотека

электронных книг. Несмотря на

то что Litres – магазин

электронных книг, кое- что (в

основном классику и

периодику) можно взять

бесплатно в

специальном разделе. 32 000

бесплатных книг 0+. Книги можно

скачать, однако для этого

необходимо авторизироваться на

сайте.'
            ],
            [
                'link' => 'http://www.knigafund.ru/',
                'img_link' => 'http://www.knigafund.ru/images/ib-logo.png?1447160113',
                'name' => 'Электронно-библиотечная система «КнигаФонд»',
                'description' => 'Электронно-библиотечная система «КнигаФонд» предлагает своим пользователям получить доступ более чем к 170 000 книг и материалов, в числе которых электронные учебники, учебные пособия, монографии, научные публикации, учебно-методические материалы, учебные и научные издания от ведущих издательств и вузов, а также классическая художественная литература.'
            ],
            [
                'link' => 'http://school-collection.edu.ru/',
                'img_link' => 'http://school-collection.edu.ru/img/logo.svg',
                'name' => 'Единая коллекция ЦОР',
                'description' => 'Информационная система "Единое окно

доступа к образовательным ресурсам"

предоставляет свободный доступ к

каталогу образовательных интернет-

ресурсов и полнотекстовой электронной

учебно-методической библиотеке для

общего и профессионального образования.'
            ],
            [
                'link' => 'http://window.edu.ru/',
                'img_link' => 'http://window.edu.ru/img/logo.window.svg',
                'name' => 'Единое окно доступа к информационным ресурсам',
                'description' => 'Электронный каталог библиотеки учебной литературы онлайн – уникальная возможность повысить свой уровень образования и получить неограниченный доступ к широкому спектру книг для школьников, студентов и преподавателей. На портале размещены электронные версии учебных материалов из библиотек вузов различных регионов России, научная и методическая литература. Электронные книги доступны как для чтения онлайн, так и для скачивания.'
            ],
            [
                'link' => 'https://www.book.ru/',
                'img_link' => 'https://www.book.ru/images/logo.gif',
                'name' => 'Электронно-библиотечная система от правообладателя',
                'description' => 'BOOK.ru — лицензионная библиотека, которая содержит учебные и научные издания от преподавателей ведущих вузов России. Отвечает требованиям ГОСТ 7.0.96-2016. Фонд электронной библиотеки комплектуется на основании новых ФГОС ВО, СПО.
    Библиотека регулярно пополняется новыми изданиями. На сайте размещаются книги до выхода их печатных аналогов.'
            ],
            [
                'link' => 'http://www.tehlit.ru/',
                'img_link' => null,
                'name' => 'ТехЛит.ру',
                'description' => 'Крупнейшая бесплатная электронная интернет библиотека для "технически умных" людей.'
            ]
        ];


        return view('services.el-lib', ['libs' => $libs]);
    }

    public function scholarship()
    {
        return view('static.scholarship');
    }

    public function paidEducational()
    {
        return view('static.paid');
    }

    public function canteen()
    {
        return view('services.canteen');
    }

    public function holidays()
    {
        $service = new App\BusinessLogic\Holiday\HolidayService();
        $holidays = $service->orderByStartDate()->all();

        return view('student.calendar', ['holidays' => $holidays]);
    }

    public function assistance()
    {
        return view('student.assistance-service');
    }

    public function information_security()
    {
        return view('static.information_security');
    }
}