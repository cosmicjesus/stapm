<?php

namespace App\Http\Controllers\PublicPart;


use App\BusinessLogic\ProfessionProgram\ProfessionPriem;
use App\Models\ProfessionProgram;
use App\Models\SocialPartner;
use App\Models\Student;

class EntrantController
{

    public function priem()
    {

        $programs = new ProfessionPriem();

        return view('entrant.priem', ['programs' => $programs]);
    }

    public function partnership()
    {
        $partners = SocialPartner::query()
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->map(function ($partner) {
                return new \App\BusinessLogic\SocialPartner\SocialPartner($partner);
            });
        return view('entrant.partnership', ['partners' => $partners]);
    }

    public function history()
    {
        return view('entrant.history');
    }

    public function admission()
    {
        return view('entrant.admission');
    }

    public function openDoor()
    {
        return view('entrant.opendoor');
    }

    public function ovz()
    {
        return view('entrant.ovz');
    }

    public function raiting($slug)
    {

        $showRaiting = \App\Helpers\Settings::realAvgRait();
        if (!$showRaiting) {
            abort(404,'Нот фаунд эйчтитипи эксепшен');
        }

        $program = ProfessionProgram::findBySlugOrFail($slug);
        $program_id = $program->id;
        $result = collect();
        $enrollees = Student::query()
            ->select(['firstname', 'lastname', 'middlename', 'avg', 'contract_target_set', 'has_original'])
            ->where('profession_program_id', $program_id)
            ->where('status', 'enrollee')
            ->orderBy('avg', 'DESC')
            ->get()
            ->groupBy(function ($item, $key) {
                return $item->contract_target_set ? 'contract' : 'regular';
            })
            ->sort()
            ->toArray();

        return view('entrant.raiting', ['enrollees' => $enrollees]);
    }
}