<?php

namespace App\Http\Controllers\PublicPart;


use Advmaker\CarbonPeriod;
use App\BusinessLogic\Holiday\HolidayService;
use App\Helpers\Settings;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderReferenceRequest;
use App\Models\Help;
use App\Models\Holiday;
use App\Models\Student;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class ReferencesController extends Controller
{

    protected $log;
    protected $successLog;
    protected $logPath = '/logs/references/ErrorOrderReferenceLog.log';
    protected $successLogPath = '/logs/references/SuccessOrderReferenceLog.log';

    public function __construct()
    {
        $this->log = new Logger('ErrorOrderReferenceLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->successLog = new Logger('SuccessOrderReferenceLog');
        $this->successLog->pushHandler(new RotatingFileHandler(storage_path() . $this->successLogPath, 7));
    }

    public function studentReference()
    {
        $groups = TrainingGroup::query()
            ->where('status', 'teach')
            // ->orderBy('profession_program_id')
            ->orderBy('pattern')
            ->orderBy('course')
            ->get()
            ->keyBy('id')
            ->map(function ($group) {
                return str_replace("{Курс}", $group->course, $group->pattern);
            })->toArray();

        return view('student.order-references', ['groups' => $groups]);
    }

    public function getDataForForm()
    {
        $studentsData = collect([
            'lastnames' => Settings::studentLastNames(),
            'firstnames' => Settings::studentFirstNames(),
            'middlenames' => Settings::studentMiddleNames()
        ]);
        return response()->json($studentsData);
    }

    public function orderReference(OrderReferenceRequest $request)
    {
        $student = Student::query()->with('decrees')->where('lastname', $request->lastname)
            ->where('firstname', $request->firstname)
            ->where('middlename', $request->middlename)
            ->where('group_id', $request->group_id)
            ->where('status', 'student')
            ->first();
        $types = $request->input('types');

        $str = 'Параметры запроса ';
        foreach ($request->input() as $key => $value) {
            if (!is_array($value)) {
                $str .= $key . " : " . $value . " // ";
            }
        }
        $str .= " Дата и время: " . Carbon::now()->format('H:i:s d.m.Y');
        if (is_null($student)) {
            $this->log->addError('Не найден студент по заданным параметрам. ' . $str);
            return response()->json([
                'success' => false,
                'tpl' => view('errors.alert', ['type' => 'danger', 'message' =>
                    'Студента с таким ФИО не обнаружено, возможно вы ввели неверные данные или база еще не обновилась.<br> 
Пожалуйста, проверьте данные или обратитесь в кабинет 8А для заказа справки'])->render()
            ]);
        }

        if (is_null($types)) {
            return response()->json([
                'success' => false,
                'tpl' => view('errors.alert', ['type' => 'danger', 'message' =>
                    'Не выбраны справки'])->render()
            ]);
        }

        $studentClass = new \App\BusinessLogic\Student\Student($student);
        $params = [
            'date' => Carbon::now()->format('Y-m-d'),
            'decree' => ($studentClass->getDecrees()->last())->getNumber(),
            'decree_id' => ($studentClass->getDecrees()->last())->getDecreeId(),
            'student_id' => $studentClass->getId(),
            'start_training' => $studentClass->getDateOfActualTransfer() ? $studentClass->getDateOfActualTransfer() : $studentClass->getGroup()->getEducationPlan()->getStartTrainig(),
            'end_training' => $studentClass->getGroup()->getEducationPlan()->getEndTrainig(),
            //'count' => $request->input('count')
        ];
        if (in_array('money', $types)) {
            $params['count'] = $request->input('money')['count'];
            $params['period'] = $request->input('money')['period'];
            $reference = new Help();
            $reference->student_id = $studentClass->getId();
            $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->max('number')) + 1;
            $reference->number = $number;
            $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $reference->params = $params;
            $reference->type = 'money';
            $reference->status = 'booked';
            $reference->save();
        }

        if (in_array('training', $types)) {
            $params['count'] = $request->input('training')['count'];
            unset($params['period']);
            $reference = new Help();
            $reference->student_id = $studentClass->getId();
            $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->max('number')) + 1;
            $reference->number = $number;
            $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $reference->params = $params;
            $reference->type = 'training';
            $reference->status = 'booked';
            $reference->save();
        }


        $day = $this->getGiveDay();
        $tpl = 'Ваша заявка №{number} успешно создана.';
        $this->successLog->addInfo('Справка успешно заказана. ' . $str);
        return response()->json([
            'success' => true,
            'tpl' => view('errors.alert', [
                'type' => 'success',
                'message' => massStrReplace($tpl, ['number' => $reference->id]),
                'day' => $day,
                'moneyMessage' => in_array('money', $types),
                'trainingReference' => in_array('training', $types)
            ])->render()
        ]);

    }

    /**
     * @return string
     */
    protected function getGiveDay()
    {
        $next = Carbon::now()->setTime(0, 0, 0, 0)->addDay(1);
        $endDate = $this->findInHolidays($next);
        $totalDate = null;

        if (!is_null($endDate)) {
            $endCarbon = $endDate->addDay();
            while ($endCarbon->isWeekend() || !is_null($this->findInHolidays($endCarbon))) {
                $endCarbon->addDay();
            }

            $totalDate = $endCarbon->format('d.m.Y');
        } else {
            while ($next->isWeekend() || !is_null($this->findInHolidays($next))) {
                $next->addDay();
            }
            $totalDate = $next->format('d.m.Y');
        }

        return $totalDate;
    }

    protected function findInHolidays($date)
    {
        $holidays = Holiday::query()->orderBy('start_date', 'ASC')->get()->map(function ($holiday) {
            $end = is_null($holiday->end_date) ? $holiday->start_date : $holiday->end_date;
            return ['start_date' => $holiday->start_date, 'end_date' => $end];
        })->toArray();

        $endDate = null;

        foreach ($holidays as $holiday) {
            if ($this->isDateBetweenDates($date, $holiday['start_date'], $holiday['end_date'])) {
                $endDate = $holiday['end_date'];
                break;
            };
        }

        return $endDate;
    }

    protected function isDateBetweenDates($date, $startDate, $endDate)
    {
        if ($date == $startDate && $date == $endDate) {
            return true;
        }

        return $date >= $startDate && $date <= $endDate;
    }

}