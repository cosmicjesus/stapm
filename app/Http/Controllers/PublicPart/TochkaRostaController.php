<?php

namespace App\Http\Controllers\PublicPart;


use App\BusinessLogic\TochkaRosta\TochkaRosta;
use App\BusinessLogic\TochkaRosta\TochkaService;
use App\Http\Controllers\Controller;
use App\Models\TochkaRostaItem;

class TochkaRostaController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new TochkaService();
        } catch (\Exception $e) {
        }
    }

    public function index()
    {

        $items = $this->service->onlyVisible()->orderBy('publication_date', 'DESC')->paginate(10);
        return view('student.tochka.index', ['items' => $items]);
    }

    public function one($slug)
    {
        $model = TochkaRostaItem::findBySlugOrFail($slug);
        $item = new TochkaRosta($model);

        return view('student.tochka.one', ['item' => $item]);
    }

}