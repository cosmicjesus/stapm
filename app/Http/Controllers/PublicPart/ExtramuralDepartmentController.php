<?php

namespace App\Http\Controllers\PublicPart;


use App\Http\Controllers\Controller;

class ExtramuralDepartmentController extends Controller
{

    public function index()
    {
        return view('extramural.index');
    }

    public function duties()
    {
        return view('extramural.duties');
    }

}