<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function formData()
    {
        $keys = [
            'names',
            'middlenames',
            'graduation_organizations_name',
            'graduation_organizations_place',
            'birthPlaces',
            'regions',
            'streets',
            'cities',
            'settlement',
            'area'
        ];

        $data = [];

        foreach ($keys as $key) {
            //$data[$key] = (array)\Cache::get($key);
            $data[$key] = \Cache::get($key);
        }

        return $data;
    }
}
