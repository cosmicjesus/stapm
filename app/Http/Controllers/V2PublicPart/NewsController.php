<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\NewsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    protected $service;

    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        try {
            $this->service = new NewsService();
        } catch (\Exception $e) {
        }
    }

    public function index(Request $request)
    {
        return $this->service->onlyNews()->onlyActive()->paginate($request->limit);
    }

    public function last($count = 5)
    {
        return $this->service->onlyActive()->onlyNews()->last($count);
    }

    public function list()
    {
        return view('v2.news.list');
    }

    public function one($slug)
    {
        try {
            $news = $this->service->getBySlug($slug);
            return view('v2.news.one', ['news' => $news]);
        } catch (\Exception $exception) {

        }
    }

    public function annoncements(Request $request)
    {
        return $this->service->onlyActive()->onlyEvents()->last(4);
    }

    public function mayNews(Request $request){
        return $this->service->onlyActive()->onlyWarNews()->getAll();
    }
}
