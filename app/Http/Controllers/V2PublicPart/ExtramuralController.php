<?php

namespace App\Http\Controllers\V2PublicPart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExtramuralController extends Controller
{
    public function index()
    {
        return view('client.extramural.index');
    }

    public function duties()
    {
        return view('client.extramural.duties');
    }
}
