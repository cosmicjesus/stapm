<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\ReplaceClassService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReplaceClassController extends Controller
{
    public function index()
    {
        return view('client.student.replace');
    }

    public function getReplaceClass(Request $request, $date)
    {
        return (new ReplaceClassService())->getReplaceByDate($date);
    }
}
