<?php

namespace App\Http\Controllers\V2PublicPart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistanceLearningController extends Controller
{
    public function index()
    {
        $items = [
            [
                'label' => 'Об организации учебного процесса с применением электронного обучения и дистанционных
                            образовательных технологий',
                'url' => route('client.dist.documents'),
                'icon' => 'fa-file-text'
            ],
            [
                'label' => 'ГИА',
                'url' => route('client.dist.gia'),
                'icon' => 'fa-file-text'
            ],
            [
                'label' => 'Расписание',
                'url' => route('client.distance-learning.time-table'),
                'icon' => 'fa-calendar'
            ],
            [
                'label' => 'Расписание квалификационных экзаменов',
                'url' => route('client.dist.qualification-exam'),
                'icon' => 'fa-calendar'
            ],
            [
                'label' => 'Военные сборы',
                'url' => route('client.dist.military'),
                'icon' => 'fa-male'
            ],
            [
                'label' => 'Ответственные лица за организацию дистанционного обучения',
                'url' => route('client.responsible-person'),
                'icon' => 'fa-user'
            ],
            [
                'label' => 'Инструкции и рекомендации для обучающихся и родителей',
                'url' => route('client.dist.instructions'),
                'icon' => 'fa-file-text'
            ],
            [
                'label' => 'Телефоны горячей линии для родителей и студентов',
                'url' => route('client.distance-learning.hot-line'),
                'icon' => 'fa-phone'
            ],
            [
                'label' => 'Мероприятия (конкурсы) для обучающихся с 6 по 30 апреля',
                'url' => route('client.dist-conc'),
                'icon' => 'fa-thumbs-up'
            ],
            [
                'label' => 'Платформы',
                'url' => route('client.dist.platforms'),
                'icon' => 'fa-server'
            ],
            //server
            [
                'label' => 'Досуговая деятельность',
                'url' => route('client.dist-dosug'),
                'icon' => 'fa-futbol-o'
            ],
            [
                'label' => 'Полезные ссылки',
                'url' => route('client.services.el-library'),
                'icon' => 'fa-book'
            ],
            [
                'label' => 'Психологическая помощь во время режима самоизоляции',
                'url' => route('client.services.psychologist'),
                'icon' => 'fa-user'
            ],
        ];

        $sections=collect($items)->chunk(3)->toArray();

        return view('client.dist.index', compact('sections'));
    }

    public function hotLine()
    {
        return view('client.dist.hot-line');
    }

    public function timetable()
    {
        $groups = [
            1 => [
                '11' => 11,
                '12' => 12,
                '12a' => '12А',
                13 => 13,
                14 => 14,
                16 => 16,
                17 => 17,
                'tm1' => 'ТМ-1',
                'tm1a' => 'ТМ-1А',
                'te1' => 'ТЭ-1',
                'ssa1' => 'ССА-1',
                'is1' => 'ИС-1',
                'bas1' => 'БАС-1',
                'asu1' => 'АСУ-1',
            ],
            2 => [
                21 => 21,
                22 => 22,
                23=>23,
                24 => 24,
                26 => 26,
                'tm2' => 'ТМ-2',
                'tm2a' => 'ТМ-2А',
                'te2' => 'ТЭ-2',
                'ssa2' => 'ССА-2',
                'bas2' => 'БАС-2',
                'asu2' => 'АСУ-2'],
            3 => [
                'is3' => 'ИС-3',
                'tm3' => 'ТМ-3',
                'tm3a' => 'ТМ-3А',
                'te3' => 'ТЭ-3',
                'asu3' => 'АСУ-3',
                'ks3' => 'КС-3',
                'sp3' => 'СП-3'
            ]
        ];

        return view('client.dist.timetable', compact('groups'));
    }
}
