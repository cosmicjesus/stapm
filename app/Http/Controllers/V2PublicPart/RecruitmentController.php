<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\EntrantService;
use App\AppLogic\Storage\StudentStorage;
use App\Exceptions\DuplicateEntryException;
use App\Http\Controllers\Controller;
use App\Jobs\AddEnrolleeToRecruitmentProgram;
use App\Jobs\AddEntrantFile;
use App\Jobs\AddParentJob;
use App\Jobs\SendEntrantEmail;
use App\Models\Entrant;
use App\Models\Student;
use App\Models\StudentFile;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RecruitmentController extends Controller
{

    protected $entrantService;

    public function __construct(EntrantService $entrantService)
    {
        $this->entrantService = $entrantService;
    }

    public function checkEntrant(Request $request)
    {
        try {
            $this->entrantService->checkUnique($request->all());
            return responder()->success()->respond();
        } catch (DuplicateEntryException $duplicateEntryException) {
            return responder()->error(422, $duplicateEntryException->getMessage())->respond(422);
        } catch (\Exception $exception) {
            return responder()->error(500, $exception->getMessage())->respond();
        }
    }

    public function check(Request $request)
    {
        try {
            $entrant = Entrant::query()->where($request->input())->first();

            if($entrant){
                return responder()->success(['id'=>$entrant->id])->respond();
            }

            throw new ModelNotFoundException();
        }catch (ModelNotFoundException $exception){
            return responder()->error(404,'Абитуриент не найден, проверьте введенные данные')->respond(404);
        }
        catch (\Exception $exception) {
            return responder()->error(500,$exception->getMessage())->respond(500);
        }
    }


    public function makeEnrollee(Request $request)
    {
        try {
            $this->entrantService->checkUnique($request->input('enrollee'));
            DB::beginTransaction();
            $enrollee = array_merge($request->input('enrollee'), ['passport_issuance_date' => $request->enrollee['passport_data']['issuanceDate']]);
            $enrolleeModel = Student::query()->create($enrollee);
            if ($enrolleeModel->id) {
                $this->addParents($enrolleeModel->id, $request->input('parents'));
                $this->addPrograms($enrolleeModel->id, $request->input('programs'));
                if ($enrolleeModel->email) {
                    dispatch(new SendEntrantEmail($enrolleeModel))->delay((Carbon::now())->addSeconds(120));
                }
            }
            DB::commit();
            return responder()->success(['id' => $enrolleeModel->id])->respond();
        }
        catch (DuplicateEntryException $duplicateEntryException) {
            return responder()->error(422, $duplicateEntryException->getMessage())->respond(422);
        }
        catch (\Exception $exception) {
            DB::rollBack();
            return responder()->error($exception->getCode(), $exception->getMessage())->respond();
        }
    }

    public function addParents($id, $parents)
    {
        $now = Carbon::now();
        foreach ($parents as $parent) {
            $now = $now->addSeconds(20);
            dispatch(new AddParentJob($id, $parent))->delay($now);
        }
    }

    public function addPrograms(int $id, array $programs)
    {
        $now = Carbon::now();
        foreach ($programs as $program) {
            $now = $now->addSeconds(15);
            dispatch(new AddEnrolleeToRecruitmentProgram($id, $program))->delay($now);
        }
    }

    public function uploadFiles(Request $request, $id)
    {
        try {

            $entrant = Student::query()->with('files')->findOrFail($id);

            foreach ($request->all('files')['files'] as $file) {
                $path = (new StudentStorage())->putFile($entrant->id, $file['file']);

                $file = new StudentFile([
                    'name' => $file['name'],
                    'path' => $path
                ]);

                $entrant->files()->save($file);

            }

            return responder()->success()->respond();
        } catch (\Exception $exception) {
            DB::rollBack();
            return responder()->error($exception->getCode(), $exception->getMessage())->respond();
        }
    }
}
