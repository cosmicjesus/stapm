<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Employee\Employee;
use App\AppLogic\Services\EmployeeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new EmployeeService();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {

        return view('client.index.employees.list');
    }

    public function administration()
    {
        try {
            $administration = $this->service->onlyWorks()->onlyAdministration()->get();


            return $administration->map(function (Employee $employee) {
                return [
                    'full_name' => $employee->getFullName(),
                    'positions' => $employee->getPositions(['onlyFullTime' => true, 'onlyActive' => true]),
                    'photo' => $employee->getPhoto(),
                    'sort' => $employee->getAdministrationSort(),
                    'profile_url' => $employee->getProfileUrl(),
                    'phone' => $employee->getPhones(),
                    'email' => $employee->getEmails()
                ];
            });
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function pedagogical(Request $request)
    {
        try {
            $teachers = $this->service->onlyWorks()->onlyTeachers()->onlyFullTime()->paginate($request->limit);

            return $teachers;
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function profile($slug)
    {
        $employee = $this->service->getBySlug($slug);
        return view('client.index.employees.profile', ['employee' => $employee]);
    }

    public function information(){
        return view('client.employees.information');
    }
}
