<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\ConferenceService;
use App\Exceptions\CustomModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConferenceController extends Controller
{

    protected $service;

    public function __construct(ConferenceService $conferenceService)
    {
        $this->service = $conferenceService;
    }

    public function detail($slug)
    {
        try {
            $conference = $this->service->getBySlug($slug);
            return view('client.conference.detail', ['conference' => $conference]);
        } catch (CustomModelNotFoundException $exception) {
            abort(404, $exception->getMessage());
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }
}
