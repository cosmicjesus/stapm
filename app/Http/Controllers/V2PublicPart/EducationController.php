<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\ProfessionProgram\ProfessionProgram;
use App\AppLogic\Services\ProfessionProgramService;
use App\Exceptions\CustomModelNotFoundException;
use App\Http\Controllers\Controller;


class EducationController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ProfessionProgramService();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function education()
    {
        try {
            $programs = $this->service->onlyActive()->onlyOnSite()->get();
            return view('client.index.education', ['programs' => $programs]);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function detailProgram($slug)
    {
        try {
            $program = $this->service->onlyOnSite()->onlyActive()->getBySlug($slug);
            return view('client.index.education.detail', ['program' => $program]);
        } catch (CustomModelNotFoundException $exception) {
            abort(404, $exception->getMessage());
        }
    }

    public function umk($slug)
    {
        try {
            $query = $this->service->onlyOnSite()->onlyActive()->getBySlug($slug);
            $program = $query->getProfession();
            $umk = $this->service->makeUmk($query->getId());
            //dd($umk);
            return view('client.index.umk', ['program' => $program, 'umk' => $umk]);
        } catch (CustomModelNotFoundException $exception) {
            abort(404, $exception->getMessage());
        }
    }

    public function standarts()
    {
        try {
            $programs = $this->service->onlyActive()->getAll()->map(function ($program) {
                return [
                    'name' => $program->getNameWithForm(),
                    'slug' => $program->getSlug(),
                    'has_standart' => $program->hasStandart(),
                    'standart_link' => $program->getStandartUrl()
                ];
            });
            return view('client.index.education.standarts', ['programs' => $programs]);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function getOnlyActiveOnIndex()
    {
        try {
            $programs = $this->service->onlyActive()->onlyOnSite()->getAll()->map(function (ProfessionProgram $program) {
                return [
                    'name' => $program->getNameWithForm(),
                    'slug' => $program->getSlug(),
                    'has_image' => $program->hasImage(),
                    'image' => $program->getImage(),
                    'description' => $program->getDescription()
                ];
            });

            return response()->json($programs);

        } catch (\Exception $exception) {

        }
    }
}
