<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\CategoryDocument\CategoryDocument;
use App\AppLogic\Services\NewsService;
use App\AppLogic\Services\ProfessionProgramService;
use App\AppLogic\ProfessionProgram\ProfessionProgram as ProgramHandler;
use App\AppLogic\TrainingGroup\TrainingGroup;
use App\Http\Controllers\Controller;
use App\Mail\Feedback;
use App\Models\CategoriesDocument;
use App\Models\Media;
use App\Models\ProfessionProgram;
use App\Models\Regulation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class IndexController extends Controller
{
    protected $programService;

    public function __construct()
    {

    }

    public function index()
    {
        return view('client.index');
    }

    public function basicInformation()
    {
        return view('client.index.basic-information');
    }

    public function structura()
    {
        return view('client.index.structura');
    }

    public function documents()
    {
        $categories = CategoriesDocument::query()->where('active', true)
            ->whereNull('parent_id')
            ->orderBy('sort')
            ->get()
            ->map(function ($category) {
                return new CategoryDocument($category);
            });

        return view('client.index.documents', ['categories' => $categories]);
    }

    public function regulations()
    {
        try {
            $now = Carbon::now()->format('Y');
            $years = [($now - 3), $now];
            $regulations = Regulation::query()
                ->where('active', true)
                ->whereBetween('date', $years)
                ->orderBy('date')
                ->get()
                ->groupBy('date')
                ->map(function ($regulation) {
                    return $regulation->map(function ($item) {
                        return (new \App\AppLogic\Regulation\Regulation($item))->full();
                    });
                });


            return $regulations;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function material()
    {
        return view('client.index.material');
    }

    public function fhd()
    {
        $parentCategories = CategoriesDocument::query()->with('childs')->where('slug', 'fhd')->first();

        $categories = $parentCategories->childs->chunk(3);

        return view('client.index.finance.years', compact('categories'));
    }

    public function detailFhd($slug)
    {
        $category = CategoriesDocument::query()->with('parent')->where('slug', $slug)->first();
        if (!$category) {
            abort(404, 'Категория не найдена');
        }
        if ($category->parent) {
            if ($category->parent->slug != 'fhd') {
                abort(404, 'Категория не найдена');
            }
        }

        return view('client.docs', ['category' => $slug]);
    }

    public function hotLine()
    {
        return view('client.index.hot-line');
    }

    public function rooms()
    {
        return view('client.index.rooms');
    }

    public function scholarship()
    {
        return view('client.index.scholarship');
    }

    public function paidEducational()
    {
        return view('client.index.paid');
    }

    /**
     * @return \Exception|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function acceptanceTransfer()
    {
        try {
            $programs = ProfessionProgram::query()
                ->whereHas('groups', function ($q) {
                    return $q->where('status', 'teach');
                })
                ->with('profession')
                ->get()
                ->sortBy('education_form_id')
                ->sortBy('profession.code')
                ->map(function ($program) {
                    return new ProgramHandler($program);
                })
                ->map(function (ProgramHandler $program) {
                    return [
                        'name' => $program->getNameWithForm(),
                        'groups' => $program->getTrainingGroups()->map(function (TrainingGroup $group) {
                            return [
                                'name' => $group->getName(),
                                'course' => $group->getCourse(),
                                'step' => $group->getStep(),
                                'max_people' => $group->getMaxPeople(),
                                'count_vacancy_transfer' => $group->getCountVacancyTransfer()
                            ];
                        })->sortBy('name')
                    ];
                });
            return view('client.index.transfer', ['programs' => $programs]);
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function feedback()
    {
        return view('client.feedback');
    }

    public function mediaAboutUs()
    {
        $medias = Media::query()->orderBy('sort', 'DESC')->get();

        return view('client.index.smi-about-us', ['medias' => $medias]);
    }

    public function feedbackSend(Request $request)
    {
        try {
            Mail::to(env('FEEDBACK_EMAIL', 'priem@stapm.ru'))->send(new Feedback($request->all()));
            return ['success' => true];
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function may9()
    {
        $newsService = new NewsService();
        $news = $newsService->onlyActive()->onlyWarNews()->getAll();

        return view('client.may9', ['news' => $news]);
    }

    public function distance()
    {
        $groups = [
            1 => [
                '11' => 11,
                '12' => 12,
                '12a' => '12А',
                13 => 13,
                14 => 14,
                16 => 16,
                17 => 17,
                'tm1' => 'ТМ-1',
                'tm1a' => 'ТМ-1А',
                'te1' => 'ТЭ-1',
                'ssa1' => 'ССА-1',
                'is1' => 'ИС-1',
                'bas1' => 'БАС-1',
                'asu1' => 'АСУ-1',
            ],
            2 => [
                21 => 21,
                22 => 22,
                24 => 24,
                26 => 26,
                'tm2' => 'ТМ-2',
                'tm2a' => 'ТМ-2А',
                'te2' => 'ТЭ-2',
                'ssa2' => 'ССА-2',
                'bas2' => 'БАС-2',
                'asu2' => 'АСУ-2'],
            3 => [
                'is3' => 'ИС-3',
                'tm3' => 'ТМ-3',
                'tm3a' => 'ТМ-3А',
                'te3' => 'ТЭ-3',
                'asu3' => 'АСУ-3',
                'ks3' => 'КС-3',
                'sp3' => 'СП-3'
            ]
        ];

        return view('client.dist.index', compact('groups'));
    }

    public function prevention()
    {
        return view('rrr');
    }
}
