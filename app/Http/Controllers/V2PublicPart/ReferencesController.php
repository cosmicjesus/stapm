<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\ReferenceService;
use App\Http\Requests\OrderReferenceRequest;
use App\Models\Holiday;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferencesController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ReferenceService();
        } catch (\Exception $e) {
            abort(500, 'Ошибка при инициализации сервиса справок');
        }
    }

    public function referencePage()
    {
        return view('client.student.reference');
    }

    public function orderReference(OrderReferenceRequest $request)
    {
        $student = Student::query()->where([
            'lastname' => $request->lastname,
            'firstname' => $request->firstname,
            'middlename' => $request->middlename,
            'group_id' => $request->group_id,
            'status' => 'student'
        ])->first();

        if (!$student) {
            return [
                'success' => false,
                'errors' => ['Студента с таким ФИО не обнаружено, возможно вы ввели неверные данные или база еще не обновилась. 
Пожалуйста, проверьте данные или обратитесь в кабинет 8А для заказа справки']
            ];
        }


        if ($student->long_absent) {
            return [
                'success' => false,
                'errors' => ['Вы находитесь в числе длительноотсутствующих студентов, для получения справки обратитесь в кабинет 8А']
            ];
        }
        try {
            $student = new \App\AppLogic\Student\Student($student);
            $params = [
                'date' => Carbon::now()->format('Y-m-d'),
                'decree' => ($student->getDecrees()->last())['number'],
                'decree_id' => ($student->getEnrollmentDecree())['id'],
                'student_id' => $student->getId(),
                'start_training' => $student->getDateOfActualTransfer() ? $student->getDateOfActualTransfer() : $student->getTrainingGroup()->getStartDate(),
                'end_training' => $student->getTrainingGroup()->getEndDate(),
            ];
            $messages = [];
            if (in_array('training', $request->types)) {
                $referenceId = $this->service->makeTrainingReference(array_merge($params, $request->training));
                if ($referenceId) {
                    $day = $this->getGiveDay();
                    $messages[] = str_replace("{day}", $day, "Получить справку об обучении Вы можете {day}, в кабинете 8А c 11:30 до 16:00");
                }
            }
            if (in_array('money', $request->types)) {
                $referenceId = $this->service->makeMoneyReference(array_merge($params, $request->money));
                if ($referenceId) {
                    $messages[] = 'Справку о стипендии Вы можете получить в ближайший вторник';
                }
            }
            if (in_array('military', $request->types)) {
                $referenceId = $this->service->makeMilitaryReference(array_merge($params, [
                    'start_date' => $student->getDateOfActualTransfer() ? $student->getDateOfActualTransfer() : $student->getTrainingGroup()->getStartDate(),
                    'end_date' => $student->getTrainingGroup()->getEndDate(),
                ]));
                if ($referenceId) {
                    $day = $this->getGiveDay();
                    $messages[] = str_replace("{day}", $day, "Получить справку в военный комиссариат Вы можете {day}, в кабинете 8А c 11:30 до 16:00");
                }
            }
            return [
                'success' => true,
                'messages' => $messages,
            ];
        } catch (\Exception $exception) {
            abort(500, 'Произошла непредвиденная ошибка');
        }
    }

    /**
     * @return string|null
     */
    protected function getGiveDay()
    {
        $next = Carbon::now()->setTime(0, 0, 0, 0)->addDay(1);
        $endDate = $this->findInHolidays($next);
        $totalDate = null;

        if (!is_null($endDate)) {
            $endCarbon = $endDate->addDay();
            while ($endCarbon->isWeekend() || !is_null($this->findInHolidays($endCarbon))) {
                $endCarbon->addDay();
            }

            $totalDate = $endCarbon->format('d.m.Y');
        } else {
            while ($next->isWeekend() || !is_null($this->findInHolidays($next))) {
                $next->addDay();
            }
            $totalDate = $next->format('d.m.Y');
        }

        return $totalDate;
    }

    /**
     * @param $date
     * @return |null
     */
    protected function findInHolidays($date)
    {
        $holidays = Holiday::query()->orderBy('start_date', 'ASC')->get()->map(function ($holiday) {
            $end = is_null($holiday->end_date) ? $holiday->start_date : $holiday->end_date;
            return ['start_date' => $holiday->start_date, 'end_date' => $end];
        })->toArray();

        $endDate = null;

        foreach ($holidays as $holiday) {
            if ($this->isDateBetweenDates($date, $holiday['start_date'], $holiday['end_date'])) {
                $endDate = $holiday['end_date'];
                break;
            };
        }

        return $endDate;
    }

    /**
     * @param $date
     * @param $startDate
     * @param $endDate
     * @return bool
     */
    protected function isDateBetweenDates($date, $startDate, $endDate)
    {
        if ($date == $startDate && $date == $endDate) {
            return true;
        }

        return $date >= $startDate && $date <= $endDate;
    }
}
