<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Holiday\HolidayService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function codeOfHonor()
    {
        return view('client.student.code-of-honor');
    }

    public function memoToStudent()
    {
        return view('client.student.memo-to-student');
    }

    public function antiCorruption()
    {
        return view('client.student.anti-corruption');
    }

    public function holidays()
    {
        $service = new HolidayService();
        $holidays = $service->orderByStartDate('DESC')->all();

        return view('client.student.calendar', ['holidays' => $holidays]);
    }

    public function assistance()
    {
        return view('client.student.assistance-service');
    }

    public function vospitanie()
    {
        return view('client.student.vospitanie');
    }
}
