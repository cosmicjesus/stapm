<?php

namespace App\Http\Controllers\V2PublicPart;

use App\AppLogic\Services\ProfessionProgramService;
use App\AppLogic\Services\RecruitmentProgramService;
use App\AppLogic\Services\SelectionCommitteeService;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\SocialPartner;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntrantController extends Controller
{

    protected $programService;
    protected $selectionCommitteeService;

    public function __construct()
    {
        try {
            $this->programService = new ProfessionProgramService();
            $this->selectionCommitteeService = new SelectionCommitteeService();
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function admission()
    {
        $programs = $this->programService->programPriem();

        $committee = $this->selectionCommitteeService->getActiveCommittee();
        $now = Carbon::now();
        $nextCommitteePublicationDate = Carbon::create($now->year, 3, 1);
        if (in_array($now->month, [11, 12])) {
            $nextCommitteePublicationDate->addYear();
        }

        return view('client.entrant.admission', [
            'programs' => $programs,
            'committee' => $committee,
            'nextCommitteePublicationDate' => $nextCommitteePublicationDate->format('d.m.Y')
        ]);
    }

    public function openDoor()
    {
        return view('client.entrant.opendoor');
    }

    public function ovz()
    {
        return view('client.entrant.ovz');
    }

    public function history()
    {
        return view('client.entrant.history');
    }

    public function priem()
    {

        $activeCommitteeID = $this->selectionCommitteeService->getActiveCommitteeId();

        if (is_null($activeCommitteeID)) {
            return view('client.entrant.priem', ['data' => ['programs' => [], 'total' => []]]);
        }

        $data = ((new RecruitmentProgramService())->getProgramsByCommitteeId($activeCommitteeID));

        return view('client.entrant.priem', compact('data', 'activeCommitteeID'));
    }

    public function raiting(string $slug)
    {
        if (!setting('showEnrolleesRatins')) {
            abort(404);
        }
        try {
            $program = (new RecruitmentProgramService())->getBySlug($slug);
            $entrants = collect($program['entrants']['peoples'])
                ->filter(fn($entrant) => in_array($entrant['status'], ['enrollee', 'unconfirmed_enrollee']) && $entrant['is_priority'])
                ->sortByDesc(function ($entrant) {
                    return [$entrant['average_score'], $entrant['sum_of_grades_of_specialized_disciplines']];
                })->values();

            return view('client.entrant.raiting', compact('program', 'entrants'));

        } catch (CustomModelNotFoundException $exception) {
            abort(404, $exception->getMessage());
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function partnership()
    {
        $partners = SocialPartner::query()
            ->where('active', true)
            ->orderBy('sort')
            ->get()
            ->map(function ($partner) {
                return new \App\BusinessLogic\SocialPartner\SocialPartner($partner);
            });
        return view('client.entrant.partnership', ['partners' => $partners]);
    }
}
