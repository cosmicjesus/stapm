<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EntrantService;
use App\Exceptions\DuplicateEntryException;
use App\Http\Requests\EnrollmentEntrantsRequest;
use App\Http\Requests\UpdateEnrolleePersonal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntrantController extends Controller
{
    protected $service;

    public function __construct(EntrantService $entrantService)
    {
        try {
            $this->service = $entrantService;
        } catch (\Exception $e) {
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->filter($request->filter)->paginate($request->limit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->service->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEnrolleePersonal $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEnrolleePersonal $request, $id)
    {
        return $this->service->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return $this->service->delete($request->ids);
    }

    public function pdfEntrantReport(Request $request)
    {
        return $this->service->reportToPdf($request->all());

    }

    public function makeReport(Request $request)
    {
        return $this->service->reportDataToJson($request->all());
    }

    public function refreshCount()
    {
        return $this->service->refreshCount();
    }

    public function enrollment(EnrollmentEntrantsRequest $request){
        return $this->service->enrollment($request->all());
    }

    public function checkUnique(Request $request)
    {
        try {
            $this->service->checkUnique($request->all());
            return response()->json(['success' => true]);
        } catch (DuplicateEntryException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Такой человек уже существует в базе'
            ])->setStatusCode(422);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}
