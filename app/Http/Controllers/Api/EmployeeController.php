<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EmployeeService;
use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\UpdateEmployeePersonalInfo;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function GuzzleHttp\Psr7\_parse_request_uri;

class EmployeeController extends Controller
{

    protected $service;

    public function __construct(EmployeeService $employeeService)
    {
        $this->service = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->filter($request->filter)->paginate($request->total);
    }

    public function allForSelect()
    {
        return $this->service->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        return $this->service->addEmployee($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->getById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeePersonalInfo $request, $id)
    {
        return $this->service->updateInfo($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function militaryRecord(Request $request, $id)
    {
        try {
            $employee = Employee::query()->findOrFail($id);
            $data = $request->input();
            if(array_key_exists('recruitment_center',$data)){
                $data['recruitment_center_id'] = $data['recruitment_center']['id'];
            }
            $employee->update($data);
            return responder()->success(['message' => 'Данные воинского учета успешно обновлены', 'ee' => $request->input()])->respond(200);
        } catch (\Exception $exception) {
            return responder()->error(500, $exception->getMessage())->respond(500);
        }
    }

    public function all()
    {
        try {
            $employees = Employee::query()
                ->with('credentials')
                ->where('status', 'work')
                ->orderBy('lastname')
                ->orderBy('firstname')
                ->get()
                ->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'full_name' => trim($employee->lastname . " " . $employee->firstname . " " . $employee->middlename),
                        'credentials' => $employee->credentials
                    ];
                });

            return response()->json($employees);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }
}
