<?php

namespace App\Http\Controllers\Api;

use App\Models\Student;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use PDF;

class ReportController extends Controller
{
    public function countStudentsOnDate(Request $request)
    {
        try {
            $data = Carbon::createFromFormat('Y-m-d', $request->report_date);

            $groups = TrainingGroup::query()
                ->with('students')
                ->onlyTeach()
                ->orderBy('course')
                ->orderBy('pattern')
                ->get()
                ->groupBy('course')
                ->map(function ($course) use ($data) {
                    return $course->map(function ($group) use ($data) {
                        $totalStudentsCount = $group->students->filter(function ($student) use ($data) {
                            return $student->enrollment_date <= $data;
                        })->count();
                        $students = $group->students->filter(function ($student) use ($data) {
                            return ($student->status == 'student') && ($student->enrollment_date <= $data);
                        })->count();
                        $longTime = $group->students->filter(function ($student) use ($data) {
                            return ($student->status == 'student') && ($student->enrollment_date <= $data) && $student->long_absent;
                        })->count();
                        $inArmy = $group->students->filter(function ($student) use ($data) {
                            return ($student->status == 'inArmy') && ($student->enrollment_date <= $data);
                        })->count();

                        $academic = $group->students->filter(function ($student) use ($data) {
                            return ($student->status == 'academic') && ($student->enrollment_date <= $data);
                        })->count();

                        return [
                            'group_name' => $group->full_name,
                            'totalStudentsCount' => $totalStudentsCount,
                            'students' => $students,
                            'longTimeAbsent' => $longTime,
                            'inArmy' => $inArmy,
                            'academic' => $academic
                        ];
                    });
                });


            $totalSums = [];

            /**
             * @var $students Collection
             */

            $students = Student::query()->whereDate('enrollment_date', '<=', $data->format('Y-m-d'))->onlyTeach()->get();

            $totalSums['totalStudentsCount'] = $students->count();
            $totalSums['students'] = $students->filter(function ($student) {
                return $student->status == Student::$studentStatus;
            })->count();
            $totalSums['longTimeAbsent'] = $students->filter(function ($student) {
                return ($student->status == Student::$studentStatus) && ($student->long_absent == true);
            })->count();;
            $totalSums['inArmy'] = $students->filter(function ($student) {
                return $student->status == Student::$armyStatus;
            })->count();;
            $totalSums['academic'] = $students->filter(function ($student) {
                return $student->status == Student::$academicStatus;
            })->count();;

            $pdf = PDF::loadView('reports.countStudentsReport',
                [
                    'date' => $data,
                    'data' => $groups,
                    'totalSums' => $totalSums
                ]
            );

            //return $pdf->stream();
            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
