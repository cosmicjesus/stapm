<?php

namespace App\Http\Controllers\Api;

use App\Models\StudentFile;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use League\Flysystem\FileNotFoundException;

class ProtectedFileController extends Controller
{

    public function __invoke()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getStudentFile(Request $request, $uuid)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            
            $file = StudentFile::whereUuid($uuid)->firstOrFail();
            $isExists = Storage::disk('student')->has($file->path);
            if (!$isExists) {
                throw new FileNotFoundException($file->path);
            }
            $pathToFile = storage_path('app/private/students/' . $file->path);
            $pathInfo = pathinfo($pathToFile);
            if ($pathInfo['extension'] == 'pdf') {
                return response()->file($pathToFile);
            }
            return response()->download($pathToFile);
        } catch (FileNotFoundException $exception) {
            return $exception->getMessage();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
