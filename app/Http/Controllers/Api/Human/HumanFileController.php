<?php

namespace App\Http\Controllers\Api\Human;

use App\AppLogic\Storage\StudentStorage;
use App\AppLogic\Student\StudentFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddStudentFileRequest;
use App\Jobs\DeleteHumanFileJob;
use App\Models\Student;
use App\Models\StudentFile as StudentFileModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HumanFileController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $human_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddStudentFileRequest $request, int $human_id)
    {
        try {
            $student = Student::query()->with('files')->findOrFail($human_id);

            $path = (new StudentStorage())->putFile($human_id, $request->file('file'));

            $file = new StudentFileModel([
                'name' => $request->name,
                'path' => $path
            ]);

            $student->files()->save($file);

            return responder()->success([
                'file' => (new StudentFile($file))->build()
            ])->respond();

        } catch (AccessDeniedHttpException $exception) {
            return responder()->success([
                'success' => false,
                'message' => 'У вас нет прав на выполнение данного действия'
            ])->respond(403);
        } catch (ModelNotFoundException $exception) {
            return responder()->success([
                'success' => false,
                'message' => "Не найден студент с ID {$human_id}"
            ])->respond(404);
        } catch (\Exception $exception) {
            responder()->success([
                'success' => false,
                'message' => $exception->getMessage()
            ])->respond(500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $human_id
     * @param int $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $human_id, int $file)
    {
        try {
            $model = StudentFileModel::query()->findOrFail($file);
            $path = $model->path;

            if ($model->delete()) {
                dispatch((new DeleteHumanFileJob($path))->delay(100));
            }


            return responder()->success()->respond();

        } catch (ModelNotFoundException $exception) {
            return responder()->success([
                'success' => false,
                'message' => "Не найден файл с ID {$human_id}"
            ])->respond(404);
        } catch (\Exception $exception) {
            responder()->success([
                'success' => false,
                'message' => $exception->getMessage()
            ])->respond(500);
        }
    }
}
