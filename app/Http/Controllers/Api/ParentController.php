<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\CacheService;
use App\Http\Requests\AddParentRequest;
use App\Models\UserParent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddParentRequest $request)
    {
        try {
            $model = UserParent::query()->create($request->all());
            $model->childrens()->attach($request->user_id);
            CacheService::refreshParentsCount();
            return new JsonResponse([
                'success' => true,
                'message' => 'Родитель был успешно добавлен',
                'parent_id' => $model->id
            ], 200);
        } catch (\Exception $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            UserParent::destroy($id);
            CacheService::refreshParentsCount();
            return new JsonResponse([
                'success' => true,
                'message' => 'Родитель был успешно удален',
            ], 200);
        } catch (\Exception $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine()
            ], 500);
        }
    }
}
