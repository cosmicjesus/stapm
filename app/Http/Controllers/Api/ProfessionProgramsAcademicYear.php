<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\ProfessionProgramAcademicYearService;
use App\Http\Requests\ProfessionProgram\CopySubjectsFromAcademicYearRequest;
use App\Http\Requests\ProfessionProgram\CreateProfessionProgramAcademicRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfessionProgramsAcademicYear extends Controller
{
    protected $service;

    public function __construct(ProfessionProgramAcademicYearService $service)
    {
        $this->service = $service;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateProfessionProgramAcademicRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProfessionProgramAcademicRequest $request)
    {
        return $this->service->store($request->all());
    }

    public function copySubjectFromAnotherYear(CopySubjectsFromAcademicYearRequest $request)
    {
        return $this->service->copySubjectFromAnotherYear($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
