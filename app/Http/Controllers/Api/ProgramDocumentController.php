<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\FileComponent\FileComponent;
use App\AppLogic\Pdf\PDF;
use App\AppLogic\ProfessionProgram\ProgramFiles;
use App\AppLogic\Services\ProgramDocumentService;
use App\AppLogic\Storage\ComponentDocumentStorage;
use App\AppLogic\Storage\ProfessionProgramStorage;
use App\AppLogic\Storage\StorageFiles;
use App\Http\Requests\AddSubjectInProgramRequest;
use App\Models\ComponentDocument;
use App\Models\ProgramDocument;
use App\Models\ProgramSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramDocumentController extends Controller
{
    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ProgramDocumentService();
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Не могу инициализировать сервис <br>' . $exception->getMessage()])->setStatusCode(500);
        }
    }

    public function addDocumentInSubject(Request $request)
    {
        return $this->service->AddDocumentInSubject($request->all());
    }

    public function deleteDocument(Request $request, $id)
    {
        return $this->service->deleteFile($id);
    }

    public function updateDocument(Request $request, $id)
    {
        return $this->service->updateDocument($request, $id);
    }

    public function addSubject(AddSubjectInProgramRequest $request)
    {
        return $this->service->addSubject($request->all());
    }

    public function deleteSubjectFromProgram($profession_program_id, $subject_id)
    {
        return $this->service->deleteSubjectFromProgram(['profession_program_id' => $profession_program_id, 'subject_id' => $subject_id]);
    }

    public function deleteSubject(Request $request, $profession_program_id, $subject_id)
    {
        return response()->json(
            $this->service->deleteSubject($profession_program_id, $subject_id)
        );
    }

    public function updateBasicDocument(Request $request, $id)
    {
        return response()->json($this->service->updateBasic($id, $request->file('file'), $request->type));
    }

    public function addAdditionalDocument(Request $request, $program_id)
    {
        return response()->json($this->service->addAdditionalDocument($program_id, $request));
    }

    public function updateProgramSubject(Request $request, $id)
    {
        try {
            $model = ProgramSubject::findOrFail($id);

            $model->update($request->all());
            return [
                'success' => true,
                'message' => 'Индекс был успешно обновлен'
            ];
        } catch (\Exception $exception) {

        }
    }

    public function getSubjectFiles($program_id, $subject_id)
    {
        return $this->service->getSubjectFilesById($program_id, $subject_id);
    }

    public function buildFromComponents(Request $request, $id)
    {
        try {
            $model = ProgramDocument::query()->with('programtagle')->findOrFail($id);

            $documentHandle = (new ProgramFiles($model))->getComponentsPaths();

            $newFile = PDF::mergeFiles($documentHandle);

            $newPath = (new ProfessionProgramStorage())->putRawFile($newFile, $model->programtagle->profession_program_id, $model->path);

            $model->update(['path' => $newPath]);

            return [
                'success' => true,
                'path' => StorageFiles::getProgramDocument($newPath)
            ];

        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function createComponent(Request $request, $id)
    {
        try {
            $documentModel = ProgramDocument::query()->findOrFail($id);
            $componentPath = (new ComponentDocumentStorage())->putFile($request->file('file'));

            $component = new ComponentDocument([
                'name' => $request->name,
                'sort' => $request->sort,
                'path' => $componentPath
            ]);

            $documentModel->components()->save($component);
            $formatComponent = (new FileComponent($component))->full();

            return ['success' => true, 'component' => $formatComponent];

        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function reloadFile(Request $request, $profession_program_id, $id)
    {
        return $this->service->updateDocument($request, $id, $profession_program_id);
    }
}
