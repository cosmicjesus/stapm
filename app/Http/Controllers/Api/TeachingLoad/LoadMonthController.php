<?php

namespace App\Http\Controllers\Api\TeachingLoad;

use App\Models\LoadMonth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoadMonthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {

            $months = LoadMonth::query()
                ->where('academic_year', $request->input('year'))
                ->get()->map(function (LoadMonth $month) {
                    return [
                        'name' => $month->name,
                        'start_date' => $month->start_date->format('Y-m-d'),
                        'end_date' => $month->end_date->format('Y-m-d'),
                        'semester' => $month->semester
                    ];
                });

            return responder()->success(['success' => true, 'months' => $months])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $year = $request->input('academic_year');


            foreach ($request->input('months') as $month) {
                $params = array_merge($month, ['academic_year' => $year]);
                LoadMonth::query()->create($params);
            }

            return responder()->success(['success' => true])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
