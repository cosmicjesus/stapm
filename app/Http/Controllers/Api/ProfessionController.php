<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProfessionRequest;
use App\Models\Profession;
use App\Models\ProfessionProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $limit = $request->input('limit');

            $professions = Profession::query()
                ->orderBy('code')
                ->paginate($limit);

            return response()->json($professions);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    public function all()
    {
        try {

            $professions = Profession::query()
                ->orderBy('code')
                ->get();

            return response()->json($professions);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessionRequest $request)
    {
        try {

            $profession = new Profession();
            $profession->code = $request->input('code');
            $profession->name = $request->input('name');
            $profession->save();

            return response()->json(
                [
                    'message' => 'Профессия была добавлена'
                ]
            );
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $countExistProfession = Profession::query()
                ->where('code', $request->code)
                ->where('name', $request->name)
                ->whereKeyNot($id)
                ->count();

            if ($countExistProfession) {
                return response()->json([
                    'success' => false,
                    'message' => 'Такая профессия уже существует в базе'
                    //'message'=>$countExistProfession
                ])->setStatusCode(200);
            }
            Profession::query()->findOrFail($id)
                ->update([
                    'code' => $request->code,
                    'name' => $request->name
                ]);

            return response()->json([
                'success' => true,
                'message' => 'Профессия была успешно обновлена'
                //'message'=>$countExistProfession
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $programsCount = ProfessionProgram::query()->where('profession_id', $id)->count();

            if ($programsCount) {
                return response()->json([
                    'success' => false,
                    'message' => 'Невозможно удалить профессию, так как имеются профессиональные программы по этой профессии'
                ])->setStatusCode(200);
            }
            Profession::destroy($id);
            return response()->json([
                'success' => true,
                'message' => 'Профессия была успешно удалена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}
