<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EmployeeService;
use App\Http\Requests\AddSubjectToEmployee;
use App\Http\Controllers\Controller;
use App\Models\EmployeeSubject;

class EmployeeSubjectController extends Controller
{

    protected $service;

    public function __construct()
    {
        $this->service = new EmployeeService();
    }

    public function addSubject(AddSubjectToEmployee $request, $employee_id)
    {
        return $this->service->attachSubject($employee_id, $request->all());
    }

    public function deleteSubject($id)
    {
        try {
            EmployeeSubject::destroy($id);

            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно удалена'
            ]);
        } catch (\Exception $exception) {
            response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
