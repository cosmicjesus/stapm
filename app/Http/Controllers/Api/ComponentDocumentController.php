<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\FileComponent\FileComponent;
use App\AppLogic\Storage\ComponentDocumentStorage;
use App\Models\ComponentDocument;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentDocumentController extends Controller
{

    protected $storage;

    public function __construct()
    {
        $this->storage = new ComponentDocumentStorage();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $model = ComponentDocument::query()->findOrFail($id);
            $model->update($request->all());
            return response()->json(
                [
                    'success' => true,
                    'updated_at' => $model->updated_at->format('Y-m-d H:i:s')
                ]
            );
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => "Не найден компонент с ID {$id}"
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            $model = ComponentDocument::query()->findOrFail($id);
            $path = $model->path;

            $model->delete();

            \Storage::disk('component_documents')->delete($path);

            return response()->json([
                'success' => true
            ]);
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => "Не найден компонент с ID {$id}"
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function reloadFile(Request $request, $id)
    {
        try {
            $model = ComponentDocument::query()->findOrFail($id);
            $path = $this->storage->putFile($request->file('file'), $model->path);

            $model->update(['path' => $path]);

            return [
                'success' => true,
                'component' => (new FileComponent($model))->full()
            ];

        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => "Не найден компонент с ID {$id}"
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
