<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\RecruitmentCenter\RecruitmentCenterService;
use App\Http\Requests\RecruitmentCenterRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecruitmentCenterController extends Controller
{
    protected $service;

    public function __construct(RecruitmentCenterService $recruitmentCenterService)
    {
        $this->service = $recruitmentCenterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->service->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RecruitmentCenterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecruitmentCenterRequest $request)
    {
        return $this->service->create($request->input());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param RecruitmentCenterRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecruitmentCenterRequest $request, $id)
    {
        return $this->service->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
