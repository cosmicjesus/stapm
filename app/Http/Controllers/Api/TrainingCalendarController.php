<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\TrainingCalendarService;
use App\Http\Requests\CreateTrainingCalendarRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainingCalendarController extends Controller
{

    protected $service;

    public function __construct(TrainingCalendarService $service)
    {
        $this->service = $service;
    }

    public function all()
    {
        return $this->service->all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->getByYear($request->year);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTrainingCalendarRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTrainingCalendarRequest $request)
    {
        return $this->service->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
