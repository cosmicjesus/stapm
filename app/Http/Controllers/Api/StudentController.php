<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\ReferenceService;
use App\AppLogic\Services\StudentService;
use App\AppLogic\Storage\StudentStorage;
use App\Http\Requests\AddStudentDecreeRequest;
use App\Http\Requests\AddStudentFileRequest;
use App\Http\Requests\EditStudentPersonalInfo;
use App\Http\Requests\RetireStudentRequest;
use App\Http\Requests\TransferStudentRequest;
use App\Http\Requests\UpdateEducationRequest;
use App\Http\Requests\UpdatePassportRequest;
use App\Models\Activity;
use App\Models\Student;
use App\Models\StudentFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class StudentController extends Controller
{


    protected $service;

    public function __construct(StudentService $studentService)
    {
        try {
            $this->service = $studentService;
        } catch (\Exception $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->filter($request->filter)->paginate($request->limit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->service->createStudent($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        try {
            $student = $this->service->getById($id);
            \activity('student')
                ->tap(function (Activity $activity) {
                    $activity->event = 'show_profile';
                })
                ->log("Просмотр профиля студента {$student->getFullName()}");
            return $student->full();
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($id, $request->all());
    }


    public function updatePersonal(EditStudentPersonalInfo $request, $id)
    {
        return $this->service->updatePersonal($id, $request->all());
    }

    public function updatePassport(UpdatePassportRequest $request, $id)
    {
        return $this->service->updatePassport($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkUnique(Request $request)
    {
        try {
            $isHasUser = Student::checkUnique($request->all());

            if ($isHasUser) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Пользователь с такими данными уже существует'
                ], 422);
            }

            return new JsonResponse([
                'success' => true,
            ], JsonResponse::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function addDecree(AddStudentDecreeRequest $request, $id)
    {
        return $this->service->addDecree($id, $request->all());
    }

    public function updateAddresses(Request $request, $id)
    {
        return $this->service->updateAddresses($id, $request->all());
    }

    public function updateEducation(UpdateEducationRequest $request, $id)
    {
        return $this->service->updateEducation($id, $request->all());
    }

    public function retireStudents(RetireStudentRequest $request)
    {
        return $this->service->retireStudents($request->all());
    }

    public function transferStudents(TransferStudentRequest $request)
    {
        return $this->service->transferStudents($request->all());
    }

    public function reinstateStudents(Request $request)
    {
        return $this->service->reinstateStudents($request->all());
    }

    public function uploadPhoto(Request $request, $id)
    {
        try {
            $student = Student::query()->findOrFail($id);

            $student->uploadImage($request->file('file'));
//            $newPath = (new StudentStorage())->putFoto($id, $request->file('file'), $student->photo);
//
//            $student->update(['photo' => $newPath]);

            \activity($student->status)
                ->on($student)
                ->tap(function ($event) {
                    $event->event = 'updated';
                })
                ->log("Обновление фотографии у {$student->full_name}");
            return [
                'success' => true,
                'photo' => (new StudentStorage())->getPhoto($student->photo)
            ];

        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Не найден студент с таким ID'
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function deletePhoto($id)
    {
        try {
            $student = Student::query()->findOrFail($id);
            $path = $student->photo;
            $student->update(['photo' => null]);
            \activity($student->status)
                ->on($student)
                ->tap(function ($event) {
                    $event->event = 'deleted';
                })
                ->log("Удаление фотографии у {$student->full_name}");
            (new StudentStorage())->deleteFile($path);

            return [
                'success' => true,
            ];

        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Не найден студент с таким ID'
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function addFile(AddStudentFileRequest $request, $id)
    {
        try {
            $student = Student::query()->with('files')->findOrFail($id);

            $path = (new StudentStorage())->putFile($id, $request->file('file'));

            $file = new StudentFile([
                'name' => $request->name,
                'path' => $path
            ]);

            $student->files()->save($file);

            return [
                'success' => true,
                'file' => (new \App\AppLogic\Student\StudentFile($file))->build()
            ];

        } catch (AccessDeniedHttpException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'У вас нет прав на выполнение данного действия.'
            ])->setStatusCode(403);
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Не найден студент с таким ID'
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function deleteFile($id)
    {
        try {
            $model = StudentFile::query()->findOrFail($id);
            $path = $model->path;
            (new StudentStorage())->deleteFile($path);

            $model->delete();

            return [
                'success' => true
            ];
        } catch (\Exception $exception) {

        }
    }

    public function makeTrainingReference(Request $request, $id)
    {
        return (new ReferenceService())->makeTrainingReferenceByStudentID($id, $request->all());
    }

    public function massPrintMilitaryReferences(Request $request)
    {
        return $this->service->massPrintMilitaryReferences($request->recruit_center_id);
    }

    public function profileToPdf(Request $request)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return $this->service->makeProfile($request->id);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function exportProfileToAsuRso(Request $request, $id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return (new StudentService())->exportAsuRsoTableByStudentIds($id);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
