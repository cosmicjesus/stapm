<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\User\User;
use App\Models\Activity;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('login', 'password');

        try {

            if (!$token = JWTAuth::attempt(array_merge($credentials, ['banned' => false]))) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Нет пользователя с такими данными. Пожалуйста, проверьте введенные данные'
                ], 401);
            }
        } catch (JWTException $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'Произошла серверная ошибка при авторизации'
            ], 500);
        }

        $user = \Auth::user();
        if ($user->profile_type == Student::class) {
            return response()->json([
                'status' => 'error',
                'message' => 'Нет пользователя с такими данными. Пожалуйста, проверьте введенные данные'
            ], 401);
        }
//        activity()
//            ->tap(function(Activity $activity) {
//                $activity->event = 'login';
//            })
//            ->log('Вошел в систему');
        return response()->json([
            'status' => 'success',
            'token' => $token

        ])->header('Authorization', $token);
    }

    public function profile(Request $request)
    {


        try {
            $authUser = \Auth::user();
            if ($authUser->banned) {
                throw new JWTException();
            }
            $user = new User($authUser);
            return response()->json(
                [
                    'roles' => $user->getPermissions(),
                    'token' => 'admin',
                    'introduction' => \Auth::user(),
                    'avatar' => $user->getPhoto(),
                    'name' => $user->getName()
                ]
            );
        } catch (JWTException $exception) {
            return response()->json([
                'status' => 'error',
                'message' => 'Не авторизован'
            ], 401);
        }
    }

    public function logout(Request $request)
    {
        try {
            auth()->logout();
            return response()->json(['message' => 'Successfully logged out']);
        } catch (JWTException $exception) {
            return response()->json([
                'status' => 'error',
                'message' => 'Не авторизован'
            ], 401);
        }
    }
}
