<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Decree\Decree;
use App\AppLogic\Services\ReferenceService;
use App\AppLogic\Student\Student;
use App\Models\Help;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class ReferencesController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ReferenceService();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function training(Request $request)
    {
        try {
            $student = Student::buildByID($request->student_id);
            $data = [
                'params' => $request->input()
            ];
            if (isset($request->reference_id)) {
                $reference = Help::query()->findOrFail($request->reference_id);
                $data['number'] = $reference->number;
                $reference->status = Help::status[0];
                $reference->save();
            } else {
                $number = Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->max('number');
                $data['number'] = $number + 1;
                $this->createReference($data);
            }

            $decree = Decree::buildById($request->decree_id);

            $repeat = isset($request->count) && ($request->count > 2) ? $request->count : 2;
            $pdf = PDF::loadView('references.training',
                [
                    'repeat' => $repeat,
                    'student' => $student,
                    'data' => $data,
                    'decree' => $decree,
                    'withStyle' => true
                ]
            );

            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function createReference($params)
    {
        try {
            $model = new Help();
            $model->student_id = $params['params']['student_id'];
            $model->number = $params['number'];
            $model->params = $params['params'];
            $model->date_of_issue = Carbon::now()->format('Y-m-d H:i:s');
            $model->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $model->type = Help::types['training'];
            $model->status = Help::status[0];
            $model->save();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function referencesList(Request $request)
    {
        return $this->service->filter($request->filter)->paginate();
    }

    public function prinyMoneyList(Request $request)
    {
        return $this->service->makeMoneyReferenceList($request->all());
    }

    public function makeMilitaryReference(Request $request, $id)
    {
        return $this->service->makeMilitaryReferenceByStudentId($id, $request->all());
    }

    public function voenkomat(Request $request, $id)
    {

        try {

            $student = Student::buildByID($id);
            $decree = Decree::buildById($request->decree_id);

            $pdf = PDF::loadView('references.voenkomat',
                [
                    'withStyle' => true,
                    'student' => $student,
                    'decree' => $decree,
                    'input' => $request->all()
                ]
            );

            if (!is_null($request->ref_id)) {
                Help::query()->findOrFail($request->ref_id)->update([
                    'status' => 'issued',
                    'date_of_issue' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
