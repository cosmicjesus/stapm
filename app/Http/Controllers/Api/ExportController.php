<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\ActivityLog\ActivityLogService;
use App\AppLogic\Employee\Employee;
use App\AppLogic\Services\ExportService;
use App\AppLogic\Services\StudentService;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\Student;
use App\Models\StudentFile;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use JWTAuth;
use League\Flysystem\FileNotFoundException;
use PDF;

class ExportController extends Controller
{

    protected $studentService;
    protected $exportService;

    public function __construct(StudentService $studentService, ExportService $exportService)
    {
        try {
            $this->studentService = $studentService;
            $this->exportService = $exportService;
        } catch (\Exception $e) {
        }
    }


    public function base(Request $request, $type)
    {
        try {
            $students = [];
            if (isset($request->ids)) {
                $students = $this->studentService->getByIds($request->ids);
            } else {
                $students = $this->studentService->getOnlyTeachStudents();
            }
            /**
             * @var $students Collection
             */
            $students->transform(function ($item, $key) use ($type) {
                return $this->buildBaseList($item, $key, $type);
            })->all();

            $baseType = $type == 'full' ? 'полной' : 'банковской';
            ActivityLogService::writeExportActivity("Выгрузка {$baseType} базы");

            return new JsonResponse($students, 200);
        } catch (\Exception $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine()
            ], 500);
        }

    }

    protected function buildBaseList($item, $key, $type)
    {
        try {
            /**
             * @var $item \App\AppLogic\Student\Student
             */
            $data = [
                'iteration' => $key + 1,
                'lastname' => $item->getLastName(),
                'firstname' => $item->getFirstname(),
                'middlename' => $item->getMiddlename(),
                'gender' => getGender($item->getGender()),
                'birthday' => dateFormat($item->getBirthday()),
                'nationality' => getNationality($item->getNationality()),
                'index' => $item->getAddress('registration', 'index'),
                'region' => $item->getAddress('registration', 'region'),
                'area' => $item->getAddress('registration', 'area'),
                'settlement' => $item->getAddress('registration', 'settlement'),
                'city' => $item->getAddress('registration', 'city'),
                'street' => $item->getAddress('registration', 'street'),
                'house_number' => $item->getAddress('registration', 'house_number'),
                'housing' => $item->getAddress('registration', 'housing'),
                'apartment_number' => $item->getAddress('registration', 'apartment_number'),
                'document_type' => $item->getPassportData('documentType'),
                'document_series' => $item->getPassportData('series'),
                'document_number' => $item->getPassportData('number'),
                'document_issuanceDate' => dateFormat($item->getPassportData('issuanceDate')),
                'document_issued' => $item->getPassportData('issued'),
                'document_subdivisionCode' => $item->getPassportData('subdivisionCode'),
                'birthPlace' => $item->getPassportData('birthPlace'),
                'country' => getNationality($item->getNationality()),
                'phone' => $item->getPhone(),
                'inn' => $item->getInn(),
                'snils' => $item->getSnils(),
                'email' => $item->getEmail(),
                'home_phone' => '',
                'work_phone' => ''
            ];

            if ($type == 'bank') {
                $data['organization_code'] = env('ORGANIZATION_CODE', 547);
                $data['control_information'] = '';
                $data['comment'] = '';
                $data['latin_name'] = '';
                $data['latin_lastname'] = '';
                $data['document_subdivisionCode2'] = $item->getPassportData('subdivisionCode');
            } elseif ($type == 'full') {
                $categories = $item->getPrivilegedCategory();
                $data['profession'] = $item->getTrainingGroup()->getName();
                $data['average_score'] = $item->getAverageScore();
                $data['graduation_organization_name'] = $item->getGraduationOrganizationName();
                $data['graduation_date'] = dateFormat($item->getGraduationDate());
                $data['count_class'] = 9;
                $data['language'] = getLanguage($item->getLanguage());
                $data['privileget_category'] = count($categories) == 0 ? 'нет льгот' : getPrivilegeCategory($categories[0]);
                $data['need_hostel'] = $item->getNeedHostelStatus() ? 'Нуждается' : 'Не нуждается';
            }
            return $data;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    public function recruits()
    {
        try {
            $students = Student::query()
                ->whereHas('program', function ($q) {
                    $q->where('education_form_id', 1);
                })
                ->where('status', 'student')
                ->where('gender', 0)
                ->orderBy('lastname')
                ->orderBy('firstname')
                ->get()
                ->map(function ($student, $key) {
                    $item = new \App\AppLogic\Student\Student($student);

                    return [
                        'iteration' => $key + 1,
                        'lastname' => $item->getLastName(),
                        'firstname' => $item->getFirstname(),
                        'middlename' => $item->getMiddlename(),
                        'group' => $item->getTrainingGroup()->getName(),
                        'birthday' => dateFormat($item->getBirthday()),
                        'age' => $item->getAge(),
                        'address_registration' => $item->buildAddress('registration'),
                        'military_recruitment_office' => ($item->getRecruitmentCenter())['name'],
                        'military_document_type' => militaryDocumentType($item->getMilitaryDocumentType()),
                        'military_document_number' => $item->getMilitaryDocumentNumber(),
                        'fitness_for_military_service' => fitnessForMilitaryServices($item->getFitnessForMilitaryService()),
                        'military_specialty' => $item->getMilitarySpecialty(),

                    ];
                });

            ActivityLogService::writeExportActivity('Выгрузка базы призыников');
            return $students;
        } catch (\Exception $exception) {
            return response()->json([
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function getStudentProfileToAsuRso(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $student = Student::onlyTeach()->findOrFail($id);
            ActivityLogService::writeExportActivity("Выгрузка профиля студента {$student->lastname} {$student->firstname} {$student->middlename} ID:{$student->id}", 'student');
            DB::commit();
            return [$this->buildListForAsuRso($student)];
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'errorCode' => 404,
                'exception' => 'ModelNotFoundException',
                'message' => 'Не найден студент с таким идентификатором'
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }
    }

    protected function buildListForAsuRso(Student $studentModel)
    {
        $student = new \App\AppLogic\Student\Student($studentModel);
        $enrollmentDecree = ($student->getDecrees())->last();
        $document = $student->getPassport();
        return [
            'lastname' => $student->getLastname(),
            'firstname' => $student->getFirstname(),
            'middlename' => $student->getMiddlename(),
            'birthday' => dateFormat($student->getBirthday()),
            'gender' => gender($student->getGender()),
            'snils' => $student->getSnils() ?? '',
            'place_of_stay' => $student->buildAddress('place_of_stay'),
            'financy' => $student->isContractTargetSetStatus() ? 'За счет бюджета субъекта РФ по договору о целевом обучении' : 'За счет бюджета субъекта РФ',
            'group' => $student->getGroup()['code'],
            'decree_number' => $enrollmentDecree['decree']['number'],
            'decree_date' => dateFormat($enrollmentDecree['decree']['date']),
            'decree_start' => dateFormat($enrollmentDecree['decree']['date']),
            'reason' => reason($enrollmentDecree['type'], $enrollmentDecree['reason']),
            'nationality' => nationality($student->getNationality()),
            'health_category' => '',
            'disability' => '',
            'health_group' => '',
            'sport_group' => 'Основная',
            'need_adaptive_program' => 'Нет',
            'need_long_med' => 'Нет',
            'education' => educationLevel($student->getEducationId()),
            'graduation_date' => dateFormat($student->getGraduationDate()),
            'end_spec_organization' => 'Нет',
            'international_treaty' => 'Нет',
            'average_ball' => str_replace(',', '.', round($student->getAvg(), 2)),
            'privelige_category' => '',
            'phone' => $student->getPhone(),
            'email' => '',
            'comment' => $student->getInn() ?? '',
            'residential' => $student->buildAddress('residential'),
            'documentType' => documentType($document['documentType']) ?? '',
            'documentSeries' => $document['series'] ?? '',
            'documentNumber' => $document['number'] ?? '',
            'documentIssuanceDate' => dateFormat($document['issuanceDate']) ?? '',
            'documentIssued' => $document['issued'] ?? '',
            'documentSubdivisionCode' => $document['subdivisionCode'] ?? '',
            'documentBirthPlace' => $document['birthPlace'] ?? '',
            'registration' => $student->buildAddress('registration'),
            'military_rank' => $student->getGender() == 0 ? 'Подлежит призыву' : '',
            'military_spec' => '',
            'militaty_fit_category' => '',
            'military_group' => '',
            'zapas' => '',
            'military_document_number' => '',
            'sostav' => '',
            'voenkomat' => ($student->getRecruitmentCenter())['name'] ?? '',
            'military_prepare' => 'Нет',
            'scep_u' => 'Нет'
        ];
    }

    public function exportT2Form($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();

            return $this->exportService->makeT2ByStudentId($id);
        } catch (FileNotFoundException $exception) {
            return $exception->getMessage();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }


    public function exportStatement($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            $wordFile = $this->exportService->makeStatementOnEnrollmentByEntrantId($id);
            return response()->download(($wordFile['file']->save()), $wordFile['filename']);
        } catch (FileNotFoundException $exception) {
            return $exception->getMessage();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    protected function getMilitarian()
    {
        $model = \App\Models\Employee::query()
            ->with('recruitmentCenter')
            ->where('status', 'work')
            ->where(function ($query) {
                $query->where('gender', 0)
                    ->orWhereNotNull('military_status');
            })
            ->whereHas('positions', function ($q) {
                $q->whereIn('type', [0, 1]);
            })
            ->orderBy('lastname')
            ->get();

        return $model;

    }

    protected function makeMilitaryServicePdf($data, $title = 'Список военнообязанных')
    {

        $pdf = PDF::loadView('reports.military-service',
            [
                'title' => $title,
                'employees' => $data
            ],
            [],
            [
                'format' => 'A4-L'
            ]
        );

        return $pdf->stream($title);
    }

    public function listForMilitaryService()
    {
        try {
            JWTAuth::parseToken()->authenticate();

            $data = $this->getMilitarian();

            // dd($data);

            return $this->makeMilitaryServicePdf($data);
        } catch (FileNotFoundException $exception) {
            return $exception->getMessage();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
