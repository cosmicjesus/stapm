<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\ProfessionProgramService;
use App\Http\Requests\CreateProfessionProgramRequest;
use App\Http\Requests\UpdateProfessionProgramRequest;
use App\Models\ProfessionProgram;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfessionProgramController extends Controller
{


    protected $service;


    public function __construct(ProfessionProgramService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $programs = $this->service->filter($request->filter)->paginate($request->limit);
            return response()->json($programs);
        } catch (\Exception $exception) {
            return response()->json([
                'line' => $exception->getLine(),
                'trace' => $exception->getTrace(),
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProfessionProgramRequest $request)
    {

        $program = $this->service->create($request->all());

        return $program;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return response()->json($this->service->getById($id));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfessionProgramRequest $request, $id)
    {
        return $this->service->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all()
    {
        try {
            $programs = ProfessionProgram::query()->with('profession')->get()
                ->sortBy('profession.code')
                ->map(function ($program) {
                    return ['id' => $program->id, 'name' => $program->name_with_form, 'duration' => $program->trainin_period, 'reduction' => $program->reduction];
                });

            return response()->json($programs);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function subjectByYear(Request $request, $profession_program_id, $academic_year_id)
    {
        return $this->service->getSubjectByYear($profession_program_id, $academic_year_id);
    }


    public function putImage(Request $request, $id)
    {
        return $this->service->update($id, $request->all());
    }
}
