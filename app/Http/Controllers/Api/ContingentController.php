<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\CacheService;
use App\Exceptions\NonEditableObjectException;
use App\Models\Entrant;
use App\Models\Student;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContingentController extends Controller
{
    protected $cacheService;

    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    protected function getUserStatus(int $id)
    {
        $user = Student::query()->select('status')->findOrFail($id);

        return $user->status;
    }

    public function updateData(Request $request, $id)
    {

        DB::beginTransaction();
        try {
            $status = $this->getUserStatus($id);

            $userModel = null;

            if ($status == Entrant::$entrantStatus) {
                $userModel = Entrant::class;
            } elseif ($status == Student::$expelledStatus) {
                throw new NonEditableObjectException();
            } else {
                $userModel = Student::class;
            }


            $model = \App::make($userModel);
            /** @var Student $model */
            $user = $model->query()->findOrFail($id);
            $user->update($request->all());
            if (isset($request->passport_data)) {
                $this->cacheService->putSubdivisionCodesCache([
                    'code' => $request->passport_data['subdivisionCode'],
                    'issued' => $request->passport_data['issued']
                ]);
            }
            DB::commit();
            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        } catch (NonEditableObjectException $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'errors' => ["Нельзя отредактировать данные этого человека, т.к он является отчисленным"]
            ])->setStatusCode(422);
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'errors' => ["Не найден человек с ID {$id}"]
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}
