<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Requests\Employee\CreatePreviousJobRequest;
use App\Models\EmployeePreviousPosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeePreviousJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(CreatePreviousJobRequest $request)
    {
        try {
            $model = EmployeePreviousPosition::create($request->input());

            return responder()->success(['id' => $model->id])->respond();
        } catch (\Exception $exception) {
            return responder()->error(500, $exception->getMessage())->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
