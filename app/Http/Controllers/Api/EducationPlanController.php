<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Pdf\PDF;
use App\AppLogic\Services\EducationPlanService;
use App\Http\Requests\CreateEducationPlanRequest;
use App\Http\Requests\UpdateEducationPlanRequest;
use App\Models\EducationPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EducationPlanController extends Controller
{

    protected $service;

    public function __construct(EducationPlanService $educationPlanService)
    {
        try {
            $this->service = $educationPlanService;
        } catch (\Exception $exception) {

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $plans = $this->service->filter($request->filter)->paginate($request->limit);
            return response()->json($plans);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getFile()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEducationPlanRequest $request)
    {

        //return response()->json($request->file('file'));
        return response()->json($this->service->create($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->getOne($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEducationPlanRequest $request, $id)
    {
        return $this->service->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->service->delete($id));
    }

    public function all()
    {
        return $this->service->all();
    }

    public function refreshFile(Request $request, $id)
    {
        return response()->json($this->service->refreshFile($id, $request->all(), $request->file('file')));
    }

    public function createComponent(Request $request, $id)
    {
        return $this->service->createComponent($request->all());
    }

    public function getPlansByProgramId(Request $request, $profession_program_id)
    {
        return $this->service->getPlansByProgramId($profession_program_id);
    }

    public function build(Request $request, $id)
    {
        return $this->service->buildFile($id);
    }
}
