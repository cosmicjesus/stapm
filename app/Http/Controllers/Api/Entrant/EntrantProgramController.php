<?php

namespace App\Http\Controllers\Api\Entrant;

use App\Exceptions\DuplicateEntryException;
use App\Http\Controllers\Controller;
use App\Models\RecruitmentProgramEnrollee;
use Illuminate\Http\Request;

class EntrantProgramController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $entrant_id)
    {
        try {
            $isHasProgram = RecruitmentProgramEnrollee::query()
                ->where('recruitment_program_id', $request->input('recruitment_program_id'))
                ->where('enrollee_id', $entrant_id)
                ->first();
            if ($isHasProgram) {
                throw new DuplicateEntryException();
            }
            $program = RecruitmentProgramEnrollee::query()
                ->create(
                    array_merge(
                        $request->input(),
                        ['enrollee_id' => $entrant_id]
                    )
                );
            return responder()->success(['id' => $program->id])->respond();
        } catch (DuplicateEntryException $duplicateEntryException) {
            return responder()->error(422, 'Такая программа уже существует в списке программ абитуриента')->respond(422);
        } catch (\Exception $exception) {
            return responder()->error(500, $exception->getMessage())->respond();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $entrant_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $entrant_id, int $id)
    {
        try {
            $program = RecruitmentProgramEnrollee::query()->findOrFail($id);

            $priorityProgram = RecruitmentProgramEnrollee::query()
                ->where('enrollee_id', $entrant_id)
                ->where('is_priority', true)
                ->first();

            if ($priorityProgram && $request->input('is_priority')) {
                if ($priorityProgram->id !== $id) {
                    throw new DuplicateEntryException();
                }
            }

            $program->update($request->input());

            return responder()->success($request->input())->respond();
        } catch (DuplicateEntryException $entryException) {
            return responder()->error(422, 'У абитуриента уже выбрана приоритетная программа')->respond(422);
        } catch (\Exception $exception) {
            return responder()->error(500, $exception->getMessage())->respond();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $entrant_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($entrant_id, $id)
    {
        try {
            RecruitmentProgramEnrollee::destroy($id);

            return responder()->success()->respond();
        } catch (\Exception $exception) {

        }
    }
}
