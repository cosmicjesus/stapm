<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreatePositionRequest;
use App\Http\Requests\UpdatePositionRequest;
use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * @var $type
         * @var $limit
         * @var $name
         */
        try {
            extract($request->input(), EXTR_OVERWRITE);

            $model = Position::query();

            $model->when(filter_var($type, FILTER_VALIDATE_INT), function ($q) use ($type) {
                return $q->where('position_type_id', $type);
            });

            $positions = $model->orderBy('name')->paginate($limit);

            $total = $positions->total();

            $data = $positions->map(function ($position) {
                return (new \App\AppLogic\Position\Position($position))->buildOnResponse();
            });

            return response()->json([
                'total' => $total,
                'positions' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePositionRequest $request)
    {
        try {
            Position::query()
                ->create([
                    'name' => $request->name,
                    'position_type_id' => $request->type
                ]);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Должность была успешно добавлена'
                ]
            );
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePositionRequest $request, $id)
    {
        try {

            $position = Position::query()->findOrFail($id);
            $position->update($request->all());
            return response()->json([
                'success' => true,
                'message' => 'Должность была успешно обновлена'
            ]);

        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $position = Position::query()->with('employee_position')->findOrFail($id);

            $countEmployees = $position->employee_position->count();

            if ($countEmployees) {
                return response()->json([
                    'success' => false,
                    'message' => 'Удаление невозможно, т.к имеются сотрудники назначенные на эту должность'
                ]);
            }

            $position->delete();

            return response()->json([
                'success' => true,
                'message' => 'Должность была успешно удалена'
            ]);

        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    public function all()
    {
        try {
            $positions = Position::query()
                ->orderBy('name')
                ->get()
                ->map(function ($position) {
                    return ['id' => $position->id, 'name' => $position->name];
                });
            return response()->json($positions);

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine()
            ])->setStatusCode(500);
        }
    }
}
