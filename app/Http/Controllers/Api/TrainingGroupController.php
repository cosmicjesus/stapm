<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\TrainingGroupService;
use App\Http\Controllers\BaseController;
use App\Http\Requests\TrainingGroup\CreateTrainingGroupRequest;
use App\Models\TrainingGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class TrainingGroupController extends BaseController
{

    protected $service;

    public function __construct(TrainingGroupService $groupService)
    {
        $this->service = $groupService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->service->filter($request->filter)->paginate($request->limit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTrainingGroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTrainingGroupRequest $request)
    {
        return $this->service->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function graduation(Request $request)
    {
        return $this->service->graduation($request->all());
    }

    public function getTransferMap()
    {
        return $this->service->getTransferMap();
    }

    public function transferNextTerm(Request $request)
    {
        return $this->service->transferNextTerm($request->all());
    }

    public function transferNextYear(Request $request)
    {
        return $this->service->transferNextYear($request->all());
    }

    public function exportStudentsProfiles($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return $this->service->exportStudentsProfiles($id);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function exportStudentsToAsurso($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return $this->service->exportStudentsToAsuRso($id);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function exportStudentsParentsToAsuRso($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return $this->service->exportStudentsParentsToAsuRso($id);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function exportStudentsPhotos($id)
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return $this->service->exportStudentsPhotos($id);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function massPrintReferences($id)
    {
        return $this->service->massPrintReferences($id);
    }

    public function all()
    {
        try {
            $groups = TrainingGroup::query()
                ->onlyTeach()
                ->get()
                ->map(function ($group) {
                    return (new \App\AppLogic\TrainingGroup\TrainingGroup($group))->buildToSelect();
                });

            return new JsonResponse($groups, 200);
        } catch (\Exception $exception) {

        }
    }
}
