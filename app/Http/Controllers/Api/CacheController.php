<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\CacheService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CacheController extends Controller
{
    protected $service;

    public function __construct(CacheService $cacheService)
    {
        $this->service = $cacheService;
    }

    public function all()
    {
        return $this->service->all();
    }

    public function getRawSubdivision()
    {
        return $this->service->getSubdivisionCodes();
    }

    public function saveRawSubdivision(Request $request)
    {
        return $this->service->updateSubdivisionCodes($request->codes);
    }

    public function subdivision()
    {
        return new JsonResponse($this->service->getFormatSubdivisionCodes(), 200);
    }

    public function birthPlaces()
    {
        return new JsonResponse($this->service->getBirthPlaces(), 200);
    }

    public function educationalOrganizations()
    {
        return new JsonResponse($this->service->getEducationalOrganizations(), 200);
    }

    public function educationalInstitutions()
    {
        return new JsonResponse($this->service->getEducationalInstitutions(), 200);
    }

    public function personalFormData()
    {
        try {
            return $this->service->getPersonalFormData();
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Произошла ошибка при получении списка имен и отчеств. Автозаполнение недоступно',
                'error_message' => $exception->getMessage()])->setStatusCode(500);
        }
    }

    public function getAddressesData()
    {
        try {
            return new JsonResponse($this->service->getAddressesDataCache(), 200);
        } catch (\Exception $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ], 500);
        }
    }
}
