<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\SelectionCommitteeService;
use App\Http\Requests\CreateCommitteeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SelectionCommitteeController extends Controller
{


    protected $service;

    public function __construct(SelectionCommitteeService $selectionCommitteeService)
    {
        try {
            $this->service = $selectionCommitteeService;
        } catch (\Exception $e) {
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->service->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCommitteeRequest $request)
    {
        return $this->service->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return [1, 1, 1, 1, 1];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPrograms(Request $request, $id)
    {
        return $this->service->getProgramsByCommitteeId($id);
    }

    public function onlyActive()
    {
        return $this->service->getActiveCommitteeId();
    }

    public function activePrograms()
    {
        return $this->service->getProgramsByCommitteeId();
    }

    public function buildReportOfManning(Request $request)
    {
        return $this->service->buildReportOfManning($request->all());
    }
}
