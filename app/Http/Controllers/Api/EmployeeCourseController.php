<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EmployeeService;
use App\Http\Requests\AddCourseRequest;
use App\Http\Controllers\Controller;
use App\Models\QualificationCourse;

class EmployeeCourseController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new EmployeeService();
    }

    public function addCourse(AddCourseRequest $request, $employee_id)
    {
        return $this->service->addCourse($employee_id, $request->all());
    }

    public function deleteCourse($id)
    {
        try {
            QualificationCourse::destroy($id);

            return response()->json([
                'success' => true,
                'message' => 'Курсы были успешно удалены'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
