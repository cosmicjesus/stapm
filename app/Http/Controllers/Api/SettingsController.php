<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SettingsController extends Controller
{
    public function getSettings()
    {
        $this->authorize('siteSettings', 'God');

        try {
            return setting()->all();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function updateSettings(Request $request)
    {
        $this->authorize('siteSettings', 'God');
        try {
            $settings = $request->settings;

            foreach ($settings as $key => $setting) {
                setting([$key => $setting])->save();
            }

            return [
                'success' => true
            ];

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}
