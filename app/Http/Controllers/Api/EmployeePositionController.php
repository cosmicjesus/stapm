<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EmployeeService;
use App\Http\Requests\AddPositionToEmployee;
use App\Http\Requests\UpdateEmployeePositionRequest;
use App\Models\EmployeePosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeePositionController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new EmployeeService();
    }

    public function addPosition(AddPositionToEmployee $request, $id)
    {
        return $this->service->attachPosition($id, $request->all());
    }

    public function updatePosition(UpdateEmployeePositionRequest $request, $id)
    {
        try {

            $model = EmployeePosition::query()->findOrFail($id);
            $model->start_date = $request->start_date;
            $model->date_layoff = $request->date_layoff;
            $model->type = $request->type;
            $model->save();

            return response()->json([
                'success' => true,
                'message' => 'Должность была успешно обновлена'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
