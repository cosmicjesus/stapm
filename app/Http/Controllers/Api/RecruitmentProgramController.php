<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\RecruitmentProgramService;
use App\Http\Requests\CreateRecruitmentProgramRequest;
use App\Http\Requests\UpdateRecruitmentProgram;
use App\Http\Controllers\Controller;

class RecruitmentProgramController extends Controller
{
    protected $service;

    public function __construct()
    {
        try {
            $this->service = new RecruitmentProgramService();
        } catch (\Exception $e) {
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRecruitmentProgramRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRecruitmentProgramRequest $request)
    {
        return $this->service->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRecruitmentProgram $request, $id)
    {
        return $this->service->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
