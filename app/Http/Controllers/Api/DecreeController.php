<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\DecreeService;
use App\AppLogic\Storage\DecreeStorage;
use App\Http\Requests\UpdateDecreeRequest;
use App\Models\Decree;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DecreeController extends Controller
{

    protected $service;
    protected $storage;

    public function __construct()
    {
        try {
            $this->service = new DecreeService();
            $this->storage = new DecreeStorage();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return $this->service->filter($request->filter)->paginate($request->limit);
    }

    public function uploadFile(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $decree = Decree::query()->findOrFail($id);
            $filePath = $this->storage->putDecree($request->file('file'), $decree->file_path);
            $decree->update(['file_path' => $filePath]);
            DB::commit();
            return new JsonResponse([
                'success' => true,
                'message' => 'Приказ был успешно загружен'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->detail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDecreeRequest $request, $id)
    {
        return $this->service->updateDecree($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
