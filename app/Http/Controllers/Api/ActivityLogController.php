<?php

namespace App\Http\Controllers\Api;


use App\AppLogic\ActivityLog\ActivityLogService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{

    protected $service;

    public function __construct()
    {
        try {
            $this->service = new ActivityLogService();
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function index(Request $request)
    {
        try {
            $logs = $this->service->filter($request->filter)->paginate($request->limit);

            return $logs;
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }
}