<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\ActivityLog\ActivityLogService;
use App\AppLogic\Services\CacheService;
use App\Models\Activity;
use App\Models\Student;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use PDF;

class DashboardController extends Controller
{

    protected $cacheService;

    public function __construct()
    {
        $this->cacheService = new CacheService();
    }

    public function statistic(Request $request)
    {
        $statistic = collect();
        $statistic->put('studentsCount', $this->cacheService->getStudentsCount());
        $statistic->put('enrolleesCount', $this->cacheService->getEnrolleesCount());
        $statistic->put('employeesCount', $this->cacheService->getEmployeesCount());
        $statistic->put('parentsCount', $this->cacheService->getParentsCount());
        $statistic->put('trainingGroupsCount', $this->cacheService->getTrainingGroupsCount());
        $statistic->put('programsCount', $this->cacheService->getProgramsCount());
        $statistic->put('educationsPlansCount', $this->cacheService->educationsPlansCount());

        return response()->json([
            'statistic' => $statistic->all()
        ]);
    }

    public function expiredPassports()
    {
        $students = Student::query()
            ->whereHas('program', function ($q) {
                $q->where('education_form_id', 1);
            })
            ->whereIn('status', ['student', 'academic', 'inArmy'])
            ->whereNotNull('passport_issuance_date')
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get();

        $data = [];

        foreach ($students as $student) {
            if (!is_null($student->passport_issuance_date)) {
                $now = Carbon::now();
                $dateOfEndPassport = $student->birthday->addMonth(1);
                $birthday = $student->birthday;
                $age = $dateOfEndPassport->diff($now, false)->y;
                $passportDate = $student->passport_issuance_date;

                $passportAge = $passportDate->diff($birthday, false)->y;

                if ((($age >= 20 && $age < 45) && $passportAge < 20) || ($age >= 45 && $passportAge < 45)) {
                    $stud = new \App\AppLogic\Student\Student($student);
                    $data[] = [
                        'id' => $stud->getId(),
                        'full_name' => $stud->getFullName(),
                        'group' => $stud->getGroup()['name'],
                        'birthday' => $birthday->format('d.m.Y'),
                        'passport_end' => $stud->getDocumentEndDate()
                    ];
                }
            }
        }

        return $data;
    }

    public function exportExpiredPassport(Request $request)
    {
        DB::beginTransaction();
        try {
            $pdf = PDF::loadView('export.expired-passports',
                [
                    'data' => $request->data,
                ]
            );
            DB::commit();

            ActivityLogService::writeActivity('get_report', 'Выгруза списка студентов с просроченными паспортами', 'report');

            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }
    }
}
