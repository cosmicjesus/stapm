<?php

namespace App\Http\Controllers\Api;

use App\Models\GroupLoad;
use App\Models\GroupLoadSubject;
use App\Models\GroupLoadSubjectPeriod;
use App\Models\LoadMonth;
use App\Models\LoadSubjectItem;
use App\Models\SubjectMonth;
use App\Models\TrainingGroup;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AppLogic\TrainingGroup as GroupClass;

class TeacherLoadController extends Controller
{
    protected function getGroupsByAcademicYear($year)
    {
        $loads = GroupLoad::query()
            ->where('academic_year', $year)
            ->with(['group', 'groupPeriod'])
            ->get();

        return $loads->map(function ($load) {
            return [
                'id' => $load->id,
                'name' => str_replace('{Курс}', $load->groupPeriod->number, $load->group->pattern),
                'course' => $load->groupPeriod->number,
                'group_id' => $load->group_id,

            ];
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($year)
    {
        try {

            $groups = $this->getGroupsByAcademicYear($year);
            return responder()->success(['groups' => $groups])->respond();

        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function getGroupsByYear($year)
    {
        try {
            $trainingGroups = TrainingGroup::query()->onlyTeach()
                ->where('term_type', 'Semester')
                ->whereHas('academicYears', function ($q) use ($year) {
                    return $q->where('year', $year);
                })->with(['academicYears' => function ($q) use ($year) {
                    return $q->where('year', $year);
                }])->orderBy('pattern')->get();

            $groups = $trainingGroups->map(function ($group) {
                $year = ($group->academicYears->first());
                return [
                    'id' => $group->id,
                    'name' => str_replace('{Курс}', $year->number, $group->pattern),
                    'course' => $year,
                    'group_period_id' => $year->id
                ];
            });

            return responder()->success(['groups' => $groups])->respond();

        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function addGroupInLoad(Request $request)
    {
        try {
            foreach ($request->input('groups') as $group) {
                $data = array_merge($group, ['academic_year' => $request->input('academic_year')]);
                GroupLoad::query()->create($data);
            }

            $groups = $this->getGroupsByAcademicYear($request->input('academic_year'));

            return responder()->success(['success' => true, 'groups' => $groups])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function addSubject(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $load = GroupLoad::query()->with(['group', 'group.currentAcademicYear', 'group.currentAcademicYear.terms'])->findOrFail($request->input('id'));
            $group = $load->group;

            $terms = $group->currentAcademicYear->terms->keyBy('number');
            $loadSubject = GroupLoadSubject::query()->create([
                'group_load_id' => $id,
                'subject_id' => $request->input('subject_id'),
                'comment' => $request->input('comment')
            ]);

            foreach ($request->input('terms') as $term) {
                $loadSubject->terms()->create([
                    'load_subject_id' => $loadSubject->id,
                    'term_id' => $terms[$term['semester']]['id'],
                    'plan' => $term['plan'],
                    'semester' => $term['semester'],
                    'employee_id' => $term['employee_id']
                ]);
            }

            $months = LoadMonth::query()->where('academic_year', $load->academic_year)->get();

            foreach ($months as $month) {
                SubjectMonth::query()->create([
                    'month_id' => $month->id,
                    'subject_id' => $loadSubject->id
                ]);
            }
            DB::commit();
            $subject = [
                'id' => $loadSubject->id,
                'name' => $loadSubject->subject->name
            ];

            return responder()->success(['success' => true, 'subject' => $subject, 'll' => $load, 'tt' => $terms])->respond();
        } catch (\Exception $exception) {
            DB::rollBack();
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $groupModel = GroupLoad::query()->with(['group', 'groupPeriod', 'subjects'])->findOrFail($id);


            $group = [
                'id' => $groupModel->id,
                'name' => str_replace('{Курс}', $groupModel->groupPeriod->number, $groupModel->group->pattern),
                'subjects' => $groupModel->subjects->map(function ($subject) {
                    return [
                        'id' => $subject->pivot->id,
                        'name' => $subject->name
                    ];
                })
            ];
            return responder()->success(['success' => true, 'group' => $group])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function getDetailSubject($id, $subjectId)
    {
        try {
            $periods = GroupLoadSubjectPeriod::query()
                ->with(['employee', 'term', 'term.calendarItem'])
                ->where('load_subject_id', $subjectId)
                ->get()
                ->map(function (GroupLoadSubjectPeriod $period) {
                    return [
                        'id' => $period->id,
                        'semester' => $period->semester,
                        'plan' => $period->plan,
                        'employee' => [
                            'id' => $period->employee->id,
                            'full_name' => $period->employee->full_name
                        ],
                        'calendar' => [
                            'start' => $period->term->calendarItem->start_date->format('d.m.Y'),
                            'end' => $period->term->calendarItem->end_date->format('d.m.Y')
                        ]
                    ];
                });

            $months = SubjectMonth::query()->with(['month','loadItems'])->where('subject_id', $subjectId)
                ->get()
                ->map(function ($month) {
                    return [
                        'id' => $month->id,
                        'name' => $month->month->name,
                        'start_date' => $month->month->start_date->format('Y-m-d'),
                        'end_date' => $month->month->end_date->format('Y-m-d'),
                        'count_lessons'=>$month->loadItems->sum('count'),
                        'semester'=>$month->month->semester
                    ];
                });

            return responder()->success(['periods' => $periods, 'months' => $months])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function getMonthItems($id, $subjectId,$monthId)
    {
        try {
            $lessons=LoadSubjectItem::query()
                ->where('month_id',$monthId)
                ->orderBy('date')
                ->orderBy('number')
                ->get();
            return responder()->success(['lessons'=>$lessons])->respond();
        }catch (\Exception $exception){
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function addLesson(Request $request,$id, $subjectId,$monthId){
        try {
            $lesson=LoadSubjectItem::query()->create($request->input());
            return responder()->success(['id'=>$lesson->id])->respond();
        }catch (\Exception $exception){
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }
}
