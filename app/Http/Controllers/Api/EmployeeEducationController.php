<?php

namespace App\Http\Controllers\Api;

use App\AppLogic\Services\EmployeeService;
use App\Http\Requests\CreateEmployeeEducationRequest;
use App\Http\Requests\UpdateEmployeeEducationRequest;
use App\Http\Requests\UpdateEmployeePositionRequest;
use App\Models\EmployeeEducation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeEducationController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new EmployeeService();
    }

    public function addEducation(CreateEmployeeEducationRequest $request, $id)
    {
        return $this->service->attachEducation($id, $request->all());
    }

    public function updateEducation(UpdateEmployeeEducationRequest $request, $id)
    {
        try {
            EmployeeEducation::query()->findOrFail($id)->update($request->all());

            return response()->json([
                'success' => true,
                'message' => 'Уровень образования был успешно обновлен',
                'data' => $request->all()
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function deleteEducation($id)
    {
        try {
            EmployeeEducation::destroy($id);

            return response()->json([
                'success' => true,
                'message' => 'Уровень образования был успешно удален'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
