<?php

namespace App\Http\Controllers;

use App\AppLogic\Services\EntrantService;
use App\AppLogic\Services\ExportService;
use App\AppLogic\Services\ProfessionProgramService;
use App\AppLogic\Services\RecruitmentProgramService;
use App\AppLogic\Services\SelectionCommitteeService;
use App\AppLogic\Services\StudentService;
use App\Http\Controllers\Api\SelectionCommitteeController;
use App\Mail\EntrantMail;
use App\Mail\SendUploadingDocumentsFormMail;
use App\Models\Employee;
use App\Models\Entrant;
use App\Models\Student;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DateTime;
use DateTimeImmutable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\AppLogic\TrainingGroup\TrainingGroup as ClassHandler;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{

    protected $service;
    protected $priem;

    public function __construct(\App\AppLogic\Services\CacheService $cacheService)
    {
        $this->service = $cacheService;
        $this->priem = new RecruitmentProgramService();
    }

    public function test(ExportService $exportService)
    {
        if (env('APP_ENV') == 'production') {
            abort(404);
        }

        $en=(new EntrantService())->makeToStatement(2185);
dd($en);
//        $ss=ProfessionProgramService::getProgramsByIdOneType(27);
//        dd($ss);
        $file = $exportService->makeStatementOnEnrollmentByEntrantId(2188);
        //dd($file);
        return ($file['file'])->saveAs($file['filename']);
        $temp = new DateTimeImmutable();

        $start_date1 = new DateTime('1996-12-06');
        $end_date1 = new DateTime('2019-12-06');

        $start_date2 = new DateTime('2019-05-06');
        $end_date2 = new DateTime('2019-10-10');

        $interval1 = $end_date1->diff($start_date1);
        $interval2 = $end_date2->diff($start_date2);
        $interval3 = $temp->diff($temp->add($interval1)->add($interval2));

        echo $interval1->format('%y год, %m месяцев, %d дней (общее кол-во дней = %a)'), PHP_EOL;
        echo $interval2->format('%y год, %m месяцев, %d дней (общее кол-во дней = %a)'), PHP_EOL;
        echo $interval3->format('%y год, %m месяцев, %d дней (общее кол-во дней = %a)'), PHP_EOL;


        /////

//        $entrant = Entrant::query()->findOrFail(2161);
//        \Mail::to('silantev-zhenya@mail.ru')->send(new EntrantMail($entrant));
//        return new EntrantMail($entrant);

        //dd($end->diffInMilliseconds($start));
        $committeeID = (new SelectionCommitteeService())->getActiveCommitteeId();

        $programs = $this->priem->getProgramsByCommitteeId($committeeID);

        $listData = $programs['programs'];

        $dd = [];

        foreach ($listData as $program) {

            $averages = Student::query()
                ->select('avg')
                ->where('profession_program_id', $program['profession_program_id'])
                ->where('status', 'enrollee')
                ->orderBy('avg', 'DESC')
                ->get()
                ->map(function ($av) {
                    return $av->avg;
                });

            $dd[] = [
                'program_name' => $program['program_name'],
                'reception_plan' => $program['reception_plan'],
                'averages' => [
                    'count' => $averages->count(),
                    'max' => $averages->first(),
                    'min' => $averages->get(($program['reception_plan'] - 1)) ?? $averages->last()
                ]];
        }

        $myAvg = 3.8;

        $actualPrograms = [];

        foreach ($dd as $item) {

            $isHasPlace = $item['reception_plan'] > $item['averages']['count'];

            if ($isHasPlace) {
                $actualPrograms[] = $item;
            }

            $inRange = ($item['averages']['max'] >= $myAvg) && ($myAvg >= $item['averages']['min']);

            if (!$isHasPlace && $inRange) {
                $actualPrograms[] = $item;

            }

        }

        dd($dd, $actualPrograms);

    }

}
