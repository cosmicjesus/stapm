<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\TochkaRosta\TochkaRosta;
use App\BusinessLogic\TochkaRosta\TochkaRostaImage;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Models\TochkaRostaItem;
use App\Models\TochkaRostaPhoto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class TochkaRostaController extends Controller
{
    public function index()
    {
        return view('admin.tochka-rosta.index');
    }

    public function data()
    {
        $items = TochkaRostaItem::query()
            ->orderBy('publication_date', 'DESC')
            ->get()
            ->map(function ($item) {
                return new TochkaRosta($item);
            });

        return Datatables::of($items)
            ->addColumn('id', function ($new) {
                return $new->getId();
            })
            ->addColumn('title', function ($new) {
                return $new->getTitle();
            })
            ->addColumn('image', function ($new) {
                return "<a href='" . $new->getImage() . "' data-fancybox data-caption='" . $new->getTitle() . "'>Открыть</a>";
            })
            ->addColumn('publication_date', function ($new) {
                return $new->getPublicationDate();
            })
            ->addColumn('preview', function ($new) {
                return $new->getPreview();
            })
            ->addColumn('active', function ($new) {
                return $new->getActiveStr();
            })
            ->addColumn('actions', function ($new) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-primary btn-xs' href='" . $new->getPhotosListUrl() . "'><i class='fa fa-photo'></i></a>
                <button class='btn btn-danger btn-xs delete-news-js' data-route='" . $new->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";
                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions','preview','image'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.tochka-rosta.create');
    }

    public function store(Request $request)
    {
        $messages = [
            'title.required' => "Поле 'Заголовок' не заполнено",
            'publication_date.date_format' => "Первое поле 'Даты показа' не заполнено",
            'preview.required' => "Поле 'Превью' не заполнено",
            'full_text.required' => "Поле 'Полный текст' не заполнено",
        ];

        $this->validate($request, [
            'title' => 'required',
            'publication_date' => 'date_format:d.m.Y',
            'preview' => 'required',
            'full_text' => 'required'
        ], $messages);

        $news = new TochkaRostaItem();
        $news->title = $request->title;
        $news->publication_date = Carbon::createFromFormat('d.m.Y', $request->publication_date)->format('Y-m-d');
        $news->active = $request->active ? true : false;
        $news->preview = $request->preview;
        $news->full_text = $request->full_text;
        $news->sort = 500;
        $news->save();

        if ($request->file('file')) {
            $image = StorageFile::putTochkaRostaImage($request->file('file'));
            $news->image = $image;
            $news->save();
        }

        return redirect()->route('admin.tochka-rosta');
    }

    public function photos($id)
    {
        $model = TochkaRostaItem::findOrFail($id);
        $item = new TochkaRosta($model);
        return view('admin.tochka-rosta.photos', ['item' => $item]);
    }

    public function uploadPhoto(Request $request, $id)
    {
        try {
            $model = new TochkaRostaPhoto();
            $model->news_id = $id;
            $model->sort = 500;
            $model->path = '';
            $model->save();

            $model->path = StorageFile::putTochkaRostaPhoto($id, $request->file('file'));
            $model->save();

            $photo = new TochkaRostaImage($model);

            return response()->json([
                'success' => true,
                'path' => $photo->getPath(),
                'deleteUrl' => $photo->getDeleteUrl()
            ])->setStatusCode(200);

        } catch (\Exception $exception) {

        }
    }

    public function deleteTochkaPhoto($photo_id)
    {
        try {
            $photo = TochkaRostaPhoto::findOrFail($photo_id);
            $path = $photo->path;
            $photo->delete();
            StorageFile::deleteTochkaRostaPhoto($path);

            return response('Фотография успешно удалена');

        } catch (\Exception $exception) {
            return response('<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>", 422);
        }
    }

    public function edit($slug)
    {

    }

    public function update()
    {

    }
}