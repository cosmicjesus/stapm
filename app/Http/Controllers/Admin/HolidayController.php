<?php

namespace App\Http\Controllers\Admin;

use App\BusinessLogic\Holiday\HolidayService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class HolidayController extends Controller
{

    private $service;

    public function __construct()
    {
        $this->service = new HolidayService();
    }

    public function index()
    {
        return view('admin.holidays.index');
    }

    public function data()
    {
        $data = $this->service->orderByStartDate('DESC')->all();

        return Datatables::of($data)
            ->addColumn('id', function ($holiday) {
                return $holiday->getId();
            })
            ->addColumn('description', function ($holiday) {
                return $holiday->getDescription();
            })
            ->addColumn('start_date', function ($holiday) {
                return $holiday->getStartDate();
            })
            ->addColumn('end_date', function ($holiday) {
                return $holiday->getEndDate();
            })
            ->addColumn('actions', function ($holiday) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . $holiday->getEditUrl() . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-holiday-js' data-name='" . $holiday->getDescription() . "' data-route='" . $holiday->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function store(Request $request)
    {
        try {
            $this->service->create($request->all());
            return response()->json([
                'success' => true
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(422);
        }
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        try {
            $this->service->destroy($id);
            return response()->json([
                'success' => true
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(422);
        }
    }
}
