<?php

namespace App\Http\Controllers\Admin;

use App\BusinessLogic\Enrollee\Enrollee;
use App\BusinessLogic\Student\StudentService;
use App\Helpers\Settings;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\Decree;
use App\Models\Education;
use App\Models\Student;
use App\Models\StudentParent;
use App\Models\Subject;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;
use Yajra\Datatables\Datatables;
use PDF;

class StudentController extends Controller
{
    protected $service;
    protected $groups;
    protected $educations;

    public function __construct()
    {
        try {
            $this->service = new StudentService();
            $this->groups = TrainingGroup::query()
                ->where('status', 'teach')
                ->orderBy('pattern')
                ->orderBy('course')
                ->get()
                ->map(function ($group) {
                    return new \App\BusinessLogic\TrainingGroup\TrainingGroup($group);
                });

            $this->educations = Education::query()
                ->get()
                ->keyBy('id')
                ->map(function ($edu) {
                    return $edu->name;
                })
                ->toArray();
        } catch (\Exception $e) {

        }
    }

    public function index()
    {
        $this->authorize('index', 'Student');
        return view('admin.students.index', ['groups' => $this->groups]);
    }

    public function data(Request $request)
    {
        try {
            Log::info('достаем студентов для дата таблес');
            $service = $this->service->onlyStudents();

            if (!empty($request->get('name'))) {
                $service->filterByName($request->get('name'));
            }

            if (!empty($request->get('group'))) {
                $service->filterByGroup($request->get('group'));
            }

            if (!empty($request->get('status'))) {
                $service->filterByStatus($request->get('status'));
            }
            if ($request->get('sortByGroup') === 'true') {
                $service->orderByGroup();
            }
            if (filter_var($request->onlyLongAbsent, FILTER_VALIDATE_BOOLEAN)) {
                $service->onlyLongAbsent();
            }

            if (filter_var($request->facility, FILTER_VALIDATE_INT)) {
                $service->filterByPrivilegy($request->facility);
            }

            $students = $service->orderByFullName()->map();

            return Datatables::of($students)
                ->addColumn('checkbox', function ($student) {
                    return json_encode(['id' => $student->getId(), 'full_name' => $student->getFullName()]);
                })
                ->addColumn('id', function ($student) use ($request) {
                    return $student->getId();
                })
                ->addColumn('full_name', function ($student) {
                    $full_name = $student->getFullName();

                    if ($student->isLongAbsent()) {
                        $full_name .= " (Длительно отсутствующий)";
                    }

                    return $full_name;
                })
                ->addColumn('birthday', function ($student) {
                    return $student->getBirthday();
                })
                ->addColumn('group', function ($student) {
                    return $student->getGroup()->getName();
                })
                ->addColumn('status', function ($student) {
                    return $student->getStatus();
                })
                ->addColumn('privilegy', function ($student) {
                    return $student->getPrivelegeCategory();
                })
                ->addColumn('phone', function ($student) {
                    return $student->getPhone();
                })
                ->addColumn('actions', function ($student) {
                    $table = "<div class='table-buttons'>#CONTENT#</div>";
                    $buttons = "<a class='btn btn-primary btn-xs' href=" . $student->getProfileUrl() . "><i class='fa fa-user'></i></a>";

                    $str = str_replace('#CONTENT#', $buttons, $table);
                    return $str;
                })
                ->rawColumns(['checkbox', 'actions'])
                ->make(true);
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
            return response()->json(['data' => $exception->getMessage()]);
        }
    }

    public function profile($slug)
    {
        $this->authorize('index', 'Student');
        $model = Student::findBySlugOrFail($slug);
        $student = new \App\BusinessLogic\Student\Student($model);
        return view('admin.students.profile', ['student' => $student]);
    }

    public function edit($slug)
    {
        $this->authorize('actions', 'Student');
        $student = Student::findBySlugOrFail($slug);
        //dd($student);
        return view('admin.students.edit', ['student' => $student]);
    }

    /**
     * @param Request $request
     * @param $slug
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $slug)
    {
        $this->authorize('actions', 'Student');
        $student = Student::findBySlugOrFail($slug);
        $student->lastname = $request->input('lastname');
        $student->firstname = $request->input('firstname');
        $student->middlename = $request->input('middlename');
        $student->gender = $request->input('gender');
        $student->phone = empty($request->phone) ? null : $request->phone;
        $student->birthday = Carbon::createFromFormat('d.m.Y', $request->birthday)->format('Y-m-d');
        $student->passport_issuance_date = isset($request->passport_issuance_date) ? Carbon::createFromFormat('d.m.Y', $request->passport_issuance_date)->format('Y-m-d') : null;
        $student->date_of_actual_transfer = isset($request->date_of_actual_transfer) ? Carbon::createFromFormat('d.m.Y', $request->date_of_actual_transfer)->format('Y-m-d') : null;
        /** @var TYPE_NAME $student */
        $student->avg = $request->avg;
        $student->snils = $request->snils;
        $student->inn = $request->inn;
        $student->education_id = $request->education_id;
        $student->privileged_category = is_null($request->privileged_category) ? $request->privileged_category : filter_var($request->privileged_category, FILTER_VALIDATE_INT);
        $student->medical_policy_number = $request->medical_policy_number;
        $student->long_absent = filter_var($request->long_absent, FILTER_VALIDATE_BOOLEAN);
        $student->has_certificate = filter_var($request->has_certificate, FILTER_VALIDATE_BOOLEAN);
        $student->comment = $request->input('comment');
        $student->slug = null;
        $student->save();
        Settings::refreshStudentDataCache();
        return redirect()->route('admin.students.profile', ['slug' => $student->slug]);
    }

    public function allocation(Request $request)
    {
        try {

            $decree = Decree::query()->firstOrCreate([
                'number' => $request->input('decree_number'),
                'date' => Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d')
            ]);

            $specialReasons = ['academic', 'inArmy'];
            $reason = $request->input('reason');

            if (in_array($request->input('reason'), $specialReasons)) {
                $status = $request->input('reason');
            } else {
                $status = 'expelled';
            }

            $groups = TrainingGroup::query()->get()
                ->keyBy('id')
                ->map(function ($group) {
                    return str_replace('{Курс}', $group->course, $group->pattern);
                })
                ->toArray();

            foreach ($request->input('ids') as $key => $id) {
                $student = Student::query()->findOrFail($id);
                $student->status = $status;
                $student->save();
                $student->decrees()->attach([$decree->id => ['type' => 'allocation', 'reason' => $reason, 'group_from' => $groups[$student->group_id]]]);
            }
            Settings::refreshStudentDataCache();
            return response()->json([
                'success' => true,
                'message' => 'Студенты были успешно отчисленны',
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    public function transferOtherGroup(Request $request)
    {
        try {
            $decree = Decree::query()->firstOrCreate([
                'number' => $request->input('decree_number'),
                'date' => Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d')
            ]);

            $groups = TrainingGroup::query()->get()
                ->keyBy('id')
//                ->map(function ($group) {
//                    return str_replace('{Курс}', $group->course, $group->pattern);
//                })
                ->toArray();

            foreach ($request->input('ids') as $key => $id) {
                $student = Student::query()->findOrFail($id);
                $groupFrom = $groups[$student->group_id];
                $groupTo = $groups[$request->input('group')];
                $student->group_id = $groupTo['id'];
                $student->profession_program_id = $groupTo['profession_program_id'];
                $student->save();
                $student->decrees()->attach([$decree->id => [
                    'type' => 'transfer',
                    'reason' => null,
                    'group_from' => str_replace('{Курс}', $groupFrom['course'], $groupFrom['pattern']),
                    'group_to' => str_replace('{Курс}', $groupTo['course'], $groupTo['pattern'])
                ]]);
            }
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => 'Студенты были успешно переведены',
            ]);


        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    public function create()
    {
        $this->authorize('create', 'Student');
        return view('admin.students.create', [
            'educations' => $this->educations,
            'groups' => $this->groups
        ]);
    }

    public function store(Request $request)
    {
        try {
            $this->authorize('create', 'Student');
            $group = TrainingGroup::query()->findOrFail($request->input('group_id'));

            $decree_date = Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d');

            $student = new Student();
            $student->lastname = $request->lastname;
            $student->firstname = $request->firstname;
            $student->middlename = $request->middlename;
            $student->gender = $request->gender;
            $student->education_id = $request->education;
            $student->group_id = $request->group_id;
            $student->profession_program_id = $group->profession_program_id;
            $student->avg = $request->avg;
            $student->phone = empty($request->phone) ? null : $request->phone;
            $student->birthday = Carbon::createFromFormat('d.m.Y', $request->birthday)->format('Y-m-d');
            $student->graduation_date = Carbon::createFromFormat('d.m.Y', $request->graduation_date)->format('Y-m-d');
            $student->start_date = Carbon::createFromFormat('d.m.Y', $request->graduation_date)->format('Y-m-d');
            $student->passport_issuance_date = isset($request->passport_issuance_date) ? Carbon::createFromFormat('d.m.Y', $request->passport_issuance_date)->format('Y-m-d') : null;
            $student->has_original = true;
            $student->has_certificate = true;
            $student->date_of_actual_transfer = $decree_date;
            $student->enrollment_date = $decree_date;
            $student->need_hostel = isset($request->has_original) ? true : false;
            $student->person_with_disabilities = isset($request->person_with_disabilities) ? true : false;
            $student->contract_target_set = isset($request->contract_target_set) ? true : false;
            $student->graduation_organization_name = isset($request->graduation_organization_name) ? $request->graduation_organization_name : null;
            $student->status = 'student';
            $student->save();

            $decree = Decree::query()->firstOrCreate([
                'number' => $request->input('decree_number'),
                'date' => Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d')
            ]);

            $groupString = str_replace('{Курс}', $group->course, $group->pattern);

            $student->decrees()->attach($decree->id, [
                'type' => 'enrollment',
                'group_to' => $groupString,
                'reason' => $request->input('reason')
            ]);
            Settings::refreshStudentDataCache();

            return response()->json([
                'data' => $request->input()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => "<b>" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }

    }

    public function getDecrees(Request $request, $id)
    {
        $student = \App\BusinessLogic\Student\Student::buildByID($id);
        $decrees = $student->getDecrees();
        $str = '';
        foreach ($decrees as $decree) {
            $str .= "<option value='" . $decree->getId() . "'>" . $decree->getNumber() . "</option>";
        }

        return response()->json([
            'options' => $str
        ])->setStatusCode(200);
    }

    public function addParent(Request $request, $id)
    {
        try {
            $model = new StudentParent();
            $model->student_id = $id;
            $model->lastname = $request->input('lastname');
            $model->firstname = $request->input('firstname');
            $model->middlename = $request->input('middlename');
            $model->gender = $request->input('gender');
            $model->phone = $request->input('phone');
            $model->relationshipType = $request->input('relationshipType');
            $model->save();

            $student = Enrollee::build(Student::findOrFail($id));

            return response()->json([
                'template' => view('admin.enrollees.inc.parents', ['enrollee' => $student])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => "<b>" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }
    }

    public function createCountReport()
    {

        $now = Carbon::now()->year;
        $students = Student::query()
            ->with('program')
            ->whereIn('status', ['student', 'inArmy', 'academic'])
            ->get()
            ->filter(function ($student) {
                return $student->program->education_form_id == 1;
            });

        $actualStudents = $students->groupBy(function ($student) {
            return $student->program->type_id == 1 ? 'ppssz' : 'ppkrs';
        });


        $enrollment = $students->filter(function ($student) use ($now) {
            return !is_null($student->enrollment_date) && ($student->enrollment_date->year == $now);
        });

        $diviletToType = $enrollment->groupBy(function ($student) {
            return $student->program->type_id == 1 ? 'ppssz' : 'ppkrs';
        });
        $types = [
            'ppssz',
            'ppkrs'
        ];

        $result = [];
        foreach ($types as $type) {

            $students = $actualStudents->get($type)->map(function ($student) {
                return new \App\BusinessLogic\Student\Student($student);
            });
            $enrollments = is_null($diviletToType->get($type)) ? [] : $diviletToType->get($type)->map(function ($student) {
                return new \App\BusinessLogic\Student\Student($student);
            });
            $gridAge = [];

            foreach ($students as $student) {
                if (!isset($gridAge[$student->getAgeOfStartNextYear()])) {
                    $gridAge[$student->getAgeOfStartNextYear()]['students_total_count'] = 1;
                    $gridAge[$student->getAgeOfStartNextYear()]['students_woman_count'] = $student->getGender();
                    $gridAge[$student->getAgeOfStartNextYear()]['ids'] = $student->getId();

                    $gridAge[$student->getAgeOfStartNextYear()]['enrollment_total_count'] = 0;
                    $gridAge[$student->getAgeOfStartNextYear()]['enrollment_woman_count'] = 0;
                } else {
                    $gridAge[$student->getAgeOfStartNextYear()]['students_total_count']++;
                    $gridAge[$student->getAgeOfStartNextYear()]['students_woman_count'] += $student->getGender();
                    $gridAge[$student->getAgeOfStartNextYear()]['ids'] .= " | " . $student->getId();
                    $gridAge[$student->getAgeOfStartNextYear()]['enrollment_total_count'] = 0;
                    $gridAge[$student->getAgeOfStartNextYear()]['enrollment_woman_count'] = 0;
                }
            }

            foreach ($enrollments as $enrollment) {
                if (!isset($gridAge[$enrollment->getAgeOfStartNextYear()])) {
                    $gridAge[$enrollment->getAgeOfStartNextYear()]['enrollment_total_count'] = 1;
                    $gridAge[$enrollment->getAgeOfStartNextYear()]['enrollment_woman_count'] = $enrollment->getGender();
                } else {
                    $gridAge[$enrollment->getAgeOfStartNextYear()]['enrollment_total_count']++;
                    $gridAge[$enrollment->getAgeOfStartNextYear()]['enrollment_woman_count'] += $enrollment->getGender();
                }
            }
            ksort($gridAge);

            $result[$type] = [
                'enrollment' => [
                    'total' => is_null($diviletToType->get($type)) ? 0 : $diviletToType->get($type)->count(),
                    'woman' => is_null($diviletToType->get($type)) ? 0 : $diviletToType->get($type)->filter(function ($student) {
                        return $student->gender == 1;
                    })->count()
                ],
                'students' => [
                    'total' => $actualStudents->get($type)->count(),
                    'woman' => $actualStudents->get($type)->filter(function ($student) {
                        return $student->gender == 1;
                    })->count()
                ],
                'gridAge' => $gridAge
            ];
        }

        $pdf = PDF::loadView('export.spo1-students-count', ['data' => $result]);

        return $pdf->stream('students-count.pdf');
    }

    public function reportOnDate(Request $request)
    {
        $reportDate = Carbon::now()->format('d.m.Y');
        $students = Student::query()
            ->with('program')
            ->whereIn('status', ['student', 'inArmy', 'academic'])
            ->get()
            ->filter(function ($student) {
                return $student->program->education_form_id == 1;
            })
            ->map(function ($student) {
                return new \App\BusinessLogic\Student\Student($student);
            })
            ->filter(function ($student) use ($reportDate) {
                return $student->getAgeOnDate($reportDate) < 18;
            });

        $groups = TrainingGroup::query()
            ->where('status', 'teach')
            ->get()
            ->map(function ($group) {
                return new \App\BusinessLogic\TrainingGroup\TrainingGroup($group);
            });

        $training_groups = [];

        foreach ($groups as $group) {
            $training_groups[$group->getName()] = $group->getCurator()->getFullName();
        }

        $students = $students->groupBy(function ($student) {
            return $student->getGroup()->getName();
        });
        $pdf = PDF::loadView('export.report-on-date',
            ['data' => $students, 'groups' => $training_groups],
            [],
            [
                'orientation' => 'L'
            ]
        );

        return $pdf->stream('report-on-date.pdf');
    }
}