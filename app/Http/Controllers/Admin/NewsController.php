<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\News\NewsService;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\News;
use App\Models\NewsFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class NewsController extends Controller
{
    protected $service;

    public function __construct()
    {
        try {
            $this->service = new NewsService();
        } catch (\Exception $e) {
        }
    }

    private function uploadFiles(Request $request, News $news)
    {
        if (isset($request->names) && count($request->file('files'))) {
            foreach ($request->file('files') as $key => $file) {
                if (isset($request->names[$key])) {
                    $fileModel = new NewsFile();
                    $fileModel->news_id = $news->id;
                    $fileModel->name = $request->names[$key];
                    $fileModel->save();

                    $pathToFile = StorageFile::putNewsFile($news->id, $file);

                    $fileModel->path = $pathToFile;
                    $fileModel->save();
                }
            }
        }
    }

    public function index()
    {
        return view('admin.news.index');
    }

    public function data(Request $request)
    {

        $service = $this->service;

        $news = News::query()
            ->orderBy('publication_date', 'desc')
            ->get()
            ->map(function ($newsItem) {
                return new \App\BusinessLogic\News\News($newsItem);
            });

        return Datatables::of($news)
            ->addColumn('id', function ($new) {
                return $new->getId();
            })
            ->addColumn('title', function ($new) {
                return $new->getTitle();
            })
            ->addColumn('image', function ($new) {
                return "<a href='" . $new->getImage() . "' data-fancybox data-caption='" . $new->getTitle() . "'>Открыть</a>";
            })
            ->addColumn('publication_date', function ($new) {
                return $new->getPublicationDate();
            })
            ->addColumn('visible_to', function ($new) {
                return $new->getVisibleToDate();
            })
            ->addColumn('section', function ($new) {
                return $new->getNewsTypeStr();
            })
            ->addColumn('preview', function ($new) {
                return $new->getPreview();
            })
            ->addColumn('active', function ($new) {
                return $new->getActiveStatusStr();
            })
            ->addColumn('actions', function ($new) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . $new->getEditUrl() . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-news-js' data-route='" . $new->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions','preview','image'])
            ->make(true);

    }

    public function createPage()
    {
        return view('admin.news.create');
    }

    public function create(Request $request)
    {
        $messages = [
            'title.required' => "Поле 'Заголовок' не заполнено",
            'publication_date.date_format' => "Первое поле 'Даты показа' не заполнено",
            'preview.required' => "Поле 'Превью' не заполнено",
            'full_text.required' => "Поле 'Полный текст' не заполнено",
        ];

        $this->validate($request, [
            'title' => 'required',
            'publication_date' => 'date_format:d.m.Y',
            'preview' => 'required',
            'full_text' => 'required'
        ], $messages);

        $news = new News();
        $news->title = $request->title;
        $news->publication_date = Carbon::createFromFormat('d.m.Y', $request->publication_date)->format('Y-m-d');
        $news->visible_to = is_null($request->visible_to) ? null : Carbon::createFromFormat('d.m.Y', $request->visible_to)->format('Y-m-d');
        $news->active = $request->active ? true : false;
        $news->preview = $request->preview;
        $news->type = $request->type;
        $news->full_text = $request->full_text;
        $news->save();

        if ($request->file('file')) {
            $image = StorageFile::putNewsImage($request->file('file'));
            $news->image = $image;
            $news->save();
        }
        $this->uploadFiles($request, $news);

        return redirect()->route('admin.news');
    }

    public function edit($slug)
    {
        $news = News::findBySlug($slug);

        //dd($news->publication_date);

        return view('admin.news.edit', ['news' => new \App\BusinessLogic\News\News($news)]);
    }

    public function update(Request $request, $slug)
    {

        $messages = [
            'title.required' => "Поле 'Заголовок' не заполнено",
            'publication_date.date_format' => "Первое поле 'Даты показа' не заполнено",
            'preview.required' => "Поле 'Превью' не заполнено",
            'full_text.required' => "Поле 'Полный текст' не заполнено",
        ];

        $this->validate($request, [
            'title' => 'required',
            'publication_date' => 'date_format:d.m.Y',
            'preview' => 'required',
            'full_text' => 'required'
        ], $messages);

        $news = News::findBySlug($slug);
        $news->title = $request->title;
        $news->slug = null;
        $news->publication_date = Carbon::createFromFormat('d.m.Y', $request->publication_date)->format('Y-m-d');
        $news->visible_to = is_null($request->visible_to) ? null : Carbon::createFromFormat('d.m.Y', $request->visible_to)->format('Y-m-d');
        $news->active = $request->active ? true : false;
        $news->preview = $request->preview;
        $news->full_text = $request->full_text;
        $news->type = $request->type;
        $news->save();

        if ($request->file('file')) {
            $image = StorageFile::putNewsImage($request->file('file'));
            $news->image = $image;
            $news->save();
        }


        $this->uploadFiles($request, $news);
        return redirect()->route('admin.news');

    }

    public function delete($id)
    {
        try {
            $model = News::with('files')->findOrFail($id);
            $file = $model->image;
            $model->delete();
            StorageFile::deleteNewsFilesDir($id);
            StorageFile::deleteNewsImage($file);
            $this->dispatch(new SiteMap());
            return "Новость была удалена";
        } catch (\Exception $exception) {
            return "Ошибка";
        }
    }

    public function editFileName(Request $request, $id)
    {
        try {

            $model = NewsFile::findOrFail($id);
            $model->name = $request->input('name');
            $model->save();

            return response()->json([
                'success' => true,
                'message' => 'Документ был обновлен'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => '<b>Код ошибки' . $exception->getCode() . '</b><br><b>Текст ошибки' . $exception->getMessage() . '</b>'
            ], 422);
        }
    }

    public function deleteNewsFile($id)
    {
        try {

            $model = NewsFile::findOrFail($id);
            $path = $model->path;
            $model->delete();

            StorageFile::deleteNewsFile($path);

            return response()->json([
                'success' => true,
                'message' => 'Документ был успешно удален'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => '<b>Код ошибки' . $exception->getCode() . '</b><br><b>Текст ошибки' . $exception->getMessage() . '</b>'
            ], 422);
        }
    }

}