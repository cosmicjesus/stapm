<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\EducationPlan\EducationPlanService;
use App\BusinessLogic\ProfessionProgram\ProfessionProgramService;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Models\Education;
use App\Models\EducationPlan;
use App\Models\ProfessionProgram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class EducationPlanController extends Controller
{

    protected $service;
    protected $programs;

    public function __construct()
    {
        try {
            $this->service = new EducationPlanService();
            $this->programs = new ProfessionProgramService();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {

        $this->authorize('index', 'EduPlans');
        return view('admin.education-plans.index', ['programs' => $this->programs->map()]);
    }

    public function data(Request $request)
    {
        $this->authorize('index', 'EduPlans');

//        $service = $this->service;
//
//        $plans = $service->orderBy('profession_program_id')
//            ->orderBy('start_training')
//            ->map();

        $service = EducationPlan::query();

        $service->when(filter_var($request->program_id, FILTER_VALIDATE_INT), function ($q) {
            $q->where('profession_program_id', \request('program_id'));
        });

        $plans = $service->orderBy('profession_program_id', 'ASC')
            ->orderBy('start_training')
            ->get()
            ->map(function ($plan) {
                return new \App\BusinessLogic\EducationPlan\EducationPlan($plan);
            });

        return Datatables::of($plans)
            ->addColumn('id', function ($plan) {
                return $plan->getId();
            })
            ->addColumn('name', function ($plan) {
                return $plan->getName();
            })
            ->addColumn('program', function ($plan) {
                return $plan->getProgram();
            })
            ->addColumn('period', function ($plan) {
                return $plan->getTrainigPeriod();
            })
            ->addColumn('file', function ($plan) {
                if ($plan->getFilePath()) {
                    return "<a href='" . $plan->getFilePath() . "' target='_blank'>Открыть</a>";
                }
                return "Не загружен";
            })
            ->addColumn('actions', function ($plan) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . route('admin.education-plans.editPage', ['id' => $plan->getId()]) . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-plan-js' data-id='" . $plan->getId() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'file'])
            ->make(true);
    }

    public function createPage()
    {
        $this->authorize('create', 'EduPlans');

        $programs = $this->programs->map();
        return view('admin.education-plans.create', ['programs' => $programs]);
    }

    public function create(Request $request)
    {

        $this->authorize('create', 'EduPlans');

        $messages = [
            'program.required' => "Программа не выбрана",
            'name.required' => "Имя учебного плана не указано",
            'period.required' => "Не указано количество семестров",
            'start_training.date_format' => "Не указана дата начала обучения",
            'end_training.date_format' => "Не указана дата окончания обучения",
        ];

        $this->validate($request, [
            'program' => 'required',
            'name' => 'required',
            'period' => 'required|min:1',
            'start_training' => 'date_format:d.m.Y',
            'end_training' => 'date_format:d.m.Y'
        ], $messages);
        $options = ['adapted_program' => false, 'dual_training' => false];
        if (!is_null($request->options)) {
            foreach ($request->options as $key => $option) {
                $options[$key] = filter_var($option, FILTER_VALIDATE_BOOLEAN);
            }
        }

        $plan = new EducationPlan();
        $plan->profession_program_id = $request->program;
        $plan->name = $request->name;
        $plan->count_period = $request->period;
        $plan->start_training = Carbon::createFromFormat('d.m.Y', $request->start_training)->format('Y-m-d');
        $plan->end_training = Carbon::createFromFormat('d.m.Y', $request->end_training)->format('Y-m-d');
        $plan->active = $request->active ? true : false;
        $plan->in_revision = false;
        $plan->extra_options = $options;
        $plan->save();

        if ($request->file('file')) {
            $file = StorageFile::putEducationPlan($plan, $request->file('file'));
            $plan->full_path = $file;
            $plan->save();
        }

        return redirect()->route('admin.education-plans');
    }

    public function editPage($id)
    {
        $this->authorize('actions', 'EduPlans');

        $programs = $this->programs->map();
        $plan = $this->service->getById($id);
        return view('admin.education-plans.edit', ['programs' => $programs, 'plan' => $plan]);
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('actions', 'EduPlans');

        $messages = [
            'program.required' => "Программа не выбрана",
            'name.required' => "Имя учебного плана не указано",
            'period.required' => "Не указано количество семестров",
            'start_training.date_format' => "Не указана дата начала обучения",
            'end_training.date_format' => "Не указана дата окончания обучения",
        ];

        $this->validate($request, [
            'program' => 'required',
            'name' => 'required',
            'period' => 'required|min:1',
            'start_training' => 'date_format:d.m.Y',
            'end_training' => 'date_format:d.m.Y'
        ], $messages);

        $options = ['adapted_program' => false, 'dual_training' => false];
        if (!is_null($request->options)) {
            foreach ($request->options as $key => $option) {
                $options[$key] = filter_var($option, FILTER_VALIDATE_BOOLEAN);
            }
        }
        $plan = EducationPlan::findOrFail($id);
        $plan->profession_program_id = $request->program;
        $plan->name = $request->name;
        $plan->count_period = $request->period;
        $plan->start_training = Carbon::createFromFormat('d.m.Y', $request->start_training)->format('Y-m-d');
        $plan->end_training = Carbon::createFromFormat('d.m.Y', $request->end_training)->format('Y-m-d');
        $plan->active = $request->active ? true : false;
        $plan->in_revision = $request->in_revision ? true : false;
        $plan->extra_options = $options;
        $plan->save();

        if ($request->file('file')) {
            $file = StorageFile::putEducationPlan($plan, $request->file('file'));
            $plan->full_path = $file;
            $plan->save();
        }

        return redirect()->route('admin.education-plans');
    }

    public function delete($id)
    {
        try {
            $this->authorize('actions', 'EduPlans');
            $model = EducationPlan::findOrFail($id);
            $file = $model->full_path;

            EducationPlan::destroy($id);
            StorageFile::deleteEducationPlan($file);
            return "Учебный план был удален";
        } catch (\Exception $exception) {
            return "Ошибка";
        }
    }

}