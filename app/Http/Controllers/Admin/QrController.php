<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class QrController extends Controller
{

    public function index()
    {
        $rows = [
            [
                'name' => 'Код для страницы заказа справок',
                'route' => route('admin.qr.generate', ['type' => 'references'])
            ],
            [
                'name' => 'Код для страницы новостей',
                'route' => route('admin.qr.generate', ['type' => 'news'])
            ]
        ];

        return view('admin.qr.index', ['rows' => $rows]);
    }

    public function generate(Request $request, $type)
    {
        switch ($type) {
            case 'references':
                $this->references();
                break;
            case 'news':
                $this->news();
                break;
            default:
                $this->index();
                break;
        }
    }

    protected function references()
    {
        $pdf = PDF::loadView('admin.qr.tpl.references');

        return $pdf->stream('references.pdf');
    }

    protected function news()
    {
        $pdf = PDF::loadView('admin.qr.tpl.news');

        return $pdf->stream('references.pdf');
    }
}
