<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdministrationEmployee;
use App\Models\Employee;
use Illuminate\Http\Request;

class AdministrationEmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::query()
            ->whereHas('administrator')
            ->get()
            ->sortBy(function ($employee) {
                return $employee->administrator->sort;
            })
            ->map(function ($employee) {
                return new \App\BusinessLogic\Employee\Employee($employee);
            });
        return view('admin.administrations.index', ['employees' => $employees]);
    }

    public function createPage()
    {
        $employees = Employee::query()
            ->whereDoesntHave('administrator')
            ->where('status', 'work')
            ->orderBy('lastname', 'ASC')
            ->orderBy('firstname', 'ASC')
            ->get()
            ->map(function ($employee) {
                return new \App\BusinessLogic\Employee\Employee($employee);
            });

        return view('admin.administrations.create', ['employees' => $employees]);
    }

    public function create(Request $request)
    {
        $messages = [
            'employee_id.required' => 'Не выбран сотрудник',
            'sort.required' => 'Не задана сортировка',
            'sort.numeric' => 'Поле Порядок сортировки должно быть числом',
            'sort.min' => 'Минимальное значение поля сортировки равно 1',
        ];

        $this->validate($request, [
            'employee_id' => 'required',
            'sort' => 'required|numeric|min:1'
        ], $messages);

        $employee = Employee::findOrFail($request->employee_id);

        $model = new AdministrationEmployee([
            'sort' => $request->sort
        ]);

        $employee->administrator()->save($model);

        return redirect()->route('admin.administrations');
    }

    public function editPage(Request $request, $id)
    {
        $model = \App\Models\AdministrationEmployee::with('employee')->findOrFail($id);
        return view('admin.administrations.edit', ['item' => $model]);
    }

    public function edit(Request $request, $id)
    {
        $messages = [
            'sort.required' => 'Не задана сортировка',
            'sort.numeric' => 'Поле Порядок сортировки должно быть числом',
            'sort.min' => 'Минимальное значение поля сортировки равно 1',

        ];

        $this->validate($request, [
            'sort' => 'required|numeric|min:1'
        ], $messages);

        $model = AdministrationEmployee::findOrFail($id);
        $model->sort = $request->sort;
        $model->save();


        return redirect()->route('admin.administrations');
    }

    public function delete($id)
    {
        try {
            AdministrationEmployee::destroy($id);
            return "Сотрудник был удален из списка";
        } catch (\Exception $exception) {
            return "Ошибка";
        }
    }
}