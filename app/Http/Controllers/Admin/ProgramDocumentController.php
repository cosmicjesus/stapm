<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\ProfessionProgram;
use App\Models\ProgramBasicDocument;
use App\Models\ProgramDocument;
use App\Models\ProgramSubject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProgramDocumentController extends Controller
{

    public function uploadBasic(Request $request, $id, $type)
    {
        $model = ProgramBasicDocument::where('profession_program_id', $id)->first();

        $oldFile = $model->$type;

        $filename = StorageFile::putProgramDocument($request->file('file'), $id, $oldFile);

        if ($filename) {
            $model->$type = $filename;
            $model->save();

            return response()->json([
                'success' => true,
                'path' => $model
            ]);
        }

        return response(422, 422)->json([
            'success' => false,
            'message' => 'Произошла ошибка при загрузке'
        ]);
    }

    public function addSubject(Request $request, $id)
    {
        $message = [
            'subject_id.required' => 'Дисциплина не выбрана'
        ];

        $validator = \Validator::make($request->all(), [
            'subject_id' => 'required'
        ], $message);

        if ($validator->passes()) {
            $program = ProfessionProgram::with('subjects')->findOrFail($id);
            $program->subjects()->attach($request->subject_id, ['in_revision' => false]);
            return response('Дисциплина добавлена', 200);
        }

        return response(['errors' => $validator->errors()->all()], 422);
    }

    public function addSubjectDocument(Request $request, $id)
    {
        $message = [
            'title.required' => 'Наименование не заполнено'
        ];

        $validator = \Validator::make($request->all(), [
            'title' => 'required'
        ], $message);

        if ($validator->passes()) {
            $model = ProgramSubject::with('files')->find($id);
            $path = StorageFile::putProgramDocument($request->file('file'), $request->program_id, null, 'file');
            $newFile = new ProgramDocument([
                'title' => $request->title,
                'path' => $path,
            ]);
            $model->files()->save($newFile);
            return response('Документ был успешно загружен', 200);
        }
        return response(['errors' => $validator->errors()->all()], 422);
    }

    public function deleteSubject($id)
    {
        $model = ProgramSubject::with('files')->findOrFail($id);

        $ids = [];
        foreach ($model->files as $file) {
            $ids[] = $file->id;
            StorageFile::deleteProgramDocument($file->path);
        }

        ProgramDocument::destroy($ids);

        $model->delete();
        $this->dispatch(new SiteMap());
        return response('Удалено');
    }

    public function reloadDocument(Request $request, $id, $other = 'text')
    {
        $model = ProgramDocument::findOrFail($id);
        $progId = (ProgramSubject::find($model->programtagle_id))->profession_program_id;
        if ($other == 'other') {
            $progId = $model->programtagle_id;
        }
        $old = $model->path;
        $newPath = StorageFile::putProgramDocument($request->file('file'), $progId, $old, 'file');
        $model->path = $newPath;
        $model->save();

        return response()->json([
            'path' => StorageFile::getProgramDocument($newPath)
        ]);
    }

    public function deleteDocument(Request $request, $id)
    {
        $model = ProgramDocument::findOrFail($id);

        StorageFile::deleteProgramDocument($model->path);

        $model->delete();

        return response('Удалено');
    }

    public function addOtherDocument(Request $request, $id)
    {

        $message = [
            'file.required' => 'Файл не загружен',
            'date.date_format' => 'Дата не выбрана',
        ];

        $validator = \Validator::make($request->all(), [
            'file' => 'required',
            'date' => 'date_format:d.m.Y'
        ], $message);
        if ($validator->passes()) {

            $program = ProfessionProgram::findOrFail($id);

            $file = StorageFile::putProgramDocument($request->file('file'), $id, null);

            $program->otherDocuments()->save(new ProgramDocument([
                'title' => getOtherDocumentName($request->type),
                'path' => $file,
                'start_date' => Carbon::createFromFormat('d.m.Y', $request->date)->format('Y-m-d'),
                'type' => $request->type
            ]));

            return response('Документ был успешно загружен', 200);
        }

        return response(['errors' => $validator->errors()->all()], 422);
    }

    public function getMetodicals(Request $request, $id)
    {

        $program = ProfessionProgram::query()->findOrFail($id);
        $metodicals = (new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program))->getMetodicals();
        return Datatables::of($metodicals)
            ->addColumn('id', function ($metodical) {
                return $metodical->getId();
            })
            ->addColumn('name', function ($metodical) {
                return $metodical->getTitle();
            })
            ->addColumn('file', function ($metodical) {
                return "<a href='" . $metodical->getFileUrl() . "' target='_blank'>Открыть</a>";
            })
            ->addColumn('actions', function ($metodical) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = '';

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'file'])
            ->make(true);
    }

    public function createMetodical(Request $request, $id)
    {
        try {

            $program = ProfessionProgram::findOrFail($id);

            $file = StorageFile::putProgramDocument($request->file('file'), $id, null);

            $program->metodicals()->save(new ProgramDocument([
                'title' => $request->name,
                'path' => $file,
                'start_date' => null,
                'type' => 'metodical'
            ]));


            return response()->json([
                'success' => true,
                'message' => $request->input()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => "<b>" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }
    }
}