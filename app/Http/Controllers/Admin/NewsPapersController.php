<?php

namespace App\Http\Controllers\Admin;


use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Models\NewsPaper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class NewsPapersController extends Controller
{

    private $monthsToNubmer;

    public function __construct()
    {
        $this->monthsToNubmer = [
            9 => 1,
            10 => 2,
            11 => 3,
            12 => 4,
            1 => 5,
            2 => 6,
            3 => 7,
            4 => 8,
            5 => 9,
            6 => 10,
            7 => 11,
            8 => 12
        ];
    }

    private function monthsToNubmer($month)
    {
        if ($month > 8) {
            return $month - 8;
        } else {
            return $month + 4;
        }
    }

    public function index()
    {
        return view($this->view('index'));
    }

    public function data(Request $request)
    {
        $query = NewsPaper::query();
        $newspapers = $query->orderBy('publication_date', 'DESC')
            ->get()
            ->map(function ($newspaper) {
                return new \App\BusinessLogic\NewsPaper\NewsPaper($newspaper);
            });

        return Datatables::of($newspapers)
            ->addColumn('id', function ($new) {
                return $new->getId();
            })
            ->addColumn('number', function ($new) {
                return $new->getNumber();
            })
            ->addColumn('cover', function ($new) {
                return "<a href='" . $new->getCover() . "' data-fancybox data-caption='" . $new->getId() . "'>Открыть</a>";
            })
            ->addColumn('file', function ($new) {
                return "<a href='" . $new->getPublication() . "'>Открыть</a>";
            })
            ->addColumn('publication_date', function ($new) {
                return $new->getPublicationDate();
            })
            ->addColumn('active', function ($new) {
                return $new->getActiveStatusStr();
            })
            ->addColumn('actions', function ($new) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . $new->getEditUrl() . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-news-js' data-route='" . $new->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions','file','cover'])
            ->make(true);
    }

    public function create()
    {
        return view($this->view('create'));
    }

    public function store(Request $request)
    {
        $newsPaper = new NewsPaper();
        $publicationDate = Carbon::createFromFormat('d.m.Y', $request->publication_date);
        $newsPaper->publication_date = $publicationDate->format('Y-m-d');
        $newsPaper->number = $this->monthsToNubmer($publicationDate->month);
        $newsPaper->active = $request->active ? true : false;
        $newsPaper->save();

        $newsPaper->cover_path = StorageFile::putNewsPaperFile($newsPaper->id, $request->file('cover'));
        $newsPaper->publication_path = StorageFile::putNewsPaperFile($newsPaper->id, $request->file('file'));
        $newsPaper->save();

        return redirect()->route('admin.news-papers');
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function delete($id)
    {

    }

    private function view($template)
    {
        return 'admin.news-paper.' . $template;
    }
}