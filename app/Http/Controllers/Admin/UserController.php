<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{

    public function index()
    {
        $this->authorize('index', User::class);
        $users = User::query()->with(['profile', 'permissions'])
            ->get();

        return view('admin.users.index', ['users' => $users]);

    }

    public function data(Request $request)
    {

    }

    public function create()
    {
        $this->authorize('create', User::class);
        $employees = Employee::query()->whereDoesntHave('credentials')
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get();

        $permissions = Permission::query()
            ->orderBy('name')
            ->orderBy('sort')
            ->get()
            ->groupBy('group_name');

        return view('admin.users.create', [
            'employees' => $employees,
            'permissions' => $permissions
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create', User::class);
        $user = new User();
        $user->name = $request->login;
        $user->login = $request->login;
        $user->password = bcrypt($request->password);
        $user->user_profile_id = $request->employee_id;
        $user->save();
        $user->syncPermissions($request->input('permissions'));

        return redirect()->route('admin.users.index');
    }

    public function edit($id)
    {

        $this->authorize('edit', User::class);
        $user = User::query()->with('permissions')
            ->findOrFail($id);

        $permissions = Permission::query()
            ->orderBy('name')
            ->orderBy('sort')
            ->get()
            ->groupBy('group_name');
        
        $user_permissions = $user->permissions->map(function ($permission) {
            return $permission->name;
        })->toArray();


        return view('admin.users.edit', ['user' => $user, 'permissions' => $permissions, 'user_permissions' => $user_permissions]);
    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', User::class);
        $user = User::findOrFail($id);
        $user->login = $request->login;

        if (!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        $user->syncPermissions($request->input('permissions'));

        return redirect()->route('admin.users.index');
    }

    public function delete($id)
    {
        $this->authorize('delete', User::class);

        User::destroy($id);

        return response()->json([
            'success' => true,
            'message' => 'Данные для доступа были удалены'
        ]);
    }

}