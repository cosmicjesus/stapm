<?php

namespace App\Http\Controllers\Admin;


use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\SocialPartner;
use App\Models\SocialPartnerFile;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SocialPartnerController extends Controller
{

    private $dir = 'admin.social-partners.';

    private function uploadFiles(Request $request, $partner)
    {

        foreach ($request->file('files') as $key => $file) {
            if (isset($request->names[$key])) {
                $fileModel = new SocialPartnerFile();
                $fileModel->social_partner_id = $partner->id;
                $fileModel->name = $request->names[$key];
                $fileModel->path = ',e,e,e,e';
                $fileModel->save();

                $pathToFile = StorageFile::putPartnerFile($partner->id, $file);

                $fileModel->path = $pathToFile;
                $fileModel->save();
            }

        }
    }

    public function index()
    {
        $partners = SocialPartner::query()
            ->orderBy('sort')
            ->get()
            ->map(function ($partner) {
                return new \App\BusinessLogic\SocialPartner\SocialPartner($partner);
            });
        return view($this->dir . 'index', ['partners' => $partners]);
    }

    public function create()
    {
        return view($this->dir . 'create');
    }

    public function store(Request $request)
    {
        $model = new SocialPartner();
        $model->name = $request->name;
        $model->image_path = '11';
        $model->active = isset($request->active) ? true : false;
        $model->site_url = $request->site_url;
        $model->sort = $request->sort;
        $model->description = $request->description;
        $model->contract_type = $request->contract_type;
        $model->start_date = Carbon::createFromFormat('d.m.Y', $request->start_date)->format('Y-m-d');
        $model->end_date = Carbon::createFromFormat('d.m.Y', $request->end_date)->format('Y-m-d');
        $model->description = $request->description;
        $model->save();

        $image = StorageFile::putPartnerFile($model->id, $request->file('logo'));
        $model->image_path = $image;
        $model->save();
        if (isset($request->names) && count($request->file('files'))) {
            $this->uploadFiles($request, $model);
        }
        return redirect()->route('admin.social-partners');
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function delete($id)
    {
        try {
            SocialPartner::destroy($id);
            StorageFile::deletePartnerDir($id);
            return response()->json([
                'success' => false,
                'message' => 'Запись успешно удалена'
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

}