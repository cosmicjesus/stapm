<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SiteSettingsController extends Controller
{

    public function edit()
    {
        return view('admin.settings');
    }

    public function update(Request $request)
    {
        $req_params = $request->input();
        unset($req_params['_token']);
        foreach ($req_params as $key => $param) {
            Cache::forever($key, $param);
        }

        return redirect()->back();
    }
}