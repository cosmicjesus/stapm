<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\CategoriesDocument;
use App\Models\Document;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class DocumentController extends Controller
{

    protected $categoriesFromSelect;

    public function __construct()
    {
        $this->categoriesFromSelect = $categories = CategoriesDocument::orderBy('parent_id')
            ->orderBy('sort')
            ->get()
            ->keyBy('id')
            ->map(function ($category) {
                $cat = new \App\BusinessLogic\Document\CategoriesDocument($category);
                $name = $cat->getParent() ? $cat->getName() . " | " . $cat->getParent() : $cat->getName();
                return $name;
            });
    }

    public function indexCategories()
    {
        $categories = CategoriesDocument::orderBy('parent_id')
            ->orderBy('sort')
            ->get()
            ->map(function ($category) {
                return new \App\BusinessLogic\Document\CategoriesDocument($category);
            });

        return view('admin.documents.categories.index', ['categories' => $categories]);
    }

    public function createCategoryPage()
    {
        return view('admin.documents.categories.create', ['categories' => $this->categoriesFromSelect]);
    }

    public function createCategory(Request $request)
    {
        $messages = [
            'name.required' => "Поле 'Наименование' не заполнено",
            'sort.required' => "Поле 'Порядок сортировки' не заполнено",
            'sort.numeric' => "Значение 'Порядок сортировки' должно быть числом",
            'sort.min' => "Минимальное значение поля сортировки должно быть 1",
        ];

        $this->validate($request, [
            'name' => 'required',
            'sort' => 'required|numeric|min:1'
        ], $messages);

        $category = new CategoriesDocument();
        $category->name = $request->name;
        $category->sort = $request->sort;
        $category->parent_id = empty($request->category_id) ? null : $request->category_id;
        $category->active = isset($request->active) ? true : false;
        $category->save();
        return redirect()->route('admin.documents.categories');
    }

    public function editCategoryPage(Request $request, $id)
    {

        $category = CategoriesDocument::findOrFail($id);

        return view('admin.documents.categories.edit', ['categories' => $this->categoriesFromSelect, 'category' => $category]);
    }

    public function editCategory(Request $request, $id)
    {
        $messages = [
            'name.required' => "Поле 'Наименование' не заполнено",
            'sort.required' => "Поле 'Порядок сортировки' не заполнено",
            'sort.numeric' => "Значение 'Порядок сортировки' должно быть числом",
            'sort.min' => "Минимальное значение поля сортировки должно быть 1",
        ];

        $this->validate($request, [
            'name' => 'required',
            'sort' => 'required|numeric|min:1'
        ], $messages);

        $category = CategoriesDocument::findOrFail($id);
        $category->name = $request->name;
        $category->sort = $request->sort;
        $category->parent_id = empty($request->category_id) ? null : $request->category_id;
        $category->active = isset($request->active) ? true : false;
        $category->save();
        return redirect()->route('admin.documents.categories');

    }

    public function deleteCategory($id)
    {
        try {
            $category = CategoriesDocument::with(['childs', 'files'])->findOrFail($id);
            $name = $category->name;

            $countChilds = $category->childs->count();
            $countFiles = Document::query()->whereJsonContains('category_ids', (string)$id)->count();

            if ($countFiles) {
                return response()->json([
                    'success' => false,
                    'message' => 'Категория «' . $name . '» не может быть удалена, т.к содержит ' . $countFiles . " " . getNumEnding($countFiles, [
                            'загруженный документ',
                            'загруженных документа',
                            'загруженных документов',
                        ])
                ]);
            }

            if ($countChilds) {
                return response()->json([
                    'success' => false,
                    'message' => 'Категория «' . $name . '» не может быть удалена, т.к содержит ' . $countChilds . " " . getNumEnding($countChilds, [
                            'дочернюю категорию',
                            'дочернии категории',
                            'дочерних категорий',
                        ])
                ]);
            }
            $category->delete();
            return response()->json([
                'success' => true,
                'message' => 'Категория «' . $name . '» была успешно удалена'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => "Код: " . $exception->getCode() . " | " . $exception->getMessage()
            ])->setStatusCode(422);
        }
    }

    public function indexDocuments()
    {

        $categories = CategoriesDocument::query()
            ->orderBy('parent_id', 'asc')
            ->orderBy('sort', 'asc')
            ->get()
            ->map(function ($category) {
                return new \App\BusinessLogic\Document\CategoriesDocument($category);
            });
        return view('admin.documents.document.index', ['categories' => $categories]);
    }

    public function documentsData(Request $request)
    {

        $query = Document::query();
        $query->when(filter_var($request->input('category'), FILTER_VALIDATE_INT), function ($q) use ($request) {
            $q->whereJsonContains('category_ids', $request->input('category'));
        });
        $documents = $query->orderBy('sort', 'asc')
            ->get()
            ->map(function ($document) {
                return new \App\BusinessLogic\Document\Document($document);
            });

        return Datatables::of($documents)
            ->addColumn('id', function ($document) {
                return $document->getId();
            })
            ->addColumn('name', function ($document) {
                return $document->getName();
            })
            ->addColumn('category', function ($document) {
                $tpl = "<ul>#CONTENT#</ul>";
                $items = '';

                foreach ($document->getCategories() as $category) {
                    $items .= '<li>' . $category . '</li>';
                }

                return str_replace('#CONTENT#', $items, $tpl);
            })
            ->addColumn('file', function ($document) {
                return "<a href='" . $document->getPath() . "' target='_blank'>Открыть</a>";
            })
            ->addColumn('sort', function ($document) {
                return $document->getSort();
            })
            ->addColumn('active', function ($document) {
                return $document->getActiveStr();
            })
            ->addColumn('actions', function ($document) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . $document->getEditUrl() . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-document-js' data-name='" . $document->getName() . "' data-url='" . $document->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'file', 'category'])
            ->make(true);

    }

    public function documentCreatePage()
    {
        return view('admin.documents.document.create', ['categories' => $this->categoriesFromSelect]);
    }

    public function documentCreate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_ids' => 'required',
            'file' => 'required',
            'sort' => 'required|numeric|min:1'
        ]);


        $model = new Document();
        $model->category_ids = $request->input('category_ids');
        $model->name = $request->name;
        $model->path = StorageFile::putDocument($request->file('file'));
        $model->sort = $request->sort;
        $model->active = isset($request->active) ? true : false;
        $model->save();
        return redirect()->route('admin.documents');

    }

    public function documentEditPage($id)
    {
        $model = Document::findOrFail($id);
        $document = new \App\BusinessLogic\Document\Document($model);
        $category_ids = is_array($model->category_ids) ? $model->category_ids : [];
        return view('admin.documents.document.edit', ['category_ids' => $category_ids, 'categories' => $this->categoriesFromSelect, 'document' => $document]);
    }

    public function documentEdit(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_ids' => 'required',
            'sort' => 'required|numeric|min:1'
        ]);

        $category_ids = $request->input('category_ids');
        sort($category_ids);
        $model = Document::findOrFail($id);
        $model->category_ids = $category_ids;
        $model->name = $request->name;
        if ($request->file('file')) {
            $old = $model->path;
            $model->path = StorageFile::putDocument($request->file('file'), $old);
        }
        $model->sort = $request->sort;
        $model->active = isset($request->active) ? true : false;
        $model->save();
        return redirect()->route('admin.documents');
    }

    public function documentDelete($id)
    {
        try {
            $document = Document::query()->findOrFail($id);
            $name = $document->name;
            StorageFile::deleteDocument($document->path);
            $document->delete();

            return response()->json([
                'success' => true,
                'message' => 'Документ «' . $name . '» был успешно удален'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код:" . $exception->getCode() . " " . $exception->getMessage()
            ])->setStatusCode(422);
        }

    }

}