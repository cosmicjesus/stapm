<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Position;
use App\Models\PositionType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;

class PositionController extends Controller{


    protected $types;

    public function __construct()
    {
        $this->types=PositionType::all()
            ->keyBy('id')
            ->map(function ($position){
                return $position->name;
            })->toArray();
    }

    public function all(){
        return view('admin.positions.index');
    }

    public function allData(Request $request){
        $positions = Position::with('type')->with('employee_position');

        return Datatables::of($positions)
            ->filter(function ($query) use ($request) {
                if ($request->has('name')) {
                    $query->where('name', 'like', "%{$request->get('name')}%");
                }
                $type=(boolean)$request->get('type');
                if ($type) {
                    $query->where('position_type_id', $request->get('type'));
                }
            })
            ->addColumn('id',function ($position){
                return $position->id;
            })
            ->addColumn('name',function ($position){
                return $position->name;
            })
            ->addColumn('type',function ($position) {
                return $position->type->name;
            })
            ->addColumn('count_employees',function ($position){
                return $position->employee_position->count();
            })
            ->addColumn('actions',function ($position){
                $table="<div class='table-buttons'>#CONTENT#</div>";
                $buttons="<a class='btn btn-warning btn-xs' href=".route('admin.position.edit',['id'=>$position->id])."><i class='fa fa-edit'></i></a>";

                if ($position->can_remove){
                    $buttons.="<button class='btn btn-danger btn-xs delete-position-js' data-id=".$position->id."><i class='fa fa-trash'></i></button>";
                }
                $str=str_replace('#CONTENT#',$buttons,$table);
                return $str;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function createPage(){

        $types=$this->types;

        return view('admin.positions.create',['types'=>$types]);
    }

    public function create(Request $request){
            $messages=[
              'name.required'=>"Поле 'Наименование' не заполнено",
              'name.unique'=>"Должность с таким названием уже существует",
              'type.required'=>"Не выбран тип должности",
            ];

            $this->validate($request,[
                'name'=>'required|unique:positions,name',
                'type'=>'required'
            ],$messages);

            $model=new Position();
            $model->name=$request->name;
            $model->position_type_id=$request->type;
            $model->save();

            return redirect()->route('admin.positions');
    }

    public function editPage($id){
        try{
            $types=$this->types;
            $position=Position::findOrFail($id);
            return view('admin.positions.edit',['types'=>$types,'position'=>$position]);
        }catch (\Exception $exception){
            return redirect()->route('admin.positions');
        }
    }

    public function edit(Request $request,$id){
        $messages=[
            'name.required'=>"Поле 'Наименование' не заполнено",
            'name.unique'=>"Должность с таким названием уже существует",
            'type.required'=>"Не выбран тип должности",
        ];
        $this->validate($request,[
            'name'=>[
                'required',
                Rule::unique('positions')->ignore($id,'id')
            ],
            'type'=>'required'
        ],$messages);

        $model=Position::findOrFail($id);
        $model->name=$request->name;
        $model->position_type_id=$request->type;
        $model->save();

        return redirect()->route('admin.positions');
    }

    public function delete($id){
        try{
            $model=Position::findOrFail($id);

            $str="Должность '".$model->name."' была успешно удалена";
            //Position::destroy($id);
            return response($str,200);
        }catch (\Exception $exception){
            return response('Произошла ошибка при удалении. Попробуйте позже или обратитесь к администратору',422);
        }

    }

}