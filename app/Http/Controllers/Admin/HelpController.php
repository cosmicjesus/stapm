<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\Reference\Reference;
use App\BusinessLogic\Student\Student;
use App\Http\Controllers\Controller;
use App\Models\Help;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Writer\Word2007\Element\Text;
use Yajra\Datatables\Datatables;

class HelpController extends Controller
{
    public function trainigHelp(Request $request)
    {
        $student = Student::buildByID($request->input('student_id'));
        $number = Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->max('number');
        $data = [
            'number' => $number + 1,
            'request' => $request->input()
        ];
        if ($request->ajax()) {
            try {
                return response()->json(
                    ['result' => view('admin.helps.training',
                        [
                            'repeat' => 1,
                            'student' => $student,
                            'data' => $data,
                            'withStyle' => false
                        ]
                    )->render()])->setStatusCode(200);
            } catch (\Throwable $e) {
            }
        }

        $pdf = PDF::loadView('admin.helps.training',
            [
                'repeat' => 2,
                'student' => $student,
                'data' => $data,
                'withStyle' => true
            ]
        );
        $reference = new Help();
        $reference->student_id = $request->input('student_id');
        $reference->number = $number + 1;
        $reference->date_of_issue = Carbon::now()->format('Y-m-d H:i:s');
        $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
        $reference->params = json_encode($request->input());
        $reference->type = 'training';
        $reference->status = 'issued';
        $reference->save();
        return $pdf->stream('Справка.pdf');

    }

    public function archive()
    {
        return view('admin.reference-list');
    }

    public function data(Request $request)
    {
        $query = Help::query()
            ->orderBy('date_of_application', 'DESC');


        $query->when($request->get('all_helps') === 'false', function ($q) {
            return $q->where('status', '=', 'booked');
        });
        $type = $request->type;
        //dd($type, $type == 'training', $type == 'money');
        $query->when($type == 'training' || $type == 'money', function ($q) use ($type) {
            return $q->where('type', $type);
        });

        $helps = $query->get()->map(function ($help) {
            return new Reference($help);
        });

        return Datatables::of($helps)
            ->addColumn('id', function ($help) use ($request) {
                return $help->getId();
            })
            ->addColumn('student', function ($help) {
                return $help->getStudent();
            })
            ->addColumn('type', function ($help) {
                return $help->getType();
            })
            ->addColumn('dates', function ($help) {
                return $help->getDates();
            })
            ->addColumn('params', function ($help) {
                return $help->getParams();
            })
            ->addColumn('status', function ($help) {
                return $help->getStatus();
            })
            ->addColumn('actions', function ($help) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "";
                if ($help->getStatusStr() == 'booked') {
                    $buttons = "<a href='#' data-toggle='modal' data-target='#makeHelp' class='btn btn-primary make-help-js' data-id='" . $help->getId() . "' data-params='" . $help->getParams(true) . "'>Выдать</a>";
                }
                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'dates', 'params'])
            ->make(true);
    }

    public function deliveryOrdered(Request $request, $id)
    {
        $reference = Help::query()->findOrFail($id);

//        dd($request->input(), $reference);
        $student = Student::buildByID($request->input('student_id'));
        $data = [
            'number' => $reference->number,
            'request' => $request->input()
        ];
        //dd($request->input(), $reference);

        $count = $request->input('count') < 2 ? 2 : $request->input('count');
        $pdf = PDF::loadView('admin.helps.training',
            [
                'repeat' => $count,
                'student' => $student,
                'data' => $data,
                'withStyle' => true
            ]
        );
        $reference->params = $request->input();
        $reference->date_of_issue = Carbon::now()->format('Y-m-d H:i:s');
        $reference->status = 'issued';
        $reference->save();

        return $pdf->stream('Справка.pdf');
    }

    public function exportMoney(Request $request)
    {
        $dates = [
            Carbon::createFromFormat('d.m.Y', $request->input('start_period'))->setTime(9, 0, 0)->format('Y-m-d H:i:s'),
            Carbon::createFromFormat('d.m.Y', $request->input('end_period'))->setTime(9, 0, 0)->format('Y-m-d H:i:s')
        ];

        $referensesModel = Help::query()
            ->where('type', 'money')
            ->whereBetween('date_of_application', $dates)
            ->get();

        $referenses = $referensesModel->map(function ($ref) {
            return new Reference($ref);
        });

        foreach ($referensesModel as $item) {
            $item->status = 'issued';
            $item->save();
        }

        $pdf = PDF::loadView('admin.helps.export-money',
            [
                'references' => $referenses
            ]
        );
        return $pdf->stream('Список справок для бухгалтерии.pdf');
    }

    public function voenkomat(Request $request, $id)
    {
        $student = Student::buildByID($id);

        $pdf = PDF::loadView('admin.helps.voenkomat',
            [
                'withStyle' => true,
                'student' => $student,
                'input' => $request->all()
            ]
        );
        return $pdf->stream('Справка в военкомат.pdf');
    }
}