<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Decree;
use App\Models\EducationPlan;
use App\Models\Help;
use App\Models\ProfessionProgram;
use App\Models\Student;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use PDF;

class GroupController extends Controller
{
    public function index()
    {
        $this->authorize('index', 'Groups');

        $programs = ProfessionProgram::orderBy('profession_id')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });

        return view('admin.groups.index', ['programs' => $programs]);
    }

    public function data(Request $request)
    {
        $this->authorize('index', 'Groups');
        $model = TrainingGroup::query()
            ->where('status', 'teach');
        $model->when(filter_var($request->course, FILTER_VALIDATE_INT), function ($query) {
            $query->where('course', (int)\request('course'));
        });

        $model->when(filter_var($request->program_id, FILTER_VALIDATE_INT), function ($query) {
            $query->where('profession_program_id', \request('program_id'));
        });

        $groups = $model->orderBy('profession_program_id', 'asc')
            ->orderBy('course', 'asc')
            ->get()
            ->map(function ($group) {
                return new \App\BusinessLogic\TrainingGroup\TrainingGroup($group);
            });

        return Datatables::of($groups)
            ->addColumn('id', function ($group) {
                return $group->getId();
            })
            ->addColumn('step', function ($group) {
                return $group->getStep();
            })
            ->addColumn('name', function ($group) {
                return $group->getName();
            })
            ->addColumn('program', function ($group) {
                return $group->getProgram()->getName();
            })
            ->addColumn('plan', function ($group) {
                return "<a href='" . $group->getEducationPlan()->getFilePath() . "' target='_blank'>" . $group->getEducationPlan()->getName() . "</a>";
            })
            ->addColumn('max', function ($group) {
                return $group->getMaxPeople();
            })
            ->addColumn('countStudents', function ($group) {
                return $group->getCountStudents();
            })
            ->addColumn('curator', function ($group) {
                return $group->getCurator()->getFullName();
            })
            ->addColumn('actions', function ($group) use ($request) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<form action='" . $group->getPrintReferencesUrl() . "' method='post' target='_blank'>
<button class='btn btn-xs btn-success' type='submit' title='Напечатать справки на всю группу'>Справки</button></form>";
                $eduPlanPeriods = $group->getEducationPlan()->getCountPeriod();
                if ($group->getPeriod() < $eduPlanPeriods) {
                    $buttons .= "<a class='btn btn-xs btn-primary group-transfer-js' href='#' data-unique='" . $group->getUniqueCode() . "' data-url='" . $group->getTransferUrl() . "'>
                                <i class='fa fa-exchange' aria-hidden='true'></i>
                            </a>";
                } else {
                    $buttons .= "<a class='btn btn-xs btn-warning group-release-js' data-toggle='modal' data-target='#releaseGroupModal' data-id='" . $group->getId() . "'>
                                <i class='fa fa-sign-out' aria-hidden='true'></i>
                            </a>";
                }

                $buttons .= "<a class='btn btn-xs btn-danger delete-group-js' href='#' data-url='" . $group->getDeleteUrl() . "'><i class='fa fa-trash'></i></a>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'plan'])
            ->make(true);
    }

    public function createPage()
    {
        $this->authorize('create', 'Groups');
        $programs = ProfessionProgram::query()
            ->whereHas('profession', function ($q) {
                $q->orderBy('code');
            })->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });

        $eduPlans = EducationPlan::query()
            ->orderBy('profession_program_id')
            ->orderBy('start_training', 'ASC')
            ->get()
            ->map(function ($plan) {
                return new \App\BusinessLogic\EducationPlan\EducationPlan($plan);
            });

        return view('admin.groups.create', ['programs' => $programs, 'plans' => $eduPlans]);
    }

    public function create(Request $request)
    {
        $this->authorize('create', 'Groups');

        $messages = [

        ];

        $this->validate($request, [
            'pattern' => 'required',
            'unique_code' => [
                'required',
                Rule::unique('training_groups')
            ]
        ]);

        $group = new TrainingGroup();
        $group->profession_program_id = $request->program_id;
        $group->education_plan_id = $request->education_plan;
        $group->employee_id = $request->curator;
        $group->unique_code = $request->unique_code;
        $group->pattern = $request->pattern;
        $group->max_people = $request->max_people;
        $group->status = 'teach';
        $group->save();

        return redirect()->route('admin.groups');
    }

    public function transfer($id)
    {
        try {
            $this->authorize('actions', 'Groups');

            $model = TrainingGroup::findOrFail($id);
            $period = $model->period;
            $unique = $model->unique_code;

            if ($model->term_type == 'Course') {
                $model->course = $model->course + 1;
                $model->period = $model->period + 1;
                $decree = Decree::query()->firstOrCreate([
                    'number' => $unique . " - " . $model->course . "/" . ($model->course + 1),
                    'date' => Carbon::now()->format('Y-m-d')
                ]);

                $students = Student::query()->with('decrees')->where('group_id', $model->id)->where('status', 'student')->get();

                foreach ($students as $student) {
                    $student->decrees()->attach($decree->id, ['type' => 'internal_transfer', 'reason' => null, 'group_to' => str_replace('{Курс}', $model->course, $model->pattern)]);
                }

            } else {
                if ($period % 2 == 0) {
                    $courseFrom = $model->course;
                    $model->course = $model->course + 1;
                    $model->period = $model->period + 1;
                    $decree = Decree::query()->firstOrCreate([
                        'number' => $unique . " - " . $courseFrom . "/" . $model->course,
                        'date' => Carbon::now()->format('Y-m-d')
                    ]);

                    $students = Student::query()->with('decrees')->where('group_id', $model->id)->where('status', 'student')->get();

                    foreach ($students as $student) {
                        $student->decrees()->attach($decree->id, ['type' => 'internal_transfer', 'reason' => null, 'group_to' => str_replace('{Курс}', $model->course, $model->pattern)]);
                    }

                } else {
                    $model->period = $model->period + 1;
                }
            }
            $model->save();


            return response()->json([
                'success' => true,
                'message' => 'Группа «' . $model->unique_code . '» была переведена на следующий  период обучения'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }

    }

    public function releaseGroup(Request $request)
    {
        try {
            $data = $request->input();


            $group = TrainingGroup::query()->findOrFail($data['group_id']);
            $group->status = 'release';
            $group->expelled_decree_number = $request->input('decree_number');
            $group->expelled_decree_date = Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d');
            $group->save();

            EducationPlan::hideAfterReleaseGroup($group->education_plan_id);

            $students = Student::query()
                ->where('group_id', $data['group_id'])
                ->where('status', 'student')
                ->get();

            $decree = Decree::query()->firstOrCreate([
                'number' => $request->input('decree_number'),
                'date' => Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d')
            ]);
            $groupString = str_replace('{Курс}', $group->course, $group->pattern);
            foreach ($students as $student) {
                $student->status = 'expelled';
                $student->save();
                $student->decrees()->attach($decree->id, [
                        'type' => 'allocation',
                        'group_from' => $groupString,
                        'reason' => 'endCollege',
                    ]
                );
            }

            return response()->json([
                'success' => true,
                'message' => 'Группа ' . $groupString . ' была успешно выпущена',
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function delete($id)
    {
        $this->authorize('actions', 'Groups');

        $model = TrainingGroup::findOrFail($id);
        $fullName = str_replace('{Курс}', $model->course, $model->pattern);
        $countStudents = 1;
        if ($countStudents > 0) {
            return response()->json([
                'success' => false,
                'message' => 'Удаление группы «' . $fullName . '» невозможно, т.к в ней имеется ' . $countStudents . " " . getNumEnding($countStudents, ['студент', 'студента', 'студентов'])
            ]);
        }

        $model->delete();

        return response()->json([
            'success' => true,
            'message' => 'Группа «' . $fullName . '» была успешно удален'
        ]);
    }

    public function printReferences(Request $request, $id)
    {
        $studentsModel = Student::query();
        $result = $studentsModel->where('group_id', $id)
            ->where('status', 'student')
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get()
            ->map(function ($student) {
                return new \App\BusinessLogic\Student\Student($student);
            });

        $groupModel = TrainingGroup::query()->findOrFail($id);
        $group = new \App\BusinessLogic\TrainingGroup\TrainingGroup($groupModel);

        $students = [];

        $number = Help::query()->whereYear('date_of_issue', Carbon::now()->format('Y'))->max('number');
        $references = [];
        foreach ($result as $student) {
            $number++;
            $date = env('REFERENCE_DATE', Carbon::now()->month(9)->day(1)->format('d.m.Y'));
            $input = [
                'date' => $date,
                'decree' => $student->getDecrees()->last()->getNumber(),
                'student_id' => $student->getId(),
                'start_training' => $student->getDateOfActualTransfer() ? $student->getDateOfActualTransfer() : $group->getEducationPlan()->getStartTrainig(),
                'end_training' => $group->getEducationPlan()->getEndTrainig()
            ];
            $references[] = [
                'student_id' => $input['student_id'],
                'params' => json_encode($input),
                'number' => $number,
                'type' => 'training',
                'date_of_issue' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => 'issued'
            ];
            $students[] = [
                'full_name' => $student->getFullName(),
                'start_training' => $input['start_training'],
                'end_training' => $input['end_training'],
                'program_type' => $student->getProgram()->getTypeId() == 1 ? 'специальности' : 'профессии',
                'program_name' => $student->getProgram()->getName(),
                'education_form' => $student->getProgram()->getEducationForm(),
                'course' => $student->getGroup()->getCourse(),
                'decree' => $input['decree'],
                'number' => $number,
                'date' => $date
            ];
        }

        Help::query()->insert($references);
        $pdf = PDF::loadView('admin.helps.mass-training',
            [
                'repeat' => 2,
                'students' => $students,
                'withStyle' => true
            ]
        );
        return $pdf->stream('Справки группы ' . $group->getName() . '.pdf');

    }

    public function reportGroupAvg(Request $request)
    {
        $groups = TrainingGroup::query()
            ->where('status', 'teach')
            ->where('course', 1)
            ->get()
            ->map(function ($group) {
                return new \App\BusinessLogic\TrainingGroup\TrainingGroup($group);
            });

        $pdf = PDF::loadView('admin.reports.group-avg',
            [
                'groups' => $groups,
                'withStyle' => true
            ]
        );
        return $pdf->stream('Средний балл по группам первого курса.pdf');
    }
}