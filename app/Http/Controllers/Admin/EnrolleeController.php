<?php

namespace App\Http\Controllers\Admin;

use App\BusinessLogic\Enrollee\Enrollee;
use App\BusinessLogic\ProfessionProgram\ProfessionPriem;
use App\BusinessLogic\Student\StudentService;
use App\Export\ExportEnrollee;
use App\Helpers\Settings;
use App\Http\Controllers\Controller;
use App\Models\Decree;
use App\Models\Education;
use App\Models\ProfessionProgram;
use App\Models\Student;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Yajra\Datatables\Datatables;
use PDF;

class EnrolleeController extends Controller
{

    protected $service;
    protected $education;

    public function __construct()
    {
        try {
            $this->service = new StudentService();
            $this->education = Education::query()
                ->get()
                ->keyBy('id')
                ->map(function ($edu) {
                    return $edu->name;
                })
                ->toArray();
        } catch (\Exception $e) {
        }
    }

    public function index()
    {
        $this->authorize('index', 'Enrollee');

        $programs = ProfessionProgram::whereHas('priem')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });
        return view('admin.enrollees.index', ['programs' => $programs]);
    }

    public function data(Request $request)
    {
        $service = $this->service->orderByFullName();

        if (!empty($request->get('name'))) {
            $service->where('lastname', 'LIKE', "%{$request->get('name')}%")
                ->orWhere('firstname', 'LIKE', "%{$request->get('name')}%")
                ->orWhere('middlename', 'LIKE', "%{$request->get('name')}%");
        }

        if (!empty($request->get('profession'))) {
            $service->where('profession_program_id', '=', $request->get('profession'));
        }

        if (!empty($request->get('documents'))) {
            $value = $request->get('documents') == 1 ? true : false;

            $service->where('has_original', '=', $value);
        }
        if (!empty($request->get('medical'))) {
            $value = $request->get('medical') == 1 ? true : false;

            $service->where('has_certificate', '=', $value);
        }

        if ($request->get('contract_target_set') === 'true') {
            $service->where('contract_target_set', '=', 1);
        }

        if ($request->get('no_snils') === 'true') {
            $service->where('snils', '=', null);
        }

        if ($request->get('no_inn') === 'true') {
            $service->where('inn', '=', null);
        }

        if ($request->get('no_passport') === 'true') {
            $service->where('passport_data', '=', null);
        }

        if ($request->get('no_addresses') === 'true') {
            $service->where('addresses', '=', null);
        }
        if (filter_var($request->get('need_hostel'), FILTER_VALIDATE_BOOLEAN)) {
            $service->where('need_hostel', '=', 1);
        }


        $enrolles = $service->onlyEnrollees()->map('enrollee');


        return Datatables::of($enrolles)
            ->addColumn('missingData', function ($enrollee) {
                return $enrollee->getMissingInformation();
            })
            ->addColumn('medical', function ($enrollee) {
                return $enrollee->getMedCertificateStr();
            })
            ->addColumn('need_hostel', function ($enrollee) {
                return $enrollee->getNeedHostelStr();
            })
            ->addColumn('errors', function ($enrollee) {
                if (env('SHOW_ENROLLEE_ERRORS')) {
                    return $enrollee->getErrorData();
                }
            })
            ->addColumn('checkbox', function ($enrollee) {
                //return "<input type='checkbox' class='enrollee_check' data-enrollee='".$enrollee->getFullName()."' value='".$enrollee->getId()."'>";
                return json_encode(['id' => $enrollee->getId(), 'full_name' => $enrollee->getFullName()]);
                //return $enrollee->getId();
            })
            ->addColumn('id', function ($enrollee) {
                return $enrollee->getId();
            })
            ->addColumn('full_name', function ($enrollee) {
                return $enrollee->getFullName();
            })
            ->addColumn('program', function ($enrollee) {
                $name = $enrollee->getProgram()->getNameWithForm();

                if ($enrollee->getContractTargetSet()) {
                    $name .= " (Целевой набор)";
                }

                return $name;
            })
            ->addColumn('hasOriginal', function ($enrollee) {
                return $enrollee->getHasOriginalStr();
            })
            ->addColumn('snils', function ($enrollee) {
                return $enrollee->getSnils();
            })
            ->addColumn('inn', function ($enrollee) {
                return $enrollee->getInn();
            })
            ->addColumn('avg', function ($enrollee) {
                return $enrollee->getAvg();
            })
            ->addColumn('phone', function ($enrollee) {
                return $enrollee->getPhone();
            })
            ->addColumn('passport', function ($enrollee) {
                if (is_null($enrollee->getPassportData())) {
                    return 'Не заполнено';
                }
                return 'Заполнено';
            })
            ->addColumn('language', function ($enrollee) {

                return $enrollee->getLanguage();
            })
            ->addColumn('actions', function ($employee) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-primary btn-xs' href=" . $employee->getProfileUrl() . "><i class='fa fa-user'></i></a>";
                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['checkbox', 'actions', 'missingData', 'errors'])
            ->make(true);
    }


    public function createPage()
    {
        $this->authorize('create', 'Enrollee');

        $programs = ProfessionProgram::whereHas('priem')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });
        return view('admin.enrollees.create', ['programs' => $programs, 'educations' => $this->education]);
    }

    public function create(Request $request)
    {

        try {
            $this->validate($request, [
                'lastname' => 'required',
                'firstname' => 'required',
                'birthday' => 'date_format:d.m.Y',
                'education' => 'required',
                'profession' => 'required',
                'graduation_date' => 'date_format:d.m.Y',
                'start_date' => 'date_format:d.m.Y',
            ]);

            $selectInBase = Student::findUnique($request);
            if (!is_null($selectInBase)) {
                return response()->json([
                    'success' => false,
                    'message' => 'В базе уже существует абитуриент с такими данными'
                ])->setStatusCode(200);
            }


            $enrollee = new Student();
            $enrollee->lastname = $request->lastname;
            $enrollee->firstname = $request->firstname;
            $enrollee->middlename = $request->middlename;
            $enrollee->gender = $request->gender;
            $enrollee->education_id = $request->education;
            $enrollee->profession_program_id = $request->profession;
            $enrollee->nationality = $request->nationality;
            $enrollee->avg = $request->avg;
            $enrollee->snils = $request->snils;
            $enrollee->inn = $request->inn;
            $enrollee->language = $request->language;
            $enrollee->phone = empty($request->phone) ? null : $request->phone;
            $enrollee->birthday = Carbon::createFromFormat('d.m.Y', $request->birthday)->format('Y-m-d');
            $enrollee->graduation_date = Carbon::createFromFormat('d.m.Y', $request->graduation_date)->format('Y-m-d');
            $enrollee->start_date = Carbon::createFromFormat('d.m.Y', $request->start_date)->format('Y-m-d');
            $enrollee->has_original = isset($request->has_original) ? true : false;
            $enrollee->has_certificate = isset($request->has_certificate) ? true : false;
            $enrollee->need_hostel = isset($request->need_hostel) ? true : false;
            $enrollee->person_with_disabilities = isset($request->person_with_disabilities) ? true : false;
            $enrollee->contract_target_set = isset($request->contract_target_set) ? true : false;
            $enrollee->graduation_organization_name = isset($request->graduation_organization_name) ? $request->graduation_organization_name : null;
            $enrollee->graduation_organization_place = isset($request->graduation_organization_place) ? $request->graduation_organization_place : null;
            $enrollee->status = 'enrollee';
            $enrollee->save();

            $passport = $request->input('passport');
            $date_issuance = Carbon::createFromFormat('d.m.Y', $passport['issuanceDate'])->format('Y-m-d');

            $enrollee->passport_issuance_date = $date_issuance;
            $enrollee->passport_data = json_encode($passport);

            $params = [
                'registration' => $request->input('registration'),
                'residential' => $request->input('residential'),
                'place_of_stay' => $request->input('place_of_stay')
            ];
            $addresses = [];
            $addresses['place_of_stay'] = $params['place_of_stay'];
            $addresses['registration'] = $params['registration'];
            $addresses['residential'] = $params['residential'];

            if (isset($request->isAddressSimilar)) {
                $addresses['isAddressSimilar'] = true;
                $addresses['residential'] = $params['registration'];
            } else {
                $addresses['isAddressSimilar'] = false;
            }

            if (isset($request->isAddressPlaceOfStay)) {
                $addresses['isAddressPlaceOfStay'] = true;
                $addresses['place_of_stay'] = $params['registration'];
            } else {
                $addresses['isAddressPlaceOfStay'] = false;
            }
            $enrollee->addresses = json_encode($addresses);
            $enrollee->save();


            $codes = cache('subdivision_codes');

            $hasCode = false;

            foreach ($codes as $key => $code) {
                if (($codes[$key]['code'] == $request->passport['subdivisionCode']) && ($codes[$key]['place'] == $request->passport['issued'])) {
                    continue;
                } else {
                    $codes[] = [
                        'code' => $request->passport['subdivisionCode'],
                        'place' => $request->passport['issued']
                    ];
                }
            }
            $codes1 = collect($codes)->unique('place')->sortBy('code');

            $arr = [];

            foreach ($codes1 as $code) {
                $arr[] = [
                    'code' => $code['code'],
                    'place' => $code['place']
                ];
            }

            $names = cache('names');

            if (!in_array($request->input('firstname'), $names)) {
                $names[] = $request->input('firstname');
                sort($names);
            }

            $middlenames = cache('middlenames');

            if (!in_array($request->input('middlename'), $middlenames)) {
                $middlenames[] = $request->input('middlename');
                sort($middlenames);
            }

            $graduation_organizations_name = cache('graduation_organizations_name');

            if (!is_null($request->input('graduation_organization_name')) && !in_array($request->input('graduation_organization_name'), $graduation_organizations_name)) {
                $graduation_organizations_name[] = $request->input('graduation_organization_name');
                sort($graduation_organizations_name);
            }

            $graduation_organizations_place = cache('graduation_organizations_place');

            if (!is_null($request->input('graduation_organization_place')) && !in_array($request->input('graduation_organization_place'), $graduation_organizations_place)) {
                $graduation_organizations_place[] = $request->input('graduation_organization_place');
                sort($graduation_organizations_place);
            }

            $birthPlaces = cache('birthPlaces');

            if (!in_array(trim($request->passport['birthPlace']), $birthPlaces)) {
                $birthPlaces[] = trim($request->passport['birthPlace']);
                $birthPlaces = array_unique($birthPlaces);
                sort($birthPlaces);
            }


            $ar = [
                'registration',
                'residential',
                'place_of_stay'
            ];

            $regions = cache('regions');

            foreach ($ar as $item) {
                if (!is_null($request->$item['region']) && !in_array($request->$item['region'], $regions)) {
                    $regions[] = $request->$item['region'];
                    sort($regions);
                }
            }

            $settlement = cache('settlement');

            foreach ($ar as $item) {
                if (!is_null($request->$item['settlement']) && !in_array($request->$item['settlement'], $settlement)) {
                    $settlement[] = $request->$item['settlement'];
                    sort($settlement);
                }
            }

            $cities = cache('cities');

            foreach ($ar as $item) {
                if (!is_null($request->$item['city']) && !in_array($request->$item['city'], $cities)) {
                    $cities[] = $request->$item['city'];
                    sort($cities);
                }
            }

            $streets = cache('streets');

            foreach ($ar as $item) {
                if (!is_null($request->$item['street']) && !in_array($request->$item['street'], $streets)) {
                    $streets[] = $request->$item['street'];
                    sort($streets);
                }
            }

            $area = cache('area');

            foreach ($ar as $item) {
                if (!is_null($request->$item['area']) && !in_array($request->$item['area'], $area)) {
                    $area[] = $request->$item['area'];
                    sort($area);
                }
            }


            \Cache::forever('area', $area);
            \Cache::forever('streets', $streets);
            \Cache::forever('cities', $cities);
            \Cache::forever('settlement', $settlement);
            \Cache::forever('regions', $regions);
            \Cache::forever('birthPlaces', $birthPlaces);
            \Cache::forever('graduation_organizations_place', $graduation_organizations_place);
            \Cache::forever('graduation_organizations_name', $graduation_organizations_name);
            \Cache::forever('middlenames', $middlenames);
            \Cache::forever('names', $names);
            \Cache::forever('subdivision_codes', $arr);

            return response()->json([
                'success' => true,
                'message' => 'Абитуриент был успешно добавлен'
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ]);
        }
    }

    public function profile($id)
    {
        try {

            $enrollee = Student::query()->where('status', 'enrollee')->findOrFail($id);

            return view('admin.enrollees.profile', ['enrollee' => new Enrollee($enrollee)]);
        } catch (\Exception $exception) {
            return redirect()->route('admin.enrollees');
        }
    }

    public function edit($id)
    {
        $enrollee = Student::query()->findOrFail($id);
        $programs = ProfessionProgram::whereHas('priem')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });
        return view('admin.enrollees.edit', ['enrollee' => $enrollee, 'programs' => $programs]);
    }

    public function update(Request $request, $id)
    {
        $params = $request->input();
        unset($params['_token']);
        $enrollee = Student::query()->findOrFail($id);

        foreach ($params as $key => $param) {
            $value = $param;

            if ($key == 'birthday') {
                $value = Carbon::createFromFormat('d.m.Y', $param)->format('Y-m-d');
            }

            $enrollee->$key = $value;
        }

        $enrollee->slug = null;
        $enrollee->save();

        $names = cache('names');

        if (!in_array($request->input('firstname'), $names)) {
            $names[] = $request->input('firstname');
            sort($names);
        }

        $middlenames = cache('middlenames');

        if (!in_array($request->input('middlename'), $middlenames)) {
            $middlenames[] = $request->input('middlename');
            sort($middlenames);
        }


        \Cache::forever('middlenames', $middlenames);
        \Cache::forever('names', $names);

        return redirect()->route('admin.enrollee.profile', ['id' => $enrollee->id]);
    }

    public function enrollment(Request $request)
    {
        try {
            $group = TrainingGroup::findOrFail($request->input('group'));
            $decree_date = Carbon::createFromFormat('d.m.Y', $request->input('decree_date'))->format('Y-m-d');
            $decree = Decree::query()->firstOrCreate([
                'number' => $request->input('decree_number'),
                'date' => $decree_date
            ]);
            //dd($request->ids);
            $groupString = str_replace('{Курс}', $group->course, $group->pattern);

            $ids = $request->input('ids');
            $students = Student::query()->findMany($ids);
            foreach ($students as $student) {
                $student->update([
                    'status' => 'student',
                    'profession_program_id' => $group->profession_program_id,
                    'group_id' => $group->id,
                    'enrollment_date' => $decree_date,
                ]);
//                $student->status = 'student';
//                $student->profession_program_id = $group->profession_program_id;
//                $student->group_id = $group->id;
//                $student->enrollment_date = $decree_date;
//                $student->save();
                $student->decrees()->attach($decree['id'], ['type' => 'enrollment', 'group_to' => $groupString]);

            }

            Settings::refreshStudentDataCache();
            return response()->json([
                'success' => true,
                'message' => 'Абитуриенты были успешно зачисленны',
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $ids = $request->input('ids');

            Student::destroy($ids);

            return response()->json([
                'success' => true,
                'message' => "Абитуриенты были удалены",
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }

    }

    protected function prepareFilter(Request $request, StudentService $service)
    {

        if (!empty($request->get('profession'))) {
            $service->where('profession_program_id', '=', $request->get('profession'));
        }

        if (!is_null($request->get('documents'))) {
            $value = (int)$request->get('documents') == 1 ? 1 : 0;
            $service->where('has_original', '=', $value);
        }
//        if ($request->get('sort_fio') == 'true') {
//            $service->orderByFullName();
//        }
        $onlyContract = $request->get('only_contract');
        //dd($onlyContract);
        if (!is_null($request->get('contract_target_set'))) {
            $service->where('contract_target_set', '=', $request->get('contract_target_set'));
        }
        if (!is_null($request->get('only_need_hostel'))) {
            $service->where('need_hostel', '=', 1);
        }
        if ($request->get('sort_avg')) {
            $service->orderBy('avg', $request->get('sort_avg'));
        } else {
            $service->orderByFullName();
        }

    }

    public function reportPage()
    {
        $programs = new ProfessionPriem();
        $columns = [
            'gender' => 'Пол',
            'profession' => 'Профессия',
            'start_date' => 'Дата подачи документов',
            'phone' => 'Телефон',
            'original' => 'Оригинал документа',
            'need_hostel' => 'Нуждается в общежитии'
        ];

        return view('admin.reports.enrollees', ['programs' => $programs, 'columns' => $columns]);
    }

    public function reportData(Request $request)
    {
        try {
            $service = $this->service
                ->onlyEnrollees();


            $this->prepareFilter($request, $service);

            $resQuery = $service->map('enrollee');


            if (count($resQuery)) {

                $result = $this->buildEnroleeReportData($resQuery);

                $countColumn = 0;
                $columns = [];
                foreach ($request->input('columns') as $key => $item) {
                    if (filter_var($item, FILTER_VALIDATE_BOOLEAN)) {
                        $countColumn++;
                        $columns[] = $key;
                    }
                }
                return response()->json([
                    'success' => true,
                    'data' => view('admin.reports.tpl.enrollee',
                        [
                            'data' => $result,
                            'withStyle' => false,
                            'columns' => $columns,
                            'countColumns' => $countColumn
                        ]
                    )->render(),
                    'data1' => $request->input(),
                    'count' => $countColumn
                ]);

            } else {

                return response()->json([
                    'success' => false,
                    'message' => "По указанным параметрам результат не найден"
                ]);

            }

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    protected function buildEnroleeReportData($resQuery)
    {
        $result = collect();
        $groupSumAvg = 0;
        $countEnrolleeToCalcGroupAvg = 0;

        $data = [];
        $programs = [];
        foreach ($resQuery as $item) {

            $program = $item->getProgram()->getNameWithForm();
            $programs[$item->getProgram()->getId()]['name'] = $program;
            $programs[$item->getProgram()->getId()]['type'] = $item->getProgram()->getTypeId() == 1 ? 'специальности' : 'профессии';
            if ($item->getContractTargetSet()) {
                $program .= " (Целевой набор)";
            }

            $data[] = [
                'fullname' => $item->getFullName(),
                'program' => $program,
                'gender' => $item->getGenderToStr(),
                'original_doc' => $item->getHasOriginalStr(),
                'phone' => $item->getPhone(),
                'avg' => is_null($item->getAvg()) ? '' : $item->getAvg(),
                'start_date' => $item->getStartDate(),
                'need_hostel' => $item->getNeedHostelStr(),
            ];

            if (!is_null($item->getAvg())) {
                $groupSumAvg += $item->getAvg();
                $countEnrolleeToCalcGroupAvg++;
            }

        }

        $groupTotalAvg = $countEnrolleeToCalcGroupAvg == 0 ? '' : $groupSumAvg / $countEnrolleeToCalcGroupAvg;
        if (count($programs) == 1) {
            $program = array_shift($programs);
            $contract = \request('contract_target_set');

            if (!is_null($contract) && $contract == 1) {
                $program['name'] .= " (Целевой набор)";
            }

            $result->put('program', $program);
        }
        $result->put('enrollees', $data);
        $result->put('total_avg', round($groupTotalAvg, 4));

        return $result;
    }

    public function exportEnrolleeReport(Request $request)
    {
        try {
            $service = $this->service
                ->onlyEnrollees();


            $this->prepareFilter($request, $service);

            $resQuery = $service->map('enrollee');


            if (count($resQuery)) {

                $result = $this->buildEnroleeReportData($resQuery);

                //dd($request->input());
                $columns = is_array($request->input('columns')) ? $request->input('columns') : [];

                $countColumn = count($columns);

                $pdf = PDF::loadView('admin.reports.tpl.enrollee',
                    [
                        'data' => $result,
                        'withStyle' => true,
                        'columns' => $columns,
                        'countColumns' => $countColumn
                    ]
                );

                return $pdf->stream('Отчет по абитуриентам.pdf');

            } else {

                return response()->json([
                    'success' => false,
                    'message' => "По указанным параметрам результат не найден"
                ]);

            }

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    public function exportEnrolleeReportToExcel(Request $request)
    {
        try {
            $service = $this->service
                ->onlyEnrollees();


            $this->prepareFilter($request, $service);

            $resQuery = $service->map('enrollee');


            if (count($resQuery)) {

                $result = $this->buildEnroleeReportData($resQuery);

                //dd($request->input());
                $columns = is_array($request->input('columns')) ? $request->input('columns') : [];


                $countColumn = count($columns);

                $params = [
                    'data' => $result,
                    'withStyle' => true,
                    'columns' => $columns,
                    'countColumns' => $countColumn
                ];


                $fileName = 'Отчет по абитуриентам.xls';
                return Excel::download(new ExportEnrollee($params), $fileName);

            } else {

                return response()->json([
                    'success' => false,
                    'message' => "По указанным параметрам результат не найден"
                ]);

            }

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ]);
        }
    }

    public function updatePassport(Request $request, $id)
    {
        try {
            $issuanceDate = Carbon::createFromFormat('d.m.Y', $request->input('issuanceDate'))->format('Y-m-d');
            $enrolleeModel = Student::query()->findOrFail($id);
            $enrolleeModel->passport_data = json_encode($request->input());
            $enrolleeModel->passport_issuance_date = $issuanceDate;
            $enrolleeModel->save();

            $enrollee = Enrollee::build($enrolleeModel);

            $codes = cache('subdivision_codes');

            $hasCode = false;

            foreach ($codes as $key => $code) {
                if (($codes[$key]['code'] == $request->input('subdivisionCode')) && ($codes[$key]['place'] == $request->input('issued'))) {
                    continue;
                } else {
                    $codes[] = [
                        'code' => $request->input('subdivisionCode'),
                        'place' => $request->input('issued')
                    ];
                }
            }
            $codes1 = collect($codes)->unique('place')->sortBy('code');

            $arr = [];

            foreach ($codes1 as $code) {
                $arr[] = [
                    'code' => $code['code'],
                    'place' => $code['place']
                ];
            }
//            if (!$hasCode) {
//                $codes[] = [
//                    'code' => $request->input('subdivisionCode'),
//                    'place' => $request->input('issued')
//                ];
//            }

            $birthPlaces = cache('birthPlaces');

            if (!in_array($request->input('birthPlace'), $birthPlaces)) {
                $birthPlaces[] = $request->input('birthPlace');
                $birthPlaces = array_unique($birthPlaces);
                sort($birthPlaces);
            }

            \Cache::forever('birthPlaces', $birthPlaces);
            \Cache::forever('subdivision_codes', $arr);


            return response()->json([
                'success' => true,
                'template' => view('admin.enrollees.inc.passport', ['enrollee' => $enrollee])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }
    }

    public function updateEducation(Request $request, $id)
    {
        try {
            $enrolleeModel = Student::query()->findOrFail($id);
            $enrolleeModel->education_id = $request->input('education_id');
            $enrolleeModel->graduation_date = Carbon::createFromFormat('d.m.Y', $request->input('graduation_date'))->format('Y-m-d');
            $enrolleeModel->graduation_organization_name = $request->input('graduation_organization_name');
            $enrolleeModel->graduation_organization_place = $request->input('graduation_organization_place');
            $enrolleeModel->save();

            $enrollee = Enrollee::build($enrolleeModel);

            $graduation_organizations_name = cache('graduation_organizations_name');

            if (!in_array($request->input('graduation_organization_name'), $graduation_organizations_name)) {
                $graduation_organizations_name[] = $request->input('graduation_organization_name');
                sort($graduation_organizations_name);
            }

            $graduation_organizations_place = cache('graduation_organizations_place');

            if (!in_array($request->input('graduation_organization_place'), $graduation_organizations_place)) {
                $graduation_organizations_place[] = $request->input('graduation_organization_place');
                sort($graduation_organizations_place);
            }
            \Cache::forever('graduation_organizations_place', $graduation_organizations_place);
            \Cache::forever('graduation_organizations_name', $graduation_organizations_name);

            return response()->json([
                'success' => true,
                'template' => view('admin.enrollees.inc.education', ['enrollee' => $enrollee])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }
    }

    public function updateAdditional(Request $request, $id)
    {
        try {
            $enrolleeModel = Student::query()->findOrFail($id);
            $enrolleeModel->privileged_category = $request->input('privileged_category');
            $enrolleeModel->start_date = Carbon::createFromFormat('d.m.Y', $request->input('start_date'))->format('Y-m-d');
            $enrolleeModel->has_certificate = isset($request->has_certificate) ? true : false;
            $enrolleeModel->need_hostel = isset($request->need_hostel) ? true : false;
            $enrolleeModel->has_original = isset($request->has_original) ? true : false;
            $enrolleeModel->contract_target_set = isset($request->contract_target_set) ? true : false;
            $enrolleeModel->save();

            $enrollee = Enrollee::build($enrolleeModel);

            return response()->json([
                'success' => true,
                'template' => view('admin.enrollees.inc.additional', ['enrollee' => $enrollee])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ])->setStatusCode(422);
        }
    }

    public function updateAddresses(Request $request, $id)
    {
        try {
            $params = $request->input();
            $addresses = [];
            $addresses['place_of_stay'] = $params['place_of_stay'];
            $addresses['registration'] = $params['registration'];
            $addresses['residential'] = $params['residential'];

            if (isset($params['isAddressSimilar'])) {
                $addresses['isAddressSimilar'] = true;
                $addresses['residential'] = $params['registration'];
            } else {
                $addresses['isAddressSimilar'] = false;
            }

            if (isset($params['isAddressPlaceOfStay'])) {
                $addresses['isAddressPlaceOfStay'] = true;
                $addresses['place_of_stay'] = $params['registration'];
            } else {
                $addresses['isAddressPlaceOfStay'] = false;
            }

            $enrolleeModel = Student::query()->findOrFail($id);
            $enrolleeModel->addresses = json_encode($addresses);
            $enrolleeModel->save();

            $enrollee = Enrollee::build($enrolleeModel);

            $ar = [
                'registration',
                'residential',
                'place_of_stay'
            ];

            $regions = cache('regions');

            foreach ($ar as $item) {
                if (!is_null($request->$item['region']) && !in_array($request->$item['region'], $regions)) {
                    $regions[] = $request->$item['region'];
                    sort($regions);
                }
            }

            $settlement = cache('settlement');

            foreach ($ar as $item) {
                if (!is_null($request->$item['settlement']) && !in_array($request->$item['settlement'], $settlement)) {
                    $settlement[] = $request->$item['settlement'];
                    sort($settlement);
                }
            }

            $cities = cache('cities');

            foreach ($ar as $item) {
                if (!is_null($request->$item['city']) && !in_array($request->$item['city'], $cities)) {
                    $cities[] = $request->$item['city'];
                    sort($cities);
                }
            }

            $streets = cache('streets');

            foreach ($ar as $item) {
                if (!is_null($request->$item['street']) && !in_array($request->$item['street'], $streets)) {
                    $streets[] = $request->$item['street'];
                    sort($streets);
                }
            }

            $area = cache('area');

            foreach ($ar as $item) {
                if (!is_null($request->$item['area']) && !in_array($request->$item['area'], $area)) {
                    $area[] = $request->$item['area'];
                    sort($area);
                }
            }
            \Cache::forever('area', $area);
            \Cache::forever('streets', $streets);
            \Cache::forever('cities', $cities);
            \Cache::forever('settlement', $settlement);
            \Cache::forever('regions', $regions);
            return response()->json([
                'success' => true,
                'template' => view('admin.enrollees.inc.addresses', ['enrollee' => $enrollee])->render(),
                'hh' => $addresses
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ]);
        }
    }

    public function getCodes()
    {
        return (array)\Cache::get('subdivision_codes');
    }
}