<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\MetaTag;
use Illuminate\Http\Request;

class MetaTagController extends Controller
{
    protected $dir = 'admin.meta-tag.';

    public function index()
    {
        $tags = MetaTag::all();

        return view($this->dir . 'index', ['tags' => $tags]);
    }


    public function create()
    {
        return view($this->dir . 'create');
    }

    public function store(Request $request)
    {
        $model = new MetaTag();
        $model->route = $request->route;
        $model->title = $request->title;
        $model->description = $request->description;
        $model->keywords = $request->keywords;
        $model->save();

        return redirect()->route('admin.meta-tags');
    }

    public function edit($id)
    {
        $tags = MetaTag::query()->findOrFail($id);

        return view($this->dir . 'edit', ['tags' => $tags]);
    }

    public function update(Request $request, $id)
    {
        $model = MetaTag::query()->findOrFail($id);
        $model->route = $request->route;
        $model->title = $request->title;
        $model->description = $request->description;
        $model->keywords = $request->keywords;
        $model->save();

        return redirect()->route('admin.meta-tags');
    }
}