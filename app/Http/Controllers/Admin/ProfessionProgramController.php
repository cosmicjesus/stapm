<?php

namespace App\Http\Controllers\Admin;

use App\BusinessLogic\ProfessionProgram\ProfessionProgramService;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\EducationForm;
use App\Models\Profession;
use App\Models\ProfessionProgram;
use App\Models\ProgramBasicDocument;
use App\ProfessionProgramType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ProfessionProgramController extends Controller
{

    protected $service;
    protected $professions;
    protected $forms;
    protected $types;

    public function __construct()
    {
        try {
            $this->service = new ProfessionProgramService();
            $this->professions = Profession::get()
                ->keyBy('id')
                ->map(function ($profession) {
                    return $profession->code . " " . $profession->name;
                })->toArray();

            $this->forms = EducationForm::get()
                ->keyBy('id')
                ->map(function ($form) {
                    return $form->name;
                })->toArray();
            $this->types = ProfessionProgramType::get()
                ->keyBy('id')
                ->map(function ($type) {
                    return $type->name;
                })->toArray();
        } catch (\Exception $e) {
        }
    }

    public function all()
    {

        $this->authorize('index', 'Opop');
        return view('admin.profession-programs.index');
    }

    public function getAllData(Request $request)
    {

        $this->authorize('index', 'Opop');
        //$programs = $this->service->map();

        $programs = ProfessionProgram::query()->orderBy('sort')->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program, false);
            });

        return Datatables::of($programs)
            ->addColumn('id', function ($program) {
                return $program->getId();
            })
            ->addColumn('name', function ($program) {
                return $program->getName();
            })
            ->addColumn('form', function ($program) {
                return $program->getEducationForm();
            })
            ->addColumn('period', function ($program) {
                return $program->getPeriod();
            })
            ->addColumn('presentation', function ($program) {
                return $program->getPresentationPath();
            })
            ->addColumn('video_presentation', function ($program) {
                if ($program->getVideoPresentationUrl()) {
                    return "<a data-fancybox href='" . $program->getVideoPresentationUrl() . "&amp;autoplay=1&amp;showinfo=0'>
                                Открыть
                            </a>";
                } else {
                    return 'Отсутствует';
                }
            })
            ->addColumn('type', function ($program) {
                return $program->getType();
            })
            ->addColumn('on_site', function ($program) {
                return $program->onSite() ? 'Да' : 'Нет';
            })
            ->addColumn('sort', function ($program) {
                return $program->getSort();
            })
            ->addColumn('actions', function ($program) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-primary btn-xs' href='" . $program->getDetailUrlAdmin() . "'><i class='fa fa-book'></i></a>";
                if (checkPermissions(['program.actions'])) {
                    $buttons .= "<a class='btn btn-warning btn-xs' href='" . $program->getEditUrl() . "'><i class='fa fa-edit'></i></a>
                <button class='btn btn-danger btn-xs delete-plan-js' data-id=''><i class='fa fa-trash'></i></button>";
                }

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions', 'video_presentation', 'presentation'])
            ->make(true);
    }

    public function createPage()
    {
        $this->authorize('create', 'Opop');

        $professions = $this->professions;
        $forms = $this->forms;
        return view('admin.profession-programs.create', [
            'professions' => $professions,
            'forms' => $forms,
            'types' => $this->types
        ]);
    }

    public function create(Request $request)
    {

        $this->authorize('create', 'Opop');


        $this->validate($request, [
            'profession' => 'required',
            'form' => 'required',
            'type' => 'required',
            'period' => 'required',
            'sort' => 'required',
            'qualification' => 'required'
        ]);

        $program = new ProfessionProgram();

        $program->profession_id = $request->profession;
        $program->education_form_id = $request->form;
        $program->type_id = $request->type;
        $program->trainin_period = $request->period;
        $program->licence = is_null($request->licence) ? null : Carbon::createFromFormat('d.m.Y', $request->licence)->format('Y-m-d');
        $program->qualification = $request->qualification;
        $program->description = $request->description;
        $program->sort = $request->sort;
        $program->on_site = $request->on_site ? true : false;
        $program->video_presentation_path = $request->video_presentation;
        $program->save();

        $program->documents()->save(new ProgramBasicDocument());
        if ($request->hasFile('presentation')) {
            $path = StorageFile::putProgramDocument($request->file('presentation'), $program->id);
            $program->presentation_path = $path;
            $program->save();
        }

        return redirect()->route('admin.programs');
    }

    public function detail($slug)
    {
        $this->authorize('index', 'Opop');

        $program = $this->service->getBySlug($slug);

        return view('admin.profession-programs.detail', ['program' => $program]);
    }

    public function edit($slug)
    {

        $this->authorize('actions', 'Opop');
        $program = \App\BusinessLogic\ProfessionProgram\ProfessionProgram::buildBySlug($slug);

        return view('admin.profession-programs.edit', [
                'program' => $program,
                'professions' => $this->professions,
                'forms' => $this->forms,
                'types' => $this->types
            ]
        );
    }

    public function update(Request $request, $slug)
    {
        $this->authorize('actions', 'Opop');
        $program = ProfessionProgram::findBySlugOrFail($slug);
        $program->type_id = $request->type;
        $program->trainin_period = $request->period;
        $program->licence = is_null($request->licence) ? null : Carbon::createFromFormat('d.m.Y', $request->licence)->format('Y-m-d');
        $program->qualification = $request->qualification;
        $program->description = $request->description;
        $program->sort = $request->sort;
        $program->on_site = $request->on_site ? true : false;
        $program->video_presentation_path = $request->video_presentation;
        $program->save();

        if ($request->hasFile('presentation')) {
            $path = StorageFile::putProgramDocument($request->file('presentation'), $program->id, $program->presentation_path);
            $program->presentation_path = $path;
            $program->save();
        }

        return redirect()->route('admin.programs');
    }

}