<?php

namespace App\Http\Controllers\Admin;


use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Models\Regulation;
use App\Models\RegulationFile;
use App\Models\RegulationItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class RegulationController extends Controller
{

    private function uploadFiles(Request $request, $model, $parent_id = null)
    {
        $names = (array)$request->names;
        $files = (array)$request->file('files');

        if (count($names) && count($files)) {
            foreach ($files as $key => $file) {
                if (isset($names[$key])) {

                    if (is_null($parent_id)) {
                        $filePath = $model->id;
                    } else {
                        $filePath = $parent_id . "/" . $model->id;
                    }
                    $path = StorageFile::putRegulationFile($filePath, $file);

                    $model->files()->save(new RegulationFile(['name' => $names[$key], 'path' => $path]));
                }
            }
        }
    }

    public function index()
    {
        return view('admin.regulations.index');
    }

    public function data(Request $request)
    {

        $regualtions = Regulation::query()
            ->orderBy('date')
            ->get()
            ->map(function ($regulation) {
                return new \App\BusinessLogic\Regulation\Regulation($regulation);
            });

        return Datatables::of($regualtions)
            ->addColumn('id', function ($new) {
                return $new->getId();
            })
            ->addColumn('title', function ($new) {
                return $new->getNameOfTheSupervisory();
            })
            ->addColumn('comment', function ($new) {
                return $new->getComment();
            })
            ->addColumn('date', function ($new) {
                return $new->getDate();
            })
            ->addColumn('count_files', function ($new) {
                return $new->getCountFiles();
            })
            ->addColumn('count_violations', function ($new) {
                return $new->getCountViolations();
            })
            ->addColumn('active', function ($new) {
                return $new->getActiveStatusStr();
            })
            ->addColumn('actions', function ($new) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-warning btn-xs' href='" . $new->getViolationsPageUrl() . "'><i class='fa fa-bars'></i></a>
                <button class='btn btn-danger btn-xs' data-route='" . $new->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";


                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function create()
    {
        return view('admin.regulations.create');
    }

    public function store(Request $request)
    {

        $model = new Regulation();
        $model->name_of_the_supervisory = $request->input('name_of_the_supervisory');
        $model->comment = $request->input('comment');
        $model->date = $request->input('date');
        $model->active = isset($request->active) ? true : false;
        $model->save();

        $this->uploadFiles($request, $model);

        return redirect()->route('admin.regulations');
    }


    public function violationsList($id)
    {
        $regulation = Regulation::query()->findOrFail($id);

        return view('admin.regulations.violations', ['regulation' => new \App\BusinessLogic\Regulation\Regulation($regulation)]);
    }

    public function createViolation($id)
    {
        $route = route('admin.regulations.store-violations', ['id' => $id]);

        return view('admin.regulations.add-violations', ['route' => $route]);
    }

    public function storeViolation(Request $request, $id)
    {
        $model = new RegulationItem();
        $model->regulation_id = $id;
        $model->name_of_violation = $request->name_of_violation;
        $model->result = $request->result;
        $model->status = 'success';
        $model->active = true;
        $model->save();

        $this->uploadFiles($request, $model, $id);

        return redirect()->route('admin.regulations.violations', ['id' => $id]);
    }
}