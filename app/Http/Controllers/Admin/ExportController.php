<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\Enrollee\Enrollee;
use App\Export\AsuRsoExport;
use App\Export\ExportBase;
use App\Export\ParentsToAsuRso;
use App\Http\Controllers\Controller;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ExportController extends Controller
{
    public function enrolleeToAsuRso()
    {
        $fileName = "Импорт в АСУ РСО " . Carbon::now()->format('d.m.Y') . ".csv";
        return Excel::download(new AsuRsoExport(), $fileName);
    }

    public function base($type)
    {
        if ($type == 'bank') {
            $fileName = 'Файл для банка от ' . Carbon::now()->format('H:i:s d.m.Y') . ".xls";
        } else {
            $fileName = 'Полная база поступивших от ' . Carbon::now()->format('H:i:s d.m.Y') . ".xls";
        }
        return Excel::download(new ExportBase($type), $fileName);

    }

    public function parents()
    {
        return Excel::download(new ParentsToAsuRso(), 'Родители.csv');
    }
}
