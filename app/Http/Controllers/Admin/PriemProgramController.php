<?php

namespace App\Http\Controllers\Admin;


use App\BusinessLogic\ProfessionProgram\ProfessionPriem;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\CategoriesDocument;
use App\Models\Document;
use App\Models\PriemProgram;
use App\Models\ProfessionProgram;
use Illuminate\Http\Request;
use PDF;
use Ramsey\Uuid\Uuid;
use Storage;

class PriemProgramController extends Controller
{

    public function index()
    {
        $programs = ProfessionProgram::query()
            ->whereHas('priem')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });
        return view('admin.priem.index', ['programs' => $programs]);
    }

    public function createPage()
    {
        $programs = ProfessionProgram::query()
            ->whereDoesntHave('priem')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });
        return view('admin.priem.create', ['programs' => $programs]);
    }

    public function create(Request $request)
    {

        $messages = [
            'profession_program_id.required' => 'Программа не выбрана',
            'reception.required' => 'Не задан план приема',
            'reception.numeric' => 'Поле План приема должно быть числом',
            'reception.min' => 'Минимальное значение поля план приема равно 1',
        ];

        $this->validate($request, [
            'profession_program_id' => 'required',
            'reception' => 'required|numeric|min:1'

        ], $messages);

        $priem = new PriemProgram([
            'reception_plan' => $request->reception,
            'count_with_documents' => $request->count_with_documents,
            'count_with_out_documents' => $request->count_with_out_documents
        ]);

        $program = ProfessionProgram::findOrFail($request->profession_program_id);
        $program->priem()->save($priem);
        return redirect()->route('admin.priem');
    }


    public function update(Request $request, $id)
    {
        try {
            $model = PriemProgram::query()->findOrFail($id);
            $model->reception_plan = $request->input('reception');
            $model->count_with_documents = $request->input('count_with_documents');
            $model->count_with_out_documents = $request->input('count_with_out_documents');
            $model->save();

            $programs = ProfessionProgram::query()
                ->whereHas('priem')
                ->get()
                ->map(function ($program) {
                    return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
                });

            return response()->json([
                'success' => true,
                'template' => view('admin.priem.inc.table', ['programs' => $programs])->render()
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br> Текст ошибки:" . $exception->getMessage()
            ])->setStatusCode(422);
        }


    }

    public function delete($id)
    {
        try {
            PriemProgram::destroy($id);
            return 'Программа была удалена из листа приема';
        } catch (\Exception $exception) {
            return 'Ошибка';
        }
    }

    public function makeReport(Request $request)
    {
        $data = new ProfessionPriem($request->input('report_date'));
        $format = [];
        $format['total_count_on_forms'] = [1 => 0, 2 => 0, 3 => 0];
        foreach ($data->getPrograms() as $item) {
            if (!isset($format['data'][$item->getProfessionId()])) {
                $format['data'][$item->getProfessionId()]['code'] = $item->getCode();
                $format['data'][$item->getProfessionId()]['name'] = $item->getProfessionName();
                $format['data'][$item->getProfessionId()]['countEnrolleesWithOutDocuments'] = $item->getCountEnrolleesWithOutDocuments(true);
                $format['data'][$item->getProfessionId()]['countEnrolleesWithDocuments'] = $item->getCountEnrolleesWithDocuments(true);
                $format['data'][$item->getProfessionId()]['forms'] = [1 => 0, 2 => 0, 3 => 0];
                $format['data'][$item->getProfessionId()]['forms'][$item->getEducationFormId()] = $item->getCountEnrolleesWithDocuments(true) + $item->getCountEnrolleesWithOutDocuments(true);
                $format['total_count_on_forms'][$item->getEducationFormId()] += $item->getCountEnrolleesWithDocuments(true) + $item->getCountEnrolleesWithOutDocuments(true);
            } else {
                $format['data'][$item->getProfessionId()]['forms'][$item->getEducationFormId()] = $item->getCountEnrolleesWithDocuments(true) + $item->getCountEnrolleesWithOutDocuments(true);
                $format['total_count_on_forms'][$item->getEducationFormId()] += $item->getCountEnrolleesWithDocuments(true) + $item->getCountEnrolleesWithOutDocuments(true);
                $format['data'][$item->getProfessionId()]['countEnrolleesWithOutDocuments'] += $item->getCountEnrolleesWithOutDocuments(true);
                $format['data'][$item->getProfessionId()]['countEnrolleesWithDocuments'] += $item->getCountEnrolleesWithDocuments(true);
            }
        }
        $format['countWithDocuments'] = $data->getCountWithDocuments();
        $format['countWithOutDocuments'] = $data->getCountWithOutDocuments();
        $format['date'] = $request->input('report_date');
        //dd($format);
        $pdf = PDF::loadView('admin.reports.countEnrollees',
            [
                'data' => $format,
                'withStyle' => true
            ]
        );

        if ($request->input('put_in_documents')) {
            $fileName = str_random(15) . ".pdf";
            Storage::disk('documents')->put($fileName, $pdf->output());
            $category_id = (CategoriesDocument::query()->where('slug', 'admission')->first())->id;

            $document = new Document();
            $document->category_id = $category_id;
            $document->name = "Сведения о ходе комплектования групп приема на " . $request->input('report_date');
            $document->path = $fileName;
            $document->sort = 500;
            $document->save();
        }


        return $pdf->stream('Справка.pdf');
    }

}