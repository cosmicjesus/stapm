<?php

namespace App\Http\Controllers\Admin;

use App\AppLogic\Services\CacheService;
use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\ProfessionProgram;
use App\Models\Student;
use App\Models\StudentParent;
use App\Models\TrainingGroup;
use Carbon\Carbon;

class DashboardController extends Controller
{

    public function index()
    {
        $statistic = [];
        $employee_model = Employee::query()->where('status', 'work');
        $countPrograms = ProfessionProgram::query()->count();
        $countPlans = EducationPlan::query()->where('active', true)->count();
        $countGroups = TrainingGroup::query()->where('status', 'teach')->count();
        $enrolleesModel = Student::query()->where('status', 'enrollee');
        $studentsModel = Student::query()->whereIn('status', ['student', 'inArmy', 'academic']);
        $parentsModel = StudentParent::query();


        $statistic['count_employees'] = getNumEnding($employee_model->count(), ['сотрудник', 'сотрудникa', 'сотрудников'], true);
        $statistic['count_enrollees'] = getNumEnding($enrolleesModel->count(), ['абитуриент', 'абитуриента', 'абитуриентов'], true);
        $statistic['count_students'] = getNumEnding($studentsModel->count(), ['студент', 'студента', 'студентов'], true);
        $statistic['count_parents'] = getNumEnding($parentsModel->count(), ['родитель', 'родителя', 'родителей'], true);
        $statistic['count_ed_programs'] = $countPrograms . " " . getNumEnding($countPrograms, ['образовательная программа', 'образовательные программы', 'образовательных программ']);
        $statistic['count_ed_plans'] = $countPlans . " " . getNumEnding($countPlans, ['учебный план', 'учебных плана', 'учебных планов']);
        $statistic['count_training_groups'] = $countGroups . " " . getNumEnding($countGroups, ['учебная группа', 'учебные группы', 'учебных групп']);

        $employees = $employee_model->get();

        $students = $studentsModel->orderBy('group_id')->orderBy('lastname')->get();

        foreach ($employees as $employee) {
            if ($employee->category_date != null) {
                $now = Carbon::now();
                $different = $now->diffInDays($employee->category_date, false);
                if ($different < 0 || $different <= 90) {
                    $statistic['employees']['endCategory'][] = new \App\BusinessLogic\Employee\Employee($employee);
                }
            }
        }
        if (checkPermissions('students.view')) {
            foreach ($students as $student) {
                if (!is_null($student->passport_issuance_date)) {
                    $now = Carbon::now();
                    $dateofbirth = $student->birthday->addMonth(1);
                    $dateofbirthN = $student->birthday;
                    $age = $dateofbirth->diff($now, false)->y;
                    $passportDate = $student->passport_issuance_date;

                    $passportAge = $passportDate->diff($dateofbirthN, false)->y;

                    if ((($age >= 20 && $age < 45) && $passportAge < 20) || ($age >= 45 && $passportAge < 45)) {

                        $statistic['students']['endPassport'][] = new \App\BusinessLogic\Student\Student($student);

                    }
                }
            }
        }

        $enrolleesWithOutOriginal = $enrolleesModel->where('has_original', false)->get();

        foreach ($enrolleesWithOutOriginal as $enrollee) {
            $statistic['enrolleesWithOutDocuments'][] = new \App\BusinessLogic\Student\Student($enrollee);
        }

        return view('admin.dashboard', ['statistic' => $statistic]);
    }

}