<?php


namespace App\Http\Controllers\Admin;


use App\BusinessLogic\Employee\EmployeeService;
use App\Helpers\StorageFile;
use App\Http\Controllers\Controller;
use App\Jobs\SiteMap;
use App\Models\AdministrationEmployee;
use App\Models\Education;
use App\Models\Employee;
use App\Models\EmployeeEducation;
use App\Models\EmployeePosition;
use App\Models\EmployeeSubject;
use App\Models\Position;
use App\Models\QualificationCourse;
use App\BusinessLogic\QualificationCourse\QualificationCourse as QualificationCourseClass;
use App\Models\Subject;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;
use Yajra\Datatables\Datatables;

class EmployeeController extends Controller
{
    protected $service;

    public function __construct()
    {
        try {
            $this->service = new EmployeeService();
        } catch (\Exception $e) {

        }
    }

    public function all()
    {
        $this->authorize('index', 'Employee');
        $positions = Position::orderBy('name')->get();
        return view('admin.employees.index', ['positions' => $positions]);
    }

    public function allData(Request $request)
    {
        $service = $this->service->with('positions');

        if (!empty($request->get('name'))) {
            $service->where('lastname', 'LIKE', "%{$request->get('name')}%")
                ->orWhere('firstname', 'LIKE', "%{$request->get('name')}%")
                ->orWhere('middlename', 'LIKE', "%{$request->get('name')}%");
        }

        if (!empty($request->get('position'))) {
            $service->whereHas('positions', ['position_id', '=', $request->get('position')]);
        }

        if (!empty($request->get('category'))) {
            $service->where('category', '=', $request->get('category'));
        }
        $employees = $service
            ->onlyWork()
            ->orderByFullName()
            ->map();

        return Datatables::of($employees)
            ->addColumn('id', function ($employee) {
                return $employee->getId();
            })
            ->addColumn('full_name', function ($employee) {
                return $employee->getFullName();
            })
            ->addColumn('birthday', function ($employee) {
                return $employee->getBirthday();
            })
            ->addColumn('age', function ($employee) {
                return $employee->getAge();
            })
            ->addColumn('positions', function ($employee) {

                $tpl_list = "<ul>#ITEMS#</ul>";

                $items = '';

                foreach ($employee->getPositions() as $position) {
                    $items .= "<li>" . $position->getName() . "</li>";
                }

                $positions = str_replace('#ITEMS#', $items, $tpl_list);

                return $positions;
            })
            ->addColumn('category', function ($employee) {
                return $employee->getCategory();
            })
            ->addColumn('count_courses', function ($employee) {
                return count($employee->getCourses());
            })
            ->addColumn('actions', function ($employee) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<a class='btn btn-primary btn-xs' href=" . route('admin.employee.profile', ['slig' => $employee->getSlug()]) . "><i class='fa fa-user'></i></a>";
                $buttons .= "<button class='btn btn-warning btn-xs dismiss-employee-js' data-name='" . $employee->getFullName() . "' data-url=" . route('admin.employee.dismiss', ['id' => $employee->getId()]) . "><i class='fa fa-sign-out'></i></button>";
                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['positions', 'actions'])
            ->make(true);
    }

    public function dismiss(Request $request, $id)
    {
        try {
            $dateDismiss = Carbon::now()->format('Y-m-d');

            $employee = Employee::query()->with('positions')->findOrFail($id);

            foreach ($employee->positions as $position) {
                $employee->positions()->updateExistingPivot($position->pivot->position_id, ['date_layoff' => $dateDismiss]);
            }

            $employee->dismiss_date = $dateDismiss;
            $employee->status = "dismissed";
            $employee->save();
            $employee->administrator()->delete();
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => '<b>Сотрудник был успешно уволен</b>',
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }


    public function createPage()
    {

        $this->authorize('create', 'Employee');

        $positions = Position::orderBy('name')
            ->get()
            ->keyBy('id')
            ->map(function ($position) {
                return $position->name;
            })->toArray();

        $educations = Education::all();
        return view('admin.employees.create', ['positions' => $positions, 'educations' => $educations]);
    }

    public function create(Request $request)
    {

        $this->authorize('create', 'Employee');

        $tpl = "В базе уже существует сотрудник $request->lastname $request->firstname $request->middlename с датой рождения $request->birthday";

        $messages = [
            'lastname.required' => "Поле 'Фамилия' не заполнено",
            'lastname.check_employee' => $tpl,
            'firstname.required' => "Поле 'Имя' не заполнено",
            'birthday.date_format' => "Поле 'Дата рождения' не заполнено",
            'start_date.date_format' => "Поле 'Дата назначения' не заполнено"
        ];

        $this->validate($request, [
            'lastname' => ['required', 'check_employee'],
            'firstname' => 'required',
            'birthday' => 'date_format:d.m.Y',
            'start_date' => 'date_format:d.m.Y'
        ], $messages);

        $employee = new Employee();

        $employee->lastname = $request->lastname;
        $employee->firstname = $request->firstname;
        $employee->middlename = $request->middlename;
        $employee->gender = $request->gender;
        $employee->status = 'work';
        $employee->birthday = Carbon::createFromFormat('d.m.Y', $request->birthday)->format('Y-m-d');
        $employee->category = $request->category;
        $employee->category_date = $request->category == 0 ? null : Carbon::createFromFormat('d.m.Y', $request->category_date)->format('Y-m-d');
        $employee->start_work_date = Carbon::createFromFormat('d.m.Y', $request->start_date)->format('Y-m-d');
        $employee->save();

        $employee->educations()->attach(
            [
                $request->education =>
                    [
                        'qualification' => $request->qualification
                    ]
            ]);
        $employee->positions()->attach(
            [$request->position =>
                [
                    'start_date' => Carbon::createFromFormat('d.m.Y', $request->start_date)->format('Y-m-d'),
                    'type' => $request->type,
                    'active' => true,
                    'experience' => 0,
                    'sort' => 1
                ]
            ]
        );
        $this->dispatch(new SiteMap());
        return redirect()->route('admin.employees');

    }

    public function profile($slug)
    {
        $this->authorize('view', 'Employee');

        $employee = $this->service->oneBySlug($slug);

        return view('admin.employees.profile', ['employee' => $employee]);
    }


    public function uploadPhoto(Request $request, $slug)
    {

        $this->authorize('actions', 'Employee');

        $model = Employee::findBySlug($slug);
        $image = ImageManagerStatic::make($request->file('file'));
        $image_data = $image->exif('COMPUTED');
        $width = employeePhotoWidth($image_data['Width'], $image_data['Height']);

        $resize_image = $image->resize($width, null, function ($constraint) {
            return $constraint->aspectRatio();
        })->encode('jpg', 100);

        $filename = StorageFile::putEmployeePhoto($model, $resize_image);

        if ($filename) {
            if (!is_null($model->photo)) {
                StorageFile::deleteEmployeePhoto($model->id . "/" . $model->photo);
            }
            $model->photo = $filename;
            $model->save();

            return response()->json([
                'success' => true,
                'path' => StorageFile::getEmployeePhoto($model->id . "/" . $model->photo)
            ]);
        }
        return response(422, 422)->json([
            'success' => false,
            'message' => 'Произошла ошибка при загрузке фотографии'
        ]);

    }

    public function deletePhoto($slug)
    {
        $this->authorize('actions', 'Employee');

        $model = Employee::findBySlug($slug);
        StorageFile::deleteEmployeePhoto($model->id . "/" . $model->photo);
        $model->photo = null;
        $model->save();
        return response('Фотография была удалена', 200);
    }

    public function addCourse(Request $request)
    {

        $this->authorize('actions', 'Employee');

        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            'start_date' => 'date_format:d.m.Y',
            'count_hours' => 'required|numeric|min:6'
        ]);
        if ($validator->passes()) {
            $course = new QualificationCourse([
                'title' => $request->title,
                'start_date' => Carbon::createFromFormat('d.m.Y', $request->start_date),
                'end_date' => !empty($request->end_date) ? Carbon::createFromFormat('d.m.Y', $request->end_date) : null,
                'count_hours' => $request->count_hours,
                'description' => $request->description
            ]);
            $user = Employee::find($request->employee_id);
            $user->courses()->save($course);
            $this->dispatch(new SiteMap());
            return response('Все нормально', 200);
        }
        return response(['errors' => $validator->errors()->all()], 422);

    }

    public function getCourses(Request $request, $id)
    {

        $courses = QualificationCourse::where('employee_id', $id)
            ->orderBy('start_date', 'DESC')
            ->get()
            ->map(function ($course) {
                return new QualificationCourseClass($course);
            });

        return Datatables::of($courses)
            ->addColumn('id', function ($course) {
                return $course->getId();
            })
            ->addColumn('title', function ($course) {
                return $course->getTitle();
            })
            ->addColumn('period', function ($course) {
                return $course->getPeriod();
            })
            ->addColumn('hours', function ($course) {
                return $course->getHours();
            })
            ->addColumn('actions', function ($course) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                $buttons = "<button class='btn btn-danger btn-xs delete-course' data-id='" . $course->getId() . "'><i class='fa fa-trash'></i></button>";

                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function deleteCourse($id)
    {
        try {
            $this->authorize('actions', 'Employee');
            $model = QualificationCourse::findOrFail($id);

            $str = "Курс с наименованием '" . $model->title . "' был успешно удален";
            QualificationCourse::destroy($id);
            $this->dispatch(new SiteMap());
            return response($str, 200);
        } catch (\Exception $exception) {
            return response('Произошла ошибка при удалении. Попробуйте позже или обратитесь к администратору', 422);
        }
    }

    public function addPosition(Request $request, $id)
    {

        $this->authorize('actions', 'Employee');

        $messages = [
            'position_id.required' => 'Должность не выбрана'
        ];

        $validator = \Validator::make($request->all(), [
            'position_id' => 'required',
            'start_date' => 'date_format:d.m.Y',
        ], $messages);

        if ($validator->passes()) {
            $employee = Employee::findOrFail($id);
            $employee->positions()->attach([
                $request->position_id => [
                    'start_date' => Carbon::createFromFormat('d.m.Y', $request->start_date)->format('Y-m-d'),
                    'type' => $request->type_id
                ]
            ]);
            $this->dispatch(new SiteMap());
            return response('Все нормально', 200);
        }
        return response(['errors' => $validator->errors()->all()], 422);
    }

    public function deletePosition(Request $request, $employee_id, $id)
    {
        try {
            $employeePositions = EmployeePosition::where('employee_id', $employee_id)->count();

            if ($employeePositions == 1) {
                return response()->json([
                    'success' => false,
                    'message' => '<b>Удаление невозможно, т.к это единственная должность у сотрудника</b>',
                ])->setStatusCode(200);
            }
            EmployeePosition::destroy($id);
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => '<b>Должность была успешно удалена</b>',
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function layoff_position(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'layoff_date' => 'date_format:d.m.Y',
        ]);
        if ($validator->passes()) {
            $model = EmployeePosition::findOrFail($request->id);
            $model->date_layoff = Carbon::createFromFormat('d.m.Y', $request->layoff_date)->format('Y-m-d');
            $model->save();
            $this->dispatch(new SiteMap());
            return response('Все нормально', 200);
        }
        return response(['errors' => $validator->errors()->all()], 422);
    }

    public function edit($slug)
    {
        $this->authorize('actions', 'Employee');

        $employee = Employee::findBySlug($slug);

        return view('admin.employees.edit', ['employee' => $employee]);

    }

    public function update(Request $request, $slug)
    {

        $this->authorize('actions', 'Employee');

        $messages = [
            'lastname.required' => "Поле «Фамилия» не заполнено",
            'firstname.required' => "Поле «Имя» не заполнено",
            'birthday.required' => "Поле «Дата рождения» не заполнено",
        ];

        $this->validate($request, [
            'lastname' => 'required',
            'firstname' => 'required',
            'birthday' => 'date_format:d.m.Y',
        ], $messages);

        $employee = Employee::findBySlug($slug);
        $employee->lastname = $request->lastname;
        $employee->firstname = $request->firstname;
        $employee->middlename = $request->middlename;
        $employee->gender = $request->gender;
        $employee->birthday = Carbon::createFromFormat('d.m.Y', $request->birthday);
        $employee->category = $request->category;
        $employee->phones = $request->phones;
        $employee->emails = $request->emails;
        $employee->general_experience = $request->exp;
        $employee->specialty_exp = $request->specialty_exp;
        $employee->scientific_degree = $request->scientific_degree;
        $employee->academic_title = $request->academic_title;
        $employee->slug = null;
        $employee->category_date = is_null($request->category_date) ? null : Carbon::createFromFormat('d.m.Y', $request->category_date)
            ->format('Y-m-d');
        $employee->save();
        $this->dispatch(new SiteMap());
        return redirect()->route('admin.employee.profile', ['slug' => $employee->slug]);
    }

    public function addEducation(Request $request)
    {
        try {
            $this->authorize('actions', 'Employee');
            $model = new EmployeeEducation();
            $model->employee_id = $request->employee_id;
            $model->education_id = $request->education_id;
            $model->qualification = $request->qualification;
            $model->direction_of_preparation = $request->direction_of_preparation;
            $model->save();
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => 'Образование было успешно добавлено',
                'template' => view('admin.employees.partials._education-item', ['education' => $model, 'new' => true])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function deleteEducation(Request $request, $employee_id, $id)
    {
        try {
            $employeeEducation = EmployeeEducation::where('employee_id', $employee_id)->count();

            if ($employeeEducation == 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'Удаление невозможно, т.к это единственная запись об образовании у сотрудника',
                ])->setStatusCode(200);
            }
            EmployeeEducation::destroy($id);
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => 'Образование было успешно удалено',
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function educationUpdate(Request $request, $id)
    {
        try {
            $this->authorize('actions', 'Employee');
            $model = EmployeeEducation::query()->with('education')->findOrFail($id);
            $model->qualification = $request->qualification;
            $model->direction_of_preparation = $request->direction_of_preparation;
            $model->save();
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'education_id' => $id,
                'message' => 'Информация была успешно обновлена',
                'template' => view('admin.employees.partials._education-item', ['education' => $model, 'new' => false])->render()
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function addSubject(Request $request, $id)
    {
        try {
            $this->authorize('actions', 'Employee');

            $model = EmployeeSubject::query()->where('employee_id', $request->input('employee_id'))
                ->where('subject_id', $request->input('subject_id'))
                ->first();

            if (!is_null($model)) {
                return response()->json([
                    'success' => false,
                    'message' => '<b>Выбранная дисциплина уже назначена у сотрудника. Повторное назначение невозможно</b>'
                ])->setStatusCode(200);
            }

            $subject = new EmployeeSubject();
            $subject->employee_id = $request->input('employee_id');
            $subject->subject_id = $request->input('subject_id');
            $subject->save();

            $subjectModel = Subject::findOrFail($request->input('subject_id'));
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'data' => json_encode(['subject' => $subjectModel->name, 'delete_url' => route('admin.employee.delete-subject', ['id' => $subject->id])])
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }

    public function deleteSubject($id)
    {
        try {
            $this->authorize('actions', 'Employee');

            EmployeeSubject::destroy($id);
            $this->dispatch(new SiteMap());
            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно удалена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success' => false,
                    'message' => '<b>Произошла ошибка <br> Код ошибки:' . " " . $exception->getCode() . " <br>Текст ошибки:" . " " . $exception->getMessage() . "<b>"
                ])
                ->setStatusCode(422);
        }
    }
}