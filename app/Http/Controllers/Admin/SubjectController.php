<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\EmployeeSubject;
use App\Models\ProgramSubject;
use App\Models\Subject;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SubjectController extends Controller
{
    public function index()
    {
        $this->authorize('view', 'Subjects');

        return view('admin.subjects.index');
    }

    public function data(Request $request)
    {
        $this->authorize('view', 'Subjects');

        $query = Subject::query();
        $query->orderBy('name', 'ASC');
        $query->when(!empty($request->get('type_id')), function ($q) use ($request) {
            $q->where('type_id', $request->get('type_id'));
        });
        $query->when(!empty($request->get('name')), function ($q) use ($request) {
            $q->where('name', 'LIKE', "%{$request->get('name')}%");
        });
        $subjects = $query->get()->map(function ($subject) {
            return new \App\BusinessLogic\Subject\Subject($subject);
        });

        return Datatables::of($subjects)
            ->addColumn('id', function ($subject) {
                return $subject->getId();
            })
            ->addColumn('name', function ($subject) {
                return $subject->getName();
            })
            ->addColumn('type', function ($subject) {
                return $subject->getType();
            })
            ->addColumn('actions', function ($subject) {
                $table = "<div class='table-buttons'>#CONTENT#</div>";
                if (checkPermissions(['subject.actions'])) {
                    $buttons = "<a class='btn btn-warning btn-xs' href='" . $subject->getEditUrl() . "'><i class='fa fa-edit'></i></a>
               <button class='btn btn-danger btn-xs delete-subject-js' data-name='" . $subject->getName() . "' data-url='" . $subject->getDeleteUrl() . "'><i class='fa fa-trash'></i></button>";
                } else {
                    $buttons = '';
                }


                $str = str_replace('#CONTENT#', $buttons, $table);
                return $str;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }


    public function create()
    {
        $this->authorize('actions', 'Subjects');

        return view('admin.subjects.create');
    }

    public function store(Request $request)
    {
        try {
            $this->authorize('actions', 'Subjects');
            $sub = Subject::where('name', $request->name)
                ->where('type_id', $request->type_id)
                ->first();
            if (!is_null($sub)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Дисциплина с таким наименованием и типом уже существует'
                ])->setStatusCode(200);
            }

            $subject = new Subject();
            $subject->name = $request->name;
            $subject->type_id = $request->type_id;
            $subject->save();
            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно добавлена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ]);
        }

    }

    public function edit($id)
    {
        $this->authorize('actions', 'Subjects');

        $subject = Subject::findOrFail($id);
        return view('admin.subjects.edit', ['subject' => $subject]);
    }

    public function update(Request $request, $id)
    {
        $this->authorize('actions', 'Subjects');

        $subject = Subject::findOrFail($id);
        $subject->name = $request->name;
        $subject->type_id = $request->type_id;
        $subject->save();

        return redirect()->route('admin.subjects');
    }

    public function delete(Request $request, $id)
    {
        try {
            $this->authorize('actions', 'Subjects');
            $employeeSubject = EmployeeSubject::query()->where('subject_id', $id)->count();
            $subjectInPrograms = ProgramSubject::query()->where('subject_id', $id)->count();

            if ($employeeSubject || $subjectInPrograms) {
                return response()->json([
                    'success' => false,
                    'message' => 'Дисциплина используется в базе. Удаление невозможно'
                ])->setStatusCode(200);
            }


            Subject::destroy($id);
            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно удалена',
                'arr' => [$employeeSubject, $subjectInPrograms]
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => "Код ошибки:" . $exception->getCode() . "<br><b> Текст ошибки:" . $exception->getMessage() . "</b>"
            ]);
        }
    }
}