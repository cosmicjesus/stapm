<?php

namespace App\Http\Middleware;

use App\Models\MetaTag;
use App\Widgets\ClientMenu;
use Closure;
use Meta;

class MetaTags
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->getRequestUri();
        $tags = MetaTag::getByRouteName($route);

        if (!is_null($tags)) {
            Meta::set('description', $tags->description);
            Meta::set('keywords', $tags->keywords);
        }
        return $next($request);
    }
}
