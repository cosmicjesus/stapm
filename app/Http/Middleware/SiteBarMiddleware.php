<?php

namespace App\Http\Middleware;

use App\Widgets\ClientMenu;
use Closure;

class SiteBarMiddleware
{

    protected $menuWidget;

    public function __construct(ClientMenu $menuWidget)
    {
        $this->menuWidget = $menuWidget;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = (\Route::current())->getName();
        $menuElements = collect($this->menuWidget->items());
        $section = $menuElements->filter(function ($item) use ($routeName) {
            return isset($item['sub'][$routeName]) || in_array($routeName, $item['sub_routes']);
        })->keys()->first();
        if ($section) {
            \View::share('siteBarItems', $menuElements[$section]['sub'] ?? []);
        }
        return $next($request);
    }
}
