<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDecreeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'=>'required',
            'date'=>'required|date_format:Y-m-d'
        ];
    }
    public function messages()
    {
        return [
            'number.required' => 'Не заполнен номер приказа',
            'date.required' => 'Не указана дата приказа',
            'date.date_format' => 'Дата приказа имеет некорректный формат',
        ];
    }
}
