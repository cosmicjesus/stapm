<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePassportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documentType' => 'required',
            'number' => 'required',
            'issuanceDate' => 'required|date_format:Y-m-d',
            'subdivisionCode' => 'required',
            'issued' => 'required',
            'birthPlace' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'documentType.required' => 'Не выбран тип документа',
            'number.required' => 'Не заполнен номер документа',
            'issuanceDate.required' => 'Не указана дата выдачи документа',
            'issuanceDate.date_format' => 'Дата выдачи имеет некорректный формат',
            'subdivisionCode.required' => 'Не указан Код подразделения',
            'issued.required' => 'Не указан орган выдавший документ',
            'birthPlace.required' => 'Не указано место рождения'
        ];
    }
}
