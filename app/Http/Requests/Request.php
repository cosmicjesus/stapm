<?php

namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

abstract class Request extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator));
        throw new HttpResponseException(response()->json(
            [
                'success' => false,
                'message' => $errors->getMessage(),
                'code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                'errors' => collect($errors->errors())->flatten()
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}