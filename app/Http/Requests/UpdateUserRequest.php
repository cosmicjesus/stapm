<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|unique:users|min:8',
            'password' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Логин не заполнен',
            'login.unique' => 'Пользователь с таким логином уже существует',
            'login.min' => 'Минимальная длина логина 8 символов',
            'password.required' => 'Пароль не заполнен',
        ];
    }
}
