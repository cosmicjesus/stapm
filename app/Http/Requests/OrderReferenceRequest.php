<?php

namespace App\Http\Requests;

class OrderReferenceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|max:32',
            'firstname' => 'required|max:32',
            'middlename' => 'max:32',
            'group_id' => 'required',
            'types' => 'required|array'
        ];
    }

    public function messages()
    {
        return [
            'lastname.required'=>'Поле Фамилия не заполнено',
            'firstname.required'=>'Поле Имя не заполнено',
            'group_id.required'=>'Не выбрана группа',
            'types.required'=>'Не выбраны справки для заказа',
        ];
    }

}
