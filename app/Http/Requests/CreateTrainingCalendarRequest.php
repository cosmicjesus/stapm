<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateTrainingCalendarRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'unique' => Rule::unique('training_calendars', 'name')->where('year', $this->request->get('year'))
            ],
            'periods.*.start_date' => ['required'],
            'periods.*.end_date' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Не заполнено название календаря',
            'name.unique' => 'Календарь с таким наименованием уже существует в этом учебном году',
            'periods.*.start_date.required' => 'Не указана дата начала в одном из периодов',
            'periods.*.end_date.required' => 'Не указана дата окончания в одном из периодов',

        ];
    }
}
