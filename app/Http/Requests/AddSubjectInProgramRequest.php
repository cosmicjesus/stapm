<?php

namespace App\Http\Requests;

use App\Rules\SubjectInProfessionProgramList;
use Illuminate\Foundation\Http\FormRequest;

class AddSubjectInProgramRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_id' => ['required', new SubjectInProfessionProgramList($this->program_id, $this->index,$this->program_academic_year_id)],
            'index' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'subject_id.required' => 'Дисциплина не выбрана',
            'index.required' => 'Не указан индекс'
        ];
    }
}
