<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStudentFileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('super.admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'file' => 'required|file'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Не заполнено наименование документа',
            'file.required' => 'Не выбран файл',
            'file.file' => 'Поле Файл должно быть файлом'
        ];
    }
}
