<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEnrolleePersonal extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required',
            'firstname' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'start_date' => 'required',
            'phone' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'lastname.required' => 'Фамилия не заполнена',
            'firstname.required' => 'Имя не заполнено',
            'birthday.required'=>'Дата рождения не указана',
            'gender.required'=>'Пол не указан',
            'start_date.required'=>'Не указана дата подачи документов',
            'phone.required'=>'Не указан телефон',
        ];
    }
}
