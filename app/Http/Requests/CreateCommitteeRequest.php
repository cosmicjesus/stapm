<?php

namespace App\Http\Requests;

use App\Rules\CheckCommitteeRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateCommitteeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'start_date' => ['required', new CheckCommitteeRule(), 'before:end_date'],
            'end_date' => ['required'],
            'last_day_of_work' => ['required', 'after:end_date'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Не заполнено название',
            'start_date.required' => 'Не заполнена дата начала',
            'start_date.before' => 'Дата окончания не может быть меньше даты начала',
            'end_date.required' => 'Не заполнена дата окончания',
            'last_day_of_work.required' => 'Не заполнена дата последнего рабочего дня',
            'last_day_of_work.after' => 'Полное окончание не может быть раньше окончания приема документов'
        ];
    }
}
