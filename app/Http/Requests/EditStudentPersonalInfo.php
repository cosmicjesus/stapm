<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditStudentPersonalInfo extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|max:32',
            'firstname' => 'required|max:32',
            'middlename' => 'max:32',
            'gender' => 'required',
            'birthday' => 'required|date_format:Y-m-d',
        ];
    }

    public function messages()
    {
        return [
            'lastname.required' => 'Не заполнена фамилия',
            'lastname.max' => 'Максимальная длина поля Фамилия 32 символа',
            'firstname.required' => 'Не заполнено имя',
            'firstname.max' => 'Максимальная длина поля Имя 32 символа',
            'middlename.max' => 'Максимальная длина поля Отчество 32 символа',
            'gender.required' => 'Не выбран пол',
            'birthday.required' => 'Не заполнена дата рождения',
            'birthday.date_format' => 'Дата рождения имеет некорректный формат',
        ];
    }
}
