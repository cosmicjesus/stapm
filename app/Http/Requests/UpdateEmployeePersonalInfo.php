<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeePersonalInfo extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required',
            'firstname' => 'required',
            'birthday' => 'required|date_format:Y-m-d',
            'start_work_date' => 'required|date_format:Y-m-d',
            'gender' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'lastname.required' => 'Не заполнена фамилия',
            'firstname.required' => 'Не заполнено имя',
            'birthday.required' => 'Не заполнена дата рождения',
            'birthday.date_format' => 'Дата рождения имеет некорректный формат',
            'start_work_date.required' => 'Не заполнена дата начала работы',
            'start_work_date.date_format' => 'Дата начала работы имеет некорректный формат',
            'gender.required' => 'Не указан пол',
        ];
    }
}
