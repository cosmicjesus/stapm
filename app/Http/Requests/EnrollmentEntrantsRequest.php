<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnrollmentEntrantsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'decree_date' => 'required',
            'decree_number' => 'required',
            'entrantsData' => 'required',
            'group_id' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'decree_date.required'=>"Не указана дата приказа",
            'decree_number.required'=>"Не указан номер приказа",
            'entrantsData.required'=>'Не выбраны абитуринеты для зачисления',
            'group_id.required'=>"Не выбрана группа"
        ];
    }
}
