<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEducationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'education_id' => 'required',
            'graduation_organization_name' => 'required',
            'graduation_organization_place' => 'required',
            'graduation_date' => 'required|date_format:Y-m-d',
        ];
    }

    public function messages()
    {
        return [
            'education_id.required' => 'Не выбран уровень образования',
            'graduation_organization_name.required' => 'Не заполнено наименование образовательной организации',
            'graduation_organization_place.required' => 'Не заполнено местоположение образовательной организации',
            'graduation_date.required' => 'Не указана дата окончания образовательной организации',
            'graduation_date.date_format' => 'Дата окончания образовательной организации имеет некорректный формат',
        ];
    }
}
