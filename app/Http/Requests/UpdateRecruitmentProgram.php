<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRecruitmentProgram extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selection_committee_id' => 'required|numeric',
            'profession_program_id' => 'required|numeric',
            'reception_plan' => 'required|numeric',
            'count_with_documents' => 'required|numeric',
            'count_with_out_documents' => 'required|numeric',
            'number_of_enrolled' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'selection_committee_id.required' => 'Не выбрана приемная комиссия',
            'selection_committee_id.numeric' => 'Не выбрана приемная комиссия',
            'profession_program_id.required' => 'Не выбрана программа',
            'profession_program_id.numeric' => 'Не выбрана программа',
            'reception_plan.required' => 'Не заполнен план приема',
            'reception_plan.numeric' => 'План приема должен быть числом',
            'count_with_documents.required' => 'Не указано количество абитуриентов с оригиналами документов',
            'count_with_documents.numeric' => 'Количество абитуриентов с оригиналами документов должно быть целым числом',
            'count_with_out_documents.required' => 'Не указано количество абитуриентов без оригиналов документов',
            'count_with_out_documents.numeric' => 'Количество абитуриентов без оригиналов документов должно быть числом',
            'number_of_enrolled.numeric' => 'Кол-во зачисленных должно быть числом',
            'number_of_enrolled.required' => 'Не указано количество зачисленных',
        ];
    }
}
