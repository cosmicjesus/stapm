<?php

namespace App\Http\Requests\ProfessionProgram;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class CreateProfessionProgramAcademicRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession_program_id' => ['required'],
            'year_number' => [
                'required',
                'unique' => Rule::unique('profession_programs_academic_years', 'year_number')
                    ->where('profession_program_id', $this->request->get('profession_program_id'))
            ]
        ];
    }

    public function messages()
    {
        return [
            'profession_program_id.required' => 'Не указан индентификатор образовательной программы',
            'year_number.required'=>'Не выбран учебный год',
            'year_number.unique'=>'На выбранный учебый год уже сформирован раздел'
        ];
    }
}
