<?php

namespace App\Http\Requests\ProfessionProgram;

use Illuminate\Foundation\Http\FormRequest;

class CopySubjectsFromAcademicYearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'copy_year_id' => 'required_if:copyFromYear,true',
            'year_number' => 'required',
            'profession_program_id' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'copy_year_id.required_if' => 'Не выбран год, из которого будем копировать',
            'profession_program_id.required' => 'Не передан ID профессиональной программы',
            'profession_program_id.integer' => 'Передан некорректный ID программы',
        ];
    }
}
