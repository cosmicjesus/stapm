<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeePositionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date_format:Y-m-d',
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Не выбрана тип должности',
            'start_date.required' => 'Не указана дата назначения',
            'start_date.date_format' => 'Некорректный формат даты назначения',
        ];
    }
}
