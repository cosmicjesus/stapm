<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateConferenceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'date_of_event' => 'required|date_format:Y-m-d',
            'full_text' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Не заполнено название конкурса\\конференции',
            'date_of_event.required' => 'Не заполнена дата проведения мероприятия',
            'full_text.required' => 'Не заполнено описание события'
        ];
    }
}
