<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePositionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => [
                'required',
                Rule::unique('positions')->ignore($this->request->get('id'))
            ],
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.requires'=>'Поле Наименование должно быть заполнено',
            'name.unique'=>'Такая должность уже существует в базе',

        ];
    }

}
