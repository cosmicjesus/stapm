<?php

namespace App\Http\Requests\TrainingGroup;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class CreateTrainingGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession_program_id' => ['required'],
            'education_plan_id' => ['required'],
            'calendar_id' => ['required'],
            'pattern' => ['required'],
            'unique_code' => ['required', Rule::unique('training_groups')]
        ];
    }

    public function messages()
    {
        return [
            'unique_code.required' => 'Не заполнен код группы',
            'unique_code.unique' => 'Группа с таким кодом уже существует',
            'profession_program_id.required' => 'Не выбрана профессиональная программа',
            'education_plan_id.required' => 'Не выбран учебный план',
            'pattern.required' => 'Не указан шаблон имени учебной группы',
            'calendar_id.required' => 'Не выбран учебный календарь'
        ];
    }
}
