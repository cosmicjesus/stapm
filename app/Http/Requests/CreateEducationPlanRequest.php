<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEducationPlanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession_program_id' => 'required',
            'count_period' => 'required',
            'name' => 'required',
            'start_training' => 'required|date|date_format:Y-m-d',
            'end_training' => 'required|date|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'profession_program_id.required' => 'Не выбрана профессиональная программа',
            'count_period.required' => 'Не указано количество семестров',
            'name.required' => 'Не указано наименование учебного плана',
            'start_training.required' => 'Не указана дата начала обучения',
            'start_training.date_format' => 'Указан неверный формат даты',
            'end_training.required' => 'Не указана дата окончания обучения',
            'end_training.date_format' => 'Указан неверный формат даты',
        ];
    }
}
