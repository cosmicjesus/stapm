<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\Request;

class CreatePreviousJobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'Не заполнена дата приема',
            'end_date.required' => 'Не заполнена дата увольнения',
        ];
    }
}
