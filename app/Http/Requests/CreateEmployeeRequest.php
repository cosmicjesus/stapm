<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required',
            'firstname' => 'required',
            'birthday' => 'required|date_format:Y-m-d',
            'education.education_id' => 'required',
            'position.position_id' => 'required',
            'position.start_date' => 'required|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'lastname.required' => 'Фамилия не заполнена',
            'firstname.required' => 'Имя не заполнено',
            'birthday.required' => 'Дата рождения не заполнена',
            'birthday.date_format' => 'Некорректный формат даты рождения',
            'education.education_id.required' => 'Не выбран уровень образования',
            'position.position_id.required' => 'Не выбрана должность',
            'position.start_date.required' => 'Не указана дата приема на работу',
            'position.start_date.date_format' => 'Некорректный формат даты приема на работу'
        ];
    }
}
