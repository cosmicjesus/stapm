<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransferStudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array',
            'group_id' => 'required',
            'decree_number' => 'required',
            'decree_date' => 'required|date_format:Y-m-d',
        ];
    }

    public function messages()
    {
        return [
            'ids.required'=>'Не выбраны студенты для отчисления',
            'ids.array'=>'Идентификаторы студентов должы быть массивом',
            'group_id.required' => 'Не выбрана группа',
            'decree_number.required' => 'Не указан номер приказа',
            'decree_date.required' => 'Не указана дата приказа',
            'decree_date.date_format' => 'Дата приказа имеет некорректный формат',
        ];
    }
}
