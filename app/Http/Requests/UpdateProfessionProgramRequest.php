<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfessionProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession' => 'required',
            'form' => 'required',
            'duration' => 'required',
            'qualification' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'profession.required' => 'Выберите профессию',
            'form.required' => 'Выберите форму обучения',
            'duration.required' => 'Заполните длительность обучения',
            'duration.min' => 'Значение поля Длительность обучения должно быть в диапазоне от 10 до 120',
            'duration.max' => 'Значение поля Длительность обучения должно быть в диапазоне от 10 до 120',
            'qualification.required' => 'Заполните поле Квалификации',
            'type.required' => 'Выберите тип программы',
        ];
    }
}
