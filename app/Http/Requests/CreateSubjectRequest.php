<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSubjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле Наименование не заполнено',
            'type_id.required' => 'Не выбран тип дисциплины'
        ];
    }
}
