<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'start_date' => 'date_format:Y-m-d',
            'hours' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Не заполнено Наименование",
            'start_date.date_format' => "Некорректный формат даты периода прохождения",
            'hours.required' => "Не указано количество часов"
        ];
    }
}
