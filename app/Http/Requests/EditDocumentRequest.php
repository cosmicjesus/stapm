<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:1|max:255',
            'categories' => 'required',
            'sort' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Не заполнено наименование',
            'name.min' => 'Длина поля Наименование должно быть не меньше 1 и не более 255',
            'name.max' => 'Длина поля Наименование должно быть не меньше 1 и не более 255',
            'categories.required' => 'Не выбраны категории',
            'sort.required' => 'Не указана сортировка',
        ];
    }
}
