<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {

//        if ($request->header('host') == env('V2_ADMIN_DOMAIN_FRONT')) {
//            return parent::render($request, $exception);
//        }
//
//        $folders = [
//            env('ADMIN_DOMAIN') => 'admin',
//            env('DOMAIN') => 'client'
//        ];

//        //dd(get_class($exception));
//
//        switch ($exception) {
//            case get_class($exception) == 'Illuminate\Auth\Access\AuthorizationException':
//                return response()->view("errors." . $folders[$request->header('host')] . ".error", [
//                    'error_code' => '403',
//                    'error_message' => 'Упс! Похоже у вас нет доступа к выбранному разделу :('
//                ], 403);
//            case get_class($exception) == 'Symfony\Component\HttpKernel\Exception\NotFoundHttpException':
//                return response()->view("errors." . $folders[$request->header('host')] . ".404", [
//                    'error_code' => '404',
//                    'error_message' => 'Страница не найдена. Возможно она удалена или перемещена по другому адресу'
//                ], 404);
//
//        }

        return parent::render($request, $exception);
    }

}

