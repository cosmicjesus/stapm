<?php

namespace App\Exceptions;

use Exception;

class NonEditableObjectException extends Exception
{
    //
}
