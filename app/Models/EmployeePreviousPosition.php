<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmployeePreviousPosition
 *
 * @property int $id
 * @property int $employee_id
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property int $is_speciality_exp
 * @property string $organization_name
 * @property string $position_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereIsSpecialityExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereOrganizationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition wherePositionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePreviousPosition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmployeePreviousPosition extends Model
{
    protected $table = 'employee_previous_positions';

    protected $fillable = [
        'employee_id',
        'start_date',
        'end_date',
        'is_speciality_exp',
        'organization_name',
        'position_name'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $casts = ['is_speciality_exp'=>'boolean'];

}
