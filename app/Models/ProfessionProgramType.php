<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProfessionProgramType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfessionProgramType whereUpdatedAt($value)
 */
class ProfessionProgramType extends Model
{
    protected $fillable=[
      'name',
      'abbreviation'
    ];
}
