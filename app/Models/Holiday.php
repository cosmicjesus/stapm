<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Holiday
 *
 * @property int $id
 * @property string $description
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Holiday whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Holiday extends Model
{
    protected $dates = ['start_date', 'end_date'];

    protected $fillable = ['description', 'start_date', 'end_date'];
}