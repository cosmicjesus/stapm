<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecruitmentCenter
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentCenter whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RecruitmentCenter extends Model
{
    protected $fillable = ['name'];
}
