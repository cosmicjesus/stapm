<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;

/**
 * App\Models\ReplacingClassesImage
 *
 * @property int $id
 * @property int $replacing_class_id
 * @property string $path
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereReplacingClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClassesImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ReplacingClass $parentEntity
 */
class ReplacingClassesImage extends Model
{
    use HasImageUploads;

    protected static $imageFields = [
        'path'=>[
            'width' => 1065
        ]
    ];
    protected $fillable = ['replacing_class_id', 'path', 'sort'];

    protected $imagesUploadDisk = 'replace_class';

    // auto upload allowed
    protected $autoUploadImages = true;
    protected $imagesUploadPath = '';

    public function parentEntity()
    {
        return $this->belongsTo(ReplacingClass::class, 'replacing_class_id', 'id');
    }

    protected function pathUploadFilePath(UploadedFile $file)
    {
        return $this->parentEntity->date_of_replacing->format('Y-m-d') . '/' . Str::random(32) . '.' . $file->getClientOriginalExtension();
    }
}
