<?php

namespace App\Models;


use App\Models\Traits\WhereLikeTrait;
use App\User;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Student
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property int|null $education_id
 * @property int|null $profession_program_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $passport_issuance_date
 * @property float|null $avg
 * @property \Illuminate\Support\Carbon|null $graduation_date
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property bool|null $has_original
 * @property bool $has_certificate
 * @property int $need_hostel
 * @property string $status
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $phone
 * @property int $person_with_disabilities
 * @property int|null $exemption
 * @property string|null $graduation_organization_name
 * @property string|null $graduation_organization_place Населеный пункт уч.заведения
 * @property bool|null $contract_target_set
 * @property \Illuminate\Support\Carbon|null $date_of_actual_transfer
 * @property array|null $passport_data Паспорт
 * @property string|null $snils СНИЛС
 * @property string|null $inn
 * @property string|null $language Изучаемый язык
 * @property string|null $nationality Гражданство
 * @property array|null $addresses
 * @property int|null $privileged_category
 * @property int|null $health_category
 * @property int|null $disability
 * @property \Illuminate\Support\Carbon|null $enrollment_date
 * @property bool $long_absent
 * @property string|null $medical_policy_number
 * @property array|null $ratings_map
 * @property string|null $military_document_type Тип документа воинского учета
 * @property string|null $military_document_number Номер документа воинского учета
 * @property string|null $fitness_for_military_service Категория годности к военной службе
 * @property string|null $military_recruitment_office Название военкомата
 * @property string|null $military_specialty Специальность
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Decree[] $decrees
 * @property-read \App\Models\Education $education
 * @property-read mixed $full_name
 * @property-read \App\Models\TrainingGroup $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserParent[] $parents
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyStudents()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereAddresses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereAvg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereContractTargetSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDateOfActualTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereDisability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEnrollmentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereExemption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereFitnessForMilitaryService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationOrganizationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGraduationOrganizationPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHasCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHasOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHealthCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereLongAbsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMedicalPolicyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryRecruitmentOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitarySpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNeedHostel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePassportData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePassportIssuanceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePersonWithDisabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePrivilegedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRatingsMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSnils($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePhoto($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudentFile[] $files
 * @property int|null $selection_committee_id
 * @property int|null $recruitment_program_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSelectionCommitteeId($value)
 * @property array|null $preferential_categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student wherePreferentialCategories($value)
 * @property string|null $email Email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEmail($value)
 * @property string|null $military_status
 * @property int|null $recruitment_center_id
 * @property-read \App\Models\RecruitmentCenter $recruitmentCenter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereRecruitmentCenterId($value)
 * @property string|null $how_find_out Откуда узнали
 * @property int|null $invited_student_id ID Пригласившего студента
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereHowFindOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereInvitedStudentId($value)
 * @property-read \App\Models\Student $invitedStudent
 * @property int|null $nominal_number Поименный номер
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereNominalNumber($value)
 * @property int|null $reinstate_group_id ID группу в которую будет восстановлен студент
 * @property int|null $enrollment_decree_id ID приказа о зачислении
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereEnrollmentDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereReinstateGroupId($value)
 * @property string|null $military_rank Воинское звание
 * @property string|null $military_composition Состав
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Decree[] $enrollmentDecree
 * @property-read \App\Models\TrainingGroup|null $reinstateGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereMilitaryRank($value)
 * @property-read int|null $activities_count
 * @property-read int|null $decrees_count
 * @property-read int|null $enrollment_decree_count
 * @property-read int|null $files_count
 * @property-read int|null $parents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $credential
 * @property-read int|null $credential_count
 */
class Student extends Model
{

    use Sluggable;
    use SluggableScopeHelpers;
    use WhereLikeTrait;
    use LogsActivity;
    use HasImageUploads;

    public static $expelledStatus = 'expelled';
    public static $studentStatus = 'student';
    public static $armyStatus = 'inArmy';
    public static $academicStatus = 'academic';
    public static $graduationStatus = 'graduation';

    protected $appends = ['full_name'];
    protected static $logName = 'student';

    protected $imagesUploadDisk = 'student';

    protected $autoUploadImages = true;
    protected $imagesUploadPath = '';

    protected static $imageFields = [
        'photo' => [
            'width' => 200
        ]
    ];


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $recordEvents = ['updated'];
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {

        if (env('ACTIVITY_LOGGER_ENABLED')) {

            if ($eventName == 'updated') {
                $group_id = $this->group_id;
                $groups = TrainingGroup::query()->onlyTeach()->get()->keyBy('id')->map(function ($group) {
                    return $group->full_name;
                })->toArray();

                $oldStatus = $this->getOriginal('status');

                if ($this->status == self::$expelledStatus) {
                    return "Студент {$this->lastname} {$this->firstname} {$this->middlename} был отчислен из группы {$groups[$group_id]}";
                } elseif (in_array($this->status, [self::$academicStatus, self::$armyStatus])) {
                    return "Студент {$this->lastname} {$this->firstname} {$this->middlename} был отправлен в академический отпуск из группы {$groups[$group_id]}";
                } elseif (in_array($oldStatus, [self::$academicStatus, self::$armyStatus]) && ($this->status == self::$studentStatus)) {
                    return "Студент {$this->lastname} {$this->firstname} {$this->middlename} был восстановлен в составе группы {$groups[$group_id]}";
                } elseif ($this->getOriginal('group_id') != $this->group_id && ($this->getOriginal('group_id') != null)) {
                    return "Студент {$this->lastname} {$this->firstname} {$this->middlename} был переведен из группы {$groups[$this->getOriginal('group_id')]} в группу {$groups[$group_id]}";
                }
            }


            return "Студент {$this->lastname} {$this->firstname} {$this->middlename} был " . eventName($eventName, 'male');
        }

        return '1';
    }

    protected function photoUploadFilePath(UploadedFile $file)
    {
        return $this->id . '/' . Str::random(32) . '.' . $file->getClientOriginalExtension();
    }

    public $fillable = [
        'lastname',
        'firstname',
        'middlename',
        'birthday',
        'gender',
        'snils',
        'inn',
        'phone',
        'addresses',
        'privileged_category',
        'need_hostel',
        'medical_policy_number',
        'person_with_disabilities',
        'date_of_actual_transfer',
        'has_certificate',
        'long_absent',
        'status',
        'profession_program_id',
        'group_id',
        'enrollment_date',
        'comment',
        'language',
        'disability',
        'start_date',
        'education_id',
        'has_original',
        'graduation_organization_name',
        'graduation_organization_place',
        'graduation_date',
        'avg',
        'ratings_map',
        'contract_target_set',
        'preferential_categories',
        'disability',
        'health_category',
        'person_with_disabilities',
        'has_certificate',
        'medical_policy_number',
        'military_document_type',//Тип документа ВУ
        'military_document_number',//Номер документа ВУ
        'fitness_for_military_service',//Категория годности к ВС
        'military_recruitment_office',//Название военкомата
        'military_specialty',//Специальность,
        'military_rank',
        'military_composition',
        'preferential_categories',
        'selection_committee_id',
        'recruitment_program_id',
        'email',
        'nationality',
        'passport_issuance_date',
        'passport_data',
        'recruitment_center_id',
        'military_status',
        'how_find_out',
        'invited_student_id',
        'enrollment_decree_id',
        'reinstate_group_id',
        'nominal_number'
    ];

    protected static $logAttributes = ['*'];

    protected $dates = [
        'graduation_date',
        'enrollment_date',
        'start_date',
        'passport_issuance_date',
        'birthday',
        'date_of_actual_transfer'
    ];

    protected $casts = [
        'has_original' => 'boolean',
        'contract_target_set' => 'boolean',
        'person_with_disabilities' => 'boolean',
        'has_certificate' => 'boolean',
        'need_hoslet' => 'boolean',
        'long_absent' => 'boolean',
        'passport_data' => 'json',
        'ratings_map' => 'json',
        'addresses' => 'json',
        'preferential_categories' => 'json'
    ];


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['lastname', 'firstname', 'middlename']
            ]
        ];
    }

    public function education()
    {
        return $this->hasOne(Education::class, 'id', 'education_id');
    }

    public function program()
    {
        return $this->hasOne(ProfessionProgram::class, 'id', 'profession_program_id');
    }

    public function group()
    {
        return $this->hasOne(TrainingGroup::class, 'id', 'group_id');
    }

    public function decrees()
    {
        return $this->belongsToMany(Decree::class, 'student_decrees','student_id','decree_id')
            ->withPivot(['type', 'reason', 'group_from', 'group_to', 'id'])
            ->orderBy('date', 'DESC')
            ->orderBy('number', 'DESC');
    }

    public function recruitmentCenter()
    {
        return $this->hasOne(RecruitmentCenter::class, 'id', 'recruitment_center_id')->withDefault();
    }

    public function parents()
    {
        return $this->belongsToMany(UserParent::class, 'student_parent', 'student_id', 'parent_id');
    }

    public function files()
    {
        return $this->hasMany(StudentFile::class, 'student_id', 'id');
    }

    public function invitedStudent()
    {
        return $this->hasOne(self::class, 'id', 'invited_student_id');
    }

    public static function findUnique($request)
    {
        return self::where('lastname', $request->lastname)
            ->where('firstname', $request->firstname)
            ->where('middlename', $request->middlename)
            ->where('birthday', Carbon::createFromFormat('d.m.Y', $request->birthday)->format('Y-m-d'))
            ->first();
    }

    public static function checkUnique($input)
    {
        $human = self::query()->where('firstname', $input['firstname'])
            ->where('lastname', $input['lastname'])
            ->where('middlename', $input['middlename'])
            ->where('birthday', $input['birthday'])
            ->first();

        if (isset($input['id']) && !is_null($human)) {
            return !($human->id == $input['id']);
        }

        return $human;
    }

    public function getFullNameAttribute()
    {
        return $this->lastname . " " . $this->firstname . " " . $this->middlename;
    }

    public function scopeOnlyTeach(Builder $query)
    {
        return $query->whereIn('status', [self::$studentStatus, self::$armyStatus, self::$academicStatus]);
    }

    public function reinstateGroup()
    {
        return $this->belongsTo(TrainingGroup::class, 'reinstate_group_id', 'id');
    }

    public function enrollmentDecree()
    {
        return ($this->belongsToMany(Decree::class, 'student_decrees')
            ->withPivot(['type', 'reason', 'group_from', 'group_to', 'id'])
            ->orderBy('date', 'DESC')
            ->orderBy('number', 'DESC')
        );
    }

    public function scopeOnlyStudents($query)
    {
        return $query->where('status', self::$studentStatus);
    }

    public function credential()
    {
        return $this->morphMany(User::class, 'profile');
    }

}
