<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Help
 *
 * @property int $id
 * @property int $student_id
 * @property int $number
 * @property \Illuminate\Support\Carbon|null $date_of_application
 * @property \Illuminate\Support\Carbon|null $date_of_issue
 * @property array|null $params
 * @property string $type
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Student $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereDateOfApplication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereDateOfIssue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help onlyMilitary()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help onlyTraining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Help onlyMoney()
 */
class Help extends Model
{
    const types = ['training' => 'training', 'money' => 'money', 'military' => 'military'];
    const status = ['issued', 'booked'];

    protected $fillable = ['status', 'date_of_application', 'date_of_issue', 'params', 'student_id', 'type', 'number'];

    protected $dates = [
        'date_of_application',
        'date_of_issue'
    ];

    protected $casts = [
        'params' => 'array'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function setDateOfIssueAttribute()
    {
        $this->attributes['date_of_issue'] = Carbon::now()->format('Y-m-d H:i:s');
    }

    public function setDateOfApplicationAttribute()
    {
        $this->attributes['date_of_application'] = Carbon::now()->format('Y-m-d H:i:s');
    }

    public static function getMaxNumber()
    {
        return self::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->max('number');
    }

    public static function hideReference($id)
    {
        self::findOrFail($id)->update(['status' => 'issued', 'date_of_issue' => Carbon::now()->format('Y-m-d H:i:s')]);
    }

    public function scopeOnlyMilitary(Builder $query)
    {
        return $query->where('type', self::types['military']);
    }

    public function scopeOnlyTraining(Builder $query)
    {
        return $query->where('type', self::types['training']);
    }

    public function scopeOnlyMoney(Builder $query)
    {
        return $query->where('type', self::types['money']);
    }
}
