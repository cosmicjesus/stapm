<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StudentParent
 *
 * @property int $id
 * @property int $student_id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property int|null $relationshipType
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $gender
 * @property-read \App\Models\Student $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereRelationshipType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentParent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudentParent extends Model
{
    public function student()
    {
        return $this->hasOne(Student::class,'id','student_id');
    }
}