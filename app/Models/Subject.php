<?php

namespace App\Models;

use App\Models\SubjectType;
use App\Models\Traits\WhereLikeTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;
/**
 * App\Models\Subject
 *
 * @property-read \App\Models\SubjectType $type
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $type_id
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProfessionProgram[] $programs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subject whereUpdatedAt($value)
 * @property-read int|null $activities_count
 * @property-read int|null $programs_count
 */
class Subject extends Model
{

    use WhereLikeTrait;
    use LogsActivity;

    protected $fillable = ['name', 'type_id'];
    protected static $logAttributes = ['name', 'type_id'];
    protected static $logName = 'subject';

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Дисциплина была " . eventName($eventName, 'female');
    }


    protected static function boot()
    {
        parent::boot();
        static::creating(function () {

        });
    }

    public function type()
    {
        return $this->hasOne(SubjectType::class, 'id', 'type_id');
    }

    public function programs()
    {
        return $this->belongsToMany(ProfessionProgram::class, 'program_subjects');
    }

    public static function checkUnique($input)
    {
        $subject = self::where('name', $input['name'])->where('type_id', $input['type_id'])->first();

        if (isset($input['id']) && !is_null($subject)) {
            return $subject->id == $input['id'];
        }

        return is_null($subject);
    }

}
