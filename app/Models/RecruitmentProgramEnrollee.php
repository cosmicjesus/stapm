<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecruitmentProgramEnrollee
 *
 * @property int $id
 * @property int $recruitment_program_id
 * @property int $enrollee_id
 * @property int $is_priority
 * @property int $sum_of_grades_of_specialized_disciplines
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereEnrolleeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereIsPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereSumOfGradesOfSpecializedDisciplines($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitmentProgramEnrollee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RecruitmentProgramEnrollee extends Model
{
    protected $table='recruitment_program_enrollees';

    protected $fillable=['recruitment_program_id','enrollee_id','is_priority','sum_of_grades_of_specialized_disciplines'];


}
