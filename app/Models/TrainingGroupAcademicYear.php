<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrainingGroupAcademicYear
 *
 * @property int $id
 * @property int|null $training_group_id Айдишник группы
 * @property int|null $training_calendar_id Айдишник учебного календаря
 * @property int|null $education_plan_period_id Айдишник периода учебного плана
 * @property int $number Номер
 * @property int $year Академический год
 * @property bool $active Флаг активности
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingCalendar $calendar
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroupAcademicYear[] $terms
 * @property-read \App\Models\TrainingGroup|null $trainingGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereEducationPlanPeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereTrainingGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereYear($value)
 * @mixin \Eloquent
 * @property-read \App\Models\TrainingGroupAcademicYearTerm $activeTerm
 * @property-read mixed $next_academic_year
 * @property int $is_past Флаг того, пройден ли период
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYear whereIsPast($value)
 * @property-read int|null $terms_count
 */
class TrainingGroupAcademicYear extends Model
{
    protected $fillable = [
        'training_group_id',
        'training_calendar_id',
        'education_plan_period_id',
        'number',
        'year',
        'active',
        'is_past'
    ];

    protected $appends = ['next_academic_year'];

    protected $casts = [
        'active' => 'boolean',
        'is_past' => 'boolean'
    ];

    public function trainingGroup()
    {
        return $this->belongsTo(TrainingGroup::class);
    }

    public function terms()
    {
        return $this->hasMany(TrainingGroupAcademicYearTerm::class, 'academic_year_id', 'id');
    }

    public function activeTerm()
    {
        return $this->hasOne(TrainingGroupAcademicYearTerm::class, 'academic_year_id', 'id')->where('active', true);
    }

    public function calendar()
    {
        return $this->belongsTo(TrainingCalendar::class, 'training_calendar_id', 'id');
    }

    public function getNextAcademicYearAttribute()
    {

        return self::where([
            'training_group_id' => $this->training_group_id,
            'year' => ($this->year + 1)
        ])->first();
    }
}
