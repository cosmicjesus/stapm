<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StudentDecree
 *
 * @property int $id
 * @property int $student_id
 * @property int $decree_id
 * @property string|null $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $type
 * @property string|null $reason
 * @property string|null $group_from
 * @property string|null $group_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereGroupFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereGroupTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentDecree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudentDecree extends Model
{

}