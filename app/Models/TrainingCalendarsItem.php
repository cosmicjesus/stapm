<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrainingCalendarsItem
 *
 * @property int $id
 * @property int|null $training_calendar_id
 * @property int $number
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property \Illuminate\Support\Carbon|null $session_start_date
 * @property \Illuminate\Support\Carbon|null $session_end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingCalendar $calendar
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereSessionEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereSessionStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendarsItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrainingCalendarsItem extends Model
{
    protected $fillable = [
        'training_calendar_id',
        'number',
        'start_date',
        'end_date',
        'session_start_date',
        'session_end_date'
    ];

    protected $dates = [
        'start_date',
        'end_date',
        'session_start_date',
        'session_end_date'
    ];

    public function calendar()
    {
        return $this->belongsTo(TrainingCalendar::class, 'id', 'training_calendar_id');
    }
}
