<?php

namespace App\Models\Entrant;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class RecruitmentProgramEnrollee extends Pivot
{
    protected $casts = [
      'is_priority'=>'boolean'
    ];
}
