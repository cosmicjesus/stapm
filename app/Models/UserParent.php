<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserParent
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property string $relation_ship_type
 * @property string|null $phone
 * @property string|null $place_of_work
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $childrens
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent wherePlaceOfWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereRelationShipType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $snils
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserParent whereSnils($value)
 * @property-read mixed $full_name
 * @property-read int|null $childrens_count
 */
class UserParent extends Model
{
    protected $table = 'parents';

    protected $fillable = [
        'lastname',
        'firstname',
        'middlename',
        'birthday',
        'place_of_work',
        'gender',
        'phone',
        'relation_ship_type',
        'snils'
    ];

    protected $appends = ['full_name'];

    protected $dates = [
        'birthday'
    ];

    public function childrens()
    {
        return $this->belongsToMany(Student::class, 'student_parent', 'parent_id', 'student_id');
    }

    public function getFullNameAttribute()
    {
        return $this->lastname . " " . $this->firstname . " " . $this->middlename;
    }
}
