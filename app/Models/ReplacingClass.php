<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReplacingClass
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $date_of_replacing
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReplacingClassesImage[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereDateOfReplacing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReplacingClass whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $images_count
 */
class ReplacingClass extends Model
{
    protected $table = 'replacing_classes';

    protected $dates = ['date_of_replacing'];

    protected $fillable = ['date_of_replacing'];

    public function images()
    {
        return $this->hasMany(ReplacingClassesImage::class, 'replacing_class_id', 'id');
    }

    public function previous()
    {
        return self::whereDate('date_of_replacing', '<', $this->date_of_replacing)->max('date_of_replacing');
    }

    public function next()
    {
        return self::whereDate('date_of_replacing', '>', $this->date_of_replacing)->min('date_of_replacing');
    }
}
