<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProgramBasicDocument
 *
 * @property int $id
 * @property int $profession_program_id
 * @property string|null $standart
 * @property string|null $annotation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereStandart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramBasicDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProgramBasicDocument extends Model
{
    protected $fillable = ['annotation', 'standart','profession_program_id'];
}