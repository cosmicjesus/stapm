<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewsImage
 *
 * @property int $id
 * @property int $news_id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NewsImage extends Model
{
    protected $fillable = ['path', 'news_id'];
}
