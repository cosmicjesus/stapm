<?php

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\PriemProgram
 *
 * @property int $id
 * @property int $profession_program_id
 * @property int $reception_plan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $count_with_documents
 * @property int $count_with_out_documents
 * @property int $number_of_enrolled
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCountWithDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCountWithOutDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereNumberOfEnrolled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereReceptionPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $selection_committee_id
 * @property-read \App\Models\SelectionCommittee|null $selectionCommittee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereSelectionCommitteeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $entrants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property bool $student_source
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PriemProgram whereStudentSource($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RecruitEntrantDate[] $recruitDates
 * @property-read int|null $activities_count
 * @property-read int|null $entrants_count
 * @property-read int|null $recruit_dates_count
 * @property-read int|null $students_count
 */
class PriemProgram extends Model
{
    use LogsActivity;
    use Sluggable;
    use SluggableScopeHelpers;

    protected static string $logName = 'recruitment_program';


    /**
     * @param Activity $activity
     * @param string $eventName
     */
    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['committee_date','program.slug'],
                'separator' => '-'
            ]
        ];
    }

    protected static bool $logOnlyDirty = true;
    protected static array $logAttributesToIgnore = ['updated_at'];
    protected static array $logAttributes = ['*'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Программа приема была " . eventName($eventName, 'female');
    }

    protected $fillable = [
        'slug',
        'reception_plan',
        'count_with_documents',
        'count_with_out_documents',
        'selection_committee_id',
        'number_of_enrolled',
        'student_source'
    ];

    protected $casts = [
        'student_source' => 'boolean'
    ];

    public function getCommitteeDateAttribute()
    {
        $startCommitteeDate = $this->selectionCommittee->start_date;
        $nextYear = $startCommitteeDate->addYear();
        return $this->selectionCommittee->start_date->format('Y')."-".$nextYear->format('Y');
    }

    public function program()
    {
        return $this->hasOne(ProfessionProgram::class, 'id', 'profession_program_id');
    }

    public function selectionCommittee()
    {
        return $this->belongsTo(SelectionCommittee::class);
    }

    public function entrants()
    {
        return $this->belongsToMany(Entrant::class, 'recruitment_program_enrollees', 'recruitment_program_id', 'enrollee_id')
            ->orderBy('lastname')->orderBy('firstname')->withPivot(['is_priority', 'sum_of_grades_of_specialized_disciplines']);
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'recruitment_program_id', 'id')->whereIn('status', ['student', 'inArmy', 'academic'])->orderBy('lastname')->orderBy('firstname');
    }

    public function recruitDates()
    {
        return $this->hasMany(RecruitEntrantDate::class, 'recruitment_program_id', 'id')->orderBy('date');
    }
}
