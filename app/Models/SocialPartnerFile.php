<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialPartnerFile
 *
 * @property int $id
 * @property int $social_partner_id
 * @property string $name
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereSocialPartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartnerFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SocialPartnerFile extends Model
{

}