<?php

namespace App\Models;

use App\Models\Traits\WhereLikeTrait;
use App\User;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Employee
 *
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property int $gender
 * @property \Illuminate\Support\Carbon $birthday
 * @property string|null $photo
 * @property int $category
 * @property \Illuminate\Support\Carbon|null $category_date
 * @property string $status
 * @property string|null $start_work_date
 * @property string|null $give_passport
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $general_experience
 * @property string|null $phones
 * @property string|null $emails
 * @property int $specialty_exp
 * @property string|null $scientific_degree
 * @property string|null $academic_title
 * @property string|null $dismiss_date
 * @property-read \App\Models\AdministrationEmployee $administrator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QualificationCourse[] $courses
 * @property-read \App\User $credentials
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Education[] $educations
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Position[] $positions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereAcademicTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCategoryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereDismissDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereEmails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGeneralExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGivePassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereScientificDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereSpecialtyExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereStartWorkDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmployeePreviousPosition[] $previous_exp
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmployeePreviousPosition[] $previousExp
 * @property bool $use_dynamic_exp
 * @property-read int|null $courses_count
 * @property-read int|null $educations_count
 * @property-read int|null $positions_count
 * @property-read int|null $previous_exp_count
 * @property-read int|null $subjects_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereUseDynamicExp($value)
 * @property string|null $military_document_type Тип документа ВУ
 * @property string|null $military_document_number Номер документа документа ВУ
 * @property string|null $fitness_for_military_service Категория годности к ВС
 * @property string|null $military_specialty Специальность
 * @property string|null $military_rank Воинское звание
 * @property string|null $military_composition Группа учета
 * @property string|null $military_status Статус ВУ
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereFitnessForMilitaryService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitaryComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitaryDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitaryDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitaryRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitarySpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereMilitaryStatus($value)
 * @property int|null $recruitment_center_id
 * @property string|null $group_of_accounting Группа учета
 * @property string|null $reserve_category Группа запаса
 * @property int $is_on_special_accounting Состоит на спецучете
 * @property int $has_military_preparation Имеет военную подготовку
 * @property-read \App\Models\RecruitmentCenter $recruitmentCenter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereGroupOfAccounting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereHasMilitaryPreparation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereIsOnSpecialAccounting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereRecruitmentCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employee whereReserveCategory($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $credential
 * @property-read int|null $credential_count
 * @property-read mixed $age
 */
class Employee extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;
    use WhereLikeTrait;

    protected $appends = ['full_name','age'];
    protected $dates = ['birthday', 'category_date'];

    protected $fillable = [
        'lastname',
        'firstname',
        'middlename',
        'birthday',
        'category',
        'category_date',
        'gender',
        'general_experience',
        'status',
        'specialty_exp',
        'scientific_degree',
        'academic_title',
        'start_work_date',
        'use_dynamic_exp',
        'military_document_type',
        'military_document_number',
        'fitness_for_military_service',
        'military_specialty',
        'military_rank',
        'military_composition',
        'military_status',
        'recruitment_center_id',
        'group_of_accounting',
        'reserve_category',
        'is_on_special_accounting',
        'has_military_preparation'
    ];
    protected $casts = [
        'use_dynamic_exp' => 'boolean',
        'has_military_preparation' => 'boolean',
        'is_on_special_accounting' => 'boolean',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['lastname', 'firstname', 'middlename']
            ]
        ];
    }

    public function educations()
    {
        return $this->belongsToMany(Education::class, 'employee_educations')
            ->withPivot(['qualification', 'id', 'direction_of_preparation', 'graduation_organization_name']);
    }

    public function positions()
    {
        return $this->belongsToMany(Position::class, 'employee_positions')
            ->where('active', true)
            ->orderBy('date_layoff', 'ASC')
            ->orderBy('start_date', 'ASC')
            ->withPivot(['start_date', 'experience', 'date_layoff', 'type', 'sort', 'active', 'id']);
    }

    public function courses()
    {
        return $this->hasMany(QualificationCourse::class)
            ->orderBy('start_date', 'DESC');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'employee_subjects')->withPivot('id');
    }

    public function administrator()
    {
        return $this->hasOne(AdministrationEmployee::class);
    }

    public function getFullNameAttribute()
    {
        return $this->lastname . " " . $this->firstname . " " . $this->middlename;
    }

    public function getAgeAttribute(){
        return Carbon::now()->diffInYears($this->birthday);
    }

    public function credentials()
    {
        return $this->hasOne(User::class, 'user_profile_id', 'id');
    }

    public function recruitmentCenter()
    {
        return $this->hasOne(RecruitmentCenter::class, 'id', 'recruitment_center_id')->withDefault();
    }

    public static function checkUnique($input)
    {
        $employee = self::query()->where('firstname', $input['firstname'])
            ->where('lastname', $input['lastname'])
            ->where('middlename', $input['middlename'])
            ->where('birthday', $input['birthday'])
            ->first();

        if (isset($input['id']) && !is_null($employee)) {
            return $employee->id == $input['id'];
        }

        return is_null($employee);
    }

    public function previous_exp()
    {
        return $this->hasMany(EmployeePreviousPosition::class, 'employee_id', 'id')->orderBy('start_date');
    }

    public function credential()
    {
        return $this->morphMany(User::class, 'profile');
    }
}
