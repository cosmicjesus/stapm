<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use QCod\ImageUp\HasImageUploads;

/**
 * App\Models\File
 *
 * @property int $id
 * @property string $name
 * @property string|null $path
 * @property int $sort
 * @property int $active
 * @property string|null $objectable_type
 * @property int|null $objectable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereObjectableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereObjectableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class File extends Model
{
    use HasImageUploads;

    protected $fillable = ['name', 'active', 'sort', 'objectable_id', 'objectable_type', 'path'];

    protected static $fileFields = [
        'path'
    ];

    protected $imagesUploadDisk = 'files';

    // auto upload allowed
    protected $imagesUploadPath = '';

    protected function pathUploadFilePath($file)
    {
        return (Carbon::now())->timestamp . '/' . str_random(20) . '.' . $file->getClientOriginalExtension();
    }
}
