<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EducationForm
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationForm whereUpdatedAt($value)
 */
class EducationForm extends Model
{

   protected $fillable=[
     'name'
   ];
}
