<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrainingGroupCalendar
 *
 * @property int $id
 * @property int|null $training_group_id ID группы
 * @property int|null $training_calendar_id ID календаря
 * @property bool $active Флаг активности
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereTrainingCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereTrainingGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupCalendar whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrainingGroupCalendar extends Model
{
    protected $fillable = ['training_group_id', 'training_calendar_id', 'active', 'sort'];

    protected $casts = [
        'active' => 'boolean'
    ];
}
