<?php

namespace App\Models\Traits;


trait WhereLikeTrait
{
    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'like', '%' . $value . '%');
    }
    public function scopeOrLike($query, $field, $value)
    {
        return $query->orWhere($field, 'like', '%' . $value . '%');
    }
}