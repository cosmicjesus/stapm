<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProfessionProgramsAcademicYear
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_program_id ID программы
 * @property int $year_number Номер года
 * @property int $active Флаг активности
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ProfessionProgram $profession_program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgramsAcademicYear whereYearNumber($value)
 */
class ProfessionProgramsAcademicYear extends Model
{
    protected $fillable = ['profession_program_id', 'year_number', 'active'];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function profession_program()
    {
        return $this->belongsTo(ProfessionProgram::class);
    }
}
