<?php

namespace App\Models;


use App\Models\Traits\WhereLikeTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;

/**
 * App\Models\News
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $image
 * @property \Illuminate\Support\Carbon $publication_date
 * @property \Illuminate\Support\Carbon|null $visible_to
 * @property bool $active
 * @property string $preview
 * @property string $full_text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NewsFile[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NewsImage[] $images
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereVisibleTo($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 * @property-read int|null $images_count
 */
class News extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;
    use WhereLikeTrait;
    use HasImageUploads;

    protected $table = 'news';
    protected $fillable = ['title', 'publication_date', 'visible_to', 'active', 'preview', 'full_text', 'type'];
    protected $dates = [
        'publication_date',
        'visible_to'
    ];
    protected $casts = [
        'active' => 'boolean'
    ];
    protected $imagesUploadDisk = 'news';

    protected static $imageFields = [
        'image' => [
            'width' => 500,
        ]
    ];
    protected $imagesUploadPath = '';

    protected function imageUploadFilePath($file)
    {
        return $this->id . "/images" .Str::random(16) . '.' . $file->getClientOriginalExtension();
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['title']
            ]
        ];
    }


    public function files()
    {
        return $this->hasMany(NewsFile::class, 'news_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(NewsImage::class, 'news_id', 'id');
    }


    public static function findBySlugOrFail(string $slug, array $columns = ['*'])
    {
        return static::whereSlug($slug)->where('active', true)
            ->where(function ($query) {
                $query->where('visible_to', '>=', Carbon::now()->format('Y-m-d'))
                    ->orWhereNull('visible_to');
            })->firstOrFail($columns);
    }

    public function hasPrevious()
    {
        $date = self::where('publication_date', '<', $this->publication_date)->max('publication_date');
        return self::where('publication_date', $date)->where(function ($query) {
            $query->where('visible_to', '>=', Carbon::now()->format('Y-m-d'))
                ->orWhereNull('visible_to');
        })->orderBy('id', 'DESC')->first();
    }

    public function hasNext()
    {
        $date = self::where('publication_date', '>', $this->publication_date)->max('publication_date');
        return self::where('publication_date', $date)->where(function ($query) {
            $query->where('visible_to', '>=', Carbon::now()->format('Y-m-d'))
                ->orWhereNull('visible_to');
        })->orderBy('id', 'DESC')->first();
    }

    public function setActiveAttribute($value)
    {
        if ($value == 'true') {
            $this->attributes['active'] = 1;
        } else {
            $this->attributes['active'] = 0;
        }
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = (int)$value;
    }

    public function setVisibleToAttribute($value)
    {
        if ($value == 'null') {
            $this->attributes['visible_to'] = null;
        } else {
            $this->attributes['visible_to'] = $value;
        }
    }
}
