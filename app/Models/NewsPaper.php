<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewsPaper
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $publication_date
 * @property int $number
 * @property string|null $cover_path
 * @property string|null $publication_path
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereCoverPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper wherePublicationPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsPaper whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NewsPaper extends Model
{
    protected $dates=[
        'publication_date'
    ];
}
