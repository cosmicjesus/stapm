<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdministrationEmployee
 *
 * @property int $id
 * @property int $employee_id
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee $employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdministrationEmployee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdministrationEmployee extends Model
{
    protected $fillable=['sort'];

    public function employee(){
        return $this->hasOne(Employee::class,'id','employee_id');
    }
}