<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PositionType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PositionType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PositionType extends Model
{
    protected $fillable=[
        'name'
    ];
}
