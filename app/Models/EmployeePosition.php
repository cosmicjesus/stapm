<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmployeePosition
 *
 * @property int $id
 * @property int $employee_id
 * @property int $position_id
 * @property string $start_date
 * @property int $experience
 * @property string|null $date_layoff
 * @property int $type
 * @property int $sort
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereDateLayoff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition wherePositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeePosition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmployeePosition extends Model
{
    //
}
