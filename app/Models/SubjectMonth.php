<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubjectMonth
 *
 * @property int $id
 * @property int $month_id
 * @property int $subject_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\LoadMonth $month
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth whereMonthId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectMonth whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LoadSubjectItem[] $loadItems
 * @property-read int|null $load_items_count
 */
class SubjectMonth extends Model
{
    protected $fillable=['month_id','subject_id'];

    public function month(){
        return $this->belongsTo(LoadMonth::class,'month_id','id');
    }

    public function loadItems(){
        return $this->hasMany(LoadSubjectItem::class,'month_id','id');
    }
}
