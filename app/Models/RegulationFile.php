<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RegulationFile
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $regulation_type
 * @property int $regulation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereRegulationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereRegulationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RegulationFile extends Model
{
    protected $fillable = [
        'name',
        'path'
    ];
}