<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Regulation
 *
 * @property int $id
 * @property string $name_of_the_supervisory
 * @property string|null $comment
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationFile[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationItem[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereNameOfTheSupervisory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regulation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 * @property-read int|null $items_count
 */
class Regulation extends Model
{
    

    public function items()
    {
        return $this->hasMany(RegulationItem::class);
    }

    public function files()
    {
        return $this->morphMany(RegulationFile::class, 'regulation');
    }

}