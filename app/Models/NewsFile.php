<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewsFile
 *
 * @property int $id
 * @property int $news_id
 * @property string $name
 * @property string|null $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NewsFile extends Model
{
    protected $fillable=['name','path'];
}