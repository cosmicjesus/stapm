<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecruitEntrantDate
 *
 * @property int $id
 * @property int $recruitment_program_id ID программы приема
 * @property string|null $date Дата приема
 * @property int|null $count Количество
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PriemProgram $recruitProgram
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecruitEntrantDate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RecruitEntrantDate extends Model
{
    protected $fillable = ['recruitment_program_id', 'date', 'count'];

    public function recruitProgram()
    {
        return $this->hasOne(PriemProgram::class);
    }
}
