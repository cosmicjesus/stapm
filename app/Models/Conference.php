<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Conference
 *
 * @property int $id
 * @property string $title
 * @property string $date_of_event
 * @property string $full_text
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereDateOfEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference whereSlug($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\File[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conference findSimilarSlugs($attribute, $config, $slug)
 * @property-read int|null $activities_count
 * @property-read int|null $files_count
 */
class Conference extends Model
{
    use Sluggable;
    use LogsActivity;
    use SluggableScopeHelpers;
    protected $fillable = ['title', 'date_of_event', 'full_text', 'active'];

    protected $dates = ['date_of_event'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'conference';

    protected $casts = [
        'active' => 'boolean'
    ];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Конкурс был " . eventName($eventName, 'male');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'objectable');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['title']
            ]
        ];
    }

    public function setActiveAttribute($value)
    {
        if ($value == 'true') {
            $this->attributes['active'] = 1;
        } else {
            $this->attributes['active'] = 0;
        }
    }
}
