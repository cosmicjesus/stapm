<?php

namespace App\Models;

use App\ProfessionProgramType;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\EducationPlan;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ProfessionProgram
 *
 * @property-read \App\Models\EducationForm $education_form
 * @property-read mixed $full_name
 * @property-read \App\Models\Profession $profession
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereSlug($slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProfessionProgram withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_id
 * @property int $education_form_id
 * @property int|null $type_id
 * @property string $slug
 * @property int $trainin_period
 * @property \Illuminate\Support\Carbon|null $licence
 * @property string|null $qualification
 * @property string|null $description
 * @property int $sort
 * @property bool $on_site
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $presentation_path
 * @property string|null $video_presentation_path
 * @property string|null $reduction
 * @property-read \App\Models\ProgramBasicDocument $documents
 * @property-read mixed $name_with_form
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramDocument[] $otherDocuments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EducationPlan[] $plans
 * @property-read \App\Models\PriemProgram $priem
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @property-read \App\ProfessionProgramType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereEducationFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereLicence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereOnSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram wherePresentationPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereProfessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereReduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTraininPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereVideoPresentationPath($value)
 * @property bool $top_fifty
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereTopFifty($value)
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereImage($value)
 * @property bool $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProfessionProgram whereActive($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProfessionProgramAcademicYear[] $academicYears
 * @property-read int|null $academic_years_count
 * @property-read int|null $activities_count
 * @property-read int|null $groups_count
 * @property-read int|null $other_documents_count
 * @property-read int|null $plans_count
 * @property-read int|null $students_count
 * @property-read int|null $subjects_count
 */
class ProfessionProgram extends Model
{
    use SoftDeletes;
    use Sluggable;
    use SluggableScopeHelpers;
    use LogsActivity;
    use HasImageUploads;


    //program_files

    protected $imagesUploadDisk = 'program_files';

    // auto upload allowed
    protected $autoUploadImages = true;
    protected $imagesUploadPath = 'images';

    protected static $imageFields = [
        'image' => [
            'width' => 500
        ]
    ];

    protected function pathUploadFilePath($file)
    {
        return $this->id . "/" .  $this->id . '-' . Str::random(32) . '.' . $file->getClientOriginalExtension();
    }

    protected $casts = [
        'on_site' => 'boolean',
        'active' => 'boolean',
        'top_fifty' => 'boolean'
    ];

    protected $slugKeyName = 'slug';

    protected $fillable = [
        'profession_id',
        'education_form_id', 'trainin_period', 'type_id', 'qualification', 'licence', 'sort', 'on_site', 'top_fifty', 'active'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['profession.name', 'education_form.name']
            ]
        ];
    }

    protected static $logAttributes = ['*'];
    protected static $logName = 'profession_program';


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Профессиональная программа {$this->name_with_form} была " . eventName($eventName, 'female');
    }

    public function profession()
    {
        return $this->hasOne('App\Models\Profession', 'id', 'profession_id');
    }

    public function education_form()
    {
        return $this->belongsTo('App\Models\EducationForm');
    }

    public function type()
    {
        return $this->hasOne(ProfessionProgramType::class, 'id', 'type_id');
    }

    public function academicYears()
    {
        return $this->hasMany(ProfessionProgramsAcademicYear::class)->orderBy('year_number');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'program_subjects')
            //->orderBy('name')
            ->withPivot(['in_revision', 'id', 'index', 'section_id', 'program_academic_year_id'])->orderBy('section_id');
    }

    public function plans()
    {
        return $this->hasMany(EducationPlan::class)->orderBy('start_training');
    }

    public function priem()
    {
        return $this->hasOne(PriemProgram::class);
    }

    public function getFullNameAttribute()
    {
        return $this->profession->code . " " . $this->profession->name;
    }

    public function getNameWithFormAttribute()
    {
        //$this->load('profession', 'education_form');

        return $this->profession->code . " " . $this->profession->name . " | " . $this->education_form->name;
    }

    public function documents()
    {
        return $this->hasOne(ProgramBasicDocument::class);
    }

    public function otherDocuments()
    {
        return $this->morphMany(ProgramDocument::class, 'programtagle');
    }

    public function additionals()
    {
        return $this->otherDocuments()
            ->where('type', '!=', 'metodical')
            ->orderBy('start_date');
    }

    public function calendars()
    {
        return $this->otherDocuments()->where('type', 'calendar')->orderBy('start_date');
    }

    public function akts()
    {
        return $this->otherDocuments()->where('type', 'akt')->orderBy('start_date');
    }

    public function ppssz()
    {
        return $this->otherDocuments()->where('type', 'ppssz')->orderBy('start_date');
    }

    public function metodicals()
    {
        return $this->otherDocuments()->where('type', 'metodical')->orderBy('title');
    }

    public function enrollees($date = null)
    {
        $query = $this->hasMany(Student::class, 'profession_program_id', 'id')->where('status', 'enrollee');

        if (!is_null($date)) {
            $query->whereDate('start_date', '<=', $date);
        }

        return $query;
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'profession_program_id', 'id')->whereIn('status', ['student', 'academic', 'inArmy']);
    }

    public function groups()
    {
        return $this->hasMany(TrainingGroup::class, 'profession_program_id', 'id')->where('status', 'teach')->orderBy('course');
    }


    protected $dates = [
        'licence'
    ];

    protected $appends = [
        'full_name',
        'name_with_form'
    ];
}
