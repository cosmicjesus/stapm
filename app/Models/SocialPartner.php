<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialPartner
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image_path
 * @property string $site_url
 * @property int $sort
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $contract_type
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SocialPartnerFile[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereContractType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereSiteUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialPartner whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 */
class SocialPartner extends Model
{

    protected $dates=[
        'start_date',
        'end_date'
    ];

    public function files()
    {
        return $this->hasMany(SocialPartnerFile::class);
    }

}