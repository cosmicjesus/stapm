<?php

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TochkaRostaItem
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property \Illuminate\Support\Carbon $publication_date
 * @property string $preview
 * @property string $full_text
 * @property bool $active
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TochkaRostaPhoto[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereFullText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem wherePreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 */
class TochkaRostaItem extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $dates = ['publication_date'];

    protected $casts = ['active' => 'boolean'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['title']
            ]
        ];
    }

    public function files()
    {
        return $this->hasMany(TochkaRostaPhoto::class, 'news_id', 'id');
    }
}