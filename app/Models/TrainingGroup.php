<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\TrainingGroup
 *
 * @property int $id
 * @property int $profession_program_id
 * @property int $education_plan_id
 * @property int|null $employee_id
 * @property string $unique_code
 * @property string $pattern
 * @property int $max_people
 * @property int $course
 * @property int $period
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $term_type
 * @property string|null $expelled_decree_number
 * @property string|null $expelled_decree_date
 * @property-read \App\Models\Employee $curator
 * @property-read \App\Models\EducationPlan $education_plan
 * @property-read mixed $full_name
 * @property-read \App\Models\ProfessionProgram $program
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereEducationPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereMaxPeople($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup wherePattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereTermType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereUniqueCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $expelled_decree_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereExpelledDecreeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingCalendar[] $calendars
 * @property int|null $training_calendar_item_id ID элемента календаря
 * @property-read \App\Models\TrainingCalendarsItem|null $activePeriod
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroup whereTrainingCalendarItemId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroupAcademicYear[] $academicYears
 * @property-read \App\Models\TrainingGroupAcademicYear $currentAcademicYear
 * @property-read int|null $academic_years_count
 * @property-read int|null $calendars_count
 * @property-read int|null $students_count
 */
class TrainingGroup extends Model
{
    protected $appends = ['full_name'];
    protected $fillable = ['profession_program_id', 'education_plan_id', 'employee_id', 'unique_code', 'pattern', 'status', 'term_type', 'max_people'];

    public function program()
    {
        return $this->hasOne(ProfessionProgram::class, 'id', 'profession_program_id');
    }

    public function education_plan()
    {
        return $this->hasOne(EducationPlan::class, 'id', 'education_plan_id');
    }

    public function curator()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'group_id', 'id')
            ->whereIn('status', ['student', 'academic', 'inArmy'])
            ->orderBy('lastname');
    }

    public function scopeOnlyTeach($query)
    {
        return $query->where('status', 'teach');
    }

    public function calendars()
    {
        return $this->belongsToMany(TrainingCalendar::class, 'training_group_calendars')
            ->orderBy('training_group_calendars.sort')
            ->withPivot(['active', 'sort'])
            ->with('periods')
            ->withTimestamps();
    }

    public function activeCalendar()
    {
        $calendar = ($this->calendars())->where('active', true);
        return $calendar;
    }

    public function activePeriod()
    {
        return $this->belongsTo(TrainingCalendarsItem::class, 'training_calendar_item_id', 'id');
    }

    public function academicYears()
    {
        return $this->hasMany(TrainingGroupAcademicYear::class)->orderBy('year');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currentAcademicYear()
    {
        return $this->hasOne(TrainingGroupAcademicYear::class)->where('active', true);
    }

    public function getFullNameAttribute()
    {
        $yearNumber = $this->currentAcademicYear->number;
        //dd($yearNumber);
        return str_replace('{Курс}', $yearNumber, $this->pattern);
    }
}
