<?php

namespace App\Models;

use App\Models\Traits\WhereLikeTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Decree
 *
 * @property int $id
 * @property string $number
 * @property \Illuminate\Support\Carbon $date
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $file_path
 * @property int|null $term
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Student[] $students
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Decree whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $students_count
 */
class Decree extends Model
{

    use WhereLikeTrait;
    protected $fillable = ['number', 'date', 'type', 'file_path', 'term'];

    public static $enrollmentType = 'enrollment';
    public static $allocationType = 'allocation';
    public static $graduationType = 'graduation';
    public static $transferType = 'transfer';
    public static $internalTransferType = 'internal_transfer';
    public static $reinstateType = 'reinstate';

    protected $dates = [
        'date'
    ];

    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_decrees')
            ->orderBy('lastname')->orderBy('firstname')
            ->withPivot(['type', 'reason', 'group_from', 'group_to', 'id']);
    }
}