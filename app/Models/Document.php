<?php

namespace App\Models;


use App\Models\Traits\WhereLikeTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Document
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $name
 * @property string|null $path
 * @property int $sort
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array|null $category_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoriesDocument[] $categories
 * @property-read \App\Models\CategoriesDocument $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCategoryIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Document whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $categories_count
 */
class Document extends Model
{

    use WhereLikeTrait;

    protected $casts = [
        'category_ids' => 'array',
        'active' => 'boolean'
    ];

    protected $fillable = ['name', 'active', 'sort','path'];

    public function category()
    {
        return $this->hasOne(CategoriesDocument::class, 'id', 'category_id');
    }

    public function categories()
    {
        return $this->belongsToMany(CategoriesDocument::class, 'category_documents', 'document_id', 'category_document_id');
    }

    public function setActiveAttribute($value)
    {
        $newVal = null;
        if ($value=='true') {
            $newVal = 1;
        } else {
            $newVal = 0;
        }
        $this->attributes['active'] = $newVal;
    }
}