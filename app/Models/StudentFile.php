<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Webpatser\Uuid\Uuid;

/**
 * App\Models\StudentFile
 *
 * @property int $id
 * @property int|null $student_id
 * @property string|null $name
 * @property string|null $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $uuid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StudentFile whereUuid($value)
 * @property-read int|null $activities_count
 */
class StudentFile extends Model
{

    use LogsActivity;

    protected $fillable = ['name', 'path'];

    protected static $logName = 'student_file';


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logAttributes = ['*'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Был " . eventName($eventName, 'male') . " файл студента";
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }
}
