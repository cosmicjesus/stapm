<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GroupLoadSubject
 *
 * @property int $id
 * @property int $group_load_id
 * @property int $subject_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Subject $subject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereGroupLoadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubject whereComment($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GroupLoadSubjectPeriod[] $terms
 * @property-read int|null $terms_count
 */
class GroupLoadSubject extends Model
{
    protected $fillable = ['group_load_id', 'subject_id'];

    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    public function terms()
    {
        return $this->hasMany(GroupLoadSubjectPeriod::class,'id','load_subject_id');
    }
}
