<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\QualificationCourse
 *
 * @property int $id
 * @property int $employee_id
 * @property string $title
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property int $count_hours
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereCountHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QualificationCourse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class QualificationCourse extends Model{

    protected $fillable=[
      'title',
      'start_date',
      'end_date',
      'count_hours',
      'description'
    ];

    protected $dates=[
        'start_date',
        'end_date'
    ];

}