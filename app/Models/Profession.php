<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Profession
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession findSimilarSlugs($attribute, $config, $slug)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profession withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read mixed $full_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Profession whereUpdatedAt($value)
 */
class Profession extends Model
{
    use SoftDeletes;
    use Sluggable;
    protected $appends = ['full_name'];
    protected $fillable = ['code', 'name'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['code', 'name']
            ]
        ];
    }

    public function getFullNameAttribute()
    {
        return $this->code . " " . $this->name;
    }
}
