<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ProgramSubject
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramSubject withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $profession_program_id
 * @property int $subject_id
 * @property string|null $index
 * @property string|null $file
 * @property int $in_revision
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgramDocument[] $files
 * @property-read \App\Models\Subject $subject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereInRevision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property int|null $section_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereSectionId($value)
 * @property int|null $year
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereYear($value)
 * @property int|null $program_academic_year_id ID учебного года профпрограммы
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramSubject whereProgramAcademicYearId($value)
 * @property-read \App\Models\ProfessionProgram $professionProgram
 * @property-read int|null $activities_count
 * @property-read int|null $files_count
 */
class ProgramSubject extends Model
{
    use LogsActivity;

    protected $fillable = ['profession_program_id', 'subject_id', 'in_revision', 'index', 'section_id', 'program_academic_year_id'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'program_subject';
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at', 'created_at', 'deleted_at'];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'В программу была добавлена дисциплина';
                break;
            case 'deleted':
                return 'Из программы была удалена дисциплина';
                break;
            default:
                return $eventName;
        }

    }

    public function files()
    {
        return $this->morphMany(ProgramDocument::class, 'programtagle');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function professionProgram()
    {
        return $this->belongsTo(ProfessionProgram::class);
    }

    public static function checkSubjectInProgram($program_id, $subject_id)
    {
        return self::where(['profession_program_id' => $program_id, 'subject_id' => $subject_id])->count();
    }

    public static function createAndReturn($input)
    {
        $model = self::create([
            'profession_program_id' => $input['program_id'],
            'subject_id' => $input['subject_id'],
            'section_id' => $input['section_id'],
            'index' => $input['index'],
            'program_academic_year_id' => $input['program_academic_year_id'],
            'in_revision' => false
        ]);

        $model->load('subject');

        return [
            'id' => $model->id,
            'name' => $model->subject->name,
            'profession_program_id' => $input['program_id'],
            'index' => $input['index'],
            'section_id' => $input['section_id'],
            'program_academic_year_id' => $input['program_academic_year_id'],
            'files' => []
        ];
    }
}
