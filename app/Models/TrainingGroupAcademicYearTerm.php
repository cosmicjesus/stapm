<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrainingGroupAcademicYearTerm
 *
 * @property int $id
 * @property int|null $academic_year_id ID академического года
 * @property int|null $calendar_item_id ID элемента учебного календаря
 * @property int|null $number Номер периода
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingGroupAcademicYear|null $academicYear
 * @property-read \App\Models\TrainingCalendarsItem|null $calendarItem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereAcademicYearId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereCalendarItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $active Флаг активности
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereActive($value)
 * @property-read mixed $next_term
 * @property int $is_past Флаг того, пройден ли период
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingGroupAcademicYearTerm whereIsPast($value)
 */
class TrainingGroupAcademicYearTerm extends Model
{
    protected $fillable = ['academic_year_id', 'calendar_item_id', 'number', 'active', 'is_past'];

    protected $casts = [
        'active' => 'boolean',
        'is_past' => 'boolean'
    ];

    protected $appends = ['next_term'];

    public function academicYear()
    {
        return $this->belongsTo(TrainingGroupAcademicYear::class);
    }

    public function calendarItem()
    {
        return $this->belongsTo(TrainingCalendarsItem::class, 'calendar_item_id');
    }

    public function getNextTermAttribute()
    {
        return self::where(['academic_year_id' => $this->academic_year_id, 'number' => ($this->number + 1)])->first();
    }
}
