<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CategoriesDocument
 *
 * @property int $id
 * @property string $name
 * @property int $sort
 * @property int|null $parent_id
 * @property int $active
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoriesDocument[] $childs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $filesCount
 * @property-read \App\Models\CategoriesDocument $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriesDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $childs_count
 * @property-read int|null $documents_count
 * @property-read int|null $files_count
 * @property-read int|null $files_count_count
 */
class CategoriesDocument extends Model
{
    protected $fillable = ['id', 'name', 'sort', 'parent_id','slug'];

    public function files()
    {
        return $this->hasMany(Document::class, 'category_id', 'id')->orderBy('sort');
    }

    public function filesCount()
    {
        return $this->hasMany(Document::class, 'category_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany(CategoriesDocument::class, 'parent_id', 'id')->orderBy('sort');
    }

    public function parent()
    {
        return $this->hasOne(CategoriesDocument::class, 'id', 'parent_id');
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class, 'category_documents', 'category_document_id', 'document_id')->orderBy('sort');
    }

}
