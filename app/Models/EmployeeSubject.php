<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmployeeSubject
 *
 * @property int $id
 * @property int $employee_id
 * @property int $subject_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeSubject whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmployeeSubject extends Model
{

}