<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TochkaRostaPhoto
 *
 * @property int $id
 * @property int $news_id
 * @property string $path
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TochkaRostaPhoto whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TochkaRostaPhoto extends Model
{

}