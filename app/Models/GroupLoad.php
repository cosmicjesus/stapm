<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GroupLoad
 *
 * @property int $id
 * @property int $academic_year
 * @property int $group_id
 * @property int $group_period_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrainingGroup $group
 * @property-read \App\Models\TrainingGroupAcademicYear $groupPeriod
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereAcademicYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereGroupPeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoad whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @property-read int|null $subjects_count
 */
class GroupLoad extends Model
{
    protected $fillable = ['academic_year', 'group_id', 'group_period_id'];

    public function group()
    {
        return $this->hasOne(TrainingGroup::class, 'id', 'group_id');
    }

    public function groupPeriod()
    {
        return $this->hasOne(TrainingGroupAcademicYear::class, 'id', 'group_period_id');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'group_load_subjects', 'group_load_id', 'subject_id')->orderBy('name')->withPivot(['id']);
    }
}
