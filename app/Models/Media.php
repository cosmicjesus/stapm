<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Media
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property int $is_youtube
 * @property string $url
 * @property string $source
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereIsYoutube($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereUrl($value)
 */
class Media extends Model
{
    protected $table = 'medias';

    protected $casts = [
        'is_youtube' => 'boolean',
        'sort' => 'integer'
    ];
}


