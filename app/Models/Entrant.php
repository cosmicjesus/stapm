<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;
use App\Models\Entrant\RecruitmentProgramEnrollee;

/**
 * App\Models\Entrant
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Decree[] $decrees
 * @property-read \App\Models\Education $education
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudentFile[] $files
 * @property-read mixed $full_name
 * @property-read \App\Models\TrainingGroup $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserParent[] $parents
 * @property-read \App\Models\ProfessionProgram $program
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student like($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyStudents()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student onlyTeach()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student orLike($field, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Student whereSlug($slug)
 * @mixin \Eloquent
 * @property int $id
 * @property string $lastname
 * @property string $firstname
 * @property string|null $middlename
 * @property string $slug
 * @property \Illuminate\Support\Carbon $birthday
 * @property int $gender
 * @property string|null $photo
 * @property int|null $education_id
 * @property int|null $profession_program_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $passport_issuance_date
 * @property float|null $avg
 * @property \Illuminate\Support\Carbon|null $graduation_date
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property bool|null $has_original
 * @property bool $has_certificate
 * @property int $need_hostel
 * @property string $status
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $phone
 * @property int $person_with_disabilities
 * @property int|null $exemption
 * @property string|null $graduation_organization_name
 * @property string|null $graduation_organization_place Населеный пункт уч.заведения
 * @property bool|null $contract_target_set
 * @property \Illuminate\Support\Carbon|null $date_of_actual_transfer
 * @property array|null $passport_data Паспорт
 * @property string|null $snils СНИЛС
 * @property string|null $inn
 * @property string|null $language Изучаемый язык
 * @property string|null $nationality Гражданство
 * @property array|null $addresses
 * @property int|null $privileged_category
 * @property int|null $health_category
 * @property int|null $disability
 * @property \Illuminate\Support\Carbon|null $enrollment_date
 * @property bool $long_absent
 * @property string|null $medical_policy_number
 * @property array|null $ratings_map
 * @property string|null $military_document_type Тип документа воинского учета
 * @property string|null $military_document_number Номер документа воинского учета
 * @property string|null $fitness_for_military_service Категория годности к военной службе
 * @property string|null $military_recruitment_office Название военкомата
 * @property string|null $military_specialty Специальность
 * @property int|null $selection_committee_id
 * @property int|null $recruitment_program_id
 * @property array|null $preferential_categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereAddresses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereAvg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereContractTargetSet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereDateOfActualTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereDisability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEnrollmentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereExemption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereFitnessForMilitaryService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationOrganizationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGraduationOrganizationPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHasCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHasOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHealthCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereLongAbsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMedicalPolicyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryRecruitmentOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitarySpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNeedHostel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePassportData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePassportIssuanceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePersonWithDisabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePreferentialCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant wherePrivilegedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRatingsMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRecruitmentProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereSelectionCommitteeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereSnils($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereUpdatedAt($value)
 * @property string|null $email Email
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEmail($value)
 * @property-read \App\Models\PriemProgram|null $recruitment_program
 * @property string|null $military_status
 * @property int|null $recruitment_center_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereRecruitmentCenterId($value)
 * @property-read \App\Models\RecruitmentCenter $recruitmentCenter
 * @property string|null $how_find_out Откуда узнали
 * @property int|null $invited_student_id ID Пригласившего студента
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereHowFindOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereInvitedStudentId($value)
 * @property-read \App\Models\Student $invitedStudent
 * @property int|null $nominal_number Поименный номер
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereNominalNumber($value)
 * @property int|null $reinstate_group_id ID группу в которую будет восстановлен студент
 * @property int|null $enrollment_decree_id ID приказа о зачислении
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereEnrollmentDecreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereReinstateGroupId($value)
 * @property string|null $military_rank Воинское звание
 * @property string|null $military_composition Состав
 * @property-read \App\Models\Decree $enrollmentDecree
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryComposition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entrant whereMilitaryRank($value)
 * @property-read \App\Models\TrainingGroup|null $reinstateGroup
 * @property-read int|null $activities_count
 * @property-read int|null $decrees_count
 * @property-read int|null $enrollment_decree_count
 * @property-read int|null $files_count
 * @property-read int|null $parents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $credential
 * @property-read int|null $credential_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PriemProgram[] $recruitment_programs
 * @property-read int|null $recruitment_programs_count
 */
class Entrant extends Student
{

    use HasImageUploads;

    protected static $logName = 'entrant';
    protected $table = 'students';

    public static $entrantStatus = 'enrollee';

    protected $imagesUploadDisk = 'student';

    protected $autoUploadImages = true;
    protected $imagesUploadPath = '';

    protected static $imageFields = [
        'path' => [
            'width' => 200
        ]
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('status', function (Builder $builder) {
            $builder->whereIn('status', [self::$entrantStatus, 'unconfirmed_enrollee']);
        });
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Абитуриент {$this->lastname} {$this->firstname} {$this->middlename} был " . eventName($eventName, 'male');
    }

    public function recruitment_program()
    {
        return $this->belongsTo(PriemProgram::class)->with('program');
    }

    protected function pathUploadFilePath(UploadedFile $file)
    {
        return $this->id . '/' . Str::random(32) . '.' . $file->getClientOriginalExtension();
    }

    public function recruitment_programs()
    {
        return $this->belongsToMany(PriemProgram::class,
            'recruitment_program_enrollees',
            'enrollee_id',
            'recruitment_program_id')
            ->orderByDesc('is_priority')
            ->with('program')
            ->withPivot('sum_of_grades_of_specialized_disciplines','id','is_priority','recruitment_program_id')
            ->using(RecruitmentProgramEnrollee::class);
    }
}
