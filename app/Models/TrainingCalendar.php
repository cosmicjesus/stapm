<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrainingCalendar
 *
 * @property int $id
 * @property string $name
 * @property string $year
 * @property string $term_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingCalendarsItem[] $periods
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereTermType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrainingCalendar whereYear($value)
 * @mixin \Eloquent
 * @property-read int|null $periods_count
 */
class TrainingCalendar extends Model
{
    protected $fillable = ['name', 'year', 'term_type'];

    public function periods()
    {
        return $this->hasMany(TrainingCalendarsItem::class, 'training_calendar_id', 'id')->orderBy('number');
    }
}
