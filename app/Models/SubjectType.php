<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubjectType
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $reduction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subject[] $subjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereReduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubjectType whereUpdatedAt($value)
 * @property-read int|null $subjects_count
 */
class SubjectType extends Model
{
    protected $fillable = [
        'name',
        'reduction'
    ];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'type_id', 'id')
            ->orderBy('name');
    }
}
