<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SelectionCommittee
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PriemProgram[] $priem_programs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $last_day_of_work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectionCommittee whereLastDayOfWork($value)
 * @property-read int|null $priem_programs_count
 */
class SelectionCommittee extends Model
{
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'last_day_of_work',
        'active'
    ];

    protected $dates = [
        'start_date',
        'end_date',
        'last_day_of_work'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function priem_programs()
    {
        return $this->belongsToMany(ProfessionProgram::class, 'priem_programs')
            ->withPivot(['id', 'reception_plan', 'count_with_documents', 'count_with_out_documents', 'number_of_enrolled', 'student_source'])->withTimestamps();
    }
}
