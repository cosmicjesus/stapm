<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\EmployeeEducation
 *
 * @property int $id
 * @property int $employee_id
 * @property int $education_id
 * @property string|null $qualification
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $direction_of_preparation
 * @property-read \App\Models\Education $education
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereDirectionOfPreparation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $graduation_organization_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmployeeEducation whereGraduationOrganizationName($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \App\Models\Employee $employee
 * @property-read int|null $activities_count
 */
class EmployeeEducation extends Model
{
    use LogsActivity;

    protected $fillable = ['qualification', 'direction_of_preparation', 'graduation_organization_name'];

    protected static $logName = 'employee_education';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        $employee = $this->employee->full_name;
        return "Уровень образования сотрудника {$employee} был " . eventName($eventName, 'male');
    }

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    public function education()
    {
        return $this->belongsTo(Education::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
