<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RegulationItem
 *
 * @property int $id
 * @property int $regulation_id
 * @property string $name_of_violation
 * @property string|null $result
 * @property string $status
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegulationFile[] $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereNameOfViolation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereRegulationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegulationItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $files_count
 */
class RegulationItem extends Model
{
    public function files()
    {
        return $this->morphMany(RegulationFile::class, 'regulation');
    }

}