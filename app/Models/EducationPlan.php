<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\EducationPlan
 *
 * @property int $id
 * @property int $profession_program_id
 * @property string $name
 * @property int $count_period
 * @property \Illuminate\Support\Carbon $start_training
 * @property \Illuminate\Support\Carbon $end_training
 * @property string|null $file
 * @property string|null $full_path
 * @property bool $active
 * @property int $in_revision
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property array|null $extra_options
 * @property-read \App\Models\ProfessionProgram $professionProgram
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrainingGroup[] $training_groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereCountPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereEndTraining($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereExtraOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereFullPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereInRevision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereProfessionProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereStartTraining($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EducationPlan whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComponentDocument[] $components
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read int|null $components_count
 * @property-read int|null $training_groups_count
 */
class EducationPlan extends Model
{
    use LogsActivity;
    protected $dates = [
        'start_training',
        'end_training'
    ];

    protected $casts = [
        'extra_options' => 'array',
        'active' => 'boolean'
    ];

    protected $fillable = ['profession_program_id', 'name', 'count_period', 'start_training', 'end_training', 'full_path', 'extra_options', 'active', 'in_revision'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'education_plan';


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Учебный план {$this->name} был " . eventName($eventName, 'male');
    }

    public function professionProgram()
    {
        return $this->hasOne(ProfessionProgram::class, 'id', 'profession_program_id');
    }

    public function training_groups()
    {
        return $this->hasMany(TrainingGroup::class, 'education_plan_id', 'id');
    }

    public function components()
    {
        return $this->morphMany(ComponentDocument::class, 'file')->orderBy('sort');
    }

    public static function hideAfterReleaseGroup($plan_id)
    {
        $plan = self::query()->findOrFail($plan_id);
        $plan->active = false;
        $plan->save();
    }

}