<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LoadSubjectItem
 *
 * @property int $id
 * @property int $month_id
 * @property string $date
 * @property int $number
 * @property int $count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereMonthId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadSubjectItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LoadSubjectItem extends Model
{
    protected $fillable=['month_id','date','number','count'];
}
