<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Position
 *
 * @property int $id
 * @property string $name
 * @property int $position_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmployeePosition[] $employee_position
 * @property-read mixed $can_remove
 * @property-read \App\Models\PositionType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position wherePositionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Position whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $employee_position_count
 */
class Position extends Model
{

    protected $appends = ['can_remove'];
    protected $fillable = ['name', 'position_type_id'];

    public function type()
    {
        return $this->hasOne(PositionType::class, 'id', 'position_type_id');
    }

    public function employee_position()
    {
        return $this->hasMany(EmployeePosition::class, 'position_id', 'id');
    }

    public function getCanRemoveAttribute()
    {
        $count_empl = $this->employee_position()->count();

        return $count_empl > 0 ? false : true;
    }
}
