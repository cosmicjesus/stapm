<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GroupLoadSubjectPeriod
 *
 * @property int $id
 * @property int $load_subject_id
 * @property int $term_id
 * @property int $plan
 * @property int|null $semester
 * @property int $employee_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereLoadSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod wherePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereSemester($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereTermId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupLoadSubjectPeriod whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Employee $employee
 * @property-read \App\Models\TrainingCalendarsItem $term
 */
class GroupLoadSubjectPeriod extends Model
{
    protected $fillable = ['load_subject_id', 'term_id', 'plan', 'semester', 'employee_id'];

    public function term()
    {
        return $this->hasOne(TrainingGroupAcademicYearTerm::class, 'id', 'term_id');
    }

    public function employee(){
        return $this->hasOne(Employee::class, 'id', 'employee_id')->select(['id','lastname','middlename','firstname'])->withDefault();
    }
}
