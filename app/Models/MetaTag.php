<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MetaTag
 *
 * @property int $id
 * @property string $route
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaTag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MetaTag extends Model
{
    public static function getByRouteName($route)
    {
        return self::where('route', $route)->first();
    }
}