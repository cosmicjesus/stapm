<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ComponentDocument
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property int $sort
 * @property string $filelable_type
 * @property int $filelable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFilelableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFilelableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $file_type
 * @property int $file_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ComponentDocument whereFileType($value)
 * @property-read int|null $activities_count
 */
class ComponentDocument extends Model
{
    use LogsActivity;

    protected $fillable = ['name', 'sort', 'path'];
    protected static $logAttributes = ['*'];
    protected static $logName = 'component_document';
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at', 'created_at'];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Компонент документа был " . eventName($eventName, 'male');
    }
}
