<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ProgramDocument
 *
 * @property int $id
 * @property string $title
 * @property string $path
 * @property int $programtagle_id
 * @property string $programtagle_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereProgramtagleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereProgramtagleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComponentDocument[] $components
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $programtagle
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgramDocument whereActive($value)
 * @property-read int|null $activities_count
 * @property-read int|null $components_count
 */
class ProgramDocument extends Model
{
    use LogsActivity;

    protected $fillable = ['title', 'path', 'start_date', 'type', 'active'];

    protected $dates = ['start_date'];
    protected $casts = [
        'active' => 'boolean'
    ];

    protected static $logName = 'program_document';


    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->event = $eventName;
    }

    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logAttributes = ['*'];

    public function getDescriptionForEvent(string $eventName): string
    {

        return "Документ ОПОП был " . eventName($eventName, 'male');
    }

    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function programtagle()
    {
        return $this->morphTo();
    }

    public function components()
    {
        return $this->morphMany(ComponentDocument::class, 'file')->orderBy('sort');
    }

}