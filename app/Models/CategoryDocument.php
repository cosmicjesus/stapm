<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CategoryDocument
 *
 * @property int $id
 * @property int $category_document_id
 * @property int $document_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereCategoryDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryDocument whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CategoryDocument extends Model
{
    //
}
