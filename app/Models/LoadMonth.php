<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LoadMonth
 *
 * @property int $id
 * @property int $academic_year
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereAcademicYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $semester
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LoadMonth whereSemester($value)
 */
class LoadMonth extends Model
{
    protected $fillable = ['academic_year', 'name', 'start_date', 'end_date','semester'];

    protected $dates = ['start_date', 'end_date'];
}
