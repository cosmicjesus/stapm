<?php

namespace App\BusinessLogic\NewsPaper;


use App\Helpers\StorageFile;
use Jenssegers\Date\Date;
use Laravelrus\LocalizedCarbon\LocalizedCarbon;

class NewsPaper
{
    protected $newspaper;

    public function __construct(\App\Models\NewsPaper $newsPaper)
    {
        $this->newspaper = $newsPaper;
    }

    public function getId()
    {
        return $this->newspaper->id;
    }

    public function getNumber()
    {
        return $this->newspaper->number;
    }

    public function getCover()
    {
        return StorageFile::getNewsPaperFile($this->newspaper->cover_path);
    }

    public function getPublicationDate()
    {
        return $this->newspaper->publication_date->format('d.m.Y');
    }

    public function getTitle()
    {
        return "№ " . $this->newspaper->number . " " . (Date::createFromFormat('d.m.Y',$this->getPublicationDate())->format('F Y'));
    }

    public function getPublication()
    {
        return StorageFile::getNewsPaperFile($this->newspaper->publication_path);
    }

    public function getActiveStatus()
    {
        return $this->newspaper->active;
    }

    public function getActiveStatusStr()
    {
        return $this->newspaper->active ? 'Да' : 'Нет';
    }

    public function getEditUrl()
    {
        return 0;
    }

    public function getDeleteUrl()
    {
        return 0;
    }
}