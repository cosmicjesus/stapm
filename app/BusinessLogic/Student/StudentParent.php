<?php

namespace App\BusinessLogic\Student;


class StudentParent
{
    protected $parent;
    protected $student;

    public function __construct($parent)
    {
        $this->parent = $parent;
        $this->parent->load('student');
        $this->student = $this->parent->student;
    }

    public function getId()
    {
        return $this->parent->id;
    }

    public function getLastName()
    {
        return $this->parent->lastname;
    }

    public function getFirstName()
    {
        return $this->parent->firstname;
    }

    public function getMiddlename()
    {
        return $this->parent->middlename;
    }

    public function getFullName()
    {
        return $this->parent->lastname . " " . $this->parent->firstname . " " . $this->parent->middlename;
    }

    public function getGender()
    {
        return $this->parent->gender;
    }

    public function getGenderToStr()
    {
        return getGender($this->parent->gender);
    }

    public function getRelationShipType()
    {
        return $this->parent->relationshipType;
    }

    public function getStudent()
    {
        return new Student($this->student);
    }

    public function getRelationShipTypeStr()
    {
        return getRelationShipType($this->parent->relationshipType);
    }

    public function getPhone()
    {
        return $this->parent->phone;
    }
}