<?php

namespace App\BusinessLogic\Student;


use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use App\BusinessLogic\TrainingGroup\TrainingGroup;
use Carbon\Carbon;

class Student extends AbstractStudent
{
    protected $student;
    protected $program;
    protected $education;
    protected $group;
    protected $decrees;

    public function __construct($student)
    {
        $this->initialization($student);
        $this->student = $student;
        $this->decrees = $this->student->decrees->map(function ($decree) {
            return new StudentDecree($decree);
        });
    }


    protected function loadRelations()
    {
        $relations = [
            'program',
            'group',
            'education',
            'parents',
            'decrees'
        ];

        foreach ($relations as $relation) {
            if (!$this->student->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->student->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->student->id;
    }

    public function getLastName()
    {
        return $this->student->lastname;
    }

    public function getFirstName()
    {
        return $this->student->firstname;
    }

    public function getMiddleName()
    {
        return $this->student->middlename;
    }

    public function getAge()
    {
        $age = Carbon::now()->diffInYears(Carbon::createFromFormat('d.m.Y', $this->getBirthday()));
        return getNumEnding($age, ['год', 'года', 'лет'], true);
    }

    public function getAgeOnDate(string $dateToString, string $format = 'd.m.Y')
    {
        $age = Carbon::createFromFormat($format, $dateToString)
            ->diffInYears(Carbon::createFromFormat('d.m.Y', $this->getBirthday()));
        return $age;
    }

    public function getFullName()
    {
        return $this->student->lastname . " " . $this->student->firstname . " " . $this->student->middlename;
    }

    public function getGender()
    {
        return $this->student->gender;
    }

    public function getGenderToStr()
    {
        return getGender($this->student->gender);
    }

    public function getProgram()
    {
        return new ProfessionProgram($this->program);
    }

    public function getHasOriginal()
    {
        return $this->student->has_original;
    }

    public function getHasOriginalStr()
    {
        return $this->student->has_original ? 'Да' : 'Нет';
    }

    public function getPhone()
    {
        return $this->student->phone;
    }

    public function getPassportIssuanceDate()
    {
        if (is_null($this->student->passport_issuance_date)) {
            return 'Не указанно';
        }
        return $this->student->passport_issuance_date->format('d.m.Y');
    }

    public function getDocumentEndDate()
    {
        $birthay = $this->student->birthday;

        $age = Carbon::now()->diffInYears(Carbon::createFromFormat('d.m.Y', $this->getBirthday()));

        $year = $age < 45 ? 20 : 45;

        return Carbon::create($birthay->year + $year, $birthay->month, $birthay->day)->format('d.m.Y');
    }

    public function getDecrees()
    {
        return $this->decrees;
    }

    public function getDateOfActualTransfer()
    {
        if (is_null($this->student->date_of_actual_transfer)) {
            return null;
        }

        return $this->student->date_of_actual_transfer->format('d.m.Y');
    }

    public function getGroup()
    {
        return new TrainingGroup($this->student->group);
    }

    public function getStatus()
    {
        return getStudentStatus($this->student->status);
    }

    public function getStartDate()
    {
        return $this->student->start_date->format('d.m.Y');
    }


    public function isLongAbsent()
    {
        return $this->student->long_absent;
    }

    public function getMedicalPolicyNumber()
    {
        return $this->student->medical_policy_number;
    }

    public function getProfileUrl()
    {
        if ($this->student->status == 'enrollee') {
            return route('admin.enrollee.profile', ['id' => $this->student->id]);
        }

        return route('admin.students.profile', $this->student->slug);
    }

    public function getAgeOfStartCurrentYear()
    {
        $start_date = Carbon::now()->day(1)->month(1);

        return $start_date->diffInYears($this->student->birthday);
    }

    public function getAgeOfStartNextYear()
    {
        $start_date = Carbon::now()->day(1)->month(1)->addYear(1);

        return $start_date->diffInYears($this->student->birthday);
    }

    public function getEditLink()
    {
        return route('admin.students.edit', ['slug' => $this->student->slug]);
    }

    public function getEducation()
    {
        return $this->student->education->name;
    }

    public static function buildByID($id)
    {
        return new self(\App\Models\Student::findOrFail($id));
    }

}