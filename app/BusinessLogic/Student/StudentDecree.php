<?php

namespace App\BusinessLogic\Student;


class StudentDecree
{
    protected $decree;
    protected $pivot;


    public function __construct($decree)
    {
        $this->decree = $decree;
        $this->pivot = $decree->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getType()
    {
        return getDecreeType($this->pivot->type);
    }

    public function getDecreeId()
    {
        return $this->decree->id;
    }

    public function getNumber()
    {
        return $this->decree->number . " от " . $this->decree->date->format('d.m.Y');
    }

    public function getReason()
    {
        if (is_null($this->pivot->reason)) {
            return '';
        }

        return getReasons($this->pivot->type, $this->pivot->reason);
    }

    public function getDecreeText()
    {
        switch ($this->pivot->type) {
            case 'enrollment':
                $tpl = getDecreeTextTemplate('enrollment');
                return massStrReplace($tpl, ['group' => $this->pivot->group_to]);
                break;
            case 'allocation':
                $tpl = getDecreeTextTemplate('allocation');
                return massStrReplace($tpl, ['group' => $this->pivot->group_from]);
                break;
            case 'transfer':
                $tpl = getDecreeTextTemplate('transfer');
                return massStrReplace($tpl, [
                    'group_from' => $this->pivot->group_from,
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'internal_transfer':
                $tpl = getDecreeTextTemplate('internal_transfer');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
        }
    }
}