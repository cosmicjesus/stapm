<?php

namespace App\BusinessLogic\Student;

use App\BusinessLogic\BaseService;
use App\BusinessLogic\Enrollee\Enrollee;
use App\Models\Student as StudentModel;
use App\BusinessLogic\Student\Student as StudentClass;

class StudentService extends BaseService
{
    /**
     * @return string
     */
    function model()
    {
        return StudentModel::class;
    }

    public function orderByFullName()
    {
        $this->model = $this->model->orderBy('lastname', 'asc')->orderBy('firstname', 'asc');
        return $this;

    }

    public function onlyEnrollees()
    {
        $this->model = $this->model->where('status', 'enrollee');
        return $this;
    }

    public function onlyStudents()
    {
        $this->model = $this->model->whereIn('status', ['student', 'academic', 'inArmy']);
        return $this;
    }

    public function studentsWithAcademic()
    {
        $this->model = $this->model->where('status', 'student')->orWhere('status', 'academic')->orWhere('status', 'inArmy');
        return $this;
    }

    public function studentsWithOutAcademic()
    {
        $this->model = $this->model->where('status', 'student');
        return $this;
    }

    public function map($type = 'student')
    {
        $students = parent::all();

        if ($type == 'student') {
            return $students->map(function ($student) {
                return new StudentClass($student);
            });
        } else {
            return $students->filter(function ($enrollee) {
                return $enrollee->status == "enrollee";
            })->map(function ($student) {
                return new Enrollee($student);
            });
        }
    }


    public function filterByName($name)
    {
        $this->model = $this->model->where('lastname', 'LIKE', "%{$name}%")
            ->orWhere('firstname', 'LIKE', "%{$name}%")
            ->orWhere('middlename', 'LIKE', "%{$name}%");
        return $this;
    }

    public function onlyLongAbsent()
    {
        $this->model = $this->model->where('long_absent', true);
        return $this;
    }

    public function filterByGroup($group_id)
    {
        $this->model = $this->model->where('group_id', '=', $group_id);
        return $this;
    }

    public function filterByStatus($status)
    {
        $statusQuery = null;
        switch ($status) {
            case 'active':
                $statusQuery = $this->model->where('status', '=', 'student');
                break;
            case 'academic':
                $statusQuery = $this->model->where('status', '=', 'academic');
                break;
            case 'inArmy':
                $statusQuery = $this->model->where('status', '=', 'inArmy');
                break;
            case 'armyAndAcademic':
                $statusQuery = $this->model->whereIn('status', ['academic', 'inArmy']);
                break;
        }

        $this->model = $statusQuery;

        return $this;
    }

    public function filterByPrivilegy($privelegy_id)
    {
        $id = (int)$privelegy_id;

        $this->model = $this->model->where('privileged_category', $id);
    }

    public function orderByGroup()
    {
        $this->model = $this->model->orderBy('profession_program_id')->orderBy('group_id', 'DESC');
        return $this;
    }

    public function oneBySlug($slug)
    {
        $model = parent::getBySlug($slug);

        return new StudentClass($model);
    }

    public function getBySlug($slug)
    {
        $model = parent::getBySlug($slug);

        return new StudentClass($model);
    }
}