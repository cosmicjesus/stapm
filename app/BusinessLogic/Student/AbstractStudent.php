<?php

namespace App\BusinessLogic\Student;


use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use Carbon\Carbon;

abstract class AbstractStudent
{
    protected $student;
    protected $program;
    protected $parents;

    public function initialization($student)
    {
        $this->student = $student;
        $this->program = $student->program;
        $this->parents = $student->parents;
    }

    public function getId()
    {
        return $this->student->id;
    }

    public function getLastName()
    {
        return $this->student->lastname;
    }

    public function getFirstName()
    {
        return $this->student->firstname;
    }

    public function getMiddleName()
    {
        return $this->student->middlename;
    }

    public function getAge()
    {
        $age = Carbon::now()->diffInYears(Carbon::createFromFormat('d.m.Y', $this->getBirthday()));
        return getNumEnding($age, ['год', 'года', 'лет'], true);
    }

    public function getFullName()
    {
        return $this->student->lastname . " " . $this->student->firstname . " " . $this->student->middlename;
    }

    public function getGender()
    {
        return $this->student->gender;
    }

    public function getGenderToStr()
    {
        return getGender($this->student->gender);
    }

    public function getBirthday($format='d.m.Y')
    {
        return $this->student->birthday->format($format);
    }

    public function getAvg($isRound = false)
    {
        if (is_float($this->student->avg) && $isRound) {
            return round($this->student->avg, 2);
        }
        return $this->student->avg;
    }

    public function getProgram()
    {
        return new ProfessionProgram($this->program);
    }

    public function getEducation()
    {
        return $this->student->education->name;
    }

    public function getEducationId()
    {
        return $this->student->education->id;
    }

    public function getPassportData($indexName = null, $toAsuRso = false)
    {
        if (is_null($this->student->passport_data)) {
            return null;
        }

        $data = $this->student->passport_data;

        if (!is_null($indexName)) {
            if ($indexName == "documentType" && $toAsuRso) {
                return getDocumentType($data[$indexName]);
            }
            return isset($data[$indexName]) ? $data[$indexName] : '';
        }

        return $data;
    }

    public function getAddresses($key = null, $param = null)
    {
        if (is_null($this->student->addresses)) {
            return null;
        }

        $data = $this->student->addresses;

        if ($key) {
            if (isset($data[$key]) && is_null($param)) {
                return $data[$key];
            }
            if ($param) {
                if (isset($data[$key]) && isset($data[$key][$param])) {
                    return $data[$key][$param];
                } else {
                    return '';
                }
            }
        }

        return $data;

    }

    public function getGraduationOrganizationName()
    {
        return $this->student->graduation_organization_name;
    }

    public function getGraduationOrganizationPlace()
    {
        return $this->student->graduation_organization_place;
    }

    public function getPhone()
    {
        return $this->student->phone;
    }


    public function getHasOriginal()
    {
        return (bool)$this->student->has_original;
    }

    public function getNeedHostel()
    {
        return (bool)$this->student->need_hostel;
    }

    public function getMedCertificate()
    {
        return (bool)$this->student->has_certificate;
    }

    public function getHasOriginalStr()
    {
        return $this->student->has_original ? 'Да' : 'Нет';
    }

    public function getNeedHostelStr()
    {
        return $this->student->need_hostel ? 'Да' : 'Нет';
    }

    public function getMedCertificateStr()
    {
        return $this->student->has_certificate ? 'Да' : 'Нет';
    }

    public function getGraduationDate($format = 'd.m.Y')
    {
        return $this->student->graduation_date->format($format);
    }

    public function getStartDate()
    {
        return $this->student->start_date->format('d.m.Y');
    }

    public function isHasInn()
    {
        return is_null($this->student->inn) ? false : true;
    }

    public function getInn()
    {
        return $this->student->inn;
    }

    public function getSnils()
    {
        return $this->student->snils;
    }

    public function getLanguage($toAsuRso = false)
    {
        return getLanguage($this->student->language, $toAsuRso);
    }

    public function getDisability($toAsuRso = false)
    {
        return getDisability($this->student->disability, $toAsuRso);
    }

    public function getHealthCategory($toAsuRso = false)
    {
        return getHealthCategory($this->student->health_category, $toAsuRso);
    }

    public function getPrivelegeCategory($val = false, $toAsuRso = false)
    {

        if ($val) {
            return $this->student->privileged_category;
        }
        return getPrivilegeCategory($this->student->privileged_category, $toAsuRso);
    }

    public function getNationality($toAsuRso = false)
    {
        return getCountry($this->student->nationality, $toAsuRso);
    }

    public function buildAddress($key)
    {
        if (is_null($this->student->addresses)) {
            return '';
        }

        $addresses = json_decode($this->student->addresses, true);
        $addressArr = $addresses[$key];
        $address = '';

        $expRegion = explode(' ', $addressArr['region']);
        count($expRegion) > 1 ? $address .= $addressArr['region'] . ", " : $address .= $addressArr['region'] . " область, ";

        !is_null($addressArr['area']) ? $address .= $addressArr['area'] . " район, " : '';
        !is_null($addressArr['settlement']) ? $address .= $addressArr['settlement'] . ", " : '';
        !is_null($addressArr['index']) ? $address .= $addressArr['index'] . " " : '';
        !is_null($addressArr['city']) ? $address .= "г." . $addressArr['city'] . ", " : '';
        !is_null($addressArr['street']) ? $address .= "ул." . $addressArr['street'] . " " : '';
        !is_null($addressArr['house_number']) && !is_null($addressArr['apartment_number']) ? $address .= "д." . $addressArr['house_number'] . ", " : $address .= "д." . $addressArr['house_number'];
        !is_null($addressArr['apartment_number']) ? $address .= "кв." . $addressArr['apartment_number'] : '';

        return $address;
    }

    public function getEmail()
    {
        return '';
    }

    public function getParentsCount()
    {
        return $this->parents->count();
    }

    public function getParents()
    {
        return $this->parents->map(function ($parent) {
            return new StudentParent($parent);
        });
    }

    public function getUpdatePassportUrl()
    {
        return route('admin.enrollee.update-passport', ['id' => $this->student->id]);
    }

    public function getUpdateAddressesUrl()
    {
        return route('admin.enrollee.update-addresses', ['id' => $this->student->id]);
    }

    public function getAddParentUrl()
    {
        return route('admin.student.parent.add', ['id' => $this->student->id]);
    }
}