<?php

namespace App\BusinessLogic\TrainingGroup;

use App\BusinessLogic\EducationPlan\EducationPlan;
use App\BusinessLogic\Employee\Employee;
use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use App\BusinessLogic\Student\Student;
use App\Models\TrainingGroup as GroupModel;

class TrainingGroup
{
    protected $group;
    protected $program;
    protected $edicationPlan;
    protected $curator;
    protected $students;

    public function __construct($group)
    {
        $this->group = $group;

        $this->loadRelations();

        $this->program = $this->group->program;
        $this->edicationPlan = $this->group->education_plan;
        $this->curator = $this->group->curator;
        $this->students = $this->group->students;
    }

    public function loadRelations()
    {
        $relations = [
            'program',
            'education_plan',
            'curator',
            'students'
        ];

        foreach ($relations as $relation) {
            if (!$this->group->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->group->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->group->id;
    }

    public function getUniqueCode()
    {
        return $this->group->unique_code;
    }

    public function getPattert()
    {
        return $this->group->pattern;
    }

    public function getName()
    {
        return str_replace('{Курс}', $this->group->course, $this->group->pattern);
    }

    public function getMaxPeople()
    {
        return $this->group->max_people;
    }

    public function getCourse()
    {
        return $this->group->course;
    }

    public function getCountStudents()
    {
        return $this->students->count();
    }

    public function getCountVacancyTransfer()
    {
        $count = $this->getMaxPeople() - $this->getCountStudents();

        return $count < 0 ? 0 : $count;
    }

    public function getPeriod()
    {
        return $this->group->period;
    }

    public function getStep()
    {
        if ($this->group->term_type == 'Course') {
            return $this->group->course . " курс";

        }

        $pattern = '{course} курс {semestr} семестр';

        $semestr = $this->group->period % 2 == 0 ? 2 : 1;

        return massStrReplace($pattern, ['course' => $this->group->course, 'semestr' => $semestr]);

    }

    public function getProgram()
    {
        return new ProfessionProgram($this->program);
    }

    public function getEducationPlan()
    {
        return new EducationPlan($this->edicationPlan);
    }

    public function getCurator()
    {
        return new Employee($this->curator);
    }

    public function getStudents()
    {
        return $this->students->map(function ($student) {
            return new Student($student);
        });
    }

    public function getTotalSumStudentAvg()
    {
        $sum = 0;

        $students = $this->getStudents();

        foreach ($students as $student) {
            $sum += is_null($student->getAvg()) ? 0 : $student->getAvg();
        }

        return $sum;
    }

    public function getGroupAvg()
    {
        $avg = $this->getTotalSumStudentAvg() / $this->getCountStudents();
        return round($avg, 2);
    }

    public function getPrintReferencesUrl()
    {
        return route('admin.groups.print-references', ['id' => $this->group->id]);
    }

    public function getTransferUrl()
    {
        return route('admin.groups.transfer', ['id' => $this->group->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.groups.delete', ['id' => $this->group->id]);
    }
}