<?php

namespace App\BusinessLogic;

use App;
use Illuminate\Database\Eloquent\Model;

abstract class BaseService
{

    protected $model;

    /**
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->makeModel();
    }


    /**
     *
     * @return string Имя класса конкретной модели
     */
    abstract function model();

    public function paginate($count = 15)
    {
        return $this->model->paginate($count);
    }

    public function where($column, $znak = '=', $value)
    {
        $this->model = $this->model->where($column, $znak, $value);
        return $this;

    }

    public function orWhere($column, $znak = '=', $value)
    {
        $this->model = $this->model->orWhere($column, $znak, $value);
        return $this;

    }

    public function whereHas($relation, array $params)
    {
        $this->model = $this->model->whereHas($relation, function ($q) use ($params) {
            return $q->where($params[0], $params[1], $params[2]);
        });
    }


    public function with($relations)
    {
        $this->model = $this->model->with($relations);
        return $this;

    }

    public function all()
    {
        return $this->model->get();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function take($count)
    {
        $this->model = $this->model->take($count);
        return $this;
    }

    public function makeModel()
    {
        $model = App::make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function orderBy($column, $value = 'asc')
    {
        $this->model = $this->model->orderBy($column, $value);
        return $this;
    }

    public function getBySlug($slug)
    {
        return $this->model->findBySlugOrFail($slug);
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function clearModel()
    {
        return 0;
    }

}