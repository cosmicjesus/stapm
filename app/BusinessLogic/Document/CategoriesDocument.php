<?php

namespace App\BusinessLogic\Document;


class CategoriesDocument
{

    protected $category;
    protected $childs;
    protected $files;
    protected $parent;
    protected $countFiles;

    public function __construct(\App\Models\CategoriesDocument $category)
    {
        $this->category = $category;

        $this->childs = $this->category->childs->where('active', true)->map(function ($category) {
            return new CategoriesDocument($category);
        });


        $this->files = \App\Models\Document::query()
            ->where('active', true)
            ->whereJsonContains('category_ids', json_encode($this->category->id))
            ->orderBy('sort')
            ->get()
            ->map(function ($file) {
                return new Document($file);
            });

        $this->countFiles = $this->files->count();
        $this->parent = $this->category->parent;

    }

    protected function loadRelations()
    {
        $relations = [
            'files',
            'childs',
            'parent'
        ];

        foreach ($relations as $relation) {
            if (!$this->category->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    continue;
                }
                $this->category->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->category->id;
    }

    public function getName()
    {
        return $this->category->name;
    }

    public function getNameWithParent()
    {
        if (is_null($this->category->parent)) {
            return $this->category->name;
        }

        return $this->category->name . " | " . $this->getParent();
    }

    public function getSort()
    {
        return $this->category->sort;
    }

    public function getActive()
    {
        return $this->category->active;
    }

    public function getActiveStr()
    {
        return $this->category->active ? 'Да' : 'Нет';
    }

    public function getParent()
    {
        if (is_null($this->category->parent)) {
            return null;
        }

        return (new CategoriesDocument($this->parent))->getName();
    }

    public function getChilds()
    {
        return $this->childs;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getCountFiles()
    {
        return $this->countFiles;
    }

    public function isDelete()
    {
        return $this->getChilds();
    }

    public function getEditUrl()
    {
        return route('admin.documents.categories.edit.page', ['id' => $this->category->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.documents.categories.delete', ['id' => $this->category->id]);
    }
}