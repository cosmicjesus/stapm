<?php

namespace App\BusinessLogic\Document;


use App\Helpers\StorageFile;

class Document
{
    protected $document;
    protected $category;

    public function __construct($document)
    {
        $this->document = $document;

        $this->loadRelations();

        $this->category = $this->document->category;
    }

    protected function loadRelations()
    {
        $relations = [
            'category'
        ];

        foreach ($relations as $relation) {
            if (!$this->document->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    continue;
                }
                $this->document->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->document->id;
    }

    public function getName()
    {
        return $this->document->name;
    }

    public function getSort()
    {
        return $this->document->sort;
    }

    public function getActive()
    {
        return $this->document->active;
    }

    public function getActiveStr()
    {
        return $this->document->active ? 'Да' : 'Нет';
    }

    public function getPath()
    {
        if (is_null($this->document->path)) {
            return null;
        }

        return StorageFile::getDocument($this->document->path);
    }

    public function getCategory()
    {
        return new CategoriesDocument($this->category);
    }

    public function getCategories()
    {
        if (is_null($this->document->category_ids)) {
            return [];
        }
        $category_ids = $this->document->category_ids;
        return \App\Models\CategoriesDocument::query()->findMany($category_ids)
            ->map(function ($category) {
                return $category->name;
            });

    }

    public function getEditUrl()
    {
        return route('admin.documents.edit.page', ['id' => $this->document->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.documents.delete', ['id' => $this->document->id]);
    }
}