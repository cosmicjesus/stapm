<?php

namespace App\BusinessLogic\SiteMap;


use App;
use Carbon\Carbon;

class SiteMap
{
    protected $map;
    protected $date;

    public function __construct()
    {
        $this->map = App::make('sitemap');
        $this->date = Carbon::now()->toDayDateTimeString();
    }

    public function make()
    {
        $this->staticPages();
        $this->education();
        $this->employees();
        $this->news();
        $this->map->store('xml');
    }

    protected function staticPages()
    {
        $pages = [
            [
                'route' => route('client.index'),
                'priority' => '1.0',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.submission-form'),
                'priority' => '1.0',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.feedback'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.hot-line'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.basic-information'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.rooms'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.vospitanie'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.management-structure'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.financial-and-economic-activity'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.extramural-department.index'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.extramural-department.duties'),
                'priority' => '0.6',
                'freq' => 'monthly'
            ],
            [
                'route' => route('client.documents'),
                'priority' => '0.8',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.acceptance-transfer'),
                'priority' => '0.8',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.scholarship'),
                'priority' => '0.8',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.paid-educational-services'),
                'priority' => '0.8',
                'freq' => 'daily'
            ],
            [
                'route' => route('client.may9'),
                'priority' => '0.8',
                'freq' => 'daily'
            ]

        ];

        foreach ($pages as $page) {
            $this->map->add($page['route'], $this->date, $page['priority'], $page['freq']);
        }
    }

    protected function education()
    {
        $this->map->add(route('client.education'), $this->date, '0.9', 'daily');
        $this->map->add(route('client.education.standarts'), $this->date, '0.9', 'daily');

        $programs = App\Models\ProfessionProgram::query()
            ->where('on_site', true)
            ->orderBy('sort')
            ->get();

        foreach ($programs as $program) {
            $this->map->add(route('client.program_detail', ['slug' => $program->slug]), $this->date, '1.0', 'daily');
            $this->map->add(route('client.umk', ['slug' => $program->slug]), $this->date, '1.0', 'daily');
        }
    }

    protected function employees()
    {
        $this->map->add(route('client.employees'), $this->date, '0.9', 'daily');

        $employees = (new App\AppLogic\Services\EmployeeService())
            ->onlyWorks()
            ->onlyFullTime()
            ->onlyTeachers()
            ->get();

        foreach ($employees as $employee) {
            $this->map->add(route('client.employee_profile', ['slug' => $employee->getSlug()]), $this->date, '0.8', 'daily');
        }
    }

    protected function news()
    {
        $this->map->add(route('client.news'), $this->date, '0.8', 'daily');
        $news = (new App\AppLogic\Services\NewsService())->onlyActive()->get();
        foreach ($news as $item) {
            $this->map->add($item->getClientDetailUrl(), $this->date, '0.4', 'daily');
        }
    }

    protected function entrant()
    {
        $this->map->add(route('client.admission'), $this->date, '0.6', 'daily');
        $this->map->add(route('client.open-door'), $this->date, '0.6', 'daily');
        $this->map->add(route('client.ovz'), $this->date, '0.6', 'Weekly');
        $this->map->add(route('client.history'), $this->date, '0.6', 'Monthly');
        $this->map->add(route('client.priem'), $this->date, '0.8', 'daily');
        $this->map->add(route('client.partnership'), $this->date, '0.6', 'Weekly');
    }

    protected function student()
    {
        $this->map->add(route('client.code-of-honor'), $this->date, '0.6', 'Weekly');
        $this->map->add(route('client.memo-to-student'), $this->date, '0.6', 'Weekly');
        $this->map->add(route('client.anti-corruption'), $this->date, '0.6', 'Weekly');
        $this->map->add(route('client.order-references'), $this->date, '0.6', 'Monthly');
        $this->map->add(route('client.holidays'), $this->date, '0.6', 'Monthly');
        $this->map->add(route('client.assistance'), $this->date, '0.6', 'Monthly');
        $this->map->add(route('client.students.replaces'), $this->date, '1', 'daily');
    }

}
