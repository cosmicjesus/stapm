<?php

namespace App\BusinessLogic\Regulation;


class RegulationItem
{

    protected $item;
    protected $files;

    public function __construct($item)
    {
        $this->item = $item;
        $this->loadRelations();
        $this->files = $this->item->files;
    }

    protected function loadRelations()
    {
        $relations = [
            'files',
        ];

        foreach ($relations as $relation) {
            if (!$this->item->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->item->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->item->id;
    }

    public function getNameOfViolation()
    {
        return $this->item->name_of_violation;
    }

    public function getResult()
    {
        return $this->item->result;
    }

    public function getCountFiles()
    {
        return $this->files->count();
    }

    public function getFiles()
    {
        return $this->files->map(function ($file) {
            return new RegulationFile($file);
        });
    }
}