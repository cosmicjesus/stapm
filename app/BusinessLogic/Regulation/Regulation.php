<?php

namespace App\BusinessLogic\Regulation;


class Regulation
{
    protected $regulation;
    protected $items;
    protected $files;

    public function __construct(\App\Models\Regulation $regulation)
    {
        $this->regulation = $regulation;
        $this->loadRelations();
        $this->items = $this->regulation->items;
        $this->files = $this->regulation->files;
    }

    protected function loadRelations()
    {
        $relations = [
            'items',
            'files',
        ];

        foreach ($relations as $relation) {
            if (!$this->regulation->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->regulation->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->regulation->id;
    }

    public function getNameOfTheSupervisory()
    {
        return $this->regulation->name_of_the_supervisory;
    }

    public function getComment()
    {
        return $this->regulation->comment;
    }

    public function getDate()
    {
        return $this->regulation->date;
    }

    public function getActiveStatus()
    {
        return $this->regulation->active;
    }

    public function getActiveStatusStr()
    {
        return $this->regulation->active ? 'Да' : 'Нет';
    }

    public function getCountViolations()
    {
        return $this->items->count();
    }

    public function getViolations($forget = true)
    {
        $items = $this->items;

        if ($forget) {
            $items->forget(0);
        }

        return $items->map(function ($item) {
            return new RegulationItem($item);
        });
    }

    public function getFirstViolation()
    {
        return new RegulationItem($this->items->first());
    }

    public function getCountFiles()
    {
        return $this->files->count();
    }

    public function getFiles()
    {
        return $this->files->map(function ($file) {
            return new RegulationFile($file);
        });
    }

    public function getViolationsPageUrl()
    {
        return route('admin.regulations.violations', ['id' => $this->regulation->id]);
    }

    public function getDeleteUrl()
    {
        return 0;
    }

    public function getAddViolationUrl()
    {
        return route('admin.regulations.create-violations', ['id' => $this->regulation->id]);
    }

    public static function findById($id)
    {
        return new self(\App\Models\Regulation::query()->findOrFail($id));
    }
}