<?php

namespace App\BusinessLogic\Employee;

use App\BusinessLogic\QualificationCourse\QualificationCourse;
use App\Helpers\StorageFile;
use App\Models\Employee as EmployeeModel;
use Carbon\Carbon;

class Employee
{

    protected $employee;
    protected $positions;
    protected $educations;
    protected $genders;
    protected $categories;
    protected $courses;
    protected $administrator;
    protected $no_photo = '/img/nophoto.png';
    protected $subjects;

    public function __construct(EmployeeModel $employee)
    {
        $this->employee = $employee;

        $this->genders = genders();
        $this->categories = categories();

        $this->loadRelations();

        $this->positions = $this->employee->positions;


        $this->educations = $this->employee->educations->map(function ($education) {
            return new EmployeeEducation($education);
        });

        $this->courses = $this->employee->courses->map(function ($course) {
            return new QualificationCourse($course);
        });

        $this->administrator = $employee->administrator;

        $this->subjects = $employee->subjects->map(function ($subject) {
            return new EmployeeSubject($subject);
        });

    }

    protected function loadRelations()
    {
        $relations = [
            'positions',
            'courses',
            'administrator',
            'subjects'
        ];

        foreach ($relations as $relation) {
            if (!$this->employee->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->employee->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->employee->id;
    }

    public function getLastName()
    {
        return $this->employee->lastname;
    }

    public function getFirstName()
    {
        return $this->employee->firstname;
    }

    public function getMiddleName()
    {
        return $this->employee->middlename;
    }

    public function getFullName()
    {
        return $this->employee->lastname . " " . $this->employee->firstname . " " . $this->employee->middlename;
    }

    public function getGender()
    {
        return $this->genders[$this->employee->gender];
    }

    public function getBirthday()
    {
        return $this->employee->birthday->format('d.m.Y');
    }

    public function getAge()
    {
        $age = Carbon::now()->diffInYears(Carbon::createFromFormat('d.m.Y', $this->getBirthday()));
        return $age . " " . getNumEnding($age, ['год', 'года', 'лет']);
    }

    public function getSlug()
    {
        return $this->employee->slug;
    }

    public function getPositions($onlyFullTime = false)
    {

        $positions = $this->positions;
        if ($onlyFullTime) {
            $positions = $positions->where('pivot.type', 0);
        }
        return $positions->map(function ($position) {
            return new EmployeePosition($position);
        });
    }

    public function getEducations()
    {
        return $this->educations;
    }

    public function getPhoto()
    {
        if (is_null($this->employee->photo)) {
            return $this->no_photo;
        }
        $file = StorageFile::getEmployeePhoto($this->getId() . "/" . $this->employee->photo);
        return is_null($file) ? $this->no_photo : $file;
    }

    public function hasPhoto()
    {
        return $this->employee->photo;
    }

    public function getPhones()
    {
        return $this->employee->phones;
    }

    public function getEmails()
    {
        return $this->employee->emails;
    }

    public function getCategory()
    {
        $caregory = $this->categories[$this->employee->category];
        if ($this->getCategoryDate()) {
            $caregory .= " до " . $this->getCategoryDate();
        }
        return $caregory;
    }

    public function getCategoryDate()
    {
        if (is_null($this->employee->category_date)) {
            return null;
        }
        return $this->employee->category_date->format('d.m.Y');
    }

    public function getExpInCollege()
    {
        $diff = Carbon::createFromFormat('Y-m-d', $this->employee->start_work_date)->diff(Carbon::now());
        return calculateExp($diff);
    }

    public function getSpecialtyExp()
    {
        return getNumEnding($this->employee->specialty_exp, ['год', 'года', 'лет'], true);
    }

    public function getScientificDegree()
    {
        if (is_null($this->employee->scientific_degree)) {
            return 'нет';
        }

        return $this->employee->scientific_degree;
    }

    public function getAcademicTitle()
    {
        if (is_null($this->employee->academic_title)) {
            return 'нет';
        }

        return $this->employee->academic_title;
    }

    public function getProfileUrl()
    {
        return route('admin.employee.profile', ['slig' => $this->employee->slug]);
    }

    public function getUrl()
    {
        return route('employee_profile', ['slug' => $this->employee->slug]);
    }

    public function getCourses()
    {
        return $this->courses;
    }

    public function getAdministrator()
    {
        return new EmployeeAdministrator($this->administrator);
    }

    public function getGeneralExperience()
    {
        return getNumEnding($this->employee->general_experience, ['год', 'года', 'лет'], true);
    }

    public function getEditLink()
    {
        return route('admin.employee.edit', ['slug' => $this->employee->slug]);
    }

    public function getSubjects()
    {
        return $this->subjects;
    }

    public function getAddSubjectUrl()
    {
        return route('admin.employee.add-subject', ['id' => $this->employee->id]);
    }
}