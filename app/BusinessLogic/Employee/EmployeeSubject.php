<?php

namespace App\BusinessLogic\Employee;


class EmployeeSubject
{

    protected $subject;
    protected $pivot;

    public function __construct($subject)
    {
        $this->subject = $subject;
        $this->pivot = $subject->pivot;
    }

    public function getName()
    {
        return $this->subject->name;
    }

    public function getDeleteUrl()
    {
        return route('admin.employee.delete-subject', ['id' => $this->pivot->id]);
    }

}