<?php

namespace App\BusinessLogic\Employee;


use App\Models\AdministrationEmployee;

class EmployeeAdministrator
{
    protected $administrator;

    public function __construct(AdministrationEmployee $administrationEmployee)
    {
        $this->administrator=$administrationEmployee;
    }

    public function getID(){
        return $this->administrator->id;
    }

    public function getSort(){
        return $this->administrator->sort;
    }

    public function getEditUrl(){
        return route('admin.administrations.edit.page',['id'=>$this->administrator->id]);
    }

    public function getDeleteUrl(){
        return route('admin.administrations.delete',['id'=>$this->administrator->id]);
    }

}