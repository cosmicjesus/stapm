<?php

namespace App\BusinessLogic\Employee;


use App\BusinessLogic\BaseService;

class EmployeeService extends BaseService
{

    public function model()
    {
        return \App\Models\Employee::class;
    }

    public function orderByFullName()
    {
        $this->model = $this->model->orderBy('lastname', 'asc')->orderBy('firstname', 'asc');
        return $this;

    }

    public function onlyWork()
    {
        $this->model = $this->model->where('status', 'work');
        return $this;
    }

    public function onlyTeachers()
    {
        $this->model = $this->model->whereHas('positions', function ($q) {
            $q->where('position_type_id', 2);
        });
        return $this;
    }

    public function onlyFullTime()
    {
        $this->model = $this->model->whereHas('positions', function ($q) {
            $q->whereIn('type', [0, 1]);
        });
        return $this;
    }

    public function map()
    {
        $employees = $this->model->get();

        return $employees->map(function ($employee) {
            return new Employee($employee);
        });
    }


    public function oneBySlug($slug)
    {
        $model = parent::getBySlug($slug);

        return new Employee($model);
    }

    public function getBySlug($slug)
    {
        $model = parent::getBySlug($slug);

        return new Employee($model);
    }
}