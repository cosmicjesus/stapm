<?php


namespace App\BusinessLogic\Employee;


use App\Models\Education;

class EmployeeEducation
{
    protected $education;

    protected $pivot;

    public function __construct(Education $education)
    {

        $this->education = $education;

        $this->pivot = $education->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getName()
    {
        return $this->education->name;
    }

    public function getQualification()
    {
        return $this->pivot->qualification;
    }

    public function getDirectionOfPreparation()
    {
        return $this->pivot->direction_of_preparation;
    }

}