<?php

namespace App\BusinessLogic\Employee;

use App\Models\Position;
use Carbon\Carbon;

class EmployeePosition
{

    protected $position;

    protected $pivot;

    public function __construct(Position $position)
    {
        $this->position = $position;

        $this->pivot = $position->pivot;
    }

    public function getId(){
        return $this->pivot->id;
    }

    public function getName()
    {
        return $this->position->name;
    }

    public function getStartDate()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->pivot->start_date)->format('d.m.Y');
        return $date;
    }

    public function getLayoffDate()
    {
        if(is_null($this->pivot->date_layoff)){
            return null;
        }
        $date = Carbon::createFromFormat('Y-m-d', $this->pivot->date_layoff)->format('d.m.Y');
        return $date;
    }

    public function getExperience()
    {
        return $this->pivot->experience;
    }

    public function getPeriod()
    {
        $start_date = $this->getStartDate();

        if (is_null($this->pivot->date_layoff)) {
            return $start_date . " - Н.В";
        }

        $end_date = Carbon::createFromFormat('Y-m-d', $this->pivot->date_layoff)->format('d.m.Y');

        return $start_date . " - " . $end_date;
    }

    public function getFullExp()
    {
        if (is_null($this->pivot->start_date)) {
            return $this->getExperience();
        }
        $diff = Carbon::now()->diffInMonths(Carbon::createFromFormat('Y-m-d', $this->pivot->start_date));

        $exp = $diff + $this->getExperience();

        return $exp;
    }

    public function getExp()
    {

        $end = is_null($this->pivot->date_layoff) ? Carbon::now() : Carbon::createFromFormat('Y-m-d', $this->pivot->date_layoff);

        $interval = Carbon::createFromFormat('Y-m-d', $this->pivot->start_date)->diff($end);

        return calculateExp($interval);


//        $expInMonth=$this->getFullExp();
//
//        if($expInMonth<=0){
//            return $expInMonth." ".getNumEnding($expInMonth,['месяц','месяца','месяцев']);
//        }
//
//        $year=floor($expInMonth/12);
//        $months=$expInMonth%12;
//        $year_format=$year>0?$year." ".getNumEnding($year,['год','года','лет']):null;
//        $month_format=$months>0?$months." ".getNumEnding($months,['месяц','месяца','месяцев']):null;
//        $period=$year_format." ".$month_format;
//        return trim($period);
    }

    public function getType()
    {
        return getTypes($this->pivot->type);
    }
}