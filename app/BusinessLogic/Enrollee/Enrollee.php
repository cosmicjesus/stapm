<?php

namespace App\BusinessLogic\Enrollee;


use App\BusinessLogic\Student\AbstractStudent;

class Enrollee extends AbstractStudent
{
    public function __construct($enrollee)
    {
        $this->initialization($enrollee);
    }

    public function getProfileUrl()
    {
        return route('admin.enrollee.profile', ['id' => $this->student->id]);
    }


    public function getUpdateEducationUrl()
    {

        return route('admin.enrollee.update-education', ['id' => $this->student->id]);
    }

    public function getEditLink()
    {
        return route('admin.enrollee.edit', ['id' => $this->student->id]);
    }

    public function getContractTargetSet()
    {
        return $this->student->contract_target_set;
    }

    public function getContractTargetSetStr($showFinance = false)
    {
        if ($showFinance) {
            return $this->student->contract_target_set ? 'За счет бюджета субъекта РФ по договору о целевом обучении' : 'За счет бюджета субъекта РФ';
        } else {
            return $this->student->contract_target_set ? 'Да' : 'Нет';
        }
    }

    public function getMissingInformation()
    {

        $ul = "<ul style='padding-left: 15px;'>#ITEMS#</ul>";
        $items = [];

        if (!$this->student->has_original) {
            $items[] = 'Нет оригинала документа';
        }

        if (!$this->student->has_certificate) {
            $items[] = 'Нет медсправки';
        }

        if (is_null($this->student->passport_data)) {
            $items[] = 'Не заполнены данные паспорта';
        }

        if (is_null($this->student->addresses)) {
            $items[] = 'Не заполнены адреса';
        }

        if (is_null($this->student->graduation_organization_name)) {
            $items[] = 'Не указано название предыдущей обр. организации';
        }

        if (is_null($this->student->graduation_organization_place)) {
            $items[] = 'Не указан населенный пункт предыдущей обр. организации';
        }

        if (is_null($this->student->inn)) {
            $items[] = 'Не заполнен ИНН';
        }
        if (is_null($this->student->snils)) {
            $items[] = 'Не заполнен СНИЛС';
        }
        if (is_null($this->student->language)) {
            $items[] = 'Не указан изучаемый язык';
        }
        if ($this->getParentsCount() < 1) {
            $items[] = 'Не заполнены данные о родителях';
        }


        $arr = [
            'registration' => 'Не заполнен адрес регистрации',
            'residential' => 'Не заполнен адрес проживания',
            'place_of_stay' => 'Не заполнен адрес регистрации по месту прибывания',
        ];

        foreach ($arr as $key => $value) {
            $this->checkAddress($key) ? $items[] = $value : '';
        }

        if (!count($items)) {
            return 'Все заполнено';
        }

        $str = '';

        foreach ($items as $item) {
            $str .= "<li>" . $item . "</li>";
        }

        return str_replace('#ITEMS#', $str, $ul);
    }

    public function getErrorData()
    {

        $ul = "<ul style='padding-left: 15px;'>#ITEMS#</ul>";
        $items = [];

        if (!is_null($this->student->snils)) {
            if (iconv_strlen(str_replace(' ', '', $this->student->snils), 'UTF-8') < 11 || iconv_strlen(str_replace(' ', '', $this->student->snils), 'UTF-8') > 11) {
                $items[] = 'СНИЛС должен быть длинной 11 символов';
            }

        }

        if (!is_null($this->getPassportData())) {
            if (iconv_strlen($this->getPassportData('series'), 'UTF-8') > 8) {
                $items[] = 'Серия документа должна быть длинной не более 8 символов';
            }
            if (iconv_strlen($this->getPassportData('number'), 'UTF-8') > 16) {
                $items[] = 'Номер документа должен быть длинной не более 16 символов';
            }
            if (iconv_strlen($this->getPassportData('issued'), 'UTF-8') > 128) {
                $items[] = 'Поле "Кем выдан" должно быть длинной не более 128 символов';
            }
            if (iconv_strlen($this->getPassportData('birthPlace'), 'UTF-8') > 128) {
                $items[] = 'Место рождения должно быть длинной не более 128 символов';
            }
            if (iconv_strlen($this->getPassportData('subdivisionCode'), 'UTF-8') > 7) {
                $items[] = 'Код подразделения не должен быть длинной более 7 символов';
            }
        }

        if (!is_null($this->student->graduation_organization_name)) {
            if (iconv_strlen($this->student->graduation_organization_name, 'UTF-8') > 256) {
                $items[] = 'Наименование предыдущей ОО должно быть длинной не более 256 символов';
            }
        }
        if (!is_null($this->student->graduation_organization_place)) {
            if (iconv_strlen($this->student->graduation_organization_place, 'UTF-8') > 256) {
                $items[] = 'Населённый пункт предыдущей ОО должен быть быть длинной не более 256 символов';
            }
        }
        if (!is_null($this->buildAddress('registration'))) {
            if (iconv_strlen($this->buildAddress('registration'), 'UTF-8') > 500) {
                $items[] = 'Длинна строки с адресом регистрации не может быть больше 500 символов';
            }
        }
        if (!is_null($this->buildAddress('residential'))) {
            if (iconv_strlen($this->buildAddress('residential'), 'UTF-8') > 500) {
                $items[] = 'Длинна строки с адресом проживания не может быть больше 500 символов';
            }
        }
        if (!is_null($this->buildAddress('place_of_stay'))) {
            if (iconv_strlen($this->buildAddress('place_of_stay'), 'UTF-8') > 500) {
                $items[] = 'Длинна строки с адресом регистрации по месту прибывания не может быть больше 500 символов';
            }
        }

        if (!count($items)) {
            return 'Ошибок нет';
        }

        $str = '';

        foreach ($items as $item) {
            $str .= "<li>" . $item . "</li>";
        }

        return str_replace('#ITEMS#', $str, $ul);
    }

    protected function checkAddress($key)
    {
        if (!is_null($this->getAddresses($key))) {
            $i = 0;
            foreach ($this->getAddresses($key) as $ka => $value) {
                if (is_null($value)) {
                    $i++;
                }
            }
            if ($i == 9) {
                return true;
            }
        }
    }

    public function getUpdateAdditionalUrl()
    {
        return route('admin.enrollee.update-additional', ['id' => $this->student->id]);
    }

    public static function build($enrollee)
    {
        return new self($enrollee);
    }
}