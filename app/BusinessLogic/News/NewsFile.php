<?php

namespace App\BusinessLogic\News;


use App\Helpers\StorageFile;

class NewsFile
{

    protected $file;

    public function __construct(\App\Models\NewsFile $file)
    {
        $this->file = $file;
    }

    public function getId()
    {
        return $this->file->id;
    }

    public function getName()
    {
        return $this->file->name;
    }

    public function getPath()
    {
        return StorageFile::getNewsFile($this->file->path);
    }

    public function getReloadUrl()
    {
        return 0;
    }

    public function getDeleteUrl()
    {
        return route('admin.news-file.delete', ['id' => $this->getId()]);;
    }

    public function getEditUrl()
    {
        return route('admin.news-file.rename', ['id' => $this->getId()]);
    }

}