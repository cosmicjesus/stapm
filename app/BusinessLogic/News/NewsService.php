<?php

namespace App\BusinessLogic\News;


use App\BusinessLogic\BaseService;
use App\BusinessLogic\News\News;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class NewsService extends BaseService
{
    public function model()
    {
        return \App\Models\News::class;
    }

    public function map()
    {
        $news = parent::all();

        return $news->map(function ($new) {
            return new News($new);
        });
    }

    public function onlyVisible()
    {
        $this->model = $this->model->where('active', '=', 1)
            ->where(function ($query) {
                $query->where('visible_to', '>=', Carbon::now()->format('Y-m-d'))
                    ->orWhereNull('visible_to');
            });

        return $this;
    }

    public function onlyNews()
    {
        $this->model = $this->model->whereIn('type', [1, 3]);

        return $this;
    }

    public function onlyAnnouncements()
    {
        $this->model = $this->model->whereIn('type', [2, 3]);

        return $this;
    }

    public function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $paginator = new LengthAwarePaginator($this->map()->forPage($page, $perPage), $this->count(), $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return $paginator;
    }

    public function getBySlug($slug)
    {
        try {
            $model = parent::getBySlug($slug);

            return new \App\BusinessLogic\News\News($model);
        } catch (\Exception $exception) {
            abort(404);
        }

    }
}