<?php

namespace App\BusinessLogic\News;

use App\Helpers\StorageFile;
use App\Models\News as NewsModel;

class News
{
    protected $news;
    protected $files;
    protected $defaultImage = '/img/default-news-img.jpg';

    public function __construct(NewsModel $news)
    {
        $this->news = $news;

        $this->news->load('files');

        $this->files = $this->news->files->map(function ($file) {
            return new NewsFile($file);
        });

    }

    public function getId()
    {
        return $this->news->id;
    }

    public function getTitle()
    {
        return $this->news->title;
    }

    public function getSlug()
    {
        return $this->news->slug;
    }

    public function getPublicationDate()
    {
        return $this->news->publication_date->format('d.m.Y');
    }

    public function getVisibleToDate()
    {
        if (!is_null($this->news->visible_to)) {
            return $this->news->visible_to->format('d.m.Y');
        }

        return '';
    }

    public function getActiveStatus()
    {
        return $this->news->active;
    }

    public function getActiveStatusStr()
    {
        return $this->news->active ? 'Да' : 'Нет';
    }

    public function getPreview()
    {
        return $this->news->preview;
    }

    public function getImage()
    {
        if (is_null($this->news->image)) {
            return $this->defaultImage;
        }

        return StorageFile::getNewsImage($this->news->image);
    }

    public function getFullText()
    {
        return $this->news->full_text;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getNewsSection()
    {
        return $this->news->type;
    }

    public function getNewsTypeStr()
    {
        return getNewsSection($this->news->type);
    }

    public function getDetailUrl($route = 'news.one')
    {
        return route($route, ['slug' => $this->news->slug]);
    }

    public function getEditUrl()
    {
        return route('admin.news.edit', ['slug' => $this->news->slug]);
    }

    public function getDeleteUrl()
    {
        return route('admin.news.delete', ['id' => $this->news->id]);
    }

    public function getPrevious()
    {
        if (is_null($this->news->hasPrevious())) {
            return null;
        }
        return new self($this->news->hasPrevious());
    }

    public function getNext()
    {
        if (is_null($this->news->hasNext())) {
            return null;
        }
        return new self($this->news->hasNext());
    }
}