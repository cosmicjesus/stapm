<?php


namespace App\BusinessLogic\User;

use App\BusinessLogic\Employee\Employee;
use App\User as UserModel;

class User
{
    protected $user;
    protected $profile;

    public function __construct(UserModel $user)
    {
        $this->user=$user;

        $this->loadRelations();

        if($this->user->profile){
            $this->profile=new Employee($this->user->profile);
        }

    }

    protected function loadRelations()
    {
        $relations = [
            'profile'
        ];

        foreach ($relations as $relation) {
            if ( ! $this->user->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    continue;
                }
                $this->user->load($relation);
            }

        }
    }

    protected function notProfile(){
        return is_null($this->profile);
    }

    public function getName(){
        if($this->notProfile()){
            return $this->user->name;
        }
        return $this->profile->getFullName();
    }

    public function getPhoto(){
        if($this->notProfile()){
            return null;
        }

        if($this->profile->hasPhoto()){
            return $this->profile->getPhoto();
        }

        return null;
    }
}