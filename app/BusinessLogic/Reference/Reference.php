<?php

namespace App\BusinessLogic\Reference;


use App\BusinessLogic\Student\Student;

class Reference
{

    protected $reference;
    protected $student;

    public function __construct($reference)
    {
        $this->reference = $reference;

        $this->reference->load('student');
        $this->student = $this->reference->student;
    }

    public function getId()
    {
        return $this->reference->id;
    }

    public function getType()
    {
        return $this->reference->type == 'training' ? 'Справка об обучении' : 'Справка о стипендии';
    }

    public function getStatus()
    {
        switch ($this->reference->status) {
            case 'issued':
                return 'Выдана';
                break;
            case 'booked':
                return 'Заказана с сайта';
                break;
            default:
                return '';
                break;
        }
    }

    public function getStatusStr()
    {
        return $this->reference->status;
    }

    public function getDates()
    {
        $tpl = "<ul style='padding-left: 10px'>#CONTENT#</ul>";
        $content = "";

        if (!is_null($this->reference->date_of_application)) {
            $content .= "<li>Дата заказа: " . $this->reference->date_of_application->format("H:i:s d.m.Y") . "</li>";
        }

        if (!is_null($this->reference->date_of_issue)) {
            $content .= "<li>Дата выдачи: " . $this->reference->date_of_issue->format("H:i:s d.m.Y") . "</li>";
        }


        return str_replace('#CONTENT#', $content, $tpl);
    }

    public function getStudent()
    {
        return $this->student->lastname . " " . $this->student->firstname . " " . $this->student->middlename;
    }

    public function getStudentGroup()
    {
        return (new Student($this->student))->getGroup()->getName();
    }

    public function getParam($param)
    {
        $params = $this->reference->params;

        return isset($params[$param]) ? $params[$param] : '';
    }

    public function getParams($template = false)
    {
//        if ($template) {
//            return $this->reference->params;
//        }
//
//        $params = $this->reference->params;
//
//        $tpl = "<ul style='padding-left: 10px'>#CONTENT#</ul>";
//        $content = "";
//        foreach ($params as $index => $param) {
//            if ($index == 'period') {
//                $param = getReferencePeriod($param);
//            }
//            $content .= "<li>" . getParamKey($index) . " : " . $param . "</li>";
//        }
//
//        return str_replace('#CONTENT#', $content, $tpl);
    }
}