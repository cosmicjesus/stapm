<?php

namespace App\BusinessLogic\Subject;


class Subject
{
    protected $subject;

    public function __construct($subject)
    {
        $this->subject = $subject;
        $this->subject->load('type');
    }

    public function getId()
    {
        return $this->subject->id;
    }

    public function getName()
    {
        return $this->subject->name;
    }

    public function getType()
    {
        return $this->subject->type->name;
    }

    public function getEditUrl()
    {
        return route('admin.subjects.edit', ['id' => $this->subject->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.subjects.delete', ['id' => $this->subject->id]);
    }
}