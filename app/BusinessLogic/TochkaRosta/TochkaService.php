<?php

namespace App\BusinessLogic\TochkaRosta;


use App\BusinessLogic\BaseService;
use App\BusinessLogic\News\News;
use App\BusinessLogic\TochkaRosta\TochkaRosta;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class TochkaService extends BaseService
{
    public function model()
    {
        return \App\Models\TochkaRostaItem::class;
    }

    public function map()
    {
        $news = parent::all();

        return $news->map(function ($new) {
            return new TochkaRosta($new);
        });
    }

    public function onlyVisible()
    {
        $this->model = $this->model->where('active', '=', 1);

        return $this;
    }

    public function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $paginator = new LengthAwarePaginator($this->map()->forPage($page, $perPage), $this->count(), $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return $paginator;
    }

    public function getBySlug($slug)
    {
        try {
            $model = parent::getBySlug($slug);

            return new \App\BusinessLogic\TochkaRosta\TochkaRosta($model);
        } catch (\Exception $exception) {
            abort(404);
        }

    }
}