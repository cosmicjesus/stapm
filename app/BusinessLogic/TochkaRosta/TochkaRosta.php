<?php

namespace App\BusinessLogic\TochkaRosta;


use App\Helpers\StorageFile;
use App\Models\TochkaRostaItem;

class TochkaRosta
{

    protected $item;
    protected $defaultImage = '/img/default-news-img.jpg';
    protected $files;

    public function __construct(TochkaRostaItem $item)
    {
        $this->item = $item;

        $this->item->load('files');

        $this->files = $this->item->files->map(function ($file) {
            return new TochkaRostaImage($file);
        });
    }

    public function getId()
    {
        return $this->item->id;
    }

    public function getTitle()
    {
        return $this->item->title;
    }

    public function getSlug()
    {
        return $this->item->slug;
    }

    public function getPublicationDate()
    {
        return $this->item->publication_date->format('d.m.Y');
    }

    public function getPreview()
    {
        return $this->item->preview;
    }

    public function getImage()
    {
        if (is_null($this->item->image)) {
            return $this->defaultImage;
        }

        return StorageFile::getTochkaRostaImage($this->item->image);
    }

    public function getFullText()
    {
        return $this->item->full_text;
    }

    public function getActive()
    {
        return $this->item->active;
    }

    public function getActiveStr()
    {
        return $this->item->active ? 'Да' : 'Нет';
    }

    public function getDeleteUrl()
    {
        return 0;
    }

    public function getDetailUrl()
    {
        return route('tochka.one', ['slug' => $this->item->slug]);
    }

    public function getPhotosListUrl()
    {
        return route('admin.tochka-rosta.photos', ['id' => $this->item->id]);
    }

    public function getUploadPhotoUrl()
    {
        return route('admin.tochka-rosta.upload-photo', ['id' => $this->item->id]);
    }

    public function getPhotos()
    {
        return $this->files;
    }

}