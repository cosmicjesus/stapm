<?php

namespace App\BusinessLogic\TochkaRosta;


use App\Helpers\StorageFile;
use App\Models\TochkaRostaPhoto;

class TochkaRostaImage
{

    protected $image;

    public function __construct(TochkaRostaPhoto $photo)
    {
        $this->image = $photo;
    }

    public function getId()
    {
        return $this->image->id;
    }

    public function getPath()
    {
        return StorageFile::getTochkaRostaPhoto($this->image->path);
    }

    public function getDeleteUrl()
    {
        return route('admin.tochka-rosta.delete-photo', ['photo_id' => $this->getId()]);
    }

}