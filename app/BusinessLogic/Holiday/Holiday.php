<?php

namespace App\BusinessLogic\Holiday;


class Holiday
{
    protected $holiday;

    public function __construct($holiday)
    {
        $this->holiday = $holiday;
    }


    public function getId()
    {
        return $this->holiday->id;
    }

    public function getDescription()
    {
        return $this->holiday->description;
    }

    public function getStartDate()
    {
        return $this->holiday->start_date->format('d.m.Y');
    }

    public function getEndDate()
    {
        if (is_null($this->holiday->end_date)) {
            return null;
        }
        return $this->holiday->end_date->format('d.m.Y');
    }

    public function getPeriod()
    {
        if (is_null($this->holiday->end_date)) {
            return $this->getStartDate();
        }

        $holiday = $this->holiday;

        return $holiday->start_date->format('d.m.Y') . " - " . $holiday->end_date->format('d.m.Y');
    }

    public function getEditUrl()
    {
        return route('admin.holidays.update', ['id' => $this->holiday->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.holidays.destroy', ['id' => $this->holiday->id]);
    }
}