<?php

namespace App\BusinessLogic\Holiday;


use Carbon\Carbon;

class HolidayService
{
    private $model;

    public function __construct()
    {
        $this->model = \App\Models\Holiday::query();
    }

    public function all()
    {
        return $this->model->get()->map(function ($holiday) {
            return new Holiday($holiday);
        });
    }

    public function orderByStartDate($direction = 'asc')
    {
        $this->model = $this->model->orderBy('start_date', $direction);

        return $this;
    }

    public function create($attributes)
    {
        try {
            $this->model->create([
                'description' => $attributes['description'],
                'start_date' => Carbon::createFromFormat('d.m.Y', $attributes['start_date'])->format('Y-m-d'),
                'end_date' => is_null($attributes['end_date']) ? null : Carbon::createFromFormat('d.m.Y', $attributes['end_date'])->format('Y-m-d')
            ]);
        } catch (\Exception $exception) {

        }
    }

    public function destroy($id)
    {
        try {
            $this->model->findOrFail($id)->delete();
        } catch (\Exception $exception) {

        }
    }
}