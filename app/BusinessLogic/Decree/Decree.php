<?php

namespace App\BusinessLogic\Decree;

use App\Models\Decree as DecreeModel;

class Decree
{
    protected $decree;

    public function __construct(DecreeModel $decree)
    {
        $this->decree=$decree;
    }

    public function getId(){
        return $this->decree->id;
    }

    public function getNumber(){
        return $this->decree->number;
    }

    public function getDate(){
        return $this->decree->date->format('d.m.Y');
    }
}