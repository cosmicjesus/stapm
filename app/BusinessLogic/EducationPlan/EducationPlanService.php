<?php

namespace App\BusinessLogic\EducationPlan;

use App\BusinessLogic\BaseService;
use App\Models\EducationPlan as EduPlanModel;

class EducationPlanService extends BaseService
{
    public function model()
    {
        return EduPlanModel::class;
    }

    public function map(){
        $plans=parent::all();

        return $plans->map(function ($plan){
            return new EducationPlan($plan);
        });
    }

    public function getById($id){
        $plan=parent::getById($id);

        return new EducationPlan($plan);
    }

}