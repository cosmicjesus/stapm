<?php

namespace App\BusinessLogic\EducationPlan;


use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use App\Helpers\StorageFile;
use App\Models\EducationPlan as Model;
use Carbon\Carbon;

class EducationPlan
{
    protected $plan;
    protected $program;

    public function __construct(Model $plan)
    {
        $this->plan = $plan;
        $this->loadRelations();
        $this->program = new ProfessionProgram($plan->professionProgram, false);
    }

    protected function loadRelations()
    {
        $relations = [
            'professionProgram'
        ];

        foreach ($relations as $relation) {
            if (!$this->plan->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->plan->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->plan->id;
    }

    public function getName()
    {
        return $this->plan->name;
    }

    public function getNameOnDate()
    {
        $name = $this->plan->start_training->format('Y');
        if ($this->getExtraOption('dual_training')) {
            $name .= " [ДО]";
        }

        if ($this->getExtraOption('adapted_program')) {
            $name .= " [АОП]";
        }


        return $name;
    }

    public function getProgramId()
    {
        return $this->program->getId();
    }

    public function getProgram()
    {
        return $this->program->getNameWithForm();
    }

    public function getStartTrainig()
    {
        return $this->plan->start_training->format('d.m.Y');
    }

    public function getEndTrainig()
    {
        return $this->plan->end_training->format('d.m.Y');
    }

    public function getTrainigPeriod()
    {
        $start = $this->getStartTrainig();
        $end = $this->getEndTrainig();

        return $start . " - " . $end;
    }

    public function getCountPeriod()
    {
        return $this->plan->count_period;
    }

    public function getActiveStatus()
    {
        return $this->plan->active ? true : false;
    }

    public function getRevisionStatus()
    {
        return $this->plan->in_revision ? true : false;
    }

    public function getStatus()
    {

        if ($this->getRevisionStatus()) {
            return 'revision';
        }

        if ($this->getFilePath()) {
            return 'has_file';
        } else {
            return 'no_file';
        }

        return 'no_file';
    }

    public function getExtraOptions()
    {
        return $this->plan->extra_options;
    }

    public function getExtraOption($key)
    {
        $options = $this->getExtraOptions();
        if (is_null($options)) {
            return null;
        }

        return isset($options[$key]) && ($options[$key] == true) ? $options[$key] : null;
    }

    public function getFilePath()
    {
        if (is_null($this->plan->full_path)) {
            return null;
        }
        return StorageFile::getEducationPlan($this->plan->full_path);
    }

    public function getProgramModel()
    {
        return $this->program;
    }
}