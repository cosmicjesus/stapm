<?php

namespace App\BusinessLogic\SocialPartner;


use App\Helpers\StorageFile;

class SocialPartnerFile
{
    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function getId()
    {
        return $this->file->id;
    }

    public function getName()
    {
        return $this->file->name;
    }

    public function getPath()
    {
        return StorageFile::getPartnerFile($this->file->path);
    }
}