<?php

namespace App\BusinessLogic\SocialPartner;


use App\Helpers\StorageFile;

class SocialPartner
{
    protected $partner;
    protected $files;

    public function __construct(\App\Models\SocialPartner $partner)
    {
        $this->partner = $partner;
        $this->partner->relationLoaded('files');
        $this->files = $this->partner->files;
    }

    public function getId()
    {
        return $this->partner->id;
    }

    public function getName()
    {
        return $this->partner->name;
    }

    public function getDescription()
    {
        return $this->partner->description;
    }

    public function getSort()
    {
        return $this->partner->sort;
    }

    public function getStartDate()
    {
        if (is_null($this->partner->start_date)) {
            return null;
        }

        return $this->partner->start_date->format('d.m.Y');
    }

    public function getEndDate()
    {
        if (is_null($this->partner->start_date)) {
            return null;
        }

        return $this->partner->end_date->format('d.m.Y');
    }

    public function getContractType()
    {
        return $this->partner->contract_type;
    }

    public function getImage()
    {
        return StorageFile::getPartnerFile($this->partner->image_path);
    }

    public function getSiteUrl()
    {
        return $this->partner->site_url;
    }

    public function getFiles()
    {
        return $this->files->map(function ($file) {
            return new SocialPartnerFile($file);
        });
    }

    public function getActive()
    {
        return $this->partner->active;
    }

    public function getActiveStr()
    {
        return $this->partner->active ? 'Да' : 'Нет';
    }

    public function getEditUrl()
    {
        return 0;
    }

    public function getDeleteUrl()
    {
        return route('admin.social-partners.delete', ['id' => $this->partner->id]);
    }
}