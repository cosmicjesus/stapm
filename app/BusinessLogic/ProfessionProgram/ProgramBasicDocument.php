<?php
namespace App\BusinessLogic\ProfessionProgram;


use App\Helpers\StorageFile;

class ProgramBasicDocument
{
    protected $documnets;

    public function __construct(\App\Models\ProgramBasicDocument $documents)
    {
        $this->documnets=$documents;
    }

    public function getId(){
        return $this->documnets->id;
    }

    public function getStandart(){
        if(is_null($this->documnets->standart)){
            return null;
        }
        return StorageFile::getProgramDocument($this->documnets->standart);
    }

    public function getAnnotation(){
        if(is_null($this->documnets->annotation)){
            return null;
        }
        return StorageFile::getProgramDocument($this->documnets->annotation);
    }
}