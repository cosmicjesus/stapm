<?php

namespace App\BusinessLogic\ProfessionProgram;


class PriemProgram
{
    protected $priemProgram;

    public function __construct($priemProgram)
    {
        $this->priemProgram = $priemProgram;
    }

    public function getId()
    {
        return $this->priemProgram->id;
    }

    public function getReceptionPlan()
    {
        return $this->priemProgram->reception_plan;
    }

    public function getStaticCountWithDocuments()
    {
        return $this->priemProgram->count_with_documents;
    }

    public function getStaticCountWithOutDocuments()
    {
        return $this->priemProgram->count_with_out_documents;
    }

    public function getParams()
    {
        $params = [
            'reception' => $this->getReceptionPlan(),
            'count_with_documents' => $this->getStaticCountWithDocuments(),
            'count_with_out_documents' => $this->getStaticCountWithOutDocuments(),
            'edit_url' => $this->getEditUrl()
        ];
        return json_encode($params);
    }

    public function getEditUrl()
    {
        return route('admin.priem.update', ['id' => $this->priemProgram->id]);
    }

    public function getDeleteUrl()
    {
        return route('admin.priem.delete', ['id' => $this->priemProgram->id]);
    }
}