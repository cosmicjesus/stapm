<?php

namespace App\BusinessLogic\ProfessionProgram;

use App\BusinessLogic\BaseService;
use App\Models\ProfessionProgram as ProgramModel;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class ProfessionProgramService extends BaseService {

    public function model(){
        return \App\Models\ProfessionProgram::class;
    }

    public function paginate($perPage = 15, $columns = [ '*' ], $pageName = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $paginator = new LengthAwarePaginator($this->all()->forPage($page,$perPage), $this->count(), $perPage, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return $paginator;
    }

    public function getBySlug($slug)
    {
         $program=parent::getBySlug($slug);

         return new ProfessionProgram($program);
    }

    public function test(){
        return $this->model->get();
    }

    public function convert($programs)
    {

        $collect=collect([]);
        foreach ($programs->sortBy('sort') as $program){
            $collect->push(new ProfessionProgram($program));
        }

        return $collect;
    }

    public function map(){
        $programs=parent::all();

        return $programs->map(function ($program){
            return new ProfessionProgram($program);
        });
    }


}