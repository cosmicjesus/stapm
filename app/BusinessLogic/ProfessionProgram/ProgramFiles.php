<?php

namespace App\BusinessLogic\ProfessionProgram;


use App\Helpers\StorageFile;
use App\Models\ProgramDocument;
use Carbon\Carbon;

class ProgramFiles
{
    protected $document;

    public function __construct(ProgramDocument $document)
    {
        $this->document = $document;
    }

    public function getId()
    {
        return $this->document->id;
    }

    public function getTitle()
    {
        if (is_null($this->document->start_date)) {
            return $this->document->title;
        }

        $name = $this->document->start_date->format('Y');
        $typesArr = ['ppssz', 'akt', 'calendar'];
        $type = $this->document->type;
        if (in_array($type, $typesArr)) {
            switch ($this->document->start_date->day) {
                case 2:
                    return $name . " [ДО]";
                    break;
                case 3:
                    return $name . " [АОП]";
                    break;
                default:
                    return $name;
                    break;
            }
        }

        return $name;
    }

    public function getFileUrl()
    {
        return StorageFile::getProgramDocument($this->document->path);
    }

    public function getOtherName()
    {
        return $this->document->title . " " . $this->document->start_date->format('Y');
    }

    public function getReloadUrl()
    {
        return route('admin.program.reload-document', ['id' => $this->document->id]);
    }

    public function getReloadOtherUrl()
    {
        return route('admin.program.reload-document', ['id' => $this->document->id, 'other' => 'other']);
    }

    public function getDeleteUrl()
    {
        return route('admin.program.delete-document', ['id' => $this->document->id]);
    }
}