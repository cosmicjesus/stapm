<?php

namespace App\BusinessLogic\ProfessionProgram;

use App\BusinessLogic\EducationPlan\EducationPlan;
use App\BusinessLogic\TrainingGroup\TrainingGroup;
use App\Helpers\Settings;
use App\Helpers\StorageFile;
use App\Models\ProfessionProgram as Model;
use Carbon\Carbon;

class ProfessionProgram
{

    protected $program;
    protected $subjects;
    protected $educationPlans;
    protected $priem;
    protected $calendars;
    protected $akts;
    protected $ppssz;
    protected $enrollees;
    protected $students;
    protected $groups;
    protected $metodicals;
    protected $enrolleeDate;

    public function __construct($program, $loadRelation = true, $enroleeDate = null)
    {

        $this->program = $program;
        $this->enrolleeDate = is_null($enroleeDate) ? null : Carbon::createFromFormat('d.m.Y', $enroleeDate)->format('Y-m-d');
        if ($loadRelation) {
            $this->loadRelations();

            $this->subjects = $this->program->subjects;

            $this->educationPlans = $this->program->plans;


            $this->priem = $program->priem;

            $this->calendars = $program->calendars;
            $this->akts = $program->akts;
            $this->ppssz = $program->ppssz;
            $this->enrollees = $program->enrollees;
            $this->students = $program->students;
            $this->metodicals = $program->metodicals;
            $this->groups = $this->program->groups;
        }

    }

    protected function loadRelations()
    {
        $relations = [
            'profession',
            'education_form',
            'type',
            'subjects',
            'plans',
            'priem',
            'documents',
            'metodicals',
            'calendars',
            'akts',
            'ppssz',
            'enrollees',
            'students',
            'groups'
        ];

        foreach ($relations as $relation) {
            if (!$this->program->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->program->load($relation);
            }

        }
    }

    public function getCode()
    {
        return $this->program->profession->code;
    }

    public function getProfessionName()
    {
        return $this->program->profession->name;
    }

    public function getName()
    {
        return $this->program->profession->code . " " . $this->program->profession->name;
    }

    public function getNameWithForm()
    {
        return $this->getName() . " | " . $this->getEducationForm();
    }

    public function getLicence()
    {
        if (is_null($this->program->licence)) {
            return null;
        }

        return $this->program->licence->format('d.m.Y');
    }

    public function getProfessionId()
    {
        return $this->program->profession->id;
    }


    public function getDetailUrl()
    {
        return route('program_detail', ['slug' => $this->program->slug]);
    }

    public function getUmkUrl()
    {
        return route('umk', ['slug' => $this->program->slug]);
    }

    public function getSlug()
    {
        return $this->program->slug;
    }

    public function getPeriod()
    {
        $year = floor($this->program->trainin_period / 12);
        $months = $this->program->trainin_period % 12;
        $year_format = $year > 0 ? $year . " " . getNumEnding($year, ['год', 'года', 'лет']) : '';
        $month_format = $months > 0 ? $months . " " . getNumEnding($months, ['месяц', 'месяца', 'месяцев']) : '';
        $period = $year_format . " " . $month_format;
        return trim($period);
    }

    public function getPeriodToInt()
    {
        return $this->program->trainin_period;
    }

    public function getEducationFormId()
    {
        return $this->program->education_form->id;
    }

    public function getEducationForm()
    {
        return $this->program->education_form->name;
    }

    public function getSubjects()
    {
        return $this->subjects->map(function ($subject) {
            return new SubjectInProgram($subject);
        });
    }

    public function getSort()
    {
        return $this->program->sort;
    }

    public function getId()
    {
        return $this->program->id;
    }

    public function getDescription()
    {
        return $this->program->description;
    }

    public function onSite()
    {
        return $this->program->on_site;
    }

    public function getQualification()
    {
        return $this->program->qualification;
    }

    public function getType()
    {
        return $this->program->type->name;
    }

    public function getTypeId()
    {
        return $this->program->type->id;
    }

    public function getMetodicalUrl()
    {
        return route('admin.program.metodicals', ['id' => $this->program->id]);
    }

    public function getCreateMetodicalUrl()
    {
        return route('admin.program.create-metodical', ['id' => $this->program->id]);
    }

    public function getMetodicals()
    {
        return $this->metodicals->map(function ($metodical) {
            return new ProgramFiles($metodical);
        });
    }

    public function getEducationPlans()
    {
        return $this->educationPlans->map(function ($plan) {
            return new EducationPlan($plan);
        });
    }

    public function getCalendars()
    {
        return $this->calendars->map(function ($calendar) {
            return new ProgramFiles($calendar);
        });
    }

    public function getAkts()
    {
        return $this->akts->map(function ($akt) {
            return new ProgramFiles($akt);
        });
    }

    public function getPpssz()
    {
        return $this->ppssz->map(function ($ppssz) {
            return new ProgramFiles($ppssz);
        });
    }

    public function getPriem()
    {
        return new PriemProgram($this->priem);
    }

    public function getCountEnrolleesWithDocuments($useStatic = false)
    {
        if (Settings::ShowStaticCountEnrollees() && $useStatic) {
            return $this->getPriem()->getStaticCountWithDocuments();
        }

        if ($this->enrolleeDate) {
            $query = $this->program->enrollees($this->enrolleeDate)->where('has_original', true)->count();
            return $query;
        } else {
            $query = $this->enrollees->where('has_original', true)->count();
            return $query;
        }
    }

    public function getCountEnrolleesWithOutDocuments($useStatic = false)
    {

        if (Settings::ShowStaticCountEnrollees() && $useStatic) {
            return $this->getPriem()->getStaticCountWithOutDocuments();
        }

        if (!is_null($this->enrolleeDate)) {
            $query = $this->program->enrollees($this->enrolleeDate)->where('has_original', false)->count();
            return $query;
        } else {
            $query = $this->enrollees->where('has_original', false)->count();
            return $query;
        }
    }

    public function getCountStudents()
    {
        return $this->students->count();
    }

    public function getActiveGroups()
    {
        $groups = $this->groups->filter(function ($value) {
            return $value->status == 'teach';
        })->map(function ($group) {
            return new TrainingGroup($group);
        });

        return $groups;
    }

    public function getPresentationPath()
    {
        if (is_null($this->program->presentation_path)) {
            return null;
        }
        return "<a href='" . StorageFile::getProgramDocument($this->program->presentation_path) . "' target='_blank'>Открыть</a>";
    }

    public function getVideoPresentationCode()
    {
        if (is_null($this->program->video_presentation_path)) {
            return null;
        }

        return $this->program->video_presentation_path;
    }

    public function getVideoPresentationUrl()
    {
        if (is_null($this->program->video_presentation_path)) {
            return null;
        }

        return 'https://www.youtube.com/watch?v=' . $this->program->video_presentation_path;
    }

    public function getEditUrl()
    {
        return route('admin.programs.edit', ['slug' => $this->program->slug]);
    }

    public function getUpdateUrl()
    {
        return route('admin.programs.update', ['slug' => $this->program->slug]);
    }

    public function getDetailUrlAdmin()
    {
        return route('admin.programs.detail', ['slug' => $this->program->slug]);
    }

    public function getDocuments()
    {
        return new ProgramBasicDocument($this->program->documents);
    }

    public function getAddSubjectInProgramUrl()
    {
        return route('admin.program.add-subject', ['id' => $this->program->id]);
    }

    public function getAddOtherDocumentUrl()
    {
        return route('admin.program.add-other', ['id' => $this->program->id]);
    }

    public function getRaitingUrl()
    {
        return route('priem.raiting', ['slug' => $this->program->slug]);
    }


    public static function buildBySlug($slug)
    {
        $model = \App\Models\ProfessionProgram::findBySlugOrFail($slug);
        return new self($model);
    }
}