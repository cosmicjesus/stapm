<?php
namespace App\BusinessLogic\ProfessionProgram;

use App\Helpers\StorageFile;
use App\Models\ProgramDocument;
use App\Models\ProgramSubject;
use App\Models\Subject;

class SubjectInProgram{

    protected $subject;
    protected $pivot;
    protected $files;

    public function __construct(Subject $subject)
    {
        $this->subject=$subject;
        $this->pivot=$subject->pivot;
    }

//    protected function loadRelations()
//    {
//        $relations = [
//            'files'
//        ];
//
//        foreach ($relations as $relation) {
//            if (!$this->program->relationLoaded($relation)) {
//                if (substr_count($relation, '.') > 0) {
//                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
//                    continue;
//                }
//                $this->program->load($relation);
//            }
//
//        }
//    }

    public function getProfessionProgramId(){
        return $this->pivot->profession_program_id;
    }

    public function getFileName(){
        return $this->pivot->file;
    }
    public function getModel()
    {
        return $this->subject;
    }

    public function getName(){
        return $this->subject->name;
    }

    public function getFilePath(){
        if(is_null($this->getFileName())){
            return null;
        }
        $path=$this->getProfessionProgramId()."/".$this->getFileName();
        return StorageFile::programFile($path);
    }

    public function getStatus(){
        return (bool)$this->pivot->in_revision;
    }

    public function getId(){
        return $this->pivot->id;
    }

    public function getFiles(){
        $files=ProgramSubject::with('files')->find($this->pivot->id);
        if(is_null($files)){
            return null;
        }
        return $files->files->map(function ($file){
            return new ProgramFiles($file);
        });
    }

    public function getAddDocumentUrl(){
        return route('admin.program.add-subject-document',['id'=>$this->pivot->id]);
    }

    public function getDeleteUrl(){
        return route('admin.program.delete-subject',['id'=>$this->pivot->id]);
    }

}