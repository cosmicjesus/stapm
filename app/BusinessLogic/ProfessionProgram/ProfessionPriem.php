<?php

namespace App\BusinessLogic\ProfessionProgram;

use App\Helpers\Settings;
use App\Models\ProfessionProgram as ProgramModel;

class ProfessionPriem
{

    protected $programs;
    protected $countWithDocuments;
    protected $countWithOutDocuments;
    protected $count;
    protected $date;

    public function __construct($date = null)
    {
        $this->date = $date;

        $useStaticValues = Settings::ShowStaticCountEnrollees();

        $this->programs = ProgramModel::query()
            ->whereHas('priem')
            ->get()
            ->map(function ($program) use ($date, $useStaticValues) {
                $class = new ProfessionProgram($program, true, $date);
                $this->count += $class->getPriem()->getReceptionPlan();
                $this->countWithDocuments += $class->getCountEnrolleesWithDocuments(true);
                $this->countWithOutDocuments += $class->getCountEnrolleesWithOutDocuments(true);

                return $class;
            });
    }

    public function getPrograms()
    {
        return $this->programs;
    }

    public function getCountAll()
    {
        return $this->count;
    }

    public function getCountWithDocuments()
    {
        return $this->countWithDocuments;
    }

    public function getCountWithOutDocuments()
    {
        return $this->countWithOutDocuments;
    }

    public function getDate()
    {
        return $this->date;
    }
}