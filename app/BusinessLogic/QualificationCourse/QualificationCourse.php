<?php
namespace App\BusinessLogic\QualificationCourse;

use App\Models\QualificationCourse as CourseModel;

class QualificationCourse{

    protected $course;

    public function __construct(CourseModel $course)
    {
        $this->course=$course;
    }

    public function getId(){
        return $this->course->id;
    }

    public function getTitle(){
        return $this->course->title;
    }

    public function getDescription(){
        return $this->course->description;
    }

    public function getHours(){
        return $this->course->count_hours." ".getNumEnding($this->course->count_hours,['час','часа','часов']);
    }

    public function getPeriod(){
        if(is_null($this->course->end_date)){
            return $this->course->start_date->format('d.m.Y');
        }

        $start=$this->course->start_date->format('d.m.Y');
        $end=$this->course->end_date->format('d.m.Y');

        return $start." - ".$end;
    }
}