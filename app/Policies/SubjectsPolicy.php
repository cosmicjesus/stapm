<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubjectsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view()
    {
        if (auth()->user()->can('subject.view') || auth()->user()->can('super.admin')) {
            return true;
        }

        return false;
    }

    public function actions()
    {
        if (auth()->user()->can('subject.actions') || auth()->user()->can('super.admin')) {
            return true;
        }

        return false;
    }
}
