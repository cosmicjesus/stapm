<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        if(auth()->user()->can('user.work') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function create(){
        if(auth()->user()->can('user.create') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function edit(){
        if(auth()->user()->can('user.edit') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function delete(){
        if(auth()->user()->can('user.delete') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }
}
