<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        if(auth()->user()->can('groups.work') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function create(){
        if(auth()->user()->can('groups.create') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function actions(){
        if(auth()->user()->can('groups.actions') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }
}
