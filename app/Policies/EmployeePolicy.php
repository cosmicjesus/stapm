<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        if (auth()->user()->can('employees.work') || auth()->user()->can('super.admin')) {
            return true;
        }

        return false;
    }

    public function view()
    {
        return $this->index();
    }

    public function create()
    {
        if (auth()->user()->can('employees.create') || auth()->user()->can('super.admin')) {
            return true;
        }

        return false;
    }

    public function actions()
    {
        if (auth()->user()->can('employees.actions') || auth()->user()->can('super.admin')) {
            return true;
        }

        return false;
    }
}
