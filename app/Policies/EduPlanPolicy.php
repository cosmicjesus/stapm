<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EduPlanPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        if(auth()->user()->can('eduplans.work') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function create(){
        if(auth()->user()->can('eduplans.create') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }

    public function actions(){
        if(auth()->user()->can('eduplans.actions') || auth()->user()->can('super.admin')){
            return true;
        }

        return false;
    }
}
