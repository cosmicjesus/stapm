<?php

namespace App\Console\Commands;

use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Console\Command;
use IRebega\DocxReplacer\Docx;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class GenereateT2References extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:t2-cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генериратор справок Т2';

    protected $disk;

    protected $cellStyles;

    protected $textStyles;

    protected $tableStyles;
    protected $parentsTable;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->disk = \Storage::disk('t2-cards');

        $this->cellStyles = [
            'borderTopSize' => 1,
            'borderRightSize' => 1,
            'borderBottomSize' => 1,
            'borderLeftSize' => 1
        ];

        $this->textStyles = ['bold' => true, 'align' => 'center'];


        $table_style = new \PhpOffice\PhpWord\Style\Table;
        $table_style->setBorderColor('000000');
        $table_style->setBorderSize(1);
        $table_style->setUnit(\PhpOffice\PhpWord\SimpleType\TblWidth::PERCENT);
        $table_style->setWidth(100 * 50);

        $this->tableStyles = $table_style;
    }

    public function getStudents()
    {

        $students = Student::query()
            ->whereHas('program', function ($q) {
                $q->where('education_form_id', 1);
            })
            ->OnlyTeach()
            ->where('gender', 0)
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get()
            ->map(function ($student) {
                return (new \App\AppLogic\Student\Student($student))->buildByT2Reference();
            })->toArray();

        return $students;
    }

    public function buildEmptyParentTable($table)
    {
        for ($c = 0; $c <= 4; $c++) {
            $this->parentsTable->addRow();
            for ($r = 0; $r <= 2; $r++) {
                $this->parentsTable->addCell(2000,
                    $this->cellStyles)->addText('');
            }
        }
    }

    public function buildTableWithParents($parents)
    {
        foreach ($parents as $parent) {
            $this->parentsTable->addRow();
            for ($c = 0; $c <= 2; $c++) {
                $this->parentsTable->addCell(2000,
                    $this->cellStyles)->addText($parent[$c]);
            }
        }

    }

    public function buildParentTable($parents)
    {
        $tableWord = new PhpWord();

        $section = $tableWord->addSection();
        $table = $section->addTable($this->tableStyles);

        $table_headers = [
            'Степень родства (ближайшие родственники)',
            'Фамилия, имя, отчество',
            'Год рождения'
        ];

        $table->addRow();

        foreach ($table_headers as $header) {
            $table->addCell(2000,
                $this->cellStyles
            )->addText($header, ['bold' => true, 'align' => 'center']);
        }
        $this->parentsTable = $table;
        if (count($parents) === 0) {
            for ($c = 0; $c <= 3; $c++) {
                $table->addRow();
                for ($r = 0; $r <= 2; $r++) {
                    $table->addCell(2000,
                        $this->cellStyles)->addText('');
                }
            }
        } else {
            foreach ($parents as $parent) {
                $table->addRow();
                for ($c = 0; $c <= 2; $c++) {
                    $table->addCell(2000,
                        $this->cellStyles)->addText($parent[$c]);
                }
            }
        }


        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($tableWord, 'Word2007');

        $fullxml = $objWriter->getWriterPart('Document')->write();

        $tablexml = preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullxml);

        return $tablexml;
    }

    protected function showMessage($message)
    {
        echo $message . " " . (Carbon::now())->format('H:i:s d.m.Y') . PHP_EOL;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function handle()
    {
        if (!file_exists(storage_path() . '/app/t2template.docx')) {
            echo 'Нет шаблона для генерации' . PHP_EOL;
            return false;
        }
        $this->showMessage("Начало работы");
        $this->showMessage("Удаляю старые файлы, если они есть");
        $dirs = $this->disk->directories();
        foreach ($dirs as $dir) {
            $this->showMessage("Удалю директорию {$dir}");
            $this->disk->deleteDirectory($dir);
        }
        $this->showMessage("Удаление завершено");
        $this->showMessage("Получание студентов");
        $students = $this->getStudents();
        $this->showMessage("Студенты получены. Начало генерации");
        foreach ($students as $student) {
            $file_name = $student['lastname'] . "_" . $student['firstname'] . "_" . $student['middlename'];
            $this->showMessage("/-------------------------------------/");
            $this->showMessage("Пишу файл {$file_name}");


            $word = new TemplateProcessor(storage_path('/app/t2template.docx'));
            if (!$this->disk->exists($student['group'])) {
                $this->disk->makeDirectory($student['group']);
            }

            $file_name_with_path = storage_path() . '/app/t2-cards/' . $student['group'] . "/" . $file_name . ".docx";


            $this->showMessage("ГОЛОСУЙТЕ ЗА НАВАЛЬНОГО");
            foreach ($student as $key => $param) {
                $word->setValue($key, $param);
            }
            $this->showMessage("Путин вор");
            $parentsTable = $this->buildParentTable($student['parents']);
            $word->setValue('table_parent', $parentsTable);

            $word->saveAs($file_name_with_path);

            $this->showMessage("Закончил писать файл {$file_name}");
            $this->showMessage("/-------------------------------------/");
        }

        $this->showMessage("Конец работы");
    }
}
