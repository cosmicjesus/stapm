<?php

namespace App\Console\Commands;

use App\AppLogic\Services\ExportService;
use App\Models\Entrant;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MakeStatement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:statement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация заявлений';
    protected \Illuminate\Filesystem\FilesystemAdapter $disk;

    protected ExportService $exportService;

    /**
     * Create a new command instance.
     *
     * @param ExportService $exportService
     */
    public function __construct(ExportService $exportService)
    {
        parent::__construct();
        $this->disk = \Storage::disk('statements');
        $this->exportService = $exportService;
    }

    protected function getEntrants()
    {
        return Entrant::query()
            ->orderBy('lastname')
            ->get()
            ->toArray();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!file_exists(storage_path() . '/app/statement_on_enrollment.docx')) {
            echo 'Нет шаблона для генерации' . PHP_EOL;
            return false;
        }

        $this->showMessage('Начинаю генерацию');
        $this->showMessage("/-------------------------------------/");
        $entrants = $this->getEntrants();
        $now = (Carbon::now())->format('d.m.Y');
        if (!$this->disk->exists($now)) {
            $this->disk->makeDirectory($now);
        }
        foreach ($entrants as $entrant) {
            $this->showMessage("/-------------------------------------/");
            $this->showMessage("Генерирую заявление " . $entrant['lastname'] . " " . $entrant['firstname']);
            $wordFile = $this->exportService->makeStatementOnEnrollmentByEntrantId($entrant['id']);
            $fileNameWithPath = storage_path() . "/app/statements/" . $now . "/" . $wordFile['filename'] . ".docx";
            $wordFile['file']->saveAs($fileNameWithPath);
            $this->showMessage("Закончил генерацию " . $entrant['lastname'] . " " . $entrant['firstname']);
            $this->showMessage("/-------------------------------------/");
        }
        $this->showMessage('Конец');
    }

    protected function showMessage($message)
    {
        echo $message . " " . (Carbon::now())->format('H:i:s d.m.Y') . PHP_EOL;
    }
}
