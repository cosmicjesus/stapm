<?php

namespace App\Console\Commands;

use App;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MakeSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация сайтмапа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Начинаем генерацию карты сайта');
        (new App\BusinessLogic\SiteMap\SiteMap)->make();
        $this->info('Генерация закончена');
    }
}
