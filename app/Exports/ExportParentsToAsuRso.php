<?php

namespace App\Exports;

use App\AppLogic\Student\Student;
use App\AppLogic\User\UserParent;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ExportParentsToAsuRso implements FromView
{
    use Exportable;

    protected $parents;

    public function __construct($parents)
    {
        $this->parents = $this->parentToExport($parents);
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('export.parents-to-asu-rso',
            [
                'parents' => $this->parents
            ]);
    }

    protected function parentToExport($parentsData): array
    {
        $parents = [];
        /**
         * @var $parent UserParent
         * @var $child Student
         */
        foreach ($parentsData as $parent) {
            foreach ($parent->getChildren() as $child) {
                $parents[] = [
                    'parent_lastname' => $parent->getLastname(),
                    'parent_firstname' => $parent->getFirstname(),
                    'parent_middlename' => $parent->getMiddlename(),
                    'parent_birthday' => dateFormat($parent->getBirthday()),
                    'parent_gender' => gender($parent->getGender()),
                    'parent_snils' => $parent->getSnils(),
                    'parent_phone' => $parent->getPhone(),
                    'relation_ship_type' => getRelationShipType($parent->getRelationShipType()),
                    'child_lastname' => $child->getLastname(),
                    'child_firstname' => $child->getFirstname(),
                    'child_middlename' => $child->getMiddlename(),
                    'child_birthday' => dateFormat($child->getBirthday()),
                ];
            }
        }
        return $parents;
    }
}
