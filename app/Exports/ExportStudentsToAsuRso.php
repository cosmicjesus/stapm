<?php

namespace App\Exports;

use App\AppLogic\Student\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ExportStudentsToAsuRso implements FromView
{
    use Exportable;
    protected $students;

    public function __construct($students)
    {
        $this->students = collect($students)->map(function ($student) {
            return $this->formatStudentToArray($student);
        });
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('export.student.asurso-table',
            [
                'students' => $this->students
            ]);
    }

    protected function formatStudentToArray(Student $student)
    {
        $enrollmentDecree = ($student->getDecrees())->last();
        $document = $student->getPassport();
        $privilege_category = count($student->getPrivilegedCategory()) > 0 ? $student->getPrivilegedCategory()[0] : '';
        return [
            'lastname' => $student->getLastname(),
            'firstname' => $student->getFirstname(),
            'middlename' => $student->getMiddlename(),
            'birthday' => dateFormat($student->getBirthday()),
            'gender' => gender($student->getGender()),
            'snils' => $student->getSnils() ?? '',
            'place_of_stay' => $student->buildAddress('place_of_stay')??'',
            'financy' => $student->isContractTargetSetStatus() ? 'За счет бюджета субъекта РФ по договору о целевом обучении' : 'За счет бюджета субъекта РФ',
            'group' => $student->getGroup()['code'],
            'decree_number' => $enrollmentDecree['decree']['number'],
            'decree_date' => dateFormat($enrollmentDecree['decree']['date']),
            'decree_start' => dateFormat($enrollmentDecree['decree']['date']),
            'reason' => reason($enrollmentDecree['type'], $enrollmentDecree['reason']),
            'nationality' => nationality($student->getNationality()),
            'health_category' => getHealthCategory($student->getHealthCategory(), true),
            'disability' => getDisability($student->getDisability(), true),
            'health_group' => '',
            'sport_group' => 'Основная',
            'need_adaptive_program' => 'Нет',
            'need_long_med' => 'Нет',
            'education' => educationLevel($student->getEducationId()),
            'graduation_date' => dateFormat($student->getGraduationDate()),
            'end_spec_organization' => 'Нет',
            'international_treaty' => 'Нет',
            'average_ball' => str_replace(',', '.', round($student->getAvg(), 2)),
            'privilege_category' => getPrivilegeCategory($privilege_category, true),
            'phone' => $student->getPhone(),
            'email' => $student->getEmail(),
            'comment' => $student->getComment(),
            'residential' => $student->buildAddress('residential'),
            'documentType' => documentType($document['documentType']) ?? '',
            'documentSeries' => $document['series'] ?? '',
            'documentNumber' => $document['number'] ?? '',
            'documentIssuanceDate' => dateFormat($document['issuanceDate']) ?? '',
            'documentIssued' => $document['issued'] ?? '',
            'documentSubdivisionCode' => $document['subdivisionCode'] ?? '',
            'documentBirthPlace' => $document['birthPlace'] ?? '',
            'registration' => $student->buildAddress('registration'),
            'military_rank' => getMilitaryRank($student->getMilitaryRank()),
            'military_spec' => $student->getMilitarySpecialty(),
            'military_fit_category' => shortFitForMilitaryServices($student->getFitnessForMilitaryService()),
            'military_group' => $student->isMale()?'РА':'',
            'zapas' => '',
            'military_document_number' => $student->getMilitaryDocumentNumber(),
            'sostav' => getMilitaryComposition($student->getMilitaryComposition()),
            'voenkomat' => ($student->getRecruitmentCenter())['name'] ?? '',
            'military_prepare' => $student->isServedInTheArmy() && $student->isMale() ? 'Да' : 'Нет',
            'special_record' => 'Нет',
            'nominal_number' => $student->getNominalNumber(),
            'has_inn' => $student->hasInn(),
            'inn' => $student->getInn()
        ];
    }
}
