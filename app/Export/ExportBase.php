<?php

namespace App\Export;


use App\Models\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ExportBase implements FromView
{
    use Exportable;

    protected $start_date;
    protected $end_date;
    protected $students;
    protected $type;

    public function __construct($type)
    {
        $this->start_date = env('START_DATE_PRIEM', '2018-05-15');
        $this->end_date = env('END_DATE_PRIEM', '2018-05-18');
        $this->type = $type;
        $this->students = Student::query()->where('status', 'student')
            ->whereBetween('enrollment_date', [$this->start_date, $this->end_date])
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get()
            ->map(function ($student) {
                return new \App\BusinessLogic\Student\Student($student);
            });
    }

    public function view(): View
    {
        return view('export.base', [
            'students' => $this->students,
            'type' => $this->type
        ]);
    }
}