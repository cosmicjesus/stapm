<?php


namespace App\Export;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportEnrollee implements FromView
{

    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function view(): View
    {
        return view('admin.reports.tpl.enrollee', $this->params);
    }
}