<?php

namespace App\Export;


use App\BusinessLogic\Enrollee\Enrollee;
use App\Models\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class AsuRsoExport implements FromView
{

    use Exportable;

    protected $start_date;
    protected $end_date;
    protected $enrollees;

    public function __construct()
    {
        $this->start_date = env('START_DATE_PRIEM','2018-05-15');
        $this->end_date = env('END_DATE_PRIEM','2018-05-18');
        $this->enrollees = Student::query()->where('status', 'enrollee')
            ->whereBetween('start_date', [$this->start_date, $this->end_date])
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get()
            ->map(function ($enrollee) {
                return new Enrollee($enrollee);
            });
    }

    public function view(): View
    {
        return view('export.enrollee-to-asu-rso', [
            'enrollees' => $this->enrollees
        ]);
    }
}