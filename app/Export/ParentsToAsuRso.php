<?php

namespace App\Export;


use App\Models\StudentParent;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ParentsToAsuRso implements FromView
{
    use Exportable;

    protected $parents;

    public function __construct()
    {
        $this->parents = StudentParent::query()
            ->whereHas('student', function ($q) {
                $q->where('status', 'student');
            })
            ->orderBy('lastname')
            ->orderBy('middlename')
            ->get()
            ->map(function ($parent) {
                return new \App\BusinessLogic\Student\StudentParent($parent);
            });
    }

    public function view(): View
    {
        return view('export.parents-to-asu-rso', [
            'parents' => $this->parents
        ]);
    }
}