<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class NominalNumberImport implements ToArray, WithCustomCsvSettings
{
    use Importable;

    /**
     * @param array $array
     */
    public function array(array $array)
    {
        return $array[0];
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';',
            'input_encoding'=>'UTF-8'
        ];
    }
}
