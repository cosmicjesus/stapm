<?php

namespace App\Mail;

use App\Models\Entrant as EntrantModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendUploadingDocumentsFormMail extends Mailable
{
    use Queueable, SerializesModels;

    protected EntrantModel $entrant;

    /**
     * Create a new message instance.
     *
     * @param EntrantModel $entrant
     */
    public function __construct(EntrantModel $entrant)
    {
        $this->entrant = $entrant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('priem@stapm.ru','Приемная комиссия СТАПМ')
            ->subject('Информирование о создании формы загрузки документов')
            ->with([
                'entrant'=>$this->entrant
            ])
            ->view('emails.uploading-documents');
    }
}
