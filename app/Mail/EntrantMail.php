<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntrantMail extends Mailable
{
    use Queueable, SerializesModels;
    public $entrant;

    /**
     * Create a new message instance.
     *
     * @param $entrant
     */
    public function __construct($entrant)
    {
        $this->entrant=$entrant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('priem@stapm.ru','Приемная комиссия СТАПМ')
            ->subject('Уведомление о подаче документов в СТАПМ')
            ->with([
                'entrant'=>$this->entrant
            ])
            ->view('emails.entrant');
    }
}
