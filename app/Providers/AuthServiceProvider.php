<?php

namespace App\Providers;

use App\Policies\EduPlanPolicy;
use App\Policies\EmployeePolicy;
use App\Policies\EnrolleePolicy;
use App\Policies\GodPolicy;
use App\Policies\GroupsPolicy;
use App\Policies\OpopPolicy;
use App\Policies\StudentPolicy;
use App\Policies\SubjectsPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        'Enrollee' => EnrolleePolicy::class,
        'Student' => StudentPolicy::class,
        'Groups' => GroupsPolicy::class,
        'Employee' => EmployeePolicy::class,
        'Opop' => OpopPolicy::class,
        'EduPlans' => EduPlanPolicy::class,
        'Subjects' => SubjectsPolicy::class,
        'God' => GodPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
