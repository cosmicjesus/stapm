<?php

namespace App\Providers;

use App\Models\Employee;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


        CarbonInterval::setLocale('ru');

        Validator::extend('check_employee',function($attribute, $value, $parameters, $validator){
            $inputs=$validator->getData();

            $model=Employee::where('lastname',$inputs['lastname'])
                ->where('firstname',$inputs['firstname'])
                ->where('birthday',Carbon::createFromFormat('d.m.Y',$inputs['birthday'])->format('Y-m-d'));
            if(!is_null($inputs['middlename'])){
                $model->where('middlename',$inputs['middlename']);
            }
            $result=$model->count();

            return $result ?false:true;

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('ru_RU');
        });
    }
}
