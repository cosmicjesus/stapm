<?php

namespace App\AppLogic\QualificationCourse;

use App\Models\QualificationCourse as CourseModel;

class QualificationCourse
{

    protected $course;

    public function __construct(CourseModel $course)
    {
        $this->course = $course;
    }

    public function getId()
    {
        return $this->course->id;
    }

    public function getTitle()
    {
        return $this->course->title;
    }

    public function getDescription()
    {
        return $this->course->description;
    }

    public function getHours()
    {
        return $this->course->count_hours;
    }

    public function getStartDate()
    {
        return $this->course->start_date->format('Y-m-d');
    }

    public function getEndDate()
    {


        if (is_null($this->course->end_date)) {
            return null;
        }

        return $this->course->end_date->format('Y-m-d');
    }

    public function build()
    {
        $course = collect();

        $course->put('id', $this->getId());
        $course->put('title', $this->getTitle());
        $course->put('hours', $this->getHours());
        $course->put('start_date', $this->getStartDate());
        $course->put('end_date', $this->getEndDate());

        return $course;
    }
}