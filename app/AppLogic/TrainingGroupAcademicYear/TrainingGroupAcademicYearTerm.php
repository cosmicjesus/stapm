<?php


namespace App\AppLogic\TrainingGroupAcademicYear;

use App\AppLogic\Base\BaseInterface;
use App\Models\TrainingGroupAcademicYearTerm as Model;

class TrainingGroupAcademicYearTerm implements BaseInterface
{

    protected $term;

    public function __construct(Model $term)
    {
        $this->term = $term;
    }

    public function getId()
    {
        return $this->term->id;
    }

    public function getNumber()
    {
        return $this->term->number;
    }

    public function getActive()
    {
        return $this->term->active;
    }

    public function isLastTerm()
    {
        $year = new TrainingGroupAcademicYear($this->term->academicYear);
        $calendar = $year->getCalendar();

        return ($calendar->isSemesterTerm() && $this->getNumber() == 2) || ($calendar->isCourseTerm() && $this->getNumber() == 1);
    }

    public function getNextTerm()
    {
        if (is_null($this->term->next_term)) {
            return null;
        }
        return new self($this->term->next_term);
    }

    public function isPast()
    {
        return $this->term->is_past;
    }

    public function set($key, $value)
    {
        $this->term->update([$key => $value]);
    }

    public function setActive()
    {
        $this->term->active = true;
        $this->term->save();
    }

    public function update($column, $value=null)
    {
        if (is_array($column)) {
            $this->term->update($column);
            return;
        }

        $this->term->update($column, $value);
    }
}