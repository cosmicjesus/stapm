<?php


namespace App\AppLogic\TrainingGroupAcademicYear;

use App\AppLogic\Base\BaseInterface;
use App\AppLogic\TrainingCalendar\TrainingCalendar;
use App\AppLogic\TrainingGroup\TrainingGroup;
use App\Models\TrainingGroupAcademicYear as Model;

class TrainingGroupAcademicYear implements BaseInterface
{

    protected $year;

    public function __construct(Model $year)
    {
        $this->year = $year;
    }

    public function getId()
    {
        return $this->year->id;
    }

    public function getNumber()
    {
        return $this->year->number;
    }

    public function getYearNumber()
    {
        return $this->year->year;
    }

    public function getActive()
    {
        return $this->year->active;
    }

    public function getTerms()
    {
        $this->year->load('terms');

        return $this->year->terms->map(function ($term) {
            return (new TrainingGroupAcademicYearTerm($term));
        });
    }

    public function getFirstTerm()
    {
        return $this->getTerms()->first();
    }

    public function getActiveTerm()
    {
        $this->year->load('activeTerm');

        return new TrainingGroupAcademicYearTerm($this->year->activeTerm);
    }

    public function getTrainingGroup()
    {
        return new TrainingGroup($this->year->trainingGroup);
    }

    public function isLastYear()
    {
        return !$this->isHasNextAcademicYear();
    }

    public function hasCalendar()
    {
        return !is_null($this->year->calendar);
    }

    public function getCalendar()
    {
        $calendar = $this->year->calendar;

        return new TrainingCalendar($calendar);
    }

    public function isHasNextAcademicYear()
    {
        return $this->year->next_academic_year;
    }

    public function isPast()
    {
        return $this->year->is_past;
    }

    public function getNextAcademicYear()
    {
        return new self($this->year->next_academic_year);
    }

    public function full()
    {
        return [
            'id' => $this->getId(),
            'year' => $this->getYearNumber(),
            'calendar' => $this->hasCalendar() ? $this->getCalendar()->buildFull() : null
        ];
    }

    public function update($column, $value = null)
    {
        if (is_array($column)) {
            $this->year->update($column);
            return;
        }
        $this->year->update([$column => $value]);

    }
}