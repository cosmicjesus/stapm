<?php

namespace App\AppLogic\Storage;


use Illuminate\Http\File;
use Storage;

class ProfessionProgramStorage extends BaseStorage
{
    protected $disk;
    protected $programFilesPath = '/{id}/files/{file}';

    public function __construct()
    {
        parent::__construct();
        $this->disk = Storage::disk('program_files');
    }

    public function hasFile($path)
    {
        return $this->disk->has($path);
    }

    public function getFileUrl($url)
    {
        return $this->disk->url($url);
    }

    public function putRawFile($file, $program_id, $old = null)
    {
        $path = massStrReplace($this->programFilesPath, [
            'id' => $program_id,
            'file' => $this->generateName()
        ]);
        $isSuccessUpload = Storage::disk('program_files')->put($path, $file);

        if (!is_null($old)) {
            if (Storage::disk('program_files')->has($old)) {
                $this->deleteProgramDocument($old);
            }
        }

        return $path;
    }

    public function deleteProgramDocument($path)
    {
        try {
            Storage::disk('program_files')->delete($path);
            return true;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    function widths()
    {
        // TODO: Implement widths() method.
    }
}