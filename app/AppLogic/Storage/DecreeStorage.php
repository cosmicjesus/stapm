<?php

namespace App\AppLogic\Storage;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DecreeStorage
{
    protected $disk;

    public function __construct()
    {
        $this->disk = Storage::disk('decrees');
    }

    public function getDecree($url)
    {
        try {
            return $this->disk->url($url);
        } catch (FileNotFoundException $e) {
            return $e;
        }
    }

    public function putDecree($file, $old = null)
    {
        try {
            $path = $this->disk->put(time(), new File($file), 'private');
            if (!is_null($old)) {
                $this->deleteDecree($old);
            }

            return $path;
        } catch (\Exception $exception) {
            return $exception;
        }


    }

    public function deleteDecree($url)
    {
        $explodePath = explode('/', $url);
        $this->disk->deleteDirectory($explodePath[0]);
    }
}