<?php

namespace App\AppLogic\Storage;


use Illuminate\Http\File;
use Illuminate\Support\Str;
use Storage;

class EducationPlanStorage
{

    protected $disk;
    protected $componentStorage;
    protected $path = '{profession_program_id}/education-plans/{file}';
    protected $componentPath = 'program-{profession_program_id}/education-plans-{id}';

    public function __construct()
    {

        $this->componentStorage = Storage::disk('component_documents');
        $this->disk = Storage::disk('education_plans');
    }

    public function getFile($path)
    {
        return $this->disk->url($path);
    }

    public function putComponentFile($params, $file)
    {
        $professionProgramId = $params['profession_program_id'] ?? 'default';
        $educationPlanId = $params['id'] ?? 'default';

        $fullPath = massStrReplace($this->componentPath, [
            'profession_program_id' => $professionProgramId,
            'id' => $educationPlanId,
        ]);

        try {
            return $this->componentStorage->putFile($fullPath, new File($file));
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function putRawPlan($program_id, $file, $oldFilePath = null)
    {
        $path = massStrReplace($this->path, [
            'profession_program_id' => $program_id,
            'file' => $this->generateName()
        ]);
        try {
            $uploadSuccess = $this->disk->put($path, $file);
            if ($uploadSuccess) {
                if ($oldFilePath) {
                    $this->deletePlan($oldFilePath);
                }
                return $path;
            } else {
                throw new \Exception('Не удалось сохранить файл');
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function deletePlan($path)
    {
        $this->disk->delete($path);
    }

    protected function generateName()
    {
        return Str::uuid() . ".pdf";
    }
}