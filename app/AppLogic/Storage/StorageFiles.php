<?php

namespace App\AppLogic\Storage;


use Illuminate\Http\File;
use Storage;

class StorageFiles
{
    protected static $programFilesPath = '/{id}/files';
    protected static $educationPlanPath = '/{program_id}/education-plans';

    public static function getProgramDocument($file)
    {
        if (is_null($file)) {
            return null;
        }
        return Storage::disk('program_files')->url($file);
    }

    public static function putProgramDocument($file, $id, $old = null)
    {
        if (!is_null($old)) {
            if (Storage::disk('program_files')->has($old)) {
                self::deleteProgramDocument($old);
            }
        }

        $path = self::$programFilesPath;


        return Storage::disk('program_files')->putFile(str_replace('{id}', $id, $path), new File($file));
    }

    public static function deleteProgramDocument($path)
    {
        try {
            Storage::disk('program_files')->delete($path);
            return true;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public static function getEducationPlan($path)
    {
        try {
            return Storage::disk('program_files')->url($path);
        } catch (\Exception $exception) {

        }
    }

    public static function putEducationPlan($file, $program_id = 'file', $old = null)
    {
        try {
            if (!is_null($old)) {
                self::deleteProgramDocument($old);
            }
            return Storage::disk('program_files')->putFile(str_replace('{program_id}', $program_id, self::$educationPlanPath), new File($file));
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function getDocumentPath($path)
    {
        return Storage::disk('documents')->url($path);
    }

    public static function putDocument($file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteDocument($old);
        }

        $path = Storage::disk('documents')->put(time(), new File($file));
        return $path;
    }

    public static function deleteDocument($file)
    {
        $explodedPath = explode('/', $file);
        Storage::disk('documents')->delete($file);
        if (count($explodedPath) > 1) {
            Storage::disk('documents')->deleteDir($explodedPath[0]);
        }
        return true;
    }

}