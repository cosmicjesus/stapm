<?php

namespace App\AppLogic\Storage;


use Illuminate\Http\File;
use Storage;

class ComponentDocumentStorage
{
    protected $disk;

    public function __construct()
    {
        $this->disk = Storage::disk('component_documents');
    }

    public function putFile($file, $oldFile = null)
    {
        try {

            $path = $this->disk->putFile('', new File($file));
            if ($oldFile) {
                $this->deleteFile($oldFile);
            }

            return $path;

        } catch (\Exception $exception) {

        }
    }

    public function getUrl($path)
    {
        return $this->disk->url($path);
    }

    public function deleteFile($path)
    {
        try {
            $this->disk->delete($path);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

}