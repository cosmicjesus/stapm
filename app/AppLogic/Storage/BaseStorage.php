<?php

namespace App\AppLogic\Storage;


use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;

abstract class BaseStorage
{

    protected $width = [];


    public function __construct()
    {
        $this->setWidth();
    }

    abstract function widths();

    public function setWidth()
    {
        $this->width = $this->widths();
    }

    protected function generateName()
    {
        return str_replace('-', '', Str::uuid()) . ".pdf";
    }

    private function getImageOrientation($width, $height)
    {
        if ($height == $width) {
            return 'box';
        } elseif ($height < $width) {
            return "horizontal";
        } else {
            return "vertical";
        }
    }

    public function getWidth($width, $height)
    {
        $orientation = $this->getImageOrientation($width, $height);
        return $this->width[$orientation];
    }

    protected function resizeImage($image)
    {
        $file = ImageManagerStatic::make($image);
        $image_data = $file->exif('COMPUTED');
        //dd($image_data,is_null($image_data));
        $width = $this->getWidth($image_data['Width'], $image_data['Height']);

        $resize_image = $file->resize($width, null, function ($constraint) {
            return $constraint->aspectRatio();
        })->encode('jpg');

        return $resize_image;
    }
}
