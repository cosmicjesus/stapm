<?php

namespace App\AppLogic\Storage;

use Illuminate\Http\File;
use Storage;

class NewsStorage extends BaseStorage
{
    protected $disk;


    public function __construct()
    {
        parent::__construct();
        $this->disk = Storage::disk('news');
    }

    function widths()
    {
        return [
            'box' => 300,
            'horizontal' => 500,
            'vertical' => 200
        ];
    }

    public function checkFile($path)
    {
        return $this->disk->exists($path);
    }

    private function getNewsFilesPath(int $news_id)
    {
        return $news_id . "/files";
    }

    private function getNewsImagePath(int $news_id)
    {
        return $news_id . "/images/" . $this->generateName() . '.jpg';
    }

    /**
     * @param $url
     * @return string
     */
    public function getNewsImage($url): string
    {
        return $this->disk->url($url);
    }


    /**
     * @param $image
     * @param $news_id
     * @param null $old
     * @return string
     * @throws \Exception
     */
    public function putNewsImage($image, $news_id, $old = null)
    {
        try {
            $resize = $this->resizeImage($image);
            $name = $this->getNewsImagePath($news_id);

            $this->disk->put($name, $resize);
            return $name;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $news_id
     * @param $file
     * @param null $old
     * @return mixed
     * @throws \Exception
     */
    public function putNewsFile($news_id, $file, $old = null)
    {
        try {
            $path = $this->disk->putFile($this->getNewsFilesPath($news_id), new File($file));
            if ($path) {
                if ($old) {
                    $this->deleteNewsFile($old);
                }
                return $path;
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $path
     */
    public function deleteNewsFile($path)
    {
        $this->disk->delete($path);
    }

    /**
     * @param $dir_name
     */
    public function deleteNewsDir($dir_name)
    {
        $this->disk->deleteDirectory($dir_name);
    }

}