<?php

namespace App\AppLogic\Storage;


use Illuminate\Http\File;
use Storage;

class StudentStorage
{
    protected $disk;

    public function __construct()
    {
        $this->disk = Storage::disk('student');
    }

    public function getFile($path)
    {
        return $this->disk->url($path);
    }

    public function getPhoto($path)
    {
        $pathToFile = storage_path('app/private/students/' . $path);
        $type = pathinfo($pathToFile, PATHINFO_EXTENSION);
        $data = $this->disk->get($path);
        $fileTobase64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $fileTobase64;
    }

    public function getPhotoData($path)
    {
        return $this->disk->url($path);
    }

    public function putFoto(int $id, $file, $old)
    {
        try {
            $path = $this->disk->putFile($id, new File($file));

            if (!is_null($old)) {
                $this->deleteFile($old);
            }

            return $path;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function putFile(int $id, $file)
    {
        try {
            $path = $this->disk->putFile($id, new File($file));

            return $path;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function deleteFile(string $path)
    {
        $this->disk->delete($path);
    }

    public function deleteStudentDirectory($dirName)
    {
        $this->disk->deleteDirectory($dirName);
    }

    public function deleteStudentsDirectories($directories)
    {
        foreach ($directories as $directory) {
            $this->deleteStudentDirectory($directory);
        }
    }
}