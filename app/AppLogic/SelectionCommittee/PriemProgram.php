<?php

namespace App\AppLogic\SelectionCommittee;


use App\Models\ProfessionProgram;

class PriemProgram
{
    protected ProfessionProgram $program;
    protected $pivot;

    public function __construct(ProfessionProgram $program)
    {
        $this->program = $program;
        $this->pivot = $program->pivot;
    }

    public function getId(): int
    {
        return $this->pivot->id;
    }

    public function getProgram()
    {
        return [
            'id' => $this->program->id,
            'name' => $this->program->name_with_form,
            'slug' => $this->program->slug,
            'type' => $this->program->type_id,
            'duration' => convertTrainingPeriod($this->program->trainin_period),
            'education_form' => $this->program->education_form_id,
            'profession'=>$this->program->profession->toArray()
        ];
    }

    public function getStudentSourceStatus()
    {
        return (bool)$this->pivot->student_source;
    }

    public function getReceptionPlan()
    {
        return $this->pivot->reception_plan;
    }

    public function getStaticCountEnrolleesWithDocument()
    {
        return $this->pivot->count_with_documents;
    }

    public function getStaticCountEnrolleesWithOutDocument()
    {
        return $this->pivot->count_with_out_documents;
    }

    public function getCommitteeId()
    {
        return $this->pivot->selection_committee_id;
    }

    public function getCountNumberOfEnrolled()
    {
        return $this->pivot->number_of_enrolled;
    }

    public function getPercentageOfCompletion()
    {
        $percent = ($this->getStaticCountEnrolleesWithDocument() / $this->getReceptionPlan()) * 100;
        return round($percent, 2);
    }

    public function toTable()
    {
        $program = collect();

        $program->put('id', $this->getId());
        $program->put('program', $this->getProgram());
        $program->put('reception_plan', $this->getReceptionPlan());
        $program->put('selection_committee_id', $this->getCommitteeId());
        $program->put('count_with_out_documents', $this->getStaticCountEnrolleesWithOutDocument());
        $program->put('is_student_source', $this->getStudentSourceStatus());
        $program->put('count_with_documents', $this->getStaticCountEnrolleesWithDocument());
        $program->put('number_of_enrolled', $this->getCountNumberOfEnrolled());
        $program->put('percentage_of_completion', $this->getPercentageOfCompletion());
        return $program;
    }
}
