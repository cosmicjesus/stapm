<?php

namespace App\AppLogic\SelectionCommittee;

use App\Models\SelectionCommittee as Model;

class SelectionCommittee
{
    protected $committee;
    protected $programs;

    public function __construct(Model $committee)
    {
        $this->committee = $committee;
        $this->committee->load('priem_programs');
        $this->programs = $this->committee->priem_programs;
    }

    public function getId(): int
    {
        return $this->committee->id;
    }

    public function getName(): string
    {
        return $this->committee->name;
    }

    public function getStartDate(): string
    {
        return $this->committee->start_date->format('Y-m-d');
    }

    public function getEndDate(): string
    {
        return $this->committee->end_date->format('Y-m-d');
    }

    public function getLastDayOfWork(): string
    {
        return $this->committee->last_day_of_work->format('Y-m-d');
    }

    public function getActiveStatus(): bool
    {
        return $this->committee->active;
    }

    public function getPrograms()
    {
        return $this->programs->map(function ($program) {
            return (new PriemProgram($program))->toTable();
        });
    }

    public function full()
    {
        $committee = collect();
        $committee->put('id', $this->getId());
        $committee->put('name', $this->getName());
        $committee->put('start_date', $this->getStartDate());
        $committee->put('end_date', $this->getEndDate());
        $committee->put('last_day_of_work', $this->getLastDayOfWork());
        $committee->put('active', $this->getActiveStatus());
        $committee->put('programs', $this->getPrograms());

        return $committee;
    }
}