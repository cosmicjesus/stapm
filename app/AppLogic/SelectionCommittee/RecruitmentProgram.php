<?php

namespace App\AppLogic\SelectionCommittee;


use App\AppLogic\ProfessionProgram\ProfessionProgram;
use App\Helpers\Constants;
use App\Models\PriemProgram as Model;
use App\Models\Student;
use Illuminate\Support\Collection;

class RecruitmentProgram
{
    protected Model $program;
    protected $students;
    protected $committee;
    protected $peoples;
    protected $recruitDates;

    public function __construct(Model $program)
    {
        $this->program = $program;
        $this->students = $program->students;
        $this->committee = $program->selectionCommittee;
        $this->recruitDates = $program->recruitDates;

        if ($this->program->student_source) {
            $this->peoples = $program->students;
        } else {
            $this->peoples = $program->entrants;
        }

    }

    /**
     * Возвращает айдишник связанной записи
     * @return int
     */
    public function getId(): int
    {
        return $this->program->id;
    }

    public function getSlug(): string
    {
        return $this->program->slug;
    }

    public function getProgram()
    {
        return new ProfessionProgram($this->program->program);
    }

    /**
     * Возвращает имя программы
     * @return string
     */
    public function getProgramName(): string
    {
        return $this->program->program->name_with_form;
    }

    public function getProgramDuration()
    {
        return $this->program->program->trainin_period;
    }

    public function getProgramSlug()
    {
        return $this->program->program->slug;
    }

    public function getProgramType()
    {
        return $this->program->program->type_id;
    }

    public function getProgramId()
    {
        return $this->program->program->id;
    }

    public function getStudents()
    {
        return $this->students;
    }

    public function getSelectionCommittee()
    {
        return $this->committee;
    }

    /**
     * Возвращает True, если установлен источник данных студенты
     * @return bool
     */
    public function getStudentSourceStatus(): bool
    {
        return $this->program->student_source;
    }

    protected function getPeoplesToArray()
    {
        return $this->peoples->map(function (Student $entrant) {
            return [
                'id' => $entrant->id,
                'full_name' => $entrant->full_name,
                'average_score' => $entrant->avg,
                'has_original' => $entrant->has_original,
                'is_contract' => $entrant->contract_target_set,
                'status' => $entrant->status,
                'how_find_out' => $entrant->how_find_out,
                'start_date' => $entrant->start_date->format('Y-m-d'),
                'is_priority' => $entrant->pivot->is_priority,
                'sum_of_grades_of_specialized_disciplines' => $entrant->pivot->sum_of_grades_of_specialized_disciplines,
                'sss' => $entrant
            ];
        });
    }

    /**
     * @return array
     */
    public function getPeoples(): array
    {
        $count = $this->peoples->count();

        $peoples = $this->getPeoplesToArray();

        return ['count' => $count, 'peoples' => $peoples];
    }

    /**
     * Возвращает план приема
     * @return int
     */
    public function getReceptionPlan(): int
    {
        return $this->program->reception_plan;
    }

    /**
     * Возвращает статичное количество абитуриентов с оригиналами документов
     * @return int
     */
    public function getStaticCountWithDocuments(): int
    {
        return $this->program->count_with_documents;
    }

    /**
     * Возвращает статичное количество абитуриентов без оригиналов документа
     * @return int
     */
    public function getStaticCountWithOutDocuments(): int
    {
        return $this->program->count_with_out_documents;
    }

    protected function getPercentageOfCompletion($count)
    {
        $percent = ($count / $this->getReceptionPlan()) * 100;
        return round($percent, 2);
    }

    public function getAverageOnProgram()
    {
        $countPeoples = $this->peoples->count();

        if ($countPeoples == 0) {
            return 0;
        }

        $entrantsAverageSum = $this->peoples->map(function ($entrant) {
            return $entrant->avg ?? 0;
        })->sum();

        $totalAverage = $entrantsAverageSum / $countPeoples;
        return round($totalAverage, 3);
    }

    /**
     * Возвращает массив со количеством абитуринетов по полу
     * @return array
     */
    public function getGenderStatistic(): array
    {
        $peoples = $this->peoples;

        $countMale = $peoples->where('gender', 0)->count();
        $countFemale = $peoples->where('gender', 1)->count();

        return ['male' => $countMale, 'female' => $countFemale];
    }


    /**
     * Возвращает количество абитуриентов с договорами и без договоров
     * @return array
     */
    public function getContractTargetStatistic(): array
    {
        $peoples = $this->peoples;

        $withContract = $peoples->where('contract_target_set', true)->count();
        $withOutContract = $peoples->where('contract_target_set', false)->count();

        return ['with_contract' => $withContract, 'with_out_contract' => $withOutContract];
    }

    /**
     * Возвращает статистику по нуждающимся в общежитии
     * @return array
     */
    public function getHostelStatistic(): array
    {
        $peoples = $this->peoples;

        $needHostel = $peoples->where('need_hostel', true)->count();
        $noNeedHostel = $peoples->where('need_hostel', false)->count();

        return ['need_hostel' => $needHostel, 'no_need_hostel' => $noNeedHostel];
    }

    public function getPreferentialCategoriesStatistic()
    {
        $peoples = $this->peoples->where('preferential_categories', '!=', null);

        $categories = Constants::$privilegetCategories;

        $data = [];

        foreach ($categories as $key => $category) {
            if (!array_key_exists($key, $data)) {
                $data[$key] = 0;
            }

            foreach ($peoples as $people) {
                if (in_array($key, $people->preferential_categories)) {
                    $data[$key]++;
                }
            }
        }


        return $data;
    }

    /**
     * Возвращает количество абитуриентов с оригиналами и без оригиналов
     * @return array
     */
    public function getDocumentStatistic(): array
    {
        $peoples = $this->peoples;

        $countWithDocuments = $peoples->where('has_original', true)->count();
        $countWithOutDocuments = $peoples->where('has_original', false)->count();

        return [
            'static' => [
                'count_with_documents' => $this->getStaticCountWithDocuments(),
                'count_with_out_documents' => $this->getStaticCountWithOutDocuments(),
                'percentage_of_completion' => $this->getPercentageOfCompletion($this->getStaticCountWithDocuments())
            ],
            'real' => [
                'count_with_documents' => $countWithDocuments,
                'count_with_out_documents' => $countWithOutDocuments,
                'percentage_of_completion' => $this->getPercentageOfCompletion($countWithDocuments)
            ]
        ];
    }

    /**
     * Возвращает количество абитуриентов изучающих каждый язык
     * @return array
     */
    public function getLanguageStatistic(): array
    {
        $languages = Constants::$languages;

        $data = [];

        foreach ($languages as $key => $language) {
            $data[$key] = $this->peoples->where('language', $key)->count();
        }

        return $data;
    }

    public function getPrivilegeCategoryStatistic(): array
    {
        $categories = Constants::$privilegetCategories;

        $data = [];

        foreach ($categories as $key => $category) {
            $data[$key] = $this->peoples->where('privileged_category', $key)->count();
        }

        return $data;
    }

    public function getHowFindOutStatistic()
    {

        $sources = Constants::$howFindOut;

        $data = [];

        foreach ($sources as $key => $source) {
            $data[$key] = $this->peoples->where('how_find_out', $key)->count();
        }

        return $data;
    }

    public function getMinAndMaxAvg()
    {
        $min = $this->peoples->min('avg');
        $max = $this->peoples->max('avg');

        return ['min' => $min, 'max' => $max];
    }

    public function getDayRecruitStatistic()
    {
        $peoples = $this->getPeoplesToArray()->sortBy('full_name')->groupBy('start_date')->toArray();
        return $this->recruitDates->keyBy('date')->map(function ($date) use ($peoples) {
            $recruitPeoples = array_key_exists($date->date, $peoples) ? $peoples[$date->date] : [];
            return ['count' => $date->count, 'peoples' => $recruitPeoples];
        });
    }

    public function getGraphicStatistic()
    {
        $stats = collect();

        $stats->put('day_recruit', $this->getDayRecruitStatistic());

        return $stats;
    }

    /**
     * Коллекция статистики
     * @return Collection
     */
    public function getStatistic(): Collection
    {
        $statistic = collect();

        $statistic->put('gender_statistic', $this->getGenderStatistic());
        $statistic->put('documents_statistic', $this->getDocumentStatistic());
        $statistic->put('language_statistic', $this->getLanguageStatistic());
        $statistic->put('contract_statistic', $this->getContractTargetStatistic());
        $statistic->put('hostel_statistic', $this->getHostelStatistic());
        $statistic->put('preferential_categories_statistic', $this->getPreferentialCategoriesStatistic());
        $statistic->put('how_find_out_statistic', $this->getHowFindOutStatistic());
        $statistic->put('avg_statistic', $this->getMinAndMaxAvg());

        return $statistic;
    }

    public function full()
    {
        $program = collect();

        $program->put('id', $this->getId());
        $program->put('program_name', $this->getProgramName());
        $program->put('average_score_on_program', $this->getAverageOnProgram());
        $program->put('entrants', $this->getPeoples());
        $program->put('is_student_source', $this->getStudentSourceStatus());
        $program->put('statistic', $this->getStatistic());
        $program->put('graphic_statistic', $this->getGraphicStatistic());
        $program->put('selection_committee', $this->getSelectionCommittee());

        return $program;
    }


}
