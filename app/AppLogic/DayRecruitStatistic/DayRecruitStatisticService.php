<?php


namespace App\AppLogic\DayRecruitStatistic;


use App\AppLogic\Base\BaseService;
use App\Models\RecruitEntrantDate;

class DayRecruitStatisticService extends BaseService
{

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return RecruitEntrantDate::class;
    }

    public function incrementOrCreateOnDate($date, $recruitment_program_id)
    {

        $model = $this->model::query()->where(['date' => $date, 'recruitment_program_id' => $recruitment_program_id])->first();

        if ($model) {
            $model->increment('count');
        } else {
            $this->model::query()->create(['date' => $date, 'recruitment_program_id' => $recruitment_program_id, 'count' => 1]);
        }
    }

    public function decrementStatistic($params)
    {
        $model = $this->model::query()->where($params)->first();

        if ($model) {
            if ($model->count === 1) {
                $model->delete();
            } else {
                $model->decrement('count');
            }
        }

    }

    public function refreshStatistic($oldParams, $newParams)
    {
        $this->decrementStatistic($oldParams);

        $this->incrementOrCreateOnDate($newParams['date'], $newParams['recruitment_program_id']);
    }
}