<?php

namespace App\AppLogic\Document;


use App\AppLogic\CategoryDocument\CategoryDocument;
use App\AppLogic\Storage\StorageFiles;

class Document
{
    protected $document;

    public function __construct($document)
    {
        $this->document = $document;
    }

    public function getId()
    {
        return $this->document->id;
    }

    public function getName()
    {
        return $this->document->name;
    }

    public function getSort()
    {
        return $this->document->sort;
    }

    public function getActive()
    {
        return $this->document->active;
    }

    public function getCategories()
    {
        $this->document->load('categories');
        $categories = $this->document->categories;

        return $categories->map(function ($category) {
            return (new CategoryDocument($category))->buildToDocument();
        });
    }

    public function getPath()
    {
        return StorageFiles::getDocumentPath($this->document->path);
    }

    public function getLastUpdateDate()
    {
        if (is_null($this->document->updated_at)) {
            return null;
        }

        return $this->document->updated_at->format('H:i:s d.m.Y');
    }

    public function build()
    {
        $document = collect();

        $document->put('id', $this->getId());
        $document->put('name', $this->getName());
        $document->put('sort', $this->getSort());
        $document->put('categories', $this->getCategories());
        $document->put('path', $this->getPath());
        $document->put('active', $this->getActive());
        $document->put('last_update', $this->getLastUpdateDate());

        return $document;
    }
}