<?php

namespace App\AppLogic\News;


use App\AppLogic\Storage\NewsStorage;

class NewsFile
{
    protected $file;
    protected $storage;

    public function __construct($file)
    {
        $this->file = $file;
        $this->storage = new NewsStorage();
    }

    public function getId()
    {
        return $this->file->id;
    }

    public function getName()
    {
        return $this->file->name;
    }

    public function getPath()
    {
        return $this->storage->getNewsImage($this->file->path);
    }

    public function full()
    {
        $file = collect();

        $file->put('id', $this->getId());
        $file->put('name', $this->getName());
        $file->put('path', $this->getPath());

        return $file;
    }
}