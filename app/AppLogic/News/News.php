<?php

namespace App\AppLogic\News;

use App\AppLogic\Storage\NewsStorage;
use App\Models\News as Model;

class News
{
    protected $news;
    protected $files;
    protected $images;
    protected $storage;

    protected $defaultImage = '/img/default-news-img.jpg';

    /**
     * News constructor.
     * @param Model $news
     */
    public function __construct(Model $news)
    {
        $this->news = $news;
        $this->news->load(['files', 'images']);
        $this->files = $this->news->files;
        $this->images = $this->news->images;
        $this->storage = new NewsStorage();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->news->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->news->title;
    }

    /**
     * @return string
     */
    public function getPublicationDate(): string
    {
        return $this->news->publication_date->format('Y-m-d');
    }

    /**
     * @return string|null
     */
    public function getVisibleToDate()
    {
        if (is_null($this->news->visible_to)) {
            return null;
        }

        return $this->news->visible_to->format('Y-m-d');
    }

    /**
     * @return bool
     */
    public function getActiveStatus(): bool
    {
        return $this->news->active;
    }

    /**
     * @return string
     */
    public function getPreview(): string
    {
        return $this->news->preview;
    }

    /**
     * @return string
     */
    public function getFullText(): string
    {
        return $this->news->full_text;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->news->type;
    }

    /**
     * @return bool
     */
    public function hasImage(): bool
    {
        return (!is_null($this->news->image)) && ($this->storage->checkFile($this->news->image));
    }

    /**
     * @return string|null
     */
    public function getImage()
    {
        if ($this->hasImage()) {
            return $this->storage->getNewsImage($this->news->image);
        }

        return $this->defaultImage;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files->map(function ($file) {
            return (new NewsFile($file))->full();
        });
    }

    /**
     * @return bool
     */
    public function hasImages(): bool
    {
        return $this->images->count() > 0 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images->map(function ($image) {
            return (new NewsImage($image))->full();
        });
    }

    public function getClientDetailUrl()
    {
        return route('client.news.one', ['slug' => $this->news->slug]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function buildBase()
    {
        $news = collect();
        $news->put('id', $this->getId());
        $news->put('title', $this->getTitle());
        $news->put('active', $this->getActiveStatus());
        $news->put('type', $this->getType());
        $news->put('publication_date', $this->getPublicationDate());
        $news->put('visible_to', $this->getVisibleToDate());
        $news->put('has_image', $this->hasImage());
        $news->put('image', $this->getImage());
        $news->put('url', $this->getClientDetailUrl());
        $news->put('preview', $this->getPreview());
        return $news;
    }

    public function full()
    {
        $news = $this->buildBase();
        $news->put('full_text', $this->getFullText());
        $news->put('files', $this->getFiles());
        $news->put('images', $this->getImages());
        return $news;
    }
}