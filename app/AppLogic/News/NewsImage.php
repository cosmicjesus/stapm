<?php

namespace App\AppLogic\News;


use App\AppLogic\Storage\NewsStorage;

class NewsImage
{
    protected $image;
    protected $storage;

    public function __construct($newsImage)
    {
        $this->image = $newsImage;
        $this->storage = new NewsStorage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->image->id;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->storage->getNewsImage($this->image->path);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function full()
    {
        return collect([
            'id' => $this->getId(),
            'path' => $this->getPath()
        ]);
    }
}