<?php


namespace App\AppLogic\Services;

use App\AppLogic\Base\BaseService;
use App\AppLogic\Student\Student;
use App\AppLogic\TrainingGroup\TrainingGroup as ClassHandler;
use App\AppLogic\TrainingGroupAcademicYear\TrainingGroupAcademicYearTerm;
use App\Models\Decree;
use App\Models\Education;
use App\Models\EducationPlan;
use App\Models\TrainingCalendar;
use App\Models\TrainingGroup as Model;
use App\Models\Student as StudentModel;
use App\Models\TrainingGroupAcademicYear;
use App\AppLogic\TrainingGroupAcademicYear\TrainingGroupAcademicYear as AcademicYearClassHandler;
use Chumper\Zipper\Zipper;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TrainingGroupService extends BaseService
{
    protected $classHandler;
    protected $studentService;

    /**
     * TrainingGroupService constructor.
     * @throws \Exception
     */
    public function __construct(StudentService $studentService)
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->onlyTeach()->orderBy('pattern', 'ASC')->orderBy('course');
        $this->classHandler = ClassHandler::class;

        $this->studentService = $studentService;
    }

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function filter($stringParams)
    {
        return $this;
    }

    public function paginate($perPage = 15)
    {
        try {
            $groups = $this->queryModel->paginate($perPage);

            $data = $groups->map(function ($group) {
                return (new ClassHandler($group))->buildToTable();
            })->sortBy('full_name')->values();

            return response()->json([
                'count' => $groups->total(),
                'groups' => $data
            ]);

        } catch (\Exception $exception) {

        }
    }

    public function show($id)
    {
        try {
            $group = $this->queryModel->findOrFail($id);
            $groupToResponse = (new $this->classHandler($group))->full();
            return responder()->success($groupToResponse)->respond();
        } catch (ModelNotFoundException $exception) {
            return responder()->error($exception->getCode(), "Не найдена группа с ID {$id}")->respond(404);
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    public function store($data)
    {
        try {
            $calendar = TrainingCalendar::findOrFail($data['calendar_id']);
            $educationPlan = EducationPlan::query()->findOrFail($data['education_plan_id']);
            $group = $this->model::query()->create(array_merge($data, ['status' => 'teach', 'term_type' => $calendar->term_type]));
            $this->createPeriods($group, $calendar->year, $educationPlan->count_period / 2);
            $this->setCalendarOnFirstYear($group, $calendar);

            CacheService::refershTrainingGroupsCount();

            return responder()->success(['message' => "Группа успешно создана. ID {$group->id}"])->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    protected function createPeriods($groupModel, $startYear, $countPeriods)
    {

        for ($i = 1; $i <= $countPeriods; $i++) {
            $academicYear = TrainingGroupAcademicYear::create([
                'training_group_id' => $groupModel->id,
                'training_calendar_id' => null,
                'education_plan_period_id' => null,
                'number' => $i,
                'year' => $startYear,
                'active' => false,
                'is_past' => false,
            ]);

            $countTerms = $groupModel->term_type == 'semester' ? 2 : 1;

            for ($k = 1; $k <= $countTerms; $k++) {
                $academicYear->terms()->create([
                    'calendar_item_id' => null,
                    'number' => $k,
                    'active' => false,
                    'is_past' => false,
                ]);
            }
            $startYear++;
        }
    }

    protected function setCalendarOnFirstYear($groupModel, $calendarModel)
    {
        /**
         * @var $group ClassHandler
         * @var $yearTerm TrainingGroupAcademicYearTerm
         */
        $group = new $this->classHandler($groupModel);
        $calendar = new \App\AppLogic\TrainingCalendar\TrainingCalendar($calendarModel);

        $firstYear = $group->getAcademicYears()->first();
        $yearTerms = $firstYear->getTerms();

        $calendarPeriods = $calendar->getPeriods();

        $periodsIdByNumber = [];

        foreach ($calendarPeriods as $calendarPeriod) {
            $periodsIdByNumber[$calendarPeriod['number']] = $calendarPeriod['id'];
        }

        foreach ($yearTerms as $key => $yearTerm) {
            $number = $yearTerm->getNumber();
            $calendarItemId = $periodsIdByNumber[$number];
            $updateFields = ['calendar_item_id' => $calendarItemId];
            if ($key == 0) {
                $updateFields['active'] = true;
            }
            $yearTerm->update($updateFields);
        }
        $firstYear->update(['active' => true, 'training_calendar_id' => $calendar->getId()]);
    }


    protected function getGroupOnTransferMap()
    {
        $groups = $this->queryModel->onlyTeach()->with('currentAcademicYear')->get()
            ->map(function ($group) {
                return new ClassHandler($group);
            })
            ->map(function (ClassHandler $group) {
                $transferType = null;

                if ($group->getCurrentAcademicYear()->isLastYear() && $group->getCurrentAcademicYear()->getActiveTerm()->isLastTerm()) {
                    $transferType = 'Graduation';
                } else {
                    $transferType = $group->getCurrentAcademicYear()->getActiveTerm()->isLastTerm() ? 'AcademicYear' : 'AcademicTerm';
                }

                $next_year = $group->getCurrentAcademicYear()->getYearNumber();
                return [
                    'id' => $group->getId(),
                    'transferType' => $transferType,
                    'course' => $group->getCourse(),
                    'name' => $group->getName(),
                    'period_id' => $group->getCurrentAcademicYear()->getId(),
                    'term_type' => $group->getCurrentAcademicYear()->getCalendar()->getTermType(),
                    'next_year' => $transferType == 'Graduation' ? $next_year : $next_year + 1
                ];
            });

        return $groups;
    }

    public function buildTransferMap()
    {

        $groups = $this->getGroupOnTransferMap();

        $transfer = (collect($groups))->groupBy(['transferType', 'next_year', 'term_type', 'course']);

        $trArr = [];

        foreach ($transfer as $transferType => $item) {
            if ($transferType == 'Graduation') {
                foreach ($item as $yearKey => $val) {
                    $trArr[] = [
                        'target' => $transferType,
                        'year' => $yearKey,
                        'academicYears' => collect($val)->flatten(2)->groupBy('course')->toArray()
                    ];
                }
            } else {
                foreach ($item as $termKey => $terms) {
                    foreach ($item as $yearKey => $termType) {
                        foreach ($termType as $key => $groups) {
                            $params = [
                                'target' => $transferType,
                                'year' => $yearKey,
                                'termType' => $key,
                                'academicYears' => $groups
                            ];

                            if ($transferType === 'AcademicTerm') {
                                $params['termNumber'] = 2;
                            }
                            $trArr[] = $params;
                        }
                    }
                }
            }
        }

        return collect($trArr)->sortBy('target');
    }

    public function getTransferMap()
    {
        try {
            $transferMap = $this->buildTransferMap();
            return responder()->success($transferMap)->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    public function transferNextTerm($input)
    {
        try {
            $periods = TrainingGroupAcademicYear::query()
                ->find($input['periodIds'])
                ->map(function ($period) {
                    return new AcademicYearClassHandler($period);
                });

            foreach ($periods as $period) {
                /**
                 * @var $period AcademicYearClassHandler
                 */
                $activeTerm = $period->getActiveTerm();

                $activeTerm->getNextTerm()->set('active', true);

                $activeTerm->update(['active' => false, 'is_past' => true]);
            }

            return responder()->success()->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    public function getAcademicYearsByIds($ids)
    {
        $periods = TrainingGroupAcademicYear::query()
            ->find($ids)
            ->map(function ($period) {
                return new AcademicYearClassHandler($period);
            });

        return $periods;
    }

    public function transferNextYear($input)
    {
        DB::beginTransaction();
        try {
            $periods = $this->getAcademicYearsByIds($input['periodIds']);
            $groupIds = [];

            $trainingCalendar = TrainingCalendar::query()->findOrFail($input['training_calendar_id']);

            $calendarHandler = new \App\AppLogic\TrainingCalendar\TrainingCalendar($trainingCalendar);
            $calendarPeriods = $calendarHandler->getPeriods();

            $calendarPeriodsGroupById = $calendarPeriods->keyBy('number')->map(function ($period) {
                return $period['id'];
            });

            foreach ($periods as $key => $period) {
                /**
                 * @var $period AcademicYearClassHandler
                 */
                $period->update(['active' => false, 'is_past' => true]);
                $period->getActiveTerm()->update(['active' => false, 'is_past' => true]);
                $nextPeriod = $period->getNextAcademicYear();
                /**
                 * @var $nextPeriod AcademicYearClassHandler
                 */
                $nextPeriod->update('training_calendar_id', $input['training_calendar_id']);
                $terms = $nextPeriod->getTerms();
                $groupIds[] = $period->getTrainingGroup()->getId();
                foreach ($terms as $term) {
                    /***
                     * @var $term TrainingGroupAcademicYearTerm
                     */
                    $term->set('calendar_item_id', $calendarPeriodsGroupById[$term->getNumber()]);
                }
                $nextPeriod->update('active', true);
                $nextPeriod->getFirstTerm()->set('active', true);
            }
            $decree = (new DecreeService())->getDecreeByParams(
                [
                    'number' => $input['decree_number'],
                    'date' => $input['decree_date'],
                    'type' => Decree::$internalTransferType
                ]
            );
            foreach ($groupIds as $groupId) {
                $this->transferGroupForNextYear($groupId, $decree);
            }

            DB::commit();
            return responder()->success()->respond();
        } catch (\Exception $exception) {
            DB::rollBack();
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    protected function transferGroupForNextYear($group_id, $decree)
    {
        $group = $this->model::query()->findOrFail($group_id);
        /**
         * @var $groupHandler ClassHandler
         */
        $groupHandler = new $this->classHandler($group);

        $students = $groupHandler->getStudents();

        foreach ($students as $student) {

            /**
             * @var $student Student
             */

            $student->transferNextYear($decree->id);

        }

    }

    public function exportStudentsProfiles($group_id)
    {
        $group = $this->queryModel->with('students')->findOrFail($group_id);

        $students = $group->students->map(function ($student) {
            return new Student($student);
        });

        $pdf = (new StudentService())->buildStudentsPdfProfiles($students);

        return $pdf->stream();
    }

    public function exportStudentsToAsuRso($group_id)
    {
        $group = $this->queryModel->with('students')->findOrFail($group_id);

        $studentsIds = $group->students->map(function ($student) {
            return $student->id;
        })->toArray();

        return (new StudentService())->exportAsuRsoTableByStudentIds($studentsIds);
    }

    public function exportStudentsParentsToAsuRso($group_id)
    {
        $group = $this->queryModel->with('students')->findOrFail($group_id);

        $parents = $group->students->map(function ($student) {
            return $student->parents;
        })->flatten();

        return (new StudentService())->exportParentsToAsuRso($parents);
    }

    public function exportStudentsPhotos($group_id)
    {
        $students = StudentModel::query()
            ->onlyTeach()
            ->where('group_id', $group_id)
            ->whereNotNull('photo')
            ->orderBy('lastname')
            ->orderBy('firstname')
            ->get();
        if ($students->count() === 0) {
            abort(404, 'В выбраной группе нет фотографий для выгрузки');
        }
        $group = $this->queryModel->findOrFail($group_id);

        $archiveName = storage_path() . "/app/students-photos/{$group->unique_code}.zip";
        $archive = new Zipper();
        $archive->make($archiveName);
        foreach ($students as $key => $student) {
            $photoData = base_path() . '/storage/app/private/students/' . $student->photo;
            $fileName = "{$key} | {$student->full_name}.jpeg";
            $archive->add($photoData, $fileName);
        }
        $archive->close();
        return response()->download($archiveName);
    }

    public function graduation($input)
    {
        try {

            $periods = $this->getAcademicYearsByIds($input['periodIds']);
            $groupsIds = $periods->map(function ($period) {
                return $period->getTrainingGroup()->getId();
            });

            $graduationParams = [
                'groupsIds' => $groupsIds,
                'date' => $input['decree_date'],
                'number' => $input['decree_number']
            ];

            $this->graduationGroup($graduationParams);

            foreach ($periods as $period) {
                /**
                 * @var $period AcademicYearClassHandler
                 */
                $period->update(['active' => false, 'is_past' => true]);

                $period->getActiveTerm()->update(['active' => false, 'is_past' => true]);
            }

            CacheService::refershTrainingGroupsCount();
            return responder()->success()->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->respond(500);
        }
    }

    public function graduationGroup(array $input)
    {

        $expelledDecree = Decree::firstOrCreate([
            'date' => $input['date'],
            'number' => $input['number'],
            'type' => Decree::$graduationType,
            'term' => getCurrentPeriod($input['date'])
        ]);

        $groupQuery = $this->queryModel->with('students')->whereIn('id', $input['groupsIds']);
        $groups = $groupQuery->get();

        $studentsIdsByGroups = $groups->map(function ($group) {
            return [
                'id' => $group->id,
                'studentIds' => $group->students->map(function ($student) {
                    return $student->id;
                })
            ];
        });

        foreach ($studentsIdsByGroups as $item) {
            $this->studentService->retireStudents([
                'ids' => $item['studentIds'],
                'reason' => 'endCollege',
                'decree_date' => $input['date'],
                'decree_number' => $input['number']
            ]);
        }

        $groupQuery->update([
            'expelled_decree_id' => $expelledDecree->id,
            'status' => 'release'
        ]);


    }

    public function massPrintReferences($group_id)
    {
        try {
            $students = StudentModel::query()
                ->where('group_id', $group_id)
                ->get()
                ->map(function ($student) {
                    return $student->id;
                })->toArray();


            return (new ReferenceService())->makeReferencesByStudentIds($students);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }
}
