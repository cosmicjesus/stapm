<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\Models\Subject;

class SubjectService extends BaseService
{

    protected $queryModel;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model->query();
    }

    function model()
    {
        return Subject::class;
    }

    public function filter($filter)
    {
        $params = json_decode($filter, true);
        list('type_id' => $type_id, 'name' => $name) = $params;
        $this->queryModel->when(filter_var($type_id, FILTER_VALIDATE_INT), function ($q, $param) {
            return $q->where('type_id', $param);
        })->when($name, function ($q) use ($name) {
            return $q->like('name', $name);
        });

        return $this;
    }

    public function paginate($perPage = 15)
    {
        $subjects = $this->queryModel->orderBy('name')->paginate($perPage);
        $data = $subjects->map(function ($plan) {
            return (new \App\AppLogic\Subject\Subject($plan))->buildOnResponse();
        });

        return [
            'total' => $subjects->total(),
            'subjects' => $data
        ];
    }

    public function create($input)
    {
        try {
            if (!$this->checkUnique($input)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Дисциплина с таким типом и названием уже существует'
                ])->setStatusCode(422);
            }

            Subject::create($input);

            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно добавлена'
            ])->setStatusCode(200);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => true,
                'line' => $exception->getLine(),
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function update($id, $input)
    {
        try {
            if (!$this->checkUnique($input)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Дисциплина с таким типом и названием уже существует'
                ])->setStatusCode(422);
            }

            Subject::query()->findOrFail($id)->update($input);
            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно обновлена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => true,
                'line' => $exception->getLine(),
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    protected function checkUnique($params)
    {
        return Subject::checkUnique($params);
    }

    public function delete($id)
    {
        try {
            $subject = Subject::query()->with('programs')->findOrFail($id);

            if ($subject->programs->count() > 0) {
                $programs = $subject->programs->map(function ($program) {
                    return $program->name_with_form;
                });
                return response()->json([
                    'success' => false,
                    'message' => 'Невозможно удалить дисциплину т.к она прикреплена к следующим образовательным программам',
                    'programs' => $programs
                ])->setStatusCode(422);
            }
            $subject->delete();
            return response()->json([
                'success' => true,
                'message' => 'Дисциплина была успешно удалена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine()
            ])->setStatusCode(500);
        }
    }
}