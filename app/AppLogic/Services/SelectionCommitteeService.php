<?php

namespace App\AppLogic\Services;

use App\AppLogic\Base\BaseService;
use App\Exceptions\CustomModelNotFoundException;
use App\Http\Requests\Request;
use App\Models\Document;
use App\Models\SelectionCommittee as Model;
use App\AppLogic\SelectionCommittee\SelectionCommittee;
use DB;
use Illuminate\Support\Str;
use PDF;

class SelectionCommitteeService extends BaseService
{
    protected $queryModel;
    protected $handlerClass;
    protected $documentsService;


    public function __construct()
    {
        parent::__construct();
        $this->documentsService = new DocumentService();
        $this->queryModel = $this->model::query();
        $this->handlerClass = SelectionCommittee::class;
    }


    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function startQuery()
    {
        $this->queryModel = $this->model::query();
        return $this;
    }

    public function all()
    {
        try {
            $items = $this->queryModel->orderBy('start_date', 'DESC')->get()->map(function ($item) {
                return (new $this->handlerClass($item))->full();
            });

            return response()->json([
                'active_committe_id' => $this->getActiveCommitteeId(),
                'items' => $items
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'line' => $exception->getLine(),
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function store($input)
    {
        DB::beginTransaction();
        try {
            $this->queryModel->create($input);
            DB::commit();
            return response()->json([
                'message' => 'Все нормально',
                'success' => true
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function update($id, $input)
    {
        DB::beginTransaction();
        try {

            DB::commit();
            return response()->json([
                'message' => 'Все нормально',
                'success' => true
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {

            DB::commit();
            return response()->json([
                'message' => 'Все нормально',
                'success' => true
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function getById($id)
    {
        $model = $this->queryModel->find($id);

        if (!$model) {
            throw new CustomModelNotFoundException('Не найдена приемная коммиссия с таким ID');
        }

        return (new $this->handlerClass($model))->full();
    }

    public function getActiveCommitteeId()
    {
        try {
            $model = $this->getActiveCommittee();

            return $model->id ?? null;
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }


    public function getActiveCommittee(): ?\App\Models\SelectionCommittee
    {
        try {
            $model = $this->queryModel->orderBy('start_date', 'DESC')->where('active', true)->first();

            return $model;
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function getProgramsByCommitteeId($id = null)
    {
        try {

            if (is_null($id)) {
                $id = $this->getActiveCommitteeId();
            }
            $committee = $this->startQuery()->getById($id);

            return [
                'success' => true,
                'programs' => $committee['programs']
            ];
        } catch (CustomModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function buildReportOfManning(array $request)
    {
        try {
            $programs = $this->getProgramsByCommitteeId();

            $reportDataTable = $this->buildReportOfManningTable($programs['programs']);

            $reportDate = isset($request['report_date']) ? dateFormat($request['report_date']) : carbon()->format('d.m.Y');

            $activeCommittee = $this->getActiveCommittee();
            $academicYear = [
                'start' => $activeCommittee->start_date->format('Y'),
                'end' => $activeCommittee->start_date->addYear()->format('Y')
            ];
            $reportData = array_merge($reportDataTable, ['date' => $reportDate, 'academicYear' => $academicYear]);

            $putInSection = filter_var($request['put_in_section'], FILTER_VALIDATE_BOOLEAN);


            $pdf = PDF::loadView('reports.reportOfManningEntrants',
                [
                    'data' => $reportData
                ],
                [],
                [
                    'format' => 'A4-L'
                ]
            );

            if ($putInSection) {
                $this->putDocumentOnSection($pdf, $reportDate);
            }

            return $pdf->stream("Сведения о комплектовании приема {$reportDate}");

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    protected function buildReportOfManningTable($programs)
    {
        $reportData = [];
        foreach ($programs as $program) {
            $profession = $program['program']['profession'];
            if (!array_key_exists($profession['id'], $reportData)) {
                $reportData[$profession['id']] = [
                    'code' => $profession['code'],
                    'name' => $profession['name'],
                    'reception_plan' => 0,
                    'count_with_documents' => 0,
                    'count_with_out_documents' => 0,
                    'number_of_enrolled' => 0,
                    'forms' => [1 => 0, 2 => 0, 3 => 0]
                ];
            }
            $reportData[$profession['id']]['count_with_documents'] += $program['count_with_documents'];
            $reportData[$profession['id']]['count_with_out_documents'] += $program['count_with_out_documents'];
            $reportData[$profession['id']]['reception_plan'] += $program['reception_plan'];
            $reportData[$profession['id']]['number_of_enrolled'] += $program['number_of_enrolled'];
            $reportData[$profession['id']]['forms'][$program['program']['education_form']] += $program['count_with_documents'];

        }

        $reportDataToCollect = collect($reportData)->sortBy('code')->values();

        $total = [
            'count_with_out_documents' => $reportDataToCollect->sum('count_with_out_documents'),
            'count_with_documents' => $reportDataToCollect->sum('count_with_documents'),
            'number_of_enrolled' => $reportDataToCollect->sum('number_of_enrolled'),
            'reception_plan' => $reportDataToCollect->sum('reception_plan'),
            'forms' => [
                1 => $reportDataToCollect->sum('forms.1'),
                2 => $reportDataToCollect->sum('forms.2'),
                3 => $reportDataToCollect->sum('forms.3')
            ]
        ];

        $result = [
            'data' => $reportDataToCollect,
            'total' => $total,
        ];

        return $result;
    }

    public function putDocumentOnSection($pdf, $date = null)
    {

        if (is_null($date)) {
            $date = carbon()->format('d.m.Y');
        }
        $filename = time() . '/' . Str::random() . '.pdf';
        \Storage::disk('documents')->put($filename, $pdf->output());
        $categoryId = $this->documentsService->getCategoryIdBySlug('admission');
        $documentModel = Document::create([
            'name' => "Сведения о ходе комплектования групп приема на {$date}",
            'path' => $filename,
            'sort' => 500
        ]);

        $documentModel->categories()->sync([$categoryId]);
    }

}