<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\SelectionCommittee\RecruitmentProgram;
use App\Exceptions\DuplicateEntryException;
use App\Models\PriemProgram;
use App\Models\SelectionCommittee;
use App\Models\Student;
use DB;

class RecruitmentProgramService extends BaseService
{

    protected $queryModel;
    protected $handlerClass;

    public function __construct()
    {
        parent::__construct();

        $this->queryModel = $this->model::query();
        $this->handlerClass = RecruitmentProgram::class;
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return PriemProgram::class;
    }

    protected function checkProgramInList(array $input)
    {
        $isHasProgram = $this->queryModel->whereSelectionCommitteeId($input['selection_committee_id'])
            ->whereProfessionProgramId($input['profession_program_id'])
            ->first();

        if ($isHasProgram) {
            throw new DuplicateEntryException();
        }
    }

    public function show($id)
    {
        try {

            $model = $this->queryModel->findOrFail($id);
            $program = (new $this->handlerClass($model))->full();
            return response()->json([
                'success' => true,
                'program' => $program
            ]);
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ]
            )->setStatusCode(500);
        }
    }

    public function getBySlug(string $slug)
    {
        try {
            $model = $this->queryModel->where('slug', $slug)->first();
            return (new $this->handlerClass($model))->full();
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ]
            )->setStatusCode(500);
        }
    }


    public function store(array $input)
    {
        DB::beginTransaction();
        try {
            $this->checkProgramInList($input);
            $committeeModel = SelectionCommittee::query()->findOrFail($input['selection_committee_id']);

            $committeeModel->priem_programs()->attach($input['profession_program_id'], $input);

            DB::commit();

            return response()->json([
                'success' => true
            ]);
        } catch (DuplicateEntryException $exception) {
            return response()->json(
                [
                    'success' => false,
                    'errors' => [
                        'По этой программе уже ведется набор в этой приемной комиссии'
                    ]
                ]
            )->setStatusCode(422);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(
                [
                    'success' => false,
                    'errors' => [
                        $exception->getMessage()
                    ]
                ]
            )->setStatusCode(500);
        }
    }

    public function update(int $id, array $input)
    {
        DB::beginTransaction();
        try {

            $this->queryModel->findOrFail($id)->update($input);

            DB::commit();

            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(
                [
                    'success' => false,
                    'errors' => [
                        $exception->getMessage()
                    ]
                ]
            )->setStatusCode(500);
        }
    }

    public function getProgramsByCommitteeId(int $committeeId)
    {
        $this->startQuery();
        try {
            $showStaticCountEntrants = setting('showStaticCountEnrollees', true);
            $programs = $this->queryModel
                ->whereSelectionCommitteeId($committeeId)
                ->get()
                ->map(function ($program) use ($showStaticCountEntrants) {
                    $p = new RecruitmentProgram($program);
                    $documentStatistic = $p->getDocumentStatistic();
                    return [
                        'id' => $p->getId(),
                        'profession_program_id' => $p->getProgramId(),
                        'slug' => $p->getSlug(),
                        'duration' => $p->getProgram()->getDuration(),
                        'program_name' => $p->getProgramName(),
                        'program_slug' => $p->getProgramSlug(),
                        'reception_plan' => $p->getReceptionPlan(),
                        'type' => $p->getProgramType(),
                        'data' => $showStaticCountEntrants ? $documentStatistic['static'] : $documentStatistic['real']
                    ];
                })->sortBy('program_name');

            $total = [
                'reception_plan' => $programs->sum('reception_plan'),
                'count_with_documents' => $programs->sum('data.count_with_documents'),
                'count_with_out_documents' => $programs->sum('data.count_with_out_documents')
            ];

            return ['programs' => $programs, 'total' => $total];
        } catch (\Exception $exception) {

        }
    }

    public function getActualProgramByAverage($average)
    {
        try {
            $committeeID = (new SelectionCommitteeService())->getActiveCommitteeId();

            $listData = ($this->getProgramsByCommitteeId($committeeID))['programs'];


            $dd = [];
            foreach ($listData as $program) {

                $averages = Student::query()
                    ->select('avg')
                    ->where('profession_program_id', $program['profession_program_id'])
                    ->where('status', 'enrollee')
                    ->orderBy('avg', 'DESC')
                    ->get()
                    ->map(function ($av) {
                        return $av->avg;
                    });

                $dd[] = [
                    'program_name' => $program['program_name'],
                    'reception_plan' => $program['reception_plan'],
                    'averages' => [
                        'count' => $averages->count(),
                        'max' => $averages->first(),
                        'min' => $averages->get(($program['reception_plan'] - 1)) ?? $averages->last()
                    ]];
            }

            $actualPrograms = [];

            foreach ($dd as $item) {
                $isHasPlace = $item['reception_plan'] > $item['averages']['count'];
                $inRange = ($item['averages']['max'] >= $average) && ($average >= $item['averages']['min']);
                if ($isHasPlace || (!$isHasPlace && $inRange)) {
                    $actualPrograms[] = $item;
                }
            }

            return responder()->success(['programs' => $actualPrograms])->respond();
        } catch (\Exception $exception) {
            return $this->showException($exception);
        }
    }
}
