<?php


namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\TrainingCalendar\TrainingCalendar;
use App\Models\TrainingCalendar as Model;
use DB;
use function PHPSTORM_META\map;

class TrainingCalendarService extends BaseService
{

    protected $queryModel;
    protected $classHandler;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->orderBy('name');
        $this->classHandler = TrainingCalendar::class;

    }

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function all()
    {
        try {
            $calendars = $this->queryModel
                ->orderBy('year')
                ->orderBy('name')
                ->get()
                ->map(function ($calendar) {
                    /**
                     * @var $convertCalendar TrainingCalendar
                     */

                    $convertCalendar = new $this->classHandler($calendar);

                    return [
                        'id' => $convertCalendar->getID(),
                        'year' => $convertCalendar->getYear(),
                        'name' => $convertCalendar->getName(),
                        'termType' => $convertCalendar->getTermType()
                    ];

                });
            return responder()->success(['calendars' => $calendars])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function getByYear($year)
    {
        $calendars = $this->queryModel->where('year', $year);

        return $calendars->get()->map(function ($calendar) {
            return (new $this->classHandler($calendar))->buildFull();
        });
    }

    public function store($input)
    {
        try {
            DB::beginTransaction();
            $model = $this->model::query()->create($input);

            foreach ($input['periods'] as $period) {
                $model->periods()->create($period);
            }
            DB::commit();
            return response()->json([
                'success' => true,
                'id' => $model->id
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(500);
        }
    }
}
