<?php

namespace App\AppLogic\Services;

use App\AppLogic\Base\BaseService;
use App\AppLogic\ProfessionProgram\ProgramSubject;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\ProfessionProgram;
use App\Models\ProgramBasicDocument;
use App\Models\SelectionCommittee;

class ProfessionProgramService extends BaseService
{
    protected $professionProgram;
    protected $class;

    /**
     * ProfessionProgramService constructor.
     * @throws \Exception
     * @var $professionProgram ProfessionProgram
     */

    public function __construct()
    {
        /**
         * @var $model ProfessionProgram
         */
        parent::__construct();
        $this->professionProgram = $this->model::query()->where('active', true)->whereHas('profession', function ($q) {
            return $q->orderBy('code');
        });
        $this->class = \App\AppLogic\ProfessionProgram\ProfessionProgram::class;
    }

    function model()
    {
        return ProfessionProgram::class;
    }


    public function create($input)
    {
        try {

            if ($this->checkProgramOnBase($input)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Такая программа уже существует в базе'
                ])->setStatusCode(422);
            }

            $program = $this->model->create([
                'profession_id' => $input['profession'],
                'education_form_id' => $input['form'],
                'licence' => $input['licence'],
                'trainin_period' => $input['duration'],
                'type_id' => $input['type'],
                'qualification' => $input['qualification'],
                'sort' => $input['sort'],
                'top_fifty' => $input['top_fifty'],
                'active' => $input['active'],
                'on_site' => true,
            ]);

            ProgramBasicDocument::create([
                'profession_program_id' => $program->id
            ]);

            CacheService::refershProgramsCount();

            return response()->json([
                'success' => true,
                'message' => 'Программа была успешно добавлена'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => true,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function checkProgramOnBase($input)
    {
        $programCount = $this->model
            ->where('profession_id', $input['profession'])
            ->where('education_form_id', $input['form'])
            ->where('trainin_period',$input['duration'])->count();

        return $programCount > 0;
    }

    public function update(int $id, array $input)
    {
        try {

            $model = $this->model->findOrFail($id);

            $model->update([
                'qualification' => $input['qualification'],
                'licence' => $input['licence'],
                'trainin_period' => $input['duration'],
                'on_site' => $input['on_site'],
                'active' => $input['active'],
                'top_fifty' => $input['top_fifty'],
                'sort' => $input['sort']
            ]);

            if (array_key_exists('image', $input)) {
                $model->uploadImage($input['image']);
            }

            return response()->json([
                'success' => true,
                'message' => 'Программа была успешно обновлена'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => true,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function filter($filter)
    {
        $filter = json_decode($filter, true);
        $this->professionProgram->when(filter_var($filter['type'], FILTER_VALIDATE_INT), function ($q, $filter) {
            return $q->where('type_id', $filter);
        })->when(filter_var($filter['form'], FILTER_VALIDATE_INT), function ($q, $filter) {
            return $q->where('education_form_id', '=', $filter);
        });
        return $this;
    }

    public function paginate($perPage = 10)
    {
        $programs = $this->professionProgram->paginate($perPage);
        $data = $programs->map(function ($program) {
            return (new \App\AppLogic\ProfessionProgram\ProfessionProgram($program))->buildOnTable();
        })->sortBy('profession.name')->values()->toArray();

        return [
            'total' => $programs->total(),
            'programs' => $data
        ];
    }

    public function onlyActive()
    {
        $this->professionProgram->where('active', true);
        return $this;
    }

    public function onlyOnSite()
    {
        $this->professionProgram->where('on_site', true);
        return $this;
    }

    public function get()
    {
        return $this->professionProgram->get()->sortBy('profession.code')->map(function ($program) {
            return (new \App\AppLogic\ProfessionProgram\ProfessionProgram($program))->buildOnTable();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getAll()
    {
        return $this->professionProgram->get()->sortBy('profession.code')->map(function ($program) {
            return new \App\AppLogic\ProfessionProgram\ProfessionProgram($program);
        });
    }

    public function getById($id)
    {
        $program = parent::getById($id);

        return (new \App\AppLogic\ProfessionProgram\ProfessionProgram($program))->full();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {

        $program = $this->professionProgram->where('slug', $slug)->first();

        if ($program) {
            return new $this->class($program);
        }

        throw (new CustomModelNotFoundException)->setModel($this->model, [$slug], 'Не найдена программа с таким slug');

    }

    public function makeUmk($profession_program_id)
    {
        try {
            $years = (new ProfessionProgramAcademicYearService())->getActiveAcademicYears($profession_program_id);
            if ($years->count() === 0) {
                return [];
            }
            $yearsIds = $years->map(function ($year) {
                return $year->id;
            })->toArray();

            $yearsKeyById = $years->keyBy('id')->map(function ($year) {
                return $year->year_number;
            })->toArray();

            $subjects = $this->getSubjectsByAcademicIds($profession_program_id, $yearsIds)->groupBy('pivot.program_academic_year_id');

            $umk = [];

            foreach ($subjects as $key => $subject) {
                $year = $yearsKeyById[$key];
                $umk[$year] = ProgramSubject::formatToProgram($subject, true, true);
            }

            ksort($umk);

            return $umk;
        } catch (\Exception $exception) {

        }
    }

    public function programPriem()
    {
        try {
            $lastCommitteeProgram = SelectionCommittee::query()
                ->with('priem_programs')
                ->orderBy('start_date', 'DESC')
                ->first();

            $committeePrograms = $lastCommitteeProgram->priem_programs->map(function ($program) {
                $programData = new \App\AppLogic\ProfessionProgram\ProfessionProgram($program);
                return [
                    'name' => $programData->getName(),
                    'education_form_id' => $programData->getEducationFormId(),
                    'reception_plan' => $program->pivot->reception_plan,
                    'duration' => $programData->getDuration(),
                    'presentation_path' => $programData->getPresentationPath(),
                    'video_presentation' => $programData->getVideoPresentationUrl()
                ];
            })->toArray();

            return $committeePrograms;
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    protected function getSubjectsByAcademicIds($profession_program_id, $academic_year_ids)
    {
        try {
            return $this->model::find($profession_program_id)->subjects()->wherePivotIn('program_academic_year_id', (array)$academic_year_ids)->get();
        } catch (\Exception $exception) {

        }
    }

    public function getSubjectByYear($profession_program_id, $academic_year_id)
    {
        try {
            $subjects = $this->getSubjectsByAcademicIds($profession_program_id, $academic_year_id);

            if (!$subjects) {
                return response()->json([
                    'success' => true,
                    'program' => []
                ]);
            }

            return response()->json([
                'success' => true,
                'subjects' => ProgramSubject::formatToProgram($subjects)
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(500);
        }
    }

    public function priemList()
    {
        try {
            $programs = $this->professionProgram
                ->whereHas('priem')
                ->with(['profession', 'priem'])
                ->get()
                ->sortBy('profession.code')
                ->map(function ($program) {
                    $program = new \App\AppLogic\ProfessionProgram\ProfessionProgram($program);
                    $isShowStaticCountEnrollees = setting('showStaticCountEnrollees');
                    return [
                        'programName' => $program->getNameWithForm(),
                        'slug' => $program->getSlug(),
                        'type' => $program->getType() == 2 ? 'профессии' : 'специальности',
                        'receptionPlan' => $program->getReceptionPlan(),
                        'countWithDocuments' => $isShowStaticCountEnrollees ? $program->getStaticCountEnrolleesWithDocuments() : $program->getCountEnrolleeWithDocuments(),
                        'countWithOutDocuments' => $isShowStaticCountEnrollees ? $program->getStaticCountEnrolleesWithOutDocuments() : $program->getCountEnrolleeWithOutDocuments()
                    ];
                });

            $total = [
                'totalReception' => $programs->sum('receptionPlan'),
                'countWithOutDocuments' => $programs->sum('countWithOutDocuments'),
                'countWithDocuments' => $programs->sum('countWithDocuments'),
            ];

            return collect(['programs' => $programs, 'total' => $total]);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public static function getProgramsByIdOneType($program_id)
    {
        $currentProgram = ProfessionProgram::query()->findOrFail($program_id);

        $activeCommitteePrograms = ((new SelectionCommitteeService())->getProgramsByCommitteeId())['programs'];

        $activeProgramsIds = $activeCommitteePrograms->map(fn($program) => $program['program']['id'])->toArray();

        return ProfessionProgram::query()
            ->where('type_id', $currentProgram->type_id)
            ->where('education_form_id', $currentProgram->education_form_id)
            ->where('id', '!=', $program_id)
            ->get()
            ->filter(fn($program) => in_array($program->id,$activeProgramsIds));
    }

}
