<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Student\Student;
use App\Exceptions\CustomModelNotFoundException;
use App\Exports\ExportParentsToAsuRso;
use App\Exports\ExportStudentsToAsuRso;
use App\Models\Activity;
use App\Models\TrainingGroup as TrainingGroupModel;
use App\Models\Decree;
use App\Models\Student as Model;
use Illuminate\Http\JsonResponse;
use DB;
use JWTAuth;
use Maatwebsite\Excel\Excel;
use PDF;
use App\AppLogic\User\UserParent;

class StudentService extends BaseService
{
    protected $queryModel;
    protected $class;
    protected $cacheService;


    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->whereIn('status', ['student', 'inArmy', 'academic'])->orderBy('lastname')->orderBy('firstname');
        $this->class = Student::class;
        $this->cacheService = new CacheService();
    }

    public function model()
    {
        return Model::class;
    }

    public function filter($params)
    {
        $filterParams = json_decode($params, true);
        if (count($filterParams)) {
            list('lastname' => $lastname, 'group_id' => $group_id, 'privilege_ids' => $privilege_ids, 'status' => $status, 'id' => $id) = $filterParams;
            $this->filterById($id);
            $this->filterByLastname($lastname);
            $this->filterByGroup($group_id);
            $this->filterByPrivilegeCategory($privilege_ids);
            $this->filterByStatus($status);
        }
        return $this;
    }

    public function filterById($id)
    {
        $this->queryModel->when($id, function ($q, $id) {
            $ids = explode('/', $id);
            if (count($ids) == 1) {
                $q->where('id', $ids[0]);
            } else {
                $q->whereIn('id', $ids);
            }
        });
    }

    public function filterByLastname($lastname)
    {
        $this->queryModel->when($lastname, function ($q, $lastname) {
            $lastnamesArr = explode('/', $lastname);
            if (count($lastnamesArr) == 1) {
                $q->like('lastname', $lastnamesArr[0]);
            } else {
                $q->whereIn('lastname', $lastnamesArr);
            }
        });
    }

    public function filterByGroup($group_id)
    {
        $this->queryModel->when($group_id, function ($q, $group_id) {
            $q->like('group_id', $group_id);
        });
    }

    public function filterByPrivilegeCategory($privilege_ids)
    {
        if (count($privilege_ids)) {
            $this->queryModel->when($privilege_ids, function ($q, $privilege_ids) {
                $q->whereIn('privileged_category', $privilege_ids);
            });
        }
    }

    public function filterByStatus($status)
    {
        if ($status) {
            switch ($status) {
                case 1:
                    $this->queryModel->where('status', '=', 'student');
                    break;
                case 2:
                    $this->queryModel->where('status', '=', 'academic');
                    break;
                case 3:
                    $this->queryModel->where('status', '=', 'inArmy');
                    break;
                case 4:
                    $this->queryModel->whereIn('status', ['academic', 'inArmy']);
                    break;
            }
        }
    }

    public function paginate($perPage = 15)
    {
        try {
            $students = $this->queryModel->paginate($perPage);
            $data = $students->map(function ($student) {
                return (new $this->class($student))->buildOnTable();
            });

            return new JsonResponse([
                'total' => $students->total(),
                'students' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'errors' => [$exception->getMessage()],
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function getByIds(array $ids)
    {
        $students = $this->model::query()->onlyStudents()->orderBy('lastname')->orderBy('firstname')->find($ids);

        return $students->map(function ($student) {
            return (new $this->class($student));
        });
    }

    public function getById($id): Student
    {
        $student = $this->queryModel->find($id);
        if ($student) {
            return (new $this->class($student));
        }

        throw (new CustomModelNotFoundException())->setModel($this->model, $id, 'Не найден студент с идентификатором');
    }

    public function getOnlyTeachStudents()
    {
        $students = $this->queryModel->get();

        return $students->map(function ($student) {
            return (new $this->class($student));
        });
    }

    public function updatePersonal($student_id, $input)
    {
        try {

            if (Model::checkUnique($input)) {
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Студент с такими данными уже существует'
                ], 422);
            }

            $this->update($student_id, $input);
            return new JsonResponse([
                'success' => true,
                'message' => 'Персональная информация была успешно обновлена'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function update($student_id, $data)
    {
        DB::beginTransaction();
        try {
            $student = $this->queryModel->find($student_id);
            $student->update($data);

            DB::commit();

            return ['success' => true];
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function updatePassport($id, $input)
    {
        try {
            $user = $this->model::query()->findOrFail($id);
            $user->passport_issuance_date = $input['issuanceDate'];
            $user->passport_data = $input;
            $user->save();
            $this->cacheService->putSubdivisionCodesCache(['code' => $input['subdivisionCode'], 'issued' => $input['issued']]);
            $this->cacheService->putCache('birthPlaces', $input['birthPlace']);

            return new JsonResponse([
                'success' => true,
                'message' => 'Данные паспорта были успешно обновлены'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    /**
     * @param int $id
     * @param array $input
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateEducation(int $id, array $input)
    {

        try {
            $user = $this->model::query()->findOrFail($id);
            $user->education_id = $input['education_id'];
            $user->has_original = $input['has_original'];
            $user->graduation_organization_name = $input['graduation_organization_name'];
            $user->graduation_organization_place = $input['graduation_organization_place'];
            $user->graduation_date = $input['graduation_date'];
            /** @var TYPE_NAME $user */
            $user->avg = $input['avg'];
            $user->ratings_map = $input['ratings_map'];
            $user->save();

            $this->cacheService->putCache('educationalOrganizations', $input['graduation_organization_name']);
            $this->cacheService->putCache('locationsOfEducationalInstitutions', $input['graduation_organization_place']);

            return new JsonResponse([
                'success' => true,
                'message' => 'Данные образования были успешно обновлены'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    /**
     * @param int $id
     * @param array $input
     */
    public function moveToGroup(int $student_id, array $input)
    {
        try {
            $student = Model::query()->findOrFail($student_id);
            $group = \App\Models\TrainingGroup::query()->findOrFail($input['group_id']);
            $decree = $this->getDecree($input, Decree::$enrollmentType);

            $student->update([
                'group_id' => $group->id,
                'profession_program_id' => $group->profession_program_id
            ]);


            $this->attachDecree($student, $decree->id, [
                'type' => 'enrollment',
                'group_to' => $group->full_name,
                'reason' => $input['reason'] ?? null
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function updateAddresses(int $student_id, array $addresses)
    {
        try {
            $student = Model::query()->findOrFail($student_id);

            if ($addresses['isAddressSimilar']) {
                $addresses['residential'] = $addresses['registration'];
            }
            if ($addresses['isAddressPlaceOfStay']) {
                $addresses['place_of_stay'] = $addresses['registration'];
            }

            $student->addresses = $addresses;
            $student->save();
            $keys = [
                'registration',
                'residential',
                'place_of_stay'
            ];

            return new JsonResponse([
                'success' => true,
                'message' => 'Адреса были успешно обновлены'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function createStudent(array $input)
    {
        DB::beginTransaction();
        try {
            $student = Model::query()->create($input);

            $student_id = $student->id;

            \activity('student')
                ->performedOn($student)
                ->withProperties(['attributes' => collect($input)->forget(['education', 'passport', 'enrollment_information', 'addresses'])])
                ->tap(function (Activity $activity) {
                    $activity->event = 'created';
                })
                ->log('Студент добавлен');
            sleep(3);
            $this->updateEducation($student_id, $input['education']);
            $this->updatePassport($student_id, $input['passport']);
            $this->moveToGroup($student_id, $input['enrollment_information']);
            $this->updateAddresses($student_id, $input['addresses']);

            $this->cacheService->refreshStudentDataCache();
            $this->cacheService::refreshStudentsCount();
            DB::commit();
            return new JsonResponse([
                'success' => true,
                'message' => "Студент был успешно добавлен, ID " . $student_id
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function addDecree(int $student_id, array $input)
    {
        try {
            $student = Model::query()->findOrFail($student_id);
            $group = \App\Models\TrainingGroup::query()->findOrFail($input['group_id']);
            $decree = $this->getDecree($input, Decree::$enrollmentType);

            $this->attachDecree($student, $decree->id, [
                'type' => 'enrollment',
                'group_to' => $group->full_name,
                'reason' => $input['reason'] ?? null
            ]);
            $decrees = (new Student($student))->getDecrees();

            return new JsonResponse([
                'success' => true,
                'message' => 'Приказ был успешно добавлен',
                'decrees' => $decrees
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function attachDecree($studentModel, int $decree_id, array $decree_params)
    {
        try {
            $studentModel->decrees()->attach([$decree_id => $decree_params]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function getDecree($input, $type)
    {
        try {
            return Decree::query()->firstOrCreate([
                'number' => $input['decree_number'],
                'date' => $input['decree_date'],
                'term' => getCurrentPeriod($input['decree_date']),
                'type' => $type
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function retireStudents(array $input)
    {
        try {
            $decreeType = $input['reason'] == 'endCollege' ? Decree::$graduationType : Decree::$allocationType;

            $decree = $this->getDecree($input, $decreeType);

            $specialReasons = ['academic', 'inArmy'];

            if (in_array($input['reason'], $specialReasons)) {
                $status = $input['reason'];
            } elseif ($input['reason'] == 'endCollege') {
                $status = Model::$graduationStatus;
            } else {
                $status = Model::$expelledStatus;
            }

            $groups = TrainingGroupModel::query()->onlyTeach()->with('currentAcademicYear')->get()
                ->keyBy('id')
                ->map(function ($group) {
                    return $group->full_name;
                })
                ->toArray();

            $students = Model::query()->findOrFail($input['ids']);

            foreach ($students as $student) {
                if (!is_null($student->reinstate_group_id) && ($decreeType == Decree::$graduationType)) {
                    (new Student($student))->transferInReinstateGroup();

                } else {
                    $student->status = $status;
                    $student->save();
                    $this->attachDecree($student, $decree->id, ['type' => $decreeType, 'reason' => $input['reason'], 'group_from' => $groups[$student->group_id]]);
                }
            }

            $this->cacheService->refreshStudentDataCache();
            $this->cacheService::refreshStudentsCount();
            return new JsonResponse([
                'success' => true,
                'message' => 'Студенты были успешно отчислены',
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function transferStudents($input)
    {
        try {

            $decree = $this->getDecree($input, Decree::$transferType);

            $groups = TrainingGroupModel::query()->get()
                ->keyBy('id')
                ->toArray();

            $students = Model::query()->findOrFail($input['ids']);

            foreach ($students as $student) {
                $oldGroupId = $student->group_id;
                $student->group_id = $input['group_id'];
                $student->profession_program_id = $groups[$input['group_id']]['profession_program_id'];
                $student->save();
                $this->attachDecree($student, $decree->id, [
                    'type' => 'transfer',
                    'reason' => null,
                    'group_from' => $groups[$oldGroupId]['full_name'],
                    'group_to' => $groups[$input['group_id']]['full_name']
                ]);

            }
            return new JsonResponse([
                'success' => true,
                'message' => 'Студенты были успешно переведены',
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function reinstateStudents(array $input)
    {
        try {
            $decree = $this->getDecree($input, Decree::$reinstateType);

            $groups = TrainingGroupModel::query()->get()
                ->keyBy('id')
                ->toArray();

            $students = Model::query()->findOrFail($input['ids']);

            foreach ($students as $student) {
                $student->group_id = $input['group_id'];
                $student->status = 'student';
                $student->profession_program_id = $groups[$input['group_id']]['profession_program_id'];
                $student->save();
                $this->attachDecree($student, $decree->id, [
                    'type' => 'reinstate',
                    'reason' => null,
                    'group_to' => $groups[$input['group_id']]['full_name']
                ]);

            }
            return new JsonResponse([
                'success' => true,
                'message' => 'Студенты были успешно восстановлены',
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function massPrintMilitaryReferences(int $recruit_center_id)
    {
        try {
            $students = $this->model::query()
                ->where('long_absent', false)
                ->where('status', 'student')
                ->where('gender', 0)
                ->where('military_status', 'Recruit')
                ->where('recruitment_center_id', $recruit_center_id)
                ->orderBy('lastname')
                ->get()
                ->map(function ($student) {
                    $student = (new Student($student));
                    $group = $student->getTrainingGroup();
                    $decree = $student->getDecrees()->last();
                    return [
                        'full_name' => $student->getFullName(),
                        'birthday' => $student->getBirthday(),
                        'course' => $student->getCourse(),
                        'education_id' => $student->getEducationId(),
                        'start_date' => $group->getEducationPlan()->getStartTraining(),
                        'end_date' => $group->getEducationPlan()->getEndTraining(),
                        'program' => $group->getProgram()->getName(),
                        'recruitment_center_name' => ($student->getRecruitmentCenter())['name'],
                        'decree' => \App\AppLogic\Decree\Decree::buildById($decree['decree_id'])
                    ];
                });

            if ($students->count() === 0) {
                return response()->json([
                    'success' => false,
                    'errors' => ['К выбранному военкомату не прикреплен ни один студент']
                ])->setStatusCode(422);
            }
            $pdf = PDF::loadView('references.military',
                [
                    'withStyle' => true,
                    'students' => $students,
                ]
            );
            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function buildStudentsPdfProfiles($students)
    {
        ini_set("pcre.backtrack_limit", "500000000");
        $pdf = PDF::loadView('export.student.student-profiles',
            [
                'students' => $students,
            ]
        );

        return $pdf;
    }

    public function exportAsuRsoTableByStudentIds($ids)
    {
        $studentsIds = is_array($ids) ? $ids : (array)$ids;

        $students = $this->queryModel->findOrFail($studentsIds);

        $studentToArray = $students->map(function ($student) {
            return new $this->class($student);
        });

        return (new ExportStudentsToAsuRso($studentToArray))->download('Выгрузка_студентов_в_АСУРСО.csv', Excel::CSV);
    }

    public function exportParentsToAsuRso($parents)
    {
        $parentsToHandler = $parents->map(function ($parent) {
            return new UserParent($parent);
        });

        return (new ExportParentsToAsuRso($parentsToHandler))->download('Выгрузка_родителей_в_АСУРСО.csv', Excel::CSV);
    }

    public function makeProfile($id)
    {
        try {
            $student = $this->queryModel->findOrFail($id);

            $studentToArray = [
                (new $this->class($student))
            ];

            $pdf = $this->buildStudentsPdfProfiles($studentToArray);

            return $pdf->stream();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }
}
