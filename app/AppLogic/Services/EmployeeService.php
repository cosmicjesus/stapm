<?php

namespace App\AppLogic\Services;

use App\AppLogic\Base\BaseService;
use App\Models\Employee as Model;
use App\Models\QualificationCourse;
use App\AppLogic\Employee\Employee;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EmployeeService extends BaseService
{

    protected $queryModel;
    protected $class;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()
            ->orderBy('lastname', 'ASC')
            ->orderBy('firstname', 'ASC');
        $this->class = Employee::class;

    }

    function model()
    {
        return Model::class;
    }

    public function filter($params)
    {
        $filterParams = json_decode($params, true);

        list('category_id' => $category_id, 'position_id' => $position_id, 'lastname' => $lastname) = $filterParams;

        $this->onlyWorks();

        $this->filterByCategoryId($category_id);

        $this->filterByPositionId($position_id);

        $this->filterByLastname($lastname);

        return $this;
    }

    public function onlyWorks()
    {
        $this->queryModel->where('status', 'work');
        return $this;
    }

    public function onlyTeachers()
    {
        $this->queryModel->whereHas('positions', function ($q) {
            $q->where('position_type_id', 2);
        });

        return $this;
    }

    public function onlyFullTime()
    {
        $this->queryModel->whereHas('positions', function ($q) {
            $q->whereIn('type', [0, 1]);
        });
        return $this;
    }

    public function onlyAdministration()
    {
        $this->queryModel->whereHas('administrator', function ($q) {
            $q->orderBy('sort');
        });

        return $this;
    }

    protected function filterByCategoryId($category_id)
    {
        $this->queryModel->when(filter_var($category_id, FILTER_VALIDATE_INT), function ($q, $category_id) {
            $q->where('category', $category_id);
        });
    }

    protected function filterByPositionId($position_id)
    {
        $this->queryModel->when(filter_var($position_id, FILTER_VALIDATE_INT), function ($q, $position_id) {
            $q->whereHas('positions', function ($q) use ($position_id) {
                return $q->where('position_id', '=', $position_id)->whereNull('date_layoff');
            });
        });
    }

    protected function filterByLastname($lastname)
    {
        $this->queryModel->when($lastname, function ($q, $lastname) {
            $q->like('lastname', $lastname);
        });
    }

    public function get()
    {
        try {
            $employees = $this->queryModel->get();
            $data = $employees->map(function ($employee) {
                return (new $this->class($employee));
            });

            return $data;
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function all()
    {
        try {
            $this->onlyWorks();
            $employees = $this->get()->map(function ($employee) {
                return $employee->buildOnTable();
            });

            return responder()->success($employees)->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(),$exception->getMessage())->respond(500);
        }
    }

    public function getById($id)
    {
        try {
            $employee = parent::getById($id);

            $employeeProfile = (new $this->class($employee))->full();

            return response()->json($employeeProfile);

        } catch (\Exception $exception) {

            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);

        }
    }

    public function getBySlug(string $slug)
    {
        try {
            $user = $this->model->findBySlugOrFail($slug);

            return (new $this->class($user))->full();
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Сотрудник не найден');
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function paginate($perPage = 15)
    {
        try {
            $employees = $this->queryModel->paginate($perPage);
            $data = $employees->map(function ($employee) {
                return (new $this->class($employee))->buildOnTable();
            });

            return response()->json([
                'total' => $employees->total(),
                'employees' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function addEmployee($input)
    {
        try {

            if (!Model::checkUnique($input)) {
                return response()->json([
                    'message' => 'Сотрудник с такими ФИО уже существует',
                ])->setStatusCode(500);
            }

            $employee = Model::query()->create($input);
            $employee_id = $employee->id;
            $this->attachEducation($employee_id, $input['education']);
            $this->attachPosition($employee_id, $input['position']);

            return response()->json([
                'message' => 'Сотрудник был успешно добавлен',
                'position' => $input['position'],
                'education' => $input['education'],
                'input' => $input
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function updateInfo($employee_id, $input)
    {
        try {

            if (!Model::checkUnique($input)) {
                return response()->json([
                    'message' => 'Сотрудник с такими ФИО уже существует',
                ])->setStatusCode(500);
            }

            $model = $this->queryModel->findOrFail($employee_id);

            $model->update($input);
            return response()->json([
                'message' => 'Информация была успешно обновлена',
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function attachPosition($employee_id, $input)
    {
        try {
            $model = $this->queryModel->with('positions')->findOrFail($employee_id);
            $model->positions()->attach($input['position_id'], [
                    'start_date' => $input['start_date'],
                    'date_layoff' => isset($input['date_layoff']) ? $input['date_layoff'] : null,
                    'type' => $input['type'],
                    'experience' => 0,
                ]
            );
            $positions = (new $this->class($model))->getPositions();
            return response()->json([
                'message' => 'Должность была успешно добавлена',
                'positions' => $positions
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function attachEducation($employee_id, $input)
    {
        try {
            $model = $this->queryModel->with('educations')->findOrFail($employee_id);
            $model->educations()->attach($input['education_id'], [
                    'qualification' => $input['qualification'],
                    'direction_of_preparation' => $input['direction_of_preparation'],
                    'graduation_organization_name' => $input['graduation_organization_name']
                ]
            );
            $educations = (new $this->class($model))->getEducations();
            return response()->json([
                'message' => 'Уровень образования был успешно добавлен',
                'educations' => $educations
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }

    }

    public function attachSubject($employee_id, $input)
    {
        try {
            $model = $this->queryModel->with('subjects')->findOrFail($employee_id);
            $model->subjects()->attach($input['subject_id']);
            $subjects = (new $this->class($model))->getSubjects();
            return response()->json([
                'message' => 'Дисциплина была успешно добавлена',
                'subjects' => $subjects
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function addCourse($employee_id, $input)
    {
        try {
            $employee = $this->queryModel->with('courses')->findOrFail($employee_id);

            $courseModel = new QualificationCourse([
                'title' => $input['title'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'count_hours' => $input['hours'],
                'description' => null
            ]);


            $course = $employee->courses()->save($courseModel);

            return response()->json([
                'message' => 'Курсы были успешно добавлены',
                'id' => $course->id
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }
}
