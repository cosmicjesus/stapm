<?php

namespace App\AppLogic\Services;


use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\ProfessionProgram;
use App\Models\Student;
use App\Models\StudentParent;
use App\Models\TrainingGroup;
use Cache;
use function foo\func;

class CacheService
{

    public function all()
    {
        $cache = collect();
        $cache->put('subdivision_codes', $this->getFormatSubdivisionCodes());

        return $cache;
    }

    public function getSubdivisionCodes()
    {
        return Cache::get('subdivision_codes') ?? [];
    }

    public function getFormatSubdivisionCodes()
    {
        $codes = $this->getSubdivisionCodes();

        $converted = [];

        foreach ($codes as $key => $item) {
            foreach ($item as $value) {
                $converted[] = [
                    'code' => $key,
                    'issued' => $value
                ];
            }
        }

        return collect($converted)->sortBy('code')->values()->toArray();
    }

    public function putSubdivisionCodesCache($params)
    {
        $codes = $this->getSubdivisionCodes();

        if (array_key_exists($params['code'], $codes)) {
            if (!in_array($params['issued'], $codes[$params['code']])) {
                $codes[$params['code']][] = $params['issued'];
            }
        } else {
            $codes[$params['code']] = [];
            $codes[$params['code']][] = $params['issued'];
        }

        Cache::forever('subdivision_codes', $codes);
    }

    public function updateSubdivisionCodes($codes)
    {
        Cache::forever('subdivision_codes', $codes);
        return [
            'success' => true
        ];
    }

    public function getEducationalOrganizations()
    {
        return Cache::get('educationalOrganizations') ?? [];
    }

    public function getEducationalInstitutions()
    {
        return Cache::get('locationsOfEducationalInstitutions') ?? [];
    }

    public function putCache($key, $value)
    {
        if (!is_null($value)) {
            $item = Cache::get($key) ?? [];
            $cache = collect($item);

            $sorted = $cache->push($value)->unique()->values()->toArray();

            Cache::forever($key, $sorted);
        }
    }

    public function getFirstNames()
    {
        return Cache::get('studentFirstNames') ?? [];
    }

    public function getMiddleNames()
    {
        return Cache::get('studentMiddleNames') ?? [];
    }

    public function getPersonalFormData()
    {
        return collect([
            'firstnames' => $this->getFirstNames(),
            'middlenames' => $this->getMiddleNames()
        ]);
    }

    public function getBirthPlaces()
    {
        return Cache::get('birthPlaces');
    }

    public function getAddressesDataCache()
    {
        $keys = [
            'regions',
            'streets',
            'cities',
            'settlement',
            'area'
        ];
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = Cache::get($key) ?? [];
        }
        return $data;
    }

    public function getStudentsCount()
    {

        $count = Cache::get('students_count');

        if ($count) {
            return $count;
        }

        return self::refreshStudentsCount();
    }

    public function getEmployeesCount()
    {
        $count = Cache::get('employees_count');

        if ($count) {
            return $count;
        }

        return self::refreshEmployeesCount();
    }

    public function getEnrolleesCount()
    {

        $count = Cache::get('enrollees_count');

        if ($count) {
            return $count;
        }

        return self::refreshEnrolleesCount();
    }

    public function getParentsCount()
    {
        $count = Cache::get('parentsCount');

        if ($count) {
            return $count;
        }

        return self::refreshParentsCount();
    }

    public function getTrainingGroupsCount()
    {
        $count = Cache::get('trainingGroupsCount');

        if ($count) {
            return $count;
        }

        return self::refershTrainingGroupsCount();
    }

    public function getProgramsCount()
    {
        $count = Cache::get('programsCount');

        if ($count) {
            return $count;
        }

        return self::refershProgramsCount();
    }

    public function educationsPlansCount()
    {
        $count = Cache::get('educationsPlansCount');

        if ($count) {
            return $count;
        }

        return self::refreshPlansCount();
    }

    public static function refreshPlansCount()
    {
        $count = EducationPlan::query()->where('active', true)->count();

        Cache::forever('educationsPlansCount', $count);

        return $count;
    }

    public static function refreshStudentsCount()
    {
        $count = Student::query()->whereIn('status', ['student', 'academic', 'inArmy'])->count();

        Cache::forever('students_count', $count);

        return $count;
    }

    public static function refreshEmployeesCount()
    {
        $count = Employee::query()->where('status', 'work')->count();
        Cache::forever('employees_count', $count);

        return $count;
    }

    public static function refreshEnrolleesCount()
    {
        $count = Student::query()->where('status', 'enrollee')->count();

        Cache::forever('enrollees_count', $count);

        return $count;
    }

    public static function refreshParentsCount()
    {
        $count = StudentParent::query()->count();

        Cache::forever('parentsCount', $count);

        return $count;
    }

    public static function refershTrainingGroupsCount()
    {
        $count = TrainingGroup::query()->where('status', 'teach')->count();

        Cache::forever('trainingGroupsCount', $count);

        return $count;
    }

    public static function refershProgramsCount()
    {
        $count = ProfessionProgram::query()->count();

        Cache::forever('programsCount', $count);

        return $count;
    }

    private static function getStudentData($field)
    {
        return Student::query()->select($field)
            //->whereNotIn('status', ['expelled', 'enrollee'])
            ->whereNotNull($field)
            ->orderBy($field)
            ->distinct()
            ->get()
            ->map(function ($item) use ($field) {
                return $item->$field;
            });
    }

    public function refreshStudentDataCache()
    {
        $data = [
            [
                'field' => 'firstname',
                'cache_field_name' => 'studentFirstNames'
            ],
            [
                'field' => 'middlename',
                'cache_field_name' => 'studentMiddleNames'
            ],
            [
                'field' => 'lastname',
                'cache_field_name' => 'studentLastNames'
            ]
        ];

        foreach ($data as $item) {
            $resourse = self::getStudentData($item['field']);
            Cache::forever($item['cache_field_name'], $resourse);
        }
    }
}