<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\EducationPlan\EducationPlan;
use App\AppLogic\FileComponent\FileComponent;
use App\AppLogic\Pdf\PDF;
use App\AppLogic\Storage\EducationPlanStorage;
use App\AppLogic\Storage\StorageFiles;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\ComponentDocument;
use App\Models\EducationPlan as Model;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class EducationPlanService extends BaseService
{
    protected $queryModel;
    protected $storage;


    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::with('professionProgram');
        $this->storage = new EducationPlanStorage();
    }

    function model()
    {
        return Model::class;
    }

    public function filter($filter)
    {
        $this->queryModel->where('active', true);
        $params = json_decode($filter, true);
        $this->queryModel->when(filter_var($params['profession_program_id'], FILTER_VALIDATE_INT), function ($q, $param) {
            return $q->where('profession_program_id', $param);
        });

        return $this;
    }

    protected function filterByProfessionProgramId($program_id)
    {
        $this->queryModel->where('profession_program_id', $program_id);

        return $this;
    }

    public function paginate($perPage = 15)
    {
        $plans = $this->queryModel->orderBy('profession_program_id')->orderBy('start_training')->paginate($perPage);
        $data = $plans->map(function ($plan) {
            return (new EducationPlan($plan))->buildOnResponse();
        });

        return [
            'total' => $plans->total(),
            'data' => $data
        ];
    }

    public function convertExtraOptions($options)
    {
        $convertOptions = collect();
        foreach ($options as $key => $option) {
            $convertOptions->put($key, filter_var($option, FILTER_VALIDATE_BOOLEAN));
        }

        return $convertOptions;
    }

    public function create($input)
    {
        DB::beginTransaction();
        $file_path = null;
        try {

            if ($input->file('file')) {
                $file_path = StorageFiles::putEducationPlan($input->file('file'), $input['profession_program_id']);
            }
            $this->model->create([
                'profession_program_id' => $input['profession_program_id'],
                'name' => $input['name'],
                'start_training' => $input['start_training'],
                'end_training' => $input['end_training'],
                'count_period' => $input['count_period'],
                'active' => filter_var($input['active'], FILTER_VALIDATE_BOOLEAN),
                'extra_options' => $this->convertExtraOptions($input['options']),
                'full_path' => $file_path,
                'in_revision' => false,
            ]);
            CacheService::refreshPlansCount();
            DB::commit();
            return ['success' => true, 'message' => 'Учебный план был успешно добавлен'];
        } catch (\Exception $exception) {
            DB::rollBack();
            StorageFiles::deleteProgramDocument($file_path);
            return ['message' => $exception->getMessage()];
        }
    }

    public function update($input)
    {
        DB::beginTransaction();
        try {
            $model = $this->model->findOrFail($input['id'])->update($input);
            DB::commit();
            return ['success' => true];
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function all()
    {
        try {
            $plans = $this->queryModel
                ->where('active', true)
                ->orderBy('profession_program_id')
                ->orderBy('start_training')
                ->get()
                ->map(function ($plan) {
                    return (new EducationPlan($plan))->buildOnResponse();
                });

            return responder()->success($plans)->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage())
                ->respond();
        }
    }

    public function getOne($id)
    {
        try {
            $plan = $this->getById($id);
            return (new EducationPlan($plan))->full();
        } catch (ModelNotFoundException $exception) {
            throw new CustomModelNotFoundException('Не найден УП с заданным ID');
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function refreshFile($id, $input, $file)
    {
        try {
            $plan = Model::query()->findOrFail($id);
            $oldFile = $plan->full_path;

            $newFile = StorageFiles::putEducationPlan($file, $input['program_id'], $oldFile);
            $plan->full_path = $newFile;
            $plan->save();

            return ['success' => true, 'message' => 'Учебный план был успешно обновлен'];

        } catch (\Exception $exception) {
            return ['line' => $exception->getLine(), 'message' => $exception->getMessage()];
        }
    }

    public function delete($id)
    {
        try {
            $plan = Model::query()->with('training_groups')->findOrFail($id);
            if ($plan->training_groups->count()) {
                return [
                    'success' => false,
                    'message' => 'Невозможно удалить учебный план, т.к имеются группы прикрепленные к нему'
                ];
            }

            $plan->delete();

            $file = $plan->full_path;
            if (!is_null($file)) {
                StorageFiles::deleteProgramDocument($file);
            }
            CacheService::refreshPlansCount();
            return [
                'success' => true,
                'message' => 'Учебный план был успешно удален'
            ];

        } catch (\Exception $exception) {
            return ['line' => $exception->getLine(), 'message' => $exception->getMessage()];
        }
    }

    public function getPlansByProgramId($program_id)
    {
        try {
            $this->filterByProfessionProgramId($program_id);

            $plans = $this->queryModel->get()->map(function ($plan) {
                return (new EducationPlan($plan))->buildOnResponse();
            });

            return responder()->success($plans)->respond();
        } catch (\Exception $exception) {

        }
    }

    public function buildFile(int $id)
    {
        try {
            $model = $this->getById($id);
            $componentPaths = (new EducationPlan($model))->getComponentsPaths();

            $newPlan = PDF::mergeFiles($componentPaths);

            $path = $this->storage->putRawPlan($model->profession_program_id, $newPlan, $model->full_path);
            $model->update(['full_path' => $path]);
            return [
                'success' => true,
                'path' => $this->storage->getFile($path)
            ];

        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function createComponent($input)
    {
        DB::beginTransaction();
        try {
            $path = $this->storage->putComponentFile($input, $input['file']);
            $model = new ComponentDocument([
                'name' => $input['name'],
                'sort' => $input['sort'],
                'path' => $path
            ]);
            $educationPlanModel = $this->queryModel->findOrFail($input['id']);
            $educationPlanModel->components()->save($model);
            DB::commit();

            $formatComponent = (new FileComponent($model))->full();

            return ['success' => true, 'component' => $formatComponent];
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}