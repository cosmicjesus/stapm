<?php


namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\ReplaceClass\ReplaceClass;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\ReplacingClass;
use App\Models\ReplacingClassesImage;
use DB;
use Intervention\Image\Exception\NotFoundException;
use QCod\ImageUp\Exceptions\InvalidUploadFieldException;

class ReplaceClassService extends BaseService
{

    protected $queryModal;
    protected $handlerClass;


    public function __construct()
    {
        parent::__construct();
        $this->queryModal = $this->model::query();
        $this->handlerClass = ReplaceClass::class;
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return ReplacingClass::class;
    }

    public function paginate($perPage = 15)
    {
        try {
            $items = $this->queryModal->orderByDesc('date_of_replacing')->paginate($perPage);
            $data = $items->map(function ($employee) {
                return (new $this->handlerClass($employee))->full();
            });

            return response()->json([
                'total' => $items->total(),
                'items' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function create($input)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->create($input);

            foreach ($input['files'] as $key => $file) {
                $this->attachFile($model->id, $file, ($key + 1));
            }
            DB::commit();
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function getReplaceByDate($date)
    {
        try {
            $replaceClass = $this->queryModal->whereDate('date_of_replacing', $date)->first();

            if (is_null($replaceClass)) {
                $replaceClass = $this->model::query()->whereDate('date_of_replacing', '>', $date)->orderBy('date_of_replacing')->first();
                if (is_null($replaceClass)) {
                    throw new CustomModelNotFoundException('Не найдена замена');
                }
            }

            $replace = (new $this->handlerClass($replaceClass))->full('id');

            return response()->json([
                'success' => true,
                'data' => $replace
            ]);
        } catch (NotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Не найдены замены на указанную дату'
            ]);
        } catch (CustomModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Не найдены замены на указанную дату',
                'ss' => $replaceClass
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function attachFile($model_id, $file, $index = 500)
    {
        try {
            $model = ReplacingClassesImage::query()->create([
                'replacing_class_id' => $model_id,
                'sort' => $index,
            ]);

            $model->uploadImage($file);
        } catch (InvalidUploadFieldException $e) {
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}