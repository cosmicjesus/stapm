<?php


namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\Models\Student;
use App\Models\StudentParent;
use App\Models\UserParent;

class UserActionsService extends BaseService
{

    protected $queryModel;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query();
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Student::class;
    }

    public function updatePassportData(int $user_id, array $input)
    {
        $user = $this->queryModel->findOrFail($user_id);

        $user->passport_issuance_date = $input['issuanceDate'];
        $user->nationality = $input['nationality'];
        unset($input['nationality']);
        $user->passport_data = $input;
        $user->save();

        return [
            'success' => true,
            'message' => 'Данные паспорта были успешно обновлены'
        ];
    }

    public function updateAddressesData($user_id, $addresses)
    {
        $user = $this->queryModel->findOrFail($user_id);
        if ($addresses['isAddressSimilar']) {
            $addresses['residential'] = $addresses['registration'];
        }
        if ($addresses['isAddressPlaceOfStay']) {
            $addresses['place_of_stay'] = $addresses['registration'];
        }

        $user->update(['addresses' => $addresses]);

        return [
            'success' => true,
            'message' => 'Адреса были успешно обновлены'
        ];
    }


    public function checkParentsAndReturnId($parent)
    {
        $parentModel = UserParent::query()->firstOrCreate($parent);
        return $parentModel->id;
    }

    public function attachParents($user_id, $parents)
    {
        $parentIds = [];

        foreach ($parents as $parent) {
            $parentIds[] = $this->checkParentsAndReturnId($parent);
        }

        $userModel = $this->model::query()->findOrFail($user_id);
        $userModel->parents()->attach($parentIds);

        return [
            'success' => true,
            'message' => 'Родители были успешно добавлены'
        ];
    }
}