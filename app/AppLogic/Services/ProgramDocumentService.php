<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\ProfessionProgram\ProgramFiles;
use App\AppLogic\Storage\ComponentDocumentStorage;
use App\AppLogic\Storage\StorageFiles;
use App\Models\ProfessionProgram;
use App\Models\ProgramBasicDocument;
use App\Models\ProgramDocument;
use App\Models\ProgramSubject;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProgramDocumentService extends BaseService
{
    protected $files;

    public function __construct()
    {
        parent::__construct();

        $this->files = [
            [
                'name' => 'Рабочая программа',
                'file' => null,
                'active' => true
            ],
            [
                'name' => 'Рабочая программа [исходник]',
                'file' => null,
                'active' => false
            ],
            [
                'name' => 'КОС',
                'file' => null,
                'active' => false
            ],
            [
                'name' => 'Методические указания для ЛПЗ',
                'file' => null,
                'active' => false
            ],
            [
                'name' => 'Методические указания для практических работ',
                'file' => null,
                'active' => false
            ]
        ];
    }

    function model()
    {
        return ProgramDocument::class;
    }

    public function AddDocumentInSubject($request)
    {
        try {
            $file = $this->createDocumentRecord($request);

            return [
                'id' => $file->id,
                'name' => $file->title,
                'url' => StorageFiles::getProgramDocument($file->path),
                'updated_at' => $file->updated_at->format('Y-m-d H:i:s'),
                'components' => []
            ];

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function createDocumentRecord($input)
    {
        $model = ProgramSubject::with('files')->find($input['subject_id']);

        $path = null;

        if ($input['file'] instanceof UploadedFile) {
            $path = $this->handleUploadFile($input['file'], $input['profession_program_id']);
        }

        $file = new ProgramDocument([
            'title' => $input['name'],
            'path' => $path,
            'active' => $input['active']
        ]);

        $file = $model->files()->save($file);


        return $file;
    }

    public function handleUploadFile($file, $program_id, $oldFile = null)
    {
        try {
            return StorageFiles::putProgramDocument($file, $program_id, $oldFile);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function deleteFile($id)
    {
        try {
            $document = ProgramDocument::query()->with('components')->findOrFail($id);

            foreach ($document->components as $component) {
                (new ComponentDocumentStorage())->deleteFile($component->path);
                $component->delete();
            }

            $path = $document->path;
            $document->delete();
            StorageFiles::deleteProgramDocument($path);
            return ['success' => true, 'message' => 'Документ был успешно удален'];
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $request
     * @param $id
     * @return array|string
     * @throws \Exception
     */
    public function updateDocument(Request $request, $id, $profession_program_id = null)
    {

        try {
            $document = ProgramDocument::query()->with('programtagle')->findOrFail($id);
            $path = $document->path;
            if (is_null($profession_program_id)) {
                $profession_program_id = $document->programtagle->id;
            }
            //$program_id = $document->programtagle_id;

            $newPath = $this->handleUploadFile($request->file('file'), $profession_program_id, $path);

            $document->path = $newPath;
            $document->save();

            return [
                'url' => StorageFiles::getProgramDocument($newPath),
                'message' => 'Документ был успешно обновлен',
                'updated_at' => $document->updated_at->format('Y-m-d H:i:s')
            ];
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'message' => "Не найден документ с индентификатором {$id}",
                'exception' => 'ModelNotFoundException'
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    protected function checkSubjectInProgram($input)
    {
        return ProgramSubject::checkSubjectInProgram($input['program_id'], $input['subject_id']);
    }

    public function createDocumentRecords($params)
    {
        foreach ($this->files as $file) {
            $this->createDocumentRecord(array_merge($file, ['subject_id' => $params['id'], 'profession_program_id' => $params['profession_program_id']]));
        }
    }

    public function addSubject($input)
    {
        try {
            $subject = ProgramSubject::createAndReturn($input);

            $this->createDocumentRecords($subject);

            return ['success' => true, 'subject' => $subject];

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function deleteSubject($profession_program_id, $subject_id)
    {
        try {
            $subject = ProgramSubject::with('files')->findOrFail($subject_id);

            if ($subject->files->count() > 0) {
                $ids = [];
                $files = [];
                foreach ($subject->files as $file) {
                    $ids[] = $file->id;
                    $files[] = $file->path;
                }
                ProgramDocument::destroy($ids);
                StorageFiles::deleteProgramDocument($files);
            }
            $subject->delete();
            return ['success' => true, 'message' => 'Дисциплина была успешно удалена'];
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function updateBasic($id, $file, $type)
    {
        try {
            $model = ProgramBasicDocument::query()->findOrFail($id);

            $path = StorageFiles::putProgramDocument($file, $model->profession_program_id, $model->$type);

            $model->update([$type => $path]);

            return [
                'success' => true,
                'message' => 'Документ был успешно обновлен',
                'url' => StorageFiles::getProgramDocument($path)
            ];
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function addAdditionalDocument($program_id, $request)
    {
        try {
            $path = $this->handleUploadFile($request->file('file'), $program_id);
            $program = ProfessionProgram::with('otherDocuments')->findOrFail($program_id);
            $documentModel = new ProgramDocument([
                'title' => $request->type,
                'type' => $request->type,
                'start_date' => $request->date,
                'path' => $path
            ]);

            $file = $program->otherDocuments()->save($documentModel);
            $file = new ProgramFiles($file);
            return [
                'id' => $file->getId(),
                'name' => $file->getTitle(),
                'url' => $file->getFileUrl(),
                'date' => $file->getDate(),
                'type' => $file->getType()
            ];
        } catch (\Exception $exception) {
            return ['message' => $exception->getMessage()];
        }
    }

    public function getSubjectFilesById($program_id, $subject_id)
    {
        try {
            $model = ProgramSubject::query()->with('subject', 'files')->findOrFail($subject_id);
            $data = [
                'id' => $model->id,
                'index' => $model->index,
                'section_id' => $model->section_id,
                'name' => $model->subject->name,
                'updated_at' => $model->updated_at->format('d.m.Y H:i:s'),
                'profession_program_id' => $program_id,
                'files' => $model->files->map(function ($file) use ($program_id) {
                    $file = new ProgramFiles($file);
                    return [
                        'id' => $file->getId(),
                        'name' => $file->getTitle(),
                        'url' => $file->getFileUrl(),
                        'components' => $file->getComponents(),
                        'program_id' => $program_id,
                        'updated_at' => $file->getUpdatedDate(),
                    ];
                })
            ];
            return $data;
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function deleteSubjectFromProgram($params)
    {
        try {
            $model = ProgramSubject::query()->with('files')->findOrFail($params['subject_id']);
            $files = $model->files;

            foreach ($files as $file) {
                $this->deleteFile($file->id);
            }

            $model->delete();

            return response()->json([
                'success' => true,
                'message' => 'Успешно'
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}