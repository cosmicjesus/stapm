<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Reference\Reference;
use App\AppLogic\Student\Student;
use App\Models\Help;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PDF;
use phpDocumentor\Reflection\Types\Integer;

class ReferenceService extends BaseService
{

    protected $class;
    protected $queryModel;

    public function __construct()
    {
        parent::__construct();
        $this->class = Reference::class;
        $this->queryModel = $this->model::query()->where('status', Help::status[1])->orderBy('date_of_application', 'DESC');
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Help::class;
    }

    public function filter($params)
    {
        $filterParams = json_decode($params, true);
        list('type' => $type) = $filterParams;
        $this->queryModel->when($type, function ($q, $type) {
            return $q->where('type', $type);
        });
        return $this;
    }


    /**
     * @param $studentId
     * @param $params
     * @return mixed
     */
    public function makeTrainingReferenceByStudentID($studentId, $params)
    {
        try {
            $student = (new StudentService())->getById($studentId);
            $currentDecree = $student->getEnrollmentDecree();
            if ($currentDecree['id'] != $params['decree_id']) {
                $currentDecree = $student->getDecrees()->filter(function ($decree) use ($params) {
                    return $decree['id'] == $params['decree_id'];
                })->first();
            }
            $studentWithParams = [
                'student' => $student,
                'decree' => $currentDecree,
                'params' => [
                    'count' => array_key_exists('count', $params) ? $params['count'] : 2,
                    'start_date' => $params['start_date'],
                    'end_date' => $params['end_date'],
                    'date' => $params['date'],
                    'decree_id' => $params['decree_id'],
                    'decree' => $currentDecree['number']
                ]
            ];
            $paramsByCreateReference = array_merge($studentWithParams['params'], ['student_id' => $studentId]);

            if (array_key_exists('reference_id', $params)) {
                Help::hideReference($params['reference_id']);
                $number = $this->getNumberById($params['reference_id']);
            } else {
                $number = $this->createTrainingReference($paramsByCreateReference);
            }

            $studentWithParams['number'] = $number;

            return $this->makeTrainingReferencesList([$studentWithParams]);
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function makeReferencesByStudentIds(array $studentIds)
    {
        $studentsData = (new StudentService())->getByIds($studentIds);

        $students = [];

        foreach ($studentsData as $studentsDatum) {
            /**
             * @var $studentsDatum Student
             */
            $decree = $studentsDatum->getDecrees()->first();
            $params = [
                'student' => $studentsDatum,
                'decree' => $decree,
                'params' => [
                    'count' => 2,
                    'start_date' => $studentsDatum->getTrainingGroup()->getStartDate(),
                    'end_date' => $studentsDatum->getTrainingGroup()->getEndDate(),
                    'date' => carbon()->format('Y-m-d'),
                    'decree_id' => $decree['id'],
                    'decree' => $decree['number']
                ]
            ];
            $paramsByCreateReference = array_merge($params['params'], ['student_id' => $studentsDatum->getId()]);
            $params['number'] = $number = $this->createTrainingReference($paramsByCreateReference);
            $students[] = $params;
        }
        return $this->makeTrainingReferencesList($students);
    }

    /**
     * @param array $students
     * @return mixed
     */
    protected function makeTrainingReferencesList(array $students)
    {
        try {
            $pdf = PDF::loadView('references.training.list',
                [
                    'items' => $students
                ]
            );

            return $pdf->stream();
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function makeMilitaryReferenceByStudentId($studentId, $params)
    {
        $student = (new StudentService())->getById($studentId);
        $currentDecree = $student->getEnrollmentDecree();
        if ($currentDecree['id'] != $params['decree_id']) {
            $currentDecree = $student->getDecrees()->filter(function ($decree) use ($params) {
                return $decree['id'] == $params['decree_id'];
            })->first();
        }

        $isHasId = array_key_exists('reference_id', $params) && ($params['reference_id'] != 'null');

        $reference_id = $isHasId ? $params['reference_id'] : $this->makeMilitaryReference(['student_id' => $student->getId()]);

        $reference = Help::findOrFail($reference_id);

        $params['reference_id'] = $reference_id;
        $params['number'] = $reference->number;
        $params['date_issue'] = $reference->date_of_application->format('d.m.Y');

        $studentWithParams = [
            'student' => $student,
            'decree' => $currentDecree,
            'params' => $params
        ];

        if ($reference_id) {
            Help::hideReference($params['reference_id']);
        }

        return $this->makeMilitaryReferencesList([$studentWithParams]);
    }

    protected function makeMilitaryReferencesList(array $students)
    {
        try {
            $pdf = PDF::loadView('references.military.list',
                [
                    'items' => $students
                ]
            );

            return $pdf->stream();
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    protected function getNumberById($reference_id)
    {
        return (Help::findOrFail($reference_id))->number;
    }

    /**
     * @param array $params
     * @return int
     */
    protected function createTrainingReference(array $params): int
    {

        $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->onlyTraining()->max('number')) + 1;
        $reference = Help::create([
            'student_id' => $params['student_id'],
            'number' => $number,
            'date_of_application' => Carbon::now()->format('Y-m-d H:i:s'),
            'params' => $params,
            'type' => Help::types['training'],
            'status' => Help::status[0]
        ]);

        return $number;
    }

    public function paginate(int $perPage = 15)
    {
        try {
            /**
             * @var $references Collection
             */
            $references = $this->queryModel->paginate($perPage);
            $data = $references->map(function ($referense) {
                return (new $this->class($referense))->buildOnTable();
            });

            return new JsonResponse([
                'total' => $references->total(),
                'references' => $data
            ]);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function makeMoneyReferenceList(array $input)
    {
        try {
            $dates = [
                Carbon::createFromFormat('Y-m-d', $input['start_date'])->setTime(9, 0, 0)->format('Y-m-d H:i:s'),
                Carbon::createFromFormat('Y-m-d', $input['end_date'])->setTime(9, 0, 0)->format('Y-m-d H:i:s')
            ];
            $references = Help::query()->where('type', 'money')
                ->whereBetween('date_of_application', $dates)
                ->get();

            if ($references->count() < 1) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Нет заявок на справки о стипендии в заданный период'
                ], 500);
            }
            $referensesList = $references->map(function ($ref) {
                return (new Reference($ref))->buildOnTable();
            });

            foreach ($references as $item) {
                $item->status = 'issued';
                $item->save();
            }
            $pdf = PDF::loadView('references.export-money',
                [
                    'references' => $referensesList
                ]
            );
            return response(base64_encode($pdf->output()), 200)->withHeaders([
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="wqwq.pdf"'
            ]);


        } catch (\Exception $exception) {
            throw $exception;
        }
    }


    public function makeTrainingReference(array $params)
    {
        \DB::beginTransaction();
        try {
            $reference = new Help();
            $reference->student_id = $params['student_id'];
            $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->onlyTraining()->max('number')) + 1;
            $reference->number = $number;
            $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $reference->params = $params;
            $reference->type = Help::types['training'];
            $reference->status = 'booked';
            $reference->save();
            \DB::commit();
            return $reference->id;

        } catch (\Exception $exception) {
            \DB::rollBack();
            return $exception;
        }
    }

    public function makeMoneyReference(array $params)
    {
        \DB::beginTransaction();
        try {
            $reference = new Help();
            $reference->student_id = $params['student_id'];
            $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->onlyMoney()->max('number')) + 1;
            $reference->number = $number;
            $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $reference->params = $params;
            $reference->type = Help::types['money'];
            $reference->status = 'booked';
            $reference->save();
            \DB::commit();
            return $reference->id;
        } catch (\Exception $exception) {
            \DB::rollBack();
            return $exception;
        }

    }

    public function makeMilitaryReference(array $params)
    {
        \DB::beginTransaction();
        try {


            $reference = new Help();
            $reference->student_id = $params['student_id'];
            $number = (Help::query()->whereYear('date_of_application', Carbon::now()->format('Y'))->onlyMilitary()->max('number')) + 1;
            $reference->number = $number;
            $reference->date_of_application = Carbon::now()->format('Y-m-d H:i:s');
            $reference->params = $params;
            $reference->type = Help::types['military'];
            $reference->status = 'booked';
            $reference->save();
            \DB::commit();
            return $reference->id;
        } catch (\Exception $exception) {
            \DB::rollBack();
            return $exception;
        }
    }
}
