<?php


namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\ProfessionProgramsAcademicYear;
use App\Models\ProgramSubject;
use Carbon\Carbon;
use DB;

class ProfessionProgramAcademicYearService extends BaseService
{
    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query();
    }

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return ProfessionProgramsAcademicYear::class;
    }

    public function store($input)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->create($input);
            DB::commit();
            return response()->json([
                'success' => true,
                'id' => $model->id
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(500);
        }
    }

    public function copySubjectFromAnotherYear($input)
    {
        DB::beginTransaction();
        try {
            $subjects = ProgramSubject::query()->where([
                'profession_program_id' => $input['profession_program_id'],
                'program_academic_year_id' => $input['copy_year_id']
            ])->get();
            if ($subjects->count() == 0) {
                throw new CustomModelNotFoundException('В выбраном году нет дисциплин. Нечего копировать');
            }

            $subjectsToArray = $subjects->map(function ($subject) use ($input) {
                $now = Carbon::now()->toDateTimeString();
                $subject['program_academic_year_id'] = $input['academic_year_id'];
                unset($subject['deleted_at']);
                unset($subject['id']);
                return $subject;
            })->toArray();

            $ids = [];

            foreach ($subjectsToArray as $value) {
                $model = ProgramSubject::create($value);

                $ids[] = $model->id;
            }

            foreach ($ids as $id) {
                (new ProgramDocumentService)->createDocumentRecords(['id' => $id, 'profession_program_id' => $input['profession_program_id']]);
            }

            DB::commit();
            return response()->json([
                'success' => true,
            ]);
        } catch (CustomModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'success' => true,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(422);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()],
                'line' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function getActiveAcademicYears($profession_program_id)
    {
        try {
            $years = $this->queryModel->where(['profession_program_id' => $profession_program_id, 'active' => true])->get();

            return $years;
        } catch (\Exception $exception) {

        }
    }

    public function getActiveAcademicYearsIds($profession_program_id)
    {
        try {
            $years = $this->getActiveAcademicYears($profession_program_id);

            return $years->map(function ($year) {
                return $year->id;
            })->toArray();
        } catch (\Exception $exception) {

        }
    }
}
