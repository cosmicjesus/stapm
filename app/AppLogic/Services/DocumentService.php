<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Document\Document;
use App\AppLogic\Storage\StorageFiles;
use App\Models\CategoriesDocument;
use App\Models\Document as Model;
use Illuminate\Http\JsonResponse;


class DocumentService extends BaseService
{

    protected $queryModel;
    protected $class;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::with('categories')->orderBy('sort');
        $this->class = Document::class;
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function filter($params)
    {
        $filterParams = json_decode($params, true);
        list('name' => $name, 'category_id' => $category_id) = $filterParams;
        $this->filterByName($name);
        $this->filterByCategoryId($category_id);
        return $this;
    }

    protected function filterByName($name)
    {
        $this->queryModel->when($name, function ($q, $name) {
            $q->like('name', $name);
        });
    }

    protected function filterByCategoryId($category_id)
    {
        $this->queryModel->when(filter_var($category_id, FILTER_VALIDATE_INT), function ($q, $category_id) {
            $q->whereHas('categories', function ($q) use ($category_id) {
                return $q->where('category_document_id', '=', $category_id);
            });
        });
    }

    public function paginate($perPage = 15)
    {
        try {
            $documents = $this->queryModel->paginate($perPage);
            $data = $documents->map(function ($document) {
                return (new $this->class($document))->build();
            });

            return response()->json([
                'total' => $documents->total(),
                'documents' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function create($input)
    {
        try {

            $model = $this->model->create($input);
            $categories = explode(',', $input['categories']);
            $model->categories()->sync($categories);
            $file = StorageFiles::putDocument($input['file']);
            $model->path = $file;
            $model->save();

            return new JsonResponse([
                'success' => true,
                'message' => 'Документ был успешно добавлен'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function update($id, $input)
    {
        try {

            $model = $this->queryModel->findOrFail($id);
            $model->update($input);
            $model->touch();
            $model->categories()->sync($input['categories']);

            return new JsonResponse([
                'success' => true,
                'message' => 'Документ был успешно обновлен'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function reloadFile($input)
    {
        try {
            $document = $this->queryModel->findOrFail($input['id']);
            $path = $document->path;
            $newPath = StorageFiles::putDocument($input['file'], $path);
            $document->path = $newPath;
            $document->save();
            return new JsonResponse([
                'success' => true,
                'message' => 'Файл был успешно обновлен'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function deleteDocument(int $id)
    {
        try {
            $document = $this->queryModel->findOrFail($id);
            $path = $document->path;
            $document->delete();
            StorageFiles::deleteDocument($path);
            return new JsonResponse([
                'success' => true,
                'message' => 'Документ был успешно удален'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function getCategoryIdBySlug($slug)
    {
        $category = CategoriesDocument::query()->select('id')->where('slug', $slug)->first();
        return $category->id ?? null;
    }
}