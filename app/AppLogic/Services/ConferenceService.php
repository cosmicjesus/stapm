<?php


namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Conferences\Conference;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\Conference as Model;
use App\Models\File;
use DB;

class ConferenceService extends BaseService
{

    protected $queryModel;
    protected $classHandler;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->orderBy('date_of_event');
        $this->classHandler = Conference::class;
    }

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function onlyActive()
    {
        $this->queryModel->where('active', true);
        return $this;
    }

    public function buildMenuArray()
    {
        return $this->queryModel->get()->map(function (Model $conference) {
            return [
                'name' => $conference->title,
                'url' => route('client.conferences.detail', ['slug' => $conference->slug])
            ];
        })->toArray();
    }

    public function getBySlug($slug)
    {
        $conference = $this->model::query()->where('slug', $slug)->first();

        if ($conference) {
            return (new $this->classHandler($conference))->buildFull();
        }

        throw new CustomModelNotFoundException('Не найдена конференция с таким Slug');

    }

    public function getAll()
    {
        try {
            $data = $this->queryModel->get();

            return $data->map(function ($conference) {
                return (new $this->classHandler($conference))->buildFull();
            });
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function createConference($input)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->create($input);
            if (array_key_exists('files', $input) && count($input['files'])) {
                $this->attachFiles($model->id, $input['files']);
            }
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Конференция была успешно добавлена'
            ])->setStatusCode(200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function attachFiles($id, $files)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->findOrFail($id);
            foreach ($files as $key => $file) {
                $fileModel = new File([
                    'name' => $file['name'],
                    'sort' => ($key + 1),
                ]);
                $fileModel->uploadFile($file['path']);
                $model->files()->save($fileModel);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

}