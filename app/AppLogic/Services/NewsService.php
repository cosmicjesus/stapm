<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\News\News;
use App\AppLogic\Storage\NewsStorage;
use App\Models\News as Model;
use Carbon\Carbon;
use DB;
use Illuminate\Http\JsonResponse;

class NewsService extends BaseService
{
    protected $queryModel;
    protected $class;
    protected $storage;

    /**
     * NewsService constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->orderBy('publication_date', 'DESC');
        $this->class = News::class;
        $this->storage = new NewsStorage();
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    /**
     *
     */
    public function onlyActive()
    {
        $this->queryModel->where('active', true)->where(function ($query) {
            $query->where('visible_to', '>=', Carbon::now()->format('Y-m-d'))
                ->orWhereNull('visible_to');
        });
        return $this;
    }

    /**
     *
     */
    public function onlyNews()
    {
        $this->queryModel->whereIn('type', [1, 3,4]);
        return $this;
    }

    /**
     *
     */
    public function onlyEvents()
    {
        $this->queryModel->whereIn('type', [2, 3]);
        return $this;
    }

    public function onlyWarNews()
    {
        $this->queryModel->whereIn('type', [4]);
        return $this;
    }

    /**
     * @param $filterParams
     * @return $this
     */
    public function filter($filterParams)
    {
        $params = json_decode($filterParams, true);

        list('title' => $title, 'type' => $type) = $params;

        $this->filterByTitle($title);
        $this->filterByType($type);
        return $this;
    }

    /**
     * @param $title
     */
    protected function filterByTitle($title)
    {
        $this->queryModel->when($title, function ($q, $title) {
            return $q->like('title', $title);
        });
    }


    /**
     * @param $type
     */
    protected function filterByType($type)
    {
        $this->queryModel->when($type, function ($q, $type) {
            switch ($type) {
                case 1:
                    $this->onlyNews();
                    break;
                case 2:
                    $this->onlyEvents();
                    break;
            }
        });
    }

    /**
     * @param int $perPage
     * @return array
     */
    public function paginate($perPage = 15)
    {
        try {
            $data = $this->queryModel->paginate($perPage);
            $news = $data->map(function ($item) {
                return (new $this->class($item))->buildBase();
            });

            return [
                'total' => $data->total(),
                'news' => $news
            ];
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function get()
    {
        try {
            $data = $this->queryModel->get();

            return $data->map(function ($news) {
                return (new $this->class($news));
            });
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function getBySlug(string $slug)
    {
        try {
            $news = $this->model::findBySlugOrFail($slug);
            return (new $this->class($news))->full();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function uploadImage(Model $model, $file)
    {
        DB::beginTransaction();
        try {
            $newImage = $this->storage->putNewsImage($file, $model->id, $model->model);
            $model->image = $newImage;
            $model->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $model
     * @param $images
     * @return JsonResponse
     * @throws \Exception
     */
    public function uploadAdditionalPhotos($model, $images)
    {
        try {
            if (isset($images)) {
                $imagesList = [];
                foreach ($images as $image) {
                    $imagesList[] = $this->attachAdditionalPhoto($model->id, $image);
                }
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Фотографии были успешно загружены',
                    'images' => $imagesList
                ]);
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $model_id
     * @param $image
     * @return mixed
     * @throws \Exception
     */
    public function attachAdditionalPhoto($model_id, $image)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->with('images')->findOrFail($model_id);
            $path = $this->storage->putNewsImage($image, $model_id);
            $newsFile = $model->images()->create(['path' => $path]);
            DB::commit();
            return $newsFile;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $model
     * @param $files
     * @throws \Exception
     */
    protected function attachFiles($model, $files)
    {
        try {
            if (isset($files)) {
                foreach ($files as $file) {
                    $this->attachFile($model->id, $file);
                }
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $model_id
     * @param $file
     * @return JsonResponse
     * @throws \Exception
     */
    public function attachFile($model_id, $file)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->with('files')->findOrFail($model_id);
            $path = $this->storage->putNewsFile($model_id, $file['file']);
            $data = ['name' => $file['name'], 'path' => $path];
            $newsFile = $model->files()->create($data);
            DB::commit();
            return new JsonResponse([
                'success' => true,
                'message' => 'Файл был успешно добавлен',
                'file' => $newsFile
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function create($input, $file)
    {
        DB::beginTransaction();
        try {
            $model = $this->model::query()->create($input);
            if ($file) {
                $model->uploadImage($file);
            }
            $this->attachFiles($model, $input['files']);
            $this->uploadAdditionalPhotos($model, $input['images']);
            //throw new \Exception();
            DB::commit();
            return new JsonResponse([
                'success' => true,
                'message' => 'Новость была успешно добавлена'
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function deleteNews(int $news_id)
    {
        DB::beginTransaction();
        try {
            $this->model::destroy($news_id);
            $this->storage->deleteNewsDir($news_id);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Новость и все сопутствующие файлы были удалены'
            ];
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function getAll(){
        try {
            $news = $this->queryModel->get();
            return $news->map(function ($item) {
                $news = (new $this->class($item));
                return [
                    'id' => $news->getId(),
                    'title' => $news->getTitle(),
                    'publication_date' => $news->getPublicationDate(),
                    'preview' => $news->getPreview(),
                    'image' => $news->getImage(),
                    'url' => $news->getClientDetailUrl()
                ];
            });
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function last($count = 5)
    {
        try {
            $news = $this->queryModel->take($count)->get();
            return $news->map(function ($item) {
                $news = (new $this->class($item));
                return [
                    'id' => $news->getId(),
                    'title' => $news->getTitle(),
                    'publication_date' => $news->getPublicationDate(),
                    'preview' => $news->getPreview(),
                    'image' => $news->getImage(),
                    'url' => $news->getClientDetailUrl()
                ];
            });
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
