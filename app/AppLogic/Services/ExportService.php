<?php


namespace App\AppLogic\Services;


use IRebega\DocxReplacer\Docx;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Style\Table as WordTable;

class ExportService
{
    protected StudentService $studentService;

    protected EntrantService $entrantService;

    protected string $t2Template;
    protected string $statementOnEnrollment;

    protected WordTable $tableStyles;

    protected array $cellStyles;

    protected array $textStyles;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
        $this->entrantService = new EntrantService;
        $this->t2Template = storage_path() . '/app/t2template.docx';
        $this->statementOnEnrollment = storage_path() . '/app/statement_on_enrollment.docx';

        $this->cellStyles = [
            'borderTopSize' => 1,
            'borderRightSize' => 1,
            'borderBottomSize' => 1,
            'borderLeftSize' => 1
        ];

        $this->textStyles = ['bold' => true, 'align' => 'center'];


        $table_style = new WordTable;
        $table_style->setBorderColor('000000');
        $table_style->setBorderSize(1);
        $table_style->setUnit(\PhpOffice\PhpWord\SimpleType\TblWidth::PERCENT);
        $table_style->setWidth(100 * 50);

        $this->tableStyles = $table_style;

    }

    protected function checkFile(string $file)
    {
        if (!file_exists($file)) {
            return false;
        }

        return true;
    }

    protected function buildParentsTable($parents)
    {
        $tableWord = new PhpWord();

        $section = $tableWord->addSection();
        $table = $section->addTable($this->tableStyles);

        $table_headers = [
            'Степень родства (ближайшие родственники)',
            'Фамилия, имя, отчество',
            'Год рождения'
        ];

        $table->addRow();

        foreach ($table_headers as $header) {
            $table->addCell(2000,
                $this->cellStyles
            )->addText($header, ['bold' => true, 'align' => 'center']);
        }
        $this->parentsTable = $table;
        if (count($parents) === 0) {
            for ($c = 0; $c <= 3; $c++) {
                $table->addRow();
                for ($r = 0; $r <= 2; $r++) {
                    $table->addCell(2000,
                        $this->cellStyles)->addText('');
                }
            }
        } else {
            foreach ($parents as $parent) {
                $table->addRow();
                for ($c = 0; $c <= 2; $c++) {
                    $table->addCell(2000,
                        $this->cellStyles)->addText($parent[$c]);
                }
            }
        }


        return $this->makeXmlTable($tableWord);
    }

    protected function makeXmlTable($tableWord)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($tableWord, 'Word2007');

        $fullxml = $objWriter->getWriterPart('Document')->write();

        $tablexml = preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullxml);

        return $tablexml;
    }

    public function makeT2ByStudentId($id)
    {
        try {
            if (!$this->checkFile($this->t2Template)) {
                throw new \Exception('Не найден файл ' . $this->t2Template);
            }

            $student = $this->studentService->getById($id);
            $studentByT2 = $student->buildByT2Reference();
            $word = new TemplateProcessor($this->t2Template);

            $file_name = "Т2 " . $studentByT2['lastname'] . " " . $studentByT2['firstname'] . " " . $studentByT2['middlename'] . ".docx";

            foreach ($studentByT2 as $key => $param) {
                $word->setValue($key, $param);
            }

            $parentsTable = $this->buildParentsTable($studentByT2['parents']);
            $word->setValue('table_parent', $parentsTable);

            return response()->download($word->save(), $file_name);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    protected function buildTable($data, $headers, $headerStyles = [])
    {
        $tableWord = new PhpWord();

        $section = $tableWord->addSection();

        $table = $section->addTable($this->tableStyles);

        $table->addRow();

        foreach ($headers as $header) {
            $table->addCell(
                null,
                $this->cellStyles
            )->addText($header, $headerStyles);
        }

        foreach ($data as $item) {
            $table->addRow();
            foreach ($item as $elem) {
                $table->addCell(2000, $this->cellStyles)
                    ->addText($elem);
            }
        }

        return $this->makeXmlTable($tableWord);
    }

    public function makeStatementOnEnrollmentByEntrantId($entrant_id)
    {
        try {
            if (!$this->checkFile($this->statementOnEnrollment)) {
                throw new \Exception('Не найден файл ' . $this->statementOnEnrollment);
            }

            $entrantData = $this->entrantService->makeToStatement($entrant_id);

            $word = new TemplateProcessor($this->statementOnEnrollment);

            $fileName = "Заявление на поступление {$entrantData['lastname']} {$entrantData['firstname']} {$entrantData['middlename']}.docx";

            foreach ($entrantData as $key => $param) {
                if (is_string($param) || is_null($param)) {
                    $word->setValue($key, $param);
                }
            }

            $programsTableHeader = [
                'Наименование професси/специальности',
                'Порядок приоритета'
            ];

            $parentsTableHeader = [
                'Родство',
                'ФИО',
                'Дата рождения',
                'СНИЛС',
                'Место работы, должность',
                'Телефон'
            ];
            $programs = array_map(function ($program) {
                return [$program, ''];
            }, $entrantData['other_programs']);

            $programsTable = $this->buildTable($programs, $programsTableHeader,['bold' => true, 'align' => 'center']);

            if(count($programs)){
                $word->setValue('programs_table', $programsTable);
            }else{
                $word->setValue('programs_table', " ");
            }

            $parentsTable = $this->buildTable($entrantData['parents'], $parentsTableHeader, ['bold' => true, 'align' => 'center']);
            $word->setValue('parents', $parentsTable);

            return [
                'file' => $word,
                'filename' => $fileName
            ];

        } catch (\Exception $exception) {
            dd($exception->getMessage(), $exception->getLine(), $exception->getFile());
        }
    }
}
