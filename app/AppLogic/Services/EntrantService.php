<?php


namespace App\AppLogic\Services;

use App\AppLogic\Base\BaseService;
use App\AppLogic\DayRecruitStatistic\DayRecruitStatisticService;
use App\AppLogic\Entrant\Entrant;
use App\AppLogic\Storage\StudentStorage;
use App\Exceptions\DuplicateEntryException;
use App\Models\Activity;
use App\Models\Entrant as Model;
use App\Models\PriemProgram;
use App\Models\TrainingGroup;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use phpDocumentor\Reflection\DocBlock\StandardTagFactory;

/**
 * Class EntrantService
 * @package App\AppLogic\Services
 */
class EntrantService extends BaseService
{

    protected string $classHandler;
    protected $queryModel;
    protected UserActionsService $userActionService;
    protected CacheService $cacheService;
    private bool $isSortedSet = false;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query();

        $this->classHandler = Entrant::class;
        $this->userActionService = new UserActionsService();

        $this->cacheService = new CacheService;

    }

    /**
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    public function startQuery()
    {
        $this->queryModel = $this->model::query();

        return $this;
    }

    public function filter($filterParams = null)
    {
        $params = json_decode($filterParams, true);

        list(
            'id' => $id,
            'lastname' => $lastname,
            'profession_program_id' => $profession_program_id,
            'document_status' => $document_status,
            'need_hostel' => $need_hostel,
            'only_contract' => $only_contract,
            'no_inn' => $no_inn,
            'no_snils' => $no_snils,
            'sortByStartDate' => $sortByStartDate
            ) = $params;
        $this->startQuery();
        $this->filterById($id);
        $this->filterByLastname($lastname);
        $this->filterByProfessionProgramId($profession_program_id);
        $this->filterByDocumentStatus($document_status);
        $this->filterByContractStatus($only_contract);
        $this->filterByHostelStatus($need_hostel);
        $this->filterByInn($no_inn);
        $this->filterBySnils($no_snils);


        if (in_array($sortByStartDate, ['asc', 'desc'])) {
            $this->sortByStartDate($sortByStartDate);
        } elseif (is_string($sortByStartDate) && !empty($sortByStartDate)) {
            $this->filterByStartDate($sortByStartDate);
        }

        if (!$this->isSortedSet) {
            $this->sortByFullname();
        }

        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    protected function filterById($id)
    {
        $this->queryModel->when($id, function ($q, $id) {
            $explodedIds = explode('/', $id);

            if (count($explodedIds) > 1) {
                $q->whereIn('id', $explodedIds);
            } else {
                $id = array_shift($explodedIds);

                $q->orWhere('id', $id);
            }
        });
        return $this;
    }

    /**
     * @param $lastname
     * @return $this
     */
    protected function filterByLastname($lastname)
    {
        $this->queryModel->when($lastname, function ($q, $lastname) {
            $explodedLastnames = explode('/', $lastname);

            if (count($explodedLastnames) > 1) {
                $q->whereIn('lastname', $explodedLastnames);
            } else {
                $q->like('lastname', $lastname);
            }
        });
        return $this;
    }

    /**
     * @param $profession_program_id
     * @return $this
     */
    protected function filterByProfessionProgramId($profession_program_id)
    {
        $this->queryModel->when($profession_program_id, function ($q, $profession_program_id) {
            //$q->where('profession_program_id', $profession_program_id);
            $q->whereHas('recruitment_programs', function ($q) use ($profession_program_id) {
                return $q->where('recruitment_program_id', $profession_program_id);

            });
        });
        return $this;

    }

    /**
     * @param $documentStatus
     * @return $this
     */
    protected function filterByDocumentStatus($documentStatus)
    {
        $this->queryModel = $this->queryModel->when($documentStatus, function ($q, $status) {
            $documentStatus = $status == 'onlyHasOriginal';
            $q->where('has_original', $documentStatus);
        });

    }

    /**
     * @param $contractStatus
     * @return $this
     */
    protected function filterByContractStatus($contractStatus)
    {
        $this->queryModel = $this->queryModel->when($contractStatus, function ($q) {
            $q->where('contract_target_set', true);
        });

        return $this;
    }

    /**
     * @param $hostelStatus
     * @return $this
     */
    protected function filterByHostelStatus($hostelStatus)
    {
        if (!empty($hostelStatus)) {
            $this->queryModel = $this->queryModel->where('need_hostel', $hostelStatus);

            return $this;
        }
    }

    /**
     * @param $innStatus
     * @return $this
     */
    protected function filterByInn($innStatus)
    {
        $this->queryModel = $this->queryModel->when($innStatus, function ($q) {
            $q->where('inn', null);
        });
        return $this;
    }

    /**
     * @param $snilsStatus
     * @return $this
     */
    protected function filterBySnils($snilsStatus)
    {
        $this->queryModel = $this->queryModel->when($snilsStatus, function ($q) {
            $q->where('inn', null);
        });
        return $this;
    }

    /**
     * @param $categories
     * @return $this
     */
    public function filterByPrivilegedCategories($categories)
    {
        if (count($categories) > 0) {
            foreach ($categories as $key => $category) {
                if ($key == 0) {
                    $this->queryModel = $this->queryModel->whereJsonContains('preferential_categories', $category);
                } else {
                    $this->queryModel = $this->queryModel->OrWhereJsonContains('preferential_categories', $category);
                }
            }
        }
        return $this;
    }

    /**
     * @param $gender
     * @return $this
     */
    public function filterByGender($gender)
    {
        if (in_array($gender, [1, 0])) {
            $this->queryModel = $this->queryModel->where('gender', $gender);
        }

        return $this;
    }


    /**
     * @param string $sortByStartDate
     * @return $this
     */
    public function filterByStartDate(string $sortByStartDate)
    {
        if ($sortByStartDate === 'today') {
            $date = (Carbon::now())->toDateString();
        } else {
            $date = (Carbon::createFromFormat('d.m.Y', $sortByStartDate))->toDateString();
        }

        $this->queryModel = $this->queryModel->where('start_date', $date);

        return $this;
    }

    /**
     * @param string $sortByStartDate
     * @return $this
     */
    public function sortByStartDate(string $sortByStartDate)
    {
        $this->queryModel = $this->queryModel->orderBy('start_date', $sortByStartDate);
        $this->isSortedSet = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function sortByFullname()
    {
        $this->queryModel = $this->queryModel->orderBy('lastname')->orderBy('firstname');
        return $this;
    }

    public function checkUnique(array $input, $id = null)
    {
        $this->startQuery();
        $entrant = $this->queryModel->where([
            'lastname' => $input['lastname'],
            'firstname' => $input['firstname'],
            'middlename' => $input['middlename'],
            'birthday' => $input['birthday'],
        ])->whereIn('status', ['student', 'enrollee', 'inArmy', 'academic', 'expelled', 'unconfirmed_enrollee'])->first();

        if ($entrant) {

            if (is_null($id)) {
                throw new DuplicateEntryException('Такой человек уже есть в базе');
            }

            if (!is_null($id) && ($entrant->id != $id)) {
                throw new DuplicateEntryException('Такой человек уже есть в базе');
            }
        }

        return ['success' => true];

    }

    /**
     * @param int $perPage
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function paginate($perPage = 15)
    {
        try {
            $entrants = $this->queryModel->paginate($perPage);
            $data = $entrants->map(function ($entrant) {
                return (new $this->classHandler($entrant))->buildToTable();
            });

            return response()->json([
                'total' => $entrants->total(),
                'entrants' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    protected function formatAddressesData(array $addresses): array
    {
        if ($addresses['isAddressSimilar']) {
            $addresses['residential'] = $addresses['registration'];
        }
        if ($addresses['isAddressPlaceOfStay']) {
            $addresses['place_of_stay'] = $addresses['registration'];
        }

        return $addresses;
    }

    public function setProgramAndCommieettId(int $entrant_id, int $recruitment_program_id)
    {
        $entrant = $this->queryModel->findOrFail($entrant_id);

        $recruitmentProgram = PriemProgram::query()->findOrFail($recruitment_program_id);

        $params = [
            'recruitment_program_id' => $recruitment_program_id,
            'selection_committee_id' => $recruitmentProgram->selection_committee_id,
            'profession_program_id' => $recruitmentProgram->profession_program_id
        ];

        $entrant->update($params);

        return [
            'success' => true,
            'Абитуриент успешно переведен на другую специальность'
        ];
    }

    public function getById($id): ?Model
    {
        $this->startQuery();
        return $this->queryModel->findOrFail($id);

    }

    public function show(int $id)
    {
        DB::beginTransaction();
        try {
            $entrant = $this->getById($id);

            $handlerEntrant = (new $this->classHandler($entrant))->full();
            \activity('entrant')
                ->tap(function (Activity $activity) {
                    $activity->event = 'show_profile';
                })
                ->log("Просмотр профиля абитуриента {$handlerEntrant['full_name']}");
            DB::commit();
            return response()->json([
                'success' => true,
                'entrant' => $handlerEntrant
            ]);
        } catch (ModelNotFoundException $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Не найден абитуриент с идентификатором {$id}"
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function store(array $input)
    {
        DB::beginTransaction();
        try {
            $this->checkUnique($input);
            $entrant = $this->model::query()->create($input);

            $entrantID = $entrant->id;

            \activity('entrant')
                ->performedOn($entrant)
                ->withProperties(['attributes' => collect($input)->forget(['passport', 'addresses', 'parents'])])
                ->tap(function (Activity $activity) {
                    $activity->event = 'created';
                })
                ->log('Абитуриент добавлен');
            sleep(2);

            $this->setProgramAndCommieettId($entrantID, $input['recruitment_program_id']);
            sleep(2);
            $addresses = $this->formatAddressesData($input['addresses']);
            $entrant->update([
                'addresses' => $addresses,
                'passport_issuance_date' => $input['passport']['issuanceDate'],
                'nationality' => $input['passport']['nationality'],
                'passport_data' => collect($input['passport'])->forget('nationality')->toArray()
            ]);

            if (count($input['parents'])) {
                $this->userActionService->attachParents($entrantID, $input['parents']);
            }
            DB::commit();
            CacheService::refreshEnrolleesCount();
            $this->cacheService->putSubdivisionCodesCache(['code' => $input['passport']['subdivisionCode'], 'issued' => $input['passport']['issued']]);
            (new DayRecruitStatisticService())->incrementOrCreateOnDate($input['start_date'], $input['recruitment_program_id']);
            return response()->json([
                'success' => true,
                'message' => $entrantID
            ]);
        } catch (DuplicateEntryException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Такой человек уже есть в базе'
            ])->setStatusCode(422);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function delete(array $ids)
    {
        DB::beginTransaction();
        try {
            $entrants = $this->queryModel->find($ids);

            foreach ($entrants as $entrant) {
                $entrant->delete();
                (new StudentStorage())->deleteStudentDirectory($entrant->id);
            }
            DB::commit();
            $this->cacheService::refreshEnrolleesCount();
            return responder()->success()->respond();
        } catch (\Exception $exception) {
            DB::rollBack();

            return responder()
                ->error($exception->getCode(), $exception->getMessage() . " File: " . $exception->getFile() . " Line: " . $exception->getLine())
                ->respond(500);
        }
    }

    public function refreshCount()
    {
        try {
            $this->cacheService::refreshEnrolleesCount();

            $countEntrants = $this->cacheService::refreshEnrolleesCount();
            return responder()->success(['count_entrants' => $countEntrants])->respond();
        } catch (\Exception $exception) {
            return responder()
                ->error($exception->getCode(), $exception->getMessage() . " File: " . $exception->getFile() . " Line: " . $exception->getLine())
                ->respond(500);
        }
    }

    protected function buildReportData($params)
    {
        try {
            list(
                'profession_program_id' => $profession_program_id,
                'document_status' => $document_status,
                'need_hostel' => $need_hostel,
                'sortByAverage' => $sortByAverage,
                'gender' => $gender,
                'onlyPriority' => $onlyPriority) = $params;
            $privilegedCategories = array_key_exists('privilegedCategories', $params) ? $params['privilegedCategories'] : [];
            $this->filterByProfessionProgramId($profession_program_id);
            $this->filterByDocumentStatus($document_status);
            $this->filterByPrivilegedCategories($privilegedCategories);
            $this->filterByHostelStatus($need_hostel);
            //$this->filterByGender($gender);
            $this->sortByFullname();
            $entrants = $this->queryModel->get()->map(function ($entrant) {
                /**
                 * @var $entrant Entrant;
                 */
                $entrant = (new $this->classHandler($entrant));
                return [
                    'lastname' => $entrant->getLastname(),
                    'firstname' => $entrant->getFirstname(),
                    'middlename' => $entrant->getMiddlename(),
                    'full_name' => $entrant->getFullName(),
                    'gender' => $entrant->getGender(),
                    'birthday' => $entrant->getBirthday(),
                    'recruitment_programs' => $entrant->getPrograms(),
                    'start_date' => $entrant->getStartDate(),
                    'has_original' => $entrant->getHasOriginalStatus(),
                    'contract_target_set' => $entrant->isContractTargetSetStatus(),
                    'phone' => $entrant->getPhone(),
                    'need_hostel' => $entrant->getNeedHostelStatus(),
                    'avg' => $entrant->getAverageScore(),
                    'health' => $entrant->getHealthData(),
                    'files' => $entrant->getFiles(),
                    'addresses' => [
                        'registration' => $entrant->buildAddress('registration'),
                        'place_of_stay' => $entrant->buildAddress('place_of_stay'),
                    ],
                    'preferential_categories' => $entrant->getPrivilegedCategory()
                ];
            });

            if (filter_var($onlyPriority, FILTER_VALIDATE_BOOLEAN)) {
                $entrants = $entrants->filter(function ($entrant) use ($profession_program_id) {
                    return $entrant['recruitment_programs']
                        ->filter(fn($program) => $program['is_priority'] && $program['recruitment_program_id'] == $profession_program_id)
                        ->isNotEmpty();
                })->values();
            }

            if ($sortByAverage == 'asc') {
                return $entrants->sortBy('avg')->values();
            } elseif ($sortByAverage == 'desc') {
                return $entrants->sortByDesc('avg')->values();
            }

            return $entrants;
        } catch (\Exception $exception) {

        }
    }

    public function reportToPdf($request)
    {
        list('params' => $params, 'columns' => $columns) = $request;

        $paramsToArray = json_decode($params, true);
        $columnsToArray = json_decode($columns, true);

        $countActiveColumns = count(array_filter($columnsToArray, function ($value) {
            return $value;
        }));

        $entrants = $this->buildReportData($paramsToArray);

        $totalAverage = $entrants->sum('avg') / $entrants->count();

        $program = null;

        if (!empty($paramsToArray['profession_program_id'])) {
            $activeCommitteeId = (new SelectionCommitteeService())->getActiveCommitteeId();

            $RecruitmentProgras = PriemProgram::query()->where([
                'profession_program_id' => $paramsToArray['profession_program_id']
            ])->first();
        }

        $pdf = \PDF::loadView('reports.entrants-report',
            [
                'entrants' => $entrants,
                'columns' => $columnsToArray,
                'totalAverage' => round($totalAverage, 4),
                'totalRowSize' => $countActiveColumns + 1
            ],
            [],
            [
                'format' => $countActiveColumns > 6 ? 'A4-L' : 'A4-P'
            ]);

        return $pdf->stream();
    }

    public function reportDataToJson($params)
    {
        try {
            $entrants = $this->buildReportData($params);
            return response()->json([
                'success' => true,
                'entrants' => $entrants
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(500);
        }
    }

    public function update(int $id, array $input)
    {
        try {
            /**
             * @var $entrant \App\Models\Entrant
             */
            $this->checkUnique($input, $id);
            $entrant = $this->getById($id);

//            $oldParams = ['date' => $entrant->start_date->format('Y-m-d'), 'recruitment_program_id' => $entrant->recruitment_program_id];
//
//            $newParams = ['date' => $input['start_date'], 'recruitment_program_id' => $input['recruitment_program_id']];

            $entrant->update($input);

            // $diff = array_diff($oldParams, $newParams);

//            if (count($diff)) {
//                (new DayRecruitStatisticService())->refreshStatistic($oldParams, $newParams);
//            }

            return response()->json([
                'success' => true
            ]);

        } catch (DuplicateEntryException $exception) {
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(422);
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'errors' => [$exception->getMessage()]
            ])->setStatusCode(404);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => true,
                'message' => $exception
            ])->setStatusCode(500);
        }
    }

    public function enrollment($input)
    {
        try {
            $decree = (new DecreeService())->getDecreeByParams(['number' => $input['decree_number'], 'date' => $input['decree_date']]);
            $training_group = TrainingGroup::query()->findOrFail($input['group_id']);
            $groupHandler = new \App\AppLogic\TrainingGroup($training_group);
            foreach ($input['entrantsData'] as $key => $nominal_number) {
                $entrantModel = \App\Models\Entrant::findOrFail($key);
                $entrantClass = new Entrant($entrantModel);
                $entrantClass->enrollment([
                    'decree_id' => $decree->id,
                    'decree_date' => $decree->date,
                    'group_name' => $groupHandler->getFullName(),
                    'group_id' => $groupHandler->getId(),
                    'profession_program_id' => $groupHandler->getProfessionProgram()['id'],
                    'nominal_number' => $nominal_number == 0 ? null : $nominal_number
                ]);
            }

            $this->cacheService::refreshEnrolleesCount();
            $this->cacheService::refreshStudentsCount();
            return responder()->success()->respond();
        } catch (\Exception $exception) {
            return responder()->error($exception->getCode(), $exception->getMessage())->data(['line' => $exception->getLine(), 'file' => $exception->getFile()])->respond(500);
        }
    }

    /**
     * @param int $entrant_id
     * @return array
     */
    public function makeToStatement(int $entrant_id): array
    {
        $entrant = $this->getById($entrant_id);
        /**
         * @var $entrantClass Entrant
         */
        $entrantClass = new $this->classHandler($entrant);
        $graduationDate = $entrantClass->getGraduationDate();

        $preferentialCategories = $entrantClass->getPrivilegedCategory();

        $categoriesToStr = '';

        if (count($preferentialCategories)) {
            foreach ($preferentialCategories as $preferentialCategory) {
                $categoriesToStr .= getPrivilegeCategory($preferentialCategory) . ", ";
            }
            $categoriesToStr = substr($categoriesToStr, 0, -2);
        } else {
            $categoriesToStr = 'Отсутствуют';
        }

        $personal = [
            'lastname' => $entrantClass->getLastname(),
            'firstname' => $entrantClass->getFirstname(),
            'middlename' => $entrantClass->getMIddlename(),
            'birthday' => dateFormat($entrantClass->getBirthday()),
            'nationality' => getNationality($entrantClass->getNationality()),
            'document_type' => getDocumentType($entrantClass->getPassportData('documentType')),
            'document_series' => $entrantClass->getPassportData('series'),
            'document_number' => $entrantClass->getPassportData('number'),
            'subdivision_code' => $entrantClass->getPassportData('subdivisionCode'),
            'issued' => $entrantClass->getPassportData('issued'),
            'issuanceDate' => dateFormat($entrantClass->getPassportData('issuanceDate')),
            'birthplace' => $entrantClass->getPassportData('birthPlace'),
            'phone' => $entrantClass->getPhone(),
            'email' => $entrantClass->getEmail(),
            'graduation_year' => is_null($graduationDate) ? '___________' : dateFormat($graduationDate, 'Y-m-d', 'Y'),
            'graduation_date' => is_null($graduationDate) ? '_______________' : dateFormat($graduationDate),
            'start_date' => dateFormat($entrantClass->getStartDate()),
            'graduation_organization' => $entrantClass->getGraduationOrganizationName() . ", " . $entrantClass->getGraduationOrganizationPlace(),
            'education' => getEducation($entrantClass->getEducationId()),
            'language' => $entrantClass->getLanguage() ? getLanguage($entrantClass->getLanguage()) : '',
            'need_hostel' => $entrantClass->getNeedHostelStatus() ? 'Нуждаюсь' : 'Не нуждаюсь',
            'preferentialCategories' => $categoriesToStr
        ];

        $registrationAddress = $this->buildAddressToStatement($entrantClass, 'registration');
        $residentialAddress = $this->buildAddressToStatement($entrantClass, 'residential');
        $programs = $this->buildProgramToStatement($entrantClass);
        $parents = $this->buildParentsToStatement($entrantClass);

        return array_merge($personal, $registrationAddress, $residentialAddress, $programs, $parents);

    }

    protected function buildParentsToStatement(Entrant $entrant): array
    {
        $parents = $entrant->getParents()->map(function ($parent) {
            return [
                getRelationShipType($parent['relation_ship_type']),
                $parent['full_name'],
                dateFormat($parent['birthday']),
                $parent['snils'],
                $parent['place_of_work'],
                $parent['phone']
            ];
        })->toArray();
        return ['parents' => $parents];
    }

    protected function buildProgramToStatement(Entrant $entrant): array
    {
        $programs = $entrant->getPrograms();

        $priorityProgram = $programs->filter(fn($program) => $program['is_priority'])->first();

        $splitName = explode("|", $priorityProgram['program_name']);

        $otherPrograms = $programs
            ->filter(fn($program) => $program['id'] != $priorityProgram['id'])
            ->map(function ($program) {
                $splitName = explode("|", $program['program_name']);
                return trim($splitName[0]);
            })->toArray();

        if (count($otherPrograms) === 0) {
            $otherPrograms = (ProfessionProgramService::getProgramsByIdOneType($priorityProgram['program_id']))
                ->map(fn($program) => $program->full_name)
                ->toArray();
        }

        return [
            'priority_program_name' => trim($splitName[0]),
            'other_programs' => $otherPrograms
        ];
    }

    protected function buildAddressToStatement(Entrant $entrant, string $addressType = 'registration'): array
    {

        $address = $entrant->getAddress($addressType);

        $registrationDate = $entrant->getAddress($addressType, 'registration_date');

        $isHasRegistrationDate = !is_null($registrationDate);
        $isLiveInSettlement = !empty($entrant->getAddress($addressType, 'settlement'));

        return [
            "${addressType}_region" => $entrant->getAddress($addressType, 'region'),
            "${addressType}_area" => $address['area'] ?? '-',
            "${addressType}_city" => $isLiveInSettlement ? $address['settlement'] : $address['city'],
            "${addressType}_city_area" => $address['city_area'] ?? '-',
            "${addressType}_index" => $address['index'],
            "${addressType}_region" => $address['region'],
            "${addressType}_street" => $address['street'],
            "${addressType}_housing" => $address['housing'],
            "${addressType}_settlement" => $address['settlement'],
            "${addressType}_house_number" => $address['house_number'],
            "${addressType}_apartment_number" => $address['apartment_number'] ?? '-',
            "${addressType}_registration_date" => $isHasRegistrationDate ? dateFormat($registrationDate) : ''
        ];
    }

}
