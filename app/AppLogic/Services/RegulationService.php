<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Regulation\Regulation;
use App\Exceptions\ModelNotFountException;
use App\Models\Regulation as Model;
use Illuminate\Http\JsonResponse;

class RegulationService extends BaseService
{
    protected $queryModel;
    protected $class;


    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->orderBy('date', 'DESC');
        $this->class = Regulation::class;
    }

    public function model()
    {
        return Model::class;
    }

    public function paginate($perPage = 15)
    {
        try {
            $regulations = $this->queryModel->paginate($perPage);
            $data = $regulations->map(function ($regulation) {
                return (new $this->class($regulation))->buildOnTable();
            });
            $total = $regulations->total();

            return new JsonResponse([
                'regulations' => $data,
                'total' => $total
            ]);
        } catch (\Exception $exception) {

        }
    }

    public function getById($id)
    {
        $regulation = $this->queryModel->find($id);
        if ($regulation) {
            return new JsonResponse((new $this->class($regulation))->full(), 200);
        }

        throw (new ModelNotFountException())->setModel($this->model, $id, 'Не найдено предписание с идентификатором');
    }
}