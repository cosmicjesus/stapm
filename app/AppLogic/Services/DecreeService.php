<?php

namespace App\AppLogic\Services;


use App\AppLogic\Base\BaseService;
use App\AppLogic\Decree\Decree;
use App\Helpers\Constants;
use App\Models\Decree as Model;
use Illuminate\Http\JsonResponse;

class DecreeService extends BaseService
{
    protected $queryModel;
    protected $class;

    public function __construct()
    {
        parent::__construct();
        $this->class = Decree::class;
        $this->queryModel = $this->model::query()->orderBy('date', 'DESC')->orderBy('id', 'DESC');
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Model::class;
    }

    /**
     * @param $params
     * @return $this
     */
    public function filter($params)
    {
        $filterParams = json_decode($params, true);
        list('term' => $term, 'date' => $date, 'number' => $number, 'student_lastname' => $studentLastName) = $filterParams;
        $this->filterByTerm($term);
        $this->filterByDates($date);
        $this->filterByNumber($number);
        $this->filterByStudentLastName($studentLastName);
        return $this;
    }


    /**
     * @param $term
     */
    protected function filterByTerm($term)
    {
        $this->queryModel->when($term, function ($q, $term) {
            return $q->where('term', $term);
        });
    }

    /**
     * @param $dates
     */
    protected function filterByDates($dates)
    {
        $this->queryModel->when($dates, function ($q, $dates) {
            if ($dates[0] == $dates[1]) {
                return $q->where('date', $dates[0]);
            } else {
                return $q->whereBetween('date', $dates);
            }
        });
    }

    /**
     * @param $number
     */
    protected function filterByNumber($number)
    {
        $this->queryModel->when($number, function ($q, $number) {
            return $q->like('number', $number);
        });
    }

    /**
     * @param $lastname
     */
    protected function filterByStudentLastName($lastname)
    {
        $this->queryModel->when($lastname, function ($q, $lastname) {
            return $q->whereHas('students', function ($query) use ($lastname) {
                return $query->whereLastname($lastname);
            });

        });
    }

    /**
     * @param int $perPage
     * @return JsonResponse
     */
    public function paginate($perPage = 15)
    {
        try {
            $decrees = $this->queryModel->paginate($perPage);
            $data = $decrees->map(function ($decree) {
                return (new $this->class($decree))->full();
            });

            return new JsonResponse([
                'total' => $decrees->total(),
                'decrees' => $data
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ])->setStatusCode(500);
        }
    }

    public function getDecreeByParams($params)
    {
        try {
            $decreeParams = array_merge($params, ['term' => getCurrentPeriod($params['date'])]);
            $decree = $this->model::query()->firstOrCreate($decreeParams);

            return $decree;
        } catch (\Exception $exception) {

        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function detail(int $id)
    {
        try {
            $decree = $this->model::query()->with('students')->findOrFail($id);

            $res = [];

            $type = $decree->type;

            if (in_array($type, ['enrollment', 'transfer', 'reinstate','internal_transfer_next_year','internal_transfer_next_term'])) {
                $res = $decree->students->groupBy('pivot.group_to');
            } else {
                $res = $decree->students->groupBy('pivot.group_from');
            }
            $result = [];
            $decreeTypes = Constants::$detailDecreeTemplate;
            foreach ($res as $key => $value) {
                $result[] = [
                    'group' => $key,
                    'text' => str_replace('{group}', $key, $decreeTypes[$type]),
                    'students' => $value->map(function ($student) {
                        return ['full_name' => $student->full_name, 'reason' => $student->pivot->reason];
                    })
                ];
            }

            return new JsonResponse([
                'success' => true,
                'detail' => $result
            ], 200);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    protected function checkUnique($input)
    {
        $decree = $this->model::query()->where('number', $input['number'])->where('date', $input['date'])->first();
        if (!$decree) {
            return true;
        }
        return $decree->id == $input['id'];
    }

    public function updateDecree(int $id, array $input)
    {
        try {
            if (!$this->checkUnique($input)) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Такой приказ уже существует'
                ], 422);
            }
            $this->model::query()->findOrFail($id)->update($input);
            return new JsonResponse([
                'success' => true,
                'message' => 'Приказ был успешно обновлен',
            ], 200);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}