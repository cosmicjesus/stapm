<?php


namespace App\AppLogic\FIle;

use App\Models\File as Model;

class File
{
    protected $file;

    public function __construct(Model $file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->file->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->file->name;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->file->sort;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->file->active;
    }

    public function getPath()
    {
        return \Storage::disk('files')->url($this->file->path);
    }

    public function buildFull()
    {
        $file = collect();

        $file->put('id', $this->getId());
        $file->put('name', $this->getName());
        $file->put('path', $this->getPath());
        $file->put('sort', $this->getSort());

        return $file;
    }
}