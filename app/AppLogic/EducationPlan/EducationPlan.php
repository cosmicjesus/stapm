<?php


namespace App\AppLogic\EducationPlan;


use App\AppLogic\FileComponent\FileComponent;
use App\AppLogic\ProfessionProgram\ProfessionProgram;
use App\AppLogic\Storage\ProfessionProgramStorage;
use App\AppLogic\Storage\StorageFiles;
use Illuminate\Support\Collection;

class EducationPlan
{
    protected $plan;
    protected $program;
    protected $trainingGroups;
    protected $components;
    protected $storage;

    public function __construct(\App\Models\EducationPlan $educationPlan)
    {
        $this->plan = $educationPlan;
        $this->plan->load('professionProgram', 'training_groups', 'components');
        $this->program = $this->plan->professionProgram;
        $this->trainingGroups = $this->plan->training_groups;
        $this->components = $this->plan->components;
        $this->storage = new ProfessionProgramStorage();
    }

    public function getId()
    {
        return $this->plan->id;
    }

    public function getName()
    {
        return $this->plan->name;
    }

    public function getNameFromStartDate()
    {
        return $this->plan->start_training->format('Y');
    }

    public function getPeriod()
    {
        return $this->plan->count_period;
    }

    public function getStartTraining()
    {
        return $this->plan->start_training->format('Y-m-d');
    }

    public function getEndTraining()
    {
        return $this->plan->end_training->format('Y-m-d');
    }

    public function getActive()
    {
        return $this->plan->active;
    }

    public function getProgram()
    {
        $program = new ProfessionProgram($this->program);

        return ['id' => $program->getId(), 'name' => $program->getNameWithForm()];
    }

    public function hasFile()
    {
        return !is_null($this->plan->full_path) && $this->storage->hasFile($this->plan->full_path);
    }


    public function getFile()
    {
        if (is_null($this->plan->full_path)) {
            return null;
        }

        return StorageFiles::getEducationPlan($this->plan->full_path);
    }

    /**
     * @return array|null
     */
    public function getExtraOptions()
    {
        return $this->plan->extra_options ?? [];
    }

    /**
     * @return bool
     */
    public function getIsRemoveStatus()
    {
        $countGroups = $this->trainingGroups->count();

        return (bool)($countGroups < 1);
    }


    /**
     * @return Collection
     */
    public function getComponents(): Collection
    {
        return $this->components->map(function ($component) {
            return (new FileComponent($component))->full();
        });
    }

    /**
     * @return Collection
     */
    public function getComponentsPaths()
    {
        $components = $this->getComponents();

        return $components->map(function ($component) {
            return '../public' . $component['path'];
        });
    }

    public function getUpdatedAt()
    {
        return $this->plan->updated_at->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function buildOnResponse()
    {
        $plan = collect();
        $plan->put('id', $this->getId());
        $plan->put('name', $this->getName());
        $plan->put('program', $this->getProgram());
        $plan->put('count_period', $this->getPeriod());
        $plan->put('start_training', $this->getStartTraining());
        $plan->put('end_training', $this->getEndTraining());
        $plan->put('active', $this->getActive());
        $plan->put('file', $this->getFile());
        $plan->put('isRemove', $this->getIsRemoveStatus());
        $plan->put('extra_options', $this->getExtraOptions());
        $plan->put('updated_at', $this->getUpdatedAt());
        return $plan;
    }

    public function full()
    {
        $base = $this->buildOnResponse();
        $base->put('components', $this->getComponents());
        $base->put('hasFile', $this->hasFile());
        return $base;
    }
}