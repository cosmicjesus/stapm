<?php

namespace App\AppLogic\Abstracts;


use App\AppLogic\Storage\StudentStorage;
use App\AppLogic\Student\Student;
use App\AppLogic\Student\StudentFile;
use App\AppLogic\User\UserParent;
use App\Helpers\Constants;
use Carbon\Carbon;
use Storage;

abstract class AbstractStudent
{
    protected $human;
    protected $storage;

    /**
     * @param $human
     */
    public function initialization($human)
    {
        $this->human = $human;
        $this->storage = new StudentStorage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->human->id;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->human->lastname;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->human->firstname;
    }

    public function getMiddlename()
    {
        return $this->human->middlename;
    }

    public function getFullName()
    {
        return $this->human->full_name;
    }

    public function getBirthday()
    {
        return $this->human->birthday->format('Y-m-d');
    }

    public function getAge()
    {
        $age = Carbon::now()->diffInYears($this->human->birthday);
        return $age;
    }

    public function getAgeAtTheEndOfYear()
    {
        $age = Carbon::now()->month(12)->day(31)->diffInYears($this->human->birthday);
        return $age;
    }

    public function getGender()
    {
        return $this->human->gender;
    }

    public function getStatus()
    {
        return $this->human->status;
    }

    public function getPhone()
    {
        return $this->human->phone;
    }

    public function getEmail()
    {
        return $this->human->email;
    }

    public function getPrivilegedCategory()
    {
        return $this->human->preferential_categories ?? [];
    }

    public function getSnils()
    {
        return $this->human->snils;
    }

    public function getInn()
    {
        return $this->human->inn;
    }

    public function getMedicalPolicy()
    {
        return $this->human->medical_policy_number;
    }

    public function getNationality()
    {
        return $this->human->nationality;
    }

    public function getAvg()
    {
        return $this->human->avg;
    }

    public function getHasOriginalStatus()
    {
        return $this->human->has_original;
    }

    public function has_certificate()
    {
        return $this->human->has_certificate;
    }

    public function getDateOfActualTransfer()
    {
        if (is_null($this->human->date_of_actual_transfer)) {
            return null;
        }

        return $this->human->date_of_actual_transfer->format('Y-m-d');
    }

    public function hasPhoto()
    {
        return !is_null($this->human->photo) && \Storage::disk('student')->has($this->human->photo);
    }

    public function getPhoto()
    {
        if ($this->hasPhoto()) {
            return $this->storage->getPhoto($this->human->photo);
        }

        return null;
    }

    public function getEducationId()
    {
        return $this->human->education_id;
    }

    public function getGraduationDate()
    {
        return $this->human->graduation_date?$this->human->graduation_date->format('Y-m-d'):null;
    }

    public function getGraduationOrganizationName()
    {
        return $this->human->graduation_organization_name;
    }

    public function getGraduationOrganizationPlace()
    {
        return $this->human->graduation_organization_place;
    }

    public function getRatingsMap()
    {
        return $this->human->ratings_map;
    }

    public function getAverageScore()
    {
        return $this->human->avg;
    }

    public function getPassport()
    {
        return $this->human->passport_data;
    }

    public function getPassportWithNationality()
    {
        return [
            'nationality' => $this->getNationality(),
            'passport' => $this->getPassport()
        ];
    }

    /**
     * @param string $key
     * @return string
     */
    public function getPassportData(string $key)
    {
        $data = $this->getPassport();

        return $data[$key] ?? '';
    }

    public function getAddresses()
    {
        return $this->human->addresses;
    }

    public function getLanguage()
    {
        return $this->human->language;
    }

    /**
     * @return bool
     */
    public function getNeedHostelStatus(): bool
    {
        return $this->human->need_hostel;
    }

    /**
     * @param string $type
     * @param null $key
     * @return array|string
     */
    public function getAddress(string $type, $key = null)
    {
        $addresses = $this->getAddresses();

        if (is_null($key)) {
            return $addresses[$type] ?? [];
        }
        return $addresses[$type][$key] ?? '';
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getEducationData($key = null)
    {
        $education = collect();

        $education->put('education_id', $this->getEducationId());
        $education->put('graduation_date', $this->getGraduationDate());
        $education->put('graduation_organization_name', $this->getGraduationOrganizationName());
        $education->put('graduation_organization_place', $this->getGraduationOrganizationPlace());
        $education->put('avg', $this->getAverageScore());
        $education->put('ratings_map', $this->getRatingsMap());
        $education->put('has_original', $this->getHasOriginalStatus());

        if (!is_null($key)) {
            return $education[$key];
        }

        return $education;
    }

    /**
     * @return mixed
     */
    public function getParents()
    {
        $this->human->load('parents');

        $parents = $this->human->parents->map(function ($parent) {
            return (new UserParent($parent))->build();
        });

        return $parents;

    }

    public function getFiles()
    {
        $this->human->load('files');

        $files = $this->human->files->map(function (\App\Models\StudentFile $file) {
            return (new StudentFile($file))->build();
        });

        return $files;

    }

    /**
     * @return mixed
     */
    public function getMilitaryDocumentType()
    {
        return $this->human->military_document_type;
    }

    /**
     * @return mixed
     */
    public function getMilitaryDocumentNumber()
    {
        return $this->human->military_document_number;
    }


    /**
     * @return mixed
     */
    public function getFitnessForMilitaryService()
    {
        return $this->human->fitness_for_military_service;
    }

    /**
     * @return mixed
     */
    public function getMilitarySpecialty()
    {
        return $this->human->military_specialty;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->human->comment;
    }

    public function getRecruitmentCenter()
    {
        $this->human->load('recruitmentCenter');

        return [
            'id' => $this->human->recruitmentCenter->id,
            'name' => $this->human->recruitmentCenter->name,
        ];
    }

    /**
     * @return string|null
     */
    public function getMilitaryStatus(): ?string
    {
        return $this->human->military_status;
    }

    public function getMilitaryRank(): ?string
    {
        return $this->human->military_rank;
    }

    public function getMilitaryComposition(): ?string
    {
        return $this->human->military_composition;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getMilitaryData()
    {
        $data = collect();
        $data->put('military_document_type', $this->getMilitaryDocumentType());
        $data->put('military_document_number', $this->getMilitaryDocumentNumber());
        $data->put('fitness_for_military_service', $this->getFitnessForMilitaryService());
        $data->put('military_specialty', $this->getMilitarySpecialty());
        $data->put('recruitment_center', $this->getRecruitmentCenter());
        $data->put('military_status', $this->getMilitaryStatus());
        $data->put('military_rank', $this->getMilitaryRank());
        $data->put('military_composition', $this->getMilitaryComposition());

        return $data;
    }

    public function isContractTargetSetStatus()
    {
        return $this->human->contract_target_set;
    }

    public function buildAddress($key)
    {
        if (is_null($this->human->addresses)) {
            return '';
        }

        $addresses = $this->human->addresses;
        if (array_key_exists($key, $addresses)) {

            $addressArr = $addresses[$key];
            $address = '';
            if (!is_null($addressArr['region'])) {
                $expRegion = explode(' ', $addressArr['region']);
                count($expRegion) > 1 ? $address .= $addressArr['region'] . ", " : $address .= $addressArr['region'] . " область, ";
            }
            !is_null($addressArr['area']) ? $address .= $addressArr['area'] . " район, " : '';
            !is_null($addressArr['settlement']) ? $address .= $addressArr['settlement'] . ", " : '';
            !is_null($addressArr['index']) ? $address .= $addressArr['index'] . " " : '';
            !is_null($addressArr['city']) ? $address .= "г." . $addressArr['city'] . ", " : '';
            array_key_exists('city_area', $addressArr) && !is_null($addressArr['city_area']) ? $address .= $addressArr['city_area'] . ' район, ' : '';
            !is_null($addressArr['street']) ? $address .= "ул." . $addressArr['street'] . ", " : '';
            !is_null($addressArr['house_number']) ? $address .= "д." . $addressArr['house_number'] . ", " : '';
            !is_null($addressArr['apartment_number']) ? $address .= "кв." . $addressArr['apartment_number'] : '';

            return $address;
        }
        return '';
    }

    /**
     * @return string|null
     */
    public function getHealthCategory(): ?string
    {
        return $this->human->health_category;
    }

    public function getDisability()
    {
        return $this->human->disability;
    }

    public function getPersonWithDisabilitiesStatus()
    {
        return $this->human->person_with_disabilities;
    }

    public function getHealthData()
    {
        $health = collect();

        $health->put('medical_policy_number', $this->getMedicalPolicy());
        $health->put('preferential_categories', $this->getPrivilegedCategory());
        $health->put('health_category', $this->getHealthCategory());
        $health->put('disability', $this->getDisability());
        $health->put('person_with_disabilities', $this->getPersonWithDisabilitiesStatus());
        $health->put('has_certificate', $this->has_certificate());

        return $health;
    }

    public function getHowFindOut()
    {
        return $this->human->how_find_out;
    }

    public function getInvitedStudent()
    {
        $this->human->load('invitedStudent');

        $invitedStudent = $this->human->invitedStudent;

        if (is_null($invitedStudent)) {
            return null;
        }

        $student = new Student($invitedStudent);

        return [
            'id' => $student->getId(),
            'full_name' => $student->getFullName(),
            'training_group' => $student->getGroup(),
            'name_with_group' => $student->getFullName() . " [" . $student->getGroup()['name'] . "]"
        ];
    }

    public function hasInn()
    {
        return $this->human->inn != null;
    }

    public function hasSnils()
    {
        return $this->human->snils != null;
    }

    public function hasMilitaryDocumentId()
    {
        return !is_null($this->human->military_document_type);
    }

    public function hasMilitaryDocumentNumber()
    {
        return !is_null($this->human->military_document_number);
    }

    public function hasNotPhoto()
    {
        return is_null($this->human->photo);
    }

    public function isMale()
    {
        return $this->human->gender === 0;
    }

    public function isFemale()
    {
        return $this->human->gender === 1;
    }

    public function isServedInTheArmy()
    {
        return $this->getMilitaryStatus() === 'ServedInTheArmy';
    }
}
