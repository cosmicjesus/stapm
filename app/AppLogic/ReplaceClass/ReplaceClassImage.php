<?php

namespace App\AppLogic\ReplaceClass;


use App\Models\ReplacingClassesImage;

class ReplaceClassImage
{
    protected $image;

    public function __construct(ReplacingClassesImage $image)
    {
        $this->image = $image;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function full()
    {
        $image = collect();
        $image->put('url', $this->getImagePath());
        return $image;
    }

    public function getImagePath()
    {
        return \Storage::disk('replace_class')->url($this->image->path);
    }
}