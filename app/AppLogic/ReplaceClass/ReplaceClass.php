<?php

namespace App\AppLogic\ReplaceClass;


use App\Models\ReplacingClass;
use Illuminate\Support\Collection;

class ReplaceClass
{
    protected $replaceClass;
    protected $images;

    public function __construct(ReplacingClass $replaceClass)
    {
        $this->replaceClass = $replaceClass;
        $this->replaceClass->load('images');
        $this->images = $this->replaceClass->images;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->replaceClass->id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->replaceClass->date_of_replacing->format('Y-m-d');
    }

    /**
     * @return \App\Models\ReplacingClassesImage[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getImages()
    {
        return $this->images->map(function ($image) {
            return (new ReplaceClassImage($image))->full();
        });
    }

    /**
     * @return int
     */
    public function getCountImages(): int
    {
        return $this->getImages()->count();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function full($excluded = []): Collection
    {
        $entity = collect();
        $entity->put('id', $this->getID());
        $entity->put('date_of_replacing', $this->getDate());
        $entity->put('images', $this->getImages());
        $entity->put('count_images', $this->getCountImages());
        $entity->put('previous_date', $this->getPreviousDate());
        $entity->put('next_date', $this->getNextDate());

        $entity->forget($excluded);

        return $entity;
    }

    /**
     * Возвращает дату предыдущей замены
     * @return string|null
     */
    public function getPreviousDate(): ?string
    {
        return $this->replaceClass->previous();
    }

    /**
     * Возвращает дату следующей замены
     * @return string|null
     */
    public function getNextDate(): ?string
    {
        return $this->replaceClass->next();
    }
}