<?php


namespace App\AppLogic;

use App\AppLogic\EducationPlan\EducationPlan;
use App\AppLogic\Employee\Employee;
use App\AppLogic\ProfessionProgram\ProfessionProgram;
use App\Models\TrainingGroup as Model;

class TrainingGroup
{

    protected $group;

    public function __construct(Model $trainingGroup)
    {
        $this->group = $trainingGroup;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->group->id;
    }

    /**
     * @return array
     */
    public function getProfessionProgram(): array
    {
        $program = new ProfessionProgram($this->group->program);

        return [
            'id' => $program->getId(),
            'name' => $program->getNameWithForm()
        ];
    }

    /**
     * @return array
     */
    public function getEducationPlan(): array
    {
        $plan = new EducationPlan($this->group->education_plan);

        return [
            'id' => $plan->getId(),
            'name' => $plan->getName(),
            'count_semesters' => $plan->getPeriod()
        ];
    }

    /**
     * @return array
     */
    public function getCurator(): array
    {
        $curator = new Employee($this->group->curator);

        return [
            'id' => $curator->getId(),
            'full_name' => $curator->getFullName()
        ];
    }

    /**
     * @return string
     */
    public function getUniqueCode(): string
    {
        return $this->group->unique_code;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->group->pattern;
    }

    /**
     * @return int
     */
    public function getCourse(): int
    {
        return $this->group->course;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->group->full_name;
    }

    /**
     * @return int
     */
    public function getPeriod(): int
    {
        return $this->group->period;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->group->status;
    }

    public function isReadyToRelease()
    {
        return ($this->getEducationPlan())['count_semesters'] === $this->getPeriod();
    }

    /**
     * @return array
     */
    public function buildToTable(): array
    {
        return [
            'id' => $this->getId(),
            'full_name' => $this->getFullName(),
            'unique_code' => $this->getUniqueCode(),
            'pattern' => $this->getPattern(),
            'curator' => $this->getCurator(),
            'course' => $this->getCourse(),
            'education_plan' => $this->getEducationPlan(),
            'program' => $this->getProfessionProgram(),
            'is_ready_to_release' => $this->isReadyToRelease(),
        ];
    }

}