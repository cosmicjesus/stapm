<?php

namespace App\AppLogic\Subject;

use App\Models\Subject as Model;

class Subject
{
    protected $subject;
    protected $programs;

    public function __construct(Model $subject)
    {
        $this->subject = $subject;
        $this->subject->load('programs');
        $this->programs = $this->subject->programs;
    }

    public function getId()
    {
        return $this->subject->id;
    }

    public function getName()
    {
        return $this->subject->name;
    }

    public function getType()
    {
        return $this->subject->type_id;
    }

    public function isRemove()
    {
        return $this->programs->count() >= 1 ? false : true;
    }

    public function buildOnResponse()
    {
        $subject = collect();

        $subject->put('id', $this->getId());
        $subject->put('name', $this->getName());
        $subject->put('type_id', $this->getType());
        $subject->put('isRemove', $this->isRemove());
        return $subject;
    }
}