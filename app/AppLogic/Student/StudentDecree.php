<?php

namespace App\AppLogic\Student;


use App\AppLogic\Decree\Decree;

class StudentDecree
{
    protected $decree;
    protected $pivot;


    public function __construct($decree)
    {
        $this->decree = $decree;
        $this->pivot = $decree->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getDecreeId()
    {
        return $this->decree->id;
    }

    public function getType()
    {
        return $this->pivot->type;
    }

    public function getNumber()
    {
        return $this->decree->number . " от " . $this->decree->date->format('d.m.Y');
    }

    public function getReason()
    {
        return $this->pivot->reason;
    }

    public function build()
    {
        return [
            'id' => $this->getId(),
            'decree_id' => $this->getDecreeId(),
            'number' => $this->getNumber(),
            'decree' => $this->getDecree()->full(),
            'type' => $this->getType(),
            'decreeText' => $this->getDecreeText(),
            'reason' => $this->getReason()
        ];
    }

    /**
     * @return mixed
     */
    public function getDecree()
    {
        return new Decree($this->decree);
    }

    public function getDecreeText()
    {
        switch ($this->pivot->type) {
            case 'enrollment':
                $tpl = getDecreeTextTemplate('enrollment');
                return massStrReplace($tpl, ['group' => $this->pivot->group_to]);
                break;
            case 'allocation':
                $tpl = getDecreeTextTemplate('allocation');
                return massStrReplace($tpl, ['group' => $this->pivot->group_from]);
                break;
            case 'transfer':
                $tpl = getDecreeTextTemplate('transfer');
                return massStrReplace($tpl, [
                    'group_from' => $this->pivot->group_from,
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'internal_transfer':
                $tpl = getDecreeTextTemplate('internal_transfer');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'internal_transfer_next_year':
                $tpl = getDecreeTextTemplate('internal_transfer_next_year');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'internal_transfer_next_term':
                $tpl = getDecreeTextTemplate('internal_transfer_next_term');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'graduation':
                $tpl = getDecreeTextTemplate('graduation');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
            case 'reinstate':
                $tpl = getDecreeTextTemplate('reinstate');
                return massStrReplace($tpl, [
                    'group_to' => $this->pivot->group_to
                ]);
                break;
        }
    }
}