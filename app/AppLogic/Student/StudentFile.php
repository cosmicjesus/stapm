<?php

namespace App\AppLogic\Student;

use App\AppLogic\Storage\StudentStorage;
use \App\Models\StudentFile as Model;
use Illuminate\Support\Collection;

class StudentFile
{
    protected $file;

    public function __construct(Model $file)
    {
        $this->file = $file;
    }

    public function getId()
    {
        return $this->file->id;
    }

    public function getName()
    {
        return $this->file->name;
    }

    public function getUuid()
    {
        return $this->file->uuid;
    }

    public function getPath()
    {
        return route('api.students.files', ['uuid' => $this->getUuid()]);
    }


    public function build(): Collection
    {
        $file = collect();
        $file->put('id', $this->getId());
        $file->put('name', $this->getName());
        $file->put('path', $this->getPath());

        return $file;
    }
}