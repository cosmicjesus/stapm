<?php

namespace App\AppLogic\Student;

use App\AppLogic\Abstracts\AbstractStudent;
use App\AppLogic\TrainingGroup\TrainingGroup;
use App\Models\Student as Model;
use Carbon\Carbon;

class Student extends AbstractStudent
{
    public function __construct(Model $student)
    {
        $this->initialization($student);
    }

    public function buildOnTable()
    {
        $student = collect();

        $student->put('id', $this->getId());
        $student->put('firstname', $this->getFirstname());
        $student->put('lastname', $this->getLastname());
        $student->put('middlename', $this->getMiddlename());
        $student->put('full_name', $this->getFullName());
        $student->put('birthday', $this->getBirthday());
        $student->put('gender', $this->getGender());
        $student->put('long_absent', $this->isLongAbsent());
        $student->put('training_group', $this->getGroup());
        $student->put('status', $this->getStatus());
        $student->put('phone', $this->getPhone());
        $student->put('preferential_categories', $this->getPrivilegedCategory());
        $student->put('enrollment_date', $this->getEnrollmentDate());
        $student->put('nominal_number', $this->getNominalNumber());

        return $student;
    }

    public function full()
    {
        $student = $this->buildOnTable();
        $student->put('avg', $this->getAvg());
        $student->put('snils', $this->getSnils());
        $student->put('inn', $this->getInn());
        $student->put('date_of_actual_transfer', $this->getDateOfActualTransfer());
        $student->put('medical_policy_number', $this->getMedicalPolicy());
        $student->put('education_id', $this->getEducationId());
        $student->put('has_certificate', $this->has_certificate());
        $student->put('start_date', $this->getStartDate());
        $student->put('give_document_date', $this->getDateGiveDocumentDate());
        $student->put('end_date', $this->getEndDate());
        $student->put('comment', $this->getComment());
        $student->put('photo', $this->getPhoto());
        $student->put('files', $this->getFiles());
        $student->put('comment', $this->getComment());
        $student->put('decrees', $this->getDecrees());
        $student->put('passport', $this->getPassportWithNationality());
        $student->put('health', $this->getHealthData());
        $student->put('addresses', $this->getAddresses());
        $student->put('education', $this->getEducationData());
        $student->put('parents', $this->getParents());
        $student->put('military', $this->getMilitaryData());
        $student->put('how_find_out', $this->getHowFindOut());
        $student->put('invited_student', $this->getInvitedStudent());
        $student->put('enrollment_decree', $this->getEnrollmentDecree());
        $student->put('reinstate_group', $this->getReinstateGroup());
        return $student;
    }

    public function buildByT2Reference()
    {
        $group = $this->getTrainingGroup();
        $decree = ($this->getDecrees()->last())['decree'];
        $parents = $this->getParents();

        return [
            'lastname' => $this->getLastname(),
            'firstname' => $this->getFirstname(),
            'middlename' => $this->getMiddlename(),
            'birthday' => dateFormat($this->getBirthday()),
            'inn' => $this->getInn(),
            'snils' => $this->getSnils(),
            'graduation_organization_name' => $this->getGraduationOrganizationName(),
            'gr_date' => dateFormat($this->getGraduationDate(), 'Y-m-d', 'Y'),
            'registration_address' => $this->buildAddress('registration'),
            'place_of_stay_address' => $this->buildAddress('place_of_stay'),
            'registration_date' => dateFormat($this->getAddress('registration', 'registration_date')),
            'phone' => $this->getPhone(),
            'passport_number' => $this->getPassportData('series') . " " . $this->getPassportData('number'),
            'issuanceDate' => dateFormat($this->getPassportData('issuanceDate')),
            'issued' => $this->getPassportData('issued'),
            'birthPlace' => $this->getPassportData('birthPlace'),
            'group' => $group->getName() . "[" . $group->getUniqueCode() . "]",
            'education' => educationLevel($this->getEducationId()),
            'created_date' => dateFormat($this->getStartDate()),
            'program' => $this->getTrainingGroup()->getProgram()->getName(),
            'decree_number' => $decree['number'],
            'decree_date' => dateFormat($decree['date']),
            'fitnessForMilitaryServices' => fitnessForMilitaryServices($this->getFitnessForMilitaryService()),
            'recruitment_center' => $this->getRecruitmentCenter()['name'],
            'parents' => $parents->map(function ($parent) {
                return [
                    0 => relationShipType($parent['relation_ship_type']),
                    1 => $parent['lastname'] . " " . $parent['firstname'] . " " . $parent['middlename'],
                    2 => ''
                ];
            })
        ];
    }

    public function update($column, $value = null)
    {
        if (is_array($column)) {
            $this->human->update($column);
            return;
        }
        $this->human->update([$column => $value]);

    }

    public function isLongAbsent()
    {
        return $this->human->long_absent;
    }

    public function getTrainingGroup()
    {
        $this->human->load('group');
        $group = $this->human->group;

        return new TrainingGroup($group);
    }

    public function getGroup()
    {
        $group = $this->getTrainingGroup();

        return ['id' => $group->getId(), 'name' => $group->getName(), 'code' => $group->getUniqueCode()];
    }

    public function getStartDate()
    {
        if (is_null($this->getDateOfActualTransfer())) {
            return $this->getTrainingGroup()->getStartDate();
        }

        return $this->getDateOfActualTransfer();
    }

    public function getDateGiveDocumentDate()
    {
        return $this->human->start_date;
    }

    public function getEndDate()
    {
        return $this->getTrainingGroup()->getEndDate();
    }

    public function getCourse()
    {
        return $this->getTrainingGroup()->getCourse();
    }

    public function getDecrees()
    {
        $this->human->load('decrees');
        return $this->human->decrees->map(function ($decree) {
            return (new StudentDecree($decree))->build();
        });
    }

    public function getEnrollmentDecree()
    {
        $decree = $this->human->decrees->where('pivot.id', $this->human->enrollment_decree_id)->values()->first();
        return !is_null($decree) ? (new StudentDecree($decree))->build() : null;
    }

    public function getEnrollmentDecreeId()
    {
        return $this->human->enrollment_decree_id;
    }

    public function getReinstateGroupId()
    {
        return $this->human->reinstate_group_id;
    }

    public function getReinstateGroup()
    {
        $this->human->load('reinstateGroup');
        if (is_null($this->human->reinstateGroup)) {
            return null;
        }
        return (new TrainingGroup($this->human->reinstateGroup))->buildToSelect();
    }

    public function getDocumentEndDate()
    {
        $birthay = $this->human->birthday;

        $age = Carbon::now()->diffInYears($birthay);

        $year = $age < 45 ? 20 : 45;

        return Carbon::create($birthay->year + $year, $birthay->month + 1, $birthay->day)->format('d.m.Y');
    }

    public function getEnrollmentDate()
    {
        return $this->human->enrollment_date->format('Y-m-d');
    }

    public static function buildById($id)
    {
        return new self(Model::query()->findOrFail($id));
    }


    protected function hasReinstateGroup()
    {
        return $this->human->reinstate_group_id;
    }

    public function transferInReinstateGroup()
    {
        $this->update([
            'reinstate_group_id' => null,
            'group_id' => $this->getReinstateGroupId()
        ]);
    }

    public function transferNextYear($decree_id)
    {
        if ($this->hasReinstateGroup()) {
            $this->transferInReinstateGroup();
            return;
        }

        $groupName = $this->getTrainingGroup()->getName();
        $this->human->decrees()->attach($decree_id, ['type' => 'internal_transfer_next_year', 'reason' => null, 'group_to' => $groupName]);
    }

    public function getNominalNumber()
    {
        return $this->human->nominal_number;
    }

}
