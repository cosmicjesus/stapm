<?php


namespace App\AppLogic\Base;


interface BaseInterface
{

    public function update($column, $value = null);

}