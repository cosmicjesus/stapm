<?php


namespace App\AppLogic\Base;


use App;
use Illuminate\Database\Eloquent\Model;

abstract class BaseService
{
    /**
     * @var $model Model
     */

    protected $model;
    protected $queryModel;

    /**
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->makeModel();
    }


    /**
     * @return string Имя класса конкретной модели
     */
    abstract function model();

    public function makeModel()
    {
        $model = App::make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function startQuery()
    {
        $this->queryModel = $this->model::query();

        return $this;
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function showException(\Exception $exception, $status = 500)
    {
        return responder()->error($exception->getCode(), $exception->getMessage())->data(['line' => $exception->getLine(), 'file' => $exception->getFile()])->respond($status);
    }
}