<?php

namespace App\AppLogic\Position;


class Position
{
    protected $position;
    protected $countEmployees;

    public function __construct($position)
    {
        /**
         * @var $position \App\Models\Position
         */

        $this->position = $position;
        $this->position->load('employee_position');
        $this->countEmployees = $this->position->employee_position->count();
    }

    public function getId()
    {
        return $this->position->id;
    }

    public function getName()
    {
        return $this->position->name;
    }

    public function getCountEmployees()
    {
        return $this->countEmployees;
    }

    public function getType()
    {
        return $this->position->position_type_id;
    }

    public function isRemove()
    {
        return $this->countEmployees < 1 ? true : false;
    }

    public function buildOnResponse()
    {
        $position = collect();

        $position->put('id', $this->getId());
        $position->put('name', $this->getName());
        $position->put('type', $this->getType());
        $position->put('countEmployees', $this->getCountEmployees());
        $position->put('isRemove', $this->isRemove());

        return $position;
    }
}