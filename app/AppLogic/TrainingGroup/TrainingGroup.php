<?php

namespace App\AppLogic\TrainingGroup;

use App\AppLogic\EducationPlan\EducationPlan;
use App\AppLogic\Student\Student;
use App\AppLogic\TrainingGroupAcademicYear\TrainingGroupAcademicYear;
use App\AppLogic\Employee\Employee;
use App\AppLogic\ProfessionProgram\ProfessionProgram;
use App\Models\TrainingGroup as GroupModel;

class TrainingGroup
{
    protected $group;
    protected $program;
    protected $educationPlan;
    protected $curator;
    protected $students;
    public $currentAcademicYear;

    public function __construct(GroupModel $group)
    {
        $this->group = $group;

        $this->loadRelations();

        $this->program = $this->group->program;
        $this->educationPlan = $this->group->education_plan;
        $this->curator = $this->group->curator;
        $this->students = $this->group->students;
        $this->currentAcademicYear = $this->group->currentAcademicYear;
    }

    public function loadRelations()
    {
        $relations = [
            'program',
            'education_plan',
            'curator',
            'students',
            'currentAcademicYear'
        ];

        foreach ($relations as $relation) {
            if (!$this->group->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
                    continue;
                }
                $this->group->load($relation);
            }

        }
    }

    public function buildToSelect()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'start_date' => $this->getStartDate(),
            'end_date' => $this->getEndDate(),
            'course' => $this->getCourse(),
            'profession_program_id' => $this->getProgram()->getId()
        ];
    }

    public function getId()
    {
        return $this->group->id;
    }

    public function getUniqueCode()
    {
        return $this->group->unique_code;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->group->pattern;
    }

    public function getName()
    {
        if ($this->group->status == 'teach') {
            $yearNumber = $this->getCurrentAcademicYear()->getNumber();
            return str_replace('{Курс}', $yearNumber, $this->group->pattern);
        } else {
            return $this->getUniqueCode();
        }
    }

    public function getMaxPeople()
    {
        return $this->group->max_people;
    }

    public function getCourse()
    {
        return $this->getCurrentAcademicYear()->getNumber();
    }

    public function getCountStudents()
    {
        return $this->students->count();
    }

    public function getCountVacancyTransfer()
    {
        $count = $this->getMaxPeople() - $this->getCountStudents();

        return $count < 0 ? 0 : $count;
    }

    public function getPeriod()
    {
        return $this->group->period;
    }

    public function getStep()
    {

        $yearNumber = $this->getCurrentAcademicYear()->getNumber();
        $termNumber = $this->getCurrentAcademicYear()->getActiveTerm()->getNumber();

        if ($this->group->term_type == 'Course') {
            return $yearNumber . " курс";

        }

        $pattern = '{course} курс {semester} семестр';


        return massStrReplace($pattern, ['course' => $yearNumber, 'semester' => $termNumber]);

    }

    public function getAcademicYears()
    {
        $this->group->load('academicYears');

        $years = $this->group->academicYears->map(function ($year) {
            return (new TrainingGroupAcademicYear($year));
        });

        return $years;
    }

    public function getCurrentAcademicYear()
    {
        return (new TrainingGroupAcademicYear($this->currentAcademicYear));
    }

    public function getCurrentYearNumber()
    {
        return $this->getCurrentAcademicYear()->getYearNumber();
    }

    public function getCurrentCalendar()
    {
        return $this->getCurrentAcademicYear()->getCalendar();
    }

    public function isTransferNextTerm()
    {

    }

    public function isTransferNextYear()
    {
        return $this->getCurrentAcademicYear()->getActiveTerm()->isLastTerm();
    }

    public function getProgram()
    {
        return new ProfessionProgram($this->program);
    }

    public function getEducationPlan()
    {
        return new EducationPlan($this->educationPlan);
    }

    public function getStartDate()
    {
        return $this->getEducationPlan()->getStartTraining();
    }

    public function getEndDate()
    {
        return $this->getEducationPlan()->getEndTraining();
    }

    public function getCurator()
    {
        return new Employee($this->curator);
    }

    public function getCuratorData()
    {
        return [
            'id' => $this->getCurator()->getId(),
            'full_name' => $this->getCurator()->getFullName()
        ];
    }

    public function getStudents()
    {
        return $this->students->map(function ($student) {
            return new Student($student);
        });
    }

    public function getTotalSumStudentAvg()
    {
        $sum = 0;

        $students = $this->getStudents();

        foreach ($students as $student) {
            $sum += is_null($student->getAvg()) ? 0 : $student->getAvg();
        }

        return $sum;
    }

    public function getGroupAvg()
    {
        $avg = $this->getTotalSumStudentAvg() / $this->getCountStudents();
        return round($avg, 2);
    }

    public function getProfessionProgram(): array
    {
        $program = new ProfessionProgram($this->group->program);

        return [
            'id' => $program->getId(),
            'name' => $program->getNameWithForm()
        ];
    }


    /**
     * @return array
     */
    public function buildToTable(): array
    {
        return [
            'id' => $this->getId(),
            'full_name' => $this->getName(),
            'unique_code' => $this->getUniqueCode(),
            'pattern' => $this->getPattern(),
            'curator' => $this->getCuratorData(),
            'course' => $this->getCourse(),
            'education_plan' => $this->getEducationPlan()->buildOnResponse(),
            'program' => $this->getProfessionProgram(),
            'academic_year' => $this->getCurrentAcademicYear()->getYearNumber()
        ];
    }

    public function full()
    {
        return [
            'id' => $this->getId(),
            'full_name' => $this->getName(),
            'curator' => $this->getCurator(),
            'academic_years' => $this->getAcademicYears()->map(function ($year) {
                return $year->full();
            })->keyBy('year'),
        ];
    }

}