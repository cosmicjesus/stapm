<?php

namespace App\AppLogic\Regulation;


class Regulation
{
    protected $regulation;

    public function __construct($regulation)
    {
        $this->regulation = $regulation;
    }

    public function getId()
    {
        return $this->regulation->id;
    }

    public function getNameOfTheSupervisory()
    {
        return $this->regulation->name_of_the_supervisory;
    }

    public function getYear()
    {
        return $this->regulation->date;
    }

    public function getComment()
    {
        return $this->regulation->comment;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        $this->regulation->load('items');

        return $this->regulation->items->map(function ($item) {
            return (new RegulationItem($item))->full();
        });
    }

    public function getFiles()
    {
        $this->regulation->load('files');

        return $this->regulation->files->map(function ($file) {
            return (new RegulationFile($file))->full();
        });
    }

    public function buildOnTable()
    {
        $regulation = collect();
        $regulation->put('id', $this->getId());
        $regulation->put('name_of_the_supervisory', $this->getNameOfTheSupervisory());
        $regulation->put('year', $this->getYear());
        $regulation->put('comment', $this->getComment());
        $regulation->put('count_violations', $this->getItems()->count());
        return $regulation;
    }

    public function full()
    {
        $regulation = $this->buildOnTable();
        $regulation->put('violations', $this->getItems());
        $regulation->put('files', $this->getFiles());

        return $regulation;
    }
}