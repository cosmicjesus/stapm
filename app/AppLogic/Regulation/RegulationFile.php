<?php

namespace App\AppLogic\Regulation;


use App\Helpers\StorageFile;

class RegulationFile
{

    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function getId()
    {
        return $this->file->id;
    }

    public function getName()
    {
        return $this->file->name;
    }

    public function getPath()
    {
        return StorageFile::getRegulationFile($this->file->path);
    }

    public function full()
    {
        return collect([
            'id' => $this->getId(),
            'name' => $this->getName(),
            'path' => $this->getPath()
        ]);
    }
}