<?php


namespace App\AppLogic\RecruitmentCenter;


use App\AppLogic\Base\BaseService;

class RecruitmentCenterService extends BaseService
{
    protected $queryModel;
    protected $classHandler;

    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query();
        $this->classHandler = RecruitmentCenter::class;
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return \App\Models\RecruitmentCenter::class;
    }

    public function all()
    {
        try {
            return $this->queryModel->get()->map(function ($center) {
                return (new $this->classHandler($center))->buildFull();
            });
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function create($input)
    {
        try {
            $this->queryModel->create($input);

            return [
                'success' => true,
                'message' => 'ВК был успешно добавлен'
            ];
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }

    public function update($id, $input)
    {
        try {
            $this->queryModel->findOrFail($id)->update($input);

            return [
                'success' => true,
                'message' => 'ВК был успешно обновлен'
            ];
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ])->setStatusCode(500);
        }
    }
}