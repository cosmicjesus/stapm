<?php

namespace App\AppLogic\RecruitmentCenter;

use App\Models\RecruitmentCenter as Model;

class RecruitmentCenter
{
    protected $center;

    public function __construct(Model $center)
    {
        $this->center = $center;
    }

    public function getId(): int
    {
        return $this->center->id;
    }

    public function getName(): string
    {
        return $this->center->name;
    }

    public function buildFull()
    {
        $center = collect();
        $center->put('id', $this->getId());
        $center->put('name', $this->getName());
        return $center;
    }
}