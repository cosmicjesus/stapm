<?php


namespace App\AppLogic\Conferences;

use App\AppLogic\FIle\File;
use App\Models\Conference as Model;

class Conference
{
    protected $conference;
    protected $files;

    public function __construct(Model $conference)
    {
        $this->conference = $conference;
        $this->conference->load('files');

        $this->files = $this->conference->files;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->conference->id;
    }

    public function getTitle(): string
    {
        return $this->conference->title;
    }

    public function getDateOfEvent(): string
    {
        return $this->conference->date_of_event->format('Y-m-d');
    }

    public function getFullText(): string
    {
        return $this->conference->full_text;
    }

    public function getSlug(): string
    {
        return $this->conference->slug;
    }

    public function getFiles()
    {
        return $this->files->map(function ($file) {
            return (new File($file))->buildFull();
        });
    }

    public function buildFull()
    {
        $conference = collect();

        $conference->put('id', $this->getId());
        $conference->put('title', $this->getTitle());
        $conference->put('slug', $this->getSlug());
        $conference->put('full_text', $this->getFullText());
        $conference->put('date_of_event', $this->getDateOfEvent());
        $conference->put('files', $this->getFiles());
        return $conference;
    }
}