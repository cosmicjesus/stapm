<?php

namespace App\AppLogic\Decree;

use App\AppLogic\Storage\DecreeStorage;
use App\Models\Decree as DecreeModel;

class Decree
{
    protected $decree;
    protected $decreesStorage;

    public function __construct(DecreeModel $decree)
    {
        $this->decree = $decree;
        $this->decreesStorage = new DecreeStorage();
    }

    public function getId()
    {
        return $this->decree->id;
    }

    public function getNumber()
    {
        return $this->decree->number;
    }

    public function getDate()
    {
        return $this->decree->date->format('Y-m-d');
    }

    public function getType()
    {
        return $this->decree->type;
    }

    public function getFile()
    {
        if (is_null($this->decree->file_path)) {
            return null;
        }

        return $this->decreesStorage->getDecree($this->decree->file_path);
    }

    public function full()
    {
        return [
            'id' => $this->getId(),
            'number' => $this->getNumber(),
            'date' => $this->getDate(),
            'type' => $this->getType(),
            'file_path' => $this->getFile()
        ];
    }

    public static function buildById($decree_id)
    {
        return (new self(DecreeModel::findOrFail($decree_id)))->full();
    }

}