<?php

namespace App\AppLogic\FileComponent;


use App\AppLogic\Storage\ComponentDocumentStorage;

class FileComponent
{
    protected $component;
    protected $storage;

    public function __construct($component)
    {
        $this->component = $component;
        $this->storage = new ComponentDocumentStorage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->component->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->component->name;
    }

    /**
     * @return int
     */
    public function getFileID(): int
    {
        return $this->component->file_id;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->component->sort;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->storage->getUrl($this->component->path);
    }

    public function getUpdatedAt()
    {
        return $this->component->updated_at->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function full()
    {
        return collect([
            'id' => $this->getId(),
            'name' => $this->getName(),
            'sort' => $this->getSort(),
            'updated_at' => $this->getUpdatedAt(),
            'file_id' => $this->getFileID(),
            'path' => $this->getPath()
        ]);
    }
}