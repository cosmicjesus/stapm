<?php

namespace App\AppLogic\ActivityLog;


use App\AppLogic\Base\BaseService;
use App\Models\Activity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class ActivityLogService extends BaseService
{

    protected $queryModel;
    protected $classHandler;

    /**
     * ActivityLogService constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->queryModel = $this->model::query()->with('causer')->orderBy('created_at', 'DESC');
        $this->classHandler = ActivityLog::class;
    }

    /**
     *
     * @return string Имя класса конкретной модели
     */
    function model()
    {
        return Activity::class;
    }

    public function filter($filterParams)
    {

        $params = json_decode($filterParams, true);
        list('dates' => $dates, 'event' => $event, 'subject_id' => $subject_id, 'log_name' => $log_name) = $params;

        $this->filterByDates($dates);
        $this->filterByEvent($event);
        $this->filterByLogName($log_name);
        $this->filterBySubjectId($subject_id);

        return $this;
    }

    protected function filterByDates($dates)
    {
        $this->queryModel->when($dates, function (Builder $q, $dates) {
            if ($dates[0] == $dates[1]) {
                $q->whereDate('created_at', $dates[0]);
            } else {
                $q->whereBetween('created_at',
                    [
                        Carbon::createFromFormat('Y-m-d', $dates[0])->format('Y-m-d H:i:s'),
                        Carbon::createFromFormat('Y-m-d', $dates[1])->setTime(23, 59, 59)->format('Y-m-d H:i:s')
                    ]);
            }
        });

        return $this;
    }

    protected function filterByEvent($event)
    {
        $this->queryModel->when($event, function (Builder $q, $event) {
            $q->where('event', $event);
        });

        return $this;
    }

    public function filterByLogName($logName)
    {
        $this->queryModel->when($logName, function (Builder $q, $logName) {
            $q->where('log_name', $logName);
        });

        return $this;
    }

    protected function filterBySubjectId($id)
    {
        $this->queryModel->when($id, function (Builder $q, $id) {
            $q->where('subject_id', $id);
        });

        return $this;
    }

    public function paginate($perPage = 15)
    {
        try {
            $activity = $this->queryModel->paginate($perPage);

            $data = $activity->map(function ($item) {
                return (new $this->classHandler($item))->build();
            });

            return [
                'total' => $activity->total(),
                'data' => $data
            ];
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public static function writeActivity($event, $description, $logName = 'default')
    {
        activity($logName)
            ->tap(function (Activity $activity) use ($event) {
                $activity->event = $event;
            })
            ->log($description);
    }

    public static function writeExportActivity($description,$logName='export')
    {
        self::writeActivity('export', $description, $logName);
    }
}