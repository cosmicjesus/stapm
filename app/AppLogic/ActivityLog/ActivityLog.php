<?php

namespace App\AppLogic\ActivityLog;


use App\AppLogic\User\User;

class ActivityLog
{
    protected $activity;
    protected $user;

    /**
     * ActivityLog constructor.
     * @param $activity
     */
    public function __construct($activity)
    {
        $activity->load('causer');
        $this->activity = $activity;

    }

    /**
     * @return string
     */
    public function getCreateAtData(): string
    {
        return $this->activity->created_at->format("d.m.Y H:i:s");
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function build()
    {
        $log = collect();
        $log->put('created_at', $this->getCreateAtData());
        $log->put('user', $this->getUserName());
        $log->put('log_name', $this->getLogName());
        $log->put('properties', $this->getProperties());
        $log->put('description', $this->getDescription());
        $log->put('event', $this->getEventName());
        $log->put('subject_id', $this->getSubjectId());
        return $log;
    }

    /**
     * @return string
     */
    public function getLogName(): string
    {
        return $this->activity->log_name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->activity->description;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->activity->event;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return is_null($this->activity->causer) ? '' : (new User($this->activity->causer))->getName();
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        $props = $this->activity->properties;

        if (!isset($props['attributes'])) {
            return [];
        }

        $megreProps = [];
        $old = !isset($props['old']) ? [] : $props['old'];
        foreach ($props['attributes'] as $key => $value) {
            $values = ['new_value' => $value];
            $values['old_value'] = isset($old[$key]) ? $old[$key] : '-';
            $megreProps[$key] = $values;
        }

        return $megreProps;

    }

    public function getSubjectId()
    {
        return $this->activity->subject_id;
    }
}
