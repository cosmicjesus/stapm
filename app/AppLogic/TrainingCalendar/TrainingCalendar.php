<?php


namespace App\AppLogic\TrainingCalendar;

use App\Models\TrainingCalendar as Model;

class TrainingCalendar
{
    protected $calendar;
    protected $periods;

    public function __construct(Model $calendar)
    {
        $this->calendar = $calendar;
        $this->calendar->load('periods');
        $this->periods = $this->calendar->periods;

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->calendar->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->calendar->name;
    }

    public function getPeriods()
    {
        return $this->periods->map(function ($period) {
            return (new TrainingCalendarItem($period))->buildFull();
        });
    }

    /**
     * @return bool
     */
    public function getInUseStatus(): bool
    {
        return true;
    }

    public function getYear()
    {
        return $this->calendar->year;
    }

    public function getTermType()
    {
        return $this->calendar->term_type;
    }

    public function isSemesterTerm()
    {
        return $this->getTermType() === 'semester';
    }

    public function isCourseTerm()
    {
        return $this->getTermType() === 'course';
    }

    public function buildFull()
    {
        $calendar = collect();

        $calendar->put('id', $this->getId());
        $calendar->put('name', $this->getName());
        $calendar->put('inUse', $this->getInUseStatus());
        $calendar->put('periods', $this->getPeriods());
        $calendar->put('year', $this->getYear());
        $calendar->put('term_type', $this->getTermType());

        return $calendar;
    }
}