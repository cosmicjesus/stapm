<?php


namespace App\AppLogic\TrainingCalendar;


use App\Models\TrainingCalendarsItem;

class TrainingCalendarItem
{

    protected $item;

    public function __construct(TrainingCalendarsItem $calendarsItem)
    {
        $this->item = $calendarsItem;
    }

    public function getId()
    {
        return $this->item->id;
    }

    public function getNumber()
    {
        return $this->item->number;
    }

    public function getStartDate()
    {
        if (is_null($this->item->start_date)) {
            return null;
        }
        return $this->item->start_date->toDateString();
    }

    public function getEndDate()
    {
        if (is_null($this->item->end_date)) {
            return null;
        }

        return $this->item->end_date->toDateString();
    }

    public function getSessionStartDate()
    {
        if (is_null($this->item->session_start_date)) {
            return null;
        }

        return $this->item->session_start_date->toDateString();
    }

    public function getSessionEndDate()
    {
        if (is_null($this->item->session_end_date)) {
            return null;
        }

        return $this->item->session_end_date->toDateString();
    }

    public function buildFull()
    {
        $item = collect();

        $item->put('id', $this->getId());
        $item->put('number', $this->getNumber());
        $item->put('start_date', $this->getStartDate());
        $item->put('end_date', $this->getEndDate());
        $item->put('session_start_date', $this->getSessionStartDate());
        $item->put('session_end_date', $this->getSessionEndDate());

        return $item;
    }
}