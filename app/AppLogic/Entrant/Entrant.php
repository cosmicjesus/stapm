<?php

namespace App\AppLogic\Entrant;


use App\AppLogic\Abstracts\AbstractStudent;
use \App\Models\Entrant as Model;

class Entrant extends AbstractStudent
{
    public function __construct(Model $entrant)
    {
        $this->initialization($entrant);
    }

    public function buildToTable()
    {
        $entrant = collect();

        $entrant->put('id', $this->getId());
        $entrant->put('lastname', $this->getLastname());
        $entrant->put('firstname', $this->getFirstname());
        $entrant->put('middlename', $this->getMiddlename());
        $entrant->put('full_name', $this->getFullName());
        $entrant->put('gender', $this->getGender());
        $entrant->put('status', $this->getStatus());
        $entrant->put('age_at_end_of_year', $this->getAgeAtTheEndOfYear());
        $entrant->put('birthday', $this->getBirthday());
        $entrant->put('need_hostel', $this->getNeedHostelStatus());
        $entrant->put('has_original', $this->getHasOriginalStatus());
        $entrant->put('has_certificate', $this->has_certificate());
        $entrant->put('snils', $this->getSnils());
        $entrant->put('contract_target_set', $this->isContractTargetSetStatus());
        $entrant->put('inn', $this->getInn());
        $entrant->put('phone', $this->getPhone());
        $entrant->put('language', $this->getLanguage());
        $entrant->put('email', $this->getEmail());
        $entrant->put('passport', $this->getPassportWithNationality());
        $entrant->put('avg', $this->getAverageScore());
        $entrant->put('start_date', $this->getStartDate());
        //$entrant->put('profession_program', $this->getProfessionProgram());
        $entrant->put('recruitment_programs', $this->getPrograms());
        $entrant->put('blank_data', $this->getBlankData());

        return $entrant;
    }

    public function full($excepted = [])
    {
        $entrant = $this->buildToTable();
        $entrant->put('how_find_out', $this->getHowFindOut());
        $entrant->put('invited_student', $this->getInvitedStudent());
        $entrant->put('start_date', $this->getStartDate());
        $entrant->put('status', $this->getStatus());
        $entrant->put('addresses', $this->getAddresses());
        $entrant->put('military', $this->getMilitaryData());
        $entrant->put('parents', $this->getParents());
        $entrant->put('comment', $this->getComment());
        $entrant->put('health', $this->getHealthData());
        $entrant->put('education', $this->getEducationData());
        $entrant->put('files', $this->getFiles());

        if (!array_key_exists('photo', $excepted)) {
            $entrant->put('photo', $this->getPhoto());
        }

        return $entrant;
    }

    public function getStartDate()
    {
        return $this->human->start_date ? $this->human->start_date->format('Y-m-d') : null;
    }

    public function getProfessionProgram()
    {
        return $this->human->recruitment_program;
    }

    public function getBlankData()
    {
        $data = collect();

        if (!$this->hasInn()) {
            $data->push('Не заполнен ИНН');
        }

        if (!$this->hasSnils()) {
            $data->push('Не заполнен СНИЛС');
        }

        if ($this->getParents()->count() < 1) {
            $data->push('Нет данных о родителях');
        };

        if ($this->hasNotPhoto()) {
            $data->push('Нет фотографии');
        }
        if (($this->getAgeAtTheEndOfYear() > 16) && $this->isMale()) {
            if (!$this->hasMilitaryDocumentId()) {
                $data->push('Нет данных о документе воинского учета');
            }
            if (!$this->hasMilitaryDocumentNumber()) {
                $data->push('Нет номера документа ВУ');
            }
        }

        return $data;
    }

    public function getPrograms()
    {
        $this->human->load('recruitment_programs.program');

        return $this->human->recruitment_programs->map(function ($recruitment_program) {
            return [
                'id' => $recruitment_program->pivot->id,
                'program_name' => $recruitment_program->program->name_with_form,
                'program_id' => $recruitment_program->program->id,
                'recruitment_program_id' => $recruitment_program->pivot->recruitment_program_id,
                'is_priority' => $recruitment_program->pivot->is_priority,
                'duration' => $recruitment_program->program->trainin_period,
                'sum_of_grades_of_specialized_disciplines' => $recruitment_program->pivot->sum_of_grades_of_specialized_disciplines
            ];
        });
    }

    public function enrollment($params)
    {
        $this->human->decrees()->attach($params['decree_id'], ['type' => 'enrollment', 'reason' => 'onConditionsOfFreeAdmission', 'group_to' => $params['group_name']]);
        $decrees = $this->human->decrees->last();

        $this->human->update([
            'status' => 'student',
            'group_id' => $params['group_id'],
            'profession_program_id' => $params['profession_program_id'],
            'enrollment_date' => $params['decree_date'],
            'nominal_number' => $params['nominal_number'],
            'enrollment_decree_id' => $decrees->pivot->id
        ]);
    }
}
