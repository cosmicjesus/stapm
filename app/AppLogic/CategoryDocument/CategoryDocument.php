<?php

namespace App\AppLogic\CategoryDocument;


use App\AppLogic\Document\Document;

class CategoryDocument
{
    protected $category;
    protected $files;

    public function __construct($category)
    {
        $this->category = $category;
        $this->category->load('documents');
        $this->category->load('childs');
        $this->files = $this->category->documents;
    }

    public function getId()
    {
        return $this->category->id;
    }

    public function getName()
    {
        return $this->category->name;
    }

    public function getSort()
    {
        return $this->category->sort;
    }

    public function getNameWithParent()
    {
        $this->category->load('parent');

        if (is_null($this->category->parent)) {
            return $this->category->name;
        }

        return $this->category->name . " | " . $this->category->parent->name;
    }

    public function hasFiles()
    {
        return $this->files->count();
    }

    public function hasActiveFiles()
    {
        $count = $this->files->where('active', true)->count();
        return $count > 0 ? true : false;
    }

    public function getActiveFiles()
    {
        return $this->files->where('active', 1)->map(function ($file) {
            return new Document($file);
        });
    }

    public function getChilds()
    {
        return $this->category->childs->where('active', true)->map(function ($category) {
            return new self($category);
        });
    }

    public function buildToDocument()
    {
        return ['id' => $this->getId(), 'name' => $this->getNameWithParent()];
    }
}