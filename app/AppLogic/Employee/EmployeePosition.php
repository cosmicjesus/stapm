<?php

namespace App\AppLogic\Employee;


use App\Models\Position;
use Carbon\Carbon;
use DateInterval;

class EmployeePosition
{
    protected $position;

    protected $pivot;

    public function __construct(Position $position)
    {
        $this->position = $position;

        $this->pivot = $position->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getName()
    {
        return $this->position->name;
    }

    public function getPosition()
    {
        return [
            'id' => $this->position->id,
            'name' => $this->position->name
        ];
    }

    public function getType()
    {
        return $this->pivot->type;
    }

    public function getStartDate()
    {
        return $this->pivot->start_date;
    }

    public function getLayoffDate()
    {
        if (is_null($this->pivot->date_layoff)) {
            return null;
        }
        return $this->pivot->date_layoff;

    }

    protected function getExperience()
    {
        return $this->pivot->experience;
    }

    public function getFullExp()
    {
        if (is_null($this->pivot->start_date)) {
            return $this->getExperience();
        }
        $diff = Carbon::now()->diffInMonths(Carbon::createFromFormat('Y-m-d', $this->pivot->start_date));

        $exp = $diff + $this->getExperience();

        return $exp;
    }

    public function getExp()
    {

        $end = is_null($this->pivot->date_layoff) ? Carbon::now() : Carbon::createFromFormat('Y-m-d', $this->pivot->date_layoff);

        $interval = Carbon::createFromFormat('Y-m-d', $this->pivot->start_date)->diff($end);

        return $this->calculateExperience($interval);
    }


    public function build()
    {
        $position = collect();

        $position->put('id', $this->getId());
        $position->put('position', $this->getPosition());
        $position->put('start_date', $this->getStartDate());
        $position->put('date_layoff', $this->getLayoffDate());
        $position->put('type', $this->getType());
        $position->put('experience', $this->getExp());

        return $position;
    }

    private function calculateExperience(DateInterval $dateInterval)
    {
        $date = [
            'years' => 0,
            'months' => 0,
            'days' => 0
        ];

        switch (true) {
            case $dateInterval->y > 0:
                $date['years'] = $dateInterval->y;
            case $dateInterval->m > 0:
                $date['months'] = $dateInterval->m;
            case $dateInterval->d > 0:
                $date['days'] = $dateInterval->d;
                break;
            default:
                $date['days'] = 1;

        }
        return $date;
    }
}