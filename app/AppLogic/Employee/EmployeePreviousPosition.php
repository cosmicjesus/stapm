<?php


namespace App\AppLogic\Employee;


class EmployeePreviousPosition
{
    private $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    public function getId()
    {
        return $this->entity->id;
    }

    public function getStartDate()
    {
        return $this->entity->start_date->format('Y-m-d');
    }

    public function getEndDate()
    {
        return $this->entity->end_date->format('Y-m-d');
    }

    public function getIsSpecialityExp()
    {
        return $this->entity->is_speciality_exp;
    }

    public function getOrganizationName()
    {
        return $this->entity->organization_name;
    }

    public function getPositionName()
    {
        return $this->entity->position_name;
    }

    public function buildFull()
    {
        return [
            'id' => $this->getId(),
            'start_date' => $this->getStartDate(),
            'end_date' => $this->getEndDate(),
            'is_speciality_exp' => $this->getIsSpecialityExp(),
            'organization_name' => $this->getOrganizationName(),
            'position_name' => $this->getPositionName()
        ];
    }
}
