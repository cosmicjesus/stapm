<?php

namespace App\AppLogic\Employee;

use App\AppLogic\QualificationCourse\QualificationCourse;
use App\BusinessLogic\Employee\EmployeeAdministrator;
use App\Helpers\StorageFile;
use App\Models\Employee as EmployeeModel;
use Carbon\Carbon;

class Employee
{

    protected $employee;
    protected $positions;
    protected $educations;
    protected $genders;
    protected $categories;
    protected $courses;
    protected $administrator;
    protected $no_photo = '/img/nophoto.png';
    protected $subjects;

    public function __construct(EmployeeModel $employee)
    {
        $this->employee = $employee;

    }


    public function buildOnTable()
    {
        $employee = collect();
        $employee->put('id', $this->getId());
        $employee->put('full_name', $this->getFullName());
        $employee->put('birthday', $this->getBirthday());
        $employee->put('age', $this->getAge());
        $employee->put('category', $this->getCategory());
        $employee->put('positions', $this->getPositions(['onlyActive' => true]));
        $employee->put('profile_url', $this->getProfileUrl());
        $employee->put('category_date', $this->getCategoryDate());
        $employee->put('general_experience', $this->getGeneralExperience());
        $employee->put('speciality_experience', $this->getSpecialtyExp());
        $employee->put('college_experience', $this->getExpInCollege());
        $employee->put('scientific_degree', $this->getScientificDegree());
        $employee->put('academic_title', $this->getAcademicTitle());
        $employee->put('use_dynamic_exp', $this->getUseDynamicExpStatus());
        return $employee;
    }

    public function full()
    {
        $collect = $this->buildOnTable();

        $collect->put('lastname', $this->getLastName());
        $collect->put('firstname', $this->getFirstName());
        $collect->put('middlename', $this->getMiddleName());
        $collect->put('start_work_date', $this->getStartWorkDate());
        $collect->put('gender', $this->getGender());
        $collect->put('education', $this->getEducations());
        $collect->put('subjects', $this->getSubjects());
        $collect->put('courses', $this->getCourses());
        $collect->put('photo', $this->getPhoto());
        $collect->put('previous_exp', $this->getPreviousExp());
        $collect->put('military_record', $this->getMilitaryRecordData());

        return $collect;
    }


    public function getId()
    {
        return $this->employee->id;
    }

    public function getLastName()
    {
        return $this->employee->lastname;
    }

    public function getFirstName()
    {
        return $this->employee->firstname;
    }

    public function getMiddleName()
    {
        return $this->employee->middlename;
    }

    public function getFullName()
    {
        return $this->employee->lastname . " " . $this->employee->firstname . " " . $this->employee->middlename;
    }

    public function getStartWorkDate()
    {
        return $this->employee->start_work_date;
    }

    public function getGender()
    {
        return $this->employee->gender;
    }

    public function getBirthday()
    {
        return $this->employee->birthday->format('Y-m-d');
    }

    public function getAge()
    {
        $age = Carbon::now()->diffInYears(Carbon::createFromFormat('Y-m-d', $this->getBirthday()));
        return $age;
    }

    public function getSlug()
    {
        return $this->employee->slug;
    }

    public function getAdministrationSort()
    {
        $this->employee->load('administrator');

        return $this->employee->administrator->sort ?? null;
    }

    public function getPositions($params = [])
    {
        $this->employee->load('positions');
        $positions = $this->employee->positions;
        if (isset($params['onlyFullTime']) && $params['onlyFullTime']) {
            $positions = $positions->where('pivot.type', 0);
        }

        if (isset($params['onlyActive']) && $params['onlyActive']) {
            $positions = $positions->where('pivot.date_layoff', null);
        }
        return $positions->map(function ($position) {
            return (new EmployeePosition($position))->build();
        });
    }

    public function getEducations()
    {
        $this->employee->load('educations');

        $educations = $this->employee->educations;

        return $educations->map(function ($education) {
            return (new \App\AppLogic\Employee\EmployeeEducation($education))->build();
        });

    }

    public function getSubjects()
    {
        $this->employee->load('subjects');

        $subjects = $this->employee->subjects;

        return $subjects->map(function ($subject) {
            return (new EmployeeSubject($subject))->build();
        });
    }

    public function getCourses()
    {
        $this->employee->load('courses');

        $courses = $this->employee->courses;
        return $courses->map(function ($course) {
            return (new QualificationCourse($course))->build();
        });
    }

    public function getPhoto()
    {
        if (is_null($this->employee->photo)) {
            return $this->no_photo;
        }
        $file = StorageFile::getEmployeePhoto($this->getId() . "/" . $this->employee->photo);
        return is_null($file) ? $this->no_photo : $file;
    }

    public function getProfileUrl()
    {
        return route('client.employee_profile', ['slug' => $this->employee->slug]);
    }

    public function hasPhoto()
    {
        return $this->employee->photo;
    }

    public function getPhones()
    {
        return $this->employee->phones;
    }

    public function getEmails()
    {
        return $this->employee->emails;
    }

    public function getCategory()
    {
        return $this->employee->category;
    }

    public function getCategoryDate()
    {
        if (is_null($this->employee->category_date)) {
            return null;
        }
        return $this->employee->category_date->format('Y-m-d');
    }

    public function getExpInCollege()
    {
        $diff = Carbon::createFromFormat('Y-m-d', $this->employee->start_work_date)->diff(Carbon::now());
        return calculateExp($diff);
    }

    public function getSpecialtyExp()
    {
        return $this->employee->specialty_exp;
    }

    public function getScientificDegree()
    {
        return $this->employee->scientific_degree;
    }

    public function getAcademicTitle()
    {
        return $this->employee->academic_title;
    }

    public function getAdministrator()
    {
        return new EmployeeAdministrator($this->administrator);
    }

    public function getGeneralExperience()
    {
        return $this->employee->general_experience;
    }

    public function getUseDynamicExpStatus()
    {
        return $this->employee->use_dynamic_exp;
    }

    public function getMilitaryStatus()
    {
        return $this->employee->military_status;
    }

    public function getMilitaryDocumentType()
    {
        return $this->employee->military_document_type;
    }

    public function getMilitaryDocumentNumber()
    {
        return $this->employee->military_document_number;
    }

    public function getFitnessOfMilitaryService()
    {
        return $this->employee->fitness_for_military_service;
    }

    public function getMilitaryRank()
    {
        return $this->employee->military_rank;
    }

    public function getMilitarySpeciality()
    {
        return $this->employee->military_specialty;
    }

    public function getMilitaryComposition()
    {
        return $this->employee->military_composition;
    }

    public function getReserveCategory()
    {
        return $this->employee->reserve_category;
    }

    public function getGroupOfAccounting()
    {
        return $this->employee->group_of_accounting;
    }

    public function getIsOnSpecialAccounting()
    {
        return $this->employee->is_on_special_accounting;
    }

    public function getHasMilitaryPreparation()
    {
        return $this->employee->has_military_preparation;
    }

    public function getRecruitmentCenter()
    {
        $this->employee->load('recruitmentCenter');

        return [
            'id' => $this->employee->recruitmentCenter->id,
            'name' => $this->employee->recruitmentCenter->name,
        ];
    }

    public function getMilitaryRecordData()
    {
        return collect([
            'military_status' => $this->getMilitaryStatus(),
            'military_document_type' => $this->getMilitaryDocumentType(),
            'military_document_number' => $this->getMilitaryDocumentNumber(),
            'fitness_for_military_service' => $this->getFitnessOfMilitaryService(),
            'military_rank' => $this->getMilitaryRank(),
            'military_specialty' => $this->getMilitarySpeciality(),
            'military_composition' => $this->getMilitaryComposition(),
            'recruitment_center' => $this->getRecruitmentCenter(),
            'group_of_accounting' => $this->getGroupOfAccounting(),
            'reserve_category' => $this->getReserveCategory(),
            'is_on_special_accounting' => $this->getIsOnSpecialAccounting(),
            'has_military_preparation' => $this->getHasMilitaryPreparation()
        ]);
    }

    public function getPreviousExp()
    {
        $this->employee->load('previous_exp');
        $previous_exp = $this->employee->previous_exp;
        return $previous_exp->map(function ($exp) {
            return (new EmployeePreviousPosition($exp))->buildFull();
        });
    }

}
