<?php

namespace App\AppLogic\Employee;


class EmployeeEducation
{
    protected $education;

    protected $pivot;

    public function __construct($education)
    {
        $this->education = $education;

        $this->pivot = $education->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getEducationId()
    {
        return $this->education->id;
    }

    public function getQualification()
    {
        return $this->pivot->qualification;
    }

    public function getDirectionOfPreparation()
    {
        return $this->pivot->direction_of_preparation;
    }

    public function getGraduationOrganizationName()
    {
        return $this->pivot->graduation_organization_name;
    }

    public function build()
    {
        $education = collect();

        $education->put('id', $this->getId());
        $education->put('education_id', $this->getEducationId());
        $education->put('qualification', $this->getQualification());
        $education->put('direction_of_preparation', $this->getDirectionOfPreparation());
        $education->put('graduation_organization_name', $this->getGraduationOrganizationName());

        return $education;
    }
}