<?php

namespace App\AppLogic\Employee;


class EmployeeSubject
{
    protected $subject;

    protected $pivot;

    public function __construct($subject)
    {
        $this->subject = $subject;

        $this->pivot = $subject->pivot;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getSubject()
    {
        return [
            'id' => $this->subject->id,
            'name' => $this->subject->name,
        ];
    }


    public function build()
    {
        $subject = collect();

        $subject->put('id', $this->getId());
        $subject->put('subject', $this->getSubject());

        return $subject;
    }

}