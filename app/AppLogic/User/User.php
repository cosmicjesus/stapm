<?php

namespace App\AppLogic\User;


use App\AppLogic\Employee\Employee;
use App\AppLogic\Student\Student;

class User
{

    protected $user;
    protected $profile;
    protected $no_photo = '/img/nophoto.png';
    protected $permissions;
    /**
     * @var string
     */
    private $profile_type;

    public function __construct($user)
    {
        $this->user = $user;

        $this->loadRelations();

        if ($this->user->profile) {
            if ($this->user->profile_type == \App\Models\Employee::class) {
                $this->profile = new Employee($this->user->profile);
                $this->profile_type = 'employee';
            } else {
                $this->profile = new Student($this->user->profile);
                $this->profile_type = 'student';
            }
        }

        $this->permissions = $this->user->permissions;

    }


    protected function loadRelations()
    {
        $relations = [
            'profile',
            'permissions'
        ];

        foreach ($relations as $relation) {
            if (!$this->user->relationLoaded($relation)) {
                if (substr_count($relation, '.') > 0) {
                    continue;
                }
                $this->user->load($relation);
            }

        }
    }

    public function getId()
    {
        return $this->user->id;
    }

    protected function hasPhofile()
    {
        return !is_null($this->profile);
    }

    public function getProfileType() :string
    {
        return $this->profile_type;
    }

    public function getName()
    {
        if ($this->hasPhofile()) {
            return $this->profile->getFullName();
        }
        return $this->user->name;
    }

    public function getPhoto()
    {
        if ($this->hasPhofile() && $this->profile->hasPhoto()) {
            return $this->profile->getPhoto();
        }

        return $this->no_photo;
    }

    public function getLogin()
    {
        return $this->user->login;
    }

    public function getBlockedStatus()
    {
        return $this->user->banned;
    }

    public function getPermissions()
    {
        return $this->permissions->map(function ($permission) {
            return $permission->name;
        });
    }
}
