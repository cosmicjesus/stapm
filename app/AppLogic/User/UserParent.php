<?php

namespace App\AppLogic\User;

use App\AppLogic\Student\Student;
use App\Models\UserParent as Model;

class UserParent
{
    protected $parent;

    public function __construct(Model $parent)
    {
        $this->parent = $parent;
    }

    public function getId()
    {
        return $this->parent->id;
    }

    public function getLastname()
    {
        return $this->parent->lastname;
    }

    public function getFirstname()
    {
        return $this->parent->firstname;
    }

    public function getMiddlename()
    {
        return $this->parent->middlename;
    }

    public function getFullName()
    {
        return $this->parent->full_name;
    }

    public function getBirthday()
    {
        return $this->parent->birthday->format('Y-m-d');
    }

    public function getGender()
    {
        return $this->parent->gender;
    }

    public function getRelationShipType()
    {
        return $this->parent->relation_ship_type;
    }

    public function getPhone()
    {
        return $this->parent->phone;
    }

    public function getSnils()
    {
        return $this->parent->snils;
    }

    public function getPlaceOfWork()
    {
        return $this->parent->place_of_work;
    }

    public function getChildren()
    {
        return $this->parent->childrens->map(function ($child) {
            return new Student($child);
        });
    }

    public function getCountChildren()
    {
        return $this->parent->childrens->count();
    }

    public function build()
    {
        $parent = collect();

        $parent->put('id', $this->getId());
        $parent->put('firstname', $this->getFirstname());
        $parent->put('lastname', $this->getLastname());
        $parent->put('middlename', $this->getMiddlename());
        $parent->put('full_name', $this->getFullName());
        $parent->put('birthday', $this->getBirthday());
        $parent->put('gender', $this->getGender());
        $parent->put('relation_ship_type', $this->getRelationShipType());
        $parent->put('phone', $this->getPhone());
        $parent->put('place_of_work', $this->getPlaceOfWork());
        $parent->put('snils', $this->getSnils());

        return $parent;
    }
}
