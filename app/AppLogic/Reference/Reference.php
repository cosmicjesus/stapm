<?php

namespace App\AppLogic\Reference;


use App\AppLogic\Student\Student;

class Reference
{
    protected $reference;
    protected $student;

    public function __construct($reference)
    {
        $this->reference = $reference;
    }

    public function buildOnTable()
    {
        $reference = collect();

        $reference->put('id', $this->getId());
        $reference->put('student', $this->getStudent());
        $reference->put('type', $this->getType());
        $reference->put('date_of_application', $this->getDateOfApplication());
        $reference->put('date_of_issue', $this->getDateOfIssue());
        $reference->put('params', $this->getParams());
        $reference->put('status', $this->getStatus());

        return $reference;
    }

    public function getId()
    {
        return $this->reference->id;
    }

    public function getType()
    {
        return $this->reference->type;
    }

    public function getStatus()
    {
        return $this->reference->status;
    }

    public function getDateOfApplication()
    {
        return $this->reference->date_of_application ? $this->reference->date_of_application->format("H:i:s d.m.Y") : '';
    }

    public function getDateOfIssue()
    {
        return $this->reference->date_of_issue ? $this->reference->date_of_issue->format("H:i:s d.m.Y") : '';
    }

    public function getDates()
    {
        $tpl = "<ul style='padding-left: 10px'>#CONTENT#</ul>";
        $content = "";

        if (!is_null($this->reference->date_of_application)) {
            $content .= "<li>Дата заказа: " . $this->reference->date_of_application->format("H:i:s d.m.Y") . "</li>";
        }

        if (!is_null($this->reference->date_of_issue)) {
            $content .= "<li>Дата выдачи: " . $this->reference->date_of_issue->format("H:i:s d.m.Y") . "</li>";
        }


        return str_replace('#CONTENT#', $content, $tpl);
    }

    public function getStudent()
    {
        $this->reference->load('student');
        return (new Student($this->reference->student))->full();
    }

    public function getStudentGroup()
    {
    }

    public function getParam($param)
    {

    }

    public function getParams()
    {
        return $this->reference->params;
    }
}