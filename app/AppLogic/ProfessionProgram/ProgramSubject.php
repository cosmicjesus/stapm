<?php

namespace App\AppLogic\ProfessionProgram;


use App\Helpers\StorageFile;
use App\Models\Subject;
use App\Models\ProgramSubject as ProgramSubjectModel;

class ProgramSubject
{
    protected $subject;
    protected $pivot;
    protected $files;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->pivot = $subject->pivot;
    }

//    protected function loadRelations()
//    {
//        $relations = [
//            'files'
//        ];
//
//        foreach ($relations as $relation) {
//            if (!$this->program->relationLoaded($relation)) {
//                if (substr_count($relation, '.') > 0) {
//                    // вложенные связи пропускаем - все равно не проверятся, а если уж есть верзнего уровня - значит и остальные должны быть
//                    continue;
//                }
//                $this->program->load($relation);
//            }
//
//        }
//    }

    public function getProfessionProgramId()
    {
        return $this->pivot->profession_program_id;
    }

    public function getFileName()
    {
        return $this->pivot->file;
    }

    public function getIndex()
    {
        return $this->pivot->index;
    }

    public function getModel()
    {
        return $this->subject;
    }

    public function getName()
    {
        return $this->subject->name;
    }

    public function getProgramAcademicYearId()
    {
        return $this->pivot->program_academic_year_id;
    }

    public function getFilePath()
    {
        if (is_null($this->getFileName())) {
            return null;
        }
        $path = $this->getProfessionProgramId() . "/" . $this->getFileName();
        return StorageFile::programFile($path);
    }

    public function getStatus()
    {
        return (bool)$this->pivot->in_revision;
    }

    public function getId()
    {
        return $this->pivot->id;
    }

    public function getSectionId()
    {
        return $this->pivot->section_id;
    }

    public function getSubjectId()
    {
        return $this->subject->id;
    }

    public function getFiles()
    {
        $files = ProgramSubjectModel::with('files')->find($this->pivot->id);
        if (is_null($files)) {
            return null;
        }
        return $files->files->map(function ($file) {
            return new ProgramFiles($file);
        });
    }

    public static function formatToProgram($subjectsArray, $withFiles = false, $onlyActiveFiles = false)
    {
        $subjects = (collect($subjectsArray))->map(function ($subject) {
            return new self($subject);
        });

        $subjectsCollect = collect();


        /**
         * @var $subject self
         */
        foreach ($subjects as $key => $subject) {
            $item = [
                'id' => $subject->getId(),
                'name' => $subject->getName(),
                'index' => $subject->getIndex(),
                'section_id' => $subject->getSectionId(),
                'program_academic_year_id' => $subject->getProgramAcademicYearId()
            ];
            if ($withFiles) {
                $files = [];
                foreach ($subject->getFiles() as $i => $file) {
                    $files[$i]['id'] = $file->getId();
                    $files[$i]['name'] = $file->getTitle();
                    $files[$i]['url'] = $file->getFileUrl();
                    $files[$i]['active'] = $file->getActive();
                    $files[$i]['components'] = $file->getComponents();
                    $files[$i]['program_id'] = $subject->getProfessionProgramId();
                    $files[$i]['updated_at'] = $file->getUpdatedDate();
                }
                if ($onlyActiveFiles) {
                    $item['files'] = array_filter($files, function ($file) {
                        return $file['active'];
                    });
                } else {
                    $item['files'] = $files;
                }
            }
            $subjectsCollect->push($item);
        }
        $sortSubjects = $subjectsCollect->sort(function ($a, $b) {
            return $a['section_id'] === $b['section_id'] ? $a['index'] <=> $b['index'] : $a['section_id'] <=> $b['section_id'];
        })->toArray();

        return $sortSubjects;
    }
}