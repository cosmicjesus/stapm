<?php

namespace App\AppLogic\ProfessionProgram;


use App\AppLogic\FileComponent\FileComponent;
use App\Helpers\StorageFile;
use App\Models\ProgramDocument;
use Illuminate\Support\Collection;

class ProgramFiles
{
    protected $document;
    protected $components;

    public function __construct(ProgramDocument $document)
    {
        $this->document = $document;
        $this->document->load('components');
        $this->components = $this->document->components;
    }

    public function getId()
    {
        return $this->document->id;
    }

    public function getTitle()
    {
        if (is_null($this->document->start_date)) {
            return $this->document->title;
        }

        $name = $this->document->start_date->format('Y');

        $typesArr = ['ppssz', 'akt', 'calendar'];
        $type = $this->document->type;
        if (in_array($type, $typesArr)) {
            switch ($this->document->start_date->day) {
                case 2:
                    return $name . " [ДО]";
                    break;
                case 3:
                    return $name . " [АОП]";
                    break;
                default:
                    return $name;
                    break;
            }
        }

        return $name;
    }

    public function getFileUrl()
    {
        if (is_null($this->document->path)) {
            return null;
        }

        return StorageFile::getProgramDocument($this->document->path);
    }

    public function getOtherName()
    {
        return $this->document->title . " " . $this->document->start_date->format('Y');
    }

    public function getType()
    {
        return $this->document->type;
    }

    public function getUpdatedDate()
    {
        return $this->document->updated_at->format('Y-m-d H:i:s');
    }

    public function getDate()
    {
        if (is_null($this->document->start_date)) {
            return null;
        }

        return $this->document->start_date->format('d.m.Y');
    }

    public function getActive()
    {
        return $this->document->active;
    }

    /**
     * @return Collection
     */
    public function getComponents(): Collection
    {
        return $this->components->map(function ($component) {
            return (new FileComponent($component))->full();
        });
    }

    /**
     * @return Collection
     */
    public function getComponentsPaths()
    {
        $components = $this->getComponents();

        return $components->map(function ($component) {
            return '../public' . $component['path'];
        });
    }
}