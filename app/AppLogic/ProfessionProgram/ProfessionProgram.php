<?php

namespace App\AppLogic\ProfessionProgram;


use App\AppLogic\EducationPlan\EducationPlan;
use App\AppLogic\Storage\ProfessionProgramStorage;
use App\AppLogic\Storage\StorageFiles;
use App\AppLogic\TrainingGroup\TrainingGroup;
use App\Helpers\StorageFile;

class ProfessionProgram
{

    protected $program;
    protected $profession;
    protected $subjects;
    protected $additionals;
    protected $basicDocuments;
    protected $storage;
    protected $priem;

    public function __construct($program)
    {
        $this->program = $program;
        $this->program->load('profession');
        $this->profession = $this->program->profession;
        $this->program->load('documents');
        $this->basicDocument = $this->program->documents;
        $this->program->load('priem');
        $this->priem = $this->program->priem;
        $this->storage = new ProfessionProgramStorage();
    }

    public function getId()
    {
        return $this->program->id;
    }

    public function getNameWithForm()
    {
        return $this->program->name_with_form;
    }

    public function getName()
    {
        return $this->getProfession()['name'];
    }

    public function getProfession()
    {
        return [
            'id' => $this->profession->id,
            'name' => $this->profession->full_name
        ];
    }

    public function getEducationFormId()
    {
        return $this->program->education_form_id;
    }

    public function getType()
    {
        return $this->program->type_id;
    }

    public function getDuration()
    {
        return $this->program->trainin_period;
    }

    public function getSort()
    {
        return $this->program->sort;
    }

    public function getOnSiteStatus()
    {
        return $this->program->on_site;
    }

    public function getQualification()
    {
        return $this->program->qualification;
    }

    public function getDescription()
    {
        return $this->program->description;
    }

    public function getLicence()
    {
        if (is_null($this->program->licence)) {
            return null;
        }

        return $this->program->licence->format('Y-m-d');
    }

    public function hasImage()
    {
        return !is_null($this->program->image);
    }

    public function getImage()
    {
        return \Storage::disk('program_files')->url($this->program->image);
    }

    public function getReduction()
    {
        return $this->program->reduction;
    }

    public function getSlug()
    {
        return $this->program->slug;
    }

    public function getTopFiftyStatus()
    {
        return $this->program->top_fifty;
    }

    public function getActiveStatus()
    {
        return $this->program->active;
    }

    public function buildOnTable()
    {
        $response = collect();

        $response->put('id', $this->getId());
        $response->put('profession', $this->getProfession());
        $response->put('educationForm', $this->getEducationFormId());
        $response->put('type', $this->getType());
        $response->put('duration', $this->getDuration());
        $response->put('sort', $this->getSort());
        $response->put('active', $this->getActiveStatus());
        $response->put('on_site', $this->getOnSiteStatus());
        $response->put('licence', $this->getLicence());
        $response->put('qualification', $this->getQualification());
        $response->put('slug', $this->getSlug());
        $response->put('has_image', $this->hasImage());
        $response->put('image', $this->getImage());
        $response->put('top_fifty', $this->getTopFiftyStatus());

        return $response;
    }

    /**
     * @return \Illuminate\Support\Collection
     * @var $subject ProgramSubject
     */

    public function getSubjects()
    {
        $this->program->load('subjects');
        $this->subjects = $this->program->subjects->map(function ($subject) {
            return new ProgramSubject($subject);
        });

        $subjects = collect();
        foreach ($this->subjects as $key => $subject) {
            $item = [
                'id' => $subject->getId(),
                'name' => $subject->getName(),
                'index' => $subject->getIndex(),
                'section_id' => $subject->getSectionId(),
                'profession_program_id' => $this->program->id
            ];
            $files = [];
            foreach ($subject->getFiles() as $i => $file) {
                $files[$i]['id'] = $file->getId();
                $files[$i]['name'] = $file->getTitle();
                $files[$i]['url'] = $file->getFileUrl();
                $files[$i]['components'] = $file->getComponents();
                $files[$i]['program_id'] = $this->getId();
                $files[$i]['updated_at'] = $file->getUpdatedDate();
            }
            $item['files'] = $files;
            $subjects->push($item);
        }
        $sortSubjects = $subjects->sort(function ($a, $b) {
            return $a['section_id'] === $b['section_id'] ? $a['index'] <=> $b['index'] : $a['section_id'] <=> $b['section_id'];
        });
        return $sortSubjects;
    }

    public function getSubjectWithOutFiles()
    {
        $this->program->load('subjects');
        $this->subjects = $this->program->subjects->map(function ($subject) {
            return new ProgramSubject($subject);
        });

        $subjects = collect();
        foreach ($this->subjects as $key => $subject) {
            $item = [
                'id' => $subject->getId(),
                'name' => $subject->getName(),
                'index' => $subject->getIndex(),
                'section_id' => $subject->getSectionId(),
                'profession_program_id' => $this->program->id
            ];
            $subjects->push($item);
        }
        $sortSubjects = $subjects->sort(function ($a, $b) {
            return $a['section_id'] === $b['section_id'] ? $a['index'] <=> $b['index'] : $a['section_id'] <=> $b['section_id'];
        });
        return $sortSubjects;
    }

    public function getBasicDocuments()
    {
        $documents = collect();
        $this->program->load('documents');
        $basicDocument = $this->program->documents;
        $standart = [
            'id' => $basicDocument->id,
            'url' => StorageFiles::getProgramDocument($basicDocument->standart),
            'type' => 'standart',
            'name' => 'Образовательный стандарт'
        ];
        $annotation = [
            'id' => $basicDocument->id,
            'url' => StorageFiles::getProgramDocument($basicDocument->annotation),
            'type' => 'annotation',
            'name' => 'Аннотации РП'
        ];
        $documents->push($standart)->push($annotation);
        return $documents;
    }

    /**
     * @return bool
     */
    public function hasStandart(): bool
    {
        return !is_null($this->basicDocument->standart) && $this->storage->hasFile($this->basicDocument->standart);
    }

    /**
     * @return string
     */
    public function getStandartUrl(): string
    {
        return $this->storage->getFileUrl($this->basicDocument->standart);
    }

    /**
     * @return bool
     */
    public function hasAnnotation(): bool
    {
        return !is_null($this->basicDocument->annotation) && $this->storage->hasFile($this->basicDocument->annotation);
    }

    /**
     * @return string
     */
    public function getAnnotationUrl(): string
    {
        return $this->storage->getFileUrl($this->basicDocument->annotation);
    }

    public function getCountStudents()
    {
        $this->program->load('students');
        return $this->program->students->count();
    }

    public function getEducationPlans()
    {
        $this->program->load('plans');

        return $this->program->plans->where('active', true)->map(function ($plan) {
            return new EducationPlan($plan);
        });
    }

    public function getTrainingGroups()
    {
        $this->program->load('groups');

        return $this->program->groups->map(function ($group) {
            return new TrainingGroup($group);
        });
    }

    public function getAdditionalDocuments()
    {
        $this->program->load('additionals');
        $additionals = $this->program->additionals;

        $data = ['akt' => [], 'calendar' => [], 'ppssz' => []];

        foreach ($additionals as $additional) {
            $file = new ProgramFiles($additional);
            $data[$additional['type']][] = [
                'id' => $file->getId(),
                'url' => $file->getFileUrl(),
                'name' => $file->getTitle(),
                'date' => $file->getDate(),
                'type' => $file->getType()
            ];
        }
        return $data;
    }

    public function full()
    {
        $program = $this->buildOnTable();
        $program->put('subjects', $this->getSubjectWithOutFiles());
        $program->put('academic_years', $this->getAcademicYears());
        $program->put('basicDocuments', $this->getBasicDocuments());
        $program->put('additionalDocuments', $this->getAdditionalDocuments());
        return $program;

    }

    public function getAcademicYears()
    {
        $this->program->load('academicYears');
        $academicYears = $this->program->academicYears;

        return $academicYears->map(function ($year) {
            $academicYear = new ProfessionProgramsAcademicYear($year);

            return [
                'id' => $academicYear->getId(),
                'year_number' => $academicYear->getYearNumber(),
                'active' => $academicYear->getActive()
            ];
        });
    }


    public function getPresentationPath()
    {
        if (is_null($this->program->presentation_path)) {
            return null;
        }
        return "<a href='" . StorageFile::getProgramDocument($this->program->presentation_path) . "' target='_blank'>Открыть</a>";
    }

    public function getVideoPresentationCode()
    {
        if (is_null($this->program->video_presentation_path)) {
            return null;
        }

        return $this->program->video_presentation_path;
    }

    public function getVideoPresentationUrl()
    {
        if (is_null($this->program->video_presentation_path)) {
            return null;
        }

        return 'https://www.youtube.com/watch?v=' . $this->program->video_presentation_path;
    }

    public function getReceptionPlan()
    {
        return $this->priem->reception_plan;
    }

    public function getStaticCountEnrolleesWithDocuments()
    {
        return $this->priem->count_with_documents;
    }

    public function getStaticCountEnrolleesWithOutDocuments()
    {
        return $this->priem->count_with_out_documents;
    }

    public function getCountEnrolleeWithDocuments()
    {
        return 0;
    }

    public function getCountEnrolleeWithOutDocuments()
    {
        return 0;
    }
}