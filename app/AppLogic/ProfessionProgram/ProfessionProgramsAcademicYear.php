<?php


namespace App\AppLogic\ProfessionProgram;

use App\Models\ProfessionProgramsAcademicYear as Model;

class ProfessionProgramsAcademicYear
{

    protected $academicYear;

    public function __construct(Model $academicYear)
    {
        $this->academicYear = $academicYear;
    }

    public function getId()
    {
        return $this->academicYear->id;
    }

    public function getYearNumber()
    {
        return $this->academicYear->year_number;
    }

    public function getActive()
    {
        return $this->academicYear->active;
    }
}