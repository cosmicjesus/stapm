<?php

namespace App\AppLogic\Pdf;

use Illuminate\Support\Str;
use Mpdf\MpdfException;

class PDF
{
    public static function mergeFiles($files)
    {

        try {
            $mpdfWrite = new Mpdf(['format' => 'A4', 'writingHTMLfooter' => false]);
            $mpdfRead = new Mpdf(['format' => 'A4', 'writingHTMLfooter' => false]);
            $mpdfWrite->SetImportUse();
            $mpdfWrite->list_indent_first_level = 0;

            $mpdfWrite->SetDisplayMode('fullpage');
            foreach ($files as $item) {
                $pagecount = $mpdfWrite->SetSourceFile($item);
                $mpdfRead->SetSourceFile($item);
                for ($i = 1; $i <= $pagecount; $i++) {
                    $readingPage = $mpdfRead->ImportPage($i);
                    $size = $mpdfRead->UseTemplate($readingPage);
                    if ($i <= $pagecount) {
                        $orientation = $size['w'] > $size['h'] ? "L" : "P";
                        $mpdfWrite->AddPage($orientation);
                    }
                    $import_page = $mpdfWrite->ImportPage($i);
                    $mpdfWrite->UseTemplate($import_page);
                }


            }
            return $mpdfWrite->Output(Str::random(18) . ".pdf", 'S');
        } catch (MpdfException $e) {
            throw $e;
        }
    }
}