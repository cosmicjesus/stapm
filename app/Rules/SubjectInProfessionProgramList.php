<?php

namespace App\Rules;

use App\Models\ProgramSubject;
use Illuminate\Contracts\Validation\Rule;

class SubjectInProfessionProgramList implements Rule
{

    protected $profession_program_id;
    protected $index;
    protected $program_academic_year_id;

    /**
     * SubjectInProfessionProgramList constructor.
     * @param int $profession_program_id
     * @param string $index
     */
    public function __construct(int $profession_program_id, string $index, int $program_academic_year_id)
    {
        $this->profession_program_id = $profession_program_id;
        $this->index = $index;
        $this->program_academic_year_id = $program_academic_year_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $hasSubject = ProgramSubject::query()->where([
            'profession_program_id' => $this->profession_program_id,
            'index' => $this->index,
            'program_academic_year_id' => $this->program_academic_year_id,
            'subject_id' => $value
        ])->count();

        return $hasSubject > 0 ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Такая дисциплина с таким индексом уже есть в листе дисциплин на выбранный учебный год. Повторное добавление невозможно';
    }
}
