<?php

namespace App\Rules;

use App\Models\SelectionCommittee;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckCommitteeRule implements Rule
{
    protected $year;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $year = Carbon::createFromFormat('Y-m-d', $value)->year;
        $this->year = $year;
        $model = SelectionCommittee::query()->whereYear('start_date', $year)->first();

        return is_null($model);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "На {$this->year} год уже сформированна комиссия, повторное формирование невозможно";
    }
}
