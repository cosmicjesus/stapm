<?php

namespace App\Widgets;

use App\Models\PriemProgram;
use App\Models\ProfessionProgram;
use Arrilot\Widgets\AbstractWidget;

class RealAvgRaiting extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $programs = ProfessionProgram::query()
            ->whereHas('priem')
            ->orderBy('sort')
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });

        $result = [];

        foreach ($programs as $program) {
            $name = "Рейтинг поданных заявлений по ";
            if ($program->getTypeId() == 2) {
                $name .= "профессии";
            } else {
                $name .= "спeциальности";
            }

            $result[] = [
                'name' => $name . " " . $program->getNameWithForm(),
                'url' => $program->getRaitingUrl()
            ];
        }


        return view('widgets.real_avg_raiting', [
            'config' => $this->config,
            'result' => $result
        ]);
    }
}
