<?php

namespace App\Widgets;

use App\Models\Help;
use Arrilot\Widgets\AbstractWidget;

class AdminNotifications extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = 0;
        $notifications = [
            'countBookedReference' => 0,
        ];
        if(checkPermissions('references.view')){
        $countReferences = Help::query()->where('status', 'booked')->count();
        $countReferences > 0 ? $notifications['countBookedReference'] += $countReferences : $notifications['countBookedReference'] = 0;}

        foreach ($notifications as $key => $notification) {
            if ($notification > 0) {
                $count++;
            }
        }
        return view('widgets.admin_notifications', [
            'config' => $this->config,
            'count' => $count,
            'notifications' => $notifications
        ]);
    }
}
