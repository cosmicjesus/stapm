<?php

namespace App\Widgets;

use App\AppLogic\Services\ConferenceService;
use App\Models\Conference;
use Arrilot\Widgets\AbstractWidget;

class ClientMenu extends AbstractWidget
{

    protected $conferenceService;

    public function __construct($config = [], ConferenceService $conferenceService)
    {

        parent::__construct($config);

        $this->conferenceService = $conferenceService;
    }

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'template' => 'top'
    ];

    public function items()
    {
        $conferences = $this->getConferences();
        $menu = [
            'index' => [
                'name' => 'Сведения об образовательной организации',
                'short_name' => 'Сведения об образовательной организации',
                'sub' => [
                    'client.basic-information' => [
                        'name' => 'Основные сведения',
                        'url' => route('client.basic-information')
                    ],
                    'client.management-structure' => [
                        'name' => 'Структура и органы управления образовательной организацией',
                        'url' => route('client.management-structure')
                    ],
                    'client.documents' => [
                        'name' => 'Документы',
                        'url' => route('client.documents')
                    ],
                    'client.education' => [
                        'name' => 'Образование',
                        'url' => route('client.education')
                    ],
                    'client.education.standarts' => [
                        'name' => 'Образовательные стандарты',
                        'url' => route('client.education.standarts')
                    ],
                    'client.employees' => [
                        'name' => 'Руководство и педагогический состав',
                        'url' => route('client.employees')
                    ],
                    'client.material' => [
                        'name' => 'Материально-техническое обеспечение',
                        'url' => route('client.material')
                    ],
                    'client.financial-and-economic-activity' => [
                        'name' => 'Финансово-хозяйственная деятельность',
                        'url' => route('client.financial-and-economic-activity')
                    ],
                    'client.hot-line' => [
                        'name' => 'Горячая линия',
                        'url' => route('client.hot-line'),
                        'parent' => true
                    ],
                    'client.paid-educational-services' => [
                        'name' => 'Платные образовательные услуги',
                        'url' => route('client.paid-educational-services'),
                    ],
                    'client.news' => [
                        'name' => 'Новости',
                        'url' => route('client.news')
                    ],
                    'client.scholarship' => [
                        'name' => 'Стипендия и иные виды материальной поддержки',
                        'url' => route('client.scholarship')
                    ],
                    'client.acceptance-transfer' => [
                        'name' => 'Вакантные места для приема (перевода)',
                        'url' => route('client.acceptance-transfer')
                    ],
                    'client.mediaAboutUs' => [
                        'name' => 'СМИ о нас',
                        'url' => route('client.mediaAboutUs')
                    ],
                    'client.may9' => [
                        'name' => 'Год памяти и славы',
                        'url' => route('client.may9')
                    ],
                    [
                        'url' => "http://3d-tour.stapm.ru/",
                        'name' => '3д Тур по техникуму'
                    ]
                ],
                'sub_routes' => [
                    'client.umk',
                    'client.program_detail',
                    'client.employee_profile',
                    'client.news.one',
                    'client.rooms',
                    'client.financial-and-economic-activity.detail'
                ]
            ],
            'enrollees' => [
                'name' => 'Абитуриенту',
                'short_name' => 'Абитуриенту',
                'sub' => [
                    'client.admission' => [
                        'name' => 'Приемная комиссия',
                        'url' => route('client.admission')
                    ],
                    'client.priem' => [
                        'name' => 'Информация о ходе приема',
                        'url' => route('client.priem')
                    ],
                    'client.partnership' => [
                        'name' => 'Социальное партнерство',
                        'url' => route('client.partnership')
                    ],
                    'client.open-door' => [
                        'name' => 'Дни открытых дверей',
                        'url' => route('client.open-door')
                    ],
                    'client.history' => [
                        'name' => 'Историческая справка',
                        'url' => route('client.history')
                    ],
                    'client.ovz' => [
                        'name' => 'Условия обучения инвалидов и лиц с ограниченными возможностями здоровья',
                        'url' => route('client.ovz')
                    ],
                    'client.feedback' => [
                        'name' => 'Обратная связь',
                        'url' => route('client.feedback')
                    ],
                    'client.submission-form' => [
                        'name' => 'Онлайн подача документов',
                        'url' => route('client.submission-form')
                    ],
                    'client.uploading-documents' => [
                        'name' => 'Догрузка документов',
                        'url' => route('client.uploading-documents')
                    ]
                ],
                'sub_routes' => [
                    'client.priem.raiting'
                ]
            ],
            'student' => [
                'name' => 'Студенту',
                'short_name' => 'Студенту',
                'sub' => [
                    'client.code-of-honor' => [
                        'name' => 'Кодекс чести',
                        'url' => route('client.code-of-honor')
                    ],
                    'client.memo-to-student' => [
                        'name' => 'Памятка студенту',
                        'url' => route('client.memo-to-student')
                    ],
                    'client.hot-line' => [
                        'name' => 'Горячая линия',
                        'url' => route('client.hot-line'),
                    ],
//                    [
//                        'name' => 'Студенческая газета',
//                        'url' => 'news-papers'
//                    ],
                    'client.anti-corruption' => [
                        'name' => 'Противодействие коррупции',
                        'url' => route('client.anti-corruption')
                    ],
                    'client.holidays' => [
                        'name' => 'Выходные и праздничные дни',
                        'url' => route('client.holidays')
                    ],
                    'client.vospitanie' => [
                        'name' => 'Средства воспитания',
                        'url' => route('client.vospitanie')
                    ],
                    'client.assistance' => [
                        'name' => 'Служба содействия трудоустройства выпускников',
                        'url' => route('client.assistance')
                    ],
                    'client.order-references' => [
                        'name' => 'Заказ справок',
                        'url' => route('client.order-references')
                    ],
                    'client.students.replaces' => [
                        'name' => 'Замены занятий',
                        'url' => route('client.students.replaces')
                    ],
                ],
                'sub_routes' => [
                    'tochka.one',
                    'news.one'
                ]
            ],
            'employees' => [
                'name' => 'Сотруднику',
                'short_name' => 'Сотруднику',
                'sub' => [
                    [
                        'url' => "https://spo.asurso.ru/",
                        'name' => 'АСУ РСО'
                    ],
                    'client.employees.information' => [
                        'url' => route('client.employees.information'),
                        'name' => 'Информация'
                    ],
                ],
                'sub_routes' => []
            ],
            'extramular' => [
                'name' => 'Заочное отделение',
                'short_name' => 'Заоч. отделение',
                'sub' => [
                    'client.extramural-department.index' => [
                        'name' => 'Основная информация',
                        'url' => route('client.extramural-department.index')
                    ],
                    'client.extramural-department.duties' => [
                        'name' => 'Обязанности студента заочника',
                        'url' => route('client.extramural-department.duties')
                    ]
                ],
                'sub_routes' => []
            ],
            'information_secutity' => [
                'name' => 'Информационная безопасность',
                'short_name' => 'Инф. безопасность',
                'sub' => [
                    'client.services.information_security' => [
                        'name' => 'Информационная безопасность',
                        'url' => route('client.services.information_security')
                    ]
                ],
                'sub_routes' => []
            ],
            'services' => [
                'name' => 'Услуги',
                'short_name' => 'Услуги',
                'sub' => [
                    'client.services.clinic' => [
                        'name' => 'Здравпункт',
                        'url' => route('client.services.clinic')
                    ],
                    'client.services.psychologist' => [
                        'name' => 'Страничка психолога',
                        'url' => route('client.services.psychologist')
                    ],
                    'client.services.canteen' => [
                        'name' => 'Столовая',
                        'url' => route('client.services.canteen')
                    ],
                    'client.services.library' => [
                        'name' => 'Библиотека',
                        'url' => route('client.services.library')
                    ],
                    'client.services.el-library' => [
                        'name' => 'Электронная библиотека',
                        'url' => route('client.services.el-library')
                    ]
                ],
                'sub_routes' => []
            ],
            'psy_pedagogical' => [
                'name' => 'Психолого-педагогическое сопровождение',
                'short_name' => 'Псих.-пед. сопр.',
                'sub' => [
                    'client.services.psychologist' => [
                        'name' => 'Страничка психолога',
                        'url' => route('client.services.psychologist')
                    ],
                    'client.vospitanie' => [
                        'name' => 'Средства воспитания',
                        'url' => route('client.vospitanie')
                    ],
                    'client.feedback' => [
                        'name' => "Обратная связь",
                        'url' => route('client.feedback')
                    ],
                    'client.services.psychologist.documents' => [
                        'name' => 'Документы',
                        'url' => route('client.services.psychologist.documents')
                    ],
                    'client.prevention'=>[
                        'name'=>'Профилактика',
                        'url'=>route('client.prevention')
                    ]

                ],
                'sub_routes' => []
            ],
            'electronic-educational-resources' => [
                'name' => 'Эл. обр. ресурсы',
                'short_name' => 'Эл. обр. ресурсы',
                'sub' => [
                    [
                        'url' => 'http://xn--80abucjiibhv9a.xn--p1ai/',
                        'name' => 'Минобр науки РФ'
                    ],
                    [
                        'url' => "http://www.edu.ru/",
                        'name' => 'Российское образование'
                    ],
                    [
                        'url' => "http://window.edu.ru/",
                        'name' => 'Единое окно'
                    ],
                    [
                        'url' => "http://school-collection.edu.ru/",
                        'name' => 'Единая коллекция ЦОР'
                    ],
                    [
                        'url' => "http://fcior.edu.ru/",
                        'name' => 'ФЦИОР'
                    ],
                    [
                        'url' => "http://scienceport.ru/",
                        'name' => 'Наука против террора'
                    ],
                ],
                'sub_routes' => []
            ],
            'conferences' => [
                'name' => 'Конкурсы и конференции',
                'short_name' => 'Конкурсы и конференции',
                'sub' => $conferences,
                'sub_routes' => [
                    'client.conferences.detail'
                ]
            ], 'may9' => [
                'name' => '2020-Год памяти и славы',
                'short_name' => '2020-Год памяти и славы',
                'sub' => [[
                    'name' => 'Год памяти и славы',
                    'url' => route('client.may9')
                ]],
                'sub_routes' => []
            ],
            'dist'=>[
                'name' => 'Дистанционное обучение',
                'short_name' => 'Дистанционное обучение',
                'sub'=>[
                    'client.distance-learning'=>[
                        'name'=>'Дистанционное обучение',
                        'url'=>route('client.distance-learning')
                    ],
                    'client.dist.documents'=>[
                        'name'=>'Об организации учебного процесса с применением электронного обучения и дистанционных
                            образовательных технологий',
                        'url'=>route('client.dist.documents')
                    ],
                    'client.dist.gia'=>[
                        'name'=>'ГИА',
                        'url'=>route('client.dist.gia')
                    ],
                    'client.distance-learning.time-table'=>[
                        'name'=>'Расписание',
                        'url'=>route('client.distance-learning.time-table')
                    ],
                    'client.dist.qualification-exam'=>[
                        'name'=>'Расписание квалификационных экзаменов',
                        'url'=>route('client.dist.qualification-exam')
                    ],
                    'client.dist.military'=>[
                        'name'=>'Военные сборы',
                        'url'=>route('client.dist.military')
                    ],
                    'client.responsible-person'=>[
                        'name'=>'Ответственные за организацю дистанционного обучения',
                        'url'=>route('client.responsible-person')
                    ],
                    'client.dist.instructions'=>[
                        'name'=>'Ответственные за организацю дистанционного обучения',
                        'url'=>route('client.dist.instructions')
                    ],
                    'client.distance-learning.hot-line'=>[
                        'name'=>'Телефоны горячей линии для родителей и студентов',
                        'url'=>route('client.distance-learning.hot-line')
                    ],
                    'client.dist.platforms'=>[
                        'name'=>'Платформы',
                        'url'=>route('client.dist.platforms')
                    ],
                    'client.dist-dosug'=>[
                        'name'=>'Досуговая деятельность',
                        'url'=>route('client.dist-dosug')
                    ],
                    'client.dist-conc'=>[
                        'name'=>'Мероприятия (конкурсы) для обучающихся с 6 по 30 апреля',
                        'url'=>route('client.dist-conc')
                    ],
                ],
                'sub_routes' => []
            ]
        ];

        return $menu;
    }

    protected function getConferences()
    {
        try {
            $conferences = $this->conferenceService->onlyActive()->buildMenuArray();

            return $conferences;
        } catch (\Exception $exception) {

        }
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $template = $this->config['template'];
        return view('widgets.client_menu.' . $template, [
            'items' => $this->items()
        ]);
    }
}
