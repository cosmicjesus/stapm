<?php

namespace App\Widgets;

use App\Helpers\CustomGitRepo;
use Arrilot\Widgets\AbstractWidget;
use Cz\Git\GitRepository;

class GitInfo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @throws \Cz\Git\GitException
     */


    public function run()
    {
        if (env('SHOW_GIT_WIDGET', 'true')) {
            $repo = new CustomGitRepo(base_path());
            $template = $this->config['template'];
            return view('widgets.git_info.' . $template, [
                'repo' => $repo,
            ]);
        }
    }
}
