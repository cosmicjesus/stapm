<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Route;

class Menu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'isInner' => false
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */

    protected function build($section = null)
    {
        $menu = [
            'index' => [
                'name' => 'Сведения об ОУ',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Основные сведения',
                        'route_name' => 'basic-information'
                    ],
                    [
                        'name' => 'Структура управления',
                        'route_name' => 'management-structure'
                    ],
                    [
                        'name' => 'Документы',
                        'route_name' => 'documents'
                    ],
                    [
                        'name' => 'Образование',
                        'route_name' => 'education'
                    ],
                    [
                        'name' => 'Образовательные стандарты',
                        'route_name' => 'education.standarts'
                    ],
                    [
                        'name' => 'Руководство и педагогический состав',
                        'route_name' => 'employees'
                    ],
                    [
                        'name' => 'Материально-техническое обеспечение',
                        'route_name' => 'material'
                    ],
                    [
                        'name' => 'Финансово-хозяйственная деятельность',
                        'route_name' => 'financial-and-economic-activity'
                    ],
                    [
                        'name' => 'Горячая линия',
                        'route_name' => 'hot-line',
                        'parent' => true
                    ],
                    [
                        'name' => 'Платные образовательные услуги',
                        'route_name' => 'paid-educational-services',
                    ],
                    [
                        'name' => 'Новости',
                        'route_name' => 'news'
                    ],
                    [
                        'name' => 'Анонсы / События',
                        'route_name' => 'announcements.index'
                    ],
                    [
                        'name' => 'Стипендия и иные виды материальной поддержки',
                        'route_name' => 'scholarship'
                    ],
                    [
                        'name' => 'Вакантные места для приема-перевода',
                        'route_name' => 'acceptance-transfer'
                    ],
                    [
                        'name' => 'Год памяти и славы',
                        'route_name' => 'client.may9'
                    ]
                ],
                'sub_routes' => [
                    'umk',
                    'program_detail',
                    'employee_profile',
                    'news.one',
                    'rooms'
                ]
            ],
            'enrollees' => [
                'name' => 'Абитуриенту',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Приемная комиссия',
                        'route_name' => 'admission'
                    ],
                    [
                        'name' => 'Информация о ходе приема',
                        'route_name' => 'priem'
                    ],
                    [
                        'name' => 'Социальное партнерство',
                        'route_name' => 'partnership'
                    ],
                    [
                        'name' => 'Дни открытых дверей',
                        'route_name' => 'open-door'
                    ],
                    [
                        'name' => 'Историческая справка',
                        'route_name' => 'history'
                    ],
                    [
                        'name' => 'Условия обучения инвалидов и лиц с ограниченными возможностями здоровья',
                        'route_name' => 'ovz'
                    ]
                ],
                'sub_routes' => [
                    'priem.raiting'
                ]
            ],
            'student' => [
                'name' => 'Студенту',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Кодекс чести',
                        'route_name' => 'code-of-honor'
                    ],
                    [
                        'name' => 'Памятка студенту',
                        'route_name' => 'memo-to-student'
                    ],
                    [
                        'name' => 'Горячая линия',
                        'route_name' => 'hot-line',
                        'parent' => true
                    ],
                    [
                        'name' => 'Точка Роста',
                        'route_name' => 'tochka'
                    ],
                    [
                        'name' => 'Студенческая газета',
                        'route_name' => 'news-papers'
                    ],
                    [
                        'name' => 'Противодействие коррупции',
                        'route_name' => 'anti-corruption'
                    ],
                    [
                        'name' => 'Выходные и праздничные дни',
                        'route_name' => 'holidays'
                    ],
                    [
                        'name' => 'Средства воспитания',
                        'route_name' => 'vospitanie'
                    ],
                    [
                        'name' => 'Служба содействия трудоустройства выпускников',
                        'route_name' => 'assistance'
                    ],
                    [
                        'name' => 'Заказ справок',
                        'route_name' => 'order-references'
                    ]
                ],
                'sub_routes' => [
                    'tochka.one',
                    'news.one'
                ]
            ],
            'employees' => [
                'name' => 'Сотруднику',
                'link' => null,
                'sub' => [
                    [
                        'link' => "https://spo.asurso.ru/",
                        'name' => 'АСУ РСО'
                    ]
                ],
                'sub_routes' => [
                ]
            ],
            'extramular' => [
                'name' => 'Заочное отделение',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Основная информация',
                        'route_name' => 'extramural-department.index'
                    ],
                    [
                        'name' => 'Обязанности студента заочника',
                        'route_name' => 'extramural-department.duties'
                    ]
                ],
                'sub_routes' => [
                ]
            ],
            'information_secutity' => [
                'name' => 'Информационная безопасность',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Информационная безопасность',
                        'route_name' => 'services.information_security'
                    ]
                ],
                'sub_routes' => [
                ]
            ],
            'services' => [
                'name' => 'Услуги',
                'link' => null,
                'sub' => [
                    [
                        'name' => 'Здравпункт',
                        'route_name' => 'services.clinic'
                    ],
                    [
                        'name' => 'Столовая',
                        'route_name' => 'services.canteen'
                    ],
                    [
                        'name' => 'Библиотека',
                        'route_name' => 'services.library'
                    ],
                    [
                        'name' => 'Электронная библиотека',
                        'route_name' => 'services.el-library'
                    ]
                ],
                'sub_routes' => [
                ]
            ],
            'electronic-educational-resources' => [
                'name' => 'Эл. обр. ресурсы',
                'link' => null,
                'sub' => [
                    [
                        'link' => 'http://xn--80abucjiibhv9a.xn--p1ai/',
                        'name' => 'Минобр науки РФ'
                    ],
                    [
                        'link' => "http://www.edu.ru/",
                        'name' => 'Российское образование'
                    ],
                    [
                        'link' => "http://window.edu.ru/",
                        'name' => 'Единое окно'
                    ],
                    [
                        'link' => "http://school-collection.edu.ru/",
                        'name' => 'Единая коллекция ЦОР'
                    ],
                    [
                        'link' => "http://fcior.edu.ru/",
                        'name' => 'ФЦИОР'
                    ],
                    [
                        'link' => "http://scienceport.ru/",
                        'name' => 'Наука против террора'
                    ],
                ],
                'sub_routes' => [
                ]
            ]
        ];
        if ($section) {
            return $menu[$section]['sub'];
        }
        return $menu;
    }

    public function run()
    {
        if ($this->config['isInner']) {
            $route = Route::currentRouteName();
            $menu = $this->build();
            $section = '';
            foreach ($menu as $key => $item) {
                foreach ($item['sub'] as $value) {
                    if (isset($value['route_name']) && ($value['route_name'] == $route)) {
                        $section = $key;
                        break;
                    }
                }
                if (isset($item['sub_routes'])) {
                    if (in_array($route, $item['sub_routes'])) {
                        $section = $key;
                        break;
                    }
                }

            }
            return view("widgets.menu.side-bar", [
                'menu' => $this->build($section),
            ]);
        }
        $template = $this->config['template'];

        return view("widgets.menu." . $template, [
            'menu' => $this->build(),
        ]);
    }
}
