<?php

namespace App\Widgets\Admin\AdminMenu;

use App\Models\Help;
use App\Models\Holiday;
use App\Models\MetaTag;
use App\Models\NewsPaper;
use App\Models\Regulation;
use App\Models\SocialPartner;
use App\Models\Student;
use App\Models\AdministrationEmployee;
use App\Models\CategoriesDocument;
use App\Models\Document;
use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\News;
use App\Models\Position;
use App\Models\PriemProgram;
use App\Models\ProfessionProgram;
use App\Models\Subject;
use App\Models\TochkaRostaItem;
use App\Models\TrainingGroup;
use Arrilot\Widgets\AbstractWidget;

class AdminMenu extends AbstractWidget
{


    protected function getItems()
    {
        $menu = [
            [
                'title' => 'Главная',
                'route' => 'admin.index',
                'target' => null,
                'custom_route' => false,
                'icon' => 'fa-tachometer',
                'count' => null,
                'visible' => null
            ],
            [
                'title' => 'Пользователи',
                'route' => 'admin.users.index',
                'target' => null,
                'custom_route' => false,
                'icon' => 'fa-user',
                'count' => null,
                'visible' => ['user.work']
            ],
            [
                'title' => 'Настройки сайта',
                'route' => 'admin.site-settings',
                'target' => null,
                'custom_route' => false,
                'icon' => 'fa-cogs',
                'count' => null,
                'visible' => ['super.admin']
            ],
            [
                'title' => 'QR Коды',
                'route' => 'admin.qr',
                'target' => null,
                'custom_route' => false,
                'icon' => 'fa-cogs',
                'count' => null,
                'visible' => ['super.admin']
            ],
            [
                'title' => 'Справки',
                'route' => 'admin.references.archive',
                'target' => null,
                'custom_route' => false,
                'icon' => 'fa-id-card-o',
                'count' => Help::query()->count(),
                'visible' => ['references.view']
            ],
            [
                'title' => 'Контингент',
                'route' => null,
                'icon' => 'fa-user',
                'count' => null,
                'visible' => null,
                'children' => [
                    [
                        'title' => 'Абитуриенты',
                        'route' => 'admin.enrollees',
                        'visible' => ['enrollees.view'],
                        'count' => Student::where('status', 'enrollee')->count(),
                    ],
                    [
                        'title' => 'Студенты',
                        'route' => 'admin.students',
                        'visible' => ['students.view'],
                        'count' => Student::whereIn('status', ['student', 'academic', 'inArmy'])->count(),
                    ],
                    [
                        'title' => 'Сотрудники',
                        'route' => 'admin.employees',
                        'visible' => ['employees.work'],
                        'count' => Employee::where('status', 'work')->count(),
                    ],
                    [
                        'title' => 'Должности',
                        'route' => 'admin.positions',
                        'visible' => null,
                        'count' => Position::count(),
                    ]

                ]
            ],
            [
                'title' => 'Образование',
                'route' => null,
                'icon' => 'fa-book',
                'count' => null,
                'visible' => null,
                'children' => [
                    [
                        'title' => 'ОПОП',
                        'route' => 'admin.programs',
                        'visible' => ['program.work'],
                        'count' => ProfessionProgram::count(),
                    ],
                    [
                        'title' => ' Учебные планы',
                        'route' => 'admin.education-plans',
                        'visible' => ['eduplans.work'],
                        'count' => EducationPlan::query()->where('active', true)->count(),
                    ],
                    [
                        'title' => 'Учебные группы',
                        'route' => 'admin.groups',
                        'visible' => ['groups.work'],
                        'count' => TrainingGroup::query()->where('status', 'teach')->count(),
                    ],
                    [
                        'title' => 'Дисциплины',
                        'route' => 'admin.subjects',
                        'visible' => ['subject.view'],
                        'count' => Subject::count(),
                    ]
                ]
            ],
            [
                'title' => 'Приемная комиссия',
                'route' => null,
                'icon' => 'fa-user',
                'count' => null,
                'visible' => null,
                'children' => [
                    [
                        'title' => 'Таблица приема',
                        'route' => 'admin.priem',
                        'visible' => ['priem.work'],
                        'count' => PriemProgram::count(),
                    ],
                    [
                        'title' => 'Отчет по абитуриентам',
                        'route' => 'admin.reports.enrollees',
                        'visible' => ['report.work'],
                        'count' => 0,
                    ]
                ]
            ],
            [
                'title' => 'Сайт',
                'route' => null,
                'icon' => 'fa-book',
                'count' => null,
                'visible' => null,
                'children' => [
                    [
                        'title' => 'Администрация техникума',
                        'route' => 'admin.administrations',
                        'visible' => ['site.administrations'],
                        'count' => AdministrationEmployee::count(),
                    ],
                    [
                        'title' => 'Социальные партнеры',
                        'route' => 'admin.social-partners',
                        'visible' => ['documents.view'],
                        'count' => SocialPartner::query()->count(),
                    ],
                    [
                        'title' => 'Новости',
                        'route' => 'admin.news',
                        'visible' => ['news.view'],
                        'count' => News::count(),
                    ],
                    [
                        'title' => 'Категории документов',
                        'route' => 'admin.documents.categories',
                        'visible' => ['documents.view'],
                        'count' => CategoriesDocument::count(),
                    ],
                    [
                        'title' => 'Документы',
                        'route' => 'admin.documents',
                        'visible' => ['documents.view'],
                        'count' => Document::count(),
                    ],
                    [
                        'title' => 'Предписания',
                        'route' => 'admin.regulations',
                        'visible' => ['documents.view'],
                        'count' => Regulation::query()->count(),
                    ],
                    [
                        'title' => 'Точка Роста',
                        'route' => 'admin.tochka-rosta',
                        'visible' => ['tocka.view'],
                        'count' => TochkaRostaItem::query()->count(),
                    ],
                    [
                        'title' => 'Газета техникума',
                        'route' => 'admin.news-papers',
                        'visible' => ['tocka.view'],
                        'count' => NewsPaper::query()->count(),
                    ],
                    [
                        'title' => 'Праздники',
                        'route' => 'admin.holidays.index',
                        'visible' => ['super.admin'],
                        'count' => Holiday::query()->count(),
                    ],
                    [
                        'title' => 'Мета-теги',
                        'route' => 'admin.meta-tags',
                        'visible' => ['super.admin'],
                        'count' => MetaTag::query()->count(),
                    ]
                ]
            ],
            [
                'title' => 'Сайт СТАПМ',
                'route' => env('APP_URL'),
                'target' => '_blank',
                'custom_route' => true,
                'icon' => 'fa-book',
                'visible' => null,
                'count' => null,
            ]
        ];

        return $menu;
    }

    protected function build()
    {
        $currentRouteName = \Route::current()
            ->getName();
        $currentRouteParams = \Route::current()
            ->parameters();

        foreach ($this->getItems() as $item) {
            $selected = ($item['route'] == $currentRouteName) ? true : false;
            $extended = (isset($item['children']) && count($item['children'])) ? true : false;

            $childSelected = false;
            if ($extended) {
                foreach ($item['children'] as &$child) {
                    if ($child['route'] == $currentRouteName) {
                        $child['selected'] = true;
                        $childSelected = $selected = true;
                    } else {
                        $child['selected'] = false;
                    }
                }
                unset($child);
            }
            $item['selected'] = $selected;
            $item['extended'] = $extended;
            $item['childSelected'] = $childSelected;
            $menu[] = $item;
        }

        return $menu;
    }

    public function run()
    {
        //dd($this->build());
        return view('widgets.admin.menu.menu', ['menu' => $this->build()]);
    }

}