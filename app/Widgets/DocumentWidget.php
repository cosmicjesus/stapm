<?php

namespace App\Widgets;

use App\AppLogic\CategoryDocument\CategoryDocument;
use App\Models\CategoriesDocument;
use App\Models\Document;
use Arrilot\Widgets\AbstractWidget;

class DocumentWidget extends AbstractWidget
{

    protected $config = [];


    public function run()
    {

        $slugs = $this->config['category_slugs'];
        $category = CategoriesDocument::query()->with('documents')
            ->where('slug', $slugs)
            ->first();
        if ($category) {
            $categoryClass = new CategoryDocument($category);
            $documents = $categoryClass->getActiveFiles();
            $template = isset($this->config['template']) ? $this->config['template'] : 'list';

            return view('widgets.document_widget.' . $template, [
                'config' => $this->config,
                'documents' => $documents
            ]);
        }
    }
}
