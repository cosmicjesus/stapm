<?php

namespace App\Widgets;

use App\BusinessLogic\User\User;
use Arrilot\Widgets\AbstractWidget;

class AdminBlock extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */

    public function getUser(){
        $user=\Auth::user();
        return new User($user);
    }

    public function run()
    {
        //

        return view('widgets.admin_block', [
            'user' => $this->getUser(),
        ]);
    }
}
