<?php

namespace App\Widgets;

use App\Models\ProfessionProgram;
use App\Models\SelectionCommittee;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Database\Query\Builder;

class AdmissionProfession extends AbstractWidget
{

    protected $config = [];


    public function run()
    {
        $lastCommittee = SelectionCommittee::query()->with('priem_programs')->orderBy('start_date', 'DESC')->first('id');
        dd($lastCommittee);
        $programs = ProfessionProgram::query()
            ->get()
            ->map(function ($program) {
                return new \App\BusinessLogic\ProfessionProgram\ProfessionProgram($program);
            });

        return view('widgets.admission_profession', [
            'config' => $this->config,
            'programs' => $programs
        ]);
    }
}
