<?php

namespace App\Widgets\Client;

use App\Models\News;
use Arrilot\Widgets\AbstractWidget;
use Carbon\Carbon;

class NewsIndex extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $news = $this->getNews();
        //dd($news);
        return view('widgets.news_index', [
            'config' => $this->config,
            'news' => $news
        ]);
    }

    public function getNews()
    {
        $date = Carbon::now()->format('Y-m-d');
        $news = [];
        $newsQuery = News::where('active', '=', 1)
            ->whereIn('type', [1, 3])
            ->where(function ($query) use ($date) {
                $query->where('visible_to', '>=', $date)
                    ->orWhereNull('visible_to');
            })
            ->orderBy('publication_date', 'DESC')
            ->take(4)
            ->get()
            ->map(function ($item) {
                return new \App\BusinessLogic\News\News($item);
            });

        $news['last'] = $newsQuery->first();
        $newsQuery->shift();
        $news['other'] = $newsQuery->all();

        return $news;
    }
}
