<?php

namespace App\Widgets;

use App\BusinessLogic\ProfessionProgram\ProfessionProgram;
use App\Models\ProfessionProgram as ProgramModel;
use Arrilot\Widgets\AbstractWidget;

class ExtramuralProfessions extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $programs = ProgramModel::query()
            ->where('education_form_id', 3)
            ->whereHas('priem')
            ->get()
            ->map(function ($program) {
                return new ProfessionProgram($program);
            });

        return view('widgets.extramural_professions', [
            'programs' => $programs,
        ]);
    }
}
