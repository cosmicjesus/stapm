<?php


namespace App\Helpers;


use Carbon\Carbon;
use Cz\Git\GitRepository;

class CustomGitRepo extends GitRepository
{
    public function getLastCommitDate()
    {

        $this->begin();
        $dates = exec('git log --pretty=\'%cd\' -n 1 2>&1');
        $this->end();
        if ($dates) {
            return Carbon::createFromTimeString($dates)->format('H:i d.m.Y');
        }
        return null;
    }
}