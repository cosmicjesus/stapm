<?php

namespace App\Helpers;


use App\Models\Education;
use App\Models\SubjectType;

class Constants
{
    public static $educationLevels = [
        1 => 'Без основного общего образования',
        2 => 'Основное общее образование',
        3 => 'Среднее общее образование',
        4 => 'Неполное среднее профессиональное образование',
        5 => 'СПО по программам подготовки квалифицированных рабочих (служащих)',
        6 => 'СПО по программам подготовки специалистов среднего звена',
        7 => 'Высшее образование - бакалавриат',
        8 => 'Высшее образование - незаконченное высшее образование',
        9 => 'Высшее образование - подготовка кадров высшей квалификации',
        10 => 'Высшее образование - специалитет, магистратура',
        11 => 'Профессиональная переподготовка',
    ];

    public static $countries = [
        'russia' => 'Россия',
        'ukraine' => 'Украина',
        'belarus' => 'Беларусь',
        'kazakhstan' => 'Казахстан',
        'moldova' => 'Молдова',
        'armenia' => 'Армения',
        'azerbaijan' => 'Азербайджан',
        'uzbekistan' => 'Узбекистан',
        'kyrgyzstan' => 'Киргизия',
        'tajikistan' => 'Таджикистан',
        'turkmenistan' => 'Туркменистан',
        'georgia' => 'Грузия'
    ];

    public static $documentsType = [
        1 => 'Вид на жительство',
        2 => 'Удостоверение беженца РФ',
        3 => 'Разрешение на вр. проживание в РФ',
        21 => 'Паспорт РФ',
        'Военный билет',
        'Вр. военный билет',
        'Вр. удостоверение беженца РФ',
        'Временное удостоверение личности гражданина РФ',
        'Документ о вр. убежище на территории РФ',
        'Другой',
        'Загранпаспорт гражданина РФ',
        'Иностранное свид-во о рождении',
        'Иностранный паспорт',
        'Свид-во о рождении',
        'Свидетельство о предоставлении вр. убежища',
        'Свидетельство о ходатайстве беженца',
        'Удостоверение беженца РФ',
        'Удостоверение военнослужащего',
        'Удостоверение личности лица без гражданства РФ',
        'Удостоверение личности отдельных категорий лиц'
    ];

    public static $howFindOut = [
        'openDoorsDay' => 'openDoorsDay',
        'internet' => 'internet',
        'fromRelative' => 'fromRelative',
        'fromEmployee' => 'fromEmployee',
        'fromStudent' => 'fromStudent',
        'other' => 'other',
    ];

    public static $newsTypes = [
        0 => 'Новости',
        1 => 'События',
        2 => 'Новости и события'
    ];

    public static $languages = [
        'english' => 'Английский язык',
        'france' => 'Французский язык',
        'german' => 'Немецкий язык'
    ];

    public static $healthCategories = [
        'hearingLoss' => "Нарушения слуха",
        'sightDisorders' => "Нарушения зрения",
        'speechDisorders' => "Нарушения речи",
        'musculoskeletalDisorders' => "Нарушения опорно-двигательного аппарата",
        'mentalDisorders' => "Задержка психического развития",
        'intellectualDisability' => "Умственная отсталость",
        'autism' => "Расстройства аутистического спектра",
        'complexDefects' => "Сложные дефекты",
        'somaticDefects' => "Соматические нарушения",
    ];

    public static $disabilities = [
        'Первая группа',
        'Вторая группа',
        'Третья группа',
        'Ребенок-инвалид'
    ];

    public static $privilegetCategories = [
        'other' => 'Другая',
        'invalid' => 'Инвалид',
        'needy_family' => 'Малообеспеченная семья',
        'incomplete_family' => 'Неполная семья',
        'full_state_support' => 'Полное государственное обеспечение',
        'without_parental_care' => 'Сирота/без родительской опеки',
    ];

    public static $relationShipTypes = [
        'mother' => 'мать',
        'father' => 'отец',
        'relative' => 'родственник',
        'adoptiveParent' => 'приёмный родитель',
        'stepparent' => 'усыновитель',
        'guardian' => 'опекун',
        'trustee' => 'попечитель',
    ];


    public static $referencesPeriod = [
        'за 3 месяца',
        'за 4 месяца',
        'за 6 месяцев',
        'за календарный год'
    ];
    public static $detailDecreeTemplate = [
        'enrollment' => 'Зачислить в группу «{group}»',
        'transfer' => 'Перевести в группу «{group}»',
        'allocation' => 'Отчислить из группы «{group}»',
        'graduation' => 'Отчислить из группы «{group}» в связи с завершением обучения',
        'internal_transfer' => 'Внутренний перевод',
        'internal_transfer_next_year' => 'Перевести на след. год обучения в составе группы «{group}»',
        'internal_transfer_next_term' => 'Перевести на след. период обучения студентов группы  «{group}»',
        'reinstate' => 'Восстановить в составе группы «{group}»'
    ];

    public static $educationForms = [
        1 => 'Очная',
        2 => 'Очно-заочная',
        3 => 'Заочная'
    ];

    public static $educationPlanExtraOptions = [
        'dual_training' => '[ДО]',
        'adapted_program' => '[АОП]'
    ];

    public static $additionalDocumentsTypes = [
        'akt' => 'Акты согласования',
        'ppssz' => 'ППССЗ/ППКРС',
        'calendar' => 'Календарные графики'
    ];

    public static $militaryDocumentType = [
        'recruitId' => 'Удостоверение гражданина подлежащего призыву на военную службу',
        'militaryId' => 'Военный билет',
        'temporary_military_id' => 'Вр. военный билет',
        'military_identification_card' => 'Удостоверение военнослужащего'
    ];
    public static $fitnessForMilitaryServices = [
        'fit' => 'А - Годен к военной службе',
        'fitWithSmallLimitations' => 'Б - Годен к военной службе с незначительными ограничениями',
        'limitedlyFit' => 'В - Ограниченно годен к военной службе',
        'temporarilyUnfit' => 'Г - Временно не годен к военной службе',
        'unfit' => 'Д - Не годен к военной службе'
    ];

    public static $shortFitnessForMilitaryServices = [
        'fit' => 'Годен к военной службе',
        'fitWithSmallLimitations' => 'Годен к военной службе с незначительными ограничениями',
        'limitedlyFit' => 'Ограниченно годен к военной службе',
        'temporarilyUnfit' => 'Временно не годен к военной службе',
        'unfit' => 'Не годен к военной службе'
    ];

    public static $militaryStatuses = [
        'NotWorthMilitaryPost' => 'Не состоит на воинском учете',
        'Recruit' => 'Призывник',
        'ServedInTheArmy' => 'Прошел службу в ВС',
        'ReleasedFromMilitaryService' => 'Освобожден от призыва в ВС ',
        'Militarian' => 'Военнообязанный'
    ];

    public static $militaryCompositions = [
        'soldiersAndSailors' => "SoldiersAndSailors",
        'ensignAndWarrantOfficers' => "EnsignAndWarrantOfficers",
        'officersAndMarshals' => "OfficersAndMarshals"
    ];

    public static $militaryRanks = [
        'conscript' => "Conscript",
        'commonSoldier' => "CommonSoldier",
        'lanceCorporal' => "LanceCorporal",
        'lanceSergeant' => "LanceSergeant",
        'sergeant' => "Sergeant",
        'staffSergeant' => "StaffSergeant",
        'pettyOfficer' => "PettyOfficer",
        'ensign' => "Ensign",
        'seniorEnsign' => "SeniorEnsign",
        'sublieutenant' => "Sublieutenant",
        'lieutenant' => "Lieutenant",
        'seniorLieutenant' => "SeniorLieutenant",
        'captain' => "Captain",
        'major' => "Major",
        'lieutenantColonel' => "LieutenantColonel",
        'colonel' => "Colonel",
        'majorGeneral' => "MajorGeneral",
        'lieutenantGeneral' => "LieutenantGeneral",
        'colonelGeneral' => "ColonelGeneral",
        'generalOfTheArmy' => "GeneralOfTheArmy",
        'marshal' => "Marshal"
    ];

    public static $groupOfAccounting = [
        'army' => 'Army',
        'fleet' => 'Fleet'
    ];

    public static $eventNames = [
        'male' => [
            'created' => 'добавлен',
            'deleted' => 'удален',
            'updated' => 'обновлен'
        ],
        'female' => [
            'created' => 'добавлена',
            'deleted' => 'удалена',
            'updated' => 'обновлена'
        ]
    ];

    public static $genders = [
        0 => 'Мужской',
        1 => 'Женский'
    ];

    public static $howFindOutSource = [
        'openDoorsDay' => 'openDoorsDay',
        'internet' => 'internet',
        'fromRelative' => 'fromRelative',
        'fromEmployee' => 'fromEmployee',
        'fromStudent' => 'fromStudent',
        'other' => 'other'
    ];

    public static $reasons = [
        'enrollment' => [
            'onConditionsOfFreeAdmission' => 'На условиях свободного приема',
            'OtherEducationForm' => 'Переведен с других форм обучения в данной ПОО',
            'OtherEducationOrganizations' => 'Переведен из других профессиональных образовательных организаций и образовательных организаций высшего образования',
            'BackFromArmy' => 'Возвратился из рядов вооруженных сил',
            'BackFromPrevExpelled' => 'Возвратился из числа ранее отчисленных',
            'BackFromAcademicVacation' => 'Возвратился из академического отпуска',
            'OtherReasons' => 'Другие причины',
        ],
        'allocation' => [
            'academic' => 'Академический отпуск',
            'endCollege' => 'В связи с окончанием Учреждения',
            'voluntarilyLeftTheEducationalInstitution' => 'Добровольно оставил образовательное учреждение',
            'deductedOnFailure' => 'Отчислен по неуспеваемости',
            'inArmy' => 'Призван в ряды вооруженных сил',
            'transferOtherCollege' => 'Переведен в другие образовательные учреждения среднего и высшего профессионального образования',
            'transferIntoOtherFormsOfTraining' => 'Переведен на другие формы обучения в данной образовательной организации',
            'dead' => 'Умер'
        ],
        'transfer' => [
            'internal_transfer'
        ]
    ];

    public static function decreeTypes()
    {
        return [
            'enrollment' => 'Зачисление',
            'transfer' => 'Перевод',
            'internal_transfer' => 'Внутренний перевод',
            'internal_transfer_next_year' => 'Перевод на следующий учебный год',
            'internal_transfer_next_term' => 'Перевод на следующий период обучения',
            'allocation' => 'Отчисление',
            'graduation' => 'Завершение обучения',
            'reinstate' => 'Восстановление'
        ];
    }

    public static function decreeTextTemplates()
    {
        return [
            'enrollment' => 'Зачислен в группу «{group}»',
            'transfer' => 'Переведен из группы «{group_from}» в группу «{group_to}»',
            'allocation' => 'Отчислить из группы «{group}»',
            'internal_transfer' => 'Перевести на след. год обучения в составе группы «{group_to}»',
            'reinstate' => 'Допущен до занятий в составе группы «{group_to}»',
            'graduation' => 'Отчислить из группы «{group_to}» в связи с завершением обучения',
            'internal_transfer_next_year' => 'Перевести на след. год обучения в составе группы «{group_to}»',
            'internal_transfer_next_term' => 'Перевести на след. период обучения студентов группы  «{group_to}»',
        ];
    }

    public static function detailDecreeTemplate()
    {
        return [
            'enrollment' => 'Зачислить в группу «{group}»',
            'transfer' => 'Перевести из группы «{group}»',
            'allocation' => 'Отчислить из группы «{group}»',
            'internal_transfer' => 'Перевести на след. год обучения в составе группы «{group}»',
            'internal_transfer_next_year' => 'Перевести на след. год обучения в составе группы «{group}»',
            'internal_transfer_next_term' => 'Перевести на след. период обучения студентов группы  «{group}»',
            'reinstate' => 'Восстановить в составе группы «{group}»'
        ];
    }

    public static function subjectTypes()
    {
        return SubjectType::query()->get()->toArray();
    }

    public static function getEducations()
    {
        return Education::query()->get();
    }

    public static function relationShipTypes()
    {
        return [
            'мать',
            'отец',
            'родственник',
            'опекун',
            'приёмный родитель',
            'усыновитель',
            'попечитель'
        ];
    }

}
