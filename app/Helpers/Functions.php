<?php

//Файл с глобальными функциями


//Функция правильного окончания исчисляемого
use App\Helpers\CustomGitRepo;
use App\Models\PositionType;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic;


use \App\Helpers\Constants;

function checkPermissions($permissions)
{
    if (is_null($permissions)) {
        return true;
    }
    if (auth()->user()->can($permissions) || auth()->user()->can('super.admin')) {
        return true;
    }
    return false;
}

if (!function_exists('positionTypes')) {


    function positionTypes()
    {
        $model = PositionType::all()
            ->keyBy('id')
            ->map(function ($position) {
                return $position->name;
            })->toArray();

        return $model;
    }

}

if (!function_exists('genders')) {

    function genders()
    {
        $genders = [
            'Мужской',
            'Женский'
        ];

        return $genders;
    }

}

function getGender($gender)
{
    $genders = genders();

    return isset($genders[$gender]) ? $genders[$gender] : $genders[0];
}

if (!function_exists('categories')) {

    function categories()
    {
        $categories = [
            1 => 'Без категории',
            2 => 'Экзамен на соответствие',
            3 => 'Первая категория',
            4 => 'Высшая категория'
        ];

        return $categories;
    }

}


if (!function_exists('types')) {

    function types()
    {
        $types = [
            'Штатный',
            'Внутренний совместитель',
            'Внешний совместитель'
        ];

        return $types;
    }

}

if (!function_exists('carbon')) {
    function carbon($date = null, $format = null): Carbon
    {
        if ($date) {
            return Carbon::createFromFormat($format, $date);
        }

        return Carbon::now();
    }
}

function getTypes($type)
{
    $types = types();

    return isset($types[$type]) ? $types[$type] : null;
}

function facilities()
{
    return [
        'Инвалид',
        'Сирота/без родительской опеки',
        'Многодетная семья',
        'Малообеспеченная семья'
    ];
}

function getImageOrientation($width, $height)
{
    if ($height == $width) {
        return 'box';
    } elseif ($height < $width) {
        return "horizontal";
    } else {
        return "vertical";
    }
}

function getPositions()
{
    $positions = \App\Models\Position::orderBy('name', 'ASC')
        ->get()
        ->keyBy('id')
        ->map(function ($position) {
            return $position->name;
        })->toArray();

    return $positions;
}

function getEmployees()
{
    $employees = \App\Models\Employee::orderBy('lastname', 'ASC')->orderBy('firstname', 'ASC')
        ->get()
        ->keyBy('id')
        ->map(function ($employee) {
            return $employee->lastname . " " . $employee->firstname . " " . $employee->middlename;
        })->toArray();

    return $employees;
}

function employeePhotoWidth($width, $height)
{
    $orientation = getImageOrientation($width, $height);

    $widths = [
        'box' => 250,
        'horizontal' => 300,
        'vertical' => 200
    ];

    return $widths[$orientation];
}

function newsPhotoWidth($width, $height)
{
    $orientation = getImageOrientation($width, $height);

    $widths = [
        'box' => 250,
        'horizontal' => 400,
        'vertical' => 200
    ];

    return $widths[$orientation];
}

function resizeNewsPhoto($file)
{
    $image = ImageManagerStatic::make($file);
    $image_data = $image->exif('COMPUTED');
    $width = newsPhotoWidth($image_data['Width'], $image_data['Height']);

    $resize_image = $image->resize($width, null, function ($constraint) {
        return $constraint->aspectRatio();
    })->encode('jpg');

    return $resize_image;
}

function calculateExp(DateInterval $dateInterval)
{

    $date = [];

    switch (true) {
        case $dateInterval->y > 0:
            $date[] = $dateInterval->y . " " . getNumEnding($dateInterval->y, ['год', 'года', 'лет']);
        case $dateInterval->m > 0:
            $date[] = $dateInterval->m . " " . getNumEnding($dateInterval->m, ['месяц', 'месяца', 'месяцев']);
        case $dateInterval->d > 0:
            $date[] = $dateInterval->d . " " . getNumEnding($dateInterval->d, ['день', 'дня', 'дней']);
            break;
        default:
            $date[] = "1 " . getNumEnding(1, ['день', 'дня', 'дней']);

    }
    return implode(" ", $date);
}

function massStrReplace($str, array $params)
{

    $newstr = null;

    foreach ($params as $key => $param) {
        if (is_null($newstr)) {
            $newstr = str_replace('{' . $key . '}', $param, $str);
        } else {
            $newstr = str_replace('{' . $key . '}', $param, $newstr);
        }

    }

    return $newstr;
}

function getSubjects()
{
    $query = \App\Models\Subject::query()
        ->with('type')
        ->orderBy('name')
        ->get();

    $subjects = [];
    foreach ($query as $item) {
        $subjects[$item->type->id]['title'] = $item->type->name;
    }

    foreach ($query as $q) {
        $subjects[$q->type_id]['items'][] = ['id' => $q->id, 'name' => $q->name];
    }


    return $subjects;
}

function getDocTitles()
{
    $titles = ['Рабочая программа', 'Методические указания'];
    return $titles;
}

function getTrainigGroups($withExpelled = false)
{
    $groupsModel = \App\Models\TrainingGroup::query();
    if (!$withExpelled) {
        $groupsModel->where('status', 'teach');
    }
    $groups = $groupsModel->orderBy('profession_program_id')
        ->orderBy('course')
        ->get()
        ->keyBy('id')
        ->map(function ($group) {
            return str_replace('{Курс}', $group->course, $group->pattern);
        })
        ->toArray();

    return $groups;
}

function getOtherDocumentName($key)
{
    $titles = [
        'akt' => 'Акт согласования',
        'ppssz' => 'ППССЗ/ППКРС',
        'calendar' => 'Календарный график'
    ];

    return $titles[$key];
}

function studentsStatus()
{
    return [
        'enrollee' => 'Абитуриент',
        'student' => 'Обучается',
        'inArmy' => 'В РА',
        'academic' => 'В академическом отпуске',
        'expelled' => 'Отчислен'
    ];
}

function getStudentStatus($status)
{
    $statusArr = studentsStatus();

    return isset($statusArr[$status]) ? $statusArr[$status] : array_shift($statusArr);
}


function allocationReasons()
{
    return getReasons('allocation');
}

function reasons()
{
    $reasons = [
        'enrollment' => [
            'onConditionsOfFreeAdmission' => 'На условиях свободного приема',
            'OtherEducationForm' => 'Переведен с других форм обучения в данной ПОО',
            'OtherEducationOrganizations' => 'Переведен из другой образовательной организации',
            'BackFromArmy' => 'Возвратился из рядов вооруженных сил',
            'BackFromPrevExpelled' => 'Возвратился из числа ранее отчисленных',
            'BackFromAcademicVacation' => 'Возвратился из академического отпуска',
            'OtherReasons' => 'Другие причины',
        ],
        'allocation' => [
            'academic' => 'Академический отпуск',
            'endCollege' => 'В связи с окончанием Учреждения',
            'voluntarilyLeftTheEducationalInstitution' => 'Добровольно оставил образовательное учреждение',
            'deductedOnFailure' => 'Отчислен по неуспеваемости',
            'inArmy' => 'Призван в ряды вооруженных сил',
            'transferOtherCollege' => 'Переведен в другие образовательные учреждения среднего и высшего профессионального образования',
            'transferIntoOtherFormsOfTraining' => 'Переведен на другие формы обучения в данной образовательной организации',
            'dead' => 'Умер'
        ],
        'transfer' => [
            'internal_transfer'
        ]
    ];

    return $reasons;
}

function getReasons($type, $reason = null)
{
    $reasons = reasons();

    if (!is_null($reason)) {
        return $reasons[$type][$reason];
    }

    return $reasons[$type];

}

function getDecreeType($type)
{
    $types = Constants::decreeTypes();

    return $types[$type];
}

function getDecreeTextTemplate($tplKey)
{
    $templates = Constants::decreeTextTemplates();

    return array_key_exists($tplKey, $templates) ? $templates[$tplKey] : $tplKey;
}

function getSubjectTypes()
{
    return Constants::subjectTypes();
}

function getEducation($education_id)
{
    $levels = Constants::$educationLevels;

    return $levels[$education_id] ?? $education_id;
}

function getEducations()
{
    return Constants::getEducations();
}

function getEducationsOnSelect()
{
    return (Constants::getEducations())->keyBy('id')->map(function ($edu) {
        return $edu->name;
    })->toArray();
}

function getParamKey($key)
{
    $params = [
        'date' => 'Дата',
        'count' => 'Кол-во',
        'decree' => 'Приказ',
        'end_date' => 'Окончание обучения',
        'start_date' => 'Начало обучения',
        'end_training' => 'Окончание обучения',
        'start_training' => 'Начало обучения',
        'decree_id' => 'Id Приказа',
        'student_id' => 'Id Студента',
        'period' => 'Период'
    ];

    return isset($params[$key]) ? $params[$key] : 'Нет ключа';
}

function getCountrues()
{
    return Constants::$countries;
}

function getCountry($key, $toAsuRso = false)
{
    if (is_null($key)) {
        if ($toAsuRso) {
            return '';
        } else {
            return 'Не указано';
        }
    }

    $countrues = getCountrues();

    return isset($countrues[$key]) ? $countrues[$key] : '';
}


function getLanguages()
{
    return Constants::$languages;
}

function getLanguage($key, $toAsuRso = false)
{
    if (is_null($key)) {
        if ($toAsuRso) {
            return '';
        } else {
            return 'Не указано';
        }
    }

    $languages = getLanguages();

    return array_key_exists($key, $languages) ? $languages[$key] : $key;
}

function getEduFormToAsuRso($key)
{
    $forms = [
        1 => 'Очное',
        2 => 'Очно-заочное',
        3 => 'Заочное'
    ];

    return $forms[$key];
}

function getPrivilegeCategories()
{
    return Constants::$privilegetCategories;
}

function getPrivilegeCategory($key, $toAsuRso = false)
{
    if (is_null($key)) {
        if ($toAsuRso) {
            return '';
        } else {
            return 'без льгот';
        }
    }

    $categories = getPrivilegeCategories();

    return array_key_exists($key, $categories) ? $categories[$key] : $key;
}

//getHealthCategories

function getHealthCategories()
{
    return Constants::$healthCategories;
}

function getHealthCategory($key, $toAsuRso = false)
{
    if (is_null($key)) {
        if ($toAsuRso) {
            return '';
        } else {
            return 'Не указано';
        }
    }

    $categories = getHealthCategories();

    return array_key_exists($key, $categories) ? $categories[$key] : $key;
}

function getDisabilities()
{
    return Constants::$disabilities;
}

function getDisability($key, $toAsuRso = false)
{
    if (is_null($key)) {
        if ($toAsuRso) {
            return '';
        } else {
            return 'Нет';
        }
    }

    $disabilities = getDisabilities();

    return array_key_exists($key, $disabilities) ? $disabilities[$key] : $key;
}

function getDocumentsType()
{
    return Constants::$documentsType;
}

function getRelationShipTypes()
{
    return Constants::$relationShipTypes;
}

function getRelationShipType($key)
{
    if (is_null($key)) {
        return '';
    }

    $types = getRelationShipTypes();

    return isset($types[$key]) ? $types[$key] : '';
}

function getDocumentType($key)
{
    if (is_null($key)) {
        return '';
    }

    $types = getDocumentsType();

    return isset($types[$key]) ? $types[$key] : '';
}

function checkStrlen($string, $length, $operator)
{
    return strlen(str_replace(' ', '', $string)) . "" . $operator . " " . $length;
}

function getDomain()
{
    if (isset($_SERVER['HTTP_HOST'])) {
        $domainStr = env('DOMAINS');
        $domainArr = explode('|', $domainStr);
        $host = $_SERVER['HTTP_HOST'];
        if (in_array($host, $domainArr)) {
            return $host;
        } else {
            return env('ADMIN_DOMAIN');
        }
    }
}

function getNewsSections()
{
    return Constants::$newsTypes;
}

function getNewsSection($key)
{
    $sections = Constants::$newsTypes;

    return isset($sections[$key]) ? $sections[$key] : '';
}

function getReferencesPeriod()
{
    return Constants::$referencesPeriod;
}

function getReferencePeriod($key)
{
    $periods = Constants::$referencesPeriod;

    return isset($periods[$key]) ? $periods[$key] : '';
}

function dateFormat($dateStr, $dateFormat = 'Y-m-d', $responseFormat = 'd.m.Y')
{
    if (!is_null($dateStr) && (strlen($dateStr) > 0)) {
        return \Carbon\Carbon::createFromFormat($dateFormat, $dateStr)->format($responseFormat);
    }
}

function getNationality($nationality)
{
    $countries = Constants::$countries;

    return $countries[$nationality] ?? $nationality;
}

function getTermKey($decreeDate)
{
    $date = Carbon::createFromFormat('Y-m-d', $decreeDate);

    $startYear = 2012;
    $maxYear = Carbon::now()->format('Y');

    $table = [];
    for ($i = $startYear; $i <= $maxYear; $i++) {
        $start = Carbon::create($i, 8, 1);
        $end = Carbon::create($i + 1, 7, 31);

        $table[$i] = [$start, $end];
    }
    $keyYear = null;

    foreach ($table as $key => $item) {
        if ($date >= $item[0] && $date <= $item[1]) {
            $keyYear = $key;
            break;
        }
    }

    return $keyYear;
}

function getCurrentPeriod($date)
{

    $carbonDate = Carbon::createFromFormat('Y-m-d', $date);

    return $carbonDate->month < 8 ? $carbonDate->addYear(-1)->year : $carbonDate->year;

}

function lastCommitId()
{
    try {
        $repo = new CustomGitRepo(base_path());
        return $repo->getLastCommitId();
    } catch (\Cz\Git\GitException $e) {
    }
}