<?php

namespace App\Helpers;


class AsuRsoImportMapping
{
    public static function gender($gender)
    {
        $genders = [
            'Male' => 0,
            'Female' => 1
        ];

        return $genders[$gender];
    }

    public static function education($education)
    {
        $educations = [
            'Basic' => 2,
            'Secondary' => 3,
            'VocationalBasic' => 6,
            'VocationalSecondary' => 5,
            'IncompleteSecondary' => 4,
            'HigherBachelor' => 7,
            'HigherMagistracy' => 10,
            'HigherPostGraduate' => 9,
            'HigherIncomplete' => 8,
            'WithoutBasic' => 1
        ];

        return $educations[$education];
    }

    public static function positionType($type)
    {
        $types = [
            'Executives' => 1,
            'TeachingStuff' => 2,
            'SupportStuff' => 3,
            'ServiceStuff' => 4
        ];

        return $types[$type];
    }

    public static function category($category)
    {
        $cat = [
            'First' => 2,
            'Higher' => 3
        ];

        return $cat[$category];
    }
}