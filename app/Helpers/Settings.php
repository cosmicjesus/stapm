<?php

namespace App\Helpers;


use App\Models\Student;
use Illuminate\Support\Facades\Cache;

class Settings
{
    public static function ShowOldSiteMessage()
    {
        return Cache::get('show_old_site_message');
    }

    public static function licenceNum()
    {
        return Cache::get('licence_num');
    }

    public static function licenceDate()
    {
        return Cache::get('licence_date');
    }

    public static function orderHelpsFromSite()
    {
        return Cache::get('order_helps_from_site');
    }

    public static function svidAccredNum()
    {
        return Cache::get('accred_num');
    }

    public static function svidAccredDate()
    {
        return Cache::get('accred_date');
    }

    public static function countPlaceOnMan()
    {
        return Cache::get('count_place_on_man');
    }

    public static function countPlaceOnWoman()
    {
        return Cache::get('count_place_on_woman');
    }

    public static function allPlaceOnHostel()
    {
        return self::countPlaceOnMan() + self::countPlaceOnWoman();
    }

    public static function studentLastNames()
    {
        return Cache::get('studentLastNames', function () {
            $data = self::getStudentData('lastname');
            Cache::forever('studentLastNames', $data);
            return $data;
        });
    }

    public static function studentFirstNames()
    {
        return Cache::get('studentFirstNames', function () {
            $data = self::getStudentData('firstname');
            Cache::forever('studentFirstNames', $data);
            return $data;
        });
    }

    public static function studentMiddleNames()
    {
        return Cache::get('studentMiddleNames', function () {
            $data = self::getStudentData('middlename');
            Cache::forever('studentMiddleNames', $data);
            return $data;
        });
    }

    private static function getStudentData($field)
    {
        return Student::query()->select($field)
            ->whereNotIn('status', ['expelled', 'enrollee'])
            ->whereNotNull($field)
            ->orderBy($field)
            ->distinct()
            ->get()
            ->map(function ($item) use ($field) {
                return $item->$field;
            });
    }

    public static function refreshStudentDataCache()
    {
        $data = [
            [
                'field' => 'firstname',
                'cache_field_name' => 'studentFirstNames'
            ],
            [
                'field' => 'middlename',
                'cache_field_name' => 'studentMiddleNames'
            ],
            [
                'field' => 'lastname',
                'cache_field_name' => 'studentLastNames'
            ]
        ];

        foreach ($data as $item) {
            $resourse = self::getStudentData($item['field']);
            Cache::forever($item['cache_field_name'], $resourse);
        }
    }

    public static function ShowStaticCountEnrollees()
    {
        return Cache::get('show_static_count_enrollees');
    }

    public static function realAvgRait()
    {
        return Cache::get('real_avg_rait');
    }
}