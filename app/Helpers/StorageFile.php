<?php

namespace App\Helpers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\File;
use Storage;

class StorageFile
{

    protected static $programDocumentsPath = '/{id}/documents';
    protected static $programFilesPath = '/{id}/files';

    public static function programFile($path)
    {
        if (Storage::disk('program_files')->exists($path)) {
            return Storage::disk('program_files')->url($path);
        } else {
            return false;
        }

    }

    public static function putEmployeePhoto($model, $file, $extension = 'jpg')
    {
        try {
            $name = $model->slug . "-" . rand(0, 99) . "." . $extension;
            Storage::disk('employees')->put($model->id . '/' . $name, $file);
            return $name;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function getEmployeePhoto($file)
    {
        if (Storage::disk('employees')->exists($file)) {
            return Storage::disk('employees')->url($file);
        } else {
            return null;
        }

    }


    public static function deleteEducationPlan($path)
    {
        Storage::disk('program_files')->delete($path);
    }

    public static function putEducationPlan($model, $file)
    {
        if (!is_null($model->full_path)) {
            self::deleteEducationPlan($model->full_path);
        }
        return Storage::disk('program_files')->putFile($model->profession_program_id . '/education-plans', new File($file));
    }

    public static function getEducationPlan($path)
    {
        return Storage::disk('program_files')->url($path);
    }

    public static function deleteEmployeePhoto($file)
    {
        try {
            Storage::disk('employees')->delete($file);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public static function getNewsImage($path)
    {
        return Storage::disk('news')->url($path);
    }

    public static function putNewsImage($file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteNewsImage($old);
        }
        $resize = resizeNewsPhoto($file);
        //dd($file,$resize);
        $name = str_random(10) . ".jpg";

        Storage::disk('news')->put($name, $resize);
        return $name;
    }

    public static function deleteNewsImage($path)
    {
        Storage::disk('news')->delete($path);
    }

    public static function getProgramDocument($file)
    {
        return Storage::disk('program_files')->url($file);
    }

    public static function putProgramDocument($file, $id, $old = null)
    {
        $agrs = func_get_args();

        if (!is_null($old)) {
            self::deleteProgramDocument($old);
        }

        $path = self::$programDocumentsPath;

        if (isset($agrs[3]) && $agrs[3] == 'file') {
            $path = self::$programFilesPath;
        }

        return Storage::disk('program_files')->putFile(str_replace('{id}', $id, $path), new File($file));
    }

    public static function deleteProgramDocument($file)
    {
        Storage::disk('program_files')->delete($file);
        return true;
    }

    public static function getDocument($path)
    {
        return Storage::disk('documents')->url($path);
    }

    public static function putDocument($file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteDocument($old);
        }

        $path = Storage::disk('documents')->put('', new File($file));
        return $path;
    }

    public static function deleteDocument($file)
    {
        Storage::disk('documents')->delete($file);
        return true;
    }

    public static function getNewsFile($url)
    {
        return self::getNewsImage($url);
    }

    public static function putNewsFile($id, $file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteNewsFile($old);
        }

        $path = Storage::disk('news')->put($id, new File($file));

        return $path;
    }

    public static function deleteNewsFile($path)
    {
        self::deleteNewsImage($path);
    }

    public static function deleteNewsFilesDir($name)
    {
        Storage::disk('news')->deleteDirectory($name);
    }


    public static function getTochkaRostaImage($path)
    {
        return Storage::disk('tochka-rosta')->url($path);
    }

    public static function putTochkaRostaImage($file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteTochkaRostaImage($old);
        }
        $resize = resizeNewsPhoto($file);

        //dd($file,$resize);
        $name = str_random(13) . ".jpg";

        Storage::disk('tochka-rosta')->put($name, $resize);
        return $name;
    }

    public static function deleteTochkaRostaImage($path)
    {
        Storage::disk('tochka-rosta')->delete($path);
    }

    public static function getTochkaRostaPhoto($path)
    {
        return self::getTochkaRostaImage($path);
    }

    public static function deleteTochkaRostaPhoto($path)
    {
        self::deleteTochkaRostaImage($path);
    }

    public static function putTochkaRostaPhoto($id, $file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteTochkaRostaPhoto($old);
        }

        $path = Storage::disk('tochka-rosta')->put($id, new File($file));

        return $path;
    }

    public static function getRegulationFile($url)
    {
        return Storage::disk('regulations')->url($url);
    }

    public static function putRegulationFile($id, $file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteRegulationFile($old);
        }

        $path = Storage::disk('regulations')->put($id, new File($file));

        return $path;
    }

    public static function deleteRegulationFile($path)
    {
        self::deleteNewsImage($path);
    }

    public static function getPartnerFile($path)
    {
        return Storage::disk('social-partners')->url($path);
    }

    public static function putPartnerFile($id, $file, $old = null)
    {
        if (!is_null($old)) {
            self::deletePartnerFile($old);
        }

        $path = Storage::disk('social-partners')->put($id, new File($file));

        return $path;
    }

    public static function deletePartnerFile($path)
    {
        Storage::disk('social-partners')->delete($path);
    }

    public static function deletePartnerDir($dir)
    {
        Storage::disk('social-partners')->deleteDirectory($dir);
    }

    public static function getNewsPaperFile($url)
    {
        return Storage::disk('news-papers-files')->url($url);
    }

    public static function putNewsPaperFile($id, $file, $old = null)
    {
        if (!is_null($old)) {
            self::deleteNewsPaperFile($old);
        }

        $path = Storage::disk('news-papers-files')->put($id, new File($file));

        return $path;
    }

    public static function deleteNewsPaperFile($path)
    {
        Storage::disk('news-papers-files')->delete($path);
    }

    public static function deleteNewsPaperDir($dir)
    {
        Storage::disk('news-papers-files')->deleteDirectory($dir);
    }

}