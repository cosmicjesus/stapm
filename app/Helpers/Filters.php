<?php

use App\Helpers\Constants;

if (!function_exists('getNumEnding')) {
    /**
     * @param $number
     * @param $endingArray
     * @return mixed
     */
    function getNumEnding($number, $endingArray, $withNmber = false)
    {
        $defNumber = $number;

        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $endingArray[0];
                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $endingArray[1];
                    break;
                default:
                    $ending = $endingArray[2];
            }
        }

        if ($withNmber) {
            return $defNumber . " " . $ending;
        } else {
            return $ending;
        }
    }

}

/**
 * @param $period
 * @return string
 */
function convertTrainingPeriod($period)
{
    $year = floor($period / 12);
    $months = $period % 12;
    $year_format = $year > 0 ? $year . " " . getNumEnding($year, ['год', 'года', 'лет']) : '';
    $month_format = $months > 0 ? $months . " " . getNumEnding($months, ['месяц', 'месяца', 'месяцев']) : '';
    $period = $year_format . " " . $month_format;
    return trim($period);
}

if (!function_exists('educationForms')) {
    function educationForms($form_id)
    {
        $forms = Constants::$educationForms;

        return isset($forms[$form_id]) ? $forms[$form_id] : $form_id;
    }
}

if (!function_exists('educationLevel')) {
    function educationLevel($level)
    {
        $levels = Constants::$educationLevels;

        return array_key_exists($level, $levels) ? $levels[$level] : $level;
    }
}

if (!function_exists('educationPlanExtraOption')) {
    function educationPlanExtraOption($option_key)
    {
        $options = Constants::$educationPlanExtraOptions;

        return isset($options[$option_key]) ? $options[$option_key] : $option_key;
    }
}

if (!function_exists('additionalDocumentsType')) {
    function additionalDocumentsType($type)
    {
        $types = Constants::$additionalDocumentsTypes;

        return isset($types[$type]) ? $types[$type] : $type;
    }
}

if (!function_exists('militaryDocumentType')) {
    function militaryDocumentType($type)
    {
        $types = Constants::$militaryDocumentType;

        return isset($types[$type]) ? $types[$type] : $type;
    }
}

if (!function_exists('fitnessForMilitaryServices')) {
    function fitnessForMilitaryServices($type)
    {
        $types = Constants::$fitnessForMilitaryServices;

        return isset($types[$type]) ? $types[$type] : $type;
    }
}


if (!function_exists('eventName')) {
    function eventName($eventName, $type = 'male')
    {
        $eventNames = Constants::$eventNames[$type];

        return isset($eventNames[$eventName]) ? $eventNames[$eventName] : $eventName;
    }
}

if (!function_exists('gender')) {
    function gender($key)
    {
        $genders = Constants::$genders;

        return array_key_exists($key, $genders) ? $genders[$key] : $key;
    }
}

if (!function_exists('relationShipType')) {
    function relationShipType($key)
    {
        $relations = Constants::$relationShipTypes;

        return array_key_exists($key, $relations) ? $relations[$key] : $key;
    }
}

if (!function_exists('reason')) {
    function reason($type, $reason)
    {
        $reasons = Constants::$reasons[$type];

        return array_key_exists($reason, $reasons) ? $reasons[$reason] : $reason;
    }
}
if (!function_exists('nationality')) {
    function nationality($nationality)
    {
        $countries = Constants::$countries;

        return $countries[$nationality] ?? $nationality;
    }
}

if (!function_exists('documentType')) {
    function documentType($type)
    {
        $types = Constants::$documentsType;

        return array_key_exists($type, $types) ? $types[$type] : $type;
    }
}

if (!function_exists('militaryStatus')) {
    function militaryStatus($status)
    {
        $militaryStatuses = Constants::$militaryStatuses;

        return array_key_exists($status, $militaryStatuses) ? $militaryStatuses[$status] : $status;
    }
}

if (!function_exists('howFindOutSource')) {
    function howFindOutSource($source)
    {
        $sources = [];
        $howFindOutSources = Constants::$howFindOutSource;

        $sources[$howFindOutSources['openDoorsDay']] = 'День открытых дверей';
        $sources[$howFindOutSources['internet']] = 'Сайт/интернет';
        $sources[$howFindOutSources['fromRelative']] = 'От родственника';
        $sources[$howFindOutSources['fromEmployee']] = 'От сотрудника';
        $sources[$howFindOutSources['fromStudent']] = 'От студента';
        $sources[$howFindOutSources['other']] = 'Иное';

        return array_key_exists($source, $sources) ? $sources[$source] : $source;
    }
}

if (!function_exists('shortFitForMilitaryServices')) {
    function shortFitForMilitaryServices($category)
    {
        $categories = Constants::$shortFitnessForMilitaryServices;
        return array_key_exists($category, $categories) ? $categories[$category] : $category;
    }
}

if (!function_exists('getMilitaryRank')) {
    function getMilitaryRank($rank)
    {
        $militaryRanks = Constants::$militaryRanks;
        $ranks = [];
        $ranks[$militaryRanks['conscript']] = "Подлежит призыву";
        $ranks[$militaryRanks['commonSoldier']] = "Рядовой (матрос)";
        $ranks[$militaryRanks['lanceCorporal']] = "Ефрейтор (старший матрос)";
        $ranks[$militaryRanks['lanceSergeant']] = "Младший сержант (старшина 2-й статьи)";
        $ranks[$militaryRanks['sergeant']] = "Сержант (старшина 1-й статьи)";
        $ranks[$militaryRanks['staffSergeant']] = "Старший сержант (главный старшина)";
        $ranks[$militaryRanks['pettyOfficer']] = "Старшина (главный корабельный старшина)";
        $ranks[$militaryRanks['ensign']] = "Прапорщик (мичман)";
        $ranks[$militaryRanks['seniorEnsign']] = "Старший прапорщик (старший мичман)";
        $ranks[$militaryRanks['sublieutenant']] = "Младший лейтенант";
        $ranks[$militaryRanks['lieutenant']] = "Лейтенант";
        $ranks[$militaryRanks['seniorLieutenant']] = "Старший лейтенант";
        $ranks[$militaryRanks['major']] = "Майор (капитан 3-го ранга)";
        $ranks[$militaryRanks['lieutenantColonel']] = "Подполковник (капитан 2-го ранга)";
        $ranks[$militaryRanks['colonel']] = "Полковник (капитан 1-го ранга)";
        $ranks[$militaryRanks['majorGeneral']] = "Генерал-майор (контр-адмирал)";
        $ranks[$militaryRanks['lieutenantGeneral']] = "Генерал-лейтенант (вице-адмирал)";
        $ranks[$militaryRanks['colonelGeneral']] = "Генерал-полковник (адмирал)";
        $ranks[$militaryRanks['generalOfTheArmy']] = "Генерал армии (адмирал флота)";
        $ranks[$militaryRanks['marshal']] = "Маршал Российской Федерации (Адмирал Флота РФ)";
        $ranks[$militaryRanks['captain']] = "Капитан (капитан-лейтенант)";

        return array_key_exists($rank, $ranks) ? $ranks[$rank] : $rank;
    }
}
//group_of_accounting

if (!function_exists('groupOfAccounting')) {
    function groupOfAccounting($group)
    {
        $groupsOfAccounting = Constants::$groupOfAccounting;
        $groups = [];
        $groups[$groupsOfAccounting['army']] = "РА";
        $groups[$groupsOfAccounting['fleet']] = "ВМФ";

        return array_key_exists($group, $groups) ? $groups[$group] : $group;
    }
}

if (!function_exists('getMilitaryComposition')) {
    function getMilitaryComposition($composition)
    {
        $militaryCompositions = Constants::$militaryCompositions;
        $compositions = [];
        $compositions[$militaryCompositions['soldiersAndSailors']] = 'Солдаты, матросы, сержанты, старшины';
        $compositions[$militaryCompositions['ensignAndWarrantOfficers']] = 'Прапорщики и мичманы';
        $compositions[$militaryCompositions['officersAndMarshals']] = 'Офицеры и маршалы';

        return array_key_exists($composition, $compositions) ? $compositions[$composition] : $composition;
    }
}