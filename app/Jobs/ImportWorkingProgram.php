<?php

namespace App\Jobs;

use App\Models\ProgramDocument;
use App\Models\ProgramSubject;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class ImportWorkingProgram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $model;
    protected $pathTpl='{program_id}/program/{file}';
    protected $path;
    protected $log;
    protected $logPath = '/logs/ImportWorkingProgram.log';

    public function __construct(ProgramSubject $programSubject)
    {
        $this->model=$programSubject;
        $this->file=$programSubject->file;
        $this->log = new Logger('FtpSyncLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->path=massStrReplace($this->pathTpl,
            [
                'program_id'=>$programSubject->profession_program_id,
                'file'=>$programSubject->file
            ]
        );

    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $name=$this->model->profession_program_id.'/files/'.str_random().'.pdf';

        $this->log->addInfo('Ищем файл ' . $this->model->file);
        if (Storage::disk('sync')->has($this->path)) {
            $this->log->addInfo('Файл ' . $this->model->file . ' найден. Грузим');
            $content = Storage::disk('sync')->get($this->path);

            Storage::disk('program_files')->put($name, $content);
            $this->log->addInfo('Файл ' . $this->model->file . ' загружен и сохранен. Пишем в базу');
            $this->model->files()->save(
                new ProgramDocument(['title'=>'Рабочая программа','path'=>$name])
            );
            $this->log->addInfo('Файл ' . $this->model->file . ' сохранен в базу.');
        } else {
            $this->log->addInfo('Файл ' . $this->model->file . ' не найден');
        }
    }
}
