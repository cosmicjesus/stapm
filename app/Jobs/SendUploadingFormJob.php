<?php

namespace App\Jobs;

use App\Mail\EntrantMail;
use App\Mail\SendUploadingDocumentsFormMail;
use App\Models\Entrant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendUploadingFormJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Entrant $entrant;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Entrant $entrant)
    {
        $this->entrant=$entrant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->entrant->email)->send(new SendUploadingDocumentsFormMail($this->entrant));
    }
}
