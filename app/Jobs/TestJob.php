<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class TestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $log;
    protected $logPath = '/logs/QueueTestLog.log';
    protected $name;

    public function __construct()
    {

        $this->log = new Logger('QueueTestLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = \Carbon\Carbon::now()->format('H:i:s d.m.Y');

        $this->log->info('Команда выполнена в ' . $now);
    }
}
