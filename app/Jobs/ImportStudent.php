<?php

namespace App\Jobs;

use App\Models\Decree;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportStudent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $student;
    private $educations;
    private $groups;

    public function __construct(array $student, $educations, $groups)
    {
        $this->student = $student;
        $this->educations = $educations;
        $this->groups = $groups;
    }


    public function handle()
    {
        try {

            $array = $this->student;
            $educations = $this->educations;
            $groups = $this->groups;

            $decree = Decree::query()->firstOrCreate([
                'number' => $array[9],
                'date' => Carbon::createFromFormat('d.m.Y', $array[10])->format('Y-m-d')
            ]);

            $groupToString = str_replace('{Курс}', 1, $groups[$array[8]]['pattern']);

            $student = new Student();
            $student->lastname = $array[0];
            $student->firstname = $array[1];
            $student->middlename = $array[2];
            $student->birthday = Carbon::createFromFormat('d.m.Y', $array[3])->format('Y-m-d');
            $student->gender = array_search($array[4], genders());
            $student->education_id = $educations[$array[20]]['id'];
            $student->group_id = $groups[$array[8]]['id'];
            $student->profession_program_id = $groups[$array[8]]['profession_program_id'];
            $student->status = 'student';
            $student->avg = empty($array[24]) ? null : $array[24];
            $student->passport_issuance_date = empty($array[33]) ? null : Carbon::createFromFormat('d.m.Y', $array[33])->format('Y-m-d');
            $student->start_date = Carbon::createFromFormat('d.m.Y', $array[21])->format('Y-m-d');
            $student->graduation_date = Carbon::createFromFormat('d.m.Y', $array[21])->format('Y-m-d');
            $student->has_original = true;
            $student->has_certificate = true;
            $student->need_hostel = false;
            $student->phone = empty($array[26]) ? null : $array[26];
            $student->person_with_disabilities = false;
            $student->contract_target_set = false;
            $student->save();
            $student->decrees()->attach($decree->id, ['group_to' => $groupToString]);
        }catch (\Exception $exception){
            dd($exception->getMessage());
        }
    }
}
