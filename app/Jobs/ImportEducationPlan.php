<?php

namespace App\Jobs;

use App\Helpers\StorageFile;
use App\Models\EducationPlan;
use Illuminate\Bus\Queueable;
use Illuminate\Http\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class ImportEducationPlan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $plan;
    protected $pathTpl = '{program_id}/plans/{file}';
    protected $path;
    protected $log;
    protected $logPath = '/logs/EducationPlansLog.log';
    protected $name;

    public function __construct(EducationPlan $educationPlan)
    {
        $this->plan = $educationPlan;
        $this->log = new Logger('FtpSyncLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->path = massStrReplace($this->pathTpl, ['program_id' => $educationPlan->profession_program_id, 'file' => $educationPlan->file]);
        $this->name = $educationPlan->profession_program_id . "/education-plans/" . str_random(10) . '.pdf';
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $model = $this->plan;
        $name = $model->profession_program_id . "/education-plans/" . str_random(10) . '.pdf';

        $this->log->addInfo('Ищем файл ' . $model->file . ' ' . $name);
        if (Storage::disk('sync')->has($this->path)) {
            $this->log->addInfo('Файл ' . $model->file . ' найден. Грузим');
            $content = Storage::disk('sync')->get($this->path);

            Storage::disk('program_files')->put($name, $content);
            $this->log->addInfo('Файл ' . $model->file . ' загружен и сохранен. Пишем в базу');
            $model->full_path = $name;
            $model->save();
            $this->log->addInfo('Файл ' . $model->file . ' сохранен в базу.');
        } else {
            $this->log->addInfo('Файл ' . $model->file . ' не найден');
        }

    }
}
