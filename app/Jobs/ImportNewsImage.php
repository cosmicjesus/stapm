<?php

namespace App\Jobs;

use App\Models\News;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use League\Flysystem\FileNotFoundException;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Storage;

class ImportNewsImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $model;
    private $imageName;
    private $pathTpl = 'news_img/{image}';
    private $path;
    private $log;
    private $logPath = '/logs/ImportNewsImages.log';

    public function __construct(News $newsModel, $imageName)
    {
        $this->model = $newsModel;
        $this->imageName = $imageName;
        $this->log = new Logger('NewsImageImport');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->path = massStrReplace($this->pathTpl,
            [
                'image' => $imageName
            ]
        );
    }


    public function handle()
    {
        $name = str_random(8) . ".jpg";
        $this->log->addInfo('Ищем файл с именем ' . $this->imageName);
        if (Storage::disk('old')->has($this->path)) {
            $this->log->addInfo('Файл ' . $this->imageName . ' найден. Грузим');
            try {
                $content = Storage::disk('old')->get($this->path);
                Storage::disk('news')->put($name,$content);
                $this->log->addInfo('Файл ' . $this->imageName . ' загружен. Новое имя '.$name.' Пишем в базу');
                $this->model->image=$name;
                $this->model->save();
                $this->log->addInfo('Запись завершена. Обновлено изображение у новости с названием '.$this->model->title);
            } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
                $this->log->addInfo('Произошла ошибка при загрузке файла ' . $this->imageName . '. Установлено дефолтное изображение. Error:'.$e->getMessage());
            }
        } else {
            $this->log->addInfo('Файл ' . $this->imageName . ' не найден. Установлено дефолтное изображение');
        }
    }
}
