<?php

namespace App\Jobs;

use App\Models\ProfessionProgram;
use App\Models\ProgramDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class ImportOtherDocument implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $pathTpl='{program_id}/documents/{file}';
    protected $path;
    protected $log;
    protected $logPath = '/logs/ImportOtherDocuments.log';

    public function __construct($array)
    {
        $this->data=$array;
        $this->log = new Logger('FtpSyncLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->path=massStrReplace($this->pathTpl,
            [
                'program_id'=>$array['program'],
                'file'=>$array['file']
            ]
        );
    }


    public function handle()
    {
        $name=$this->data['program']."/documents/".str_random(11).".pdf";
        $this->log->addInfo('Ищем файл ' . $this->data['file']);
        if (Storage::disk('sync')->has($this->path)) {
            $this->log->addInfo('Файл ' . $this->data['file'] . ' найден. Грузим');
            try {
                $content = Storage::disk('sync')->get($this->path);
            } catch (FileNotFoundException $e) {
            }

            Storage::disk('program_files')->put($name, $content);
            $this->log->addInfo('Файл ' . $this->data['file'] . ' загружен и сохранен. Пишем в базу');
            $model=ProfessionProgram::with('otherDocuments')->findOrFail($this->data['program']);
            $model->otherDocuments()->save(
                new ProgramDocument(
                    [
                        'title'=>$this->data['title'],
                        'path'=>$name,
                        'type'=>$this->data['type'],
                        'start_date'=>$this->data['date']
                    ]
                )
            );
            $this->log->addInfo('Файл ' . $this->data['file'] . ' сохранен в базу.');
        } else {
            $this->log->addInfo('Файл ' . $this->data['file'] . ' не найден');
        }
    }
}
