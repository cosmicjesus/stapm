<?php

namespace App\Jobs;

use App\Models\Student;
use App\Models\UserParent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddParentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $parent;
    private $id;


    /**
     * AddParentJob constructor.
     * @param int $id
     * @param array $parent
     */
    public function __construct(int $id, array $parent)
    {
        $this->id = $id;
        $this->parent = $parent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $parent = $this->parent;
        if (array_key_exists('place_of_work', $parent)) {
            $parent['place_of_work'] = str_replace(["\r\n", "\r", "\n"], ' ', strip_tags($parent['place_of_work']));
        } else {
            $parent['place_of_work'] = null;
        }


        $parentModel = UserParent::query()->firstOrCreate($parent);

        $entrant = Student::query()->find($this->id);

        $entrant->parents()->attach($parentModel->id);
    }
}
