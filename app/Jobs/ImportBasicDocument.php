<?php

namespace App\Jobs;

use App\Models\ProgramBasicDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class ImportBasicDocument implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $pathTpl = '{program_id}/documents/{file}';
    protected $annotationPath;
    protected $standartPath;
    protected $log;
    protected $logPath = '/logs/ImportBasicDocuments.log';

    public function __construct($array)
    {
        $this->data = $array;
        $this->log = new Logger('FtpSyncLog');
        $this->log->pushHandler(new RotatingFileHandler(storage_path() . $this->logPath, 7));
        $this->standartPath = massStrReplace($this->pathTpl,
            [
                'program_id' => $array['program'],
                'file' => $array['standart']
            ]
        );

        $this->annotationPath = massStrReplace($this->pathTpl,
            [
                'program_id' => $array['program'],
                'file' => $array['annotation']
            ]
        );
    }


    public function handle()
    {

        $annotationName = $this->data['program'] . "/documents/" . str_random(11) . ".pdf";
        $this->log->addInfo('Ищем файл аннотации ' . $this->data['annotation']);

        if (Storage::disk('sync')->has($this->annotationPath)) {
            $this->log->addInfo('Файл ' . $this->data['annotation'] . ' найден. Грузим');
            try {
                $content = Storage::disk('sync')->get($this->annotationPath);
            } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            }

            Storage::disk('program_files')->put($annotationName, $content);
            $this->log->addInfo('Файл ' . $this->data['annotation'] . ' загружен и сохранен. Пишем в базу');
        } else {
            $this->log->addInfo('Файл ' . $this->data['annotation'] . ' не найден');
        }


        $standartName = $this->data['program'] . "/documents/" . str_random(11) . ".pdf";

        if (Storage::disk('sync')->has($this->annotationPath)) {
            $this->log->addInfo('Файл ' . $this->data['standart'] . ' найден. Грузим');

            try {
                $content = Storage::disk('sync')->get($this->standartPath);
            } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            }

            Storage::disk('program_files')->put($standartName, $content);
            $this->log->addInfo('Файл ' . $this->data['standart'] . ' загружен и сохранен. Пишем в базу');
        } else {
            $this->log->addInfo('Файл ' . $this->data['standart'] . ' не найден');
        }


        $model = ProgramBasicDocument::query()->where('profession_program_id', $this->data['program'])->first();
        $model->standart = $standartName;
        $model->annotation = $annotationName;
        $model->save();

    }
}
