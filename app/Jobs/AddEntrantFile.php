<?php

namespace App\Jobs;

use App\AppLogic\Storage\StudentStorage;
use App\Models\Student;
use App\Models\StudentFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddEntrantFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $entrant;
    public $fileData;

    public function __construct(Student $entrant, $fileData)
    {
        $this->entrant = $entrant;
        $this->fileData = $fileData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = (new StudentStorage())->putFile($this->entrant->id, $this->fileData['file']);

        $file = new StudentFile([
            'name' => $this->fileData['name'],
            'path' => $path
        ]);

        $this->entrant->files()->save($file);
    }
}
