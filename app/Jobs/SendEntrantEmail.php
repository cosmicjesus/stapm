<?php

namespace App\Jobs;

use App\Mail\EntrantMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEntrantEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $entrant;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($entrant)
    {
        $this->entrant = $entrant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->entrant->email)->send(new EntrantMail($this->entrant));
    }
}
