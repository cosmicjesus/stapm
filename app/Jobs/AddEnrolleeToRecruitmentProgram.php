<?php

namespace App\Jobs;

use App\Models\RecruitmentProgramEnrollee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddEnrolleeToRecruitmentProgram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $program;

    /**
     * Create a new job instance.
     *
     * @param int $id
     * @param array $program
     */
    public function __construct(int $id, array $program)
    {
        $this->id = $id;
        $this->program = $program;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        RecruitmentProgramEnrollee::query()->create([
            'recruitment_program_id'=>$this->program['program_id'],
            'enrollee_id'=>$this->id,
            'is_priority'=>$this->program['is_priority'],
            'sum_of_grades_of_specialized_disciplines'=>$this->program['sum_of_grades_of_specialized_disciplines'],
        ]);
    }
}
