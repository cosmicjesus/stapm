(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/client/js/scripts"],{

/***/ "./resources/js/code/mobile_menu.js":
/*!******************************************!*\
  !*** ./resources/js/code/mobile_menu.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _jquery = _interopRequireDefault(__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var items = document.querySelectorAll('.mobile-menu__item');
items.forEach(function (n) {
  n.onclick = function () {
    this.classList.toggle('open');
  };
});
var menuBtn = document.getElementsByClassName('mobile-btn');
console.log(menuBtn);
menuBtn[0].addEventListener('click', function (e) {
  var menu = document.querySelector("#mobile-menu");
  var isOpen = menu.classList.contains('active');
  console.log(isOpen);

  if (isOpen) {
    vm.closeMenu(menu);
  } else {
    vm.openMenu(menu);
  }
});

/***/ }),

/***/ "./resources/js/scripts.js":
/*!*********************************!*\
  !*** ./resources/js/scripts.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./code/mobile_menu */ "./resources/js/code/mobile_menu.js");

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/scripts.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/evgenijsilantev/sites/stpm/resources/js/scripts.js */"./resources/js/scripts.js");


/***/ })

},[[1,"/client/js/manifest","/client/js/vendor"]]]);